﻿Imports System.Collections.Generic
Imports System.Text

Namespace TheMovieDB
    Public Enum TmdbOrderBy
        none = 0
        rating = 1
        release = 2
        title = 3
    End Enum
End Namespace
