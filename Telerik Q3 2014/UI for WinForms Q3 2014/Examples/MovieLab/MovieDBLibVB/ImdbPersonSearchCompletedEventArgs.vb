﻿Imports System.Collections.Generic
Imports System.Text
Imports System

Namespace TheMovieDB
    Public Class ImdbPersonSearchCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        Private m_people As TmdbPerson()
        Public ReadOnly Property People() As TmdbPerson()
            Get
                RaiseExceptionIfNecessary()
                Return m_people
            End Get
        End Property

        Public Sub New(people As TmdbPerson(), e As Exception, canceled As Boolean, state As Object)
            MyBase.New(e, canceled, state)
            Me.m_people = people
        End Sub
    End Class
End Namespace
