﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Xml.Serialization

Namespace TheMovieDB
    <XmlRoot("OpenSearchDescription")> _
    Public Class TmdbPersonSearchResults
        <XmlArrayAttribute("people")> _
        Public Property People() As TmdbPerson()
            Get
                Return m_People
            End Get
            Set(value As TmdbPerson())
                m_People = Value
            End Set
        End Property
        Private m_People As TmdbPerson()
    End Class
End Namespace
