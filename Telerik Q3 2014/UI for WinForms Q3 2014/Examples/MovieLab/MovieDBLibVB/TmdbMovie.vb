﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Xml.Serialization
Imports System.ComponentModel
Imports System.Drawing
Imports System

Namespace TheMovieDB
    <XmlType("movie")> _
    Public Class TmdbMovie
        <XmlElement("score")> _
        Private Property _score() As System.Nullable(Of Decimal)
            Get
                Return m__score
            End Get
            Set(value As System.Nullable(Of Decimal))
                m__score = Value
            End Set
        End Property
        Private m__score As System.Nullable(Of Decimal)
        Public ReadOnly Property Score() As Decimal
            Get
                If Me._score.HasValue Then
                    Return Me._score.Value
                End If

                Return 0
            End Get
        End Property

        <XmlElement("popularity")> _
        Public Property Popularity() As Decimal
            Get
                Return m_Popularity
            End Get
            Set(value As Decimal)
                m_Popularity = Value
            End Set
        End Property
        Private m_Popularity As Decimal

        <XmlElement("translated")> _
        Public Property Translated() As Boolean
            Get
                Return m_Translated
            End Get
            Set(value As Boolean)
                m_Translated = Value
            End Set
        End Property
        Private m_Translated As Boolean

        <XmlElement("language")> _
        Public Property Language() As String
            Get
                Return m_Language
            End Get
            Set(value As String)
                m_Language = Value
            End Set
        End Property
        Private m_Language As String

        <XmlElement("name")> _
        Public Property Name() As String
            Get
                Return m_Name
            End Get
            Set(value As String)
                m_Name = Value
            End Set
        End Property
        Private m_Name As String

        <XmlElement("alternative_name")> _
        Public Property AlternativeName() As String
            Get
                Return m_AlternativeName
            End Get
            Set(value As String)
                m_AlternativeName = Value
            End Set
        End Property
        Private m_AlternativeName As String

        <XmlElement("type")> _
        Public Property Type() As String
            Get
                Return m_Type
            End Get
            Set(value As String)
                m_Type = Value
            End Set
        End Property
        Private m_Type As String

        <XmlElement("id")> _
        Public Property Id() As Integer
            Get
                Return m_Id
            End Get
            Set(value As Integer)
                m_Id = Value
            End Set
        End Property
        Private m_Id As Integer

        <XmlElement("imdb_id")> _
        Public Property ImdbId() As String
            Get
                Return m_ImdbId
            End Get
            Set(value As String)
                m_ImdbId = Value
            End Set
        End Property
        Private m_ImdbId As String

        <XmlElement("url")> _
        Public Property Url() As String
            Get
                Return m_Url
            End Get
            Set(value As String)
                m_Url = Value
            End Set
        End Property
        Private m_Url As String

        <XmlElement("rating")> _
        Public Property Rating() As Decimal
            Get
                Return m_Rating
            End Get
            Set(value As Decimal)
                m_Rating = Value
            End Set
        End Property
        Private m_Rating As Decimal

        <XmlElement("certification")> _
        Public Property Certification() As String
            Get
                Return m_Certification
            End Get
            Set(value As String)
                m_Certification = Value
            End Set
        End Property
        Private m_Certification As String

        <XmlElement("overview")> _
        Public Property Overview() As String
            Get
                Return m_Overview
            End Get
            Set(value As String)
                m_Overview = Value
            End Set
        End Property
        Private m_Overview As String

        <XmlElement("released")> _
        Public Property ReleasedString() As String
            Get
                Return If(Released.HasValue, Released.Value.ToShortDateString(), "")
            End Get
            Set(value As String)
                Dim d As DateTime
                If DateTime.TryParse(value, d) Then
                    Released = d
                Else
                    Released = Nothing
                End If
            End Set
        End Property

        Public Property Released() As System.Nullable(Of DateTime)
            Get
                Return m_Released
            End Get
            Set(value As System.Nullable(Of DateTime))
                m_Released = Value
            End Set
        End Property
        Private m_Released As System.Nullable(Of DateTime)

        <XmlElement("runtime")> _
        Public Property MovieRuntime() As String
            Get
                Return Runtime.TotalMinutes.ToString()
            End Get
            Set(value As String)
                Dim minutes As Integer
                Runtime = TimeSpan.FromMinutes(If(Integer.TryParse(value, minutes), minutes, 0))
            End Set
        End Property

        Public Property Runtime() As TimeSpan
            Get
                Return m_Runtime
            End Get
            Set(value As TimeSpan)
                m_Runtime = Value
            End Set
        End Property
        Private m_Runtime As TimeSpan

        <XmlElement("budget")> _
        Public Property Budget() As String
            Get
                Return m_Budget
            End Get
            Set(value As String)
                m_Budget = Value
            End Set
        End Property
        Private m_Budget As String

        <XmlElement("revenue")> _
        Public Property Revenue() As String
            Get
                Return m_Revenue
            End Get
            Set(value As String)
                m_Revenue = Value
            End Set
        End Property
        Private m_Revenue As String

        <XmlElement("homepage")> _
        Public Property Homepage() As String
            Get
                Return m_Homepage
            End Get
            Set(value As String)
                m_Homepage = Value
            End Set
        End Property
        Private m_Homepage As String

        <XmlElement("trailer")> _
        Public Property Trailer() As String
            Get
                Return m_Trailer
            End Get
            Set(value As String)
                m_Trailer = Value
            End Set
        End Property
        Private m_Trailer As String

        <XmlArrayAttribute("categories")> _
        Public Property Categories() As TmdbCategory()
            Get
                Return m_Categories
            End Get
            Set(value As TmdbCategory())
                m_Categories = Value
            End Set
        End Property
        Private m_Categories As TmdbCategory()

        <XmlArrayAttribute("studios")> _
        Public Property Studios() As TmdbStudio()
            Get
                Return m_Studios
            End Get
            Set(value As TmdbStudio())
                m_Studios = Value
            End Set
        End Property
        Private m_Studios As TmdbStudio()

        <XmlArrayAttribute("countries")> _
        Public Property Countries() As TmdbCountry()
            Get
                Return m_Countries
            End Get
            Set(value As TmdbCountry())
                m_Countries = Value
            End Set
        End Property
        Private m_Countries As TmdbCountry()

        <XmlArrayAttribute("images")> _
        Public Property Images() As TmdbImage()
            Get
                Return m_Images
            End Get
            Set(value As TmdbImage())
                m_Images = Value
            End Set
        End Property
        Private m_Images As TmdbImage()

        <XmlArrayAttribute("cast")> _
        Public Property Cast() As TmdbCastPerson()
            Get
                Return m_Cast
            End Get
            Set(value As TmdbCastPerson())
                m_Cast = Value
            End Set
        End Property
        Private m_Cast As TmdbCastPerson()

        <XmlElement("last_modified_at")> _
        Public Property LastModifiedString() As String
            Get
                Return If(LastModified.HasValue, LastModified.Value.ToShortDateString(), "")
            End Get
            Set(value As String)
                Dim d As DateTime
                If DateTime.TryParse(value, d) Then
                    LastModified = d
                Else
                    LastModified = Nothing
                End If
            End Set
        End Property

        Public Property LastModified() As System.Nullable(Of DateTime)
            Get
                Return m_LastModified
            End Get
            Set(value As System.Nullable(Of DateTime))
                m_LastModified = Value
            End Set
        End Property
        Private m_LastModified As System.Nullable(Of DateTime)

        Public Sub LoadImages()
            For Each image As TmdbImage In Images
                If image.Size = TmdbImageSize.thumb Then
                    image.LoadImage()
                End If
            Next
        End Sub

        Public Sub LoadPoster()
            For Each image As TmdbImage In Images
                If image.Type = TmdbImageType.poster AndAlso image.Size = TmdbImageSize.w342 Then
                    image.LoadImage()
                    Exit For
                End If
            Next
        End Sub

        Public Sub LoadRequest(request As TmdbImageLoadRequest)
            Dim count As Integer = 0
            For Each image As TmdbImage In Images
                If image.Type = request.type AndAlso image.Size = request.size Then
                    image.LoadImage()
                    count += 1
                    If count = request.count Then
                        Exit For
                    End If
                End If
            Next
        End Sub

        Public Sub LoadImagesAsync()
            LoadImagesAsync(Nothing, False)
        End Sub

        Public Sub LoadImagesAsync(state As Object, poster As Boolean)
            If poster Then
                LoadImagesAsync(state, New TmdbImageLoadRequest() With { _
                 .count = 1, _
                 .size = TmdbImageSize.w342, _
                 .type = TmdbImageType.poster _
                })
            Else
                LoadImagesAsync(state, New TmdbImageLoadRequest() With { _
                 .count = 3, _
                 .size = TmdbImageSize.original, _
                 .type = TmdbImageType.backdrop _
                })
            End If
        End Sub

        Public Sub LoadImagesAsync(state As Object, request As TmdbImageLoadRequest)
            Dim asyncOp As AsyncOperation = AsyncOperationManager.CreateOperation(Nothing)
            Dim worker As New MovieImagesLoadWorkerDelegate(AddressOf MovieImagesLoadWorker)
            worker.BeginInvoke(asyncOp, state, request, Nothing, Nothing)
        End Sub

        Public Delegate Sub MovieImagesLoadCompletedEventHandler(sender As Object, e As ImdbMovieImagesLoadCompletedEventArgs)

        Public Event MovieImagesLoadCompleted As MovieImagesLoadCompletedEventHandler

        Protected Overridable Sub OnMovieImagesLoadCompleted(e As Object)
            RaiseEvent MovieImagesLoadCompleted(Me, TryCast(e, ImdbMovieImagesLoadCompletedEventArgs))
        End Sub

        Private Delegate Sub MovieImagesLoadWorkerDelegate(asyncOp As AsyncOperation, state As Object, request As TmdbImageLoadRequest)

        Private Sub MovieImagesLoadWorker(asyncOp As AsyncOperation, state As Object, request As TmdbImageLoadRequest)
            Dim exception As Exception = Nothing
            Try
                If request IsNot Nothing Then
                    LoadRequest(request)
                Else
                    LoadImages()
                End If
            Catch ex As Exception
                exception = ex
            End Try

            Dim args As New ImdbMovieImagesLoadCompletedEventArgs(Me, exception, False, state, request)
            asyncOp.PostOperationCompleted(AddressOf OnMovieImagesLoadCompleted, args)
		End Sub

        Public ReadOnly Property Poster() As Image
            Get
                For Each image As TmdbImage In Me.Images
                    If image.Type = TmdbImageType.poster AndAlso image.Image IsNot Nothing Then
                        Return image.Image
                    End If
                Next
                Return Nothing
            End Get
        End Property
    End Class

    Public Class ImdbMovieImagesLoadCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        Private m_movie As TmdbMovie
        Private m_request As TmdbImageLoadRequest

        Public ReadOnly Property Movie() As TmdbMovie
            Get
                Return m_movie
            End Get
        End Property

        Public ReadOnly Property Request() As TmdbImageLoadRequest
            Get
                Return m_request
            End Get
        End Property

        Public Sub New(movie As TmdbMovie, e As Exception, canceled As Boolean, state As Object, request As TmdbImageLoadRequest)
            MyBase.New(e, canceled, state)
            Me.m_movie = movie
            Me.m_request = request
        End Sub
    End Class

    Public Class TmdbImageLoadRequest
        Public Property type() As TmdbImageType
            Get
                Return m_type
            End Get
            Set(value As TmdbImageType)
                m_type = Value
            End Set
        End Property
        Private m_type As TmdbImageType
        Public Property size() As TmdbImageSize
            Get
                Return m_size
            End Get
            Set(value As TmdbImageSize)
                m_size = Value
            End Set
        End Property
        Private m_size As TmdbImageSize
        Public Property count() As Integer
            Get
                Return m_count
            End Get
            Set(value As Integer)
                m_count = Value
            End Set
        End Property
        Private m_count As Integer
    End Class
End Namespace
