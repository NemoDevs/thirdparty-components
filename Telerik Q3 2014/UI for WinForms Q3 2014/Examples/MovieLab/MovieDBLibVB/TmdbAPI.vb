﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Net
Imports System.Xml.Serialization
Imports System.IO
Imports System.ComponentModel
Imports System.Text.RegularExpressions
Imports MovieLab
Imports System.Xml.Linq
Imports System

Namespace TheMovieDB
    ''' <summary>
    ''' The main class for using TheMovieDb.og API.
    ''' </summary>
    Public Class TmdbAPI
        Public Delegate Sub MovieSearchAsyncCompletedEventHandler(sender As Object, e As ImdbMovieSearchCompletedEventArgs)
        Public Delegate Sub MovieInfoAsyncCompletedEventHandler(sender As Object, e As ImdbMovieInfoCompletedEventArgs)
        Public Delegate Sub PersonSearchAsyncCompletedEventHandler(sender As Object, e As ImdbPersonSearchCompletedEventArgs)
        Public Delegate Sub PersonInfoAsyncCompletedEventHandler(sender As Object, e As ImdbPersonInfoCompletedEventArgs)

        Public Event MovieSearchCompleted As MovieSearchAsyncCompletedEventHandler
        Public Event MovieSearchByImdbCompleted As MovieSearchAsyncCompletedEventHandler

        Public Event GetMovieInfoCompleted As MovieInfoAsyncCompletedEventHandler
        Public Event GetMovieImagesCompleted As MovieInfoAsyncCompletedEventHandler

        Public Event PersonSearchCompleted As PersonSearchAsyncCompletedEventHandler
        Public Event GetPersonInfoCompleted As PersonInfoAsyncCompletedEventHandler

        Private Const MOVIE_GET_INFO_URL As String = "http://api.themoviedb.org/2.1/Movie.getInfo/en/xml/{0}/{1}"
        Private Const MOVIE_IMDB_LOOKUP_URL As String = "http://api.themoviedb.org/2.1/Movie.imdbLookup/en/xml/{0}/{1}"
        Private Const MOVIE_SEARCH_URL As String = "http://api.themoviedb.org/2.1/Movie.search/en/xml/{0}/{1}"
        Private Const MOVIE_GET_IMAGES_URL As String = "http://api.themoviedb.org/2.1/Movie.getImages/en/xml/{0}/{1}"
        Private Const MOVIE_BROWSE_URL As String = "http://api.themoviedb.org/2.1/Movie.browse/en-US/xml/{0}?{1}"
        Private Const GETLIST_ As String = "http://api.themoviedb.org/2.1/Genres.getList/en/xml/{0}"

        Private Const PERSON_SEARCH_URL As String = "http://api.themoviedb.org/2.1/Person.search/en/xml/{0}/{1}"
        Private Const PERSON_GET_INFO_URL As String = "http://api.themoviedb.org/2.1/Person.getInfo/en/xml/{0}/{1}"

        Public Property ApiKey() As String
            Get
                Return m_ApiKey
            End Get
            Set(value As String)
                m_ApiKey = Value
            End Set
        End Property
        Private m_ApiKey As String

        Public Sub New(apiKey__1 As String)
            ApiKey = apiKey__1
        End Sub

        Public Function GetList() As TmdbGenre()
            Try
                Dim webClient As New WebClient()

                Dim s As New XmlSerializer(GetType(TmdbGetListResults))

                Dim data As Byte() = DataCaching.GetBytesFromFile(DataCaching.GetFileListName())
                If data Is Nothing Then
                    data = webClient.DownloadData(String.Format(GETLIST_, ApiKey))
                    DataCaching.SaveBytesToFile(DataCaching.GetFileListName(), data)
                End If

                Dim xml As String = System.Text.Encoding.UTF8.GetString(data)
                If Not xml.StartsWith("<?xml") Then
                    Throw New Exception(xml.Replace("<p>", "").Replace("</p>", ""))
                End If

                Dim r As TextReader = New StringReader(xml)
                Dim search As TmdbGetListResults = DirectCast(s.Deserialize(r), TmdbGetListResults)
                Return search.Genres
            Catch
                Return New TmdbGenre() {}
            End Try
        End Function

        ''' <summary>
        ''' the easiest and quickest way to search for a movie. It is a mandatory method in order to get the movie id to pass to the GetMovieInfo method.
        ''' </summary>
        ''' <param name="title">The title of the movie to search for.</param>
        ''' <returns></returns>
        Public Function MovieSearch(title As String) As TmdbMovie()
            Dim webClient As New WebClient()

            Dim s As New XmlSerializer(GetType(TmdbMovieSearchResults))

            Dim data As Byte() = DataCaching.GetBytesFromFile(DataCaching.GetFileMovieName(Escape(title)))
            If data Is Nothing Then
                data = webClient.DownloadData(String.Format(MOVIE_SEARCH_URL, ApiKey, Escape(title)))
                DataCaching.SaveBytesToFile(DataCaching.GetFileMovieName(Escape(title)), data)
            End If

            Dim xml As String = System.Text.Encoding.UTF8.GetString(data)
            If Not xml.StartsWith("<?xml") Then
                Throw New Exception(xml.Replace("<p>", "").Replace("</p>", ""))
            End If

            Dim r As TextReader = New StringReader(xml)
            Dim search As TmdbMovieSearchResults = DirectCast(s.Deserialize(r), TmdbMovieSearchResults)
            Return search.Movies

        End Function

#Region "MovieSearchAsyncMethods"
        Private Delegate Sub MovieSearchDelegate(title As String, userState As Object, asyncOp As AsyncOperation)
        Public Sub MovieSearchAsync(title As String)
            MovieSearchAsync(title, Nothing)
        End Sub
        Public Sub MovieSearchAsync(title As String, userState As Object)
            Dim asyncOp As AsyncOperation = AsyncOperationManager.CreateOperation(Nothing)
            Dim worker As New MovieSearchDelegate(AddressOf MovieSearchWorker)
            worker.BeginInvoke(title, userState, asyncOp, Nothing, Nothing)
        End Sub
        Private Sub MovieSearchWorker(title As String, userState As Object, asyncOp As AsyncOperation)
            Dim exception As Exception = Nothing
            Dim movies As TmdbMovie() = Nothing
            Try
                movies = MovieSearch(title)
            Catch ex As Exception
                exception = ex
            End Try
            Dim args As New ImdbMovieSearchCompletedEventArgs(movies, exception, False, userState)
			asyncOp.PostOperationCompleted(AddressOf CallOnMovieSearchCompleted, args)
		End Sub

        Private Sub CallOnMovieSearchCompleted(e As Object)
            OnMovieSearchCompleted(DirectCast(e, ImdbMovieSearchCompletedEventArgs))
        End Sub

        Protected Overridable Sub OnMovieSearchCompleted(e As ImdbMovieSearchCompletedEventArgs)
            RaiseEvent MovieSearchCompleted(Me, e)
        End Sub


#End Region

        Public Function MovieBrowse(request As TmdbMovieBrowseRequest) As TmdbMovie()
            Dim webClient As New WebClient()

            Dim arguments As New StringBuilder()

            arguments.Append("order_by=" & [Enum].GetName(GetType(TmdbOrderBy), request.order_by))
            arguments.Append("&order=" & [Enum].GetName(GetType(TmdbOrderType), request.order))

            If request.per_page <> 0 Then
                arguments.Append("&per_page=" & Convert.ToString(request.per_page))
            End If
            If request.page <> 0 Then
                arguments.Append("&page=" & Convert.ToString(request.page))
            End If
            If Not String.IsNullOrEmpty(request.query) Then
                arguments.Append("&query=" & Escape(request.query))
            End If
            If request.min_votes <> 0 Then
                arguments.Append("&min_votes=" & Convert.ToString(request.min_votes))
            End If
            If request.rating_min <> 0 Then
                arguments.Append("&rating_min=" & Convert.ToString(request.rating_min))
            End If
            If request.rating_max <> 0 Then
                arguments.Append("&rating_max=" & Convert.ToString(request.rating_max))
            End If
            If Not String.IsNullOrEmpty(request.genres) Then
                arguments.Append("&genres=" & Convert.ToString(request.genres))
            End If
            If request.release_min <> DateTime.MinValue Then
                arguments.Append("&release_min=" & DateToEpoch(request.release_min))
            End If
            If request.release_max <> DateTime.MinValue Then
                arguments.Append("&release_max=" & DateToEpoch(request.release_max))
            End If
            If request.year <> 0 Then
                arguments.Append("&year=" & Convert.ToString(request.year))
            End If

            Dim s As New XmlSerializer(GetType(TmdbMovieSearchResults))
            Dim requestString As String = String.Format(MOVIE_BROWSE_URL, ApiKey, arguments.ToString())

            Dim data As Byte() = DataCaching.GetBytesFromFile(DataCaching.GetFileMovieBrowseName(Escape(arguments.ToString())))
            If data Is Nothing Then
                data = webClient.DownloadData(requestString)
                DataCaching.SaveBytesToFile(DataCaching.GetFileMovieBrowseName(Escape(arguments.ToString())), data)
            End If

            Dim xml As String = System.Text.Encoding.UTF8.GetString(data)
            If Not xml.StartsWith("<?xml") Then
                Throw New Exception(xml.Replace("<p>", "").Replace("</p>", ""))
            End If

            Dim r As TextReader = New StringReader(xml)
            Dim search As TmdbMovieSearchResults = DirectCast(s.Deserialize(r), TmdbMovieSearchResults)
            Return search.Movies
        End Function

#Region "MovieBrowseAsyncMethods"

        Private Delegate Sub MovieBrowseDelegate(request As TmdbMovieBrowseRequest, userState As Object, asyncOp As AsyncOperation)
        Public Sub MovieBrowseAsync(request As TmdbMovieBrowseRequest)
            MovieBrowseAsync(request, Nothing)
        End Sub
        Public Sub MovieBrowseAsync(request As TmdbMovieBrowseRequest, userState As Object)
            Dim asyncOp As AsyncOperation = AsyncOperationManager.CreateOperation(Nothing)
            Dim worker As New MovieBrowseDelegate(AddressOf MovieBrowseWorker)
            worker.BeginInvoke(request, userState, asyncOp, Nothing, Nothing)
        End Sub
        Private Sub MovieBrowseWorker(request As TmdbMovieBrowseRequest, userState As Object, asyncOp As AsyncOperation)
            Dim exception As Exception = Nothing
            Dim movies As TmdbMovie() = Nothing
            Try
                movies = MovieBrowse(request)
            Catch ex As Exception
                exception = ex
            End Try
            Dim args As New ImdbMovieSearchCompletedEventArgs(movies, exception, False, userState)
			asyncOp.PostOperationCompleted(AddressOf CallOnMovieSearchCompleted, args)
		End Sub

#End Region

        ''' <summary>
        ''' The easiest and quickest way to search for a movie based on it's IMDb ID. You can use Movie.imdbLookup method to get the TMDb id of a movie if you already have the IMDB id.
        ''' </summary>
        ''' <param name="imdbId">The IMDb ID of the movie you are searching for.</param>
        ''' <returns></returns>
        Public Function MovieSearchByImdb(imdbId As String) As TmdbMovie()
            Dim webClient As New WebClient()

            Dim s As New XmlSerializer(GetType(TmdbMovieSearchResults))

            Dim data As Byte() = DataCaching.GetBytesFromFile(DataCaching.GetFileMovieIMDBidName(imdbId))
            If data Is Nothing Then
                data = webClient.DownloadData(String.Format(MOVIE_IMDB_LOOKUP_URL, ApiKey, imdbId))
                DataCaching.SaveBytesToFile(DataCaching.GetFileMovieIMDBidName(imdbId), data)
            End If
            Dim xml As String = System.Text.Encoding.UTF8.GetString(data)
            If Not xml.StartsWith("<?xml") Then
                Throw New Exception(xml.Replace("<p>", "").Replace("</p>", ""))
            End If

            Dim r As TextReader = New StringReader(xml)
            Dim search As TmdbMovieSearchResults = DirectCast(s.Deserialize(r), TmdbMovieSearchResults)
            Return search.Movies

        End Function

#Region "MovieSearchByImdbAsyncMethods"
        Private Delegate Sub MovieSearchByImdbDelegate(imdbId As String, userState As Object, asyncOp As AsyncOperation)
        Public Sub MovieSearchByImdbAsync(imdbId As String)
            MovieSearchByImdbAsync(imdbId, Nothing)
        End Sub
        Public Sub MovieSearchByImdbAsync(imdbId As String, userState As Object)
            Dim asyncOp As AsyncOperation = AsyncOperationManager.CreateOperation(Nothing)
            Dim worker As New MovieSearchDelegate(AddressOf MovieSearchByImdbWorker)
            worker.BeginInvoke(imdbId, userState, asyncOp, Nothing, Nothing)
        End Sub
        Private Sub MovieSearchByImdbWorker(imdbId As String, userState As Object, asyncOp As AsyncOperation)
            Dim exception As Exception = Nothing
            Dim movies As TmdbMovie() = Nothing
            Try
                movies = MovieSearchByImdb(imdbId)
            Catch ex As Exception
                exception = ex
            End Try
            Dim args As New ImdbMovieSearchCompletedEventArgs(movies, exception, False, userState)
            asyncOp.PostOperationCompleted(AddressOf OnMovieSearchByImdbCompleted, args)
		End Sub

        Protected Overridable Sub OnMovieSearchByImdbCompleted(e As Object)
            RaiseEvent MovieSearchByImdbCompleted(Me, TryCast(e, ImdbMovieSearchCompletedEventArgs))
        End Sub
#End Region

        ''' <summary>
        ''' retrieve specific information about a movie. Things like overview, release date, cast data, genre's, YouTube trailer link, etc...
        ''' </summary>
        ''' <param name="id">The ID of the TMDb movie you are searching for.</param>
        ''' <returns></returns>
        Public Function GetMovieInfo(id As Integer) As TmdbMovie
            Dim webClient As New WebClient()

            Dim s As New XmlSerializer(GetType(TmdbMovieSearchResults))

            Dim data As Byte() = DataCaching.GetBytesFromFile(DataCaching.GetFileMovieIDName(id.ToString()))

            If data Is Nothing Then
                data = webClient.DownloadData(String.Format(MOVIE_GET_INFO_URL, ApiKey, id))
                DataCaching.SaveBytesToFile(DataCaching.GetFileMovieIDName(id.ToString()), data)
            End If

            Dim xml As String = System.Text.Encoding.UTF8.GetString(data)
            If Not xml.StartsWith("<?xml") Then
                Throw New Exception(xml.Replace("<p>", "").Replace("</p>", ""))
            End If

            Dim r As TextReader = New StringReader(xml)
            Dim search As TmdbMovieSearchResults = DirectCast(s.Deserialize(r), TmdbMovieSearchResults)

            If search.Movies.Length = 1 Then
                Return search.Movies(0)
            Else
                Return Nothing
            End If
        End Function

#Region "GetMovieInfoAsyncMethods"
        Private Delegate Sub GetMovieInfoDelegate(id As Integer, userState As Object, asyncOp As AsyncOperation)
        Public Sub GetMovieInfoAsync(id As Integer)
            GetMovieInfoAsync(id, Nothing)
        End Sub
        Public Sub GetMovieInfoAsync(id As Integer, userState As Object)
            Dim asyncOp As AsyncOperation = AsyncOperationManager.CreateOperation(Nothing)
            Dim worker As New GetMovieInfoDelegate(AddressOf GetMovieInfoWorker)
            worker.BeginInvoke(id, userState, asyncOp, Nothing, Nothing)
        End Sub
        Private Sub GetMovieInfoWorker(id As Integer, userState As Object, asyncOp As AsyncOperation)
            Dim exception As Exception = Nothing
            Dim movie As TmdbMovie = Nothing
            Try
                movie = GetMovieInfo(id)
            Catch ex As Exception
                exception = ex
            End Try
            Dim args As New ImdbMovieInfoCompletedEventArgs(movie, exception, False, userState)
			asyncOp.PostOperationCompleted(AddressOf OnGetMovieInfoCompleted, args)
		End Sub
        Protected Overridable Sub OnGetMovieInfoCompleted(e As Object)
            RaiseEvent GetMovieInfoCompleted(Me, TryCast(e, ImdbMovieInfoCompletedEventArgs))
        End Sub
#End Region

        ''' <summary>
        ''' Retrieve all of the backdrops and posters for a particular movie. This is useful to scan for updates, or new images if that's all you're after.
        ''' </summary>
        ''' <param name="id">TMDb ID (starting with tt) of the movie you are searching for.</param>
        ''' <returns></returns>
        Public Function GetMovieImages(id As Integer) As TmdbMovie
            Return GetMovieImages(id.ToString())
        End Function

#Region "GetMovieImagesMethods"
        Private Delegate Sub GetMovieImagesDelegate(imdbId As String, userState As Object, asyncOp As AsyncOperation)
        Public Sub GetMovieImagesAsync(id As Integer)
            GetMovieImagesAsync(id, Nothing)
        End Sub
        Public Sub GetMovieImagesAsync(id As Integer, userState As Object)
            Dim asyncOp As AsyncOperation = AsyncOperationManager.CreateOperation(Nothing)
            Dim worker As New GetMovieImagesDelegate(AddressOf GetMovieImagesWorker)
            worker.BeginInvoke(id.ToString(), userState, asyncOp, Nothing, Nothing)
        End Sub
        Private Sub GetMovieImagesWorker(id As String, userState As Object, asyncOp As AsyncOperation)
            Dim exception As Exception = Nothing
            Dim movie As TmdbMovie = Nothing
            Try
                movie = GetMovieImages(id)
            Catch ex As Exception
                exception = ex
            End Try
            Dim args As New ImdbMovieInfoCompletedEventArgs(movie, exception, False, userState)
            asyncOp.PostOperationCompleted(AddressOf OnGetMovieImagesCompleted, args)
		End Sub
        Protected Overridable Sub OnGetMovieImagesCompleted(e As Object)
            RaiseEvent GetMovieImagesCompleted(Me, TryCast(e, ImdbMovieInfoCompletedEventArgs))
        End Sub
#End Region

        ''' <summary>
        ''' Retrieve all of the backdrops and posters for a particular movie. This is useful to scan for updates, or new images if that's all you're after.
        ''' </summary>
        ''' <param name="imdbId">IMDB ID (starting with tt) of the movie you are searching for.</param>
        ''' <returns></returns>
        Public Function GetMovieImages(imdbId As String) As TmdbMovie
            Dim xdoc As XDocument = XDocument.Load(String.Format(MOVIE_GET_IMAGES_URL, ApiKey, imdbId))

            For Each xmovie As XElement In xdoc.Descendants("movie")
                Dim movie As New TmdbMovie() With { _
                 .Name = xmovie.Element("name").Value _
                }
                Dim ximages As XElement = xmovie.Element("images")
                If ximages Is Nothing Then
                    Continue For
                End If

                Dim images As New List(Of TmdbImage)()
                For Each ximageType As XElement In ximages.Elements()
                    Dim type As TmdbImageType = DirectCast([Enum].Parse(GetType(TmdbImageType), ximageType.Name.ToString()), TmdbImageType)
                    'int imageId = int.Parse(ximageType.Attribute("id").Value);
                    For Each ximage As XElement In ximageType.Elements("image")
                        Dim image As New TmdbImage() With { _
                         .Id = ximageType.Attribute("id").Value, _
                         .Size = DirectCast([Enum].Parse(GetType(TmdbImageSize), ximage.Attribute("size").Value), TmdbImageSize), _
                         .Type = type, _
                         .Url = ximage.Attribute("url").Value _
                        }
                        images.Add(image)
                    Next
                Next
                movie.Images = images.ToArray()
                Return movie
            Next
            Return Nothing
        End Function

#Region "GetMovieImagesMethods"
        Public Sub GetMovieImagesAsync(imdbId As String)
            GetMovieImagesAsync(imdbId, Nothing)
        End Sub
        Public Sub GetMovieImagesAsync(imdbId As String, userState As Object)
            Dim asyncOp As AsyncOperation = AsyncOperationManager.CreateOperation(Nothing)
            Dim worker As New GetMovieImagesDelegate(AddressOf GetMovieImagesWorker)
            worker.BeginInvoke(imdbId, userState, asyncOp, Nothing, Nothing)
        End Sub

#End Region

        ''' <summary>
        ''' Search for an actor, actress or production member.
        ''' </summary>
        ''' <param name="name">The name of the person you are searching for.</param>
        ''' <returns>TmdbPerson[]</returns>
        Public Function PersonSearch(name As String) As TmdbPerson()
            Dim webClient As New WebClient()

            Dim s As New XmlSerializer(GetType(TmdbPersonSearchResults))

            Dim data As Byte() = DataCaching.GetBytesFromFile(DataCaching.GetFilePersonName(name))

            If data Is Nothing Then
                data = webClient.DownloadData(String.Format(PERSON_SEARCH_URL, ApiKey, Escape(name)))
                DataCaching.SaveBytesToFile(DataCaching.GetFilePersonName(name), data)
            End If

            Dim xml As String = System.Text.Encoding.UTF8.GetString(data)
            If Not xml.StartsWith("<?xml") Then
                Throw New Exception(xml.Replace("<p>", "").Replace("</p>", ""))
            End If

            Dim r As TextReader = New StringReader(xml)
            Dim search As TmdbPersonSearchResults = DirectCast(s.Deserialize(r), TmdbPersonSearchResults)
            Return search.People
        End Function

#Region "PersonSearchAsyncMethods"
        Private Delegate Sub PersonSearchDelegate(name As String, userState As Object, asyncOp As AsyncOperation)
        Public Sub PersonSearchAsync(name As String)
            PersonSearchAsync(name, Nothing)
        End Sub
        Public Sub PersonSearchAsync(name As String, userState As Object)
            Dim asyncOp As AsyncOperation = AsyncOperationManager.CreateOperation(Nothing)
            Dim worker As New PersonSearchDelegate(AddressOf PersonSearchWorker)
            worker.BeginInvoke(name, userState, asyncOp, Nothing, Nothing)
        End Sub
        Private Sub PersonSearchWorker(name As String, userState As Object, asyncOp As AsyncOperation)
            Dim exception As Exception = Nothing
            Dim people As TmdbPerson() = Nothing
            Try
                people = PersonSearch(name)
            Catch ex As Exception
                exception = ex
            End Try
            Dim args As New ImdbPersonSearchCompletedEventArgs(people, exception, False, userState)
            asyncOp.PostOperationCompleted(AddressOf OnPersonSearchCompleted, args)
		End Sub
        Protected Overridable Sub OnPersonSearchCompleted(e As Object)
            RaiseEvent PersonSearchCompleted(Me, TryCast(e, ImdbPersonSearchCompletedEventArgs))
        End Sub
#End Region

        ''' <summary>
        ''' Retrieve the full filmography, known movies, images and things like birthplace for a specific person in the TMDb database.
        ''' </summary>
        ''' <param name="id">The ID of the TMDb person you are searching for.</param>
        ''' <returns>TmdbPerson</returns>
        Public Function GetPersonInfo(id As Integer) As TmdbPerson
            Dim webClient As New WebClient()

            Dim s As New XmlSerializer(GetType(TmdbPersonSearchResults))

            Dim data As Byte() = DataCaching.GetBytesFromFile(DataCaching.GetFilePersonIdName(id.ToString()))
            If data Is Nothing Then
                data = webClient.DownloadData(String.Format(PERSON_GET_INFO_URL, ApiKey, id))
                DataCaching.SaveBytesToFile(DataCaching.GetFilePersonIdName(id.ToString()), data)
            End If

            Dim xml As String = System.Text.Encoding.UTF8.GetString(data)
            If Not xml.StartsWith("<?xml") Then
                Throw New Exception(xml.Replace("<p>", "").Replace("</p>", ""))
            End If

            Dim r As TextReader = New StringReader(xml)
            Dim search As TmdbPersonSearchResults

            Try
                search = DirectCast(s.Deserialize(r), TmdbPersonSearchResults)
            Catch ex As Exception
            End Try

            If search.People.Length = 1 Then
                Return search.People(0)
            Else
                Return Nothing
            End If
        End Function

#Region "GetPersonInfoAsyncMethods"
        Private Delegate Sub GetPersonInfoDelegate(id As Integer, userState As Object, asyncOp As AsyncOperation)
        Public Sub GetPersonInfoAsync(id As Integer)
            GetPersonInfoAsync(id, Nothing)
        End Sub
        Public Sub GetPersonInfoAsync(id As Integer, userState As Object)
            Dim asyncOp As AsyncOperation = AsyncOperationManager.CreateOperation(Nothing)
            Dim worker As New GetPersonInfoDelegate(AddressOf GetPersonInfoWorker)
            worker.BeginInvoke(id, userState, asyncOp, Nothing, Nothing)
        End Sub
        Private Sub GetPersonInfoWorker(id As Integer, userState As Object, asyncOp As AsyncOperation)
            Dim exception As Exception = Nothing
            Dim person As TmdbPerson = Nothing
            Try
                person = GetPersonInfo(id)
            Catch ex As Exception
                exception = ex
            End Try
            Dim args As ImdbPersonInfoCompletedEventArgs = New ImdbPersonInfoCompletedEventArgs(person, exception, False, userState)
            asyncOp.PostOperationCompleted(AddressOf CallOnGetPersonInfoCompleted, args)
        End Sub
        Protected Overridable Sub OnGetPersonInfoCompleted(e As ImdbPersonInfoCompletedEventArgs)
            RaiseEvent GetPersonInfoCompleted(Me, e)
        End Sub

        Private Sub CallOnGetPersonInfoCompleted(e As Object)
            OnGetPersonInfoCompleted(DirectCast(e, ImdbPersonInfoCompletedEventArgs))
        End Sub

#End Region

        Public Shared Function Escape(s As String) As String
            Return Regex.Replace(s, "[" & Regex.Escape(New [String](Path.GetInvalidFileNameChars())) & "]", "-")
        End Function

        Public Shared Function DateFromEpoch(epoch As Long) As System.DateTime
            Dim value As System.DateTime = New System.DateTime(1970, 1, 1, 0, 0, 0, _
             0).ToLocalTime()
            Return value.AddSeconds(epoch)
        End Function

        Public Shared Function DateToEpoch([date] As System.DateTime) As Double
            Dim epoch As System.DateTime = New System.DateTime(1970, 1, 1, 0, 0, 0, _
             0).ToLocalTime()
            Dim span As System.TimeSpan = ([date] - epoch)
            Return span.TotalSeconds
        End Function
    End Class
End Namespace
