﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Xml.Serialization

Namespace TheMovieDB
    <XmlType("genre")> _
    Public Class TmdbGenre
        <XmlAttribute("name")> _
        Public Property Name() As String
            Get
                Return m_Name
            End Get
            Set(value As String)
                m_Name = Value
            End Set
        End Property
        Private m_Name As String

        <XmlElement("id")> _
        Public Property ID() As Integer
            Get
                Return m_ID
            End Get
            Set(value As Integer)
                m_ID = Value
            End Set
        End Property
        Private m_ID As Integer

        <XmlElement("url")> _
        Public Property Url() As String
            Get
                Return m_Url
            End Get
            Set(value As String)
                m_Url = Value
            End Set
        End Property
        Private m_Url As String

        Public Shared Function Build(genres As TmdbGenre(), ParamArray args As String()) As String
            Dim result As New StringBuilder()

            For Each genreName As String In args
                For Each genre As TmdbGenre In genres
                    If genre.Name = genreName Then
                        If result.Length > 0 Then
                            result.Append(",")
                        End If
                        result.Append(genre.ID)
                        Exit For
                    End If
                Next
            Next

            Return result.ToString()
        End Function
    End Class
End Namespace
