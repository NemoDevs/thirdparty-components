﻿Imports System.Collections.Generic
Imports System.Text
Imports System

Namespace TheMovieDB
    Public Class ImdbPersonInfoCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        Private m_person As TmdbPerson
        Public ReadOnly Property Person() As TmdbPerson
            Get
                RaiseExceptionIfNecessary()
                Return m_person
            End Get
        End Property

        Public Sub New(person As TmdbPerson, e As Exception, canceled As Boolean, state As Object)
            MyBase.New(e, canceled, state)
            Me.m_person = person
        End Sub
    End Class
End Namespace
