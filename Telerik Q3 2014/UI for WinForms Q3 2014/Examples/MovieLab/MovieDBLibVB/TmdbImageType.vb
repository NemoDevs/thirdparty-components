﻿Imports System.Collections.Generic
Imports System.Text

Namespace TheMovieDB
    Public Enum TmdbImageType
        poster = 0
        backdrop = 1
        profile = 2
    End Enum
End Namespace
