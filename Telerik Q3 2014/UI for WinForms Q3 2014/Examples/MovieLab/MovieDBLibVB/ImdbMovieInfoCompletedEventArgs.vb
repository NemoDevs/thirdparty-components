﻿Imports System.Collections.Generic
Imports System.Text
Imports System

Namespace TheMovieDB
    Public Class ImdbMovieInfoCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        Private m_movie As TmdbMovie
        Public ReadOnly Property Movie() As TmdbMovie
            Get
                'RaiseExceptionIfNecessary();
                Return m_movie
            End Get
        End Property

        Public Sub New(movie As TmdbMovie, e As Exception, canceled As Boolean, state As Object)
            MyBase.New(e, canceled, state)
            Me.m_movie = movie
        End Sub
    End Class
End Namespace
