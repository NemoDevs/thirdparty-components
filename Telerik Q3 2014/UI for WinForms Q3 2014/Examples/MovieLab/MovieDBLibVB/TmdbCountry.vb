﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Xml.Serialization

Namespace TheMovieDB
    <XmlType("country")> _
    Public Class TmdbCountry
        <XmlAttribute("name")> _
        Public Property Name() As String
            Get
                Return m_Name
            End Get
            Set(value As String)
                m_Name = Value
            End Set
        End Property
        Private m_Name As String

        <XmlAttribute("code")> _
        Public Property Code() As String
            Get
                Return m_Code
            End Get
            Set(value As String)
                m_Code = Value
            End Set
        End Property
        Private m_Code As String

        <XmlAttribute("url")> _
        Public Property Url() As String
            Get
                Return m_Url
            End Get
            Set(value As String)
                m_Url = Value
            End Set
        End Property
        Private m_Url As String
    End Class
End Namespace
