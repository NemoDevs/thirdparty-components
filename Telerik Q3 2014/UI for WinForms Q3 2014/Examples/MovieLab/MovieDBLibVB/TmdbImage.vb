﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Xml.Serialization
Imports System.Drawing
Imports System.Net
Imports System.IO
Imports MovieLab
Imports System

Namespace TheMovieDB
    <XmlType("image")> _
    Public Class TmdbImage
        Implements IDisposable
        <XmlAttribute("type")> _
        Public Property Type() As TmdbImageType
            Get
                Return m_Type
            End Get
            Set(value As TmdbImageType)
                m_Type = Value
            End Set
        End Property
        Private m_Type As TmdbImageType

        <XmlAttribute("size")> _
        Public Property Size() As TmdbImageSize
            Get
                Return m_Size
            End Get
            Set(value As TmdbImageSize)
                m_Size = Value
            End Set
        End Property
        Private m_Size As TmdbImageSize

        <XmlAttribute("url")> _
        Public Property Url() As String
            Get
                Return m_Url
            End Get
            Set(value As String)
                m_Url = Value
            End Set
        End Property
        Private m_Url As String

        <XmlAttribute("id")> _
        Public Property Id() As String
            Get
                Return m_Id
            End Get
            Set(value As String)
                m_Id = Value
            End Set
        End Property
        Private m_Id As String

        <XmlAttribute("width")> _
        Public Property Width() As Integer
            Get
                Return m_Width
            End Get
            Set(value As Integer)
                m_Width = Value
            End Set
        End Property
        Private m_Width As Integer

        <XmlAttribute("height")> _
        Public Property Height() As Integer
            Get
                Return m_Height
            End Get
            Set(value As Integer)
                m_Height = Value
            End Set
        End Property
        Private m_Height As Integer

        Public Property Image() As Image
            Get
                Return m_Image
            End Get
            Set(value As Image)
                m_Image = Value
            End Set
        End Property
        Private m_Image As Image

        Public Sub LoadImage()
            If Me.Image Is Nothing Then
                Image = LoadImage(Url)
            End If
        End Sub

        Public Shared Function LoadImage(Url As String) As Image
            Try
                Dim webClient As New WebClient()

                Dim filePath As String = System.IO.Path.Combine(DataCaching.GetFolder(), TmdbAPI.Escape(Url))

                Dim data As Byte() = DataCaching.GetBytesFromFile(filePath)
                If data Is Nothing Then
                    data = webClient.DownloadData(Url)
                    DataCaching.SaveBytesToFile(filePath, data)
                End If

                Using stream As New MemoryStream(data)
                    Return Image.FromStream(stream)
                End Using
            Catch generatedExceptionName As Exception
                Return Nothing
            End Try
        End Function

#Region "IDisposable Members"

        Public Sub Dispose() Implements IDisposable.Dispose
            If Image IsNot Nothing Then
                Image.Dispose()
                Image = Nothing
            End If
        End Sub

#End Region
    End Class
End Namespace
