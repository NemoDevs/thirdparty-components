﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Xml.Serialization

Namespace TheMovieDB
    <XmlRoot("OpenSearchDescription")> _
    Public Class TmdbGetListResults
        <XmlArrayAttribute("genres")> _
        Public Property Genres() As TmdbGenre()
            Get
                Return m_Genres
            End Get
            Set(value As TmdbGenre())
                m_Genres = Value
            End Set
        End Property
        Private m_Genres As TmdbGenre()
    End Class
End Namespace
