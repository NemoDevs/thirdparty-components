﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Xml.Serialization
Imports System

Namespace TheMovieDB
    <XmlType("person")> _
    Public Class TmdbPerson
        <XmlElement("score")> _
        Public Property Score() As Decimal
            Get
                Return m_Score
            End Get
            Set(value As Decimal)
                m_Score = Value
            End Set
        End Property
        Private m_Score As Decimal

        <XmlElement("popularity")> _
        Public Property Popularity() As Decimal
            Get
                Return m_Popularity
            End Get
            Set(value As Decimal)
                m_Popularity = Value
            End Set
        End Property
        Private m_Popularity As Decimal

        <XmlElement("name")> _
        Public Property Name() As String
            Get
                Return m_Name
            End Get
            Set(value As String)
                m_Name = Value
            End Set
        End Property
        Private m_Name As String

        <XmlElementAttribute("also_known_as")> _
        Public Property AlsoKnownAs() As TmdbAlsoKnownAs
            Get
                Return m_AlsoKnownAs
            End Get
            Set(value As TmdbAlsoKnownAs)
                m_AlsoKnownAs = Value
            End Set
        End Property
        Private m_AlsoKnownAs As TmdbAlsoKnownAs

        <XmlElement("url")> _
        Public Property Url() As String
            Get
                Return m_Url
            End Get
            Set(value As String)
                m_Url = Value
            End Set
        End Property
        Private m_Url As String

        <XmlElement("id")> _
        Public Property Id() As Integer
            Get
                Return m_Id
            End Get
            Set(value As Integer)
                m_Id = Value
            End Set
        End Property
        Private m_Id As Integer

        <XmlElement("biography")> _
        Public Property Biography() As String
            Get
                Return m_Biography
            End Get
            Set(value As String)
                m_Biography = Value
            End Set
        End Property
        Private m_Biography As String

        <XmlElement("known_movies")> _
        Public Property KnownMovies() As Integer
            Get
                Return m_KnownMovies
            End Get
            Set(value As Integer)
                m_KnownMovies = Value
            End Set
        End Property
        Private m_KnownMovies As Integer

        <XmlElement("birthday")> _
        Public Property BirthdayString() As String
            Get
                Return m_BirthdayString
            End Get
            Set(value As String)
                m_BirthdayString = Value
            End Set
        End Property
        Private m_BirthdayString As String

        Public ReadOnly Property Birthday() As System.Nullable(Of DateTime)
            Get
                Dim d As DateTime
                If String.IsNullOrEmpty(BirthdayString) OrElse Not DateTime.TryParse(BirthdayString, d) Then
                    Return Nothing
                Else
                    Return d
                End If
            End Get
        End Property

        <XmlElement("birthplace")> _
        Public Property Birthplace() As String
            Get
                Return m_Birthplace
            End Get
            Set(value As String)
                m_Birthplace = Value
            End Set
        End Property
        Private m_Birthplace As String

        <XmlArrayAttribute("images")> _
        Public Property Images() As TmdbImage()
            Get
                Return m_Images
            End Get
            Set(value As TmdbImage())
                m_Images = Value
            End Set
        End Property
        Private m_Images As TmdbImage()

        <XmlArrayAttribute("filmography")> _
        Public Property Filmography() As TmdbPersonFilm()
            Get
                Return m_Filmography
            End Get
            Set(value As TmdbPersonFilm())
                m_Filmography = Value
            End Set
        End Property
        Private m_Filmography As TmdbPersonFilm()
    End Class
End Namespace
