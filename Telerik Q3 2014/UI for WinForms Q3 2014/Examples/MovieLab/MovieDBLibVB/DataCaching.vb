﻿Imports System.Collections.Generic
Imports System.Text
Imports System.IO
Imports System.Reflection
Imports System

Namespace MovieLab
    Public Class DataCaching
        Public Shared Function GetFileListName() As String
            Dim listName As String = System.IO.Path.Combine(GetFolder(), "List.cache")
            Return listName
        End Function

        Public Shared Function GetFileMovieName(movie As String) As String
            Dim listName As String = System.IO.Path.Combine(GetFolder(), "Movie " & movie & ".cache")
            Return listName
        End Function

        Public Shared Function GetFileMovieBrowseName(args As String) As String
            Dim listName As String = System.IO.Path.Combine(GetFolder(), "Browse " & args & ".cache")
            Return listName
        End Function

        Public Shared Function GetFileMovieIMDBidName(id As String) As String
            Dim listName As String = System.IO.Path.Combine(GetFolder(), "IMDBID " & id & ".cache")
            Return listName
        End Function

        Public Shared Function GetFileMovieIDName(id As String) As String
            Dim listName As String = System.IO.Path.Combine(GetFolder(), "ID " & id & ".cache")
            Return listName
        End Function

        Public Shared Function GetFileImageName(url As String) As String
            Dim listName As String = System.IO.Path.Combine(GetFolder(), "image " & url & ".cache")
            Return listName
        End Function

        Public Shared Function GetFilePersonName(person As String) As String
            Dim listName As String = System.IO.Path.Combine(GetFolder(), "person " & person & ".cache")
            Return listName
        End Function

        Public Shared Function GetFilePersonIdName(id As String) As String
            Dim listName As String = System.IO.Path.Combine(GetFolder(), "personid " & id & ".cache")
            Return listName
        End Function



        Public Shared Function GetFolder() As String
            Dim currentAssemblyDirectoryName As String = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            currentAssemblyDirectoryName = Path.Combine(currentAssemblyDirectoryName, "cache")
            Return currentAssemblyDirectoryName
        End Function

        Public Shared Function GetBytesFromFile(fullFilePath As String) As Byte()
            If Not System.IO.File.Exists(fullFilePath) Then
                Return Nothing
            End If

            Dim fileStream As FileStream = File.OpenRead(fullFilePath)
            Try
                Dim bytes As Byte() = New Byte(fileStream.Length - 1) {}
                fileStream.Read(bytes, 0, Convert.ToInt32(fileStream.Length))
                fileStream.Close()
                Return bytes
            Catch
                Return Nothing
            Finally
                fileStream.Close()
            End Try
        End Function

        Public Shared Function SaveBytesToFile(fullFilePath As String, bytes As Byte()) As Boolean
            Try
                Dim fileStream As New System.IO.FileStream(fullFilePath, System.IO.FileMode.Create, System.IO.FileAccess.Write)
                fileStream.Write(bytes, 0, bytes.Length)
                fileStream.Close()
                Return True
            Catch ex As Exception
                Console.WriteLine("Exception caught in process: {0}", ex.ToString())
            End Try

            Return False
        End Function
    End Class
End Namespace
