﻿Imports System.Collections.Generic
Imports System.Text
Imports System

Namespace TheMovieDB
    Public Class ImdbMovieSearchCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        Private m_movies As TmdbMovie()
        Public ReadOnly Property Movies() As TmdbMovie()
            Get
                'RaiseExceptionIfNecessary();
                Return m_movies
            End Get
        End Property

        Public Sub New(movies As TmdbMovie(), e As Exception, canceled As Boolean, state As Object)
            MyBase.New(e, canceled, state)
            Me.m_movies = movies
        End Sub
    End Class
End Namespace
