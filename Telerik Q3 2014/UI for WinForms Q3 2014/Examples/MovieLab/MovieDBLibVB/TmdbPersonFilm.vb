﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Xml.Serialization

Namespace TheMovieDB
    <XmlType("movie")> _
    Public Class TmdbPersonFilm
        <XmlAttribute("name")> _
        Public Property Name() As String
            Get
                Return m_Name
            End Get
            Set(value As String)
                m_Name = Value
            End Set
        End Property
        Private m_Name As String

        <XmlAttribute("job")> _
        Public Property Job() As String
            Get
                Return m_Job
            End Get
            Set(value As String)
                m_Job = Value
            End Set
        End Property
        Private m_Job As String

        <XmlAttribute("department")> _
        Public Property Department() As String
            Get
                Return m_Department
            End Get
            Set(value As String)
                m_Department = Value
            End Set
        End Property
        Private m_Department As String

        <XmlAttribute("url")> _
        Public Property Url() As String
            Get
                Return m_Url
            End Get
            Set(value As String)
                m_Url = Value
            End Set
        End Property
        Private m_Url As String

        <XmlAttribute("character")> _
        Public Property Character() As String
            Get
                Return m_Character
            End Get
            Set(value As String)
                m_Character = Value
            End Set
        End Property
        Private m_Character As String

        <XmlAttribute("id")> _
        Public Property Id() As Integer
            Get
                Return m_Id
            End Get
            Set(value As Integer)
                m_Id = Value
            End Set
        End Property
        Private m_Id As Integer
    End Class
End Namespace
