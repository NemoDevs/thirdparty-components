﻿Imports System.Collections.Generic
Imports System.Text

Namespace TheMovieDB
    Public Enum TmdbImageSize
        original = 0
        cover = 1
        mid = 2
        thumb = 3
        poster = 4
        profile = 5
        w342 = 6
        w154 = 7
        w1280 = 8
    End Enum
End Namespace
