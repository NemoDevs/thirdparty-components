﻿Imports System.Collections.Generic
Imports System.Text
Imports System

Namespace TheMovieDB
    Public Class TmdbMovieBrowseRequest
        Public Property order_by() As TmdbOrderBy
            Get
                Return m_order_by
            End Get
            Set(value As TmdbOrderBy)
                m_order_by = Value
            End Set
        End Property
        Private m_order_by As TmdbOrderBy
        Public Property order() As TmdbOrderType
            Get
                Return m_order
            End Get
            Set(value As TmdbOrderType)
                m_order = Value
            End Set
        End Property
        Private m_order As TmdbOrderType
        Public Property per_page() As Integer
            Get
                Return m_per_page
            End Get
            Set(value As Integer)
                m_per_page = Value
            End Set
        End Property
        Private m_per_page As Integer
        Public Property page() As Integer
            Get
                Return m_page
            End Get
            Set(value As Integer)
                m_page = Value
            End Set
        End Property
        Private m_page As Integer
        Public Property min_votes() As Integer
            Get
                Return m_min_votes
            End Get
            Set(value As Integer)
                m_min_votes = Value
            End Set
        End Property
        Private m_min_votes As Integer
        Public Property rating_min() As Single
            Get
                Return m_rating_min
            End Get
            Set(value As Single)
                m_rating_min = Value
            End Set
        End Property
        Private m_rating_min As Single
        Public Property rating_max() As Single
            Get
                Return m_rating_max
            End Get
            Set(value As Single)
                m_rating_max = Value
            End Set
        End Property
        Private m_rating_max As Single
        Public Property genres() As String
            Get
                Return m_genres
            End Get
            Set(value As String)
                m_genres = Value
            End Set
        End Property
        Private m_genres As String
        Public Property genres_selector() As String
            Get
                Return m_genres_selector
            End Get
            Set(value As String)
                m_genres_selector = Value
            End Set
        End Property
        Private m_genres_selector As String
        Public Property release_min() As DateTime
            Get
                Return m_release_min
            End Get
            Set(value As DateTime)
                m_release_min = Value
            End Set
        End Property
        Private m_release_min As DateTime
        Public Property release_max() As DateTime
            Get
                Return m_release_max
            End Get
            Set(value As DateTime)
                m_release_max = Value
            End Set
        End Property
        Private m_release_max As DateTime
        Public Property year() As Integer
            Get
                Return m_year
            End Get
            Set(value As Integer)
                m_year = Value
            End Set
        End Property
        Private m_year As Integer
        Public Property query() As String
            Get
                Return m_query
            End Get
            Set(value As String)
                m_query = Value
            End Set
        End Property
        Private m_query As String

        Public Sub New()
            Me.order_by = TmdbOrderBy.rating
            Me.order = TmdbOrderType.desc
        End Sub

        Public Sub New(order_by As TmdbOrderBy, order As TmdbOrderType)
            Me.order_by = order_by
            Me.order = order
        End Sub

        Public Sub New(rating_min As Single, rating_max As Single)
            Me.New()
            Me.rating_min = rating_min
            Me.rating_max = rating_max
        End Sub

        Public Sub New(release_min As DateTime, release_max As DateTime)
            Me.New()
            Me.release_min = release_min
            Me.release_max = release_max
        End Sub

        Public Sub New(genres As TmdbGenre(), genreNames As String())
            Me.New()
            Me.genres = TmdbGenre.Build(genres, genreNames)
            Me.genres_selector = "or"
        End Sub

        Public Sub New(genres As String)
            Me.New()
            Me.genres = genres
            Me.genres_selector = "or"
        End Sub
    End Class
End Namespace
