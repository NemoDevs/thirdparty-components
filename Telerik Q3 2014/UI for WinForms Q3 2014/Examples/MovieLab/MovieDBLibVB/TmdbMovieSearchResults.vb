﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Xml.Serialization

Namespace TheMovieDB
    <XmlRoot("OpenSearchDescription")> _
    Public Class TmdbMovieSearchResults
        <XmlArrayAttribute("movies")> _
        Public Property Movies() As TmdbMovie()
            Get
                Return m_Movies
            End Get
            Set(value As TmdbMovie())
                m_Movies = Value
            End Set
        End Property
        Private m_Movies As TmdbMovie()
    End Class
End Namespace
