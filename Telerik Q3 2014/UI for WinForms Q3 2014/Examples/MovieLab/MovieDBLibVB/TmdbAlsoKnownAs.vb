﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Xml.Serialization

Namespace TheMovieDB
    <XmlType("also_known_as")> _
    Public Class TmdbAlsoKnownAs
        <XmlElementAttribute("name")> _
        Public Property Names() As String()
            Get
                Return m_Names
            End Get
            Set(value As String())
                m_Names = Value
            End Set
        End Property
        Private m_Names As String()
    End Class
End Namespace
