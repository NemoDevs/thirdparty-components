﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using MovieLab.Properties;
using System.Windows.Forms;

namespace MovieLab.Lab
{
    public class HeaderElement: StackLayoutElement
    {
        MovieButtonElement buttonCategory;
        MovieButtonElement buttonTop;
        MovieButtonElement buttonNew;
        SearchTextBox textBox;
        LightVisualElement logo;

        public MovieButtonElement ButtonTop { get { return buttonTop; } }
        public MovieButtonElement ButtonNew { get { return buttonNew; } }
        public LightVisualElement Logo { get { return logo; } }
        public SearchTextBox TextBox { get { return textBox; } }
        public MovieButtonElement ButtonCategory { get { return buttonCategory; } }

        protected override void InitializeFields()
        {
            base.InitializeFields();
            this.StretchHorizontally = true;
            this.StretchVertically = false;
            this.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.ElementSpacing = 0;
            this.FitInAvailableSize = true;
            this.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            buttonCategory = new MovieButtonElement();
            buttonCategory.Text = "";
            buttonCategory.StretchHorizontally = false;
            buttonCategory.StretchVertically = false;
            buttonCategory.Normal = Resources.buttonAllNormal;
            buttonCategory.Selected = Resources.buttonAllDown;
            buttonCategory.Hovered = Resources.buttonAllHover;
            buttonCategory.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            buttonCategory.MouseDown += new System.Windows.Forms.MouseEventHandler(buttonCategory_MouseDown);
            this.Children.Add(buttonCategory);

            buttonTop = new MovieButtonElement();
            buttonTop.Normal = Resources.buttonTopNormal;
            buttonTop.Hovered = Resources.buttonTopHover;
            buttonTop.Down = Resources.buttonTopDown;
            this.Children.Add(buttonTop);

            buttonNew = new MovieButtonElement();
            buttonNew.Normal = Resources.buttonNewNormal;
            buttonNew.Hovered = Resources.buttonNewHover;
            buttonNew.Down = Resources.buttonNewDown;
            buttonNew.Padding = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.Children.Add(buttonNew);

            textBox = new SearchTextBox();
            textBox.StretchHorizontally = true;
            textBox.StretchVertically = false;
            textBox.Margin = new System.Windows.Forms.Padding(90, 0, 0, 0);
            textBox.TextBoxItem.NullText = "search";
            textBox.TextBoxItem.Font = MovieControl.SegoeUI10Italic;
            this.Children.Add(textBox);

            logo = new LightVisualElement();
            logo.StretchHorizontally = false;
            logo.StretchVertically = false;
            logo.Margin = new System.Windows.Forms.Padding(0, 0, -1, 0);
            logo.Image = Resources.logo;
            this.Children.Add(logo);
        }

        void buttonCategory_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            buttonCategory.State = MovieButtonElement.ButtonStates.Selected;
            RadDropDownMenu menu = new RadDropDownMenu();
            GenresMenuItem genresMenuItem = new GenresMenuItem();
            genresMenuItem.ButtonGo.Click += new EventHandler(ButtonGo_Click);
            genresMenuItem.ButtonGo.Tag = genresMenuItem;
            menu.Items.Add(genresMenuItem);
            RadDropDownMenuElement popupElement = (RadDropDownMenuElement)menu.PopupElement;
            popupElement.Border.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            menu.Show(buttonCategory, buttonCategory.ControlBoundingRectangle.Left + 20, buttonCategory.Size.Height - 1);
            menu.MaximumSize = new System.Drawing.Size(288, 0);
            menu.DropDownClosed += new RadPopupClosedEventHandler(menu_DropDownClosed);
        }

        void ButtonGo_Click(object sender, EventArgs e)
        {
            buttonCategory.State = MovieButtonElement.ButtonStates.Normal;
            Application.DoEvents();

            MovieElement movieElement = FindAncestor<MovieElement>();
            MovieButtonElement button = (MovieButtonElement)sender;
            GenresMenuItem genresMenuItem = (GenresMenuItem)button.Tag;

            movieElement.NavigationService.Navigate(movieElement.MoviesPage);

            StringBuilder categoriesText = new StringBuilder();
            StringBuilder builder = new StringBuilder();

            categoriesText.Append("<html><font=Segoe UI><size=10>");

            for (int i = 0; i < genresMenuItem.CheckedItems.Count; i++)
            {
                GenreInfo info = genresMenuItem.CheckedItems[i];
                builder.Append(info.id);
                if (i < genresMenuItem.CheckedItems.Count-1)
                {
                    builder.Append(",");
                }
                categoriesText.Append("<color=#CCFF00>-<color=white><i>");
                categoriesText.Append(info.Name);            
            }
            movieElement.CategoriesView.Text = categoriesText.ToString();
            movieElement.MoviesPage.LoadCategories(builder.ToString());                        
        }

        void menu_DropDownClosed(object sender, RadPopupClosedEventArgs args)
        {
            RadDropDownMenu menu = (RadDropDownMenu)sender;
            menu.DropDownClosed -= new RadPopupClosedEventHandler(menu_DropDownClosed);
            buttonCategory.State = MovieButtonElement.ButtonStates.Normal;
        }
    }
}
