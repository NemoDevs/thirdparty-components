﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;

namespace MovieLab.Lab
{
    public class MovieButtonElement: LightVisualElement
    {
        public enum ButtonStates { Normal, Hovered, Down, Selected };

        ButtonStates state;

        public ButtonStates State
        {
            get { return this.state; }
            set
            {
                if (state != value)
                {
                    state = value;
                    OnStateChanged();
                }
            }
        }
        
        public Bitmap Normal { get; set; }
        public Bitmap Hovered { get; set; }
        public Bitmap Down { get; set; }
        public Bitmap Selected { get; set; }

        public MovieButtonElement()
        {
        }

        protected override void InitializeFields()
        {
            base.InitializeFields();

            this.StretchHorizontally = false;
            this.StretchVertically = false;
            this.DrawBorder = false;
            this.DrawFill = false;
        }

        protected override void OnLoaded()
        {
            base.OnLoaded();
            OnStateChanged();    
        }

        protected virtual void OnStateChanged()
        {
            switch (state)
            {
                case ButtonStates.Normal: if (this.Normal != null) this.Image = this.Normal; break;
                case ButtonStates.Hovered: if (this.Hovered != null) this.Image = this.Hovered; break;
                case ButtonStates.Down: if (this.Down != null) this.Image = this.Down; break;
                case ButtonStates.Selected: if (this.Selected != null) this.Image = this.Selected; break;
            }
        }

        protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (State != ButtonStates.Selected)
            {
                State = ButtonStates.Down;
            }
            this.Capture = true;
        }

        protected override void OnMouseUp(System.Windows.Forms.MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (State == ButtonStates.Down)
            {
                State = ButtonStates.Normal;
            }
            this.Capture = false;
        }

        protected override void OnPropertyChanged(Telerik.WinControls.RadPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == RadItem.IsMouseOverProperty)
            {
                if ((bool)e.NewValue)
                {
                    if (Control.MouseButtons == MouseButtons.Left)
                    {
                        State = ButtonStates.Down;
                    }
                    else if (State != ButtonStates.Down && State != ButtonStates.Selected)
                    {
                        State = ButtonStates.Hovered;
                    }
                }
                else
                {
                    if (State == ButtonStates.Hovered || State == ButtonStates.Down)
                    {
                        State = ButtonStates.Normal;
                    }
                }
            }
        }
    }
}
