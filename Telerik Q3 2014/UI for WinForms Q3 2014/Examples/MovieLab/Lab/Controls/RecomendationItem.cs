﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using MovieLab.Properties;
using TheMovieDB;

namespace MovieLab.Lab
{
    public class RecomendationItem: LightVisualElement
    {
        BackdropItem poster;
        LightVisualElement title;
        LightVisualElement year;
        RatingElement rating;
        TmdbMovie movie;

        public BackdropItem Poster { get { return poster; } }
        public LightVisualElement Title { get { return title; } }
        public LightVisualElement Year { get { return year; } }
        public RatingElement Rating { get { return rating; } }
        public TmdbMovie Movie { get { return movie; } }

        protected override void InitializeFields()
        {
            base.InitializeFields();
            this.StretchHorizontally = false;
            this.StretchVertically = false;
            this.MinSize = new Size(210, 50);
            this.MaxSize = new Size(210, 50);
            this.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.DrawBorder = true;
            this.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            this.BorderTopWidth = 0;
            this.BorderRightWidth = 1;
            this.BorderBottomWidth = 0;
            this.BorderLeftWidth = 0;
            this.BorderRightColor = Color.FromArgb(53, 53, 64);
            this.BorderRightShadowColor = Color.Black;
            this.ForeColor = Color.FromArgb(204, 255, 0);
            this.ClipDrawing = true;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            this.Image = Resources.waiting_bar;

            poster = new BackdropItem(null, 50, 50);
            poster.NotifyParentOnMouseInput = true;
            this.Children.Add(poster);
            
            title = new LightVisualElement();
            title.ForeColor = Color.White;
            title.TextAlignment = ContentAlignment.MiddleLeft;
            title.NotifyParentOnMouseInput = true;
            title.AutoEllipsis = true;
            title.ClipDrawing = true;
            this.Children.Add(title);

            year = new LightVisualElement();
            year.ForeColor = Color.FromArgb(107, 107, 121);
            year.TextAlignment = ContentAlignment.MiddleLeft;
            year.NotifyParentOnMouseInput = true;
            this.Children.Add(year);

            rating = new RatingElement();
            rating.NotifyParentOnMouseInput = true;
            rating.Rating = 0;
            this.Children.Add(rating);
        }

        public void Load(TmdbMovie movie)
        {
            this.movie = movie;
        }

        protected override void OnLoaded()
        {
            base.OnLoaded();
            if (this.Parent != null && this.Parent.Children.IndexOf(this) == 0)
            {
                this.BorderLeftWidth = 1;
                this.BorderLeftColor = Color.FromArgb(53, 53, 64);
                this.BorderLeftShadowColor = Color.Black;
            }
        }

        protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (movie != null)
            {
                MovieControl control = (MovieControl)ElementTree.Control;
                control.Element.NavigationService.Navigate(control.Element.DetailsPage);
                control.Element.DetailsPage.LoadMovie(movie);
            }
        }

        protected override SizeF ArrangeOverride(SizeF finalSize)
        {
            RectangleF clientRect = GetClientRectangle(finalSize);
            float x = clientRect.X;
            float y = clientRect.Y;

            poster.Arrange(new RectangleF(x, y, poster.DesiredSize.Width, poster.DesiredSize.Height));
            x += poster.DesiredSize.Width + 4;

            title.Arrange(new RectangleF(x, y, title.DesiredSize.Width, title.DesiredSize.Height));
            y += title.DesiredSize.Height;

            year.Arrange(new RectangleF(x, y, title.DesiredSize.Width, title.DesiredSize.Height));
            y += year.DesiredSize.Height;
            
            rating.Arrange(new RectangleF(x, y, title.DesiredSize.Width, title.DesiredSize.Height));

            return finalSize;
        }        
    }
}
