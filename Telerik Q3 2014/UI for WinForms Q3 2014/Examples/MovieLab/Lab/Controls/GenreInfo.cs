﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieLab.Lab
{
    public class GenreInfo
    {
        public int id;
        public string Name;

        public GenreInfo(int id, string name) { this.id = id; this.Name = name; }

        static GenreInfo[] genres;

        public static GenreInfo[] Genres
        {
            get
            {
                if (genres == null)
                {
                    genres = new GenreInfo[] {
                        new GenreInfo(28, "Action"),
                        new GenreInfo(12, "Adventure"),
                        new GenreInfo(16, "Animation"),
                        new GenreInfo(35, "Comedy"),
                        new GenreInfo(80, "Crime"),
                        new GenreInfo(105, "Disaster"),
                        new GenreInfo(18, "Drama"),
                        new GenreInfo(99, "Documentary"),
                        new GenreInfo(14, "Fantasy"),
                        new GenreInfo(36, "History"),
                        new GenreInfo(27, "Horror"),
                        new GenreInfo(10402, "Music"),
                        new GenreInfo(22, "Musical"),
                        new GenreInfo(9648, "Mystery"),
                        new GenreInfo(878, "Science Fiction"),
                        new GenreInfo(13, "Sport"),
                        new GenreInfo(10748, "Suspense"),
                        new GenreInfo(53, "Thriller"),
                        new GenreInfo(37, "Western")
                    };
                }
                return genres;
            }
        }
    }
}
