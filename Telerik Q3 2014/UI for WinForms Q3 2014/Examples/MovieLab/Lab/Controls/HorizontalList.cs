﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using Telerik.WinControls;

namespace MovieLab.Lab
{
    public class HorizontalList: LightVisualElement
    {
        #region Fields

        public Type TargetType { get; set; }
        RadListViewElement innerList;
        RadScrollBarElement scrollbar;

        #endregion

        #region Properties

        public RadScrollBarElement Scrollbar
        {
            get { return scrollbar; }
        }

        public RadListViewElement ListView
        {
            get { return innerList; }
        }

        #endregion

        #region Initialization

        public HorizontalList()
        {
           
        }
        
        protected override void InitializeFields()
        {
            base.InitializeFields();
            this.ClipDrawing = true;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            StackLayoutElement stack = new StackLayoutElement();
            stack.Orientation = System.Windows.Forms.Orientation.Vertical;

            scrollbar = new RadScrollBarElement();
            scrollbar.ScrollType = ScrollType.Horizontal;
            scrollbar.StretchHorizontally = true;
            scrollbar.StretchVertically = false;           

            stack.DrawFill = true;
            stack.BackColor = Color.FromArgb(47, 47, 61);
            stack.GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            stack.NotifyParentOnMouseInput = true;
            stack.ShouldHandleMouseInput = true;
            stack.Children.Add(scrollbar);

            innerList = new RadListViewElement();
            
            innerList.ViewType = ListViewType.IconsView;
            innerList.ItemSize = MovieThumbElement.ThumbSize.ToSize();
            innerList.ViewElement.Orientation = System.Windows.Forms.Orientation.Horizontal;
            innerList.ViewElement.Scroller.Scrollbar = this.scrollbar;
            innerList.ViewElement.HScrollBar.Visibility = ElementVisibility.Collapsed;
            innerList.ViewElement.VScrollBar.Visibility = ElementVisibility.Collapsed;
            innerList.ViewElement.Image = MovieLab.Properties.Resources.waiting_bar;
            innerList.ViewElement.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            innerList.ViewElement.ImageAlignment = ContentAlignment.TopCenter;
            innerList.MaxSize = new System.Drawing.Size(0, 280);
            innerList.Items.CollectionChanged += new Telerik.WinControls.Data.NotifyCollectionChangedEventHandler(Items_CollectionChanged);

           // innerList.Items.CollectionChanged += new Telerik.WinControls.Data.NotifyCollectionChangedEventHandler(Items_CollectionChanged);
            innerList.VisualItemCreating += new ListViewVisualItemCreatingEventHandler(internalStack_VisualItemCreating);

            innerList.NotifyParentOnMouseInput = true;
            stack.Children.Add(innerList);
            
            this.CanFocus = true;
            this.Children.Add(stack);
           
        }

        void Items_CollectionChanged(object sender, Telerik.WinControls.Data.NotifyCollectionChangedEventArgs e)
        {
            if (innerList.Items.Count > 0)
            {
                innerList.ViewElement.Image = null;
            }
            else
            {
                innerList.ViewElement.Image = MovieLab.Properties.Resources.waiting_bar;
            }
        }

        void internalStack_VisualItemCreating(object sender, ListViewVisualItemCreatingEventArgs e)
        {
            if(this.TargetType == typeof(PersonInfo))
            {
                e.VisualItem = new CastItem();
            }
            else
            {
                e.VisualItem = new MovieThumbElement();
            }
        }

        protected override bool ShouldUsePaintBuffer()
        {
            return false;
        }
        
        #endregion

        #region Layout

        protected override SizeF MeasureOverride(SizeF availableSize)
        {
            this.scrollbar.Measure(availableSize);
            return base.MeasureOverride(availableSize);
        }
        
        #endregion

        #region Event handlers

        protected override void OnLoaded()
        {
            base.OnLoaded();

            scrollbar.FirstButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            scrollbar.SecondButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            scrollbar.ThumbElement.ThumbFill.GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            scrollbar.ThumbElement.ThumbFill.BackColor = Color.White;
            scrollbar.ThumbElement.ThumbBorder.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            scrollbar.MinSize = new System.Drawing.Size(0, 4);
            scrollbar.MinSize = new System.Drawing.Size(0, 4);
            scrollbar.ThumbElement.MaxSize = new System.Drawing.Size(0, 3);
            scrollbar.ThumbElement.MaxSize = new System.Drawing.Size(0, 3);
            scrollbar.ThumbElement.ThumbFill.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            scrollbar.ThumbElement.ThumbFill.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            scrollbar.FillElement.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            scrollbar.BorderElement.BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            scrollbar.BorderElement.LeftWidth = 0;
            scrollbar.BorderElement.RightWidth = 0;
            scrollbar.BorderElement.TopWidth = 1;
            scrollbar.BorderElement.BottomWidth = 1;
            scrollbar.BorderElement.TopColor = Color.FromArgb(47, 47, 61);
            scrollbar.BorderElement.BottomColor = Color.FromArgb(76, 76, 100);
            scrollbar.ThumbElement.GripImage = null;
        }

        #endregion
    }
}
