Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports System.Windows.Forms

Namespace MovieLabVB.Lab
    Public Class HBorderElement
        Inherits LightVisualElement
        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()

            Me.StretchHorizontally = True
            Me.StretchVertically = False
            Me.MinSize = New System.Drawing.Size(0, 2)
            Me.MaxSize = New System.Drawing.Size(0, 2)
            Me.DrawBorder = True
            Me.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders
            Me.BorderLeftWidth = 0
            Me.BorderRightWidth = 0
            Me.BorderTopWidth = 0
            Me.BorderBottomWidth = 1
            Me.BorderBottomColor = Color.FromArgb(0, 0, 0)
            Me.BorderBottomShadowColor = Color.FromArgb(41, 41, 54)
        End Sub
    End Class
End Namespace
