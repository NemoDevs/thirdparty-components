Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports TheMovieDB
Imports Telerik.WinControls

Namespace MovieLabVB.Lab
    Public Class BackdropsView
        Inherits LightVisualElement
        Private title_Renamed As BackdropsTitleElement
        Private backdrops_Renamed As StackLayoutElement
        Private movie_Renamed As TmdbMovie

        Public ReadOnly Property Title() As BackdropsTitleElement
            Get
                Return title_Renamed
            End Get
        End Property
        Public ReadOnly Property Backdrops() As StackLayoutElement
            Get
                Return backdrops_Renamed
            End Get
        End Property
        Public ReadOnly Property Movie() As TmdbMovie
            Get
                Return movie_Renamed
            End Get
        End Property

        Protected Overrides Sub DisposeManagedResources()
            MyBase.DisposeManagedResources()
            movie_Renamed = Nothing
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()
            Me.StretchHorizontally = True
            Me.StretchVertically = False
            Me.ClipDrawing = True
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            title_Renamed = New BackdropsTitleElement()
            Me.Children.Add(title_Renamed)
            Me.NotifyParentOnMouseInput = True
            backdrops_Renamed = New StackLayoutElement()
            backdrops_Renamed.StretchHorizontally = True
            backdrops_Renamed.StretchVertically = False
            backdrops_Renamed.Orientation = System.Windows.Forms.Orientation.Horizontal
            backdrops_Renamed.FitInAvailableSize = True

            Dim bkDropItem As New BackdropItem(Nothing, 300, 90)
            bkDropItem.zoomImage = True
            bkDropItem.shadowOnLeft = True
            backdrops_Renamed.Children.Add(bkDropItem)

            Dim gradientBorder As New GradientBorderElement(90, 1, True)
            gradientBorder.Margin = New System.Windows.Forms.Padding(0, 0, 0, -2)
            backdrops_Renamed.Children.Add(gradientBorder)

            Dim bkDropItem1 As New BackdropItem(Nothing, 300, 90)
            bkDropItem1.zoomImage = True
            backdrops_Renamed.Children.Add(bkDropItem1)

            Dim gradientBorder1 As New GradientBorderElement(90, 1, True)
            gradientBorder1.Margin = New System.Windows.Forms.Padding(0, 0, 0, -2)
            backdrops_Renamed.Children.Add(gradientBorder1)

            Dim bkDropItem2 As New BackdropItem(Nothing, 360, 90)
            bkDropItem2.zoomImage = True
            backdrops_Renamed.Children.Add(bkDropItem2)

            Me.Children.Add(backdrops_Renamed)
        End Sub

        Public Sub Load(ByVal movie_Renamed As TmdbMovie)
            Me.movie_Renamed = movie_Renamed
            Me.Title.Title.Text = movie_Renamed.Name
            Me.Title.Rating.Rating = CInt(Fix(movie_Renamed.Rating))
            If movie_Renamed.Released.HasValue Then
                Me.Title.Year.Text = movie_Renamed.Released.Value.Year.ToString()
            Else
                Me.Title.Year.Text = ""
            End If
        End Sub

        Protected Overrides Function ArrangeOverride(ByVal finalSize As SizeF) As SizeF
            MyBase.ArrangeOverride(finalSize)
            Dim clientRect As RectangleF = GetClientRectangle(finalSize)
            backdrops_Renamed.Arrange(New RectangleF(clientRect.Right - backdrops_Renamed.DesiredSize.Width, clientRect.Y, backdrops_Renamed.DesiredSize.Width, clientRect.Height))
            Return finalSize
        End Function

        Protected Overrides Sub OnBubbleEvent(ByVal sender As Telerik.WinControls.RadElement, ByVal args As RoutedEventArgs)
            MyBase.OnBubbleEvent(sender, args)
            If args.RoutedEvent Is RadItem.MouseDownEvent Then
                If Not movie_Renamed Is Nothing Then
                    Dim element As MovieElement = FindAncestor(Of MovieElement)()
                    element.NavigationService.Navigate(element.DetailsPage)
                    element.DetailsPage.LoadMovie(movie_Renamed)
                End If
            End If
        End Sub
    End Class
End Namespace
