Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports System.Windows.Forms

Namespace MovieLabVB.Lab
    Public Class SizingGripElement
        Inherits LightVisualElement
        Private downPoint As Point
        Private downSize As Size
        Private mouseIsDown As Boolean

        Public Sub New()
            Me.Image = Resources.sizingGrip
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()

            Me.StretchHorizontally = True
            Me.StretchVertically = False
            Me.ImageAlignment = ContentAlignment.BottomRight
            Me.ImageLayout = System.Windows.Forms.ImageLayout.None
            Me.Alignment = ContentAlignment.MiddleRight
            Me.MinSize = New Size(20, 20)
            Me.ZIndex = 1000
        End Sub

        Protected Overrides Sub PaintImage(ByVal graphics As Telerik.WinControls.Paint.IGraphics)
            MyBase.PaintImage(graphics)
        End Sub

        Protected Overrides Sub OnMouseDown(ByVal e As System.Windows.Forms.MouseEventArgs)
            MyBase.OnMouseDown(e)
            If e.Button = System.Windows.Forms.MouseButtons.Left AndAlso e.Location.X > Me.Size.Width - 20 Then
                Dim form As Form = ElementTree.Control.FindForm()

                Me.Capture = True
                Me.downPoint = e.Location
                Me.downSize = form.Size
                Me.mouseIsDown = True
            End If
        End Sub

        Protected Overrides Sub OnMouseUp(ByVal e As System.Windows.Forms.MouseEventArgs)
            MyBase.OnMouseUp(e)
            Me.Capture = False
            Me.mouseIsDown = False
        End Sub

        Protected Overloads Overrides Sub OnMouseMove(ByVal e As System.Windows.Forms.MouseEventArgs)
            MyBase.OnMouseMove(e)
            If mouseIsDown Then
                Dim delta As Point = New Point(e.Location.X - downPoint.X, e.Location.Y - downPoint.Y)
                If Math.Abs(delta.X) > 2 OrElse Math.Abs(delta.Y) > 2 Then
                    Dim form As RadForm = CType(ElementTree.Control.FindForm(), RadForm)
                    form.Size = New Size(downSize.Width + delta.X, downSize.Height + delta.Y)
                    form.Refresh()
                    Application.DoEvents()
                End If
            End If
        End Sub
    End Class
End Namespace
