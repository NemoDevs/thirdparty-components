Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Windows.Forms
Imports System.Drawing
Imports Telerik.WinControls
Imports Telerik.WinControls.Layout
Imports TheMovieDB

Namespace MovieLabVB.Lab
    Public Class IconView
        Inherits RadListViewElement
        Private bigThumbs_Renamed As Boolean = False

        Public Sub New()
            Me.ItemSize = MovieThumbElement.ThumbSize.ToSize()
            Me.ItemSpacing = 0
            Me.StretchHorizontally = False
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()
            Me.ClipDrawing = True
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            Me.ViewType = ListViewType.IconsView
        End Sub

        Public Function FindMovieElement(ByVal movie As TmdbMovie) As MovieThumbElement
            For Each movieElement As MovieThumbElement In Me.ViewElement.ViewElement.Children
                If movieElement.Movie Is movie Then
                    Return movieElement
                End If
            Next movieElement
            Return Nothing
        End Function

        Protected Overrides Sub OnVisualItemCreating(ByVal args As ListViewVisualItemCreatingEventArgs)
            MyBase.OnVisualItemCreating(args)

            If Me.BigThumbs Then
                args.VisualItem = New MovieThumbElementBig()
            Else
                args.VisualItem = New MovieThumbElement()
            End If
        End Sub

        Public Property BigThumbs() As Boolean
            Get
                Return Me.bigThumbs_Renamed
            End Get
            Set(value As Boolean)
                If Me.bigThumbs_Renamed <> Value Then
                    Me.bigThumbs_Renamed = Value
                    If Value Then
                        Me.ItemSize = MovieThumbElementBig.DefaultThumbSize.ToSize()
                    Else
                        Me.ItemSize = MovieThumbElement.ThumbSize.ToSize()
                    End If
                End If
            End Set
        End Property
    End Class
End Namespace