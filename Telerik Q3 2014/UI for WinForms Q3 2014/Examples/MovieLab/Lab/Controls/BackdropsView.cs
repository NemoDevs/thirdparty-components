﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using MovieLab.Properties;
using TheMovieDB;
using Telerik.WinControls;

namespace MovieLab.Lab
{
    public class BackdropsView: LightVisualElement
    {
        BackdropsTitleElement title;
        StackLayoutElement backdrops;
        TmdbMovie movie;

        public BackdropsTitleElement Title { get { return title; } }
        public StackLayoutElement Backdrops { get { return backdrops; } }
        public TmdbMovie Movie { get { return movie; } }

        protected override void DisposeManagedResources()
        {
            base.DisposeManagedResources();
            movie = null;
        }

        protected override void InitializeFields()
        {
            base.InitializeFields();
            this.StretchHorizontally = true;
            this.StretchVertically = false;
            this.ClipDrawing = true;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            title = new BackdropsTitleElement();
            this.Children.Add(title);
            this.NotifyParentOnMouseInput = true;
            backdrops = new StackLayoutElement();
            backdrops.StretchHorizontally = true;
            backdrops.StretchVertically = false;
            backdrops.Orientation = System.Windows.Forms.Orientation.Horizontal;
            backdrops.FitInAvailableSize = true;            

            backdrops.Children.Add(new BackdropItem(null, 300, 90) { zoomImage = true, shadowOnLeft = true });
            backdrops.Children.Add(new GradientBorderElement(90, 1, true) { Margin = new System.Windows.Forms.Padding(0, 0, 0, -2) });
            backdrops.Children.Add(new BackdropItem(null, 300, 90) { zoomImage = true });
            backdrops.Children.Add(new GradientBorderElement(90, 1, true) { Margin = new System.Windows.Forms.Padding(0, 0, 0, -2) });
            backdrops.Children.Add(new BackdropItem(null, 360, 90) { zoomImage = true });
            
            this.Children.Add(backdrops);
        }

        public void Load(TmdbMovie movie)
        {
            this.movie = movie;
            this.Title.Title.Text = movie.Name;
            this.Title.Rating.Rating = (int)movie.Rating;
            this.Title.Year.Text = movie.Released.HasValue ? movie.Released.Value.Year.ToString() : "";
        }        

        protected override SizeF ArrangeOverride(SizeF finalSize)
        {
            base.ArrangeOverride(finalSize);
            RectangleF clientRect = GetClientRectangle(finalSize);
            backdrops.Arrange(new RectangleF(clientRect.Right - backdrops.DesiredSize.Width, clientRect.Y, backdrops.DesiredSize.Width, clientRect.Height));
            return finalSize;
        }

        protected override void OnBubbleEvent(Telerik.WinControls.RadElement sender, RoutedEventArgs args)
        {
            base.OnBubbleEvent(sender, args);
            if (args.RoutedEvent == RadItem.MouseDownEvent)
            {
                if (movie != null)
                {
                    MovieElement element = FindAncestor<MovieElement>();
                    element.NavigationService.Navigate(element.DetailsPage);
                    element.DetailsPage.LoadMovie(movie);
                }
            }
        }
    }
}
