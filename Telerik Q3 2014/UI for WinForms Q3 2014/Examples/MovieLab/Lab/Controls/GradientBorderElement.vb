Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing

Namespace MovieLabVB.Lab
    Public Class GradientBorderElement
        Inherits LightVisualElement
        Public Sub New(ByVal borderAngle As Single, ByVal width As Integer, ByVal solid As Boolean)
            If borderAngle = 0 Then
                Me.MinSize = New System.Drawing.Size(0, 1)
                Me.MaxSize = New System.Drawing.Size(0, 1)
                Me.StretchHorizontally = True
                Me.StretchVertically = False
            Else
                Me.MinSize = New System.Drawing.Size(1, 0)
                Me.MaxSize = New System.Drawing.Size(1, 0)
                Me.StretchHorizontally = False
                Me.StretchVertically = True
            End If
            Me.GradientAngle = borderAngle

            If solid Then
                Me.GradientStyle = Telerik.WinControls.GradientStyles.Solid
                Me.BackColor = Color.FromArgb(202, 253, 0)
            End If
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()

            Me.DrawFill = True
            Me.GradientStyle = Telerik.WinControls.GradientStyles.Linear
            Me.BackColor = Color.FromArgb(42, 42, 55)
            Me.BackColor2 = Color.FromArgb(202, 253, 0)
            Me.BackColor3 = Color.FromArgb(202, 253, 0)
            Me.BackColor4 = Color.FromArgb(42, 42, 55)
            Me.NumberOfColors = 4
            Me.GradientPercentage = 0.6F
            Me.GradientPercentage2 = 0.67F
        End Sub
    End Class
End Namespace
