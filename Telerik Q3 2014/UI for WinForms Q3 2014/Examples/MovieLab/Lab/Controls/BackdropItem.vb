Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports System.Drawing.Drawing2D

Namespace MovieLabVB.Lab
    Public Class BackdropItem
        Inherits LightVisualElement
        Public zoomImage As Boolean = False
        Public shadowOnLeft As Boolean = False

        Public Sub New(ByVal image As Image, ByVal width As Integer, ByVal height As Integer)
            Me.MinSize = New System.Drawing.Size(width, height)
            Me.MaxSize = New System.Drawing.Size(width, height)
            SetImage(image)
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()

            Me.StretchHorizontally = False
            Me.StretchVertically = False
            Me.Image = Resources.waiting_bar
            Me.NotifyParentOnMouseInput = True
        End Sub

        Public Sub SetImage(ByVal image As Image)
            If Not image Is Nothing Then
                If (Not zoomImage) AndAlso Me.MinSize <> Size.Empty Then
                    Me.Image = New Bitmap(image, Me.MinSize)
                Else
                    Me.Image = image
                    Me.ClipDrawing = True
                End If
            End If
        End Sub

        Protected Overrides Sub PaintImage(ByVal graphics As Telerik.WinControls.Paint.IGraphics)
            If zoomImage Then
                If Me.Image Is Nothing OrElse Me.Image.Width = 0 OrElse Me.Image.Height = 0 Then
                    Return
                End If

                Dim ratioX As Single = CSng(Size.Width) / Image.Width
                Dim ratioY As Single = CSng(Size.Height) / Image.Height

                Dim factor As Single = ratioX

                If factor <= 0.0F Then
                    Return
                End If

                Dim finalWidth As Integer = CInt(Fix(Math.Round(Image.Width * factor)))
                Dim finalHeight As Integer = CInt(Fix(Math.Round(Image.Height * factor)))

                Dim xStart As Integer = CInt(Fix((Size.Width - finalWidth) / 2.0F))
                Dim yStart As Integer = CInt(Fix((Size.Height - finalHeight) / 2.0F))

                graphics.DrawBitmap(Image, 0, -10, finalWidth, finalHeight)
            Else
                MyBase.PaintImage(graphics)
            End If

            If shadowOnLeft Then
                Dim g As Graphics = CType(graphics.UnderlayGraphics, Graphics)
                Using brush As LinearGradientBrush = New LinearGradientBrush(New Point(0, Size.Height), New Point(100, Size.Height), Color.FromArgb(255, 20, 20, 26), Color.FromArgb(0, 20, 20, 26))
                    g.FillRectangle(brush, 0, 0, 100, Me.Size.Height)
                End Using
            End If
        End Sub
    End Class
End Namespace
