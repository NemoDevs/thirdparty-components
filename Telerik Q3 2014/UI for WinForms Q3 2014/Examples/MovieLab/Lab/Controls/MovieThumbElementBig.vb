Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports TheMovieDB

Namespace MovieLabVB.Lab
    Public Class MovieThumbElementBig
        Inherits MovieThumbElement
        Private descriptionElement_Renamed As LightVisualElement

        Public ReadOnly Property DescriptionElement() As LightVisualElement
            Get
                Return descriptionElement_Renamed
            End Get
        End Property

        Public Shared ReadOnly Property DefaultThumbSize() As SizeF
            Get
                Return New SizeF(420, 300)
            End Get
        End Property

        Public Sub New()
            MyBase.New()
            Me.DrawBorder = True
            Me.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders
            Me.BorderTopWidth = 0
            Me.BorderLeftWidth = 0
            Me.BorderRightWidth = 0
            Me.BorderBottomWidth = 1
            Me.BorderBottomColor = Color.FromArgb(42, 42, 54)
            Me.BorderBottomShadowColor = Color.FromArgb(0, 0, 0)
            Me.NotifyParentOnMouseInput = True
        End Sub

        Protected Overrides Sub SynchronizeProperties()
            MyBase.SynchronizeProperties()
            Me.DescriptionElement.Text = (TryCast(Me.Data.Tag, TmdbMovie)).Overview
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            descriptionElement_Renamed = New LightVisualElement()
            descriptionElement_Renamed.ForeColor = Color.FromArgb(107, 107, 121)
            descriptionElement_Renamed.TextAlignment = ContentAlignment.TopLeft
            descriptionElement_Renamed.TextWrap = True
            descriptionElement_Renamed.AutoEllipsis = True
            Me.Children.Add(descriptionElement_Renamed)
        End Sub

        Protected Overrides Function MeasureOverride(ByVal availableSize As SizeF) As SizeF
            MyBase.MeasureOverride(availableSize)

            Dim desiredSize As SizeF = DefaultThumbSize
            ImageElement.Measure(New SizeF(desiredSize.Width, desiredSize.Height - 60))
            RatingElement.Measure(New SizeF(desiredSize.Width, 20))
            TextElement.Measure(New SizeF(desiredSize.Width, 20))
            TextElement2.Measure(New SizeF(desiredSize.Width, 20))
            ButtonAddToWishList.Measure(availableSize)
            DescriptionElement.Measure(New SizeF(desiredSize.Width - ImageElement.DesiredSize.Width - 20, desiredSize.Height - 60))
            Return desiredSize
        End Function

        Protected Overrides Function ArrangeOverride(ByVal finalSize As SizeF) As SizeF
            Dim clientRect As RectangleF = GetClientRectangle(finalSize)
            Dim x As Single = clientRect.X
            Dim y As Single = clientRect.Y
            ImageElement.Arrange(New RectangleF(x, y, ImageElement.DesiredSize.Width, ImageElement.DesiredSize.Height))
            x += ImageElement.DesiredSize.Width + 20
            TextElement.Arrange(New RectangleF(x, y, clientRect.Width - x, 20))
            y += 20
            TextElement2.Arrange(New RectangleF(x, y, clientRect.Width - x, 20))
            y += 20
            RatingElement.Arrange(New RectangleF(x, y, clientRect.Width - x, 20))
            y += 20
            descriptionElement_Renamed.Arrange(New RectangleF(x, y, clientRect.Width - x, clientRect.Height - y))
            ButtonAddToWishList.Arrange(New RectangleF(clientRect.X + ImageElement.DesiredSize.Width - 50, clientRect.Y + ImageElement.DesiredSize.Height + 15, ButtonAddToWishList.DesiredSize.Width, ButtonAddToWishList.DesiredSize.Height))
            Return finalSize
        End Function

        Public Overrides Function IsCompatible(ByVal data As ListViewDataItem, ByVal context As Object) As Boolean
            Return (TryCast(data.Owner, IconView)).BigThumbs
        End Function
    End Class
End Namespace
