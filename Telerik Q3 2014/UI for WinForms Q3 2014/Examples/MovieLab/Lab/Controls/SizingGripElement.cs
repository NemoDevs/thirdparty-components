﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using MovieLab.Properties;
using System.Drawing;
using System.Windows.Forms;

namespace MovieLab.Lab
{
    public class SizingGripElement: LightVisualElement
    {
        Point downPoint;
        Size downSize;
        bool mouseIsDown;

        public SizingGripElement()
        {
            this.Image = Resources.sizingGrip;
        }

        protected override void InitializeFields()
        {
            base.InitializeFields();
            
            this.StretchHorizontally = true;
            this.StretchVertically = false;
            this.ImageAlignment = ContentAlignment.BottomRight;
            this.ImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Alignment = ContentAlignment.MiddleRight;
            this.MinSize = new Size(20, 20);
            this.ZIndex = 1000;
        }

        protected override void PaintImage(Telerik.WinControls.Paint.IGraphics graphics)
        {
            base.PaintImage(graphics);
        }

        protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button == System.Windows.Forms.MouseButtons.Left && e.Location.X > this.Size.Width - 20)
            {
                Form form = ElementTree.Control.FindForm();

                this.Capture = true;
                this.downPoint = e.Location;
                this.downSize = form.Size;
                this.mouseIsDown = true;
            }
        }

        protected override void OnMouseUp(System.Windows.Forms.MouseEventArgs e)
        {
            base.OnMouseUp(e);
            this.Capture = false;
            this.mouseIsDown = false;
        }

        protected override void OnMouseMove(System.Windows.Forms.MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (mouseIsDown)
            {
                Point delta = new Point(e.Location.X - downPoint.X, e.Location.Y - downPoint.Y);
                if (Math.Abs(delta.X) > 2 || Math.Abs(delta.Y) > 2)
                {
                    RadForm form = (RadForm)ElementTree.Control.FindForm();
                    form.Size = new Size(downSize.Width + delta.X, downSize.Height + delta.Y);
                    form.Refresh();
                    Application.DoEvents();
                }
            }
        }
    }
}
