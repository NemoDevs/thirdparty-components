﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using MovieLab.Properties;
using Telerik.WinControls.Paint;

namespace MovieLab.Lab
{
    public class RatingElement: LightVisualElement
    {
        public static Bitmap starOn;
        public static Bitmap starOff;
        public static Bitmap bigStarOn;
        public static Bitmap bigStarOff;
        public static Bitmap bigStar2On;
        public static Bitmap bigStar2Off;

        private int rating;

        public Bitmap StarOn { get; set; }
        public Bitmap StarOff { get; set; }

        public int Stars { get; set; }
    
        public int Rating 
        {
            get { return rating; }
            set 
            {
                if (rating != value)
                {
                    rating = value;
                    Invalidate();
                }
            }
        }

        bool useBigStars;

        public bool UseBigStars 
        {
            get { return useBigStars; }
            set
            {
                if (useBigStars != value)
                {
                    useBigStars = value;
                    if (useBigStars)
                    {
                        StarOn = bigStarOn;
                        StarOff = bigStarOff;
                    }
                    else
                    {
                        StarOn = starOn;
                        StarOff = starOff;
                    }
                }
            }        
        }

        public int StarSpacing { get; set; }

        static RatingElement()
        {
            starOn = Resources.starOn;
            starOff = Resources.starOff;
            bigStarOff = Resources.bigStarOff;
            bigStarOn = Resources.bigStarOn;
            bigStar2Off = Resources.bigStar2Off;
            bigStar2On = Resources.bigStar2On;
        }

        public RatingElement()
        {
            Stars = 10;
            StarOn = starOn;
            StarOff = starOff;
        }

        protected override void InitializeFields()
        {
            base.InitializeFields();
            this.StretchHorizontally = true;
        }

        protected override SizeF MeasureOverride(SizeF availableSize)
        {
            SizeF desiredSize = SizeF.Empty;

            Bitmap star = StarOn;

            System.Windows.Forms.Padding border = GetBorderThickness(false);
            desiredSize.Height = star.Height + border.Vertical + Padding.Vertical;
            desiredSize.Width = (star.Width + StarSpacing)* Stars + border.Horizontal + Padding.Horizontal;

            desiredSize.Width += 40;

            return desiredSize;
        }

        protected override void PaintElement(IGraphics graphics, float angle, SizeF scale)
        {
            RectangleF clientRect = GetClientRectangle(this.Size);
            int x = (int)clientRect.X;
            int y = (int)clientRect.Y;
            int i = 0;
            for (; i < Rating; i++)
            {
                graphics.DrawBitmap(StarOn, x, y);
                x += starOn.Width + StarSpacing;
            }
            for (; i < Stars; i++)
            {
                graphics.DrawBitmap(StarOff, x, y);
                x += starOn.Width + StarSpacing;
            }

            Graphics g = (Graphics)graphics.UnderlayGraphics;
            using (SolidBrush brush = new SolidBrush(this.ForeColor))
            {
                if (UseBigStars)
                {
                    y += 3;
                }
                g.DrawString(string.Format("{0}/{1}", Rating, Stars), this.Font, brush, new PointF(x + 4, y - 2));
            }
        }
    }
}
