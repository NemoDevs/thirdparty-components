Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing

Namespace MovieLabVB.Lab
    Public Class SearchTextBox
        Inherits RadTextBoxElement
        Private buttonSearch_Renamed As MovieButtonElement

        Protected Overrides ReadOnly Property ThemeEffectiveType() As Type
            Get
                Return GetType(RadTextBoxElement)
            End Get
        End Property

        Public ReadOnly Property ButtonSearch() As MovieButtonElement
            Get
                Return buttonSearch_Renamed
            End Get
        End Property

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            buttonSearch_Renamed = New MovieButtonElement()
            buttonSearch_Renamed.Image = Resources.searchIcon
            buttonSearch_Renamed.ImageAlignment = ContentAlignment.MiddleCenter
            buttonSearch_Renamed.StretchVertically = True
            buttonSearch_Renamed.StretchHorizontally = False
            buttonSearch_Renamed.Alignment = ContentAlignment.MiddleRight
            buttonSearch_Renamed.Padding = New System.Windows.Forms.Padding(0, 0, 4, 0)
            Me.Children.Add(buttonSearch_Renamed)
        End Sub

        Protected Overrides Function ArrangeOverride(ByVal finalSize As SizeF) As SizeF
            MyBase.ArrangeOverride(finalSize)

            Dim borderThickness As System.Windows.Forms.Padding = Me.BorderThickness
            Dim clientRect As RectangleF = New RectangleF(borderThickness.Left, borderThickness.Top, finalSize.Width - borderThickness.Horizontal, finalSize.Height - borderThickness.Vertical)


            Me.TextBoxItem.Arrange(New RectangleF(clientRect.X + 2, clientRect.Y + 2, clientRect.Width - Me.buttonSearch_Renamed.DesiredSize.Width, Me.TextBoxItem.DesiredSize.Height))
            Me.buttonSearch_Renamed.Arrange(New RectangleF(clientRect.Right - Me.buttonSearch_Renamed.DesiredSize.Width, clientRect.Y, Me.buttonSearch_Renamed.DesiredSize.Width, clientRect.Height))

            Return finalSize
        End Function
    End Class
End Namespace
