Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports TheMovieDB

Namespace MovieLabVB.Lab
    Public Class RecomendationItem
        Inherits LightVisualElement
        Private poster_Renamed As BackdropItem
        Private title_Renamed As LightVisualElement
        Private year_Renamed As LightVisualElement
        Private rating_Renamed As RatingElement
        Private movie_Renamed As TmdbMovie

        Public ReadOnly Property Poster() As BackdropItem
            Get
                Return poster_Renamed
            End Get
        End Property
        Public ReadOnly Property Title() As LightVisualElement
            Get
                Return title_Renamed
            End Get
        End Property
        Public ReadOnly Property Year() As LightVisualElement
            Get
                Return year_Renamed
            End Get
        End Property
        Public ReadOnly Property Rating() As RatingElement
            Get
                Return rating_Renamed
            End Get
        End Property
        Public ReadOnly Property Movie() As TmdbMovie
            Get
                Return movie_Renamed
            End Get
        End Property

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()
            Me.StretchHorizontally = False
            Me.StretchVertically = False
            Me.MinSize = New Size(210, 50)
            Me.MaxSize = New Size(210, 50)
            Me.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
            Me.DrawBorder = True
            Me.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders
            Me.BorderTopWidth = 0
            Me.BorderRightWidth = 1
            Me.BorderBottomWidth = 0
            Me.BorderLeftWidth = 0
            Me.BorderRightColor = Color.FromArgb(53, 53, 64)
            Me.BorderRightShadowColor = Color.Black
            Me.ForeColor = Color.FromArgb(204, 255, 0)
            Me.ClipDrawing = True
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            Me.Image = Resources.waiting_bar

            poster_Renamed = New BackdropItem(Nothing, 50, 50)
            poster_Renamed.NotifyParentOnMouseInput = True
            Me.Children.Add(poster_Renamed)

            title_Renamed = New LightVisualElement()
            title_Renamed.ForeColor = Color.White
            title_Renamed.TextAlignment = ContentAlignment.MiddleLeft
            title_Renamed.NotifyParentOnMouseInput = True
            title_Renamed.AutoEllipsis = True
            title_Renamed.ClipDrawing = True
            Me.Children.Add(title_Renamed)

            year_Renamed = New LightVisualElement()
            year_Renamed.ForeColor = Color.FromArgb(107, 107, 121)
            year_Renamed.TextAlignment = ContentAlignment.MiddleLeft
            year_Renamed.NotifyParentOnMouseInput = True
            Me.Children.Add(year_Renamed)

            rating_Renamed = New RatingElement()
            rating_Renamed.NotifyParentOnMouseInput = True
            rating_Renamed.Rating = 0
            Me.Children.Add(rating_Renamed)
        End Sub

        Public Sub Load(ByVal movie_Renamed As TmdbMovie)
            Me.movie_Renamed = movie_Renamed
        End Sub

        Protected Overrides Sub OnLoaded()
            MyBase.OnLoaded()
            If Not Me.Parent Is Nothing AndAlso Me.Parent.Children.IndexOf(Me) = 0 Then
                Me.BorderLeftWidth = 1
                Me.BorderLeftColor = Color.FromArgb(53, 53, 64)
                Me.BorderLeftShadowColor = Color.Black
            End If
        End Sub

        Protected Overrides Sub OnMouseDown(ByVal e As System.Windows.Forms.MouseEventArgs)
            MyBase.OnMouseDown(e)
            If Not movie_Renamed Is Nothing Then
                Dim control As MovieControl = CType(ElementTree.Control, MovieControl)
                control.Element.NavigationService.Navigate(control.Element.DetailsPage)
                control.Element.DetailsPage.LoadMovie(movie_Renamed)
            End If
        End Sub

        Protected Overrides Function ArrangeOverride(ByVal finalSize As SizeF) As SizeF
            Dim clientRect As RectangleF = GetClientRectangle(finalSize)
            Dim x As Single = clientRect.X
            Dim y As Single = clientRect.Y

            poster_Renamed.Arrange(New RectangleF(x, y, poster_Renamed.DesiredSize.Width, poster_Renamed.DesiredSize.Height))
            x += poster_Renamed.DesiredSize.Width + 4

            title_Renamed.Arrange(New RectangleF(x, y, title_Renamed.DesiredSize.Width, title_Renamed.DesiredSize.Height))
            y += title_Renamed.DesiredSize.Height

            year_Renamed.Arrange(New RectangleF(x, y, title_Renamed.DesiredSize.Width, title_Renamed.DesiredSize.Height))
            y += year_Renamed.DesiredSize.Height

            rating_Renamed.Arrange(New RectangleF(x, y, title_Renamed.DesiredSize.Width, title_Renamed.DesiredSize.Height))

            Return finalSize
        End Function
    End Class
End Namespace
