﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using Telerik.WinControls;
using MovieLab.Properties;

namespace MovieLab.Lab
{
    public class GenresMenuItem : StackLayoutElement
    {
        MovieButtonElement buttonGo;

        public MovieButtonElement ButtonGo
        {
            get { return buttonGo; }
        }

        public List<GenreInfo> CheckedItems
        {
            get
            {
                List<GenreInfo> genres = new List<GenreInfo>();
                foreach (RadCheckBoxElement checkbox in this.Children[0].Children[0].Children)
                {
                    if (checkbox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        GenreInfo genre = (GenreInfo)checkbox.Tag;
                        genres.Add(genre);
                    }
                }
                foreach (RadCheckBoxElement checkbox in this.Children[0].Children[1].Children)
                {
                    if (checkbox.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
                    {
                        GenreInfo genre = (GenreInfo)checkbox.Tag;
                        genres.Add(genre);
                    }
                }
                return genres;
            }
        }

        public GenresMenuItem()
        {
        }
        
        protected override void InitializeFields()
        {
            base.InitializeFields();

            this.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.StretchVertically = true;
            this.StretchHorizontally = true;
            this.FitInAvailableSize = true;
            this.DrawFill = true;
            this.BackColor = Color.FromArgb(18, 18, 23);
            this.DrawBorder = true;
            this.BorderColor = Color.FromArgb(193, 241, 1);
            this.MaxSize = new Size(288, 0);
            this.BorderGradientStyle = GradientStyles.Solid;
            this.GradientStyle = GradientStyles.Solid;
        }

        protected override void CreateChildElements()
        {
            StackLayoutElement stack = new StackLayoutElement();
            stack.Orientation = System.Windows.Forms.Orientation.Horizontal;
            stack.StretchVertically = true;
            stack.StretchHorizontally = true;
            stack.FitInAvailableSize = true;
            this.Children.Add(stack);

            StackLayoutElement column1 = new StackLayoutElement();
            column1.Orientation = System.Windows.Forms.Orientation.Vertical;
            column1.FitInAvailableSize = true;
            column1.StretchVertically = true;
            stack.Children.Add(column1);

            StackLayoutElement column2 = new StackLayoutElement();
            column2.Orientation = System.Windows.Forms.Orientation.Vertical;
            column2.FitInAvailableSize = true;
            column2.StretchVertically = true;
            stack.Children.Add(column2);

            AddCheckboxes(column1, 0, GenreInfo.Genres.Length / 2);
            AddCheckboxes(column2, GenreInfo.Genres.Length / 2, GenreInfo.Genres.Length);

            buttonGo = new MovieButtonElement();
            buttonGo.StretchHorizontally = true;
            buttonGo.StretchVertically = false;
            buttonGo.Click += new EventHandler(buttonGo_Click);
            buttonGo.Normal = Resources.buttonGo;
            buttonGo.Hovered = Resources.buttonGoHover;
            buttonGo.Down = Resources.buttonGoHover;
            buttonGo.Margin = new System.Windows.Forms.Padding(-1, 0, 0, -2);
            this.Children.Add(buttonGo);
        }

        void buttonGo_Click(object sender, EventArgs e)
        {
            this.ElementTree.Control.Hide();
        }

        protected override SizeF MeasureOverride(SizeF availableSize)
        {
            SizeF desiredSize = base.MeasureOverride(availableSize);

            desiredSize.Width = 288;

            return desiredSize;
        }

        private void AddCheckboxes(RadElement parent, int startFrom, int endIndex)
        {
            for (int i = startFrom; i < endIndex; i++)
            {
                GenreInfo info = GenreInfo.Genres[i];
                RadCheckBoxElement checkbox = new RadCheckBoxElement();
                checkbox.Text = info.Name.ToUpper();
                checkbox.Tag = info;
                checkbox.IsThreeState = false;
                checkbox.StretchVertically = false;
                checkbox.StretchHorizontally = false;
                parent.Children.Add(checkbox);
            }
        }
    }
}
