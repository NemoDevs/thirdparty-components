﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using MovieLab.Properties;
using System.Drawing.Drawing2D;

namespace MovieLab.Lab
{
    public class BackdropItem: LightVisualElement
    {
        public bool zoomImage = false;
        public bool shadowOnLeft = false;

        public BackdropItem(Image image, int width, int height)
        {
            this.MinSize = new System.Drawing.Size(width, height);
            this.MaxSize = new System.Drawing.Size(width, height);
            SetImage(image);
        }

        protected override void InitializeFields()
        {
            base.InitializeFields();

            this.StretchHorizontally = false;
            this.StretchVertically = false;
            this.Image = Resources.waiting_bar;
            this.NotifyParentOnMouseInput = true;
        }

        public void SetImage(Image image)
        {
            if (image != null)
            {
                if (!zoomImage && this.MinSize != Size.Empty)
                {
                    this.Image = new Bitmap(image, this.MinSize);
                }
                else
                {
                    this.Image = image;
                    this.ClipDrawing = true;
                }
            }
        }

        protected override void PaintImage(Telerik.WinControls.Paint.IGraphics graphics)
        {
            if (zoomImage)
            {
                if (this.Image == null || this.Image.Width == 0 || this.Image.Height == 0)
                {
                    return;
                }

                float ratioX = (float)Size.Width / Image.Width;
                float ratioY = (float)Size.Height / Image.Height;

                float factor = ratioX;

                if (factor <= 0f)
                {
                    return;
                }

                int finalWidth = (int)Math.Round(Image.Width * factor);
                int finalHeight = (int)Math.Round(Image.Height * factor);

                int xStart = (int)((Size.Width - finalWidth) / 2f);
                int yStart = (int)((Size.Height - finalHeight) / 2f);

                graphics.DrawBitmap(Image, 0, -10, finalWidth, finalHeight);
            }
            else
            {
                base.PaintImage(graphics);
            }

            if (shadowOnLeft)
            {
                Graphics g = (Graphics)graphics.UnderlayGraphics;
                using (LinearGradientBrush brush = new LinearGradientBrush(
                    new Point(0, Size.Height),
                    new Point(100, Size.Height),
                    Color.FromArgb(255, 20, 20, 26),
                    Color.FromArgb(0, 20, 20, 26)))
                {
                    g.FillRectangle(brush, 0, 0, 100, this.Size.Height);
                }
            }
        }
    }
}
