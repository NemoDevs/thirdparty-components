Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports Telerik.WinControls

Namespace MovieLabVB.Lab
    Public Class HorizontalList
        Inherits LightVisualElement
#Region "Fields"

        Public Property TargetType() As Type
            Get
                Return m_TargetType
            End Get
            Set(value As Type)
                m_TargetType = Value
            End Set
        End Property
        Private m_TargetType As Type
        Private innerList As RadListViewElement
        Private m_scrollbar As RadScrollBarElement

#End Region

#Region "Properties"

        Public ReadOnly Property Scrollbar() As RadScrollBarElement
            Get
                Return m_scrollbar
            End Get
        End Property

        Public ReadOnly Property ListView() As RadListViewElement
            Get
                Return innerList
            End Get
        End Property

#End Region

#Region "Initialization"


        Public Sub New()
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()
            Me.ClipDrawing = True
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            Dim stack As New StackLayoutElement()
            stack.Orientation = System.Windows.Forms.Orientation.Vertical

            m_scrollbar = New RadScrollBarElement()
            m_scrollbar.ScrollType = ScrollType.Horizontal
            m_scrollbar.StretchHorizontally = True
            m_scrollbar.StretchVertically = False

            stack.DrawFill = True
            stack.BackColor = Color.FromArgb(47, 47, 61)
            stack.GradientStyle = Telerik.WinControls.GradientStyles.Solid
            stack.NotifyParentOnMouseInput = True
            stack.ShouldHandleMouseInput = True
            stack.Children.Add(m_scrollbar)

            innerList = New RadListViewElement()

            innerList.ViewType = ListViewType.IconsView
            innerList.ItemSize = MovieThumbElement.ThumbSize.ToSize()
            innerList.ViewElement.Orientation = System.Windows.Forms.Orientation.Horizontal
            innerList.ViewElement.Scroller.Scrollbar = Me.m_scrollbar
            innerList.ViewElement.HScrollBar.Visibility = ElementVisibility.Collapsed
            innerList.ViewElement.VScrollBar.Visibility = ElementVisibility.Collapsed
            innerList.ViewElement.Image = Resources.waiting_bar
            innerList.ViewElement.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
            innerList.ViewElement.ImageAlignment = ContentAlignment.TopCenter
            innerList.MaxSize = new System.Drawing.Size(0, 280)
            AddHandler innerList.Items.CollectionChanged, AddressOf Items_CollectionChanged

            AddHandler innerList.VisualItemCreating, AddressOf internalStack_VisualItemCreating

            innerList.NotifyParentOnMouseInput = True
            stack.Children.Add(innerList)

            Me.CanFocus = True
            Me.Children.Add(stack)

        End Sub

        Sub Items_CollectionChanged(sender As Object, e As Telerik.WinControls.Data.NotifyCollectionChangedEventArgs )
            If innerList.Items.Count > 0
            
                innerList.ViewElement.Image = Nothing
            Else
                innerList.ViewElement.Image = Resources.waiting_bar
            End If
        End Sub

        Private Sub internalStack_VisualItemCreating(sender As Object, e As ListViewVisualItemCreatingEventArgs)
            If Me.TargetType = GetType(PersonInfo) Then
                e.VisualItem = New CastItem()
            Else
                e.VisualItem = New MovieThumbElement()
            End If
        End Sub

        Protected Overrides Function ShouldUsePaintBuffer() As Boolean
            Return False
        End Function

#End Region

#Region "Layout"

        Protected Overrides Function MeasureOverride(availableSize As SizeF) As SizeF
            Me.m_scrollbar.Measure(availableSize)
            Return MyBase.MeasureOverride(availableSize)
        End Function

#End Region

#Region "Event handlers"

        Protected Overrides Sub OnLoaded()
            MyBase.OnLoaded()

            m_scrollbar.FirstButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed
            m_scrollbar.SecondButton.Visibility = Telerik.WinControls.ElementVisibility.Collapsed
            m_scrollbar.ThumbElement.ThumbFill.GradientStyle = Telerik.WinControls.GradientStyles.Solid
            m_scrollbar.ThumbElement.ThumbFill.BackColor = Color.White
            m_scrollbar.ThumbElement.ThumbBorder.Visibility = Telerik.WinControls.ElementVisibility.Collapsed
            m_scrollbar.MinSize = New System.Drawing.Size(0, 4)
            m_scrollbar.MinSize = New System.Drawing.Size(0, 4)
            m_scrollbar.ThumbElement.MaxSize = New System.Drawing.Size(0, 3)
            m_scrollbar.ThumbElement.MaxSize = New System.Drawing.Size(0, 3)
            m_scrollbar.ThumbElement.ThumbFill.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None
            m_scrollbar.ThumbElement.ThumbFill.Margin = New System.Windows.Forms.Padding(0, 1, 0, 0)
            m_scrollbar.FillElement.Visibility = Telerik.WinControls.ElementVisibility.Collapsed
            m_scrollbar.BorderElement.BoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders
            m_scrollbar.BorderElement.LeftWidth = 0
            m_scrollbar.BorderElement.RightWidth = 0
            m_scrollbar.BorderElement.TopWidth = 1
            m_scrollbar.BorderElement.BottomWidth = 1
            m_scrollbar.BorderElement.TopColor = Color.FromArgb(47, 47, 61)
            m_scrollbar.BorderElement.BottomColor = Color.FromArgb(76, 76, 100)
            m_scrollbar.ThumbElement.GripImage = Nothing
        End Sub

#End Region
    End Class
End Namespace
