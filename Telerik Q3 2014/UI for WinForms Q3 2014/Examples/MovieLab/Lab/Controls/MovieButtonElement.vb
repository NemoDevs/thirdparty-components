Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports System.Windows.Forms
Imports Telerik.WinControls

Namespace MovieLabVB.Lab
    Public Class MovieButtonElement
        Inherits LightVisualElement
        Public Enum ButtonStates
            Normal
            Hovered
            Down
            Selected
        End Enum

        Private m_state As ButtonStates

        Public Property State() As ButtonStates
            Get
                Return Me.m_state
            End Get
            Set(value As ButtonStates)
                If m_state <> value Then
                    m_state = value
                    OnStateChanged()
                End If
            End Set
        End Property

        Public Property Normal() As Bitmap
            Get
                Return m_Normal
            End Get
            Set(value As Bitmap)
                m_Normal = Value
            End Set
        End Property
        Private m_Normal As Bitmap
        Public Property Hovered() As Bitmap
            Get
                Return m_Hovered
            End Get
            Set(value As Bitmap)
                m_Hovered = Value
            End Set
        End Property
        Private m_Hovered As Bitmap
        Public Property Down() As Bitmap
            Get
                Return m_Down
            End Get
            Set(value As Bitmap)
                m_Down = Value
            End Set
        End Property
        Private m_Down As Bitmap
        Public Property Selected() As Bitmap
            Get
                Return m_Selected
            End Get
            Set(value As Bitmap)
                m_Selected = Value
            End Set
        End Property
        Private m_Selected As Bitmap

        Public Sub New()
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()

            Me.StretchHorizontally = False
            Me.StretchVertically = False
            Me.DrawBorder = False
            Me.DrawFill = False
        End Sub

        Protected Overrides Sub OnLoaded()
            MyBase.OnLoaded()
            OnStateChanged()
        End Sub

        Protected Overridable Sub OnStateChanged()
            Select Case m_state
                Case ButtonStates.Normal
                    If Me.Normal IsNot Nothing Then
                        Me.Image = Me.Normal
                    End If
                    Exit Select
                Case ButtonStates.Hovered
                    If Me.Hovered IsNot Nothing Then
                        Me.Image = Me.Hovered
                    End If
                    Exit Select
                Case ButtonStates.Down
                    If Me.Down IsNot Nothing Then
                        Me.Image = Me.Down
                    End If
                    Exit Select
                Case ButtonStates.Selected
                    If Me.Selected IsNot Nothing Then
                        Me.Image = Me.Selected
                    End If
                    Exit Select
            End Select
        End Sub

        Protected Overrides Sub OnMouseDown(e As System.Windows.Forms.MouseEventArgs)
            MyBase.OnMouseDown(e)
            If State <> ButtonStates.Selected Then
                State = ButtonStates.Down
            End If
            Me.Capture = True
        End Sub

        Protected Overrides Sub OnMouseUp(e As System.Windows.Forms.MouseEventArgs)
            MyBase.OnMouseUp(e)
            If State = ButtonStates.Down Then
                State = ButtonStates.Normal
            End If
            Me.Capture = False
        End Sub

        Protected Overrides Sub OnPropertyChanged(e As Telerik.WinControls.RadPropertyChangedEventArgs)
            MyBase.OnPropertyChanged(e)
            If e.Property.Equals(RadItem.IsMouseOverProperty) Then
                If CBool(e.NewValue) Then
                    If Control.MouseButtons = MouseButtons.Left Then
                        State = ButtonStates.Down
                    ElseIf State <> ButtonStates.Down AndAlso State <> ButtonStates.Selected Then
                        State = ButtonStates.Hovered
                    End If
                Else
                    If State = ButtonStates.Hovered OrElse State = ButtonStates.Down Then
                        State = ButtonStates.Normal
                    End If
                End If
            End If
        End Sub
    End Class
End Namespace