﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using MovieLab.Properties;
using Telerik.WinControls;
using TheMovieDB;

namespace MovieLab.Lab
{
    public class MovieThumbElement : IconListViewVisualItem
    {
        #region Fields

        Point mouseDown, mouseDown2;
        TmdbMovie movie;
        BackdropItem imageElement;
        LightVisualElement textElement;
        LightVisualElement textElement2;
        RatingElement ratingElement;
        MovieButtonElement buttonAddToWishList;

        #endregion

        #region Properties

        public BackdropItem ImageElement { get { return imageElement; } }
        public LightVisualElement TextElement { get { return textElement; } }
        public LightVisualElement TextElement2 { get { return textElement2; } }
        public RatingElement RatingElement { get { return ratingElement; } }
        public TmdbMovie Movie { get { return movie; } }
        public MovieButtonElement ButtonAddToWishList { get { return buttonAddToWishList; } }

        public static SizeF ThumbSize { get { return new SizeF(135, 260); } }

        #endregion

        #region Initialize

        public MovieThumbElement()
        {
            this.ClipDrawing = true;
            this.StretchHorizontally = false;
            this.StretchVertically = false;
            this.NotifyParentOnMouseInput = true;

            this.imageElement.MouseDown += new System.Windows.Forms.MouseEventHandler(imageElement_MouseDown);
            this.imageElement.MouseUp += new System.Windows.Forms.MouseEventHandler(imageElement_MouseUp);

            this.buttonAddToWishList.MouseDown += new System.Windows.Forms.MouseEventHandler(buttonAddToWishList_MouseDown);
            this.buttonAddToWishList.MouseUp += new System.Windows.Forms.MouseEventHandler(buttonAddToWishList_MouseUp);

            imageElement.Image = Resources.waiting_bar;
          
            this.NotifyParentOnMouseInput = true;
            this.ShouldHandleMouseInput = true;
        }

        public MovieThumbElement(TmdbMovie movie)
        {
            this.movie = movie;
            this.ClipDrawing = true;
            this.StretchHorizontally = false;
            this.StretchVertically = false;
            this.NotifyParentOnMouseInput = true;

            this.imageElement.MouseDown += new System.Windows.Forms.MouseEventHandler(imageElement_MouseDown);
            this.imageElement.MouseUp += new System.Windows.Forms.MouseEventHandler(imageElement_MouseUp);

            this.buttonAddToWishList.MouseDown += new System.Windows.Forms.MouseEventHandler(buttonAddToWishList_MouseDown);
            this.buttonAddToWishList.MouseUp += new System.Windows.Forms.MouseEventHandler(buttonAddToWishList_MouseUp);

            textElement.Text = movie.Name;
            ratingElement.Rating = (int)movie.Rating;
            textElement2.Text = movie.Released.HasValue ? movie.Released.Value.Year.ToString() : "N/A";
            if (movie.Poster != null)
            {
                imageElement.SetImage(movie.Poster);
            }
            else
            {
                imageElement.Image = Resources.waiting_bar;
            }
            this.NotifyParentOnMouseInput = true;
        }

        protected override void SynchronizeProperties()
        {
            base.SynchronizeProperties();

            this.movie = this.Data.Tag as TmdbMovie;
            
            textElement.Text = movie.Name;
            ratingElement.Rating = (int)movie.Rating;
            textElement2.Text = movie.Released.HasValue ? movie.Released.Value.Year.ToString() : "N/A";

            if (movie.Poster != null)
            {
                imageElement.SetImage(movie.Poster);
            }
            else
            {
                imageElement.Image = Resources.waiting_bar;
            }

            this.DrawFill = false;
        }
         
        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            imageElement = new BackdropItem(null, 120, 180);
            imageElement.ImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Children.Add(imageElement);

            ratingElement = new RatingElement();

            ratingElement.ForeColor = Color.FromArgb(204, 255, 0);
            this.Children.Add(ratingElement);

            textElement = new LightVisualElement();
            textElement.ForeColor = Color.White;
            textElement.TextAlignment = ContentAlignment.MiddleLeft;
            textElement.AutoEllipsis = true;
            this.Children.Add(textElement);

            textElement2 = new LightVisualElement();
            textElement2.ForeColor = Color.FromArgb(107, 107, 121);
            textElement2.TextAlignment = ContentAlignment.MiddleLeft;
            this.Children.Add(textElement2);

            buttonAddToWishList = new MovieButtonElement();
            buttonAddToWishList.Normal = Resources.buttonWishList;
            buttonAddToWishList.Hovered = Resources.buttonWishListHover;
            buttonAddToWishList.Down = Resources.buttonWishListDown;
            this.Children.Add(buttonAddToWishList);
        }

        #endregion

        #region Event handlers

        void imageElement_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            MovieElement movieElement = FindAncestor<MovieElement>();
            MovieControl control = (MovieControl)movieElement.ElementTree.Control;
            if (!control.Element.ScrollBehavior.IsRunning)
            {
                mouseDown = e.Location;
            }
        }

        void imageElement_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            MovieElement movieElement = FindAncestor<MovieElement>();
            MovieControl control = (MovieControl)movieElement.ElementTree.Control;
            if (Math.Abs(e.Location.X - mouseDown.X) < 5 &&
                Math.Abs(e.Location.Y - mouseDown.Y) < 5)
            {
                movieElement.NavigationService.Navigate(movieElement.DetailsPage);
                movieElement.DetailsPage.LoadMovie(this.movie);
            }
        }

        void buttonAddToWishList_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            MovieElement movieElement = FindAncestor<MovieElement>();
            MovieControl control = (MovieControl)movieElement.ElementTree.Control;
            
            mouseDown2 = e.Location;

        }

        void buttonAddToWishList_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            MovieElement movieElement = FindAncestor<MovieElement>();
            MovieControl control = (MovieControl)movieElement.ElementTree.Control;
            if (Math.Abs(e.Location.X - mouseDown2.X) < 5 &&
                Math.Abs(e.Location.Y - mouseDown2.Y) < 5)
            {
                if (!control.Element.ScrollBehavior.IsRunning)
                {
                    WishListMovie wishMovie = new WishListMovie();
                    wishMovie.ID = movie.Id;
                    wishMovie.Name = movie.Name;
                    wishMovie.Rating = (int)movie.Rating;
                    wishMovie.Year = movie.Released.HasValue ? movie.Released.Value.Year.ToString() : "";
                    wishMovie.Poster = ImageElement.Image;
                    wishMovie.Genres = control.GetMovieGenres(movie);
                    control.AddWishList(wishMovie);

                    control.Element.NavigationService.Navigate(control.Element.WishListPage);
                }
                else
                {
                    control.Element.ScrollBehavior.Stop();
                }
            }
        }

        #endregion

        #region Layout

        protected override SizeF MeasureOverride(SizeF availableSize)
        {
            base.MeasureOverride(availableSize);

            SizeF desiredSize = ThumbSize;
            imageElement.Measure(new SizeF(desiredSize.Width, desiredSize.Height - 60));
            ratingElement.Measure(new SizeF(desiredSize.Width, 20));
            textElement.Measure(new SizeF(desiredSize.Width, 20));
            textElement2.Measure(new SizeF(desiredSize.Width, 20));
            buttonAddToWishList.Measure(availableSize);

            return desiredSize;
        }

        protected override SizeF ArrangeOverride(SizeF finalSize)
        {
            RectangleF clientRect = GetClientRectangle(finalSize);
            imageElement.Arrange(new RectangleF(clientRect.X, clientRect.Y, imageElement.DesiredSize.Width, imageElement.DesiredSize.Height));
            textElement.Arrange(new RectangleF(clientRect.X, clientRect.Bottom - 60, clientRect.Width, 20));
            textElement2.Arrange(new RectangleF(clientRect.X, clientRect.Bottom - 40, clientRect.Width, 20));
            ratingElement.Arrange(new RectangleF(clientRect.X, clientRect.Bottom - 20, clientRect.Width, 20));
            buttonAddToWishList.Arrange(new RectangleF(
                clientRect.X + imageElement.DesiredSize.Width - 50, 
                clientRect.Y + imageElement.DesiredSize.Height -23,
                buttonAddToWishList.DesiredSize.Width,
                buttonAddToWishList.DesiredSize.Height
                ));
            return finalSize;
        }

        public override bool IsCompatible(Telerik.WinControls.UI.ListViewDataItem data, object context)
        {
            if (!(data.Tag is TmdbMovie))
            {
                return false;
            }

            if (!(data.Owner is IconView))
            {
                return true;
            }

            return !(data.Owner as IconView).BigThumbs;
        }

        #endregion
    }

}
