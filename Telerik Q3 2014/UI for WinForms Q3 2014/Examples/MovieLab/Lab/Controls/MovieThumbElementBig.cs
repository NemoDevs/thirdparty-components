﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using MovieLab.Properties;
using TheMovieDB;

namespace MovieLab.Lab
{
    public class MovieThumbElementBig : MovieThumbElement
    {
        LightVisualElement descriptionElement;

        public LightVisualElement DescriptionElement { get { return descriptionElement; } }

        public static SizeF DefaultThumbSize { get { return new SizeF(420, 300); } }

        public MovieThumbElementBig(): base()
        {
            this.DrawBorder = true;
            this.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            this.BorderTopWidth = 0;
            this.BorderLeftWidth = 0;
            this.BorderRightWidth = 0;
            this.BorderBottomWidth = 1;
            this.BorderBottomColor = Color.FromArgb(42, 42, 54);
            this.BorderBottomShadowColor = Color.FromArgb(0, 0, 0);
            this.NotifyParentOnMouseInput = true;
        }

        protected override void SynchronizeProperties()
        {
            base.SynchronizeProperties();
            this.DescriptionElement.Text = (this.Data.Tag as TmdbMovie).Overview;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            descriptionElement = new LightVisualElement();
            descriptionElement.ForeColor = Color.FromArgb(107, 107, 121);
            descriptionElement.TextAlignment = ContentAlignment.TopLeft;
            descriptionElement.TextWrap = true;
            descriptionElement.AutoEllipsis = true;
            this.Children.Add(descriptionElement);
        }

        protected override SizeF MeasureOverride(SizeF availableSize)
        {
            base.MeasureOverride(availableSize);

            SizeF desiredSize = DefaultThumbSize;
            ImageElement.Measure(new SizeF(desiredSize.Width, desiredSize.Height - 60));
            RatingElement.Measure(new SizeF(desiredSize.Width, 20));
            TextElement.Measure(new SizeF(desiredSize.Width, 20));
            TextElement2.Measure(new SizeF(desiredSize.Width, 20));
            ButtonAddToWishList.Measure(availableSize);
            DescriptionElement.Measure(new SizeF(desiredSize.Width - ImageElement.DesiredSize.Width - 20, desiredSize.Height - 60));
            return desiredSize;
        }

        protected override SizeF ArrangeOverride(SizeF finalSize)
        {
            RectangleF clientRect = GetClientRectangle(finalSize);
            float x = clientRect.X;
            float y = clientRect.Y;
            ImageElement.Arrange(new RectangleF(x, y, ImageElement.DesiredSize.Width, ImageElement.DesiredSize.Height));
            x += ImageElement.DesiredSize.Width + 20;
            TextElement.Arrange(new RectangleF(x, y, clientRect.Width - x, 20));
            y += 20;
            TextElement2.Arrange(new RectangleF(x, y, clientRect.Width - x, 20));
            y += 20;
            RatingElement.Arrange(new RectangleF(x, y, clientRect.Width - x, 20));
            y += 20;
            descriptionElement.Arrange(new RectangleF(x, y, clientRect.Width - x, clientRect.Height - y));
            ButtonAddToWishList.Arrange(new RectangleF(
              clientRect.X + ImageElement.DesiredSize.Width - 50,
              clientRect.Y + ImageElement.DesiredSize.Height + 15,
              ButtonAddToWishList.DesiredSize.Width,
              ButtonAddToWishList.DesiredSize.Height
              ));
            return finalSize;
        }

        public override bool IsCompatible(ListViewDataItem data, object context)
        {
            return (data.Owner as IconView).BigThumbs;
        }
    }
}
