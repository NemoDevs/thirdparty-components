﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using MovieLab.Properties;
using System.Windows.Forms;

namespace MovieLab.Lab
{
    public class HBorderElement: LightVisualElement
    {
        protected override void InitializeFields()
        {
            base.InitializeFields();

            this.StretchHorizontally = true;
            this.StretchVertically = false;
            this.MinSize = new System.Drawing.Size(0, 2);
            this.MaxSize = new System.Drawing.Size(0, 2);
            this.DrawBorder = true;
            this.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            this.BorderLeftWidth = 0;
            this.BorderRightWidth = 0;
            this.BorderTopWidth = 0;
            this.BorderBottomWidth = 1;
            this.BorderBottomColor = Color.FromArgb(0, 0, 0);
            this.BorderBottomShadowColor = Color.FromArgb(41, 41, 54);
        }
    }
}
