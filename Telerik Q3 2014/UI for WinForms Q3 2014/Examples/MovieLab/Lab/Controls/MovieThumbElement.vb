Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports Telerik.WinControls
Imports TheMovieDB

Namespace MovieLabVB.Lab
    Public Class MovieThumbElement
        Inherits IconListViewVisualItem
#Region "Fields"

        Private mouseDown_, mouseDown2 As Point
        Private movie_Renamed As TmdbMovie
        Private imageElement_Renamed As BackdropItem
        Private textElement_Renamed As LightVisualElement
        Private textElement2_Renamed As LightVisualElement
        Private ratingElement_Renamed As RatingElement
        Private buttonAddToWishList_Renamed As MovieButtonElement

#End Region

#Region "Properties"

        Public ReadOnly Property ImageElement() As BackdropItem
            Get
                Return imageElement_Renamed
            End Get
        End Property
        Public ReadOnly Property TextElement() As LightVisualElement
            Get
                Return textElement_Renamed
            End Get
        End Property
        Public ReadOnly Property TextElement2() As LightVisualElement
            Get
                Return textElement2_Renamed
            End Get
        End Property
        Public ReadOnly Property RatingElement() As RatingElement
            Get
                Return ratingElement_Renamed
            End Get
        End Property
        Public ReadOnly Property Movie() As TmdbMovie
            Get
                Return movie_Renamed
            End Get
        End Property
        Public ReadOnly Property ButtonAddToWishList() As MovieButtonElement
            Get
                Return buttonAddToWishList_Renamed
            End Get
        End Property

        Public Shared ReadOnly Property ThumbSize() As SizeF
            Get
                Return New SizeF(135, 260)
            End Get
        End Property

#End Region

#Region "Initialize"

        Public Sub New()
            Me.ClipDrawing = True
            Me.StretchHorizontally = False
            Me.StretchVertically = False
            Me.NotifyParentOnMouseInput = True

            AddHandler imageElement_Renamed.MouseDown, AddressOf imageElement_MouseDown
            AddHandler imageElement_Renamed.MouseUp, AddressOf imageElement_MouseUp

            AddHandler buttonAddToWishList_Renamed.MouseDown, AddressOf buttonAddToWishList_MouseDown
            AddHandler buttonAddToWishList_Renamed.MouseUp, AddressOf buttonAddToWishList_MouseUp

            imageElement_Renamed.Image = Resources.waiting_bar

            Me.NotifyParentOnMouseInput = True
            Me.ShouldHandleMouseInput = True
        End Sub

        Public Sub New(ByVal movie_Renamed As TmdbMovie)
            Me.movie_Renamed = movie_Renamed
            Me.ClipDrawing = True
            Me.StretchHorizontally = False
            Me.StretchVertically = False
            Me.NotifyParentOnMouseInput = True

            AddHandler imageElement_Renamed.MouseDown, AddressOf imageElement_MouseDown
            AddHandler imageElement_Renamed.MouseUp, AddressOf imageElement_MouseUp

            AddHandler buttonAddToWishList_Renamed.MouseDown, AddressOf buttonAddToWishList_MouseDown
            AddHandler buttonAddToWishList_Renamed.MouseUp, AddressOf buttonAddToWishList_MouseUp

            textElement_Renamed.Text = movie_Renamed.Name
            ratingElement_Renamed.Rating = CInt(Fix(movie_Renamed.Rating))
            If movie_Renamed.Released.HasValue Then
                textElement2_Renamed.Text = movie_Renamed.Released.Value.Year.ToString()
            Else
                textElement2_Renamed.Text = "N/A"
            End If
            If Not movie_Renamed.Poster Is Nothing Then
                imageElement_Renamed.SetImage(movie_Renamed.Poster)
            Else
                imageElement_Renamed.Image = Resources.waiting_bar
            End If
            Me.NotifyParentOnMouseInput = True
        End Sub

        Protected Overrides Sub SynchronizeProperties()
            MyBase.SynchronizeProperties()

            Me.movie_Renamed = TryCast(Me.Data.Tag, TmdbMovie)

            textElement_Renamed.Text = movie_Renamed.Name
            ratingElement_Renamed.Rating = CInt(Fix(movie_Renamed.Rating))
            If movie_Renamed.Released.HasValue Then
                textElement2_Renamed.Text = movie_Renamed.Released.Value.Year.ToString()
            Else
                textElement2_Renamed.Text = "N/A"
            End If

            If Not movie_Renamed.Poster Is Nothing Then
                imageElement_Renamed.SetImage(movie_Renamed.Poster)
            Else
                imageElement_Renamed.Image = Resources.waiting_bar
            End If

            Me.DrawFill = False
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            imageElement_Renamed = New BackdropItem(Nothing, 120, 180)
            imageElement_Renamed.ImageLayout = System.Windows.Forms.ImageLayout.Center
            Me.Children.Add(imageElement_Renamed)

            ratingElement_Renamed = New RatingElement()

            ratingElement_Renamed.ForeColor = Color.FromArgb(204, 255, 0)
            Me.Children.Add(ratingElement_Renamed)

            textElement_Renamed = New LightVisualElement()
            textElement_Renamed.ForeColor = Color.White
            textElement_Renamed.TextAlignment = ContentAlignment.MiddleLeft
            textElement_Renamed.AutoEllipsis = True
            Me.Children.Add(textElement_Renamed)

            textElement2_Renamed = New LightVisualElement()
            textElement2_Renamed.ForeColor = Color.FromArgb(107, 107, 121)
            textElement2_Renamed.TextAlignment = ContentAlignment.MiddleLeft
            Me.Children.Add(textElement2_Renamed)

            buttonAddToWishList_Renamed = New MovieButtonElement()
            buttonAddToWishList_Renamed.Normal = Resources.buttonWishList
            buttonAddToWishList_Renamed.Hovered = Resources.buttonWishListHover
            buttonAddToWishList_Renamed.Down = Resources.buttonWishListDown
            Me.Children.Add(buttonAddToWishList_Renamed)
        End Sub

#End Region

#Region "Event handlers"

        Private Sub imageElement_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
            Dim movieElement As MovieElement = FindAncestor(Of MovieElement)()
            Dim control As MovieControl = CType(movieElement.ElementTree.Control, MovieControl)
            If (Not control.Element.ScrollBehavior.IsRunning) Then
                mouseDown_ = e.Location
            End If
        End Sub

        Private Sub imageElement_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
            Dim movieElement As MovieElement = FindAncestor(Of MovieElement)()
            Dim control As MovieControl = CType(movieElement.ElementTree.Control, MovieControl)
            If Math.Abs(e.Location.X - mouseDown_.X) < 5 AndAlso Math.Abs(e.Location.Y - mouseDown_.Y) < 5 Then
                movieElement.NavigationService.Navigate(movieElement.DetailsPage)
                movieElement.DetailsPage.LoadMovie(Me.movie_Renamed)
            End If
        End Sub

        Private Sub buttonAddToWishList_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
            Dim movieElement As MovieElement = FindAncestor(Of MovieElement)()
            Dim control As MovieControl = CType(movieElement.ElementTree.Control, MovieControl)

            mouseDown2 = e.Location

        End Sub

        Private Sub buttonAddToWishList_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
            Dim movieElement As MovieElement = FindAncestor(Of MovieElement)()
            Dim control As MovieControl = CType(movieElement.ElementTree.Control, MovieControl)
            If Math.Abs(e.Location.X - mouseDown2.X) < 5 AndAlso Math.Abs(e.Location.Y - mouseDown2.Y) < 5 Then
                If (Not control.Element.ScrollBehavior.IsRunning) Then
                    Dim wishMovie As WishListMovie = New WishListMovie()
                    wishMovie.ID = movie_Renamed.Id
                    wishMovie.Name = movie_Renamed.Name
                    wishMovie.Rating = CInt(Fix(movie_Renamed.Rating))
                    If movie_Renamed.Released.HasValue Then
                        wishMovie.Year = movie_Renamed.Released.Value.Year.ToString()
                    Else
                        wishMovie.Year = ""
                    End If
                    wishMovie.Poster = ImageElement.Image
                    wishMovie.Genres = control.GetMovieGenres(movie_Renamed)
                    control.AddWishList(wishMovie)

                    control.Element.NavigationService.Navigate(control.Element.WishListPage)
                Else
                    control.Element.ScrollBehavior.Stop()
                End If
            End If
        End Sub

#End Region

#Region "Layout"

        Protected Overloads Overrides Function MeasureOverride(ByVal availableSize As SizeF) As SizeF
            MyBase.MeasureOverride(availableSize)

            Dim desiredSize As SizeF = ThumbSize
            imageElement_Renamed.Measure(New SizeF(desiredSize.Width, desiredSize.Height - 60))
            ratingElement_Renamed.Measure(New SizeF(desiredSize.Width, 20))
            textElement_Renamed.Measure(New SizeF(desiredSize.Width, 20))
            textElement2_Renamed.Measure(New SizeF(desiredSize.Width, 20))
            buttonAddToWishList_Renamed.Measure(availableSize)

            Return desiredSize
        End Function

        Protected Overrides Function ArrangeOverride(ByVal finalSize As SizeF) As SizeF
            Dim clientRect As RectangleF = GetClientRectangle(finalSize)
            imageElement_Renamed.Arrange(New RectangleF(clientRect.X, clientRect.Y, imageElement_Renamed.DesiredSize.Width, imageElement_Renamed.DesiredSize.Height))
            textElement_Renamed.Arrange(New RectangleF(clientRect.X, clientRect.Bottom - 60, clientRect.Width, 20))
            textElement2_Renamed.Arrange(New RectangleF(clientRect.X, clientRect.Bottom - 40, clientRect.Width, 20))
            ratingElement_Renamed.Arrange(New RectangleF(clientRect.X, clientRect.Bottom - 20, clientRect.Width, 20))
            buttonAddToWishList_Renamed.Arrange(New RectangleF(clientRect.X + imageElement_Renamed.DesiredSize.Width - 50, clientRect.Y + imageElement_Renamed.DesiredSize.Height - 23, buttonAddToWishList_Renamed.DesiredSize.Width, buttonAddToWishList_Renamed.DesiredSize.Height))
            Return finalSize
        End Function

        Public Overrides Function IsCompatible(ByVal data As Telerik.WinControls.UI.ListViewDataItem, ByVal context As Object) As Boolean
            If Not (TypeOf data.Tag Is TmdbMovie) Then
                Return False
            End If

            If Not (TypeOf data.Owner Is IconView) Then
                Return True
            End If

            Return Not (TryCast(data.Owner, IconView)).BigThumbs
        End Function

#End Region
    End Class

End Namespace
