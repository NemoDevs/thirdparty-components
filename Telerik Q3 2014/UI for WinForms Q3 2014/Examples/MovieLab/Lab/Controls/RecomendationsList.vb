Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing

Namespace MovieLabVB.Lab
    Public Class RecomendationsList
        Inherits StackLayoutElement
        Private stack_Renamed As StackLayoutElement

        Public ReadOnly Property Stack() As StackLayoutElement
            Get
                Return Me.stack_Renamed
            End Get
        End Property

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()
            Me.StretchHorizontally = True
            Me.StretchVertically = False
            Me.Orientation = System.Windows.Forms.Orientation.Horizontal
            Me.FitInAvailableSize = True
            Me.Margin = New System.Windows.Forms.Padding(0, 0, 0, 10)
            Me.ClipDrawing = True
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            Dim left As LightVisualElement = New LightVisualElement()
            left.StretchHorizontally = True
            left.DrawFill = True
            left.GradientStyle = Telerik.WinControls.GradientStyles.Solid
            left.BackColor = Color.FromArgb(43, 43, 55)
            Me.Children.Add(left)

            stack_Renamed = New StackLayoutElement()
            stack_Renamed.Orientation = System.Windows.Forms.Orientation.Horizontal
            stack_Renamed.Alignment = System.Drawing.ContentAlignment.MiddleCenter
            stack_Renamed.ElementSpacing = 5
            stack_Renamed.FitInAvailableSize = True
            stack_Renamed.StretchHorizontally = False
            stack_Renamed.StretchVertically = True
            stack_Renamed.ZIndex = 100

            Me.Children.Add(stack_Renamed)

            Dim right As LightVisualElement = New LightVisualElement()
            right.StretchHorizontally = True
            right.DrawFill = True
            right.GradientStyle = Telerik.WinControls.GradientStyles.Solid
            right.BackColor = Color.FromArgb(43, 43, 55)
            right.BorderWidth = 0
            right.DrawBorder = False
            right.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.SingleBorder
            right.Margin = New System.Windows.Forms.Padding(-1, 0, 0, 0)
            Me.Children.Add(right)
        End Sub
    End Class
End Namespace
