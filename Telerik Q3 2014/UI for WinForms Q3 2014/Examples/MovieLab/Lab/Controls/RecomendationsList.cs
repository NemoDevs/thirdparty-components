﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;

namespace MovieLab.Lab
{
    public class RecomendationsList: StackLayoutElement
    {
        StackLayoutElement stack;

        public StackLayoutElement Stack
        {
            get { return this.stack; }
        }

        protected override void InitializeFields()
        {
            base.InitializeFields();
            this.StretchHorizontally = true;
            this.StretchVertically = false;
            this.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.FitInAvailableSize = true;
            this.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.ClipDrawing = true;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            LightVisualElement left = new LightVisualElement();
            left.StretchHorizontally = true;
            left.DrawFill = true;
            left.GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            left.BackColor = Color.FromArgb(43, 43, 55);
            this.Children.Add(left);

            stack = new StackLayoutElement();
            stack.Orientation = System.Windows.Forms.Orientation.Horizontal;
            stack.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            stack.ElementSpacing = 5;
            stack.FitInAvailableSize = true;
            stack.StretchHorizontally = false;
            stack.StretchVertically = true;
            stack.ZIndex = 100;

            this.Children.Add(stack);

            LightVisualElement right = new LightVisualElement();
            right.StretchHorizontally = true;
            right.DrawFill = true;
            right.GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            right.BackColor = Color.FromArgb(43, 43, 55);
            right.BorderWidth = 0;
            right.DrawBorder = false;
            right.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.SingleBorder;
            right.Margin = new System.Windows.Forms.Padding(-1, 0, 0, 0);
            this.Children.Add(right);
        }
    }
}
