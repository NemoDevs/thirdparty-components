﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Windows.Forms;
using MovieLab.Properties;

namespace MovieLab.Lab
{
    public class BackdropsTitleElement: LightVisualElement
    {
        Bitmap leftImage;
        Bitmap rightImage;
        
        LightVisualElement title;
        LightVisualElement year;
        RatingElement rating;

        public LightVisualElement Title { get { return title; } }
        public LightVisualElement Year { get { return year; } }
        public RatingElement Rating { get { return rating; } }

        protected override void DisposeManagedResources()
        {
            base.DisposeManagedResources();
            if (leftImage != null)
            {
                leftImage.Dispose();
                leftImage = null;
            }
            if (rightImage != null)
            {
                rightImage.Dispose();
                rightImage = null;
            }
        }

        protected override void InitializeFields()
        {
            base.InitializeFields();
            this.StretchHorizontally = true;
            this.StretchVertically = true;
            this.DrawFill = true;
            this.ZIndex = 100;
            this.NotifyParentOnMouseInput = true;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            leftImage = Resources.backdropTitleLeft;
            rightImage = Resources.backdropTitleRight;

            StackLayoutElement stack = new StackLayoutElement();
            stack.FitInAvailableSize = true;
            stack.Margin = new Padding(0, 53, 0, 0);
            stack.NotifyParentOnMouseInput = true;
            this.Children.Add(stack);

            title = new LightVisualElement();
            title.TextAlignment = ContentAlignment.MiddleLeft;
            title.ForeColor = Color.Black;
            title.Text = "MORNING GLORY";
            title.Margin = new System.Windows.Forms.Padding(18, 0, 0, 0);
            title.StretchHorizontally = false;
            title.NotifyParentOnMouseInput = true;
            stack.Children.Add(title);

            year = new LightVisualElement();
            year.TextAlignment = ContentAlignment.MiddleLeft;
            year.ForeColor = Color.FromArgb(141, 142, 155);
            year.Text = "(2010)";
            year.StretchHorizontally = false;
            year.NotifyParentOnMouseInput = true;
            stack.Children.Add(year);

            rating = new RatingElement();
            rating.Rating = 5;
            rating.UseBigStars = true;
            rating.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            rating.StarSpacing = 8;
            rating.NotifyParentOnMouseInput = true;
            stack.Children.Add(rating);
        }

        protected override void PaintFill(Telerik.WinControls.Paint.IGraphics graphics, float angle, SizeF scale)
        {
            Graphics g = (Graphics)graphics.UnderlayGraphics;
            g.DrawImage(leftImage, new Point(0, this.Size.Height - leftImage.Height));
            g.DrawImage(rightImage, new Point(this.Size.Width - rightImage.Width, this.Size.Height - rightImage.Height - 7));

            using (SolidBrush brush = new SolidBrush(Color.FromArgb(234,239,247)))
            {
                g.FillRectangle(brush, new Rectangle(leftImage.Width, this.Size.Height - leftImage.Height, this.Size.Width - (leftImage.Width + rightImage.Width), 30));
            }
        }
    }
}
