Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text

Namespace MovieLabVB.Lab
    Public Class GenreInfo
        Public id As Integer
        Public Name As String

        Public Sub New(ByVal id As Integer, ByVal name As String)
            Me.id = id
            Me.Name = name
        End Sub

        Private Shared genres_Renamed As GenreInfo()

        Public Shared ReadOnly Property Genres() As GenreInfo()
            Get
                If genres_Renamed Is Nothing Then
                    genres_Renamed = New GenreInfo() {New GenreInfo(28, "Action"), New GenreInfo(12, "Adventure"), New GenreInfo(16, "Animation"), New GenreInfo(35, "Comedy"), New GenreInfo(80, "Crime"), New GenreInfo(105, "Disaster"), New GenreInfo(18, "Drama"), New GenreInfo(99, "Documentary"), New GenreInfo(14, "Fantasy"), New GenreInfo(36, "History"), New GenreInfo(27, "Horror"), New GenreInfo(10402, "Music"), New GenreInfo(22, "Musical"), New GenreInfo(9648, "Mystery"), New GenreInfo(878, "Science Fiction"), New GenreInfo(13, "Sport"), New GenreInfo(10748, "Suspense"), New GenreInfo(53, "Thriller"), New GenreInfo(37, "Western")}
                End If
                Return genres_Renamed
            End Get
        End Property
    End Class
End Namespace
