Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports Telerik.WinControls.Paint

Namespace MovieLabVB.Lab
    Public Class RatingElement
        Inherits LightVisualElement
        Public Shared starOn_ As Bitmap
        Public Shared starOff_ As Bitmap
        Public Shared bigStarOn As Bitmap
        Public Shared bigStarOff As Bitmap
        Public Shared bigStar2On As Bitmap
        Public Shared bigStar2Off As Bitmap

        Private m_rating As Integer

        Public Property StarOn() As Bitmap
            Get
                Return starOn_
            End Get
            Set(value As Bitmap)
                starOn_ = Value
            End Set
        End Property

        Public Property StarOff() As Bitmap
            Get
                Return starOff_
            End Get
            Set(value As Bitmap)
                starOff_ = Value
            End Set
        End Property

        Public Property Stars() As Integer
            Get
                Return m_Stars
            End Get
            Set(value As Integer)
                m_Stars = Value
            End Set
        End Property
        Private m_Stars As Integer

        Public Property Rating() As Integer
            Get
                Return m_rating
            End Get
            Set(value As Integer)
                If m_rating <> value Then
                    m_rating = value
                    Invalidate()
                End If
            End Set
        End Property

        Private m_useBigStars As Boolean

        Public Property UseBigStars() As Boolean
            Get
                Return m_useBigStars
            End Get
            Set(value As Boolean)
                If m_useBigStars <> value Then
                    m_useBigStars = value
                    If m_useBigStars Then
                        StarOn = bigStarOn
                        StarOff = bigStarOff
                    Else
                        StarOn = starOn_
                        StarOff = starOff_
                    End If
                End If
            End Set
        End Property

        Public Property StarSpacing() As Integer
            Get
                Return m_StarSpacing
            End Get
            Set(value As Integer)
                m_StarSpacing = Value
            End Set
        End Property
        Private m_StarSpacing As Integer

        Shared Sub New()
            starOn_ = Resources.starOn
            starOff_ = Resources.starOff
            bigStarOff = Resources.bigStarOff
            bigStarOn = Resources.bigStarOn
            bigStar2Off = Resources.bigStar2Off
            bigStar2On = Resources.bigStar2On
        End Sub

        Public Sub New()
            Stars = 10
            StarOn = starOn_
            StarOff = starOff_
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()
            Me.StretchHorizontally = True
        End Sub

        Protected Overrides Function MeasureOverride(availableSize As SizeF) As SizeF
            Dim desiredSize As SizeF = SizeF.Empty

            Dim star As Bitmap = StarOn

            Dim border As System.Windows.Forms.Padding = GetBorderThickness(False)
            desiredSize.Height = star.Height + border.Vertical + Padding.Vertical
            desiredSize.Width = (star.Width + StarSpacing) * Stars + border.Horizontal + Padding.Horizontal

            desiredSize.Width += 40

            Return desiredSize
        End Function

        Protected Overrides Sub PaintElement(graphics As IGraphics, angle As Single, scale As SizeF)
            Dim clientRect As RectangleF = GetClientRectangle(Me.Size)
            Dim x As Integer = CInt(clientRect.X)
            Dim y As Integer = CInt(clientRect.Y)
            Dim i As Integer = 0
            While i < Rating
                graphics.DrawBitmap(StarOn, x, y)
                x += starOn_.Width + StarSpacing
                i += 1
            End While
            While i < Stars
                graphics.DrawBitmap(StarOff, x, y)
                x += starOn_.Width + StarSpacing
                i += 1
            End While

            Dim g As Graphics = DirectCast(graphics.UnderlayGraphics, Graphics)
            Using brush As New SolidBrush(Me.ForeColor)
                If UseBigStars Then
                    y += 3
                End If
                g.DrawString(String.Format("{0}/{1}", Rating, Stars), Me.Font, brush, New PointF(x + 4, y - 2))
            End Using
        End Sub
    End Class
End Namespace
