Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports System.Windows.Forms

Namespace MovieLabVB.Lab
    Public Class BackdropsTitleElement
        Inherits LightVisualElement
        Private leftImage As Bitmap
        Private rightImage As Bitmap

        Private title_Renamed As LightVisualElement
        Private year_Renamed As LightVisualElement
        Private rating_Renamed As RatingElement

        Public ReadOnly Property Title() As LightVisualElement
            Get
                Return title_Renamed
            End Get
        End Property
        Public ReadOnly Property Year() As LightVisualElement
            Get
                Return year_Renamed
            End Get
        End Property
        Public ReadOnly Property Rating() As RatingElement
            Get
                Return rating_Renamed
            End Get
        End Property

        Protected Overrides Sub DisposeManagedResources()
            MyBase.DisposeManagedResources()
            If Not leftImage Is Nothing Then
                leftImage.Dispose()
                leftImage = Nothing
            End If
            If Not rightImage Is Nothing Then
                rightImage.Dispose()
                rightImage = Nothing
            End If
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()
            Me.StretchHorizontally = True
            Me.StretchVertically = True
            Me.DrawFill = True
            Me.ZIndex = 100
            Me.NotifyParentOnMouseInput = True
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            leftImage = Resources.backdropTitleLeft
            rightImage = Resources.backdropTitleRight

            Dim stack As StackLayoutElement = New StackLayoutElement()
            stack.FitInAvailableSize = True
            stack.Margin = New Padding(0, 53, 0, 0)
            stack.NotifyParentOnMouseInput = True
            Me.Children.Add(stack)

            title_Renamed = New LightVisualElement()
            title_Renamed.TextAlignment = ContentAlignment.MiddleLeft
            title_Renamed.ForeColor = Color.Black
            title_Renamed.Text = "MORNING GLORY"
            title_Renamed.Margin = New System.Windows.Forms.Padding(18, 0, 0, 0)
            title_Renamed.StretchHorizontally = False
            title_Renamed.NotifyParentOnMouseInput = True
            stack.Children.Add(title_Renamed)

            year_Renamed = New LightVisualElement()
            year_Renamed.TextAlignment = ContentAlignment.MiddleLeft
            year_Renamed.ForeColor = Color.FromArgb(141, 142, 155)
            year_Renamed.Text = "(2010)"
            year_Renamed.StretchHorizontally = False
            year_Renamed.NotifyParentOnMouseInput = True
            stack.Children.Add(year_Renamed)

            rating_Renamed = New RatingElement()
            rating_Renamed.Rating = 5
            rating_Renamed.UseBigStars = True
            rating_Renamed.Margin = New System.Windows.Forms.Padding(10, 0, 0, 0)
            rating_Renamed.StarSpacing = 8
            rating_Renamed.NotifyParentOnMouseInput = True
            stack.Children.Add(rating_Renamed)
        End Sub

        Protected Overloads Overrides Sub PaintFill(ByVal graphics As Telerik.WinControls.Paint.IGraphics, ByVal angle As Single, ByVal scale As SizeF)
            Dim g As Graphics = CType(graphics.UnderlayGraphics, Graphics)
            g.DrawImage(leftImage, New Point(0, Me.Size.Height - leftImage.Height))
            g.DrawImage(rightImage, New Point(Me.Size.Width - rightImage.Width, Me.Size.Height - rightImage.Height - 7))

            Using brush As SolidBrush = New SolidBrush(Color.FromArgb(234, 239, 247))
                g.FillRectangle(brush, New Rectangle(leftImage.Width, Me.Size.Height - leftImage.Height, Me.Size.Width - (leftImage.Width + rightImage.Width), 30))
            End Using
        End Sub
    End Class
End Namespace
