﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using MovieLab.Properties;
using System.Drawing;

namespace MovieLab.Lab
{
    public class SearchTextBox: RadTextBoxElement
    {
        MovieButtonElement buttonSearch;

        protected override Type ThemeEffectiveType
        {
            get { return typeof(RadTextBoxElement); }
        }

        public MovieButtonElement ButtonSearch
        {
            get { return buttonSearch; }
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            buttonSearch = new MovieButtonElement();
            buttonSearch.Image = Resources.searchIcon;
            buttonSearch.ImageAlignment = ContentAlignment.MiddleCenter;
            buttonSearch.StretchVertically = true;
            buttonSearch.StretchHorizontally = false;            
            buttonSearch.Alignment = ContentAlignment.MiddleRight;
            buttonSearch.Padding = new System.Windows.Forms.Padding(0, 0, 4, 0);

            this.Padding = new System.Windows.Forms.Padding(0, 1, 0, 4);
            this.Children.Add(buttonSearch);
        }

        protected override SizeF ArrangeOverride(SizeF finalSize)
        {
            base.ArrangeOverride(finalSize);

            System.Windows.Forms.Padding borderThickness = BorderThickness;
            RectangleF clientRect = new RectangleF(borderThickness.Left, borderThickness.Top,
                finalSize.Width - borderThickness.Horizontal,
                finalSize.Height - borderThickness.Vertical);
            

            this.TextBoxItem.Arrange(new RectangleF(clientRect.X + 2, clientRect.Y + 2, clientRect.Width - this.buttonSearch.DesiredSize.Width, this.TextBoxItem.DesiredSize.Height));
            this.buttonSearch.Arrange(new RectangleF(clientRect.Right - this.buttonSearch.DesiredSize.Width, clientRect.Y, this.buttonSearch.DesiredSize.Width, clientRect.Height));

            return finalSize;
        }
    }
}
