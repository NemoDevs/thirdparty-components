Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports Telerik.WinControls

Namespace MovieLabVB.Lab
    Public Class GenresMenuItem
        Inherits StackLayoutElement
        Private buttonGo_Renamed As MovieButtonElement

        Public ReadOnly Property ButtonGo() As MovieButtonElement
            Get
                Return buttonGo_Renamed
            End Get
        End Property

        Public ReadOnly Property CheckedItems() As List(Of GenreInfo)
            Get
                Dim genres As List(Of GenreInfo) = New List(Of GenreInfo)()
                For Each checkbox As RadCheckBoxElement In Me.Children(0).Children(0).Children
                    If checkbox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
                        Dim genre As GenreInfo = CType(checkbox.Tag, GenreInfo)
                        genres.Add(genre)
                    End If
                Next checkbox
                For Each checkbox As RadCheckBoxElement In Me.Children(0).Children(1).Children
                    If checkbox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
                        Dim genre As GenreInfo = CType(checkbox.Tag, GenreInfo)
                        genres.Add(genre)
                    End If
                Next checkbox
                Return genres
            End Get
        End Property

        Public Sub New()
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()

            Me.Orientation = System.Windows.Forms.Orientation.Vertical
            Me.StretchVertically = True
            Me.StretchHorizontally = True
            Me.FitInAvailableSize = True
            Me.DrawFill = True
            Me.BackColor = Color.FromArgb(18, 18, 23)
            Me.DrawBorder = True
            Me.BorderColor = Color.FromArgb(193, 241, 1)
            Me.MaxSize = New Size(288, 0)
            Me.BorderGradientStyle = GradientStyles.Solid
            Me.GradientStyle = GradientStyles.Solid
        End Sub

        Protected Overrides Sub CreateChildElements()
            Dim stack As StackLayoutElement = New StackLayoutElement()
            stack.Orientation = System.Windows.Forms.Orientation.Horizontal
            stack.StretchVertically = True
            stack.StretchHorizontally = True
            stack.FitInAvailableSize = True
            Me.Children.Add(stack)

            Dim column1 As StackLayoutElement = New StackLayoutElement()
            column1.Orientation = System.Windows.Forms.Orientation.Vertical
            column1.FitInAvailableSize = True
            column1.StretchVertically = True
            stack.Children.Add(column1)

            Dim column2 As StackLayoutElement = New StackLayoutElement()
            column2.Orientation = System.Windows.Forms.Orientation.Vertical
            column2.FitInAvailableSize = True
            column2.StretchVertically = True
            stack.Children.Add(column2)

            AddCheckboxes(column1, 0, CInt(GenreInfo.Genres.Length / 2))
            AddCheckboxes(column2, CInt(GenreInfo.Genres.Length / 2), GenreInfo.Genres.Length)

            buttonGo_Renamed = New MovieButtonElement()
            buttonGo_Renamed.StretchHorizontally = True
            buttonGo_Renamed.StretchVertically = False
            AddHandler buttonGo_Renamed.Click, AddressOf buttonGo_Click
            buttonGo_Renamed.Normal = Resources.buttonGo
            buttonGo_Renamed.Hovered = Resources.buttonGoHover
            buttonGo_Renamed.Down = Resources.buttonGoHover
            buttonGo_Renamed.Margin = New System.Windows.Forms.Padding(-1, 0, 0, -2)
            Me.Children.Add(buttonGo_Renamed)
        End Sub

        Private Sub buttonGo_Click(ByVal sender As Object, ByVal e As EventArgs)
            Me.ElementTree.Control.Hide()
        End Sub

        Protected Overloads Overrides Function MeasureOverride(ByVal availableSize As SizeF) As SizeF
            Dim desiredSize As SizeF = MyBase.MeasureOverride(availableSize)

            desiredSize.Width = 288

            Return desiredSize
        End Function

        Private Sub AddCheckboxes(ByVal parent As RadElement, ByVal startFrom As Integer, ByVal endIndex As Integer)
            Dim i As Integer = startFrom
            Do While i < endIndex
                Dim info As GenreInfo = GenreInfo.Genres(i)
                Dim checkbox As RadCheckBoxElement = New RadCheckBoxElement()
                checkbox.Text = info.Name.ToUpper()
                checkbox.Tag = info
                checkbox.IsThreeState = False
                checkbox.StretchVertically = False
                checkbox.StretchHorizontally = False
                parent.Children.Add(checkbox)
                i += 1
            Loop
        End Sub
    End Class
End Namespace
