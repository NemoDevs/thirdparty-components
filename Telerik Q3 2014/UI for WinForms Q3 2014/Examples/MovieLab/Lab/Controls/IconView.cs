﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Windows.Forms;
using System.Drawing;
using Telerik.WinControls;
using Telerik.WinControls.Layout;
using TheMovieDB;

namespace MovieLab.Lab
{
    public class IconView : RadListViewElement
    {
        private bool bigThumbs = false;

        public IconView()
        {
            this.ItemSize = MovieThumbElement.ThumbSize.ToSize();
            this.ItemSpacing = 0;
            this.StretchHorizontally = false;
        }

        protected override void InitializeFields()
        {
            base.InitializeFields();
            this.ClipDrawing = true;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            this.ViewType = ListViewType.IconsView; 
        }
        
        public MovieThumbElement FindMovieElement(TmdbMovie movie)
        {
            foreach (MovieThumbElement movieElement in this.ViewElement.ViewElement.Children)
            {
                if (movieElement.Movie == movie)
                {
                    return movieElement;
                }
            }
            return null;
        }

        protected override void OnVisualItemCreating(ListViewVisualItemCreatingEventArgs args)
        {
            base.OnVisualItemCreating(args);

            args.VisualItem = this.BigThumbs ? new MovieThumbElementBig() : new MovieThumbElement();
        }

        public bool BigThumbs
        {
            get
            {
                return this.bigThumbs;
            }
            set
            {
                if (this.bigThumbs != value)
                {
                    this.bigThumbs = value;
                    this.ItemSize = value ? MovieThumbElementBig.DefaultThumbSize.ToSize() : MovieThumbElement.ThumbSize.ToSize();
                }
            }
        }
    }
}