﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;

namespace MovieLab.Lab
{
    public class GradientBorderElement: LightVisualElement
    {
        public GradientBorderElement(float borderAngle, int width, bool solid)
        {
            if (borderAngle == 0)
            {
                this.MinSize = new System.Drawing.Size(0, 1);
                this.MaxSize = new System.Drawing.Size(0, 1);
                this.StretchHorizontally = true;
                this.StretchVertically = false;
            }
            else
            {
                this.MinSize = new System.Drawing.Size(1, 0);
                this.MaxSize = new System.Drawing.Size(1, 0);
                this.StretchHorizontally = false;
                this.StretchVertically = true;
            }
            this.GradientAngle = borderAngle;

            if (solid)
            {
                this.GradientStyle = Telerik.WinControls.GradientStyles.Solid;
                this.BackColor = Color.FromArgb(202, 253, 0);
            }
        }

        protected override void InitializeFields()
        {
            base.InitializeFields();

            this.DrawFill = true;
            this.GradientStyle = Telerik.WinControls.GradientStyles.Linear;
            this.BackColor = Color.FromArgb(42, 42, 55);
            this.BackColor2 = Color.FromArgb(202, 253, 0);
            this.BackColor3 = Color.FromArgb(202, 253, 0);
            this.BackColor4 = Color.FromArgb(42, 42, 55);
            this.NumberOfColors = 4;
            this.GradientPercentage = 0.6f;
            this.GradientPercentage2 = 0.67f;
        }
    }
}
