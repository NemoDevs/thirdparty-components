﻿using System.Drawing;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using MovieLab.Properties;
using System.Diagnostics;
using System.Collections.Generic;

namespace MovieLab.Lab
{
    public class MovieElement : StackLayoutElement
    {
        #region Fields

        private NavigationService navigationService;       
        private HeaderElement headerElement;
        private HomePageElement homePage;
        private MoviesPageElement moviesPage;
        private DetailsPageElement detailsPage;
        private WishListPageElement wishListPage;
        private Bitmap bottomLines;
        private Bitmap bottomLines2;
        private ScrollServiceBehavior scrollBehavior = new ScrollServiceBehavior();
        private SizingGripElement sizingGrip;
        private LightVisualElement categoriesView;

        #endregion

        #region Initialization

        public MovieElement()
        {
            bottomLines = Resources.form_lines_left;
            bottomLines2 = Resources.form_lines_right;
            
            this.scrollBehavior.Add(new ScrollService(this.HomePage.MoviesList.HorizontalList.ListView.ViewElement.ViewElement, this.HomePage.MoviesList.HorizontalList.Scrollbar));
            this.scrollBehavior.Add(new ScrollService(this.detailsPage.DetailsPanel.Cast.ListView.ViewElement.ViewElement,this.detailsPage.DetailsPanel.Cast.Scrollbar));
            this.scrollBehavior.Add(new ScrollService(this.MoviesPage.IconView.ViewElement.ViewElement, this.MoviesPage.IconView.ViewElement.VScrollBar));
        }

        public ScrollServiceBehavior ScrollBehavior
        {
            get
            {
                return this.scrollBehavior;
            }           
        }       

        protected override void InitializeFields()
        {
            base.InitializeFields();

            this.StretchHorizontally = true;
            this.StretchVertically = true;
            this.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.FitInAvailableSize = true;
            this.DrawFill = true;
            this.BackColor = Color.FromArgb(20, 20, 26);
            this.GradientStyle = GradientStyles.Solid;
            this.navigationService = new NavigationService(this);            
            
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            headerElement = new HeaderElement();
            this.Children.Add(headerElement);

            categoriesView = new LightVisualElement();
            categoriesView.StretchHorizontally = true;
            categoriesView.StretchVertically = false;
            categoriesView.DisableHTMLRendering = false;
            categoriesView.Text = "";
            categoriesView.TextAlignment = ContentAlignment.TopLeft;
            categoriesView.Padding = new System.Windows.Forms.Padding(20, 0, 2, 0);
            categoriesView.MinSize = new Size(0, 20);
            categoriesView.MaxSize = new Size(0, 20);
            this.Children.Add(categoriesView);

            homePage = new HomePageElement();
            moviesPage = new MoviesPageElement();
            detailsPage = new DetailsPageElement();
            wishListPage = new WishListPageElement();

            sizingGrip = new SizingGripElement();
            this.Children.Add(sizingGrip);

            this.NavigationService.Navigate(this.homePage);
        }

        #endregion

        #region Properties

        public HeaderElement Header
        {
            get { return headerElement; }
        }

        public HomePageElement HomePage
        {
            get { return homePage; }
        }

        public MoviesPageElement MoviesPage
        {
            get { return moviesPage; }
        }

        public DetailsPageElement DetailsPage
        {
            get { return detailsPage; }
        }

        public WishListPageElement WishListPage
        {
            get { return wishListPage; }
        }

        public PageElement CurrentPage
        {
            get
            {
                if (this.Children.Count > 1)
                {
                    return this.Children[2] as PageElement;
                }
                return null;
            }
            set
            {
                if (value != CurrentPage)
                {
                    if (this.Children.Count > 3)
                    {
                        this.Children.RemoveAt(2);
                    }
                    if (value != null)
                    {
                        this.Children.Insert(2, value);
                    }
                    if (this.ElementTree != null)
                    {
                        MovieControl control = (MovieControl)this.ElementTree.Control;
                        MoviesForm form = (MoviesForm)control.FindForm();
                        form.UpdateWishlistText(control.WishList.Count);
                    }
                }
            }
        }

        public NavigationService NavigationService
        {
            get
            {
                return this.navigationService;
            }

        }

        public LightVisualElement CategoriesView
        {
            get { return this.categoriesView; }
        }

        #endregion

        #region Paint

        protected override void PaintFill(Telerik.WinControls.Paint.IGraphics graphics, float angle, SizeF scale)
        {
            base.PaintFill(graphics, angle, scale);

            Graphics g = (Graphics)graphics.UnderlayGraphics;
            g.DrawImage(bottomLines, new Point(1, this.Size.Height - 16 - bottomLines.Size.Height));

            g.DrawImage(bottomLines2, new Point(this.Size.Width - 1 - bottomLines2.Size.Width, this.Size.Height - 16 - bottomLines2.Size.Height));
        }
     
        #endregion           
    }
}

