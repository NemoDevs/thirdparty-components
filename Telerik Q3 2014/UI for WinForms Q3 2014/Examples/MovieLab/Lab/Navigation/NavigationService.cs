﻿using System;
using System.Collections.Generic;

namespace MovieLab.Lab
{
    public class NavigationService
    {
        #region Fields

        private MovieElement owner;
        private List<NavigationInfo> history;
        private int index = -1;

        #endregion

        #region Constructor

        public NavigationService(MovieElement owner)
        {
            this.owner = owner;
            this.history = new List<NavigationInfo>();
        }

        #endregion

        #region Methods

        public void Navigate(PageElement page)
        {
            this.Navigate(page, null);
        }

        public void Navigate(PageElement page, object state)
        {
            if (page == null)
            {
                throw new ArgumentNullException();
            }

            int nextIndex = this.index + 1;
            int range = this.history.Count - nextIndex;
            this.history.RemoveRange(nextIndex, range);

            this.history.Add(new NavigationInfo(page, state));
            this.index = this.history.Count - 1;
            this.NavigateCore(page, state);
        }

        public void GoBack()
        {
            int newIndex = this.index - 1;

            if (newIndex >= 0)
            {
                this.NavigateTo(newIndex);
            }
        }

        public void GoForward()
        {
            int newIndex = this.index + 1;

            if (newIndex < this.history.Count)
            {
                this.NavigateTo(newIndex);
            }
        }

        protected void NavigateTo(int newIndex)
        {
            this.index = newIndex;
            NavigationInfo navigationInfo = this.history[index];
            this.NavigateCore(navigationInfo.Page, navigationInfo.State);
        }

        protected virtual void NavigateCore(PageElement page, object state)
        {
            if (this.owner.CurrentPage != null)
            {
                this.owner.CurrentPage.NavigationCompleted(false, state, page);
            }
            this.owner.CurrentPage = page;
            page.NavigationCompleted(true, state, page);

            if (page == owner.HomePage)
            {
                owner.CategoriesView.Text = "";
            }
        }

        #endregion
    }
}
