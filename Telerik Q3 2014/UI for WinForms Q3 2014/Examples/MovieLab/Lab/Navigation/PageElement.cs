﻿using Telerik.WinControls.UI;

namespace MovieLab.Lab
{
    public class PageElement : StackLayoutElement
    {
        public MovieControl Root
        {
            get { return (MovieControl)this.ElementTree.Control; }
        }

        protected internal virtual void NavigationCompleted(bool isCurrent, object state, PageElement page)
        {

        }
    }
}
