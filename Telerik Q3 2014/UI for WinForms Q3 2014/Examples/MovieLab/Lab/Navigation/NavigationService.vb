Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic

Namespace MovieLabVB.Lab
    Public Class NavigationService
#Region "Fields"

        Private owner As MovieElement
        Private history As List(Of NavigationInfo)
        Private index As Integer = -1

#End Region

#Region "Constructor"

        Public Sub New(ByVal owner As MovieElement)
            Me.owner = owner
            Me.history = New List(Of NavigationInfo)()
        End Sub

#End Region

#Region "Methods"

        Public Sub Navigate(ByVal page As PageElement)
            Me.Navigate(page, Nothing)
        End Sub

        Public Sub Navigate(ByVal page As PageElement, ByVal state As Object)
            If page Is Nothing Then
                Throw New ArgumentNullException()
            End If

            Dim nextIndex As Integer = Me.index + 1
            Dim range As Integer = Me.history.Count - nextIndex
            Me.history.RemoveRange(nextIndex, range)

            Me.history.Add(New NavigationInfo(page, state))
            Me.index = Me.history.Count - 1
            Me.NavigateCore(page, state)
        End Sub

        Public Sub GoBack()
            Dim newIndex As Integer = Me.index - 1

            If newIndex >= 0 Then
                Me.NavigateTo(newIndex)
            End If
        End Sub

        Public Sub GoForward()
            Dim newIndex As Integer = Me.index + 1

            If newIndex < Me.history.Count Then
                Me.NavigateTo(newIndex)
            End If
        End Sub

        Protected Sub NavigateTo(ByVal newIndex As Integer)
            Me.index = newIndex
            Dim navigationInfo As NavigationInfo = Me.history(index)
            Me.NavigateCore(navigationInfo.Page, navigationInfo.State)
        End Sub

        Protected Overridable Sub NavigateCore(ByVal page As PageElement, ByVal state As Object)
            If Not Me.owner.CurrentPage Is Nothing Then
                Me.owner.CurrentPage.NavigationCompleted(False, state, page)
            End If
            Me.owner.CurrentPage = page
            page.NavigationCompleted(True, state, page)

            If page Is owner.HomePage Then
                owner.CategoriesView.Text = ""
            End If
        End Sub

#End Region
    End Class
End Namespace
