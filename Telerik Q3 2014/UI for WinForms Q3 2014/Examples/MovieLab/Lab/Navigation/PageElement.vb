Imports Microsoft.VisualBasic
Imports System
Imports Telerik.WinControls.UI

Namespace MovieLabVB.Lab
    Public Class PageElement
        Inherits StackLayoutElement
        Public ReadOnly Property Root() As MovieControl
            Get
                Return CType(Me.ElementTree.Control, MovieControl)
            End Get
        End Property

        Protected Friend Overridable Sub NavigationCompleted(ByVal isCurrent As Boolean, ByVal state As Object, ByVal page As PageElement)

        End Sub
    End Class
End Namespace
