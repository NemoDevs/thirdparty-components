Imports Microsoft.VisualBasic
Imports System
Namespace MovieLabVB.Lab
    Public Class NavigationInfo
#Region "Fields"

        Private page_Renamed As PageElement
        Private state_Renamed As Object

#End Region

#Region "Constructor"

        Public Sub New(ByVal page_Renamed As PageElement, ByVal state_Renamed As Object)
            Me.page_Renamed = page_Renamed
            Me.state_Renamed = state_Renamed
        End Sub

#End Region

#Region "Properties"

        Public ReadOnly Property Page() As PageElement
            Get
                Return Me.page_Renamed
            End Get
        End Property

        Public ReadOnly Property State() As Object
            Get
                Return Me.state_Renamed
            End Get
        End Property

#End Region
    End Class
End Namespace
