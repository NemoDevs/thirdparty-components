﻿
namespace MovieLab.Lab
{
    public class NavigationInfo
    {
        #region Fields

        private PageElement page;
        private object state;

        #endregion

        #region Constructor

        public NavigationInfo(PageElement page, object state)
        {
            this.page = page;
            this.state = state;
        }

        #endregion

        #region Properties

        public PageElement Page
        {
            get { return this.page; }
        }

        public object State
        {
            get
            {
                return this.state;
            }
        }

        #endregion
    }
}
