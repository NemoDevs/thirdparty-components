﻿using System;
using Telerik.WinControls;
using TheMovieDB;
using MovieLab.Properties;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using Telerik.WinControls.UI;
using System.Threading;

namespace MovieLab.Lab
{
    public class MovieControl : RadControl
    {
        public static Font SegoeUI21Italic = new Font("Segoe UI", 21f, FontStyle.Italic);
        public static Font SegoeUI28Italic = new Font("Segoe UI", 28f, FontStyle.Italic);
        public static Font SegoeUI14 = new Font("Segoe UI", 14f);
        public static Font SegoeUI9 = new Font("Segoe UI", 9);
        public static Font SegoeUI7 = new Font("Seoge UI", 7.2f);
        public static Font SegoeUI10Italic = new Font("Segoe UI", 10f, FontStyle.Italic);

        #region Fields

        int[] recommendedMovieIDs = new int[] { 48275, 60795, 46166, 43922 };

        List<MovieThumbElement> topMovies = new List<MovieThumbElement>();
        List<MovieThumbElement> newMovies = new List<MovieThumbElement>();
        List<TmdbMovie> recommendedMovies = new List<TmdbMovie>();
        BindingList<WishListMovie> wishList = new BindingList<WishListMovie>();

        MovieElement element;
        TmdbAPI tmdbAPI;
        TmdbGenre[] genres;
        internal bool ScrollBehaviorWasRunning;

        #endregion

        #region Initialization

        public MovieControl()
        {
            try
            {

            tmdbAPI = new TmdbAPI(Settings.Default.TmdbApiKey);
            tmdbAPI.MovieSearchCompleted += new TmdbAPI.MovieSearchAsyncCompletedEventHandler(tmdbAPI_MovieSearchCompleted);
            tmdbAPI.GetMovieInfoCompleted += new TmdbAPI.MovieInfoAsyncCompletedEventHandler(tmdbAPI_GetMovieInfoCompleted);

            ThreadPool.QueueUserWorkItem(new WaitCallback(LoadGenres));

            Element.HomePage.MoviesList.SelectedPageChanged += new EventHandler(MoviesList_SelectedPageChanged);
            Element.Header.ButtonNew.Click += new EventHandler(ButtonNew_Click);
            Element.Header.ButtonTop.Click += new EventHandler(ButtonTop_Click);
            Element.Header.TextBox.ButtonSearch.Click += new EventHandler(ButtonSearch_Click);
            Element.DetailsPage.DetailsPanel.ButtonAddToWishList.Click += new EventHandler(ButtonAddToWishList_Click);
            Element.Header.TextBox.KeyDown += new KeyEventHandler(TextBox_KeyDown);
            Element.HomePage.MoviesList.HorizontalList.Image = Resources.waiting_bar;

            DateTime now = DateTime.Now;

            DateTime to = new DateTime(now.Year, now.Month, now.Day);
            DateTime from = to.AddMonths(-3);
            TmdbMovieBrowseRequest request = new TmdbMovieBrowseRequest(from, to);
            request.order_by = TmdbOrderBy.release;
            request.order = TmdbOrderType.desc;
            request.per_page = 10;
            request.page = 0;
            tmdbAPI.MovieBrowseAsync(request, newMovies); 
                
            request = new TmdbMovieBrowseRequest(8, 10);
            request.order_by = TmdbOrderBy.rating;
            request.order = TmdbOrderType.desc;
            request.release_max = new DateTime(now.Year, now.Month, now.Day);
            request.release_min = request.release_max.AddYears(-1);
            request.per_page = 10;
            request.page = 0;
            tmdbAPI.MovieBrowseAsync(request, topMovies);


            for (int i = 0; i < recommendedMovieIDs.Length; i++)
            {
                RecomendationItem item = new RecomendationItem();
                Element.HomePage.Recomendations.Stack.Children.Add(item);
                tmdbAPI.GetMovieInfoAsync(recommendedMovieIDs[i], item);
            }

            tmdbAPI.GetMovieInfoAsync(1865, Element.HomePage.BackdropsView);

            }
            catch {}
        }

        private void LoadGenres(object state)
        {
            genres = tmdbAPI.GetList();
        }

        protected override void CreateChildItems(RadElement parent)
        {
            element = new MovieElement();
            parent.Children.Add(element);
        }

        #endregion

        #region Methods

        public int GetGenreID(string genreName)
        {
            foreach (TmdbGenre g in genres)
            {
                if (g.Name == genreName)
                {
                    return g.ID;
                }
            }
            return -1;
        }

        public void AddWishList(WishListMovie wishListMovie)
        {
            foreach (WishListMovie movie in wishList)
            {
                if (movie.ID == wishListMovie.ID)
                {
                    return;
                }
            }
            wishList.Add(wishListMovie);
        }

        public string GetMovieGenres(TmdbMovie movie)
        {
            MovieControl control = (MovieControl)this.ElementTree.Control;
            TmdbMovie info = control.API.GetMovieInfo(movie.Id);
            if (info.Categories != null)
            {
                StringBuilder b = new StringBuilder();
                for (int i = 0; i < info.Categories.Length; i++)
                {
                    TmdbCategory category = info.Categories[i];
                    b.Append(category.Name);
                    if (i < info.Categories.Length - 1)
                    {
                        b.Append(",");
                    }
                }
                return b.ToString();
            }
            return "";
        }

        #endregion

        #region Properties

        public MovieElement Element
        {
            get { return element; }
        }

        public TmdbAPI API
        {
            get { return this.tmdbAPI; }
        }

        public TmdbGenre[] Genres { get { return this.genres; } }

        public BindingList<WishListMovie> WishList { get { return this.wishList; } }

        #endregion

        #region Event Handlers

        protected override void OnClientSizeChanged(EventArgs e)
        {
            base.OnClientSizeChanged(e);
            this.Element.InvalidateMeasure(true);
        }

        private void tmdbAPI_MovieSearchCompleted(object sender, ImdbMovieSearchCompletedEventArgs e)
        {
            if (e.Movies == null)
            {
                return;
            }
            if (!e.Cancelled)
            {
                List<MovieThumbElement> items = e.UserState as List<MovieThumbElement>;
                if (items == null)
                {
                    return;
                }
                items.Clear();

                try
                {
                    foreach (TmdbMovie movie in e.Movies)
                    {
                        MovieThumbElement element = new MovieThumbElement(movie);
                        element.Tag = movie;
                        element.ImageElement.Image = Resources.waiting_bar;                   
                        items.Add(element);
                        movie.MovieImagesLoadCompleted += new TmdbMovie.MovieImagesLoadCompletedEventHandler(movie_MovieImagesLoadCompleted);
                        movie.LoadImagesAsync(element, true);
                        tmdbAPI.GetMovieInfoAsync(movie.Id, element);
                    }
                }
                catch {}

                if (items == newMovies)
                {
                    Invoke(new MethodInvoker(delegate() {
                        Element.HomePage.MoviesList.HorizontalList.Image = null;
                        LoadItems(Element.HomePage.MoviesList.HorizontalList.ListView, newMovies);
                    }));
                } 
            }
        }

        private void tmdbAPI_GetMovieInfoCompleted(object sender, ImdbMovieInfoCompletedEventArgs e)
        {
            if (e.Movie == null)
            {
                return;
            }
            MovieThumbElement element = e.UserState as MovieThumbElement;
            if (element != null)
            {
                Invoke(new MethodInvoker(delegate() { element.RatingElement.Rating = (int)e.Movie.Rating; }));
            }
            RecomendationItem item = e.UserState as RecomendationItem;
            if (item != null)
            {
                Invoke(new MethodInvoker(delegate()
                {
                    item.Title.Text = e.Movie.Name;
                    item.Rating.Rating = (int)e.Movie.Rating;
                    item.Year.Text = e.Movie.Released.HasValue ? e.Movie.Released.Value.Year.ToString() : "";
                    item.Load(e.Movie);
                    e.Movie.MovieImagesLoadCompleted += new TmdbMovie.MovieImagesLoadCompletedEventHandler(movie_MovieImagesLoadCompleted);
                    e.Movie.LoadImagesAsync(item, new TmdbImageLoadRequest() { type = TmdbImageType.poster, size = TmdbImageSize.w342, count = 1 });
                }));
            }
            BackdropsView view = e.UserState as BackdropsView;
            if (view != null)
            {
                e.Movie.MovieImagesLoadCompleted += new TmdbMovie.MovieImagesLoadCompletedEventHandler(movie_MovieImagesLoadCompleted);
                view.Load(e.Movie);
                e.Movie.LoadImagesAsync(view, false);
                e.Movie.LoadPoster();
                return;
            }
        }

        private void movie_MovieImagesLoadCompleted(object sender, ImdbMovieImagesLoadCompletedEventArgs e)
        {
            if (e.Movie == null)
            {
                return;
            }
            TmdbMovie movie = e.Movie;
            movie.MovieImagesLoadCompleted -= new TmdbMovie.MovieImagesLoadCompletedEventHandler(movie_MovieImagesLoadCompleted);

            Invoke(new MethodInvoker(delegate()
            {
                MovieThumbElement element = e.UserState as MovieThumbElement;
                if (element != null)
                {
                    Image image = movie.Poster;
                    if (image == null)
                    {
                        element.ImageElement.SetImage(Resources.na_img);
                    }
                    else
                    {
                        element.ImageElement.SetImage(image);
                    }
                }
                RecomendationItem item = e.UserState as RecomendationItem;
                if (item != null)
                {
                    item.Poster.SetImage(e.Movie.Poster);
                    item.Image = null;
                }

                BackdropsView view = e.UserState as BackdropsView;
                if (view != null)
                {
                    BackdropsView view2 = Element.MoviesPage.BackdropsView;
                    int i = 0;
                    Element.HomePage.BackdropsView.Load(e.Movie);
                    view2.Load(e.Movie);
                    foreach (TmdbImage image in e.Movie.Images)
                    {
                        if (i > 4)
                        {
                            break;
                        }
                        if (image.Image != null && image.Type == TmdbImageType.backdrop && image.Size == TmdbImageSize.original)
                        {
                            ((BackdropItem)view.Children[1].Children[i]).SetImage(image.Image);
                            ((BackdropItem)view.Children[1].Children[i]).SetImage(image.Image);
                            ((BackdropItem)view2.Children[1].Children[i]).SetImage(image.Image);
                            ((BackdropItem)view2.Children[1].Children[i]).SetImage(image.Image);
                            i += 2;
                        }
                    }
                }
            }));
        }

        private void MoviesList_SelectedPageChanged(object sender, EventArgs e)
        {
            if (newMovies == null || topMovies == null)
            {
                return;
            }

            Invoke(new MethodInvoker(delegate()
            {
                Element.HomePage.MoviesList.HorizontalList.Scrollbar.Value = 0;
                if (Element.HomePage.MoviesList.SelectedPageIndex == 0)
                {
                    LoadItems(Element.HomePage.MoviesList.HorizontalList.ListView, newMovies);
                }
                else
                {
                    LoadItems(Element.HomePage.MoviesList.HorizontalList.ListView, topMovies);
                }
            }));
        }

        private void ButtonTop_Click(object sender, EventArgs e)
        {    
            this.Element.NavigationService.Navigate(this.Element.MoviesPage);
            this.Element.MoviesPage.LoadTop();
        }

        private void ButtonNew_Click(object sender, EventArgs e)
        {
            this.Element.NavigationService.Navigate(this.Element.MoviesPage);
            this.Element.MoviesPage.LoadNew();
        }         

        private void ButtonSearch_Click(object sender, EventArgs e)
        {
            this.Element.NavigationService.Navigate(this.Element.MoviesPage);
            this.Element.MoviesPage.LoadSearch(this.Element.Header.TextBox.Text);
        }

        private void ButtonAddToWishList_Click(object sender, EventArgs e)
        {
            if (!Element.ScrollBehavior.IsRunning)
            {
                TmdbMovie movie = Element.DetailsPage.Movie;
                WishListMovie wishMovie = new WishListMovie();
                wishMovie.ID = movie.Id;
                wishMovie.Name = movie.Name;
                wishMovie.Rating = (int)movie.Rating;
                wishMovie.Year = movie.Released.HasValue ? movie.Released.Value.Year.ToString() : "";
                wishMovie.Poster = Element.DetailsPage.DetailsPanel.Poster.Image;
                wishMovie.Genres = GetMovieGenres(movie);
                AddWishList(wishMovie);

                this.Element.NavigationService.Navigate(this.Element.WishListPage);
            }
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.Element.NavigationService.Navigate(this.Element.MoviesPage);
                this.Element.MoviesPage.LoadSearch(this.Element.Header.TextBox.Text);
            }
        }

        protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
        {
            base.OnMouseDown(e);

            if (this.ElementTree.GetElementAtPoint(e.Location) is RadScrollBarElement ||
                this.ElementTree.GetElementAtPoint(e.Location) is ScrollBarThumb)
            {
                this.Element.ScrollBehavior.Stop();
            }
            else
            {
                this.Element.ScrollBehavior.MouseDown(e.Location);
            }
        }

        protected override void OnMouseUp(System.Windows.Forms.MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (this.ElementTree.GetElementAtPoint(e.Location) is RadScrollBarElement ||
                this.ElementTree.GetElementAtPoint(e.Location) is ScrollBarThumb)
            {
                this.Element.ScrollBehavior.Stop();
            }
            else
            {
                this.Element.ScrollBehavior.MouseUp(e.Location);
            }

            ScrollBehaviorWasRunning = this.Element.ScrollBehavior.IsRunning;
        }

        protected override void OnMouseMove(System.Windows.Forms.MouseEventArgs e)
        {
            base.OnMouseMove(e);
            this.Element.ScrollBehavior.MouseMove(e.Location);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            base.OnMouseWheel(e);
            
            RadScrollBarElement scrollbar = null;
            int smallChange = 50;
            int step = Math.Max(1, e.Delta / SystemInformation.MouseWheelScrollDelta);
            int delta = Math.Sign(e.Delta) * step * SystemInformation.MouseWheelScrollLines;

            if (this.Element.CurrentPage == this.Element.MoviesPage)
            {
                scrollbar = this.element.MoviesPage.IconView.ViewElement.VScrollBar;
            }
            else if (this.Element.CurrentPage == this.Element.HomePage)
            {
                scrollbar = this.Element.HomePage.MoviesList.HorizontalList.Scrollbar;
            }
            else if (this.Element.CurrentPage == this.Element.DetailsPage)
            {
                scrollbar = this.Element.DetailsPage.DetailsPanel.Cast.Scrollbar;
            }

            if (scrollbar != null)
            {
                smallChange = 40;
                int result = scrollbar.Value - delta * smallChange;

                if (result > scrollbar.Maximum - scrollbar.LargeChange + 1)
                {
                    result = scrollbar.Maximum - scrollbar.LargeChange + 1;
                }

                if (result < scrollbar.Minimum)
                {
                    result = 0;
                }
                else if (result > scrollbar.Maximum)
                {
                    result = scrollbar.Maximum;
                }

                if (result != scrollbar.Value)
                {
                    scrollbar.Value = result;
                }
            }           
        }

        #endregion

        private void LoadItems(RadElement element, List<MovieThumbElement> movies)
        {
            RadListViewElement listElement = element as RadListViewElement;
            if (listElement != null)
            {
                listElement.Items.BeginUpdate();
                listElement.Items.Clear();
                foreach (MovieThumbElement movie in movies)
                {
                    listElement.Items.Add(new ListViewDataItem() { Tag = movie.Movie });
                }
                listElement.Items.EndUpdate();

                return;
            }

            element.Children.Clear();
            foreach (RadElement movie in movies)
            {
                element.Children.Add(movie);
            }
        }
    }
}
