Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports System.Windows.Forms

Namespace MovieLabVB.Lab
    Public Class HeaderElement
        Inherits StackLayoutElement
        Private buttonCategory_Renamed As MovieButtonElement
        Private buttonTop_Renamed As MovieButtonElement
        Private buttonNew_Renamed As MovieButtonElement
        Private textBox_Renamed As SearchTextBox
        Private logo_Renamed As LightVisualElement

        Public ReadOnly Property ButtonTop() As MovieButtonElement
            Get
                Return buttonTop_Renamed
            End Get
        End Property
        Public ReadOnly Property ButtonNew() As MovieButtonElement
            Get
                Return buttonNew_Renamed
            End Get
        End Property
        Public ReadOnly Property Logo() As LightVisualElement
            Get
                Return logo_Renamed
            End Get
        End Property
        Public ReadOnly Property TextBox() As SearchTextBox
            Get
                Return textBox_Renamed
            End Get
        End Property
        Public ReadOnly Property ButtonCategory() As MovieButtonElement
            Get
                Return buttonCategory_Renamed
            End Get
        End Property

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()
            Me.StretchHorizontally = True
            Me.StretchVertically = False
            Me.Orientation = System.Windows.Forms.Orientation.Horizontal
            Me.ElementSpacing = 0
            Me.FitInAvailableSize = True
            Me.Padding = New System.Windows.Forms.Padding(0, 0, 0, 2)
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            buttonCategory_Renamed = New MovieButtonElement()
            buttonCategory_Renamed.Text = ""
            buttonCategory_Renamed.StretchHorizontally = False
            buttonCategory_Renamed.StretchVertically = False
            buttonCategory_Renamed.Normal = Resources.buttonAllNormal
            buttonCategory_Renamed.Selected = Resources.buttonAllDown
            buttonCategory_Renamed.Hovered = Resources.buttonAllHover
            buttonCategory_Renamed.Padding = New System.Windows.Forms.Padding(5, 0, 0, 0)
            AddHandler buttonCategory_Renamed.MouseDown, AddressOf buttonCategory_MouseDown
            Me.Children.Add(buttonCategory_Renamed)

            buttonTop_Renamed = New MovieButtonElement()
            buttonTop_Renamed.Normal = Resources.buttonTopNormal
            buttonTop_Renamed.Hovered = Resources.buttonTopHover
            buttonTop_Renamed.Down = Resources.buttonTopDown
            Me.Children.Add(buttonTop_Renamed)

            buttonNew_Renamed = New MovieButtonElement()
            buttonNew_Renamed.Normal = Resources.buttonNewNormal
            buttonNew_Renamed.Hovered = Resources.buttonNewHover
            buttonNew_Renamed.Down = Resources.buttonNewDown
            buttonNew_Renamed.Padding = New System.Windows.Forms.Padding(0, 0, 0, 0)
            Me.Children.Add(buttonNew_Renamed)

            textBox_Renamed = New SearchTextBox()
            textBox_Renamed.StretchHorizontally = True
            textBox_Renamed.StretchVertically = False
            textBox_Renamed.Margin = New System.Windows.Forms.Padding(90, 0, 0, 0)
            textBox_Renamed.TextBoxItem.NullText = "search"
            textBox_Renamed.TextBoxItem.Font = MovieControl.SegoeUI10Italic
            Me.Children.Add(textBox_Renamed)

            logo_Renamed = New LightVisualElement()
            logo_Renamed.StretchHorizontally = False
            logo_Renamed.StretchVertically = False
            logo_Renamed.Margin = New System.Windows.Forms.Padding(0, 0, -1, 0)
            logo_Renamed.Image = Resources.logo
            Me.Children.Add(logo_Renamed)
        End Sub

        Private Sub buttonCategory_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
            buttonCategory_Renamed.State = MovieButtonElement.ButtonStates.Selected
            Dim menu As RadDropDownMenu = New RadDropDownMenu()
            Dim genresMenuItem As GenresMenuItem = New GenresMenuItem()
            AddHandler genresMenuItem.ButtonGo.Click, AddressOf ButtonGo_Click
            genresMenuItem.ButtonGo.Tag = genresMenuItem
            menu.Items.Add(genresMenuItem)
            Dim popupElement As RadDropDownMenuElement = CType(menu.PopupElement, RadDropDownMenuElement)
            popupElement.Border.Visibility = Telerik.WinControls.ElementVisibility.Collapsed
            menu.Show(buttonCategory_Renamed, buttonCategory_Renamed.ControlBoundingRectangle.Left + 20, buttonCategory_Renamed.Size.Height - 1)
            menu.MaximumSize = New System.Drawing.Size(288, 0)
            AddHandler menu.DropDownClosed, AddressOf menu_DropDownClosed
        End Sub

        Private Sub ButtonGo_Click(ByVal sender As Object, ByVal e As EventArgs)
            buttonCategory_Renamed.State = MovieButtonElement.ButtonStates.Normal
            Application.DoEvents()

            Dim movieElement As MovieElement = FindAncestor(Of MovieElement)()
            Dim button As MovieButtonElement = CType(sender, MovieButtonElement)
            Dim genresMenuItem As GenresMenuItem = CType(button.Tag, GenresMenuItem)

            movieElement.NavigationService.Navigate(movieElement.MoviesPage)

            Dim categoriesText As StringBuilder = New StringBuilder()
            Dim builder As StringBuilder = New StringBuilder()

            categoriesText.Append("<html><font=Segoe UI><size=10>")

            Dim i As Integer = 0
            Do While i < genresMenuItem.CheckedItems.Count
                Dim info As GenreInfo = genresMenuItem.CheckedItems(i)
                builder.Append(info.id)
                If i < genresMenuItem.CheckedItems.Count - 1 Then
                    builder.Append(",")
                End If
                categoriesText.Append("<color=#CCFF00>-<color=white><i>")
                categoriesText.Append(info.Name)
                i += 1
            Loop
            movieElement.CategoriesView.Text = categoriesText.ToString()
            movieElement.MoviesPage.LoadCategories(builder.ToString())
        End Sub

        Private Sub menu_DropDownClosed(ByVal sender As Object, ByVal args As RadPopupClosedEventArgs)
            Dim menu As RadDropDownMenu = CType(sender, RadDropDownMenu)
            RemoveHandler menu.DropDownClosed, AddressOf menu_DropDownClosed
            buttonCategory_Renamed.State = MovieButtonElement.ButtonStates.Normal
        End Sub
    End Class
End Namespace
