Imports Microsoft.VisualBasic
Imports System.Drawing
Imports Telerik.WinControls
Imports Telerik.WinControls.UI
Imports System.Diagnostics
Imports System.Collections.Generic

Namespace MovieLabVB.Lab
    Public Class MovieElement
        Inherits StackLayoutElement
#Region "Fields"

        Private navigationService_Renamed As NavigationService
        Private headerElement As HeaderElement
        Private homePage_Renamed As HomePageElement
        Private moviesPage_Renamed As MoviesPageElement
        Private detailsPage_Renamed As DetailsPageElement
        Private wishListPage_Renamed As WishListPageElement
        Private bottomLines As Bitmap
        Private bottomLines2 As Bitmap
        Private scrollBehavior_Renamed As ScrollServiceBehavior = New ScrollServiceBehavior()
        Private sizingGrip As SizingGripElement
        Private categoriesView_Renamed As LightVisualElement

#End Region

#Region "Initialization"

        Public Sub New()
            bottomLines = Resources.form_lines_left
            bottomLines2 = Resources.form_lines_right

            Me.scrollBehavior_Renamed.Add(New ScrollService(Me.HomePage.MoviesList.HorizontalList.ListView.ViewElement.ViewElement, Me.HomePage.MoviesList.HorizontalList.Scrollbar))
            Me.scrollBehavior_Renamed.Add(New ScrollService(Me.detailsPage_Renamed.DetailsPanel.Cast.ListView.ViewElement.ViewElement, Me.detailsPage_Renamed.DetailsPanel.Cast.Scrollbar))
            Me.scrollBehavior_Renamed.Add(New ScrollService(Me.MoviesPage.IconView.ViewElement.ViewElement, Me.MoviesPage.IconView.ViewElement.VScrollBar))
        End Sub

        Public ReadOnly Property ScrollBehavior() As ScrollServiceBehavior
            Get
                Return Me.scrollBehavior_Renamed
            End Get
        End Property

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()

            Me.StretchHorizontally = True
            Me.StretchVertically = True
            Me.Orientation = System.Windows.Forms.Orientation.Vertical
            Me.FitInAvailableSize = True
            Me.DrawFill = True
            Me.BackColor = Color.FromArgb(20, 20, 26)
            Me.GradientStyle = GradientStyles.Solid
            Me.navigationService_Renamed = New NavigationService(Me)

        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            headerElement = New HeaderElement()
            Me.Children.Add(headerElement)

            categoriesView_Renamed = New LightVisualElement()
            categoriesView_Renamed.StretchHorizontally = True
            categoriesView_Renamed.StretchVertically = False
            categoriesView_Renamed.DisableHTMLRendering = False
            categoriesView_Renamed.Text = ""
            categoriesView_Renamed.TextAlignment = ContentAlignment.TopLeft
            categoriesView_Renamed.Padding = New System.Windows.Forms.Padding(20, 0, 2, 0)
            categoriesView_Renamed.MinSize = New Size(0, 20)
            categoriesView_Renamed.MaxSize = New Size(0, 20)
            Me.Children.Add(categoriesView_Renamed)

            homePage_Renamed = New HomePageElement()
            moviesPage_Renamed = New MoviesPageElement()
            detailsPage_Renamed = New DetailsPageElement()
            wishListPage_Renamed = New WishListPageElement()

            sizingGrip = New SizingGripElement()
            Me.Children.Add(sizingGrip)

            Me.NavigationService.Navigate(Me.homePage_Renamed)
        End Sub

#End Region

#Region "Properties"

        Public ReadOnly Property Header() As HeaderElement
            Get
                Return headerElement
            End Get
        End Property

        Public ReadOnly Property HomePage() As HomePageElement
            Get
                Return homePage_Renamed
            End Get
        End Property

        Public ReadOnly Property MoviesPage() As MoviesPageElement
            Get
                Return moviesPage_Renamed
            End Get
        End Property

        Public ReadOnly Property DetailsPage() As DetailsPageElement
            Get
                Return detailsPage_Renamed
            End Get
        End Property

        Public ReadOnly Property WishListPage() As WishListPageElement
            Get
                Return wishListPage_Renamed
            End Get
        End Property

        Public Property CurrentPage() As PageElement
            Get
                If Me.Children.Count > 1 Then
                    Return TryCast(Me.Children(2), PageElement)
                End If
                Return Nothing
            End Get
            Set(value As PageElement)
                If Not Value Is CurrentPage Then
                    If Me.Children.Count > 3 Then
                        Me.Children.RemoveAt(2)
                    End If
                    If Not Value Is Nothing Then
                        Me.Children.Insert(2, Value)
                    End If
                    If Not Me.ElementTree Is Nothing Then
                        Dim control As MovieControl = CType(Me.ElementTree.Control, MovieControl)
                        Dim form As MoviesForm = CType(control.FindForm(), MoviesForm)
                        form.UpdateWishlistText(control.WishList.Count)
                    End If
                End If
            End Set
        End Property

        Public ReadOnly Property NavigationService() As NavigationService
            Get
                Return Me.navigationService_Renamed
            End Get

        End Property

        Public ReadOnly Property CategoriesView() As LightVisualElement
            Get
                Return Me.categoriesView_Renamed
            End Get
        End Property

#End Region

#Region "Paint"

        Protected Overloads Overrides Sub PaintFill(ByVal graphics As Telerik.WinControls.Paint.IGraphics, ByVal angle As Single, ByVal scale As SizeF)
            MyBase.PaintFill(graphics, angle, scale)

            Dim g As Graphics = CType(graphics.UnderlayGraphics, Graphics)
            g.DrawImage(bottomLines, New Point(1, Me.Size.Height - 16 - bottomLines.Size.Height))

            g.DrawImage(bottomLines2, New Point(Me.Size.Width - 1 - bottomLines2.Size.Width, Me.Size.Height - 16 - bottomLines2.Size.Height))
        End Sub

#End Region
    End Class
End Namespace

