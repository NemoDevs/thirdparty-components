﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;

namespace MovieLab.Lab
{
    public class RatingColumn: GridViewDataColumn
    {
        public RatingColumn()
            : base("Rating")
        {
            Width = 140;
        }

        public override Type GetCellType(GridViewRowInfo row)
        {
            if (row is GridViewDataRowInfo)
            {
                return typeof(RatingCell);
            }
            return base.GetCellType(row);
        }
    }
}
