Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing

Namespace MovieLabVB.Lab
    Public Class RatingCell
        Inherits GridDataCellElement
        Private rating_Renamed As RatingElement

        Public Sub New(ByVal column As GridViewColumn, ByVal row As GridRowElement)
            MyBase.New(column, row)

        End Sub

        Protected Overrides ReadOnly Property ThemeEffectiveType() As Type
            Get
                Return GetType(GridDataCellElement)
            End Get
        End Property

        Public ReadOnly Property Rating() As RatingElement
            Get
                Return rating_Renamed
            End Get
        End Property

        Protected Overrides Sub SetContentCore(ByVal value As Object)
            Me.Text = ""
            rating_Renamed.Rating = CInt(Fix(value))
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            rating_Renamed = New RatingElement()
            rating_Renamed.Alignment = ContentAlignment.MiddleCenter
            rating_Renamed.ForeColor = Color.FromArgb(204, 255, 0)
            Me.Children.Add(rating_Renamed)
        End Sub

        Public Overrides Function IsCompatible(ByVal data As GridViewColumn, ByVal context As Object) As Boolean
            Return TypeOf context Is GridDataRowElement AndAlso data.Name = "Rating" AndAlso MyBase.IsCompatible(data, context)
        End Function

        Protected Overrides Function ArrangeOverride(ByVal finalSize As SizeF) As SizeF
            Dim clientRect As RectangleF = GetClientRectangle(finalSize)
            rating_Renamed.Arrange(New RectangleF(clientRect.X, clientRect.Y + (clientRect.Height - rating_Renamed.DesiredSize.Height) / 2, clientRect.Width, rating_Renamed.DesiredSize.Height))
            Return finalSize
        End Function
    End Class
End Namespace
