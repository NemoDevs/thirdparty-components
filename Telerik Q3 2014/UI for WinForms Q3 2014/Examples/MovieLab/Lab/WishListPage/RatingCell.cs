﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;

namespace MovieLab.Lab
{
    public class RatingCell: GridDataCellElement
    {
        RatingElement rating;

        public RatingCell(GridViewColumn column, GridRowElement row)
            : base(column, row)
        {

        }

        protected override Type ThemeEffectiveType
        {
            get { return typeof(GridDataCellElement); }
        }

        public RatingElement Rating
        {
            get { return rating; }
        }

        protected override void SetContentCore(object value)
        {
            this.Text = "";
            rating.Rating = (int)value;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();
            
            rating = new RatingElement();
            rating.Alignment = ContentAlignment.MiddleCenter;
            rating.ForeColor = Color.FromArgb(204, 255, 0);
            this.Children.Add(rating);
        }

        public override bool IsCompatible(GridViewColumn data, object context)
        {
            return context is GridDataRowElement && data.Name == "Rating" && base.IsCompatible(data, context);
        }

        protected override SizeF ArrangeOverride(SizeF finalSize)
        {
            RectangleF clientRect = GetClientRectangle(finalSize);
            rating.Arrange(new RectangleF(clientRect.X, clientRect.Y + (clientRect.Height - rating.DesiredSize.Height) / 2, clientRect.Width, rating.DesiredSize.Height));
            return finalSize;
        }
    }
}
