﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace MovieLab.Lab
{
    public class WishListMovie
    {
        public int ID { get; set; }
        public Image Poster { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public int Rating { get; set; }
        public string Genres { get; set; }
    }
}
