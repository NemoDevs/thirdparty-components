Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI

Namespace MovieLabVB.Lab
    Public Class RatingColumn
        Inherits GridViewDataColumn
        Public Sub New()
            MyBase.New("Rating")
            Width = 140
        End Sub

        Public Overrides Function GetCellType(ByVal row As GridViewRowInfo) As Type
            If TypeOf row Is GridViewDataRowInfo Then
                Return GetType(RatingCell)
            End If
            Return MyBase.GetCellType(row)
        End Function
    End Class
End Namespace
