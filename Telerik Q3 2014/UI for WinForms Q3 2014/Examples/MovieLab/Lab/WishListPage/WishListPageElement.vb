Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Drawing
Imports System.Windows.Forms
Imports Telerik.WinControls
Imports Telerik.WinControls.UI

Namespace MovieLabVB.Lab
    Public Class WishListPageElement
        Inherits PageElement
#Region "Fields"

        Private backdrops_Renamed As BackdropsView
        Private grid_Renamed As RadGridView
        Private poster_Renamed As BackdropItem
        Private frame As LightVisualElement
        Private hostItem As RadHostItem
        Private title As LightVisualElement
        Private fontName As Font
        Private fontYear As Font
        Private fontRating As Font
        Private fontGenres As Font

#End Region

#Region "Initialization"

        Public Sub New()
            Me.frame.Image = Resources.pictureHolder
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()

            Me.StretchHorizontally = True
            Me.StretchVertically = True
            Me.FitInAvailableSize = True

            fontName = New Font("Segoe UI", 9, FontStyle.Regular)
            fontYear = New Font("Segoe UI", 9, FontStyle.Regular)
            fontRating = New Font("Segoe UI", 6, FontStyle.Regular)
            fontGenres = New Font("Segoe UI", 9, FontStyle.Italic)
        End Sub

        Protected Overrides Sub OnLoaded()
            MyBase.OnLoaded()

            If grid_Renamed.DataSource Is Nothing Then
                grid_Renamed.DataSource = (CType(ElementTree.Control, MovieControl)).WishList

                Dim column As GridViewTextBoxColumn = TryCast(grid_Renamed.Columns("Genres"), GridViewTextBoxColumn)
                column.Width = 200
                column.Multiline = True
                column.WrapText = True

                grid_Renamed.Columns("Name").Width = 200
                grid_Renamed.Columns("Year").Width = 60
                grid_Renamed.Columns("ID").IsVisible = False
                grid_Renamed.Columns("Poster").IsVisible = False
                Dim col As New GridViewDecimalColumn("rowID")
                col.Width = 30
                grid_Renamed.Columns.Insert(0, col)
                grid_Renamed.Columns.Add(New GridViewCommandColumn("Command"))
                grid_Renamed.Columns.Remove(Me.grid_Renamed.Columns("Rating"))
                grid_Renamed.Columns.Insert(5, New RatingColumn())
            End If
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            backdrops_Renamed = New BackdropsView()
            Me.Children.Add(backdrops_Renamed)

            title = New LightVisualElement()
            title.Text = "wish list"
            title.Font = MovieControl.SegoeUI21Italic
            ForeColor = Color.FromArgb(51, 51, 64)
            title.ForeColor = ForeColor
            title.StretchHorizontally = True
            title.StretchVertically = False
            Me.Children.Add(title)

            grid_Renamed = New RadGridView()
            grid_Renamed.TableElement.RowScroller.ScrollState = ScrollState.AlwaysHide
            grid_Renamed.Size = New System.Drawing.Size(690, 240)
            grid_Renamed.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill
            grid_Renamed.ReadOnly = True
            grid_Renamed.AutoSizeRows = True
            grid_Renamed.EnableGrouping = False
            grid_Renamed.AllowAddNewRow = False
            grid_Renamed.MasterView.TableHeaderRow.IsVisible = False
            AddHandler grid_Renamed.ContextMenuOpening, AddressOf grid_ContextMenuOpening
            AddHandler grid_Renamed.CommandCellClick, AddressOf grid_CommandCellClick
            AddHandler grid_Renamed.CellFormatting, AddressOf grid_CellFormatting
            AddHandler grid_Renamed.CurrentRowChanged, AddressOf grid_CurrentRowChanged

            hostItem = New RadHostItem(grid_Renamed)
            hostItem.StretchHorizontally = False
            hostItem.StretchVertically = False
            Me.Children.Add(hostItem)

            poster_Renamed = New BackdropItem(Nothing, 190, 280)
            poster_Renamed.Alignment = ContentAlignment.MiddleRight
            poster_Renamed.ImageAlignment = ContentAlignment.MiddleRight
            Me.Children.Add(poster_Renamed)

            frame = New LightVisualElement()
            Me.Children.Add(frame)
        End Sub

        Protected Overrides Sub DisposeManagedResources()
            RemoveHandler grid_Renamed.ContextMenuOpening, AddressOf grid_ContextMenuOpening
            RemoveHandler grid_Renamed.CommandCellClick, AddressOf grid_CommandCellClick
            RemoveHandler grid_Renamed.CellFormatting, AddressOf grid_CellFormatting

            MyBase.DisposeManagedResources()
        End Sub

#End Region

#Region "Properties"

        Public ReadOnly Property Backdrops() As BackdropsView
            Get
                Return backdrops_Renamed
            End Get
        End Property
        Public ReadOnly Property Grid() As RadGridView
            Get
                Return grid_Renamed
            End Get
        End Property
        Public ReadOnly Property Poster() As BackdropItem
            Get
                Return poster_Renamed
            End Get
        End Property

#End Region

#Region "Event Handlers"

        Private Sub grid_ContextMenuOpening(ByVal sender As Object, ByVal e As ContextMenuOpeningEventArgs)
            e.Cancel = True
        End Sub

        Private Sub grid_CommandCellClick(ByVal sender As Object, ByVal e As EventArgs)
            CType(ElementTree.Control, MovieControl).WishList.Remove(CType(grid_Renamed.CurrentRow.DataBoundItem, WishListMovie))
        End Sub

        Private Sub grid_CellFormatting(ByVal sender As Object, ByVal e As CellFormattingEventArgs)
            Select Case e.CellElement.ColumnInfo.Name
                Case "rowID"
                    e.CellElement.ForeColor = Color.FromArgb(204, 255, 0)
                    e.CellElement.Text = (e.RowIndex + 1).ToString() & "."
                Case "Name"
                    e.CellElement.Font = fontName
                    e.CellElement.ForeColor = Color.FromArgb(156, 156, 156)
                    e.CellElement.ClipDrawing = True
                Case "Year"
                    e.CellElement.Font = fontYear
                    e.CellElement.ForeColor = Color.FromArgb(107, 107, 121)
                Case "Genres"
                    e.CellElement.Font = fontGenres
                    e.CellElement.ForeColor = Color.FromArgb(156, 156, 156)
                Case "Command"
                    Dim commandCell As GridCommandCellElement = TryCast(e.CellElement, GridCommandCellElement)
                    commandCell.CommandButton.Image = Resources.removeFromWishlist
                    commandCell.CommandButton.BorderElement.Visibility = ElementVisibility.Hidden
                    commandCell.CommandButton.ButtonFillElement.Visibility = ElementVisibility.Hidden
                Case Else
            End Select
        End Sub

        Private Sub grid_CurrentRowChanged(ByVal sender As Object, ByVal e As CurrentRowChangedEventArgs)
            If e.CurrentRow Is Nothing Then
                Me.poster_Renamed.Image = Nothing
            Else
                Dim movie As WishListMovie = CType(e.CurrentRow.DataBoundItem, WishListMovie)
                Me.poster_Renamed.SetImage(movie.Poster)
                Me.backdrops_Renamed.Title.Title.Text = movie.Name
                Me.backdrops_Renamed.Title.Year.Text = movie.Year
                Me.backdrops_Renamed.Title.Rating.Rating = movie.Rating
            End If
        End Sub

#End Region

#Region "Layout"

        Protected Overrides Function ArrangeOverride(ByVal finalSize As SizeF) As SizeF
            Dim clientRect As RectangleF = GetClientRectangle(finalSize)
            Dim x As Single = clientRect.X
            Dim y As Single = clientRect.Y

            title.Arrange(New RectangleF(x, y, clientRect.Width, title.DesiredSize.Height))
            y += title.DesiredSize.Height + 5

            hostItem.Arrange(New RectangleF(x + 64, y, hostItem.DesiredSize.Width, hostItem.DesiredSize.Height))

            x = clientRect.Right - 64 - frame.DefaultSize.Width
            poster_Renamed.Arrange(New RectangleF(x, y, poster_Renamed.DesiredSize.Width, poster_Renamed.DesiredSize.Height))
            frame.Arrange(New RectangleF(x - 203, y - 13, frame.Image.Width, frame.Image.Height))

            Return finalSize
        End Function

#End Region
    End Class
End Namespace