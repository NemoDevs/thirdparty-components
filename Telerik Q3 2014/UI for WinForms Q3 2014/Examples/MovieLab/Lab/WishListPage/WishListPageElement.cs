﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using MovieLab.Properties;

namespace MovieLab.Lab
{
    public class WishListPageElement : PageElement
    {
        #region Fields

        private BackdropsView backdrops;
        private RadGridView grid;
        private BackdropItem poster;
        private LightVisualElement frame;
        private RadHostItem hostItem;
        private LightVisualElement title;
        private Font fontName;
        private Font fontYear;
        private Font fontRating;
        private Font fontGenres;

        #endregion

        #region Initialization

        public WishListPageElement()
        {
            this.frame.Image = Resources.pictureHolder;
        }

        protected override void InitializeFields()
        {
            base.InitializeFields();

            this.StretchHorizontally = true;
            this.StretchVertically = true;
            this.FitInAvailableSize = true;

            fontName = new Font("Segoe UI", 9, FontStyle.Regular);
            fontYear = new Font("Segoe UI", 9, FontStyle.Regular);
            fontRating = new Font("Segoe UI", 6, FontStyle.Regular);
            fontGenres = new Font("Segoe UI", 9, FontStyle.Italic);
        }

        protected override void OnLoaded()
        {
            base.OnLoaded();

            if (grid.DataSource == null)
            {
                grid.DataSource = ((MovieControl)ElementTree.Control).WishList;

                GridViewTextBoxColumn column = grid.Columns["Genres"] as GridViewTextBoxColumn;
                column.Width = 200;
                column.Multiline = true;
                column.WrapText = true;

                grid.Columns["Name"].Width = 200;
                grid.Columns["Year"].Width = 60;
                grid.Columns["ID"].IsVisible = false;
                grid.Columns["Poster"].IsVisible = false;
                grid.Columns.Insert(0, new GridViewDecimalColumn("rowID") { Width = 30 });
                grid.Columns.Add(new GridViewCommandColumn("Command"));
                grid.Columns.Remove(this.grid.Columns["Rating"]);
                grid.Columns.Insert(5, new RatingColumn());
            }
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            backdrops = new BackdropsView();
            this.Children.Add(backdrops);

            title = new LightVisualElement();
            title.Text = "wish list";
            title.Font = MovieControl.SegoeUI21Italic;
            title.ForeColor = ForeColor = Color.FromArgb(51, 51, 64);
            title.StretchHorizontally = true;
            title.StretchVertically = false;
            this.Children.Add(title);

            grid = new RadGridView();
            grid.TableElement.RowScroller.ScrollState = ScrollState.AlwaysHide;            
            grid.Size = new System.Drawing.Size(690, 240);
            grid.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
            grid.ReadOnly = true;
            grid.AutoSizeRows = true;
            grid.EnableGrouping = false;
            grid.AllowAddNewRow = false;
            grid.MasterView.TableHeaderRow.IsVisible = false;
            grid.ContextMenuOpening += new ContextMenuOpeningEventHandler(grid_ContextMenuOpening);
            grid.CommandCellClick += new CommandCellClickEventHandler(grid_CommandCellClick);
            grid.CellFormatting += new CellFormattingEventHandler(grid_CellFormatting);
            grid.CurrentRowChanged += new CurrentRowChangedEventHandler(grid_CurrentRowChanged);

            hostItem = new RadHostItem(grid);
            hostItem.StretchHorizontally = false;
            hostItem.StretchVertically = false;
            this.Children.Add(hostItem);

            poster = new BackdropItem(null, 190, 280);
            poster.Alignment = ContentAlignment.MiddleRight;
            poster.ImageAlignment = ContentAlignment.MiddleRight;
            this.Children.Add(poster);

            frame = new LightVisualElement();
            this.Children.Add(frame);
        }

        protected override void DisposeManagedResources()
        {
            grid.ContextMenuOpening -= new ContextMenuOpeningEventHandler(grid_ContextMenuOpening);
            grid.CommandCellClick -= new CommandCellClickEventHandler(grid_CommandCellClick);
            grid.CellFormatting -= new CellFormattingEventHandler(grid_CellFormatting);

            base.DisposeManagedResources();
        }

        #endregion

        #region Properties

        public BackdropsView Backdrops { get { return backdrops; } }
        public RadGridView Grid { get { return grid; } }
        public BackdropItem Poster { get { return poster; } }

        #endregion

        #region Event Handlers

        private void grid_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e)
        {
            e.Cancel = true;
        }

        private void grid_CommandCellClick(object sender, EventArgs e)
        {
            ((MovieControl)ElementTree.Control).WishList.Remove((WishListMovie)grid.CurrentRow.DataBoundItem);
        }

        private void grid_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            switch (e.CellElement.ColumnInfo.Name)
            {
                case "rowID":
                    e.CellElement.ForeColor = Color.FromArgb(204, 255, 0);
                    e.CellElement.Text = (e.RowIndex + 1).ToString() + ".";
                    break;
                case "Name":
                    e.CellElement.Font = fontName;
                    e.CellElement.ForeColor = Color.FromArgb(156, 156, 156);
                    e.CellElement.ClipDrawing = true;
                    break;
                case "Year":
                    e.CellElement.Font = fontYear;
                    e.CellElement.ForeColor = Color.FromArgb(107, 107, 121);
                    break;
                case "Genres":
                    e.CellElement.Font = fontGenres;
                    e.CellElement.ForeColor = Color.FromArgb(156, 156, 156);
                    break;
                case "Command":
                    GridCommandCellElement commandCell = e.CellElement as GridCommandCellElement;
                    commandCell.CommandButton.Image = Resources.removeFromWishlist;
                    commandCell.CommandButton.BorderElement.Visibility = ElementVisibility.Hidden;
                    commandCell.CommandButton.ButtonFillElement.Visibility = ElementVisibility.Hidden;
                    break;
                default:
                    break;
            }
        }

        void grid_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            if (e.CurrentRow == null)
            {
                this.poster.Image = null;
            }
            else
            {
                WishListMovie movie = (WishListMovie)e.CurrentRow.DataBoundItem;
                this.poster.SetImage(movie.Poster);
                this.backdrops.Title.Title.Text = movie.Name;
                this.backdrops.Title.Year.Text = movie.Year;
                this.backdrops.Title.Rating.Rating = movie.Rating;
            }
        }

        #endregion
        
        #region Layout

        protected override SizeF ArrangeOverride(SizeF finalSize)
        {
            RectangleF clientRect = GetClientRectangle(finalSize);
            float x = clientRect.X;
            float y = clientRect.Y;

            title.Arrange(new RectangleF(x, y, clientRect.Width, title.DesiredSize.Height));
            y += title.DesiredSize.Height + 20;

            hostItem.Arrange(new RectangleF(x + 64, y, hostItem.DesiredSize.Width, hostItem.DesiredSize.Height));

            x = clientRect.Right - 64 - frame.DefaultSize.Width;
            poster.Arrange(new RectangleF(x - 192, y, poster.DesiredSize.Width, poster.DesiredSize.Height));
            frame.Arrange(new RectangleF(x - 203, y - 13, frame.Image.Width, frame.Image.Height));

            return finalSize;
        }
        
        #endregion
    }
}