Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Drawing

Namespace MovieLabVB.Lab
    Public Class WishListMovie
        Public Property ID() As Integer
            Get
                Return m_ID
            End Get
            Set(value As Integer)
                m_ID = Value
            End Set
        End Property
        Private m_ID As Integer
        Public Property Poster() As Image
            Get
                Return m_Poster
            End Get
            Set(value As Image)
                m_Poster = Value
            End Set
        End Property
        Private m_Poster As Image
        Public Property Name() As String
            Get
                Return m_Name
            End Get
            Set(value As String)
                m_Name = Value
            End Set
        End Property
        Private m_Name As String
        Public Property Year() As String
            Get
                Return m_Year
            End Get
            Set(value As String)
                m_Year = Value
            End Set
        End Property
        Private m_Year As String
        Public Property Rating() As Integer
            Get
                Return m_Rating
            End Get
            Set(value As Integer)
                m_Rating = Value
            End Set
        End Property
        Private m_Rating As Integer
        Public Property Genres() As String
            Get
                Return m_Genres
            End Get
            Set(value As String)
                m_Genres = Value
            End Set
        End Property
        Private m_Genres As String
    End Class
End Namespace