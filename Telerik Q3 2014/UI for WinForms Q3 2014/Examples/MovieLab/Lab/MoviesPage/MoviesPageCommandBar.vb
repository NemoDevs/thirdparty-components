Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports System.Windows.Forms
Imports Telerik.WinControls

Namespace MovieLabVB.Lab
    Public Class MoviesPageCommandBar
        Inherits StackLayoutElement
        Private dropDownSortBy_Renamed As RadDropDownListElement
        Private buttonFilterAll_Renamed As MovieButtonElement
        Private buttonFilterByNumber_Renamed As MovieButtonElement
        Private buttonFilterAI As MovieButtonElement
        Private buttonFilterJR As MovieButtonElement
        Private buttonFilterSZ As MovieButtonElement
        Private selectedButton_Renamed As MovieButtonElement

        Public ReadOnly Property DropDownSortBy() As RadDropDownListElement
            Get
                Return dropDownSortBy_Renamed
            End Get
        End Property
        Public ReadOnly Property ButtonFilterAll() As MovieButtonElement
            Get
                Return buttonFilterAll_Renamed
            End Get
        End Property
        Public ReadOnly Property ButtonFilterByNumber() As MovieButtonElement
            Get
                Return buttonFilterByNumber_Renamed
            End Get
        End Property
        Public ReadOnly Property ButtonFilterByAI() As MovieButtonElement
            Get
                Return buttonFilterAI
            End Get
        End Property
        Public ReadOnly Property ButtonFilterByJR() As MovieButtonElement
            Get
                Return buttonFilterJR
            End Get
        End Property
        Public ReadOnly Property ButtonFilterBySZ() As MovieButtonElement
            Get
                Return buttonFilterSZ
            End Get
        End Property

        Public Property SelectedButton() As MovieButtonElement
            Get
                Return selectedButton_Renamed
            End Get
            Set(value As MovieButtonElement)
                If Not selectedButton_Renamed Is Value Then
                    If Not selectedButton_Renamed Is Nothing Then
                        selectedButton_Renamed.State = MovieButtonElement.ButtonStates.Normal
                    End If
                    selectedButton_Renamed = Value
                    selectedButton_Renamed.State = MovieButtonElement.ButtonStates.Selected
                End If
            End Set
        End Property

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()

            Me.StretchHorizontally = True
            Me.StretchVertically = False
            Me.FitInAvailableSize = True
            Me.Orientation = System.Windows.Forms.Orientation.Horizontal
            Me.ElementSpacing = 0
            Me.DrawFill = True
            Me.GradientStyle = Telerik.WinControls.GradientStyles.Solid
            Me.BackColor = Color.FromArgb(36, 36, 47)
            Me.DrawBorder = True
            Me.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders
            Me.BorderLeftWidth = 0
            Me.BorderRightWidth = 0
            Me.BorderTopWidth = 0
            Me.BorderBottomWidth = 1
            Me.BorderBottomColor = Color.FromArgb(11, 11, 14)
            Me.BorderBottomShadowColor = Color.FromArgb(58, 58, 75)
            Me.ClipDrawing = True
            Me.Margin = New Padding(0, 0, 1, 0)
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            Dim label As LightVisualElement = New LightVisualElement()
            label.Text = "SORT BY"
            label.StretchHorizontally = False
            label.ForeColor = Color.FromArgb(111, 112, 125)
            label.Font = MovieControl.SegoeUI7
            label.Margin = New Padding(86, 4, 0, 4)
            Me.Children.Add(label)

            dropDownSortBy_Renamed = New RadDropDownListElement()
            dropDownSortBy_Renamed.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            dropDownSortBy_Renamed.DataSource = System.Enum.GetNames(GetType(MoviesPageElement.SortModes))
            dropDownSortBy_Renamed.MinSize = New System.Drawing.Size(190, 20)
            dropDownSortBy_Renamed.Margin = New System.Windows.Forms.Padding(26, 6, 0, 6)
            dropDownSortBy_Renamed.StretchHorizontally = False
            dropDownSortBy_Renamed.ItemHeight = 21
            dropDownSortBy_Renamed.SelectedIndex = 0
            AddHandler dropDownSortBy_Renamed.SelectedIndexChanged, AddressOf dropDownSortBy_SelectedIndexChanged
            Me.Children.Add(dropDownSortBy_Renamed)

            Dim separator1 As LightVisualElement = New LightVisualElement()
            separator1.StretchHorizontally = False
            separator1.StretchVertically = True
            separator1.MinSize = New Size(2, 0)
            separator1.DrawBorder = True
            separator1.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders
            separator1.BorderLeftWidth = 0
            separator1.BorderRightWidth = 1
            separator1.BorderTopWidth = 0
            separator1.BorderBottomWidth = 0
            separator1.BorderRightColor = Color.FromArgb(58, 58, 75)
            separator1.BorderRightShadowColor = Color.FromArgb(0, 0, 0)
            separator1.Margin = New Padding(8, 0, 0, 0)
            Me.Children.Add(separator1)

            Dim behavior As ForeColorBehavior = New ForeColorBehavior()

            buttonFilterAll_Renamed = New MovieButtonElement()
            buttonFilterAll_Renamed.Normal = Resources.buttonFilterNormal
            buttonFilterAll_Renamed.Hovered = Resources.buttonFilterHover
            buttonFilterAll_Renamed.Down = Resources.buttonFilterHover
            buttonFilterAll_Renamed.Selected = Resources.buttonFilterSelected
            buttonFilterAll_Renamed.Text = "All"
            buttonFilterAll_Renamed.ForeColor = Color.FromArgb(204, 255, 0)
            buttonFilterAll_Renamed.Font = MovieControl.SegoeUI9
            buttonFilterAll_Renamed.Margin = New Padding(6, 3, 0, 0)
            buttonFilterAll_Renamed.AddBehavior(behavior)
            AddHandler buttonFilterAll_Renamed.Click, AddressOf buttonFilterAll_Click
            Me.Children.Add(buttonFilterAll_Renamed)

            buttonFilterByNumber_Renamed = New MovieButtonElement()
            buttonFilterByNumber_Renamed.Normal = Resources.buttonFilterNormal
            buttonFilterByNumber_Renamed.Hovered = Resources.buttonFilterHover
            buttonFilterByNumber_Renamed.Down = Resources.buttonFilterHover
            buttonFilterByNumber_Renamed.Selected = Resources.buttonFilterSelected
            buttonFilterByNumber_Renamed.Text = "#"
            buttonFilterByNumber_Renamed.ForeColor = Color.FromArgb(204, 255, 0)
            buttonFilterByNumber_Renamed.Font = MovieControl.SegoeUI9
            buttonFilterByNumber_Renamed.Margin = New Padding(0, 3, 0, 0)
            buttonFilterByNumber_Renamed.AddBehavior(behavior)
            AddHandler buttonFilterByNumber_Renamed.Click, AddressOf buttonFilterByNumber_Click
            Me.Children.Add(buttonFilterByNumber_Renamed)

            buttonFilterAI = New MovieButtonElement()
            buttonFilterAI.Normal = Resources.buttonFilterNormal
            buttonFilterAI.Hovered = Resources.buttonFilterHover
            buttonFilterAI.Down = Resources.buttonFilterHover
            buttonFilterAI.Selected = Resources.buttonFilterSelected
            buttonFilterAI.Text = "A-I"
            buttonFilterAI.ForeColor = Color.FromArgb(204, 255, 0)
            buttonFilterAI.Font = MovieControl.SegoeUI9
            buttonFilterAI.Margin = New Padding(0, 3, 0, 0)
            buttonFilterAI.AddBehavior(behavior)
            AddHandler buttonFilterAI.Click, AddressOf buttonFilterAI_Click
            Me.Children.Add(buttonFilterAI)

            buttonFilterJR = New MovieButtonElement()
            buttonFilterJR.Normal = Resources.buttonFilterNormal
            buttonFilterJR.Hovered = Resources.buttonFilterHover
            buttonFilterJR.Down = Resources.buttonFilterHover
            buttonFilterJR.Selected = Resources.buttonFilterSelected
            buttonFilterJR.Text = "J-R"
            buttonFilterJR.ForeColor = Color.FromArgb(204, 255, 0)
            buttonFilterJR.Font = MovieControl.SegoeUI9
            buttonFilterJR.Margin = New Padding(0, 3, 0, 0)
            buttonFilterJR.AddBehavior(behavior)
            AddHandler buttonFilterJR.Click, AddressOf buttonFilterJR_Click
            Me.Children.Add(buttonFilterJR)

            buttonFilterSZ = New MovieButtonElement()
            buttonFilterSZ.Normal = Resources.buttonFilterNormal
            buttonFilterSZ.Hovered = Resources.buttonFilterHover
            buttonFilterSZ.Down = Resources.buttonFilterHover
            buttonFilterSZ.Selected = Resources.buttonFilterSelected
            buttonFilterSZ.Text = "S-Z"
            buttonFilterSZ.ForeColor = Color.FromArgb(204, 255, 0)
            buttonFilterSZ.Font = MovieControl.SegoeUI9
            buttonFilterSZ.Margin = New Padding(0, 3, 0, 0)
            buttonFilterSZ.AddBehavior(behavior)
            AddHandler buttonFilterSZ.Click, AddressOf buttonFilterSZ_Click
            Me.Children.Add(buttonFilterSZ)

            Dim separator2 As LightVisualElement = New LightVisualElement()
            separator2.StretchHorizontally = False
            separator2.StretchVertically = True
            separator2.MinSize = New Size(2, 0)
            separator2.DrawBorder = True
            separator2.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders
            separator2.BorderLeftWidth = 0
            separator2.BorderRightWidth = 1
            separator2.BorderTopWidth = 0
            separator2.BorderBottomWidth = 0
            separator2.BorderRightColor = Color.FromArgb(58, 58, 75)
            separator2.BorderRightShadowColor = Color.FromArgb(0, 0, 0)
            separator2.Margin = New Padding(110, 0, 0, 0)
            Me.Children.Add(separator2)
        End Sub

#Region "Event handlers"

        Protected Overrides Sub OnLoaded()
            MyBase.OnLoaded()
            If Me.SelectedButton Is Nothing Then
                Me.SelectedButton = ButtonFilterAll
            End If
        End Sub

        Private Sub buttonFilterAll_Click(ByVal sender As Object, ByVal e As EventArgs)
            FindAncestor(Of MoviesPageElement)().FilterMode = MoviesPageElement.FilterModes.None
            SelectedButton = buttonFilterAll_Renamed
        End Sub

        Private Sub buttonFilterSZ_Click(ByVal sender As Object, ByVal e As EventArgs)
            FindAncestor(Of MoviesPageElement)().FilterMode = MoviesPageElement.FilterModes.SZ
            SelectedButton = buttonFilterSZ
        End Sub

        Private Sub buttonFilterJR_Click(ByVal sender As Object, ByVal e As EventArgs)
            FindAncestor(Of MoviesPageElement)().FilterMode = MoviesPageElement.FilterModes.JR
            SelectedButton = buttonFilterJR
        End Sub

        Private Sub buttonFilterAI_Click(ByVal sender As Object, ByVal e As EventArgs)
            FindAncestor(Of MoviesPageElement)().FilterMode = MoviesPageElement.FilterModes.AI
            SelectedButton = buttonFilterAI
        End Sub

        Private Sub buttonFilterByNumber_Click(ByVal sender As Object, ByVal e As EventArgs)
            FindAncestor(Of MoviesPageElement)().FilterMode = MoviesPageElement.FilterModes.Number
            SelectedButton = buttonFilterByNumber_Renamed
        End Sub

        Private Sub dropDownSortBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.WinControls.UI.Data.PositionChangedEventArgs)
            If dropDownSortBy_Renamed.SelectedValue Is Nothing Then
                Return
            End If
            FindAncestor(Of MoviesPageElement)().SortMode = CType(System.Enum.Parse(GetType(MoviesPageElement.SortModes), dropDownSortBy_Renamed.SelectedValue.ToString()), MoviesPageElement.SortModes)
        End Sub

#End Region
    End Class

    Public Class ForeColorBehavior
        Inherits PropertyChangeBehavior
        Public Sub New()
            MyBase.New(LightVisualElement.IsMouseOverProperty)
        End Sub

        Public Overrides Sub OnPropertyChange(ByVal element As RadElement, ByVal e As RadPropertyChangedEventArgs)
            MyBase.OnPropertyChange(element, e)
            If CBool(e.NewValue) AndAlso Not (CType(element, MovieButtonElement)).State = MovieButtonElement.ButtonStates.Selected Then
                CType(element, LightVisualElement).ForeColor = Color.Black
            Else
                element.ResetValue(LightVisualElement.ForeColorProperty, ValueResetFlags.Local)
            End If
        End Sub
    End Class
End Namespace
