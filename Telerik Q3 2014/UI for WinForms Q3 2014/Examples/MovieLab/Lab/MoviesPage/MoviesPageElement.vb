Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports Telerik.WinControls
Imports Telerik.WinControls.UI
Imports TheMovieDB
Imports System.Collections.Generic

Namespace MovieLabVB.Lab
    Public Class MoviesPageElement
        Inherits PageElement
#Region "Fields"

        Public Enum FilterModes
            None
            Number
            AI
            JR
            SZ
        End Enum
        Public Enum SortModes
            None
            Title
            Rating
            Year
        End Enum

        Private topMovies As List(Of TmdbMovie)
        Private newMovies As List(Of TmdbMovie)
        Private currentList As List(Of TmdbMovie)

        Private backdropsView_Renamed As BackdropsView
        Private commandBar_Renamed As MoviesPageCommandBar
        Private iconView_Renamed As IconView
        Private buttonIcons_Renamed As MovieButtonElement
        Private buttonList_Renamed As MovieButtonElement

        Private filterMode_Renamed As FilterModes = FilterModes.None
        Private sortMode_Renamed As SortModes = SortModes.Title
        Private grid As Boolean
        Private initialized As Boolean

#End Region

#Region "Initialization"

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()

            Me.StretchHorizontally = True
            Me.StretchVertically = True
            Me.Orientation = System.Windows.Forms.Orientation.Vertical
            Me.FitInAvailableSize = True
            Me.Margin = New Padding(1, 1, 1, 1)

            Me.ForeColor = Color.FromArgb(107, 107, 121)
            Me.Font = MovieControl.SegoeUI9
            Me.TextAlignment = ContentAlignment.TopLeft

            Me.grid = True
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            Me.Children.Add(New GradientBorderElement(0, 1, False))

            backdropsView_Renamed = New BackdropsView()
            Me.Children.Add(backdropsView_Renamed)

            Dim border As GradientBorderElement = New GradientBorderElement(0, 1, False)
            border.Margin = New Padding(0, -2, 0, 0)
            Me.Children.Add(border)

            Dim title As LightVisualElement = New LightVisualElement()
            title.BackgroundImage = Resources.shadowBackground
            title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Tile
            title.StretchVertically = False
            title.StretchHorizontally = True
            title.MinSize = New Size(0, title.BackgroundImage.Height)
            title.Image = Resources.our_movies
            title.ImageAlignment = ContentAlignment.BottomCenter
            Me.Children.Add(title)

            commandBar_Renamed = New MoviesPageCommandBar()
            Me.Children.Add(commandBar_Renamed)

            Dim horizontalStack As StackLayoutElement = New StackLayoutElement()
            horizontalStack.FitInAvailableSize = True
            horizontalStack.StretchHorizontally = True
            horizontalStack.StretchVertically = True
            horizontalStack.Padding = New Padding(0, 0, 0, 2)
            Me.Children.Add(horizontalStack)

            Dim verticalStack As StackLayoutElement = New StackLayoutElement()
            verticalStack.StretchVertically = True
            verticalStack.StretchHorizontally = False
            verticalStack.Orientation = System.Windows.Forms.Orientation.Vertical
            verticalStack.MinSize = New System.Drawing.Size(82, 0)
            verticalStack.Margin = New System.Windows.Forms.Padding(0, 8, 0, 0)
            verticalStack.ElementSpacing = 8
            horizontalStack.Children.Add(verticalStack)

            iconView_Renamed = New IconView()
            horizontalStack.Children.Add(iconView_Renamed)

            buttonIcons_Renamed = New MovieButtonElement()
            buttonIcons_Renamed.Normal = Resources.buttonGridNormal
            buttonIcons_Renamed.Hovered = Resources.buttonGridHover
            buttonIcons_Renamed.Down = Resources.buttonGridDown
            buttonIcons_Renamed.Selected = Resources.buttonGridDown
            buttonIcons_Renamed.Margin = New System.Windows.Forms.Padding(20, 0, 0, 0)
            AddHandler buttonIcons_Renamed.Click, AddressOf buttonIcons_Click
            buttonIcons_Renamed.State = MovieButtonElement.ButtonStates.Selected
            verticalStack.Children.Add(buttonIcons_Renamed)

            buttonList_Renamed = New MovieButtonElement()
            buttonList_Renamed.Normal = Resources.buttonListNormal
            buttonList_Renamed.Hovered = Resources.buttonListHover
            buttonList_Renamed.Down = Resources.buttonListDown
            buttonList_Renamed.Selected = Resources.buttonListDown
            buttonList_Renamed.Margin = New System.Windows.Forms.Padding(20, 0, 0, 0)
            AddHandler buttonList_Renamed.Click, AddressOf buttonList_Click
            verticalStack.Children.Add(buttonList_Renamed)
        End Sub

#End Region

#Region "Properties"

        Public ReadOnly Property BackdropsView() As BackdropsView
            Get
                Return backdropsView_Renamed
            End Get
        End Property
        Public ReadOnly Property CommandBar() As MoviesPageCommandBar
            Get
                Return commandBar_Renamed
            End Get
        End Property
        Public ReadOnly Property IconView() As IconView
            Get
                Return iconView_Renamed
            End Get
        End Property
        Public ReadOnly Property ButtonIcons() As MovieButtonElement
            Get
                Return buttonIcons_Renamed
            End Get
        End Property
        Public ReadOnly Property ButtonList() As MovieButtonElement
            Get
                Return buttonList_Renamed
            End Get
        End Property

        Public Property FilterMode() As FilterModes
            Get
                Return filterMode_Renamed
            End Get
            Set(value As FilterModes)
                If Me.filterMode_Renamed <> Value Then
                    Me.filterMode_Renamed = Value
                    UpdateItems()
                End If
            End Set
        End Property

        Public Property SortMode() As SortModes
            Get
                Return sortMode_Renamed
            End Get
            Set(value As SortModes)
                If Me.sortMode_Renamed <> Value Then
                    Me.sortMode_Renamed = Value
                    UpdateItems()
                End If
            End Set
        End Property

#End Region

#Region "Event Handlers"

        Private Sub buttonList_Click(ByVal sender As Object, ByVal e As EventArgs)
            If currentList Is Nothing Then
                Return
            End If
            If grid Then
                grid = False
                buttonIcons_Renamed.State = MovieButtonElement.ButtonStates.Normal
                buttonList_Renamed.State = MovieButtonElement.ButtonStates.Selected
                UpdateItems()
            End If
        End Sub

        Private Sub buttonIcons_Click(ByVal sender As Object, ByVal e As EventArgs)
            If currentList Is Nothing Then
                Return
            End If
            If (Not grid) Then
                grid = True
                buttonIcons_Renamed.State = MovieButtonElement.ButtonStates.Selected
                buttonList_Renamed.State = MovieButtonElement.ButtonStates.Normal
                UpdateItems()
            End If
        End Sub

        Private Sub tmdbAPI_MovieSearchCompleted(ByVal sender As Object, ByVal e As ImdbMovieSearchCompletedEventArgs)
            If e.Movies Is Nothing Then
                Return
            End If
            Me.IconView.Image = Nothing

            SyncLock Me
                Dim items As List(Of TmdbMovie) = TryCast(e.UserState, List(Of TmdbMovie))
                If items Is Nothing Then
                    Return
                End If
                items.Clear()

                Try
                    For Each movie As TmdbMovie In e.Movies
                        items.Add(movie)
                        AddHandler movie.MovieImagesLoadCompleted, AddressOf movie_MovieImagesLoadCompleted
                        movie.LoadImagesAsync(movie, True)
                        Root.API.GetMovieInfoAsync(movie.Id, movie)
                    Next movie
                Catch
                End Try
                currentList = items

                Dim control As MovieControl = TryCast(Me.ElementTree.Control, MovieControl)
                control.Invoke(New MethodInvoker(AddressOf AnonymousMethod1))
            End SyncLock
        End Sub
        Private Sub AnonymousMethod1()
            UpdateItems()
        End Sub

        Private Sub tmdbAPI_GetMovieInfoCompleted(ByVal sender As Object, ByVal e As ImdbMovieInfoCompletedEventArgs)
            If e.Movie Is Nothing Then
                Return
            End If
            Dim movie As TmdbMovie = TryCast(e.UserState, TmdbMovie)
            If Not movie Is Nothing Then
                Dim control As MovieControl = TryCast(Me.ElementTree.Control, MovieControl)
                control.Invoke(New MethodInvoker(Sub()
                                                     Dim movieElement As MovieThumbElement = Me.iconView_Renamed.FindMovieElement(movie)
                                                     If Not movieElement Is Nothing Then
                                                         movieElement.RatingElement.Rating = CInt(Fix(e.Movie.Rating))
                                                     End If
                                                 End Sub))
            End If
        End Sub

        Private Sub movie_MovieImagesLoadCompleted(ByVal sender As Object, ByVal e As ImdbMovieImagesLoadCompletedEventArgs)
            Try
                If IsInValidState(True) Then
                    Dim movie As TmdbMovie = e.Movie
                    RemoveHandler movie.MovieImagesLoadCompleted, AddressOf movie_MovieImagesLoadCompleted
                    Dim control As MovieControl = TryCast(Me.ElementTree.Control, MovieControl)
                    control.Invoke(New MethodInvoker(Sub()
                                                         Dim element As MovieThumbElement = Me.iconView_Renamed.FindMovieElement(movie)
                                                         Dim image As Image = movie.Poster
                                                         If Not element Is Nothing Then

                                                             If image Is Nothing Then
                                                                 element.ImageElement.SetImage(Resources.na_img)
                                                             Else
                                                                 element.ImageElement.SetImage(image)
                                                             End If
                                                         End If
                                                     End Sub))
                End If
            Catch
            End Try
        End Sub

        Protected Overrides Sub OnLoaded()
            MyBase.OnLoaded()
            If (Not initialized) Then
                initialized = True
                AddHandler Root.API.MovieSearchCompleted, AddressOf tmdbAPI_MovieSearchCompleted
            End If
        End Sub

#End Region

#Region "Methods"

        Public Sub LoadSearch(ByVal search As String)
            FindAncestor(Of MovieElement)().CategoriesView.Text = ""

            Me.IconView.Image = Resources.waiting_bar
            Me.iconView_Renamed.Items.Clear()
            Me.currentList = New List(Of TmdbMovie)()
            Root.API.MovieSearchAsync(search, currentList)
        End Sub

        Public Sub LoadCategories(ByVal genres As String)
            Me.IconView.Image = Resources.waiting_bar
            Me.iconView_Renamed.Items.Clear()

            currentList = New List(Of TmdbMovie)()
            Dim request As TmdbMovieBrowseRequest = New TmdbMovieBrowseRequest(genres)
            request.order_by = TmdbOrderBy.release
            request.order = TmdbOrderType.desc
            request.per_page = 50
            request.page = 0
            request.release_max = New DateTime(2012, 6, 1)
            request.release_min = New DateTime(1996, 1, 1)
            Root.API.MovieBrowseAsync(request, currentList)
        End Sub

        Public Sub LoadNew()
            FindAncestor(Of MovieElement)().CategoriesView.Text = ""

            Me.IconView.Image = Resources.waiting_bar
            Me.iconView_Renamed.Items.Clear()

            If newMovies Is Nothing Then
                newMovies = New List(Of TmdbMovie)()

                Dim now As DateTime = DateTime.Now
                Dim [to] As DateTime = New DateTime(now.Year, now.Month, now.Day)
                Dim From As DateTime = [to].AddMonths(-3)
                Dim request As TmdbMovieBrowseRequest = New TmdbMovieBrowseRequest(From, [to])
                request.order_by = TmdbOrderBy.release
                request.order = TmdbOrderType.desc
                request.per_page = 50
                request.page = 0
                Root.API.MovieBrowseAsync(request, newMovies)
            Else
                currentList = newMovies
                UpdateItems()
            End If
        End Sub

        Public Sub LoadTop()
            FindAncestor(Of MovieElement)().CategoriesView.Text = ""

            Me.IconView.Image = Resources.waiting_bar

            If topMovies Is Nothing Then
                topMovies = New List(Of TmdbMovie)()

                Dim now As DateTime = DateTime.Now
                Dim request As TmdbMovieBrowseRequest = New TmdbMovieBrowseRequest(8, 10)
                request.order_by = TmdbOrderBy.rating
                request.order = TmdbOrderType.desc
                request.release_min = New DateTime(now.Year - 1, now.Month, now.Day)
                request.release_max = New DateTime(now.Year, now.Month, now.Day)
                request.per_page = 50
                request.page = 0
                Root.API.MovieBrowseAsync(request, topMovies)
            Else
                currentList = topMovies
                UpdateItems()
            End If
        End Sub

        Public Sub UpdateItems()
            If currentList Is Nothing Then
                Return
            End If

            Me.IconView.Text = ""
            Me.IconView.TextAlignment = ContentAlignment.TopLeft
            iconView_Renamed.Padding = New System.Windows.Forms.Padding(0)
            Me.IconView.Image = Nothing

            Select Case SortMode
                Case SortModes.Title
                    currentList.Sort(New SortByTitleComparer())
                Case SortModes.Rating
                    currentList.Sort(New SortByRatingComparer())
                Case SortModes.Year
                    currentList.Sort(New SortByYearComparer())
            End Select

            iconView_Renamed.Items.Clear()
            iconView_Renamed.Items.BeginUpdate()

            For Each movie As TmdbMovie In currentList
                If filterMode_Renamed = FilterModes.AI AndAlso movie.Name.ToLower().TrimStart()(0) > "i"c Then
                    Continue For
                ElseIf filterMode_Renamed = FilterModes.JR AndAlso (movie.Name.ToLower().TrimStart()(0) < "j"c OrElse movie.Name.ToLower().TrimStart()(0) > "r"c) Then
                    Continue For
                ElseIf filterMode_Renamed = FilterModes.SZ AndAlso (movie.Name.ToLower().TrimStart()(0) < "s"c OrElse movie.Name.ToLower().TrimStart()(0) > "z"c) Then
                    Continue For
                ElseIf filterMode_Renamed = FilterModes.Number AndAlso (Not Char.IsNumber(movie.Name.TrimStart()(0))) Then
                    Continue For
                End If

                Dim item As New ListViewDataItem
                item.Tag = movie
                iconView_Renamed.Items.Add(item)
            Next movie

            iconView_Renamed.BigThumbs = Not grid

            iconView_Renamed.Items.EndUpdate()

            If iconView_Renamed.Items.Count = 0 Then
                Me.IconView.Text = "No results to display!"
                iconView_Renamed.Padding = New System.Windows.Forms.Padding(10)
            End If
        End Sub

#End Region

#Region "movie comparers"

        Public Class SortByTitleComparer
            Implements IComparer(Of TmdbMovie)
#Region "IComparer<TmdbMovie> Members"

            Public Function Compare(ByVal x As TmdbMovie, ByVal y As TmdbMovie) As Integer Implements System.Collections.Generic.IComparer(Of TheMovieDB.TmdbMovie).Compare

                Return String.Compare(x.Name, y.Name)
            End Function

#End Region
        End Class

        Public Class SortByRatingComparer
            Implements IComparer(Of TmdbMovie)
#Region "IComparer<TmdbMovie> Members"

            Public Function Compare(ByVal x As TmdbMovie, ByVal y As TmdbMovie) As Integer Implements System.Collections.Generic.IComparer(Of TheMovieDB.TmdbMovie).Compare

                If x.Rating < y.Rating Then
                    Return 1
                End If
                If x.Rating > y.Rating Then
                    Return -1
                End If
                Return 0
            End Function

#End Region
        End Class

        Public Class SortByYearComparer
            Implements IComparer(Of TmdbMovie)
#Region "IComparer<TmdbMovie> Members"

            Public Function Compare(ByVal x As TmdbMovie, ByVal y As TmdbMovie) As Integer Implements System.Collections.Generic.IComparer(Of TheMovieDB.TmdbMovie).Compare
                Dim year1 As Integer
                If x.Released.HasValue Then
                    year1 = x.Released.Value.Year
                Else
                    year1 = 0
                End If
                Dim year2 As Integer
                If y.Released.HasValue Then
                    year2 = y.Released.Value.Year
                Else
                    year2 = 0
                End If

                If year1 < year2 Then
                    Return 1
                End If
                If year1 > year2 Then
                    Return -1
                End If
                Return 0
            End Function

#End Region
        End Class

#End Region
    End Class
End Namespace
