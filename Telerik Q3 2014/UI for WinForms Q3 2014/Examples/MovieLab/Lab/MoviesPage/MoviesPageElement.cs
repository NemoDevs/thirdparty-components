﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using TheMovieDB;
using MovieLab.Properties;
using System.Collections.Generic;

namespace MovieLab.Lab
{
    public class MoviesPageElement : PageElement
    {
        #region Fields

        public enum FilterModes { None, Number, AI, JR, SZ };
        public enum SortModes { None, Title, Rating, Year };

        private List<TmdbMovie> topMovies;
        private List<TmdbMovie> newMovies;
        private List<TmdbMovie> currentList;

        private BackdropsView backdropsView;
        private MoviesPageCommandBar commandBar;
        private IconView iconView;
        private MovieButtonElement buttonIcons;
        private MovieButtonElement buttonList;

        private FilterModes filterMode = FilterModes.None;
        private SortModes sortMode = SortModes.Title;
        private bool grid;
        private bool initialized;

        #endregion

        #region Initialization

        protected override void InitializeFields()
        {
            base.InitializeFields();

            this.StretchHorizontally = true;
            this.StretchVertically = true;
            this.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.FitInAvailableSize = true;
            this.Margin = new Padding(1, 1, 1, 1);

            this.ForeColor = Color.FromArgb(107, 107, 121);
            this.Font = MovieControl.SegoeUI9;
            this.TextAlignment = ContentAlignment.TopLeft;

            this.grid = true;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            this.Children.Add(new GradientBorderElement(0, 1, false));

            backdropsView = new BackdropsView();
            this.Children.Add(backdropsView);

            GradientBorderElement border = new GradientBorderElement(0, 1, false);
            border.Margin = new Padding(0, -2, 0, 0);
            this.Children.Add(border);

            LightVisualElement title = new LightVisualElement();
            title.BackgroundImage = Resources.shadowBackground;
            title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Tile;
            title.StretchVertically = false;
            title.StretchHorizontally = true;
            title.MinSize = new Size(0, title.BackgroundImage.Height);
            title.Image = Resources.our_movies;
            title.ImageAlignment = ContentAlignment.BottomCenter;
            this.Children.Add(title);

            commandBar = new MoviesPageCommandBar();
            this.Children.Add(commandBar);

            StackLayoutElement horizontalStack = new StackLayoutElement();
            horizontalStack.FitInAvailableSize = true;
            horizontalStack.StretchHorizontally = true;
            horizontalStack.StretchVertically = true;
            horizontalStack.Padding = new Padding(0, 0, 0, 2);
            this.Children.Add(horizontalStack);

            StackLayoutElement verticalStack = new StackLayoutElement();
            verticalStack.StretchVertically = true;
            verticalStack.StretchHorizontally = false;
            verticalStack.Orientation = System.Windows.Forms.Orientation.Vertical;
            verticalStack.MinSize = new System.Drawing.Size(82, 0);
            verticalStack.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            verticalStack.ElementSpacing = 8;
            horizontalStack.Children.Add(verticalStack);

            iconView = new IconView();
            horizontalStack.Children.Add(iconView);

            buttonIcons = new MovieButtonElement();
            buttonIcons.Normal = Resources.buttonGridNormal;
            buttonIcons.Hovered = Resources.buttonGridHover;
            buttonIcons.Down = Resources.buttonGridDown;
            buttonIcons.Selected = Resources.buttonGridDown;
            buttonIcons.Margin = new System.Windows.Forms.Padding(20, 0, 0, 0);
            buttonIcons.Click += new EventHandler(buttonIcons_Click);
            buttonIcons.State = MovieButtonElement.ButtonStates.Selected;
            verticalStack.Children.Add(buttonIcons);

            buttonList = new MovieButtonElement();
            buttonList.Normal = Resources.buttonListNormal;
            buttonList.Hovered = Resources.buttonListHover;
            buttonList.Down = Resources.buttonListDown;
            buttonList.Selected = Resources.buttonListDown;
            buttonList.Margin = new System.Windows.Forms.Padding(20, 0, 0, 0);
            buttonList.Click += new EventHandler(buttonList_Click);
            verticalStack.Children.Add(buttonList);
        }

        #endregion

        #region Properties

        public BackdropsView BackdropsView { get { return backdropsView; } }
        public MoviesPageCommandBar CommandBar { get { return commandBar; } }
        public IconView IconView { get { return iconView; } }
        public MovieButtonElement ButtonIcons { get { return buttonIcons; } }
        public MovieButtonElement ButtonList { get { return buttonList; } }

        public FilterModes FilterMode
        {
            get { return filterMode; }
            set
            {
                if (this.filterMode != value)
                {
                    this.filterMode = value;
                    UpdateItems();
                }
            }
        }

        public SortModes SortMode
        {
            get { return sortMode; }
            set
            {
                if (this.sortMode != value)
                {
                    this.sortMode = value;
                    UpdateItems();
                }
            }
        }

        #endregion

        #region Event Handlers

        private void buttonList_Click(object sender, EventArgs e)
        {
            if (currentList == null)
            {
                return;
            }
            if (grid)
            {
                grid = false;
                buttonIcons.State = MovieButtonElement.ButtonStates.Normal;
                buttonList.State = MovieButtonElement.ButtonStates.Selected;
                UpdateItems();
            }
        }

        private void buttonIcons_Click(object sender, EventArgs e)
        {
            if (currentList == null)
            {
                return;
            }
            if (!grid)
            {
                grid = true;
                buttonIcons.State = MovieButtonElement.ButtonStates.Selected;
                buttonList.State = MovieButtonElement.ButtonStates.Normal;
                UpdateItems();
            }
        }

        private void tmdbAPI_MovieSearchCompleted(object sender, ImdbMovieSearchCompletedEventArgs e)
        {
            if (e.Movies == null)
            {
                return;
            }
            this.IconView.Image = null;

            lock (this)
            {
                List<TmdbMovie> items = e.UserState as List<TmdbMovie>;
                if (items == null)
                {
                    return;
                }
                items.Clear();

                try
                {
                    foreach (TmdbMovie movie in e.Movies)
                    {
                        items.Add(movie);
                        movie.MovieImagesLoadCompleted += new TmdbMovie.MovieImagesLoadCompletedEventHandler(movie_MovieImagesLoadCompleted);
                        movie.LoadImagesAsync(movie, true);
                        Root.API.GetMovieInfoAsync(movie.Id, movie);
                    }
                }
                catch { }
                currentList = items;

                MovieControl control = this.ElementTree.Control as MovieControl;
                control.Invoke(new MethodInvoker(delegate() { UpdateItems(); }));
            }
        }

        private void tmdbAPI_GetMovieInfoCompleted(object sender, ImdbMovieInfoCompletedEventArgs e)
        {
            if (e.Movie == null)
            {
                return;
            }
            TmdbMovie movie = e.UserState as TmdbMovie;
            if (movie != null)
            {
                MovieControl control = this.ElementTree.Control as MovieControl;
                control.Invoke(new MethodInvoker(delegate()
                {
                    MovieThumbElement movieElement = this.iconView.FindMovieElement(movie);
                    if (movieElement != null)
                    {
                        movieElement.RatingElement.Rating = (int)e.Movie.Rating;
                    }
                }));
            }
        }

        private void movie_MovieImagesLoadCompleted(object sender, ImdbMovieImagesLoadCompletedEventArgs e)
        {
            try
            {
                if (IsInValidState(true))
                {
                    TmdbMovie movie = e.Movie;
                    movie.MovieImagesLoadCompleted -= new TmdbMovie.MovieImagesLoadCompletedEventHandler(movie_MovieImagesLoadCompleted);
                    MovieControl control = this.ElementTree.Control as MovieControl;
                    control.Invoke(new MethodInvoker(delegate()
                    {
                        MovieThumbElement element = this.iconView.FindMovieElement(movie);
                        Image image = movie.Poster;
                        if (element != null)
                        {

                            if (image == null)
                            {
                                element.ImageElement.SetImage(Resources.na_img);
                            }
                            else
                            {
                                element.ImageElement.SetImage(image);
                            }
                        }
                    }));
                }
            }
            catch { }
        }

        protected override void OnLoaded()
        {
            base.OnLoaded();
            if (!initialized)
            {
                initialized = true;
                Root.API.MovieSearchCompleted += new TmdbAPI.MovieSearchAsyncCompletedEventHandler(tmdbAPI_MovieSearchCompleted);
            }
        }

        #endregion

        #region Methods

        public void LoadSearch(string search)
        {
            FindAncestor<MovieElement>().CategoriesView.Text = "";

            this.IconView.Image = Resources.waiting_bar;
            this.iconView.Items.Clear();
            this.currentList = new List<TmdbMovie>();
            Root.API.MovieSearchAsync(search, currentList);
        }

        public void LoadCategories(string genres)
        {
            this.IconView.Image = Resources.waiting_bar;
            this.iconView.Items.Clear();

            currentList = new List<TmdbMovie>();
            TmdbMovieBrowseRequest request = new TmdbMovieBrowseRequest(genres);
            request.order_by = TmdbOrderBy.release;
            request.order = TmdbOrderType.desc;
            request.per_page = 50;
            request.page = 0;
            request.release_max = new DateTime(2012, 6, 1);
            request.release_min = new DateTime(1996, 1, 1);
            Root.API.MovieBrowseAsync(request, currentList);
        }

        public void LoadNew()
        {
            FindAncestor<MovieElement>().CategoriesView.Text = "";

            this.IconView.Image = Resources.waiting_bar;
            this.iconView.Items.Clear();

            if (newMovies == null)
            {
                newMovies = new List<TmdbMovie>();

                DateTime now = DateTime.Now;
                DateTime to = new DateTime(now.Year, now.Month, now.Day);
                DateTime from = to.AddMonths(-3);
                TmdbMovieBrowseRequest request = new TmdbMovieBrowseRequest(from, to);
                request.order_by = TmdbOrderBy.release;
                request.order = TmdbOrderType.desc;
                request.per_page = 50;
                request.page = 0;
                Root.API.MovieBrowseAsync(request, newMovies);
            }
            else
            {
                currentList = newMovies;
                UpdateItems();
            }
        }

        public void LoadTop()
        {
            FindAncestor<MovieElement>().CategoriesView.Text = "";

            this.IconView.Image = Resources.waiting_bar;

            if (topMovies == null)
            {
                topMovies = new List<TmdbMovie>();

                DateTime now = DateTime.Now;
                TmdbMovieBrowseRequest request = new TmdbMovieBrowseRequest(8, 10);
                request.order_by = TmdbOrderBy.rating;
                request.order = TmdbOrderType.desc;
                request.release_min = new DateTime(now.Year - 1, now.Month, now.Day);
                request.release_max = new DateTime(now.Year, now.Month, now.Day);
                request.per_page = 50;
                request.page = 0;
                Root.API.MovieBrowseAsync(request, topMovies);
            }
            else
            {
                currentList = topMovies;
                UpdateItems();
            }
        }

        public void UpdateItems()
        {
            if (currentList == null)
            {
                return;
            }

            this.IconView.Text = "";
            this.IconView.TextAlignment = ContentAlignment.TopLeft;
            iconView.Padding = new System.Windows.Forms.Padding(0);
            this.IconView.Image = null;

            switch (SortMode)
            {
                case SortModes.Title: currentList.Sort(new SortByTitleComparer()); break;
                case SortModes.Rating: currentList.Sort(new SortByRatingComparer()); break;
                case SortModes.Year: currentList.Sort(new SortByYearComparer()); break;
            }

            iconView.Items.Clear();
            iconView.Items.BeginUpdate();
            
            foreach (TmdbMovie movie in currentList)
            {
                if (filterMode == FilterModes.AI && movie.Name.ToLower().TrimStart()[0] > 'i')
                {
                    continue;
                }
                else if (filterMode == FilterModes.JR &&
                    (movie.Name.ToLower().TrimStart()[0] < 'j' ||
                    movie.Name.ToLower().TrimStart()[0] > 'r'))
                {
                    continue;
                }
                else if (filterMode == FilterModes.SZ &&
                    (movie.Name.ToLower().TrimStart()[0] < 's' ||
                    movie.Name.ToLower().TrimStart()[0] > 'z'))
                {
                    continue;
                }
                else if (filterMode == FilterModes.Number && !char.IsNumber(movie.Name.TrimStart()[0]))
                {
                    continue;
                }

                iconView.Items.Add(new ListViewDataItem() { Tag = movie });
            }

            iconView.BigThumbs = !grid;

            iconView.Items.EndUpdate();

            if (iconView.Items.Count == 0)
            {
                this.IconView.Text = "No results to display!";
                iconView.Padding = new System.Windows.Forms.Padding(10);
            }
        }

        #endregion

        #region movie comparers

        public class SortByTitleComparer : IComparer<TmdbMovie>
        {
            #region IComparer<TmdbMovie> Members

            public int Compare(TmdbMovie x, TmdbMovie y)
            {
                return string.Compare(x.Name, y.Name);
            }

            #endregion
        }

        public class SortByRatingComparer : IComparer<TmdbMovie>
        {
            #region IComparer<TmdbMovie> Members

            public int Compare(TmdbMovie x, TmdbMovie y)
            {
                if (x.Rating < y.Rating) return 1;
                if (x.Rating > y.Rating) return -1;
                return 0;
            }

            #endregion
        }

        public class SortByYearComparer : IComparer<TmdbMovie>
        {
            #region IComparer<TmdbMovie> Members

            public int Compare(TmdbMovie x, TmdbMovie y)
            {
                int year1 = x.Released.HasValue ? x.Released.Value.Year : 0;
                int year2 = y.Released.HasValue ? y.Released.Value.Year : 0;

                if (year1 < year2) return 1;
                if (year1 > year2) return -1;
                return 0;
            }

            #endregion
        }

        #endregion
    }
}
