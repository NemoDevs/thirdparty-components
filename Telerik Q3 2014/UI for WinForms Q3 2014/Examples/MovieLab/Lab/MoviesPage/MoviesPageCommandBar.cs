﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using System.Windows.Forms;
using MovieLab.Properties;
using Telerik.WinControls;

namespace MovieLab.Lab
{
    public class MoviesPageCommandBar: StackLayoutElement
    {
        RadDropDownListElement dropDownSortBy;
        MovieButtonElement buttonFilterAll;
        MovieButtonElement buttonFilterByNumber;
        MovieButtonElement buttonFilterAI;
        MovieButtonElement buttonFilterJR;
        MovieButtonElement buttonFilterSZ;
        MovieButtonElement selectedButton;

        public RadDropDownListElement DropDownSortBy { get { return dropDownSortBy; } }
        public MovieButtonElement ButtonFilterAll { get { return buttonFilterAll; } }
        public MovieButtonElement ButtonFilterByNumber { get { return buttonFilterByNumber; } }
        public MovieButtonElement ButtonFilterByAI { get { return buttonFilterAI; } }
        public MovieButtonElement ButtonFilterByJR { get { return buttonFilterJR; } }
        public MovieButtonElement ButtonFilterBySZ { get { return buttonFilterSZ; } }

        public MovieButtonElement SelectedButton
        {
            get { return selectedButton; }
            set
            {
                if (selectedButton != value)
                {
                    if (selectedButton != null)
                    {
                        selectedButton.State = MovieButtonElement.ButtonStates.Normal;
                    }
                    selectedButton = value;
                    selectedButton.State = MovieButtonElement.ButtonStates.Selected;
                }
            }
        }

        protected override void InitializeFields()
        {
            base.InitializeFields();

            this.StretchHorizontally = true;
            this.StretchVertically = false;
            this.FitInAvailableSize = true;
            this.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.ElementSpacing = 0;
            this.DrawFill = true;
            this.GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            this.BackColor = Color.FromArgb(36, 36, 47);
            this.DrawBorder = true;
            this.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            this.BorderLeftWidth = 0;
            this.BorderRightWidth = 0;
            this.BorderTopWidth = 0;
            this.BorderBottomWidth = 1;
            this.BorderBottomColor = Color.FromArgb(11, 11, 14);
            this.BorderBottomShadowColor = Color.FromArgb(58, 58, 75);
            this.ClipDrawing = true;
            this.Margin = new Padding(0, 0, 1, 0);
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            LightVisualElement label = new LightVisualElement();
            label.Text = "SORT BY";
            label.StretchHorizontally = false;
            label.ForeColor = Color.FromArgb(111, 112, 125);
            label.Font = MovieControl.SegoeUI7;
            label.Margin = new Padding(86, 4, 0, 4);
            this.Children.Add(label);

            dropDownSortBy = new RadDropDownListElement();
            dropDownSortBy.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            dropDownSortBy.DataSource = Enum.GetNames(typeof(MoviesPageElement.SortModes));
            dropDownSortBy.MinSize = new System.Drawing.Size(190, 20);
            dropDownSortBy.Margin = new System.Windows.Forms.Padding(26, 6, 0, 6);
            dropDownSortBy.StretchHorizontally = false;
            dropDownSortBy.ItemHeight = 21;
            dropDownSortBy.SelectedIndex = 0;
            dropDownSortBy.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(dropDownSortBy_SelectedIndexChanged);
            this.Children.Add(dropDownSortBy);

            LightVisualElement separator1 = new LightVisualElement();
            separator1.StretchHorizontally = false;
            separator1.StretchVertically = true;
            separator1.MinSize = new Size(2, 0);
            separator1.DrawBorder = true;
            separator1.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            separator1.BorderLeftWidth = 0;
            separator1.BorderRightWidth = 1;
            separator1.BorderTopWidth = 0;
            separator1.BorderBottomWidth = 0;
            separator1.BorderRightColor = Color.FromArgb(58,58,75);
            separator1.BorderRightShadowColor = Color.FromArgb(0,0,0);
            separator1.Margin = new Padding(8, 0, 0, 0);
            this.Children.Add(separator1);

            ForeColorBehavior behavior = new ForeColorBehavior();

            buttonFilterAll = new MovieButtonElement();
            buttonFilterAll.Normal = Resources.buttonFilterNormal;
            buttonFilterAll.Hovered = Resources.buttonFilterHover;
            buttonFilterAll.Down = Resources.buttonFilterHover;
            buttonFilterAll.Selected = Resources.buttonFilterSelected;
            buttonFilterAll.Text = "All";
            buttonFilterAll.ForeColor = Color.FromArgb(204, 255, 0);
            buttonFilterAll.Font = MovieControl.SegoeUI9;
            buttonFilterAll.Margin = new Padding(6, 3, 0, 0);
            buttonFilterAll.AddBehavior(behavior);
            buttonFilterAll.Click += new EventHandler(buttonFilterAll_Click);
            this.Children.Add(buttonFilterAll);

            buttonFilterByNumber = new MovieButtonElement();
            buttonFilterByNumber.Normal = Resources.buttonFilterNormal;
            buttonFilterByNumber.Hovered = Resources.buttonFilterHover;
            buttonFilterByNumber.Down = Resources.buttonFilterHover;
            buttonFilterByNumber.Selected = Resources.buttonFilterSelected;
            buttonFilterByNumber.Text = "#";
            buttonFilterByNumber.ForeColor = Color.FromArgb(204, 255, 0);
            buttonFilterByNumber.Font = MovieControl.SegoeUI9;
            buttonFilterByNumber.Margin = new Padding(0, 3, 0, 0);
            buttonFilterByNumber.AddBehavior(behavior);
            buttonFilterByNumber.Click += new EventHandler(buttonFilterByNumber_Click);
            this.Children.Add(buttonFilterByNumber);

            buttonFilterAI = new MovieButtonElement();
            buttonFilterAI.Normal = Resources.buttonFilterNormal;
            buttonFilterAI.Hovered = Resources.buttonFilterHover;
            buttonFilterAI.Down = Resources.buttonFilterHover;
            buttonFilterAI.Selected = Resources.buttonFilterSelected;
            buttonFilterAI.Text = "A-I";
            buttonFilterAI.ForeColor = Color.FromArgb(204, 255, 0);
            buttonFilterAI.Font = MovieControl.SegoeUI9;
            buttonFilterAI.Margin = new Padding(0, 3, 0, 0);
            buttonFilterAI.AddBehavior(behavior);
            buttonFilterAI.Click += new EventHandler(buttonFilterAI_Click);
            this.Children.Add(buttonFilterAI);

            buttonFilterJR = new MovieButtonElement();
            buttonFilterJR.Normal = Resources.buttonFilterNormal;
            buttonFilterJR.Hovered = Resources.buttonFilterHover;
            buttonFilterJR.Down = Resources.buttonFilterHover;
            buttonFilterJR.Selected = Resources.buttonFilterSelected;
            buttonFilterJR.Text = "J-R";
            buttonFilterJR.ForeColor = Color.FromArgb(204, 255, 0);
            buttonFilterJR.Font = MovieControl.SegoeUI9;
            buttonFilterJR.Margin = new Padding(0, 3, 0, 0);
            buttonFilterJR.AddBehavior(behavior);
            buttonFilterJR.Click += new EventHandler(buttonFilterJR_Click);
            this.Children.Add(buttonFilterJR);

            buttonFilterSZ = new MovieButtonElement();
            buttonFilterSZ.Normal = Resources.buttonFilterNormal;
            buttonFilterSZ.Hovered = Resources.buttonFilterHover;
            buttonFilterSZ.Down = Resources.buttonFilterHover;
            buttonFilterSZ.Selected = Resources.buttonFilterSelected;
            buttonFilterSZ.Text = "S-Z";
            buttonFilterSZ.ForeColor = Color.FromArgb(204, 255, 0);
            buttonFilterSZ.Font = MovieControl.SegoeUI9;
            buttonFilterSZ.Margin = new Padding(0, 3, 0, 0);
            buttonFilterSZ.AddBehavior(behavior);
            buttonFilterSZ.Click += new EventHandler(buttonFilterSZ_Click);
            this.Children.Add(buttonFilterSZ);

            LightVisualElement separator2 = new LightVisualElement();
            separator2.StretchHorizontally = false;
            separator2.StretchVertically = true;
            separator2.MinSize = new Size(2, 0);
            separator2.DrawBorder = true;
            separator2.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            separator2.BorderLeftWidth = 0;
            separator2.BorderRightWidth = 1;
            separator2.BorderTopWidth = 0;
            separator2.BorderBottomWidth = 0;
            separator2.BorderRightColor = Color.FromArgb(58, 58, 75);
            separator2.BorderRightShadowColor = Color.FromArgb(0, 0, 0);
            separator2.Margin = new Padding(110, 0, 0, 0);
            this.Children.Add(separator2);
        }

        #region Event handlers

        protected override void OnLoaded()
        {
            base.OnLoaded();
            if (this.SelectedButton == null)
            {
                this.SelectedButton = ButtonFilterAll;
            }                 
        }

        void buttonFilterAll_Click(object sender, EventArgs e)
        {
            FindAncestor<MoviesPageElement>().FilterMode = MoviesPageElement.FilterModes.None;
            SelectedButton = buttonFilterAll;
        }

        void buttonFilterSZ_Click(object sender, EventArgs e)
        {
            FindAncestor<MoviesPageElement>().FilterMode = MoviesPageElement.FilterModes.SZ;
            SelectedButton = buttonFilterSZ;
        }

        void buttonFilterJR_Click(object sender, EventArgs e)
        {
            FindAncestor<MoviesPageElement>().FilterMode = MoviesPageElement.FilterModes.JR;
            SelectedButton = buttonFilterJR;
        }

        void buttonFilterAI_Click(object sender, EventArgs e)
        {
            FindAncestor<MoviesPageElement>().FilterMode = MoviesPageElement.FilterModes.AI;
            SelectedButton = buttonFilterAI;
        }

        void buttonFilterByNumber_Click(object sender, EventArgs e)
        {
            FindAncestor<MoviesPageElement>().FilterMode = MoviesPageElement.FilterModes.Number;
            SelectedButton = buttonFilterByNumber;
        }

        void dropDownSortBy_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            if (dropDownSortBy.SelectedValue == null)
            {
                return;
            }
            FindAncestor<MoviesPageElement>().SortMode = (MoviesPageElement.SortModes)Enum.Parse(typeof(MoviesPageElement.SortModes), dropDownSortBy.SelectedValue.ToString());
        }

        #endregion
    }

    public class ForeColorBehavior : PropertyChangeBehavior
    {
        public ForeColorBehavior()
            : base(LightVisualElement.IsMouseOverProperty)
        {
        }

        public override void OnPropertyChange(RadElement element, RadPropertyChangedEventArgs e)
        {
            base.OnPropertyChange(element, e);
            if ((bool)e.NewValue && 
                ((MovieButtonElement)element).State != MovieButtonElement.ButtonStates.Selected)
            {
                ((LightVisualElement)element).ForeColor = Color.Black;
            }
            else
            {
                element.ResetValue(LightVisualElement.ForeColorProperty, ValueResetFlags.Local);
            }
        }
    }
}
