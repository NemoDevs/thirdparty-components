﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using MovieLab.Properties;
using System.Windows.Forms;

namespace MovieLab.Lab
{
    public class MoviesListElement: LightVisualElement
    {
        #region Nested

        enum Direction
        {
            Left,Right
        }

        #endregion

        #region Fields

        RadPageViewStripElement pageView;
        MovieButtonElement buttonLeft;
        MovieButtonElement buttonRight;
        HorizontalList horizontalList;
        Timer timer;
        Direction scrollDirection;
        
        #endregion

        #region Properties

        public event EventHandler SelectedPageChanged;

        public RadPageViewStripElement PageView
        {
            get { return pageView; }
        }

        public MovieButtonElement ButtonLeft
        {
            get { return buttonLeft; }
        }

        public MovieButtonElement ButtonRight
        {
            get { return buttonRight; }
        }

        public HorizontalList HorizontalList
        {
            get { return horizontalList; }
        }

        public int SelectedPageIndex
        {
            get
            {
                if (this.pageView.SelectedItem == null)
                {
                    return -1;
                }
                return this.pageView.Items.IndexOf(this.pageView.SelectedItem);
            }
        }

        #endregion

        #region Initialization

        public MoviesListElement()
        {
            timer = new Timer();
            timer.Interval = 1;
            timer.Tick += new EventHandler(timer_Tick);
        }

        protected override void DisposeManagedResources()
        {
            base.DisposeManagedResources();
            if (timer != null)
            {
                timer.Stop();
                timer.Dispose();
                timer = null;
            }
        }

        protected override void InitializeFields()
        {
            base.InitializeFields();

            this.StretchHorizontally = true;
            this.StretchVertically = true;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            LightVisualElement newArrivals = new LightVisualElement();
            newArrivals.Image = Resources.newArrivals;
            newArrivals.StretchVertically = false;
            newArrivals.ImageAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.Children.Add(newArrivals);

            pageView = new RadPageViewStripElement();
            pageView.StripButtons = StripViewButtons.None;
            pageView.StretchHorizontally = true;
            pageView.StretchVertically = false;
            pageView.StripAlignment = StripViewAlignment.Bottom;
            pageView.UpdateSelectedItemContent = false;

            RadPageViewStripItem item1 = new RadPageViewStripItem("NEW ARRIVALS");
            item1.RadPropertyChanged += new Telerik.WinControls.RadPropertyChangedEventHandler(item_RadPropertyChanged);
            pageView.AddItem(item1);

            RadPageViewStripItem item2 = new RadPageViewStripItem("TOP CHOICES");
            item2.RadPropertyChanged += new Telerik.WinControls.RadPropertyChangedEventHandler(item_RadPropertyChanged);
            pageView.AddItem(item2);

            this.Children.Add(pageView);

            horizontalList = new HorizontalList();         
            pageView.ContentArea.Children.Add(horizontalList);

            buttonLeft = new MovieButtonElement();
            buttonLeft.Normal = Resources.scrollLeft;
            buttonLeft.Hovered = Resources.scrollLeftHover;
            buttonLeft.Down = Resources.scrollLeftDown;
            buttonLeft.MouseDown += new MouseEventHandler(buttonLeft_MouseDown);
            buttonLeft.MouseUp += new MouseEventHandler(buttonLeft_MouseUp);
            this.Children.Add(buttonLeft);

            buttonRight = new MovieButtonElement();
            buttonRight.Normal = Resources.scrollRight;
            buttonRight.Hovered = Resources.scrollRightHover;
            buttonRight.Down = Resources.scrollRightDown;
            buttonRight.MouseDown += new MouseEventHandler(buttonRight_MouseDown);
            buttonRight.MouseUp += new MouseEventHandler(buttonRight_MouseUp);
            this.Children.Add(buttonRight);
        }

        #endregion

        #region Layout

        protected override SizeF ArrangeOverride(SizeF finalSize)
        {
            RectangleF clientRect = GetClientRectangle(finalSize);
            float pageViewHeight = MovieThumbElement.ThumbSize.Height + 41;
            float y = clientRect.Y + (clientRect.Height - pageViewHeight) / 2;

            if (pageView != null)
            {
                pageView.Arrange(new RectangleF(clientRect.X, y, clientRect.Width, pageViewHeight));
            }

            this.Children[0].Arrange(new RectangleF(clientRect.X, y - this.Children[0].DesiredSize.Height, clientRect.Width, this.Children[0].DesiredSize.Height));

            clientRect.Width -= 6;
            y -= 1;

            if (buttonRight != null)
            {
                buttonRight.Arrange(new RectangleF(clientRect.Right - buttonRight.DesiredSize.Width + 1, y + pageViewHeight - buttonRight.DesiredSize.Height,
                    buttonRight.DesiredSize.Width, buttonRight.DesiredSize.Height));
                clientRect.Width -= buttonRight.DesiredSize.Width;
            }
            if (buttonLeft != null)
            {
                buttonLeft.Arrange(new RectangleF(clientRect.Right - buttonLeft.DesiredSize.Width + 3, y + pageViewHeight - buttonLeft.DesiredSize.Height,
                    buttonLeft.DesiredSize.Width, buttonLeft.DesiredSize.Height));
            }

            return finalSize;
        }

        #endregion

        #region Event handlers

        void item_RadPropertyChanged(object sender, Telerik.WinControls.RadPropertyChangedEventArgs e)
        {
            if (e.Property == RadPageViewStripItem.IsSelectedProperty && (bool)e.NewValue == true)
            {
                if (SelectedPageChanged != null)
                {
                    SelectedPageChanged(this, EventArgs.Empty);
                }
            }
        }

        void buttonLeft_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            timer.Start();
            scrollDirection = Direction.Left;
        }

        void buttonLeft_MouseUp(object sender, MouseEventArgs e)
        {
            timer.Stop();
        }

        void buttonRight_MouseDown(object sender, MouseEventArgs e)
        {
            scrollDirection = Direction.Right;
            timer.Start();
        }

        void buttonRight_MouseUp(object sender, MouseEventArgs e)
        {
            timer.Stop();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            int step = this.horizontalList.Scrollbar.SmallChange;
            if (this.scrollDirection == Direction.Left)
            {
                step *= -1;
            }
            if (!SetScrollValue(step))
            {
                timer.Stop();
            }
        }

        #endregion

        private bool SetScrollValue(int step)
        {
            bool result = true;
            int newValue = this.horizontalList.Scrollbar.Value + step;

            if (newValue > this.horizontalList.Scrollbar.Maximum - this.horizontalList.Scrollbar.LargeChange + 1)
            {
                newValue = this.horizontalList.Scrollbar.Maximum - this.horizontalList.Scrollbar.LargeChange + 1;
                result = false;
            }
            if (newValue < this.horizontalList.Scrollbar.Minimum)
            {
                newValue = this.horizontalList.Scrollbar.Minimum;
                result = false;
            }
            this.horizontalList.Scrollbar.Value = newValue;
            return result;
        }
    }
}
