﻿using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace MovieLab.Lab
{
    public class HomePageElement : PageElement
    {
        #region Fields

        private BackdropsView backdropsView;
        private RecomendationsList recomendations;
        private MoviesListElement moviesList;

        #endregion

        #region Initialization

        protected override void InitializeFields()
        {
            base.InitializeFields();
            this.StretchHorizontally = true;
            this.StretchVertically = true;
            this.FitInAvailableSize = true;
            this.Orientation = System.Windows.Forms.Orientation.Vertical;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            this.Children.Add(new GradientBorderElement(0, 1, false));

            backdropsView = new BackdropsView();
            this.Children.Add(backdropsView);

            GradientBorderElement border = new GradientBorderElement(0, 1, false);
            this.Children.Add(border);

            moviesList = new MoviesListElement();
            this.Children.Add(moviesList);

            LightVisualElement recomendationsTitle = new LightVisualElement();
            recomendationsTitle.Text = "our recommendations";
            recomendationsTitle.ForeColor = Color.FromArgb(51, 51, 64);
            recomendationsTitle.Font = MovieControl.SegoeUI21Italic;
            recomendationsTitle.StretchVertically = false;
            this.Children.Add(recomendationsTitle);

            recomendations = new RecomendationsList();
            this.Children.Add(recomendations);
        }

        #endregion

        #region Properties

        public BackdropsView BackdropsView { get { return this.backdropsView; } }
        public RecomendationsList Recomendations { get { return this.recomendations; } }
        public MoviesListElement MoviesList { get { return moviesList; } }

        #endregion
    }
}
