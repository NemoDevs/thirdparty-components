Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports System.Windows.Forms

Namespace MovieLabVB.Lab
    Public Class MoviesListElement
        Inherits LightVisualElement
#Region "Nested"

        Private Enum Direction
            Left
            Right
        End Enum

#End Region

#Region "Fields"

        Private pageView_Renamed As RadPageViewStripElement
        Private buttonLeft_Renamed As MovieButtonElement
        Private buttonRight_Renamed As MovieButtonElement
        Private horizontalList_Renamed As HorizontalList
        Private timer As Timer
        Private scrollDirection As Direction

#End Region

#Region "Properties"

        Public Event SelectedPageChanged As EventHandler

        Public ReadOnly Property PageView() As RadPageViewStripElement
            Get
                Return pageView_Renamed
            End Get
        End Property

        Public ReadOnly Property ButtonLeft() As MovieButtonElement
            Get
                Return buttonLeft_Renamed
            End Get
        End Property

        Public ReadOnly Property ButtonRight() As MovieButtonElement
            Get
                Return buttonRight_Renamed
            End Get
        End Property

        Public ReadOnly Property HorizontalList() As HorizontalList
            Get
                Return horizontalList_Renamed
            End Get
        End Property

        Public ReadOnly Property SelectedPageIndex() As Integer
            Get
                If Me.pageView_Renamed.SelectedItem Is Nothing Then
                    Return -1
                End If
                Return Me.pageView_Renamed.Items.IndexOf(Me.pageView_Renamed.SelectedItem)
            End Get
        End Property

#End Region

#Region "Initialization"

        Public Sub New()
            timer = New Timer()
            timer.Interval = 1
            AddHandler timer.Tick, AddressOf timer_Tick
        End Sub

        Protected Overrides Sub DisposeManagedResources()
            MyBase.DisposeManagedResources()
            If Not timer Is Nothing Then
                timer.Stop()
                timer.Dispose()
                timer = Nothing
            End If
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()

            Me.StretchHorizontally = True
            Me.StretchVertically = True
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            Dim newArrivals As LightVisualElement = New LightVisualElement()
            newArrivals.Image = Resources.newArrivals
            newArrivals.StretchVertically = False
            newArrivals.ImageAlignment = System.Drawing.ContentAlignment.BottomCenter
            Me.Children.Add(newArrivals)

            pageView_Renamed = New RadPageViewStripElement()
            pageView_Renamed.StripButtons = StripViewButtons.None
            pageView_Renamed.StretchHorizontally = True
            pageView_Renamed.StretchVertically = False
            pageView_Renamed.StripAlignment = StripViewAlignment.Bottom
            pageView_Renamed.UpdateSelectedItemContent = False

            Dim item1 As RadPageViewStripItem = New RadPageViewStripItem("NEW ARRIVALS")
            AddHandler item1.RadPropertyChanged, AddressOf item_RadPropertyChanged
            pageView_Renamed.AddItem(item1)

            Dim item2 As RadPageViewStripItem = New RadPageViewStripItem("TOP CHOICES")
            AddHandler item2.RadPropertyChanged, AddressOf item_RadPropertyChanged
            pageView_Renamed.AddItem(item2)

            Me.Children.Add(pageView_Renamed)

            horizontalList_Renamed = New HorizontalList()
            pageView_Renamed.ContentArea.Children.Add(horizontalList_Renamed)

            buttonLeft_Renamed = New MovieButtonElement()
            buttonLeft_Renamed.Normal = Resources.scrollLeft
            buttonLeft_Renamed.Hovered = Resources.scrollLeftHover
            buttonLeft_Renamed.Down = Resources.scrollLeftDown
            AddHandler buttonLeft_Renamed.MouseDown, AddressOf buttonLeft_MouseDown
            AddHandler buttonLeft_Renamed.MouseUp, AddressOf buttonLeft_MouseUp
            Me.Children.Add(buttonLeft_Renamed)

            buttonRight_Renamed = New MovieButtonElement()
            buttonRight_Renamed.Normal = Resources.scrollRight
            buttonRight_Renamed.Hovered = Resources.scrollRightHover
            buttonRight_Renamed.Down = Resources.scrollRightDown
            AddHandler buttonRight_Renamed.MouseDown, AddressOf buttonRight_MouseDown
            AddHandler buttonRight_Renamed.MouseUp, AddressOf buttonRight_MouseUp
            Me.Children.Add(buttonRight_Renamed)
        End Sub

#End Region

#Region "Layout"

        Protected Overrides Function ArrangeOverride(ByVal finalSize As SizeF) As SizeF
            Dim clientRect As RectangleF = GetClientRectangle(finalSize)
            Dim pageViewHeight As Single = MovieThumbElement.ThumbSize.Height + 41
            Dim y As Single = clientRect.Y + (clientRect.Height - pageViewHeight) / 2

            If Not pageView_Renamed Is Nothing Then
                pageView_Renamed.Arrange(New RectangleF(clientRect.X, y, clientRect.Width, pageViewHeight))
            End If

            Me.Children(0).Arrange(New RectangleF(clientRect.X, y - Me.Children(0).DesiredSize.Height, clientRect.Width, Me.Children(0).DesiredSize.Height))

            clientRect.Width -= 6
            y -= 1

            If Not buttonRight_Renamed Is Nothing Then
                buttonRight_Renamed.Arrange(New RectangleF(clientRect.Right - buttonRight_Renamed.DesiredSize.Width + 1, y + pageViewHeight - buttonRight_Renamed.DesiredSize.Height, buttonRight_Renamed.DesiredSize.Width, buttonRight_Renamed.DesiredSize.Height))
                clientRect.Width -= buttonRight_Renamed.DesiredSize.Width
            End If
            If Not buttonLeft_Renamed Is Nothing Then
                buttonLeft_Renamed.Arrange(New RectangleF(clientRect.Right - buttonLeft_Renamed.DesiredSize.Width + 3, y + pageViewHeight - buttonLeft_Renamed.DesiredSize.Height, buttonLeft_Renamed.DesiredSize.Width, buttonLeft_Renamed.DesiredSize.Height))
            End If

            Return finalSize
        End Function

#End Region

#Region "Event handlers"

        Private Sub item_RadPropertyChanged(ByVal sender As Object, ByVal e As Telerik.WinControls.RadPropertyChangedEventArgs)
            If e.Property Is RadPageViewStripItem.IsSelectedProperty AndAlso CBool(e.NewValue) = True Then
                If Not SelectedPageChangedEvent Is Nothing Then
                    RaiseEvent SelectedPageChanged(Me, EventArgs.Empty)
                End If
            End If
        End Sub

        Private Sub buttonLeft_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
            timer.Start()
            scrollDirection = Direction.Left
        End Sub

        Private Sub buttonLeft_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs)
            timer.Stop()
        End Sub

        Private Sub buttonRight_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs)
            scrollDirection = Direction.Right
            timer.Start()
        End Sub

        Private Sub buttonRight_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs)
            timer.Stop()
        End Sub

        Private Sub timer_Tick(ByVal sender As Object, ByVal e As EventArgs)
            Dim [step] As Integer = Me.horizontalList_Renamed.Scrollbar.SmallChange
            If Me.scrollDirection = Direction.Left Then
                [step] *= -1
            End If
            If (Not SetScrollValue([step])) Then
                timer.Stop()
            End If
        End Sub

#End Region

        Private Function SetScrollValue(ByVal [step] As Integer) As Boolean
            Dim result As Boolean = True
            Dim newValue As Integer = Me.horizontalList_Renamed.Scrollbar.Value + [step]

            If newValue > Me.horizontalList_Renamed.Scrollbar.Maximum - Me.horizontalList_Renamed.Scrollbar.LargeChange + 1 Then
                newValue = Me.horizontalList_Renamed.Scrollbar.Maximum - Me.horizontalList_Renamed.Scrollbar.LargeChange + 1
                result = False
            End If
            If newValue < Me.horizontalList_Renamed.Scrollbar.Minimum Then
                newValue = Me.horizontalList_Renamed.Scrollbar.Minimum
                result = False
            End If
            Me.horizontalList_Renamed.Scrollbar.Value = newValue
            Return result
        End Function
    End Class
End Namespace
