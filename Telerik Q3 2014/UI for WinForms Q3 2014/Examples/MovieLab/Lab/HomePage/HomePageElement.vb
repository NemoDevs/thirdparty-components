Imports Microsoft.VisualBasic
Imports System.Drawing
Imports System.Windows.Forms
Imports Telerik.WinControls.UI

Namespace MovieLabVB.Lab
    Public Class HomePageElement
        Inherits PageElement
#Region "Fields"

        Private backdropsView_Renamed As BackdropsView
        Private recomendations_Renamed As RecomendationsList
        Private moviesList_Renamed As MoviesListElement

#End Region

#Region "Initialization"

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()
            Me.StretchHorizontally = True
            Me.StretchVertically = True
            Me.FitInAvailableSize = True
            Me.Orientation = System.Windows.Forms.Orientation.Vertical
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            Me.Children.Add(New GradientBorderElement(0, 1, False))

            backdropsView_Renamed = New BackdropsView()
            Me.Children.Add(backdropsView_Renamed)

            Dim border As GradientBorderElement = New GradientBorderElement(0, 1, False)
            Me.Children.Add(border)

            moviesList_Renamed = New MoviesListElement()
            Me.Children.Add(moviesList_Renamed)

            Dim recomendationsTitle As LightVisualElement = New LightVisualElement()
            recomendationsTitle.Text = "our recommendations"
            recomendationsTitle.ForeColor = Color.FromArgb(51, 51, 64)
            recomendationsTitle.Font = MovieControl.SegoeUI21Italic
            recomendationsTitle.StretchVertically = False
            Me.Children.Add(recomendationsTitle)

            recomendations_Renamed = New RecomendationsList()
            Me.Children.Add(recomendations_Renamed)
        End Sub

#End Region

#Region "Properties"

        Public ReadOnly Property BackdropsView() As BackdropsView
            Get
                Return Me.backdropsView_Renamed
            End Get
        End Property
        Public ReadOnly Property Recomendations() As RecomendationsList
            Get
                Return Me.recomendations_Renamed
            End Get
        End Property
        Public ReadOnly Property MoviesList() As MoviesListElement
            Get
                Return moviesList_Renamed
            End Get
        End Property

#End Region
    End Class
End Namespace
