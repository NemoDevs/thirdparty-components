﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.Primitives;
using Telerik.WinControls.UI;
using MovieLab.Properties;
using System.IO;
using System.Reflection;

namespace MovieLab.Lab
{
    public partial class MoviesForm : RadForm
    {
        private MovieControl movieControl;
        private LightVisualElement wishListElement;
        bool down = false;

        public const int WM_NCHITTEST = 0x84;
        public const int HTBOTTOMRIGHT = 17;
        
        public MoviesForm()
        {
            string appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string cachePath = Path.Combine(appPath, "cache");
            if (!Directory.Exists(cachePath))
            {
                Directory.CreateDirectory(cachePath);
            }

            InitializeComponent();

            ThemeResolutionService.LoadPackageFile("TMDBTheme.tssp");
            ThemeResolutionService.ApplicationThemeName = "TMDBTheme";

            this.movieControl = new MovieControl();
            this.movieControl.Dock = DockStyle.Fill;
            this.movieControl.WishList.ListChanged += new System.ComponentModel.ListChangedEventHandler(WishList_ListChanged);
            this.Controls.Add(movieControl);

            this.ShowIcon = false;
            this.Text = "Telerik MovieLab";
            this.FormElement.TitleBar.ForeColor = Color.Transparent;

            InitializeTitleBarElements();
        }

        private void InitializeTitleBarElements()
        {
            ImagePrimitive img = new ImagePrimitive();
            img.Image = Properties.Resources.GreenCarbon;

            MovieButtonElement backButton = new MovieButtonElement();
            backButton.Normal = Properties.Resources.title_left_normal;
            backButton.Hovered = Resources.title_left;
            backButton.ImageAlignment = ContentAlignment.MiddleCenter;

            backButton.MouseDown += new MouseEventHandler(backButton_MouseDown);
            backButton.MouseUp += new MouseEventHandler(backButton_MouseUp);
            backButton.Shape = null;

            MovieButtonElement homeButton = new MovieButtonElement();
            homeButton.Normal = Properties.Resources.home;
            homeButton.Hovered = Resources.title_home;
            homeButton.ImageAlignment = ContentAlignment.MiddleCenter;

            homeButton.Click += new EventHandler(homeButton_Click);
            homeButton.Shape = null;

            StackLayoutElement stackElement = new StackLayoutElement();
            stackElement.Orientation = Orientation.Horizontal;
            stackElement.Alignment = ContentAlignment.MiddleLeft;
            stackElement.Margin = new Padding(0, 6, 0, 0);
            stackElement.ElementSpacing = -2;
            stackElement.Children.Add(backButton);
            stackElement.Children.Add(homeButton);

            this.FormElement.TitleBar.Children.Add(stackElement);

            wishListElement = new LightVisualElement();
            wishListElement.MaxSize = new Size(80, 20);
            wishListElement.Alignment = ContentAlignment.MiddleRight;
            wishListElement.Margin = new Padding(0, 6, 130, 0);
            UpdateWishlistText(0);

            this.FormElement.TitleBar.Children.Add(wishListElement);
            wishListElement.MouseEnter += new EventHandler(wishListElement_MouseEnter);
            wishListElement.MouseLeave += new EventHandler(wishListElement_MouseLeave);
            wishListElement.Click += new EventHandler(wishListElement_Click);
        }

        #region Methods

        public void UpdateWishlistText(int movieCount)
        {
            string color = "153,153,153";
            if (wishListElement.IsMouseOver)
            {
                color = "white";
            }
            else if (this.movieControl.Element.CurrentPage == this.movieControl.Element.WishListPage)
            {
                color = "166,179,113";
            }
            this.wishListElement.Text = "<html><color=" + color + "><b>WISH LIST (</b><color=204,255,0><b>" + movieCount + "</b><color=" + color + "><b>)</b></html>";
        }

        #endregion

        #region Event handlers

        void WishList_ListChanged(object sender, System.ComponentModel.ListChangedEventArgs e)
        {
            UpdateWishlistText(this.movieControl.WishList.Count);
        }

        void wishListElement_MouseLeave(object sender, EventArgs e)
        {
            UpdateWishlistText(this.movieControl.WishList.Count);
        }

        void wishListElement_MouseEnter(object sender, EventArgs e)
        {
            UpdateWishlistText(this.movieControl.WishList.Count);
        }

        void backButton_MouseDown(object sender, MouseEventArgs e)
        {
            down = true;
        }

        void backButton_MouseUp(object sender, MouseEventArgs e)
        {
            if (down)
            {
                down = false;
                this.movieControl.Element.NavigationService.GoBack();
            }
        }

        void wishListElement_Click(object sender, EventArgs e)
        {
            movieControl.Element.NavigationService.Navigate(movieControl.Element.WishListPage);
        }

        void homeButton_Click(object sender, EventArgs e)
        {
            this.movieControl.Element.NavigationService.Navigate(this.movieControl.Element.HomePage);
        }

        #endregion

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_NCHITTEST)
            {
                Point pt = this.PointToClient(new Point(m.LParam.ToInt32()));
                if (pt.X > this.Size.Width - 100 && pt.Y > this.Size.Height - 100)
                {
                    m.Result = (IntPtr)HTBOTTOMRIGHT;
                    return;
                }
            }
            base.WndProc(ref m);            
        }
    }
}
