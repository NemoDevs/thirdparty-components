Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports Telerik.WinControls.Layouts

Namespace MovieLabVB.Lab
    Public Class CastItem
        Inherits IconListViewVisualItem
        Private poster_Renamed As BackdropItem
        Private text_ As LightVisualElement
        Private info_Renamed As PersonInfo

        Public ReadOnly Property Poster() As BackdropItem
            Get
                Return poster_Renamed
            End Get
        End Property
        Public ReadOnly Property TextElement() As LightVisualElement
            Get
                Return text_
            End Get
        End Property
        Public ReadOnly Property Info() As PersonInfo
            Get
                Return info_Renamed
            End Get
        End Property

        Public Sub New()
            Me.poster_Renamed.SetImage(Resources.cast_img_na)
            Me.NotifyParentOnMouseInput = True
        End Sub

        Public Sub LoadInfo(ByVal info_Renamed As PersonInfo)
            If info_Renamed.Image Is Nothing Then
                Me.poster_Renamed.SetImage(Resources.cast_img_na)
            End If

            Me.info_Renamed = info_Renamed
            Me.poster_Renamed.SetImage(info_Renamed.Image)
            If info_Renamed.CastPerson.Job = "Actor" Then
                text_.Text = "<html><font=Segoe UI><color=white>" & info_Renamed.CastPerson.Name & "<br><color=gray> as <color=white>" & (info_Renamed.CastPerson.Character)
            Else
                text_.Text = "<html><font=Segoe UI><color=white>" & info_Renamed.CastPerson.Name & "<br><color=gray> as <color=white>" & (info_Renamed.CastPerson.Job)
            End If
        End Sub

        Protected Overrides Sub SynchronizeProperties()
            MyBase.SynchronizeProperties()
            LoadInfo(TryCast(Me.Data.Tag, PersonInfo))
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()
            Dim stack As StackLayoutPanel = New StackLayoutPanel()

            poster_Renamed = New BackdropItem(Nothing, 50, 77)
            poster_Renamed.Alignment = ContentAlignment.MiddleCenter
            stack.Children.Add(poster_Renamed)

            text_ = New LightVisualElement()
            text_.DisableHTMLRendering = False
            text_.TextAlignment = ContentAlignment.TopLeft
            stack.Children.Add(text_)

            stack.NotifyParentOnMouseInput = True
            stack.NotifyParentOnMouseInput = True
            Me.Children.Add(stack)

            Me.DrawFill = False
        End Sub
    End Class
End Namespace
