Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Drawing
Imports TheMovieDB

Namespace MovieLabVB.Lab
    Public Class PersonInfo
        Public Property Person() As TmdbPerson
            Get
                Return m_Person
            End Get
            Set(value As TmdbPerson)
                m_Person = Value
            End Set
        End Property
        Private m_Person As TmdbPerson

        Public Property CastPerson() As TmdbCastPerson
            Get
                Return m_CastPerson
            End Get
            Set(value As TmdbCastPerson)
                m_CastPerson = Value
            End Set
        End Property
        Private m_CastPerson As TmdbCastPerson

        Public Property Image() As Image
            Get
                Return m_Image
            End Get
            Set(value As Image)
                m_Image = Value
            End Set
        End Property
        Private m_Image As Image
    End Class
End Namespace
