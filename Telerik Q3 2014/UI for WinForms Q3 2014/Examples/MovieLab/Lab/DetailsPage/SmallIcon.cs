﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;

namespace MovieLab.Lab
{
    public class SmallIcon: StackLayoutElement
    {
        LightVisualElement poster;
        LightVisualElement title;
        LightVisualElement year;
        RatingElement rating;
        MovieButtonElement buttonAdToWishList;

        public LightVisualElement Poster { get { return poster; } }
        public LightVisualElement Title { get { return title; } }
        public LightVisualElement Year { get { return year; } }
        public RatingElement Rating { get { return rating; } }
        public MovieButtonElement ButtonAddToWishList { get { return buttonAdToWishList; } }

        public SmallIcon(string text)
        {
            this.title.Text = text;
        }
        
        protected override void InitializeFields()
        {
            base.InitializeFields();

            this.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.FitInAvailableSize = true;
            this.StretchHorizontally = false;
            this.StretchVertically = false;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            poster = new LightVisualElement();
            poster.BackColor = Color.Yellow;
            poster.GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            poster.DrawFill = true;
            poster.DrawBorder = true;
            poster.MinSize = new Size(120, 180);
            this.Children.Add(poster);

            title = new LightVisualElement();
            title.Text = "Ävatar";
            this.Children.Add(title);

            year = new LightVisualElement();
            year.Text = "2010";
            this.Children.Add(year);

            rating = new RatingElement();
            rating.Text = "******";
            this.Children.Add(rating);

            buttonAdToWishList = new MovieButtonElement();
            this.Children.Add(buttonAdToWishList);
        }
    }
}
