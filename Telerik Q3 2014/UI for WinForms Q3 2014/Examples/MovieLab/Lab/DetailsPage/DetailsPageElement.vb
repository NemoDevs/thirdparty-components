Imports Microsoft.VisualBasic
Imports System.Drawing
Imports System.Windows.Forms
Imports Telerik.WinControls.UI
Imports TheMovieDB
Imports System.Text
Imports System.Collections.Generic
Imports System.ComponentModel
Imports Telerik.WinControls
Imports System.Threading

Namespace MovieLabVB.Lab
    Public Class DetailsPageElement
        Inherits PageElement
#Region "Fields"

        Private cast As List(Of PersonInfo) = New List(Of PersonInfo)()

        Private movie_Renamed As TmdbMovie
        Private detailsPanel_Renamed As DetailsPanel
        Private recomendations_Renamed As RecomendationsList
        Private title As LightVisualElement
        Private asyncOp1 As AsyncOperation

        Private showCast_Renamed As Boolean = True
        Friend loading As Boolean = False
        Private canceled As Boolean = False
        Private resetContents As ManualResetEvent = New ManualResetEvent(False)


#End Region

#Region "Properties"

        Public ReadOnly Property DetailsPanel() As DetailsPanel
            Get
                Return detailsPanel_Renamed
            End Get
        End Property
        Public ReadOnly Property Recomendations() As RecomendationsList
            Get
                Return recomendations_Renamed
            End Get
        End Property
        Public ReadOnly Property Description() As LightVisualElement
            Get
                Return DetailsPanel.Description
            End Get
        End Property
        Public ReadOnly Property Movie() As TmdbMovie
            Get
                Return movie_Renamed
            End Get
        End Property

        Public Property ShowCast() As Boolean
            Get
                Return showCast_Renamed
            End Get
            Set(value As Boolean)
                If showCast_Renamed <> Value Then
                    showCast_Renamed = Value
                    If showCast_Renamed Then
                        DetailsPanel.CastButton.State = MovieButtonElement.ButtonStates.Selected
                        DetailsPanel.CrewButton.State = MovieButtonElement.ButtonStates.Normal
                    Else
                        DetailsPanel.CrewButton.State = MovieButtonElement.ButtonStates.Selected
                        DetailsPanel.CastButton.State = MovieButtonElement.ButtonStates.Normal
                    End If
                    UpdateCast()
                End If
            End Set
        End Property

#End Region

#Region "Initialization"

        Public Sub New()
            AddHandler DetailsPanel.CastButton.Click, AddressOf CastButton_Click
            AddHandler DetailsPanel.CrewButton.Click, AddressOf CrewButton_Click
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()

            Me.Orientation = System.Windows.Forms.Orientation.Vertical
            Me.StretchHorizontally = True
            Me.StretchVertically = True
            Me.FitInAvailableSize = True
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            Dim border As GradientBorderElement = New GradientBorderElement(0, 1, False)
            border.Margin = New Padding(0, 10, 0, 0)
            Me.Children.Add(border)

            detailsPanel_Renamed = New DetailsPanel()
            Me.Children.Add(detailsPanel_Renamed)

            border = New GradientBorderElement(0, 1, False)
            Me.Children.Add(border)

            title = New LightVisualElement()
            title.TextAlignment = ContentAlignment.MiddleCenter
            title.StretchHorizontally = True
            title.StretchVertically = False
            title.ForeColor = Color.FromArgb(51, 51, 64)
            title.Font = MovieControl.SegoeUI21Italic
            title.Text = "from the same genre"
            Me.Children.Add(title)

            recomendations_Renamed = New RecomendationsList()
            recomendations_Renamed.Margin = New Padding(0, 0, 0, 38)
            recomendations_Renamed.MinSize = New Size(0, 52)
            Me.Children.Add(recomendations_Renamed)
        End Sub

#End Region

#Region "Methods"

        Public Sub LoadMovie(ByVal movie_Renamed As TmdbMovie)
            Me.canceled = False
            Me.movie_Renamed = movie_Renamed
            Me.showCast_Renamed = True
            DetailsPanel.CastButton.State = MovieButtonElement.ButtonStates.Selected
            DetailsPanel.CrewButton.State = MovieButtonElement.ButtonStates.Normal

            Description.Text = movie_Renamed.Overview
            DetailsPanel.Title.Text = movie_Renamed.Name
            If movie_Renamed.Released.HasValue Then
                DetailsPanel.Year.Text = movie_Renamed.Released.Value.Year.ToString()
            Else
                DetailsPanel.Year.Text = ""
            End If
            DetailsPanel.Rating.Rating = CInt(Fix(movie_Renamed.Rating))
            Dim posterImg As Image = movie_Renamed.Poster
            If Not posterImg Is Nothing Then
                DetailsPanel.Poster.SetImage(posterImg)
            Else
                DetailsPanel.Poster.SetImage(Resources.na_img)
            End If
            DetailsPanel.Genres.Text = ""
            DetailsPanel.Cast.Image = Resources.waiting_bar
            DetailsPanel.Cast.Scrollbar.Value = 0
            DetailsPanel.Cast.ListView.Items.Clear()
            DetailsPanel.Image = Nothing

            recomendations_Renamed.Stack.DisposeChildren()

            recomendations_Renamed.Children(0).Visibility = ElementVisibility.Visible
            recomendations_Renamed.Children(2).Visibility = ElementVisibility.Visible

            If Not asyncOp1 Is Nothing Then
                asyncOp1.OperationCompleted()
            End If
            asyncOp1 = AsyncOperationManager.CreateOperation(Nothing)
            Dim worker1 As AsyncDelegate = New AsyncDelegate(AddressOf LoadAsyncData)
            worker1.BeginInvoke(asyncOp1, Nothing, Nothing, Nothing)
        End Sub

        Public Sub UpdateCast()
            DetailsPanel.Cast.ListView.Items.Clear()
            DetailsPanel.Cast.Scrollbar.Value = 0

            If showCast_Renamed Then
                For Each person_ As PersonInfo In cast
                    If (Not String.IsNullOrEmpty(person_.CastPerson.Name)) AndAlso person_.CastPerson.Job = "Actor" Then
                        Dim item As New ListViewDataItem
                        item.Tag = person_
                        DetailsPanel.Cast.ListView.Items.Add(item)
                    End If
                Next person_
            Else
                For Each person_ As PersonInfo In cast
                    If (Not String.IsNullOrEmpty(person_.CastPerson.Name)) AndAlso (Not String.IsNullOrEmpty(person_.CastPerson.Job)) AndAlso person_.CastPerson.Job <> "Actor" Then
                        Dim item As New ListViewDataItem
                        item.Tag = person_
                        DetailsPanel.Cast.ListView.Items.Add(item)
                    End If
                Next person_
            End If
            DetailsPanel.Cast.ListView.InvalidateMeasure()
            DetailsPanel.Cast.InvalidateMeasure()
        End Sub

        Public Sub UpdateItem(ByVal info As PersonInfo)
            DetailsPanel.Cast.ListView.SynchronizeVisualItems()
        End Sub

#End Region

#Region "Event handlers"

        Protected Friend Overrides Sub NavigationCompleted(ByVal isCurrent As Boolean, ByVal state As Object, ByVal page As PageElement)
            If (Not isCurrent) AndAlso loading Then
                canceled = True
                asyncOp1.OperationCompleted()
                loading = False
                asyncOp1 = Nothing
                Me.DetailsPanel.Cast.ListView.Items.Clear()
            End If
        End Sub

        Private Sub CrewButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            ShowCast = False
        End Sub

        Private Sub CastButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            ShowCast = True
        End Sub

#End Region

#Region "Load data"

        Private Delegate Sub AsyncDelegate(ByVal asyncOp1 As AsyncOperation, ByVal state As Object)

        Private Sub LoadAsyncData(ByVal asyncOp1 As AsyncOperation, ByVal state As Object)
            loading = True
            LoadBackdropImage()
            If canceled Then
                Return
            End If
            LoadCastImagesWorker()
            If canceled Then
                Return
            End If
            LoadRecommended()
            loading = False
        End Sub

        Private Sub LoadBackdropImage()
            If (Not Me.IsInValidState(True)) Then
                Return
            End If

            Dim control As MovieControl = CType(Me.ElementTree.Control, MovieControl)
            Dim info As TmdbMovie = control.API.GetMovieInfo(movie_Renamed.Id)

            For Each image As TmdbImage In info.Images
                If image.Type = TmdbImageType.backdrop AndAlso image.Size = TmdbImageSize.original AndAlso (Not String.IsNullOrEmpty(image.Url)) Then
                    Dim bmp As Image = TmdbImage.LoadImage(image.Url)
                    If canceled Then
                        Return
                    End If
                    control.Invoke(New MethodInvoker(Sub()
                                                         Me.DetailsPanel.Image = bmp
                                                     End Sub))
                    Exit For
                End If
            Next image
        End Sub

        Private Sub LoadCastImagesWorker()
            If (Not Me.IsInValidState(True)) Then
                Return
            End If

            Dim control As MovieControl = CType(Me.ElementTree.Control, MovieControl)

            Dim info As TmdbMovie = control.API.GetMovieInfo(movie_Renamed.Id)
            If Not info.Categories Is Nothing Then
                Dim b As StringBuilder = New StringBuilder()
                b.Append("<html><font=Segoe UI><size=10>")
                For Each category As TmdbCategory In info.Categories
                    b.Append("<color=green>-<color=white><i>" & category.Name & "</i><br>")
                Next category
                control.Invoke(New MethodInvoker(Sub()
                                                     DetailsPanel.Genres.Text = b.ToString()
                                                 End Sub))
            End If

            If canceled Then
                Return
            End If

            control.Invoke(New MethodInvoker(AddressOf AnonymousMethod1))


            cast.Clear()

            If Not info.Cast Is Nothing Then
                For Each castPerson As TmdbCastPerson In info.Cast
                    If canceled Then
                        Return
                    End If
                    Dim p As New PersonInfo
                    p.CastPerson = castPerson
                    cast.Add(p)
                Next castPerson

                If canceled Then
                    Return
                End If
                control.Invoke(New MethodInvoker(AddressOf AnonymousMethod2))
            End If


            For Each personInfo_ As PersonInfo In cast
                Dim person As TmdbPerson = control.API.GetPersonInfo(personInfo_.CastPerson.Id)
                Dim found As Boolean = False
                For Each image As TmdbImage In person.Images
                    If image.Size = TmdbImageSize.thumb AndAlso (Not String.IsNullOrEmpty(image.Url)) Then
                        personInfo_.Image = TmdbImage.LoadImage(image.Url)
                        If canceled Then
                            Return
                        End If
                        Dim fixesVbWarning As PersonInfo = personInfo_
                        control.Invoke(New MethodInvoker(Sub()
                                                             UpdateItem(fixesVbWarning)
                                                         End Sub))
                        found = True
                        Exit For
                    End If
                Next image
                If (Not found) Then
                    If canceled Then
                        Return
                    End If

                    Dim fixesVbWarning2 As PersonInfo = personInfo_
                    control.Invoke(New MethodInvoker(Sub()

                                                         For Each item As ListViewDataItem In DetailsPanel.Cast.ListView.Items
                                                             If TypeOf item.Tag Is PersonInfo Then
                                                                 fixesVbWarning2.Image = Resources.cast_img_na
                                                             End If
                                                         Next item
                                                         DetailsPanel.Cast.ListView.SynchronizeVisualItems()
                                                     End Sub))
                End If
            Next personInfo_
        End Sub
        Private Sub AnonymousMethod1()

            DetailsPanel.Cast.ListView.Items.Clear()
            DetailsPanel.Cast.Image = Nothing

        End Sub
        Private Sub AnonymousMethod2()

            UpdateCast()

            For Each item As ListViewDataItem In DetailsPanel.Cast.ListView.Items
                If TypeOf item.Tag Is CastItem Then
                    TryCast(item.Tag, CastItem).Poster.Image = Resources.waiting_bar
                End If
            Next item

        End Sub

        Private Sub LoadRecommended()
            If (Not Me.IsInValidState(True)) Then
                Return
            End If

            Dim control As MovieControl = CType(Me.ElementTree.Control, MovieControl)
            Dim info As TmdbMovie = control.API.GetMovieInfo(movie_Renamed.Id)
            If info.Categories.Length > 0 Then
                Dim catType As String = control.GetGenreID(info.Categories(0).Name).ToString()
                Dim request As TmdbMovieBrowseRequest = New TmdbMovieBrowseRequest(catType)
                request.per_page = 4
                request.page = 0
                Dim movies As TmdbMovie() = control.API.MovieBrowse(request)

                Dim i As Integer = 0
                Do While i < movies.Length AndAlso i < 4
                    If canceled Then
                        Exit Do
                    End If
                    Dim rmovie As TmdbMovie = movies(i)
                    Dim item As RecomendationItem = Nothing
                    control.Invoke(New MethodInvoker(Sub()
                                                         item = New RecomendationItem()
                                                         item.Load(rmovie)
                                                         recomendations_Renamed.Stack.Children.Add(item)
                                                     End Sub))

                    control.API.GetMovieInfoAsync(rmovie.Id, item)
                    rmovie.LoadImagesAsync(item, False)

                    i += 1
                Loop

                If movies.Length = 0 Then
                    control.Invoke(New MethodInvoker(AddressOf AnonymousMethod3))
                End If
            End If
        End Sub
        Private Sub AnonymousMethod3()
            recomendations_Renamed.Children(0).Visibility = ElementVisibility.Collapsed
            recomendations_Renamed.Children(2).Visibility = ElementVisibility.Collapsed
        End Sub

#End Region
    End Class
End Namespace
