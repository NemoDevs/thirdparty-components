Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing

Namespace MovieLabVB.Lab
    Public Class SmallIcon
        Inherits StackLayoutElement
        Private poster_Renamed As LightVisualElement
        Private title_Renamed As LightVisualElement
        Private year_Renamed As LightVisualElement
        Private rating_Renamed As RatingElement
        Private buttonAdToWishList As MovieButtonElement

        Public ReadOnly Property Poster() As LightVisualElement
            Get
                Return poster_Renamed
            End Get
        End Property
        Public ReadOnly Property Title() As LightVisualElement
            Get
                Return title_Renamed
            End Get
        End Property
        Public ReadOnly Property Year() As LightVisualElement
            Get
                Return year_Renamed
            End Get
        End Property
        Public ReadOnly Property Rating() As RatingElement
            Get
                Return rating_Renamed
            End Get
        End Property
        Public ReadOnly Property ButtonAddToWishList() As MovieButtonElement
            Get
                Return buttonAdToWishList
            End Get
        End Property

        Public Sub New(ByVal text As String)
            Me.title_Renamed.Text = text
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()

            Me.Orientation = System.Windows.Forms.Orientation.Vertical
            Me.FitInAvailableSize = True
            Me.StretchHorizontally = False
            Me.StretchVertically = False
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            poster_Renamed = New LightVisualElement()
            poster_Renamed.BackColor = Color.Yellow
            poster_Renamed.GradientStyle = Telerik.WinControls.GradientStyles.Solid
            poster_Renamed.DrawFill = True
            poster_Renamed.DrawBorder = True
            poster_Renamed.MinSize = New Size(120, 180)
            Me.Children.Add(poster_Renamed)

            title_Renamed = New LightVisualElement()
            title_Renamed.Text = "�vatar"
            Me.Children.Add(title_Renamed)

            year_Renamed = New LightVisualElement()
            year_Renamed.Text = "2010"
            Me.Children.Add(year_Renamed)

            rating_Renamed = New RatingElement()
            rating_Renamed.Text = "******"
            Me.Children.Add(rating_Renamed)

            buttonAdToWishList = New MovieButtonElement()
            Me.Children.Add(buttonAdToWishList)
        End Sub
    End Class
End Namespace
