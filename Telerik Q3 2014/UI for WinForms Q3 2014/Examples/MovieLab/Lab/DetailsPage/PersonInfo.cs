﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using TheMovieDB;

namespace MovieLab.Lab
{
    public class PersonInfo
    {
        public TmdbPerson Person { get; set; }
        public TmdbCastPerson CastPerson { get; set; }
        public Image Image { get; set; }
    }
}
