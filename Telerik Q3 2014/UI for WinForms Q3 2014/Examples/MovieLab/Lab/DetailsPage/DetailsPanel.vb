Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Telerik.WinControls.UI
Imports System.Drawing
Imports Telerik.WinControls
Imports System.Windows.Forms
Imports Telerik.WinControls.Paint
Imports System.Drawing.Drawing2D

Namespace MovieLabVB.Lab
    Public Class DetailsPanel
        Inherits LightVisualElement
#Region "Fields"

        Private imageHolder As LightVisualElement
        Private poster_Renamed As BackdropItem
        Private buttonAddToWishList_Renamed As MovieButtonElement
        Private title_Renamed As LightVisualElement
        Private rating_Renamed As RatingElement
        Private year_Renamed As LightVisualElement
        Private genres_Renamed As LightVisualElement
        Private cast_Renamed As HorizontalList
        Private crewButton_Renamed As MovieButtonElement
        Private castButton_Renamed As MovieButtonElement
        Private separator1 As LightVisualElement
        Private description_Renamed As LightVisualElement

#End Region

#Region "Properties"

        Public ReadOnly Property Poster() As BackdropItem
            Get
                Return poster_Renamed
            End Get
        End Property

        Public ReadOnly Property ButtonAddToWishList() As MovieButtonElement
            Get
                Return buttonAddToWishList_Renamed
            End Get
        End Property
        Public ReadOnly Property Title() As LightVisualElement
            Get
                Return title_Renamed
            End Get
        End Property
        Public ReadOnly Property Rating() As RatingElement
            Get
                Return rating_Renamed
            End Get
        End Property
        Public ReadOnly Property Year() As LightVisualElement
            Get
                Return year_Renamed
            End Get
        End Property
        Public ReadOnly Property Genres() As LightVisualElement
            Get
                Return genres_Renamed
            End Get
        End Property
        Public ReadOnly Property Cast() As HorizontalList
            Get
                Return cast_Renamed
            End Get
        End Property
        Public ReadOnly Property CastButton() As MovieButtonElement
            Get
                Return castButton_Renamed
            End Get
        End Property
        Public ReadOnly Property CrewButton() As MovieButtonElement
            Get
                Return crewButton_Renamed
            End Get
        End Property
        Public ReadOnly Property Description() As LightVisualElement
            Get
                Return description_Renamed
            End Get
        End Property

#End Region

#Region "Initialization"

        Public Sub New()
        End Sub

        Protected Overrides Sub InitializeFields()
            MyBase.InitializeFields()

            Me.StretchHorizontally = True
            Me.StretchVertically = True
            Me.ClipDrawing = True
            Me.Padding = New System.Windows.Forms.Padding(20, 16, 0, 0)
            Me.ImageAlignment = ContentAlignment.TopRight
            Me.BackColor = Color.FromArgb(32, 32, 41)
            Me.GradientStyle = GradientStyles.Solid
            Me.DrawFill = True
        End Sub

        Protected Overrides Sub CreateChildElements()
            MyBase.CreateChildElements()

            imageHolder = New LightVisualElement()
            imageHolder.BackgroundImage = Resources.pictureHolder
            imageHolder.MinSize = imageHolder.BackgroundImage.Size
            Me.Children.Add(imageHolder)

            Dim bmp As Bitmap = Resources.pictureHolder
            poster_Renamed = New BackdropItem(Resources.na_img, 193, 285)
            Me.Children.Add(poster_Renamed)

            buttonAddToWishList_Renamed = New MovieButtonElement()
            buttonAddToWishList_Renamed.Normal = Resources.buttonWishList
            buttonAddToWishList_Renamed.Hovered = Resources.buttonWishListHover
            buttonAddToWishList_Renamed.Down = Resources.buttonWishListDown
            Me.Children.Add(buttonAddToWishList_Renamed)

            title_Renamed = New LightVisualElement()
            title_Renamed.TextAlignment = ContentAlignment.TopLeft
            title_Renamed.Font = MovieControl.SegoeUI28Italic
            title_Renamed.ForeColor = Color.White
            title_Renamed.AutoEllipsis = True
            title_Renamed.ZIndex = 100
            Me.Children.Add(title_Renamed)

            rating_Renamed = New RatingElement()
            rating_Renamed.StarOn = RatingElement.bigStarOn
            rating_Renamed.StarOff = RatingElement.bigStarOff
            rating_Renamed.ForeColor = Color.FromArgb(204, 255, 0)
            rating_Renamed.StarSpacing = 8
            Me.Children.Add(rating_Renamed)

            year_Renamed = New LightVisualElement()
            year_Renamed.TextAlignment = ContentAlignment.TopLeft
            year_Renamed.ForeColor = Color.FromArgb(107, 107, 121)
            year_Renamed.Font = MovieControl.SegoeUI14
            Me.Children.Add(year_Renamed)

            genres_Renamed = New LightVisualElement()
            genres_Renamed.DisableHTMLRendering = False
            genres_Renamed.TextAlignment = ContentAlignment.TopLeft
            genres_Renamed.ForeColor = Color.White
            Me.Children.Add(genres_Renamed)

            cast_Renamed = New HorizontalList() With {.TargetType = GetType(PersonInfo)}
            cast_Renamed.BorderColor = Color.FromArgb(195, 244, 1)
            cast_Renamed.DrawBorder = True
            cast_Renamed.BorderGradientStyle = Telerik.WinControls.GradientStyles.Solid
            cast_Renamed.DrawFill = True
            cast_Renamed.ListView.ItemSize = New System.Drawing.Size(170, 80)
            cast_Renamed.ListView.AllowArbitraryItemHeight = False
            cast_Renamed.ListView.AllowArbitraryItemWidth = False
            cast_Renamed.GradientStyle = Telerik.WinControls.GradientStyles.Solid
            cast_Renamed.BackColor = Color.FromArgb(16, 16, 20)
            cast_Renamed.MinSize = New Size(274, 120)
            cast_Renamed.MaxSize = New Size(274, 120)
            cast_Renamed.ListView.StretchVertically = True
            CType(cast_Renamed.Children(0), LightVisualElement).DrawFill = False
            CType(cast_Renamed.Children(0), LightVisualElement).StretchVertically = True

            Dim e1 As RadElement = cast_Renamed.Children(0).Children(0)
            cast_Renamed.Children(0).Children.RemoveAt(0)
            cast_Renamed.Children(0).Children.Add(e1)

            Me.Children.Add(cast_Renamed)

            crewButton_Renamed = New MovieButtonElement()
            crewButton_Renamed.Normal = Resources.crewNormal
            crewButton_Renamed.Hovered = Resources.crewHover
            crewButton_Renamed.Down = Resources.crewDown
            crewButton_Renamed.Selected = Resources.crewHover
            Me.Children.Add(crewButton_Renamed)

            castButton_Renamed = New MovieButtonElement()
            castButton_Renamed.Normal = Resources.castNormal
            castButton_Renamed.Hovered = Resources.castHover
            castButton_Renamed.Down = Resources.castDown
            castButton_Renamed.Selected = Resources.castHover
            castButton_Renamed.State = MovieButtonElement.ButtonStates.Selected
            Me.Children.Add(castButton_Renamed)

            separator1 = New LightVisualElement()
            separator1.StretchHorizontally = False
            separator1.StretchVertically = True
            separator1.MinSize = New Size(2, 0)
            separator1.DrawBorder = True
            separator1.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders
            separator1.BorderLeftWidth = 0
            separator1.BorderRightWidth = 1
            separator1.BorderTopWidth = 0
            separator1.BorderBottomWidth = 0
            separator1.BorderRightColor = Color.FromArgb(42, 42, 54)
            separator1.BorderRightShadowColor = Color.FromArgb(0, 0, 0)
            separator1.Margin = New Padding(0, 60, 0, 20)
            Me.Children.Add(separator1)

            description_Renamed = New LightVisualElement()
            description_Renamed.TextAlignment = ContentAlignment.TopLeft
            description_Renamed.TextWrap = True
            description_Renamed.ForeColor = Color.FromArgb(156, 156, 156)
            description_Renamed.Font = MovieControl.SegoeUI9
            description_Renamed.StretchHorizontally = True
            description_Renamed.StretchVertically = True
            description_Renamed.Padding = New System.Windows.Forms.Padding(20, 60, 20, 20)
            Me.Children.Add(description_Renamed)
        End Sub

#End Region

#Region "Layout"

        Protected Overloads Overrides Function MeasureOverride(ByVal availableSize As SizeF) As SizeF
            MyBase.MeasureOverride(availableSize)
            Return New SizeF(availableSize.Width, 0)
        End Function

        Protected Overrides Function ArrangeOverride(ByVal finalSize As SizeF) As SizeF
            MyBase.ArrangeOverride(finalSize)

            Dim clientRect As RectangleF = GetClientRectangle(finalSize)
            Dim x As Single = clientRect.X
            Dim y As Single = clientRect.Y

            imageHolder.Arrange(New RectangleF(clientRect.X - 8, clientRect.Y, imageHolder.DesiredSize.Width, imageHolder.DesiredSize.Height))

            poster_Renamed.Arrange(New RectangleF(clientRect.X + 3, clientRect.Y + 10, poster_Renamed.DesiredSize.Width, poster_Renamed.DesiredSize.Height))
            x += poster_Renamed.DesiredSize.Width + 20
            y += poster_Renamed.DesiredSize.Height + 26

            buttonAddToWishList_Renamed.Arrange(New RectangleF(x - buttonAddToWishList_Renamed.DesiredSize.Width - 14, y, buttonAddToWishList_Renamed.DesiredSize.Width, buttonAddToWishList_Renamed.DesiredSize.Height))

            title_Renamed.Arrange(New RectangleF(x, clientRect.Y, clientRect.Width - x, title_Renamed.DesiredSize.Height))
            y = clientRect.Y + title_Renamed.DesiredSize.Height

            rating_Renamed.Arrange(New RectangleF(x, y, rating_Renamed.DesiredSize.Width, rating_Renamed.DesiredSize.Height))
            y += rating_Renamed.DesiredSize.Height

            year_Renamed.Arrange(New RectangleF(x, y, year_Renamed.DesiredSize.Width, year_Renamed.DesiredSize.Height))
            y += year_Renamed.DesiredSize.Height

            genres_Renamed.Arrange(New RectangleF(x, y, genres_Renamed.DesiredSize.Width, genres_Renamed.DesiredSize.Height))

            cast_Renamed.Arrange(New RectangleF(280, 232, cast_Renamed.DesiredSize.Width, cast_Renamed.DesiredSize.Height))

            x = 281 - (crewButton_Renamed.DesiredSize.Width + castButton_Renamed.DesiredSize.Width)
            crewButton_Renamed.Arrange(New RectangleF(x + 2, 231, crewButton_Renamed.DesiredSize.Width, crewButton_Renamed.DesiredSize.Height))
            castButton_Renamed.Arrange(New RectangleF(x + crewButton_Renamed.DesiredSize.Width, 231, castButton_Renamed.DesiredSize.Width, castButton_Renamed.DesiredSize.Height))

            x = 280 + cast_Renamed.DesiredSize.Width + 20
            separator1.Arrange(New RectangleF(x, clientRect.Y, clientRect.Right - x, clientRect.Height))

            Dim rect As RectangleF = New RectangleF(x, clientRect.Y + 54, clientRect.Right - x, clientRect.Height - 89)
            description_Renamed.Arrange(rect)
            description_Renamed.Layout.Measure(rect.Size)
            description_Renamed.Layout.Arrange(New RectangleF(10, 5, rect.Size.Width - 20, rect.Size.Height - 10))

            Return finalSize
        End Function

#End Region

#Region "Paint"

        Protected Overrides Sub PaintImage(ByVal graphics As IGraphics)
            Dim clientRect As RectangleF = GetClientRectangle(Me.Size)

            If Image Is Nothing OrElse Image.Width = 0 OrElse Image.Height = 0 Then
                Return
            End If

            Dim ratioX As Single = CSng(Size.Width) / Image.Width
            Dim ratioY As Single = CSng(Size.Height) / Image.Height

            Dim factor As Single = Math.Min(ratioX, ratioY)

            If factor <= 0.0F Then
                Return
            End If

            Dim finalWidth As Integer = CInt(Fix(Math.Round(Image.Width * factor)))
            Dim finalHeight As Integer = CInt(Fix(Math.Round(Image.Height * factor)))

            Dim xStart As Integer = CInt(Fix((Size.Width - finalWidth) / 2.0F))
            Dim yStart As Integer = CInt(Fix((Size.Height - finalHeight) / 2.0F))

            graphics.DrawBitmap(Image, Size.Width - finalWidth, yStart, finalWidth, finalHeight, 0.1)

            Dim g As Graphics = CType(graphics.UnderlayGraphics, Graphics)
            Using brush As LinearGradientBrush = New LinearGradientBrush(New Point(Size.Width - finalWidth - 2, Size.Height), New Point(Size.Width - finalWidth + 118, Size.Height), Color.FromArgb(255, 32, 32, 41), Color.FromArgb(0, 32, 32, 41))
                g.FillRectangle(brush, Size.Width - finalWidth - 2, 0, 120, Me.Size.Height)
            End Using
        End Sub

#End Region
    End Class
End Namespace
