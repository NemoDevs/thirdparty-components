﻿using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using TheMovieDB;
using System.Text;
using System.Collections.Generic;
using MovieLab.Properties;
using System.ComponentModel;
using Telerik.WinControls;
using System.Threading;

namespace MovieLab.Lab
{
    public class DetailsPageElement : PageElement
    {
        #region Fields

        List<PersonInfo> cast = new List<PersonInfo>();

        TmdbMovie movie;
        DetailsPanel detailsPanel;
        RecomendationsList recomendations;
        LightVisualElement title;
        AsyncOperation asyncOp1;

        private bool showCast = true;
        internal bool loading = false;
        bool canceled = false;
        ManualResetEvent resetContents = new ManualResetEvent(false);

        
        #endregion

        #region Properties

        public DetailsPanel DetailsPanel { get { return detailsPanel; } }
        public RecomendationsList Recomendations { get { return recomendations; } }
        public LightVisualElement Description { get { return DetailsPanel.Description; } }
        public TmdbMovie Movie { get { return movie; } }

        public bool ShowCast
        {
            get { return showCast; }
            set
            {
                if (showCast != value)
                {
                    showCast = value;
                    if (showCast)
                    {
                        DetailsPanel.CastButton.State = MovieButtonElement.ButtonStates.Selected;
                        DetailsPanel.CrewButton.State = MovieButtonElement.ButtonStates.Normal;
                    }
                    else
                    {
                        DetailsPanel.CrewButton.State = MovieButtonElement.ButtonStates.Selected;
                        DetailsPanel.CastButton.State = MovieButtonElement.ButtonStates.Normal;
                    }
                    UpdateCast();
                }
            }
        }

        #endregion

        #region Initialization

        public DetailsPageElement()
        {
            DetailsPanel.CastButton.Click += new System.EventHandler(CastButton_Click);
            DetailsPanel.CrewButton.Click += new System.EventHandler(CrewButton_Click);
        }

        protected override void InitializeFields()
        {
            base.InitializeFields();

            this.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.StretchHorizontally = true;
            this.StretchVertically = true;
            this.FitInAvailableSize = true;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            GradientBorderElement border = new GradientBorderElement(0, 1, false);
            border.Margin = new Padding(0, 10, 0, 0);
            this.Children.Add(border);

            detailsPanel = new DetailsPanel();
            this.Children.Add(detailsPanel);

            border = new GradientBorderElement(0, 1, false);
            this.Children.Add(border);

            title = new LightVisualElement();
            title.TextAlignment = ContentAlignment.MiddleCenter;
            title.StretchHorizontally = true;
            title.StretchVertically = false;
            title.ForeColor = Color.FromArgb(51, 51, 64);
            title.Font = MovieControl.SegoeUI21Italic;
            title.Text = "from the same genre";
            this.Children.Add(title);

            recomendations = new RecomendationsList();
            recomendations.Margin = new Padding(0, 0, 0, 38);
            recomendations.MinSize = new Size(0, 52);
            this.Children.Add(recomendations);
        }

        #endregion

        #region Methods

        public void LoadMovie(TmdbMovie movie)
        {
            this.canceled = false;
            this.movie = movie;
            this.showCast = true;
            DetailsPanel.CastButton.State = MovieButtonElement.ButtonStates.Selected;
            DetailsPanel.CrewButton.State = MovieButtonElement.ButtonStates.Normal;

            Description.Text = movie.Overview;
            DetailsPanel.Title.Text = movie.Name;
            DetailsPanel.Year.Text = movie.Released.HasValue ? movie.Released.Value.Year.ToString() : "";
            DetailsPanel.Rating.Rating = (int)movie.Rating;
            Image posterImg = movie.Poster;
            if (posterImg != null)
            {
                DetailsPanel.Poster.SetImage(posterImg);
            }
            else
            {
                DetailsPanel.Poster.SetImage(Resources.na_img);
            }
            DetailsPanel.Genres.Text = "";
            DetailsPanel.Cast.Image = Resources.waiting_bar;
            DetailsPanel.Cast.Scrollbar.Value = 0;
            DetailsPanel.Cast.ListView.Items.Clear();
            DetailsPanel.Image = null;

            recomendations.Stack.DisposeChildren();

            recomendations.Children[0].Visibility = ElementVisibility.Visible;
            recomendations.Children[2].Visibility = ElementVisibility.Visible;

            if (asyncOp1 != null)
            {
                asyncOp1.OperationCompleted();
            }
            asyncOp1 = AsyncOperationManager.CreateOperation(null);
            AsyncDelegate worker1 = new AsyncDelegate(LoadAsyncData);
            worker1.BeginInvoke(asyncOp1, null, null, null);
        }

        public void UpdateCast()
        {
            DetailsPanel.Cast.ListView.Items.Clear();
            DetailsPanel.Cast.Scrollbar.Value = 0;

            if (showCast)
            {
                foreach (PersonInfo person in cast)
                {
                    if (!string.IsNullOrEmpty(person.CastPerson.Name) && person.CastPerson.Job == "Actor")
                    {
                        DetailsPanel.Cast.ListView.Items.Add(new ListViewDataItem() { Tag = person });
                    }
                }
            }
            else
            {
                foreach (PersonInfo person in cast)
                {
                    if (!string.IsNullOrEmpty(person.CastPerson.Name) && !string.IsNullOrEmpty(person.CastPerson.Job) && person.CastPerson.Job != "Actor")
                    {
                        DetailsPanel.Cast.ListView.Items.Add(new ListViewDataItem() { Tag = person });
                    }
                }
            }
            DetailsPanel.Cast.ListView.InvalidateMeasure();
            DetailsPanel.Cast.InvalidateMeasure();
        }

        public void UpdateItem(PersonInfo info)
        {
            DetailsPanel.Cast.ListView.SynchronizeVisualItems();
        }

        #endregion

        #region Event handlers

        protected internal override void NavigationCompleted(bool isCurrent, object state, PageElement page)
        {
            if (!isCurrent && loading)
            {
                canceled = true;
                asyncOp1.OperationCompleted();
                loading = false;
                asyncOp1 = null;
                this.DetailsPanel.Cast.ListView.Items.Clear();
            }
        }

        void CrewButton_Click(object sender, System.EventArgs e)
        {
            ShowCast = false;
        }

        void CastButton_Click(object sender, System.EventArgs e)
        {
            ShowCast = true;
        }

        #endregion

        #region Load data

        private delegate void AsyncDelegate(AsyncOperation asyncOp1, object state);

        private void LoadAsyncData(AsyncOperation asyncOp1, object state)
        {
            loading = true;
            LoadBackdropImage();
            if (canceled)
            {
                return;
            }
            LoadCastImagesWorker();
            if (canceled)
            {
                return;
            }
            LoadRecommended();
            loading = false;
        }

        private void LoadBackdropImage()
        {
            if (!this.IsInValidState(true))
            {
                return;
            }

            MovieControl control = (MovieControl)this.ElementTree.Control;
            TmdbMovie info = control.API.GetMovieInfo(movie.Id);

            foreach (TmdbImage image in info.Images)
            {
                if (image.Type == TmdbImageType.backdrop && image.Size == TmdbImageSize.original && !string.IsNullOrEmpty(image.Url))
                {
                    Image bmp = TmdbImage.LoadImage(image.Url);
                    if (canceled)
                    {
                        return;
                    }
                    control.Invoke(new MethodInvoker(delegate()
                    {
                        this.DetailsPanel.Image = bmp;
                    }));
                    break;
                }
            }
        }

        private void LoadCastImagesWorker()
        {
            if (!this.IsInValidState(true))
            {
                return;
            }

            MovieControl control = (MovieControl)this.ElementTree.Control;

            TmdbMovie info = control.API.GetMovieInfo(movie.Id);
            if (info.Categories != null)
            {
                StringBuilder b = new StringBuilder();
                b.Append("<html><font=Segoe UI><size=10>");
                foreach (TmdbCategory category in info.Categories)
                {
                    b.Append("<color=green>-<color=white><i>" + category.Name + "</i><br>");
                }
                control.Invoke(new MethodInvoker(delegate() { DetailsPanel.Genres.Text = b.ToString(); }));
            }

            if (canceled)
            {
                return;
            }

            control.Invoke(new MethodInvoker(delegate() {

                DetailsPanel.Cast.ListView.Items.Clear();
                DetailsPanel.Cast.Image = null;

            }));


            cast.Clear();

            if (info.Cast != null)
            {
                foreach (TmdbCastPerson castPerson in info.Cast)
                {
                    if (canceled)
                    {
                        return;
                    }
                    cast.Add(new PersonInfo() { CastPerson = castPerson } );
                }

                if (canceled)
                {
                    return;
                }
                control.Invoke(new MethodInvoker(delegate() {
                    
                    UpdateCast();

                    foreach (ListViewDataItem item in DetailsPanel.Cast.ListView.Items)
                    {
                        if (item.Tag is CastItem)
                        {
                            (item.Tag as CastItem).Poster.Image = Resources.waiting_bar;
                        }
                    }
                    
                }));
            }


            foreach (PersonInfo personInfo in cast)
            {
                TmdbPerson person = control.API.GetPersonInfo(personInfo.CastPerson.Id);
                bool found = false;
                foreach (TmdbImage image in person.Images)
                {
                    if (image.Size == TmdbImageSize.thumb && !string.IsNullOrEmpty(image.Url))
                    {
                        personInfo.Image = TmdbImage.LoadImage(image.Url);
                        if (canceled)
                        {
                            return;
                        }
                        control.Invoke(new MethodInvoker(delegate() { UpdateItem(personInfo); }));
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    if (canceled)
                    {
                        return;
                    }
                    control.Invoke(new MethodInvoker(delegate()
                    {

                        foreach (ListViewDataItem item in DetailsPanel.Cast.ListView.Items)
                        {
                            if (item.Tag == personInfo)
                            {
                                personInfo.Image = Resources.cast_img_na;
                            }
                        }
                        DetailsPanel.Cast.ListView.SynchronizeVisualItems();
                    }));
                }
            }
        }

        private void LoadRecommended()
        {
            if (!this.IsInValidState(true))
            {
                return;
            }

            MovieControl control = (MovieControl)this.ElementTree.Control;
            TmdbMovie info = control.API.GetMovieInfo(movie.Id);            
            if (info.Categories.Length > 0)
            {
                string catType = control.GetGenreID(info.Categories[0].Name).ToString();
                TmdbMovieBrowseRequest request = new TmdbMovieBrowseRequest(catType);
                request.per_page = 4;
                request.page = 0;
                TmdbMovie[] movies = control.API.MovieBrowse(request);

                for (int i = 0; i<movies.Length && i<4; i++)
                {
                    if (canceled)
                    {
                        break;
                    }
                    TmdbMovie rmovie = movies[i];
                    RecomendationItem item = null;
                    control.Invoke(new MethodInvoker(delegate()
                    {
                        item = new RecomendationItem();
                        item.Load(rmovie);
                        recomendations.Stack.Children.Add(item);
                    }));

                    control.API.GetMovieInfoAsync(rmovie.Id, item);
                    rmovie.LoadImagesAsync(item, false);

                }

                if (movies.Length == 0)
                {
                    control.Invoke(new MethodInvoker(delegate()
                    {
                        recomendations.Children[0].Visibility = ElementVisibility.Collapsed;
                        recomendations.Children[2].Visibility = ElementVisibility.Collapsed;
                    }));
                }
            }
        }

        #endregion
    }
}
