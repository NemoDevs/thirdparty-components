﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using MovieLab.Properties;
using Telerik.WinControls.Layouts;

namespace MovieLab.Lab
{
    public class CastItem : IconListViewVisualItem
    {
        BackdropItem poster;
        LightVisualElement text;
        PersonInfo info;

        public BackdropItem Poster { get { return poster; } }
        public LightVisualElement TextElement { get { return text; } }
        public PersonInfo Info { get { return info; } }

        public CastItem()
        {
            this.poster.SetImage(Resources.cast_img_na);
            this.NotifyParentOnMouseInput = true;
        }

        public void LoadInfo(PersonInfo info)
        {
            if (info.Image == null)
            {
                this.poster.SetImage(Resources.cast_img_na);
            }

            this.info = info;
            this.poster.SetImage(info.Image);
            text.Text = "<html><font=Segoe UI><color=white>" + info.CastPerson.Name + "<br><color=gray> as <color=white>" +
                (info.CastPerson.Job == "Actor" ? info.CastPerson.Character : info.CastPerson.Job);
        }

        protected override void SynchronizeProperties()
        {
            base.SynchronizeProperties();
            LoadInfo(this.Data.Tag as PersonInfo);
        } 

        protected override void CreateChildElements()
        {
            base.CreateChildElements();
            StackLayoutPanel stack = new StackLayoutPanel();

            poster = new BackdropItem(null, 50, 77);
            poster.Alignment = ContentAlignment.MiddleCenter;
            stack.Children.Add(poster);

            text = new LightVisualElement();
            text.DisableHTMLRendering = false;
            text.TextAlignment = ContentAlignment.TopLeft;
            stack.Children.Add(text);

            stack.NotifyParentOnMouseInput = true;
            stack.NotifyParentOnMouseInput = true;
            this.Children.Add(stack);

            this.DrawFill = false;
        }
    }
}
