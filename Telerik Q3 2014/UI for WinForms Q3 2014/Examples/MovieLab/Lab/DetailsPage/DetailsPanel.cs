﻿using System;
using System.Collections.Generic;
using System.Text;
using Telerik.WinControls.UI;
using System.Drawing;
using MovieLab.Properties;
using Telerik.WinControls;
using System.Windows.Forms;
using Telerik.WinControls.Paint;
using System.Drawing.Drawing2D;

namespace MovieLab.Lab
{
    public class DetailsPanel: LightVisualElement
    {
        #region Fields

        LightVisualElement imageHolder;
        BackdropItem poster;
        MovieButtonElement buttonAddToWishList;
        LightVisualElement title;
        RatingElement rating;
        LightVisualElement year;
        LightVisualElement genres;
        HorizontalList cast;
        MovieButtonElement crewButton;
        MovieButtonElement castButton;
        LightVisualElement separator1;
        LightVisualElement description;

        #endregion

        #region Properties

        public BackdropItem Poster
        {
            get
            {
                return poster;
            }
        }

        public MovieButtonElement ButtonAddToWishList
        {
            get
            {
                return buttonAddToWishList;
            }
        }
        public LightVisualElement Title { get { return title; } }
        public RatingElement Rating { get { return rating; } }
        public LightVisualElement Year { get { return year; } }
        public LightVisualElement Genres { get { return genres; } }
        public HorizontalList Cast { get { return cast; } }
        public MovieButtonElement CastButton { get { return castButton; } }
        public MovieButtonElement CrewButton { get { return crewButton; } }
        public LightVisualElement Description { get { return description; } }

        #endregion

        #region Initialization

        public DetailsPanel()
        {           
        }
       
        protected override void InitializeFields()
        {
            base.InitializeFields();

            this.StretchHorizontally = true;
            this.StretchVertically = true;
            this.ClipDrawing = true;
            this.Padding = new System.Windows.Forms.Padding(20, 16, 0, 0);
            this.ImageAlignment = ContentAlignment.TopRight;
            this.BackColor = Color.FromArgb(32, 32, 41);
            this.GradientStyle = GradientStyles.Solid;
            this.DrawFill = true;
        }

        protected override void CreateChildElements()
        {
            base.CreateChildElements();

            imageHolder = new LightVisualElement();
            imageHolder.BackgroundImage = Resources.pictureHolder;
            imageHolder.MinSize = imageHolder.BackgroundImage.Size;
            this.Children.Add(imageHolder);
            
            Bitmap bmp = Resources.pictureHolder;
            poster = new BackdropItem(Resources.na_img, 193, 285);
            this.Children.Add(poster);

            buttonAddToWishList = new MovieButtonElement();
            buttonAddToWishList.Normal = Resources.buttonWishList;
            buttonAddToWishList.Hovered = Resources.buttonWishListHover;
            buttonAddToWishList.Down = Resources.buttonWishListDown;
            this.Children.Add(buttonAddToWishList);

            title = new LightVisualElement();
            title.TextAlignment = ContentAlignment.TopLeft;
            title.Font = MovieControl.SegoeUI28Italic;
            title.ForeColor = Color.White;
            title.AutoEllipsis = true;
            title.ZIndex = 100;
            this.Children.Add(title);

            rating = new RatingElement();
            rating.StarOn = RatingElement.bigStarOn;
            rating.StarOff = RatingElement.bigStarOff;
            rating.ForeColor = Color.FromArgb(204, 255, 0);
            rating.StarSpacing = 8;
            this.Children.Add(rating);

            year = new LightVisualElement();
            year.TextAlignment = ContentAlignment.TopLeft;
            year.ForeColor = Color.FromArgb(107, 107, 121);
            year.Font = MovieControl.SegoeUI14;
            this.Children.Add(year);

            genres = new LightVisualElement();
            genres.DisableHTMLRendering = false;
            genres.TextAlignment = ContentAlignment.TopLeft;
            genres.ForeColor = Color.White;
            this.Children.Add(genres);

            cast = new HorizontalList() { TargetType = typeof(PersonInfo) };
            cast.BorderColor = Color.FromArgb(195,244,1);
            cast.DrawBorder = true;
            cast.BorderGradientStyle = Telerik.WinControls.GradientStyles.Solid;
            cast.DrawFill = true;
            cast.ListView.ItemSize = new System.Drawing.Size(170, 80);
            cast.ListView.AllowArbitraryItemHeight = false;
            cast.ListView.AllowArbitraryItemWidth = false;
            cast.GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            cast.BackColor = Color.FromArgb(16, 16, 20);
            cast.MinSize = new Size(274, 120);
            cast.MaxSize = new Size(274, 120);
            cast.ListView.StretchVertically = true;
            ((LightVisualElement)cast.Children[0]).DrawFill = false;
            ((LightVisualElement)cast.Children[0]).StretchVertically = true;

            RadElement e1 = cast.Children[0].Children[0];
            cast.Children[0].Children.RemoveAt(0);
            cast.Children[0].Children.Add(e1);

            this.Children.Add(cast);

            crewButton = new MovieButtonElement();
            crewButton.Normal = Resources.crewNormal;
            crewButton.Hovered = Resources.crewHover;
            crewButton.Down = Resources.crewDown;
            crewButton.Selected = Resources.crewHover;
            this.Children.Add(crewButton);

            castButton = new MovieButtonElement();
            castButton.Normal = Resources.castNormal;
            castButton.Hovered = Resources.castHover;
            castButton.Down = Resources.castDown;
            castButton.Selected = Resources.castHover;
            castButton.State = MovieButtonElement.ButtonStates.Selected;
            this.Children.Add(castButton);
            
            separator1 = new LightVisualElement();
            separator1.StretchHorizontally = false;
            separator1.StretchVertically = true;
            separator1.MinSize = new Size(2, 0);
            separator1.DrawBorder = true;
            separator1.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            separator1.BorderLeftWidth = 0;
            separator1.BorderRightWidth = 1;
            separator1.BorderTopWidth = 0;
            separator1.BorderBottomWidth = 0;
            separator1.BorderRightColor = Color.FromArgb(42, 42, 54);
            separator1.BorderRightShadowColor = Color.FromArgb(0, 0, 0);
            separator1.Margin = new Padding(0, 60, 0, 20);
            this.Children.Add(separator1);

            description = new LightVisualElement();
            description.TextAlignment = ContentAlignment.TopLeft;
            description.TextWrap = true;
            description.ForeColor = Color.FromArgb(156, 156, 156);
            description.Font = MovieControl.SegoeUI9;
            description.StretchHorizontally = true;
            description.StretchVertically = true;
            description.Padding = new System.Windows.Forms.Padding(20, 60, 20, 20);
            this.Children.Add(description);
        }

        #endregion

        #region Layout

        protected override SizeF MeasureOverride(SizeF availableSize)
        {
            base.MeasureOverride(availableSize);
            return new SizeF(availableSize.Width, 0);
        }

        protected override SizeF ArrangeOverride(SizeF finalSize)
        {
            base.ArrangeOverride(finalSize);

            RectangleF clientRect = GetClientRectangle(finalSize);
            float x = clientRect.X;
            float y = clientRect.Y;

            imageHolder.Arrange(new RectangleF(clientRect.X - 8, clientRect.Y, imageHolder.DesiredSize.Width, imageHolder.DesiredSize.Height));

            poster.Arrange(new RectangleF(clientRect.X + 3, clientRect.Y + 10, poster.DesiredSize.Width, poster.DesiredSize.Height));
            x += poster.DesiredSize.Width + 20;
            y += poster.DesiredSize.Height + 26;

            buttonAddToWishList.Arrange(new RectangleF(x - buttonAddToWishList.DesiredSize.Width - 14, y, 
                buttonAddToWishList.DesiredSize.Width, buttonAddToWishList.DesiredSize.Height));

            title.Arrange(new RectangleF(x, clientRect.Y, clientRect.Width - x, title.DesiredSize.Height));
            y = clientRect.Y + title.DesiredSize.Height;

            rating.Arrange(new RectangleF(x, y, rating.DesiredSize.Width, rating.DesiredSize.Height));
            y += rating.DesiredSize.Height;

            year.Arrange(new RectangleF(x, y, year.DesiredSize.Width, year.DesiredSize.Height));
            y += year.DesiredSize.Height;

            genres.Arrange(new RectangleF(x, y, genres.DesiredSize.Width, genres.DesiredSize.Height));

            cast.Arrange(new RectangleF(280, 232, cast.DesiredSize.Width, cast.DesiredSize.Height));

            x = 281 - (crewButton.DesiredSize.Width + castButton.DesiredSize.Width);
            crewButton.Arrange(new RectangleF(x + 2, 231, crewButton.DesiredSize.Width, crewButton.DesiredSize.Height));
            castButton.Arrange(new RectangleF(x + crewButton.DesiredSize.Width, 231, castButton.DesiredSize.Width, castButton.DesiredSize.Height));

            x = 280 + cast.DesiredSize.Width + 20;
            separator1.Arrange(new RectangleF(x, clientRect.Y, clientRect.Right - x, clientRect.Height));

            RectangleF rect = new RectangleF(x, clientRect.Y + 54, clientRect.Right - x, clientRect.Height - 89);
            description.Arrange(rect);
            description.Layout.Measure(rect.Size);
            description.Layout.Arrange(new RectangleF(10, 5, rect.Size.Width - 20, rect.Size.Height - 10));

            return finalSize;
        }

        #endregion

        #region Paint

        protected override void PaintImage(IGraphics graphics)
        {
            RectangleF clientRect = GetClientRectangle(this.Size);

            if (Image == null || Image.Width == 0 || Image.Height == 0)
            {
                return;
            }

            float ratioX = (float)Size.Width / Image.Width;
            float ratioY = (float)Size.Height / Image.Height;

            float factor = Math.Min(ratioX, ratioY);

            if (factor <= 0f)
            {
                return;
            }

            int finalWidth = (int)Math.Round(Image.Width * factor);
            int finalHeight = (int)Math.Round(Image.Height * factor);

            int xStart = (int)((Size.Width - finalWidth) / 2f);
            int yStart = (int)((Size.Height - finalHeight) / 2f);

            graphics.DrawBitmap(Image, Size.Width - finalWidth, yStart, finalWidth, finalHeight, 0.1);

            Graphics g = (Graphics)graphics.UnderlayGraphics;            
            using (LinearGradientBrush brush = new LinearGradientBrush(
                new Point(Size.Width - finalWidth - 2, Size.Height),
                new Point(Size.Width - finalWidth + 118, Size.Height),
                Color.FromArgb(255, 32, 32, 41),
                Color.FromArgb(0, 32, 32, 41)))
            {
                g.FillRectangle(brush, Size.Width - finalWidth - 2, 0, 120, this.Size.Height);
            }
        }

        #endregion
    }
}
