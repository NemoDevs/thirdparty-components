Imports Microsoft.VisualBasic
Imports System
Imports Telerik.WinControls
Imports TheMovieDB
Imports System.Collections.Generic
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms
Imports System.ComponentModel
Imports Telerik.WinControls.UI
Imports System.Threading

Namespace MovieLabVB.Lab
    Public Class MovieControl
        Inherits RadControl
        Public Shared SegoeUI21Italic As Font = New Font("Segoe UI", 21.0F, FontStyle.Italic)
        Public Shared SegoeUI28Italic As Font = New Font("Segoe UI", 28.0F, FontStyle.Italic)
        Public Shared SegoeUI14 As Font = New Font("Segoe UI", 14.0F)
        Public Shared SegoeUI9 As Font = New Font("Segoe UI", 9)
        Public Shared SegoeUI7 As Font = New Font("Seoge UI", 7.2F)
        Public Shared SegoeUI10Italic As Font = New Font("Segoe UI", 10.0F, FontStyle.Italic)

#Region "Fields"

        Private recommendedMovieIDs As Integer() = New Integer() {48275, 60795, 46166, 43922}

        Private topMovies As List(Of MovieThumbElement) = New List(Of MovieThumbElement)()
        Private newMovies As List(Of MovieThumbElement) = New List(Of MovieThumbElement)()
        Private recommendedMovies As List(Of TmdbMovie) = New List(Of TmdbMovie)()
        Private wishList_Renamed As BindingList(Of WishListMovie) = New BindingList(Of WishListMovie)()

        Private element_Renamed As MovieElement
        Private tmdbAPI As TmdbAPI
        Private genres_Renamed As TmdbGenre()
        Friend ScrollBehaviorWasRunning As Boolean

#End Region

#Region "Initialization"

        Public Sub New()
            Try

                tmdbAPI = New TmdbAPI(My.Settings.Default.TmdbApiKey)
                AddHandler tmdbAPI.MovieSearchCompleted, AddressOf tmdbAPI_MovieSearchCompleted
                AddHandler tmdbAPI.GetMovieInfoCompleted, AddressOf tmdbAPI_GetMovieInfoCompleted

                ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf LoadGenres))

                AddHandler Element.HomePage.MoviesList.SelectedPageChanged, AddressOf MoviesList_SelectedPageChanged
                AddHandler Element.Header.ButtonNew.Click, AddressOf ButtonNew_Click
                AddHandler Element.Header.ButtonTop.Click, AddressOf ButtonTop_Click
                AddHandler Element.Header.TextBox.ButtonSearch.Click, AddressOf ButtonSearch_Click
                AddHandler Element.DetailsPage.DetailsPanel.ButtonAddToWishList.Click, AddressOf ButtonAddToWishList_Click
                AddHandler Element.Header.TextBox.KeyDown, AddressOf TextBox_KeyDown
                Element.HomePage.MoviesList.HorizontalList.Image = Resources.waiting_bar

                Dim now As DateTime = DateTime.Now

                Dim [to] As DateTime = New DateTime(now.Year, now.Month, now.Day)
                Dim [From] As DateTime = [to].AddMonths(-3)
                Dim request As TmdbMovieBrowseRequest = New TmdbMovieBrowseRequest(From, [to])
                request.order_by = TmdbOrderBy.release
                request.order = TmdbOrderType.desc
                request.per_page = 10
                request.page = 0
                tmdbAPI.MovieBrowseAsync(request, newMovies)

                request = New TmdbMovieBrowseRequest(8, 10)
                request.order_by = TmdbOrderBy.rating
                request.order = TmdbOrderType.desc
                request.release_min = New DateTime(now.Year - 1, now.Month, now.Day)
                request.release_max = New DateTime(now.Year, now.Month, now.Day)
                request.per_page = 10
                request.page = 0
                tmdbAPI.MovieBrowseAsync(request, topMovies)


                Dim i As Integer = 0
                Do While i < recommendedMovieIDs.Length
                    Dim item As RecomendationItem = New RecomendationItem()
                    Element.HomePage.Recomendations.Stack.Children.Add(item)
                    tmdbAPI.GetMovieInfoAsync(recommendedMovieIDs(i), item)
                    i += 1
                Loop

                tmdbAPI.GetMovieInfoAsync(1865, Element.HomePage.BackdropsView)

            Catch
            End Try
        End Sub

        Private Sub LoadGenres(ByVal state As Object)
            genres_Renamed = tmdbAPI.GetList()
        End Sub

        Protected Overrides Sub CreateChildItems(ByVal parent As RadElement)
            element_Renamed = New MovieElement()
            parent.Children.Add(element_Renamed)
        End Sub

#End Region

#Region "Methods"

        Public Function GetGenreID(ByVal genreName As String) As Integer
            For Each g As TmdbGenre In genres_Renamed
                If g.Name = genreName Then
                    Return g.ID
                End If
            Next g
            Return -1
        End Function

        Public Sub AddWishList(ByVal wishListMovie As WishListMovie)
            For Each movie As WishListMovie In wishList_Renamed
                If movie.ID = wishListMovie.ID Then
                    Return
                End If
            Next movie
            wishList_Renamed.Add(wishListMovie)
        End Sub

        Public Function GetMovieGenres(ByVal movie As TmdbMovie) As String
            Dim control As MovieControl = CType(Me.ElementTree.Control, MovieControl)
            Dim info As TmdbMovie = control.API.GetMovieInfo(movie.Id)
            If Not info.Categories Is Nothing Then
                Dim b As StringBuilder = New StringBuilder()
                Dim i As Integer = 0
                Do While i < info.Categories.Length
                    Dim category As TmdbCategory = info.Categories(i)
                    b.Append(category.Name)
                    If i < info.Categories.Length - 1 Then
                        b.Append(",")
                    End If
                    i += 1
                Loop
                Return b.ToString()
            End If
            Return ""
        End Function

#End Region

#Region "Properties"

        Public ReadOnly Property Element() As MovieElement
            Get
                Return element_Renamed
            End Get
        End Property

        Public ReadOnly Property API() As TmdbAPI
            Get
                Return Me.tmdbAPI
            End Get
        End Property

        Public ReadOnly Property Genres() As TmdbGenre()
            Get
                Return Me.genres_Renamed
            End Get
        End Property

        Public ReadOnly Property WishList() As BindingList(Of WishListMovie)
            Get
                Return Me.wishList_Renamed
            End Get
        End Property

#End Region

#Region "Event Handlers"

        Protected Overrides Sub OnClientSizeChanged(ByVal e As EventArgs)
            MyBase.OnClientSizeChanged(e)
            Me.Element.InvalidateMeasure(True)
        End Sub

        Private Sub tmdbAPI_MovieSearchCompleted(ByVal sender As Object, ByVal e As ImdbMovieSearchCompletedEventArgs)
            If e.Movies Is Nothing Then
                Return
            End If
            If (Not e.Cancelled) Then
                Dim items As List(Of MovieThumbElement) = TryCast(e.UserState, List(Of MovieThumbElement))
                If items Is Nothing Then
                    Return
                End If
                items.Clear()

                Try
                    For Each movie As TmdbMovie In e.Movies
                        Dim element_Renamed As MovieThumbElement = New MovieThumbElement(movie)
                        element_Renamed.Tag = movie
                        element_Renamed.ImageElement.Image = Resources.waiting_bar
                        items.Add(element_Renamed)
                        AddHandler movie.MovieImagesLoadCompleted, AddressOf movie_MovieImagesLoadCompleted
                        movie.LoadImagesAsync(element_Renamed, True)
                        tmdbAPI.GetMovieInfoAsync(movie.Id, element_Renamed)
                    Next movie
                Catch
                End Try

                If items Is newMovies Then
                    Invoke(New MethodInvoker(AddressOf AnonymousMethod1))
                End If
            End If
        End Sub
        Private Sub AnonymousMethod1()
            Element.HomePage.MoviesList.HorizontalList.Image = Nothing
            LoadItems(Element.HomePage.MoviesList.HorizontalList.ListView, newMovies)
        End Sub

        Private Sub tmdbAPI_GetMovieInfoCompleted(ByVal sender As Object, ByVal e As ImdbMovieInfoCompletedEventArgs)
            If e.Movie Is Nothing Then
                Return
            End If
            Dim element_Renamed As MovieThumbElement = TryCast(e.UserState, MovieThumbElement)
            If Not element_Renamed Is Nothing Then
                Invoke(New MethodInvoker(Sub()
                                             element_Renamed.RatingElement.Rating = CInt(Fix(e.Movie.Rating))
                                         End Sub))
            End If
            Dim item As RecomendationItem = TryCast(e.UserState, RecomendationItem)
            If Not item Is Nothing Then
                Invoke(New MethodInvoker(Sub()
                                             item.Title.Text = e.Movie.Name
                                             item.Rating.Rating = CInt(Fix(e.Movie.Rating))
                                             If e.Movie.Released.HasValue Then
                                                 item.Year.Text = e.Movie.Released.Value.Year.ToString()
                                             Else
                                                 item.Year.Text = ""
                                             End If
                                             item.Load(e.Movie)
                                             AddHandler e.Movie.MovieImagesLoadCompleted, AddressOf movie_MovieImagesLoadCompleted
                                             Dim imgLoadReq As New TmdbImageLoadRequest
                                             imgLoadReq.type = TmdbImageType.poster
                                             imgLoadReq.size = TmdbImageSize.w342
                                             imgLoadReq.count = 1
                                             e.Movie.LoadImagesAsync(item, imgLoadReq)
                                         End Sub))
            End If
            Dim view As BackdropsView = TryCast(e.UserState, BackdropsView)
            If Not view Is Nothing Then
                AddHandler e.Movie.MovieImagesLoadCompleted, AddressOf movie_MovieImagesLoadCompleted
                view.Load(e.Movie)
                e.Movie.LoadImagesAsync(view, False)
                e.Movie.LoadPoster()
                Return
            End If
        End Sub

        Private Sub movie_MovieImagesLoadCompleted(ByVal sender As Object, ByVal e As ImdbMovieImagesLoadCompletedEventArgs)
            If e.Movie Is Nothing Then
                Return
            End If
            Dim movie As TmdbMovie = e.Movie
            RemoveHandler movie.MovieImagesLoadCompleted, AddressOf movie_MovieImagesLoadCompleted

            Invoke(New MethodInvoker(Sub()
                                         Dim element_Renamed As MovieThumbElement = TryCast(e.UserState, MovieThumbElement)
                                         If Not element_Renamed Is Nothing Then
                                             Dim image As Image = movie.Poster
                                             If image Is Nothing Then
                                                 element_Renamed.ImageElement.SetImage(Resources.na_img)
                                             Else
                                                 element_Renamed.ImageElement.SetImage(image)
                                             End If
                                         End If
                                         Dim item As RecomendationItem = TryCast(e.UserState, RecomendationItem)
                                         If Not item Is Nothing Then
                                             item.Poster.SetImage(e.Movie.Poster)
                                             item.Image = Nothing
                                         End If

                                         Dim view As BackdropsView = TryCast(e.UserState, BackdropsView)
                                         If Not view Is Nothing Then
                                             Dim view2 As BackdropsView = Me.Element.MoviesPage.BackdropsView
                                             Dim i As Integer = 0
                                             Me.Element.HomePage.BackdropsView.Load(e.Movie)
                                             view2.Load(e.Movie)
                                             For Each image As TmdbImage In e.Movie.Images
                                                 If i > 4 Then
                                                     Exit For
                                                 End If
                                                 If Not image.Image Is Nothing AndAlso image.Type = TmdbImageType.backdrop AndAlso image.Size = TmdbImageSize.original Then
                                                     CType(view.Children(1).Children(i), BackdropItem).SetImage(image.Image)
                                                     CType(view.Children(1).Children(i), BackdropItem).SetImage(image.Image)
                                                     CType(view2.Children(1).Children(i), BackdropItem).SetImage(image.Image)
                                                     CType(view2.Children(1).Children(i), BackdropItem).SetImage(image.Image)
                                                     i += 2
                                                 End If
                                             Next image
                                         End If
                                     End Sub))
        End Sub

        Private Sub MoviesList_SelectedPageChanged(ByVal sender As Object, ByVal e As EventArgs)
            If newMovies Is Nothing OrElse topMovies Is Nothing Then
                Return
            End If

            Invoke(New MethodInvoker(AddressOf AnonymousMethod2))
        End Sub
        Private Sub AnonymousMethod2()
            Element.HomePage.MoviesList.HorizontalList.Scrollbar.Value = 0
            If Element.HomePage.MoviesList.SelectedPageIndex = 0 Then
                LoadItems(Element.HomePage.MoviesList.HorizontalList.ListView, newMovies)
            Else
                LoadItems(Element.HomePage.MoviesList.HorizontalList.ListView, topMovies)
            End If
        End Sub

        Private Sub ButtonTop_Click(ByVal sender As Object, ByVal e As EventArgs)
            Me.Element.NavigationService.Navigate(Me.Element.MoviesPage)
            Me.Element.MoviesPage.LoadTop()
        End Sub

        Private Sub ButtonNew_Click(ByVal sender As Object, ByVal e As EventArgs)
            Me.Element.NavigationService.Navigate(Me.Element.MoviesPage)
            Me.Element.MoviesPage.LoadNew()
        End Sub

        Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
            Me.Element.NavigationService.Navigate(Me.Element.MoviesPage)
            Me.Element.MoviesPage.LoadSearch(Me.Element.Header.TextBox.Text)
        End Sub

        Private Sub ButtonAddToWishList_Click(ByVal sender As Object, ByVal e As EventArgs)
            If (Not Element.ScrollBehavior.IsRunning) Then
                Dim movie As TmdbMovie = Element.DetailsPage.Movie
                Dim wishMovie As WishListMovie = New WishListMovie()
                wishMovie.ID = movie.Id
                wishMovie.Name = movie.Name
                wishMovie.Rating = CInt(Fix(movie.Rating))
                If movie.Released.HasValue Then
                    wishMovie.Year = movie.Released.Value.Year.ToString()
                Else
                    wishMovie.Year = ""
                End If
                wishMovie.Poster = Element.DetailsPage.DetailsPanel.Poster.Image
                wishMovie.Genres = GetMovieGenres(movie)
                AddWishList(wishMovie)

                Me.Element.NavigationService.Navigate(Me.Element.WishListPage)
            End If
        End Sub

        Private Sub TextBox_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs)
            If e.KeyCode = Keys.Enter Then
                Me.Element.NavigationService.Navigate(Me.Element.MoviesPage)
                Me.Element.MoviesPage.LoadSearch(Me.Element.Header.TextBox.Text)
            End If
        End Sub

        Protected Overrides Sub OnMouseDown(ByVal e As System.Windows.Forms.MouseEventArgs)
            MyBase.OnMouseDown(e)

            If TypeOf Me.ElementTree.GetElementAtPoint(e.Location) Is RadScrollBarElement OrElse TypeOf Me.ElementTree.GetElementAtPoint(e.Location) Is ScrollBarThumb Then
                Me.Element.ScrollBehavior.Stop()
            Else
                Me.Element.ScrollBehavior.MouseDown(e.Location)
            End If
        End Sub

        Protected Overrides Sub OnMouseUp(ByVal e As System.Windows.Forms.MouseEventArgs)
            MyBase.OnMouseUp(e)
            If TypeOf Me.ElementTree.GetElementAtPoint(e.Location) Is RadScrollBarElement OrElse TypeOf Me.ElementTree.GetElementAtPoint(e.Location) Is ScrollBarThumb Then
                Me.Element.ScrollBehavior.Stop()
            Else
                Me.Element.ScrollBehavior.MouseUp(e.Location)
            End If

            ScrollBehaviorWasRunning = Me.Element.ScrollBehavior.IsRunning
        End Sub

        Protected Overrides Sub OnMouseMove(ByVal e As System.Windows.Forms.MouseEventArgs)
            MyBase.OnMouseMove(e)
            Me.Element.ScrollBehavior.MouseMove(e.Location)
        End Sub

        Protected Overrides Sub OnMouseWheel(ByVal e As MouseEventArgs)
            MyBase.OnMouseWheel(e)

            Dim scrollbar As RadScrollBarElement = Nothing
            Dim smallChange As Integer = 50
            Dim [step] As Integer = CInt(Math.Max(1, e.Delta / SystemInformation.MouseWheelScrollDelta))
            Dim delta As Integer = Math.Sign(e.Delta) * [step] * SystemInformation.MouseWheelScrollLines

            If Me.Element.CurrentPage Is Me.Element.MoviesPage Then
                scrollbar = Me.element_Renamed.MoviesPage.IconView.ViewElement.VScrollBar
            ElseIf Me.Element.CurrentPage Is Me.Element.HomePage Then
                scrollbar = Me.Element.HomePage.MoviesList.HorizontalList.Scrollbar
            ElseIf Me.Element.CurrentPage Is Me.Element.DetailsPage Then
                scrollbar = Me.Element.DetailsPage.DetailsPanel.Cast.Scrollbar
            End If

            If Not scrollbar Is Nothing Then
                smallChange = 40
                Dim result As Integer = scrollbar.Value - delta * smallChange

                If result > scrollbar.Maximum - scrollbar.LargeChange + 1 Then
                    result = scrollbar.Maximum - scrollbar.LargeChange + 1
                End If

                If result < scrollbar.Minimum Then
                    result = 0
                ElseIf result > scrollbar.Maximum Then
                    result = scrollbar.Maximum
                End If

                If result <> scrollbar.Value Then
                    scrollbar.Value = result
                End If
            End If
        End Sub

#End Region

        Private Sub LoadItems(ByVal element_Renamed As RadElement, ByVal movies As List(Of MovieThumbElement))
            Dim listElement As RadListViewElement = TryCast(element_Renamed, RadListViewElement)
            If Not listElement Is Nothing Then
                listElement.Items.BeginUpdate()
                listElement.Items.Clear()
                For Each movie As MovieThumbElement In movies
                    Dim item As New ListViewDataItem
                    item.Tag = movie.Movie
                    listElement.Items.Add(item)
                Next movie
                listElement.Items.EndUpdate()

                Return
            End If

            element_Renamed.Children.Clear()
            For Each movie As RadElement In movies
                element_Renamed.Children.Add(movie)
            Next movie
        End Sub
    End Class
End Namespace
