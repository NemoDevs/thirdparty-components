Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports Telerik.WinControls
Imports Telerik.WinControls.Primitives
Imports Telerik.WinControls.UI
Imports System.IO
Imports System.Reflection

Namespace MovieLabVB.Lab
    Partial Public Class MoviesForm
        Inherits RadForm
        Private movieControl As MovieControl
        Private wishListElement As LightVisualElement
        Private down As Boolean = False

        Public Const WM_NCHITTEST As Integer = &H84
        Public Const HTBOTTOMRIGHT As Integer = 17

        Public Sub New()
            Dim appPath As String = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)
            Dim cachePath As String = Path.Combine(appPath, "cache")
            If (Not Directory.Exists(cachePath)) Then
                Directory.CreateDirectory(cachePath)
            End If

            InitializeComponent()

            ThemeResolutionService.LoadPackageFile("TMDBTheme.tssp")
            ThemeResolutionService.ApplicationThemeName = "TMDBTheme"

            Me.movieControl = New MovieControl()
            Me.movieControl.Dock = DockStyle.Fill
            AddHandler movieControl.WishList.ListChanged, AddressOf WishList_ListChanged
            Me.Controls.Add(movieControl)

            Me.ShowIcon = False
            Me.Text = "Telerik MovieLab"
            Me.FormElement.TitleBar.ForeColor = Color.Transparent

            InitializeTitleBarElements()
        End Sub

        Private Sub InitializeTitleBarElements()
            Dim img As ImagePrimitive = New ImagePrimitive()
            img.Image = Resources.GreenCarbon

            Dim backButton As MovieButtonElement = New MovieButtonElement()
            backButton.Normal = Resources.title_left_normal
            backButton.Hovered = Resources.title_left
            backButton.ImageAlignment = ContentAlignment.MiddleCenter

            AddHandler backButton.MouseDown, AddressOf backButton_MouseDown
            AddHandler backButton.MouseUp, AddressOf backButton_MouseUp
            backButton.Shape = Nothing

            Dim homeButton As MovieButtonElement = New MovieButtonElement()
            homeButton.Normal = Resources.home
            homeButton.Hovered = Resources.title_home
            homeButton.ImageAlignment = ContentAlignment.MiddleCenter

            AddHandler homeButton.Click, AddressOf homeButton_Click
            homeButton.Shape = Nothing

            Dim stackElement As StackLayoutElement = New StackLayoutElement()
            stackElement.Orientation = Orientation.Horizontal
            stackElement.Alignment = ContentAlignment.MiddleLeft
            stackElement.Margin = New Padding(0, 6, 0, 0)
            stackElement.ElementSpacing = -2
            stackElement.Children.Add(backButton)
            stackElement.Children.Add(homeButton)

            Me.FormElement.TitleBar.Children.Add(stackElement)

            wishListElement = New LightVisualElement()
            wishListElement.MaxSize = New Size(80, 20)
            wishListElement.Alignment = ContentAlignment.MiddleRight
            wishListElement.Margin = New Padding(0, 6, 130, 0)
            UpdateWishlistText(0)

            Me.FormElement.TitleBar.Children.Add(wishListElement)
            AddHandler wishListElement.MouseEnter, AddressOf wishListElement_MouseEnter
            AddHandler wishListElement.MouseLeave, AddressOf wishListElement_MouseLeave
            AddHandler wishListElement.Click, AddressOf wishListElement_Click
        End Sub

#Region "Methods"

        Public Sub UpdateWishlistText(ByVal movieCount As Integer)
            Dim color As String = "153,153,153"
            If wishListElement.IsMouseOver Then
                color = "white"
            ElseIf Me.movieControl.Element.CurrentPage Is Me.movieControl.Element.WishListPage Then
                color = "166,179,113"
            End If
            Me.wishListElement.Text = "<html><color=" & color & "><b>WISH LIST (</b><color=204,255,0><b>" & movieCount & "</b><color=" & color & "><b>)</b></html>"
        End Sub

#End Region

#Region "Event handlers"

        Private Sub WishList_ListChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ListChangedEventArgs)
            UpdateWishlistText(Me.movieControl.WishList.Count)
        End Sub

        Private Sub wishListElement_MouseLeave(ByVal sender As Object, ByVal e As EventArgs)
            UpdateWishlistText(Me.movieControl.WishList.Count)
        End Sub

        Private Sub wishListElement_MouseEnter(ByVal sender As Object, ByVal e As EventArgs)
            UpdateWishlistText(Me.movieControl.WishList.Count)
        End Sub

        Private Sub backButton_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs)
            down = True
        End Sub

        Private Sub backButton_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs)
            If down Then
                down = False
                Me.movieControl.Element.NavigationService.GoBack()
            End If
        End Sub

        Private Sub wishListElement_Click(ByVal sender As Object, ByVal e As EventArgs)
            movieControl.Element.NavigationService.Navigate(movieControl.Element.WishListPage)
        End Sub

        Private Sub homeButton_Click(ByVal sender As Object, ByVal e As EventArgs)
            Me.movieControl.Element.NavigationService.Navigate(Me.movieControl.Element.HomePage)
        End Sub

#End Region

        Protected Overrides Sub WndProc(ByRef m As Message)
            If m.Msg = WM_NCHITTEST Then
                Dim pt As Point = Me.PointToClient(New Point(m.LParam.ToInt32()))
                If pt.X > Me.Size.Width - 100 AndAlso pt.Y > Me.Size.Height - 100 Then
                    m.Result = New IntPtr(HTBOTTOMRIGHT)
                    Return
                End If
            End If
            MyBase.WndProc(m)
        End Sub
    End Class
End Namespace
