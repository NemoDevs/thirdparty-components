﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Drawing;

namespace TheMovieDB
{
    [XmlType("movie")]
    public class TmdbMovie
    {
        [XmlElement("score")]
        private decimal? _score { get; set; }
        public decimal Score
        {
            get
            {
                if (this._score.HasValue)
                    return this._score.Value;

                return 0;
            }
        }

        [XmlElement("popularity")]
        public decimal Popularity { get; set; }

        [XmlElement("translated")]
        public bool Translated { get; set; }

        [XmlElement("language")]
        public string Language { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("alternative_name")]
        public string AlternativeName { get; set; }

        [XmlElement("type")]
        public string Type { get; set; }

        [XmlElement("id")]
        public int Id { get; set; }

        [XmlElement("imdb_id")]
        public string ImdbId { get; set; }

        [XmlElement("url")]
        public string Url { get; set; }

        [XmlElement("rating")]
        public decimal Rating { get; set; }

        [XmlElement("certification")]
        public string Certification { get; set; }

        [XmlElement("overview")]
        public string Overview { get; set; }        

        [XmlElement("released")]
        public string ReleasedString 
        {
            get
            {
                return Released.HasValue ? Released.Value.ToShortDateString() : "";
            }
            set
            {
                DateTime d;
                if (DateTime.TryParse(value, out d))
                    Released = d;
                else
                    Released = null;
            }
        }

        public DateTime? Released
        {
            set;
            get;
        }

        [XmlElement("runtime")]
        public string MovieRuntime
        {
            get
            {
                return Runtime.TotalMinutes.ToString();
            }
            set
            {
                int minutes;
                Runtime = TimeSpan.FromMinutes(int.TryParse(value,out minutes) ? minutes : 0);
            }
        }

        public TimeSpan Runtime { get; set; }

        [XmlElement("budget")]
        public string Budget { get; set; }

        [XmlElement("revenue")]
        public string Revenue { get; set; }

        [XmlElement("homepage")]
        public string Homepage { get; set; }

        [XmlElement("trailer")]
        public string Trailer { get; set; }

        [XmlArrayAttribute("categories")]
        public TmdbCategory[] Categories { get; set; }

        [XmlArrayAttribute("studios")]
        public TmdbStudio[] Studios { get; set; }

        [XmlArrayAttribute("countries")]
        public TmdbCountry[] Countries { get; set; }

        [XmlArrayAttribute("images")]
        public TmdbImage[] Images { get; set; }

        [XmlArrayAttribute("cast")]
        public TmdbCastPerson[] Cast { get; set; }

        [XmlElement("last_modified_at")]
        public string LastModifiedString
        {
            get
            {
                return LastModified.HasValue ? LastModified.Value.ToShortDateString() : "";
            }
            set
            {
                DateTime d;
                if (DateTime.TryParse(value, out d))
                    LastModified = d;
                else
                    LastModified = null;
            }
        }

        public DateTime? LastModified
        {
            set;
            get;
        }

        public void LoadImages()
        {
            foreach (TmdbImage image in Images)
            {
                if (image.Size == TmdbImageSize.thumb)
                {
                    image.LoadImage();
                }
            }
        }

        public void LoadPoster()
        {
            foreach (TmdbImage image in Images)
            {
                if (image.Type == TmdbImageType.poster && image.Size == TmdbImageSize.w342)
                {
                    image.LoadImage();
                    break;
                }
            }
        }

        public void LoadRequest(TmdbImageLoadRequest request)
        {
            int count = 0;
            foreach (TmdbImage image in Images)
            {
                if (image.Type == request.type && image.Size == request.size)
                {
                    image.LoadImage();
                    count++;
                    if (count == request.count)
                    {
                        break;
                    }
                }
            }
        }

        public void LoadImagesAsync()
        {
            LoadImagesAsync(null, false);
        }

        public void LoadImagesAsync(object state, bool poster)
        {
            if (poster)
                LoadImagesAsync(state, new TmdbImageLoadRequest() { count = 1, size = TmdbImageSize.w342, type = TmdbImageType.poster });
            else
                LoadImagesAsync(state, new TmdbImageLoadRequest() { count = 3, size = TmdbImageSize.original, type = TmdbImageType.backdrop });
        }

        public void LoadImagesAsync(object state, TmdbImageLoadRequest request)
        {
            AsyncOperation asyncOp = AsyncOperationManager.CreateOperation(null);
            MovieImagesLoadWorkerDelegate worker = new MovieImagesLoadWorkerDelegate(MovieImagesLoadWorker);
            worker.BeginInvoke(asyncOp, state, request, null, null);
        }

        public delegate void MovieImagesLoadCompletedEventHandler(object sender, ImdbMovieImagesLoadCompletedEventArgs e);

        public event MovieImagesLoadCompletedEventHandler MovieImagesLoadCompleted;

        protected virtual void OnMovieImagesLoadCompleted(ImdbMovieImagesLoadCompletedEventArgs e)
        {
            if (MovieImagesLoadCompleted != null)
                MovieImagesLoadCompleted(this, e);
        }

        private delegate void MovieImagesLoadWorkerDelegate(AsyncOperation asyncOp, object state, TmdbImageLoadRequest request);

        private void MovieImagesLoadWorker(AsyncOperation asyncOp, object state, TmdbImageLoadRequest request)
        {
            Exception exception = null;
            try
            {
                if (request != null) LoadRequest(request); 
                else LoadImages();
            }
            catch (Exception ex)
            {
                exception = ex;
            }

            ImdbMovieImagesLoadCompletedEventArgs args = new ImdbMovieImagesLoadCompletedEventArgs(this, exception, false, state, request);
            asyncOp.PostOperationCompleted(
                delegate(object e) { OnMovieImagesLoadCompleted((ImdbMovieImagesLoadCompletedEventArgs)e); },
                args);
        }

        public Image Poster 
        {
            get
            {
                foreach (TmdbImage image in this.Images)
                {
                    if (image.Type == TmdbImageType.poster && image.Image != null)
                    {
                        return image.Image;
                    }
                }
                return null;
            }
        }
    }

    public class ImdbMovieImagesLoadCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        private TmdbMovie movie;
        private TmdbImageLoadRequest request;

        public TmdbMovie Movie
        {
            get
            {
                return movie;
            }
        }

        public TmdbImageLoadRequest Request
        {
            get
            {
                return request;
            }
        }

        public ImdbMovieImagesLoadCompletedEventArgs(TmdbMovie movie, Exception e, bool canceled, object state, TmdbImageLoadRequest request)
            : base(e, canceled, state)
        {
            this.movie = movie;
            this.request = request;
        }
    }

    public class TmdbImageLoadRequest
    {
        public TmdbImageType type { get; set; }
        public TmdbImageSize size { get; set; }
        public int count { get; set; }
    }
}
