﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheMovieDB
{
    public class TmdbMovieBrowseRequest
    {
        public TmdbOrderBy order_by { get; set; }
        public TmdbOrderType order { get; set; }
        public int per_page { get; set; }
        public int page { get; set; }
        public int min_votes { get; set; }
        public float rating_min { get; set; }
        public float rating_max { get; set; }
        public string genres { get; set; }
        public string genres_selector { get; set; }
        public DateTime release_min { get; set; }
        public DateTime release_max { get; set; }
        public int year { get; set; }
        public string query { get; set; }

        public TmdbMovieBrowseRequest()
        {
            this.order_by = TmdbOrderBy.rating;
            this.order = TmdbOrderType.desc;
        }

        public TmdbMovieBrowseRequest(TmdbOrderBy order_by, TmdbOrderType order)
        {
            this.order_by = order_by;
            this.order = order;
        }

        public TmdbMovieBrowseRequest(float rating_min, float rating_max)
            : this()
        {
            this.rating_min = rating_min;
            this.rating_max = rating_max;
        }

        public TmdbMovieBrowseRequest(DateTime release_min, DateTime release_max)
            : this()
        {
            this.release_min = release_min;
            this.release_max = release_max;
        }

        public TmdbMovieBrowseRequest(TmdbGenre[] genres, string[] genreNames)
            : this()
        {
            this.genres = TmdbGenre.Build(genres, genreNames);
            this.genres_selector = "or";
        }

        public TmdbMovieBrowseRequest(string genres)
            : this()
        {
            this.genres = genres;
            this.genres_selector = "or";
        }
    }
}
