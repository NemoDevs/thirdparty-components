﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace TheMovieDB
{
    [XmlRoot("OpenSearchDescription")]
    public class TmdbGetListResults
    {
        [XmlArrayAttribute("genres")]
        public TmdbGenre[] Genres { get; set; }
    }
}
