﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheMovieDB
{
    public enum TmdbImageType
    {
        poster,
        backdrop,
        profile
    }
}
