﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Drawing;
using System.Net;
using System.IO;
using MovieLab;

namespace TheMovieDB
{
    [XmlType("image")]
    public class TmdbImage: IDisposable
    {
        [XmlAttribute("type")]
        public TmdbImageType Type { get; set; }

        [XmlAttribute("size")]
        public TmdbImageSize Size { get; set; }

        [XmlAttribute("url")]
        public string Url { get; set; }

        [XmlAttribute("id")]
        public string Id { get; set; }

        [XmlAttribute("width")]
        public int Width { get; set; }

        [XmlAttribute("height")]
        public int Height { get; set; }

        public Image Image { get; set; }
        
        public void LoadImage()
        {
            if (this.Image == null)
            {
                Image = LoadImage(Url);  
            }
        }

        public static Image LoadImage(string Url)
        {
            try
            {
                WebClient webClient = new WebClient();

                string filePath = System.IO.Path.Combine(DataCaching.GetFolder(), TmdbAPI.Escape(Url));

                byte[] data = DataCaching.GetBytesFromFile(filePath);
                if (data == null)
                {
                    data = webClient.DownloadData(Url);
                    DataCaching.SaveBytesToFile(filePath, data);
                }

                using (MemoryStream stream = new MemoryStream(data))
                {
                    return Image.FromStream(stream);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (Image != null)
            {
                Image.Dispose();
                Image = null;
            }
        }

        #endregion
    }
}
