﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace TheMovieDB
{
    [XmlType("genre")]
    public class TmdbGenre
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("id")]
        public int ID { get; set; }

        [XmlElement("url")]
        public string Url { get; set; }

        public static string Build(TmdbGenre[] genres, params string[] args)
        {
            StringBuilder result = new StringBuilder();

            foreach (string genreName in args)
            {
                foreach (TmdbGenre genre in genres)
                {
                    if (genre.Name == genreName)
                    {
                        if (result.Length > 0)
                        {
                            result.Append(",");
                        }
                        result.Append(genre.ID);
                        break;
                    }
                }
            }

            return result.ToString();
        }
    }
}
