﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheMovieDB
{
    public enum TmdbImageSize
    {
        original,
        cover,
        mid,
        thumb,
        poster,
        profile,
        w342,
        w154,
        w1280
    }
}
