﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;

namespace MovieLab
{
    public class DataCaching
    {
        public static string GetFileListName() 
        {
            string listName = System.IO.Path.Combine(GetFolder(), "List.cache");
            return listName;
        }

        public static string GetFileMovieName(string movie)
        {
            string listName = System.IO.Path.Combine(GetFolder(), "Movie "+movie+".cache");
            return listName;
        }

        public static string GetFileMovieBrowseName(string args)
        {
            string listName = System.IO.Path.Combine(GetFolder(), "Browse " + args + ".cache");
            return listName;
        }

        public static string GetFileMovieIMDBidName(string id)
        {
            string listName = System.IO.Path.Combine(GetFolder(), "IMDBID " + id + ".cache");
            return listName;
        }

        public static string GetFileMovieIDName(string id)
        {
            string listName = System.IO.Path.Combine(GetFolder(), "ID " + id + ".cache");
            return listName;
        }

        public static string GetFileImageName(string url)
        {
            string listName = System.IO.Path.Combine(GetFolder(), "image " + url + ".cache");
            return listName;
        }

        public static string GetFilePersonName(string person)
        {
            string listName = System.IO.Path.Combine(GetFolder(), "person " + person + ".cache");
            return listName;
        }

        public static string GetFilePersonIdName(string id)
        {
            string listName = System.IO.Path.Combine(GetFolder(), "personid " + id + ".cache");
            return listName;
        }



        public static string GetFolder()
        {
            string currentAssemblyDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            currentAssemblyDirectoryName = Path.Combine(currentAssemblyDirectoryName, "cache");
            return currentAssemblyDirectoryName;
        }

        public static byte[] GetBytesFromFile(string fullFilePath)
        {
            if (!System.IO.File.Exists(fullFilePath))
            {
                return null;	
            }            

            FileStream fileStream = File.OpenRead(fullFilePath);
            try
            {
                byte[] bytes = new byte[fileStream.Length];
                fileStream.Read(bytes, 0, Convert.ToInt32(fileStream.Length));
                fileStream.Close();
                return bytes;
            }
            catch
            {
                return null;
            }
            finally
            {
                fileStream.Close();
            }
        }

        public static bool SaveBytesToFile(string fullFilePath, byte[] bytes)
        {
            try
            {   
                System.IO.FileStream fileStream = new System.IO.FileStream(fullFilePath, System.IO.FileMode.Create, System.IO.FileAccess.Write );
                fileStream.Write(bytes, 0, bytes.Length);
                fileStream.Close();
                return true;
            }
            catch (Exception ex)
            {                
                Console.WriteLine("Exception caught in process: {0}", ex.ToString());
            }
            
            return false;
        }
    }
}
