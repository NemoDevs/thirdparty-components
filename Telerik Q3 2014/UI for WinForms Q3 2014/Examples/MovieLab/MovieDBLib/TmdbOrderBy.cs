﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheMovieDB
{
    public enum TmdbOrderBy
    {
        none, rating, release, title
    }
}
