﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.239
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System


'This class was auto-generated by the StronglyTypedResourceBuilder
'class via a tool like ResGen or Visual Studio.
'To add or remove a member, edit your .ResX file then rerun ResGen
'with the /str option, or rebuild your VS project.
'''<summary>
'''  A strongly-typed resource class, for looking up localized strings, etc.
'''</summary>
<Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"),  _
 Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
 Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>  _
Friend Class Resources
    
    Private Shared resourceMan As Global.System.Resources.ResourceManager
    
    Private Shared resourceCulture As Global.System.Globalization.CultureInfo
    
    <Global.System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>  _
    Friend Sub New()
        MyBase.New
    End Sub
    
    '''<summary>
    '''  Returns the cached ResourceManager instance used by this class.
    '''</summary>
    <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
    Friend Shared ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
        Get
            If Object.ReferenceEquals(resourceMan, Nothing) Then
                Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("Resources", GetType(Resources).Assembly)
                resourceMan = temp
            End If
            Return resourceMan
        End Get
    End Property
    
    '''<summary>
    '''  Overrides the current thread's CurrentUICulture property for all
    '''  resource lookups using this strongly typed resource class.
    '''</summary>
    <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
    Friend Shared Property Culture() As Global.System.Globalization.CultureInfo
        Get
            Return resourceCulture
        End Get
        Set
            resourceCulture = value
        End Set
    End Property
    
    Friend Shared ReadOnly Property back() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("back", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property backdropTitleLeft() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("backdropTitleLeft", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property backdropTitleRight() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("backdropTitleRight", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property bigStar2Off() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("bigStar2Off", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property bigStar2On() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("bigStar2On", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property bigStarOff() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("bigStarOff", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property bigStarOn() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("bigStarOn", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property bottomLines() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("bottomLines", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property bottomLines2() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("bottomLines2", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonAINormal() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonAINormal", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonAISelected() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonAISelected", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonAllDown() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonAllDown", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonAllHover() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonAllHover", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonAllNormal() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonAllNormal", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonFilterHover() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonFilterHover", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonFilterNormal() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonFilterNormal", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonFilterSelected() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonFilterSelected", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonGo() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonGo", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonGoHover() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonGoHover", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonGrid() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonGrid", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonGridActive() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonGridActive", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonGridDown() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonGridDown", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonGridHover() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonGridHover", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonGridNormal() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonGridNormal", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonList() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonList", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonListActive() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonListActive", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonListDown() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonListDown", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonListHover() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonListHover", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonListNormal() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonListNormal", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonNewDown() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonNewDown", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonNewHover() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonNewHover", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonNewNormal() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonNewNormal", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonTopDown() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonTopDown", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonTopHover() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonTopHover", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonTopNormal() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonTopNormal", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonWishList() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonWishList", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonWishListDown() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonWishListDown", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property buttonWishListHover() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("buttonWishListHover", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property cast_img_na() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("cast_img_na", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property castDown() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("castDown", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property castHover() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("castHover", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property castHower() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("castHower", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property castNormal() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("castNormal", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property crewDown() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("crewDown", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property crewHover() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("crewHover", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property crewHower() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("crewHower", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property crewNormal() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("crewNormal", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property form_lines_left() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("form_lines_left", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property form_lines_right() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("form_lines_right", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property GreenCarbon() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("GreenCarbon", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property hline() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("hline", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property hline2() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("hline2", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property home() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("home", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property logo() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("logo", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property na_img() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("na_img", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property newArrivals() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("newArrivals", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property our_movies() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("our_movies", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property pictureHolder() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("pictureHolder", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property removeFromWishlist() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("removeFromWishlist", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property scrollLeft() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("scrollLeft", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property scrollLeftDown() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("scrollLeftDown", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property scrollLeftHover() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("scrollLeftHover", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property scrollRight() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("scrollRight", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property scrollRightDown() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("scrollRightDown", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property scrollRightHover() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("scrollRightHover", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property searchIcon() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("searchIcon", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property shadowBackground() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("shadowBackground", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property sizingGrip() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("sizingGrip", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property starOff() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("starOff", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property starOn() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("starOn", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property title_home() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("title_home", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property title_left() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("title_left", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property title_left_normal() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("title_left_normal", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
    
    Friend Shared ReadOnly Property waiting_bar() As System.Drawing.Bitmap
        Get
            Dim obj As Object = ResourceManager.GetObject("waiting_bar", resourceCulture)
            Return CType(obj,System.Drawing.Bitmap)
        End Get
    End Property
End Class
