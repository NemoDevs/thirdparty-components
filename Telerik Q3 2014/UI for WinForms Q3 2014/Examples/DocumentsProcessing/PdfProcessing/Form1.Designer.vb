﻿Namespace PdfProcessing
	Partial Public Class Form1
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(Form1))
			Me.pictureBox1 = New PictureBox()
			Me.dateTimePicker1 = New DateTimePicker()
			Me.buttonSave = New Telerik.WinControls.UI.RadButton()
			Me.telerikMetroTheme1 = New Telerik.WinControls.Themes.TelerikMetroTheme()
			CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.buttonSave, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.SuspendLayout()
			' 
			' pictureBox1
			' 
			Me.pictureBox1.Image = (CType(resources.GetObject("pictureBox1.Image"), Image))
            Me.pictureBox1.Location = New System.Drawing.Point(13, 13)
			Me.pictureBox1.Name = "pictureBox1"
            Me.pictureBox1.Size = New System.Drawing.Size(488, 572)
			Me.pictureBox1.TabIndex = 0
			Me.pictureBox1.TabStop = False
			' 
			' dateTimePicker1
			' 
            Me.dateTimePicker1.Location = New System.Drawing.Point(678, 545)
			Me.dateTimePicker1.Name = "dateTimePicker1"
            Me.dateTimePicker1.Size = New System.Drawing.Size(200, 20)
			Me.dateTimePicker1.TabIndex = 1
			' 
			' buttonSave
			' 
            Me.buttonSave.Location = New System.Drawing.Point(205, 602)
			Me.buttonSave.Name = "buttonSave"
            Me.buttonSave.Size = New System.Drawing.Size(110, 24)
			Me.buttonSave.TabIndex = 2
			Me.buttonSave.Text = "Save Document"
			Me.buttonSave.ThemeName = "TelerikMetro"
'			Me.buttonSave.Click += New System.EventHandler(Me.buttonSave_Click)
			' 
			' Form1
			' 
			Me.AutoScaleDimensions = New SizeF(6F, 13F)
			Me.AutoScaleMode = AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(512, 647)
			Me.Controls.Add(Me.buttonSave)
			Me.Controls.Add(Me.dateTimePicker1)
			Me.Controls.Add(Me.pictureBox1)
			Me.FormBorderStyle = FormBorderStyle.FixedSingle
			Me.MaximizeBox = False
			Me.Name = "Form1"
			' 
			' 
			' 
			Me.RootElement.ApplyShapeToControl = True
			Me.Text = "PdfProcessing"
			Me.ThemeName = "TelerikMetro"
			CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.buttonSave, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
			Me.ResumeLayout(False)

		End Sub

		#End Region

		Private pictureBox1 As PictureBox
		Private dateTimePicker1 As DateTimePicker
		Private WithEvents buttonSave As Telerik.WinControls.UI.RadButton
		Private telerikMetroTheme1 As Telerik.WinControls.Themes.TelerikMetroTheme
	End Class
End Namespace

