﻿Imports System.Collections
Imports System.ComponentModel
Imports System.Text
Imports Telerik.WinControls.Data
Imports Telerik.WinControls.UI
Imports Telerik.Windows.Documents.Common.FormatProviders
Imports Telerik.Windows.Documents.Flow.FormatProviders.Docx
Imports Telerik.Windows.Documents.Flow.FormatProviders.Rtf
Imports Telerik.Windows.Documents.Flow.FormatProviders.Txt
Imports Telerik.Windows.Documents.Flow.Model
Imports Telerik.Windows.Documents.Flow.Model.Styles
Imports Telerik.Windows.Documents.Spreadsheet.Model
Imports System.Drawing

Namespace WordGridIntegration
	Partial Public Class Form1
		Inherits RadForm
		Private Const WidthOfIndentColumns As Integer = 20

		Private Shared ReadOnly DefaultHeaderRowColor As Color = Color.FromArgb(255, 127, 127, 127)
		Private Shared ReadOnly DefaultGroupHeaderRowColor As Color = Color.FromArgb(255, 216, 216, 216)
		Private Shared ReadOnly DefaultDataRowColor As Color = Color.FromArgb(255, 251, 247, 255)

		Private _products As List(Of Product)
		Private _headerRowColor As Color
		Private _dataRowColor As Color
		Private _groupHeaderRowColor As Color
		Private _exportFormats() As String
		Private _selectedExportFormat As String
		Private _repeatHeaderRowOnEveryPage As Boolean = True

		Public Sub New()
			InitializeComponent()
		End Sub

		Public Property Products() As List(Of Product)
			Get
				Return Me._products
			End Get
			Set(ByVal value As List(Of Product))
				If Me._products IsNot value Then
					Me._products = value
				End If
			End Set
		End Property


		Public Property HeaderRowColor() As Color
			Get
				Return Me._headerRowColor
			End Get
			Set(ByVal value As Color)
				If Me._headerRowColor <> value Then
					Me._headerRowColor = value
				End If
			End Set
		End Property

		Public Property DataRowColor() As Color
			Get
				Return Me._dataRowColor
			End Get
			Set(ByVal value As Color)
				If Me._dataRowColor <> value Then
					Me._dataRowColor = value
				End If
			End Set
		End Property

		Public Property GroupHeaderRowColor() As Color
			Get
				Return Me._groupHeaderRowColor
			End Get
			Set(ByVal value As Color)
				If Me._groupHeaderRowColor <> value Then
					Me._groupHeaderRowColor = value
				End If
			End Set
		End Property

		Public ReadOnly Property ExportFormats() As IEnumerable(Of String)
			Get
				If _exportFormats Is Nothing Then
					_exportFormats = New String() { "Docx", "Rtf", "Txt" }
				End If

				Return _exportFormats
			End Get
		End Property

		Public Property SelectedExportFormat() As String
			Get
				Return _selectedExportFormat
			End Get
			Set(ByVal value As String)
				If Not Object.Equals(_selectedExportFormat, value) Then
					_selectedExportFormat = value
				End If
			End Set
		End Property

		Public Property RepeatHeaderRowOnEveryPage() As Boolean
			Get
				Return Me._repeatHeaderRowOnEveryPage
			End Get
			Set(ByVal value As Boolean)
				If Me._repeatHeaderRowOnEveryPage <> value Then
					Me._repeatHeaderRowOnEveryPage = value
				End If
			End Set
		End Property

		Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
			Me.Products = New Products().GetData(100).ToList()

			Me.SelectedExportFormat = Me.ExportFormats.First()

			Me.HeaderRowColor = DefaultHeaderRowColor
			Me.DataRowColor = DefaultDataRowColor
			Me.GroupHeaderRowColor = DefaultGroupHeaderRowColor

			Me.radGridView1.DataSource = Me.Products
			Me.radGridView1.BestFitColumns()
			Me.radGridView1.Columns("UnitPrice").FormatString = "{0:C}"
			Me.radGridView1.Columns("Date").DataType = GetType(Date)
			Me.radGridView1.Columns("Date").FormatString = "{0:d}"
			Me.radGridView1.Columns.Remove(Me.radGridView1.Columns("SubTotal"))
			Me.radGridView1.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill

			Me.radGridView1.AutoExpandGroups = True
			Dim descriptor As New GroupDescriptor()
			descriptor.GroupNames.Add("UnitPrice", ListSortDirection.Ascending)
			Me.radGridView1.GroupDescriptors.Add(descriptor)


			headerRowColorBox.Value = DefaultHeaderRowColor
			groupHeaderColorBox.Value = DefaultGroupHeaderRowColor
			dataRowColorBox.Value = DefaultDataRowColor
			Me.exportFormatDropDownList.DataSource = ExportFormats
		End Sub

		Private Sub exportButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles exportButton.Click
			Dim document As RadFlowDocument = Me.CreateDocument(radGridView1)

			Dim selectedFromat As String = Me.SelectedExportFormat
			SaveDocument(document, selectedFromat)
		End Sub
		Public Shared Sub SaveDocument(ByVal document As RadFlowDocument, ByVal selectedFromat As String)
			Dim formatProvider As IFormatProvider(Of RadFlowDocument) = Nothing
			Select Case selectedFromat
				Case "Docx"
					formatProvider = New DocxFormatProvider()
				Case "Rtf"
					formatProvider = New RtfFormatProvider()
				Case "Txt"
					formatProvider = New TxtFormatProvider()
			End Select
			If formatProvider Is Nothing Then
				Return
			End If

			Dim dialog As New SaveFileDialog()
			dialog.Filter = String.Format("{0} files|*{1}|All files (*.*)|*.*", selectedFromat, formatProvider.SupportedExtensions.First())
			dialog.FilterIndex = 1

			If dialog.ShowDialog() = DialogResult.OK Then
				Using stream = dialog.OpenFile()
					formatProvider.Export(document, stream)
				End Using
			End If
		End Sub
		Private Function CreateDocument(ByVal grid As RadGridView) As RadFlowDocument
			Dim document As New RadFlowDocument()
			Dim table As Table = document.Sections.AddSection().Blocks.AddTable()
			document.StyleRepository.AddBuiltInStyle(BuiltInStyleNames.TableGridStyleId)
			table.StyleId = BuiltInStyleNames.TableGridStyleId

            Dim columns As IList(Of GridViewColumn) = (From c In grid.Columns.OfType(Of GridViewColumn)() Order By c.Index).ToList()
            Dim indentColumns As Integer = grid.GroupDescriptors.Count
            Dim rowIndex As Integer = 0

            If grid.ShowColumnHeaders Then
                Dim headerRow As TableRow = table.Rows.AddTableRow()
                headerRow.RepeatOnEveryPage = Me.RepeatHeaderRowOnEveryPage
                Dim headerBackground As ThemableColor = Me.ConvertColor(Me.HeaderRowColor)

                If grid.GroupDescriptors.Count > 0 Then
                    Me.AddIndentCell(headerRow, indentColumns, headerBackground)
                End If

                For i As Integer = 0 To Columns.Count - 1
                    Dim cell As TableCell = headerRow.Cells.AddTableCell()
                    cell.Shading.BackgroundColor = headerBackground
                    cell.PreferredWidth = New TableWidthUnit(Columns(i).Width)
                    Dim headerRun As Run = cell.Blocks.AddParagraph().Inlines.AddRun(Columns(i).Name)
                    headerRun.FontWeight = FontWeights.Bold
                Next i
            End If

            If grid.Groups.Count > 0 Then
                For Each group As DataGroup In grid.Groups
                    rowIndex = Me.AddGroupRow(table, rowIndex, group.Level, group, Columns)
                    Me.AddDataRows(table, rowIndex, group.Level + 1, group, Columns)
                Next group
            Else
                Me.AddDataRows(table, rowIndex, 0, grid.Rows, Columns)
            End If

            document.Sections.First().Blocks.AddParagraph()
            Return document
        End Function

		Private Sub AddDataRows(ByVal table As Table, ByVal startRowIndex As Integer, ByVal indentColumnsstartColumnIndex As Integer, ByVal rows As IEnumerable(Of Telerik.WinControls.UI.GridViewRowInfo), ByVal columns As IList(Of GridViewColumn))
			Dim background As ThemableColor = ConvertColor(Me.DataRowColor)
			For Each row In rows
				Dim tableRows As TableRow = table.Rows.AddTableRow()
				If indentColumnsstartColumnIndex > 0 Then
					Me.AddIndentCell(tableRows, indentColumnsstartColumnIndex, background)
				End If

				For columnIndex As Integer = 0 To columns.Count - 1
					Dim cell As TableCell = tableRows.Cells.AddTableCell()
					Me.AddCellValue(cell, row.Cells(columnIndex).Value)
					cell.Shading.BackgroundColor = background
					cell.PreferredWidth = New TableWidthUnit(columns(columnIndex).Width)
				Next columnIndex
			Next row


		End Sub

		Private Function AddGroupRow(ByVal table As Table, ByVal rowIndex As Integer, ByVal numberOfIndentCells As Integer, ByVal group As DataGroup, ByVal columns As IList(Of GridViewColumn)) As Integer
			Dim level As Integer = Me.GetGroupLevel(group)
			Dim row As TableRow = table.Rows.AddTableRow()
			If level > 0 Then
				Me.AddIndentCell(row, level, ConvertColor(Me.DataRowColor))
			End If
			Dim cell As TableCell = row.Cells.AddTableCell()
			cell.Shading.BackgroundColor = ConvertColor(Me.GroupHeaderRowColor)
			cell.ColumnSpan = columns.Count + (If(radGridView1.GroupDescriptors.Count > 0, 1, 0)) - (If(level > 0, 1, 0))
			Me.AddCellValue(cell, group.GroupRow.HeaderText)
			Return rowIndex
		End Function

		Private Sub AddCellValue(ByVal cell As TableCell, ByVal value As Object)
			Dim stringValue As String = If(value IsNot Nothing, value.ToString(), String.Empty)
			cell.Blocks.AddParagraph().Inlines.AddRun(stringValue)
		End Sub

		Private Sub AddIndentCell(ByVal row As TableRow, ByVal indentColumns As Integer, ByVal background As ThemableColor)
			Dim indentCell As TableCell = row.Cells.AddTableCell()
			indentCell.PreferredWidth = New TableWidthUnit(indentColumns * WidthOfIndentColumns)
			indentCell.Shading.BackgroundColor = background
			indentCell.Blocks.AddParagraph()
		End Sub

		Private Function GetGroupLevel(ByVal group As DataGroup) As Integer
			Return group.Level
		End Function

		Private Function ConvertColor(ByVal color As Color) As ThemableColor
			Return ThemableColor.FromArgb(color.A, color.R, color.G, color.B)
		End Function

		Private Sub radCheckBox1_CheckStateChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radCheckBox1.CheckStateChanged
			Me.RepeatHeaderRowOnEveryPage = Me.radCheckBox1.Checked
        End Sub

        Private Sub exportFormatDropDownList_SelectedIndexChanged(sender As System.Object, e As Telerik.WinControls.UI.Data.PositionChangedEventArgs) Handles exportFormatDropDownList.SelectedIndexChanged
            Me.SelectedExportFormat = exportFormatDropDownList.Items(e.Position).Text
        End Sub
	End Class
End Namespace
