﻿Imports System.IO
Imports System.Linq
Imports System.Reflection
Imports Microsoft.Win32
Imports Telerik.Windows.Documents.Common.FormatProviders
Imports Telerik.Windows.Documents.Flow.FormatProviders.Docx
Imports Telerik.Windows.Documents.Flow.FormatProviders.Rtf
Imports Telerik.Windows.Documents.Flow.FormatProviders.Txt
Imports Telerik.Windows.Documents.Flow.Model

Namespace WordGeneration
    Public NotInheritable Class FileHelper
        Private Shared ReadOnly SampleDataFolder As String = "SampleData/"

        Private Sub New()
        End Sub
        Public Shared Sub SaveDocument(ByVal document As RadFlowDocument, ByVal selectedFromat As String)
            Dim formatProvider As IFormatProvider(Of RadFlowDocument) = Nothing
            Select Case selectedFromat
                Case "Docx"
                    formatProvider = New DocxFormatProvider()
                Case "Rtf"
                    formatProvider = New RtfFormatProvider()
                Case "Txt"
                    formatProvider = New TxtFormatProvider()
            End Select
            If formatProvider Is Nothing Then
                Return
            End If

            Dim dialog As New SaveFileDialog()
            dialog.Filter = String.Format("{0} files|*{1}|All files (*.*)|*.*", selectedFromat, formatProvider.SupportedExtensions.First())
            dialog.FilterIndex = 1

            If dialog.ShowDialog() = DialogResult.OK Then
                Using stream = dialog.OpenFile()
                    formatProvider.Export(document, stream)
                End Using
            End If
        End Sub

        Public Shared Function GetSampleResourceStream(ByVal resource As String) As Stream
            Dim [assembly] = System.Reflection.Assembly.GetExecutingAssembly()
            Dim stream As Stream = [assembly].GetManifestResourceStream(resource)
            If stream IsNot Nothing Then
                Return stream
            End If

            Return Nothing
        End Function

        Private Shared Function GetResourceUri(ByVal resource As String) As Uri
            Dim assemblyName As New AssemblyName(GetType(FileHelper).Assembly.FullName)
            Dim resourcePath As String = "/" & assemblyName.Name & ";component/" & resource
            Dim resourceUri As New Uri(resourcePath, UriKind.Relative)

            Return resourceUri
        End Function
    End Class
End Namespace
