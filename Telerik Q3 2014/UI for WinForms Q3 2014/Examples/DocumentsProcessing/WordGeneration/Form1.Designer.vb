﻿Imports System.Drawing
Namespace WordGeneration
    Partial Public Class Form1
        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Windows Form Designer generated code"

        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Me.telerikMetroTheme1 = New Telerik.WinControls.Themes.TelerikMetroTheme()
            Me.radLabel1 = New Telerik.WinControls.UI.RadLabel()
            Me.exportFormatDropDownList = New Telerik.WinControls.UI.RadDropDownList()
            Me.exportButton = New Telerik.WinControls.UI.RadButton()
            Me.radCheckBox1 = New Telerik.WinControls.UI.RadCheckBox()
            Me.radCheckBox2 = New Telerik.WinControls.UI.RadCheckBox()
            Me.pictureBox1 = New PictureBox()
            Me.radLabel2 = New Telerik.WinControls.UI.RadLabel()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.exportFormatDropDownList, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.exportButton, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radCheckBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radCheckBox2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            ' 
            ' radLabel1
            ' 
            Me.radLabel1.Location = New Point(111, 567)
            Me.radLabel1.Name = "radLabel1"
            Me.radLabel1.Size = New Size(45, 16)
            Me.radLabel1.TabIndex = 1
            Me.radLabel1.Text = "Format:"
            Me.radLabel1.ThemeName = "TelerikMetro"
            ' 
            ' exportFormatDropDownList
            ' 
            Me.exportFormatDropDownList.AllowShowFocusCues = False
            Me.exportFormatDropDownList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            Me.exportFormatDropDownList.Location = New Point(43, 565)
            Me.exportFormatDropDownList.Name = "exportFormatDropDownList"
            Me.exportFormatDropDownList.Size = New Size(62, 19)
            Me.exportFormatDropDownList.TabIndex = 2
            Me.exportFormatDropDownList.Text = "radDropDownList1"
            Me.exportFormatDropDownList.ThemeName = "TelerikMetro"
            ' 
            ' exportButton
            ' 
            Me.exportButton.Location = New Point(43, 651)
            Me.exportButton.Name = "exportButton"
            Me.exportButton.Size = New Size(388, 24)
            Me.exportButton.TabIndex = 3
            Me.exportButton.Text = "Generate"
            Me.exportButton.ThemeName = "TelerikMetro"
            '			Me.exportButton.Click += New System.EventHandler(Me.exportButton_Click)
            ' 
            ' radCheckBox1
            ' 
            Me.radCheckBox1.CheckState = CheckState.Checked
            Me.radCheckBox1.Location = New Point(43, 593)
            Me.radCheckBox1.Name = "radCheckBox1"
            Me.radCheckBox1.Size = New Size(106, 19)
            Me.radCheckBox1.TabIndex = 4
            Me.radCheckBox1.Text = "Include Header"
            Me.radCheckBox1.ThemeName = "TelerikMetro"
            Me.radCheckBox1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
            '			Me.radCheckBox1.CheckStateChanged += New System.EventHandler(Me.radCheckBox1_CheckStateChanged)
            ' 
            ' radCheckBox2
            ' 
            Me.radCheckBox2.CheckState = CheckState.Checked
            Me.radCheckBox2.Location = New Point(43, 621)
            Me.radCheckBox2.Name = "radCheckBox2"
            Me.radCheckBox2.Size = New Size(102, 19)
            Me.radCheckBox2.TabIndex = 5
            Me.radCheckBox2.Text = "Include Footer"
            Me.radCheckBox2.ThemeName = "TelerikMetro"
            Me.radCheckBox2.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
            '			Me.radCheckBox2.CheckStateChanged += New System.EventHandler(Me.radCheckBox2_CheckStateChanged)
            ' 
            ' pictureBox1
            ' 
            Me.pictureBox1.BorderStyle = BorderStyle.FixedSingle
            Me.pictureBox1.Image = My.Resources.EmailTemplate
            Me.pictureBox1.Location = New Point(43, 35)
            Me.pictureBox1.Name = "pictureBox1"
            Me.pictureBox1.Size = New Size(388, 492)
            Me.pictureBox1.TabIndex = 6
            Me.pictureBox1.TabStop = False
            ' 
            ' radLabel2
            ' 
            Me.radLabel2.Location = New Point(48, 13)
            Me.radLabel2.Name = "radLabel2"
            Me.radLabel2.Size = New Size(85, 16)
            Me.radLabel2.TabIndex = 7
            Me.radLabel2.Text = "Email Template"
            Me.radLabel2.ThemeName = "TelerikMetro"
            ' 
            ' Form1
            ' 
            Me.AutoScaleDimensions = New SizeF(6.0F, 13.0F)
            Me.AutoScaleMode = AutoScaleMode.Font
            Me.ClientSize = New Size(495, 690)
            Me.Controls.Add(Me.radLabel2)
            Me.Controls.Add(Me.pictureBox1)
            Me.Controls.Add(Me.radCheckBox2)
            Me.Controls.Add(Me.radCheckBox1)
            Me.Controls.Add(Me.exportButton)
            Me.Controls.Add(Me.exportFormatDropDownList)
            Me.Controls.Add(Me.radLabel1)
            Me.FormBorderStyle = FormBorderStyle.FixedSingle
            Me.MaximizeBox = False
            Me.Name = "Form1"
            ' 
            ' 
            ' 
            Me.RootElement.ApplyShapeToControl = True
            Me.Text = "Generate Documents"
            Me.ThemeName = "TelerikMetro"
            '			Me.Load += New System.EventHandler(Me.Form1_Load)
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.exportFormatDropDownList, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.exportButton, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radCheckBox1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radCheckBox2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private telerikMetroTheme1 As Telerik.WinControls.Themes.TelerikMetroTheme
        Private radLabel1 As Telerik.WinControls.UI.RadLabel
        Private exportFormatDropDownList As Telerik.WinControls.UI.RadDropDownList
        Private WithEvents exportButton As Telerik.WinControls.UI.RadButton
        Private WithEvents radCheckBox1 As Telerik.WinControls.UI.RadCheckBox
        Private WithEvents radCheckBox2 As Telerik.WinControls.UI.RadCheckBox
        Private pictureBox1 As PictureBox
        Private radLabel2 As Telerik.WinControls.UI.RadLabel
    End Class
End Namespace

