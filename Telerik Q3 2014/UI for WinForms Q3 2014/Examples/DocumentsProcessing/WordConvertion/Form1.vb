﻿Imports System.IO
Imports Telerik.WinControls.UI
Imports Telerik.Windows.Documents.Common.FormatProviders
Imports Telerik.Windows.Documents.Flow.FormatProviders
Imports Telerik.Windows.Documents.Flow.FormatProviders.Docx
Imports Telerik.Windows.Documents.Flow.FormatProviders.Rtf
Imports Telerik.Windows.Documents.Flow.FormatProviders.Txt
Imports Telerik.Windows.Documents.Flow.Model


Namespace WordConvertion
    Partial Public Class Form1
        Inherits RadForm
        Private Shared ReadOnly SampleDocumentFile As String = "SampleDocument.docx"
        Private providers As List(Of IFormatProvider(Of RadFlowDocument))
        Public Property Document() As RadFlowDocument

        Public Sub New()
            InitializeComponent()
        End Sub

        Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            Me.fileExtensionsDropDownList.SelectedIndex = 0
            Me.providers = New List(Of IFormatProvider(Of RadFlowDocument))() From {New DocxFormatProvider(), New RtfFormatProvider(), New TxtFormatProvider()}
        End Sub

        Private Sub loadCustomDocumentButton_Click(ByVal sender As Object, ByVal args As EventArgs) Handles loadCustomDocumentButton.Click
            Dim dialog As New OpenFileDialog()
            dialog.Filter = "Docx files|*.docx|Rtf files|*.rtf|Text files|*.txt|All files (*.*)|*.*"
            dialog.FilterIndex = 1
            If dialog.ShowDialog() = DialogResult.OK Then
                Dim extension As String = Path.GetExtension(dialog.FileName)
                Dim provider As IFormatProvider(Of RadFlowDocument) = Me.providers.FirstOrDefault(Function(p) p.SupportedExtensions.Any(Function(e) String.Compare(extension, e, StringComparison.InvariantCultureIgnoreCase) = 0))

                If provider IsNot Nothing Then

                    Try
                        Using stream As Stream = dialog.OpenFile()
                            Me.Document = provider.Import(stream)
                            Me.FileName = Path.GetFileName(dialog.FileName)
                        End Using
                    Catch e1 As Exception
                        MessageBox.Show("Could not open file.")
                        Me.Document = Nothing
                        Me.FileName = Nothing
                    End Try

                Else
                    MessageBox.Show("Could not open file.")
                End If
            End If
        End Sub


        Private isDocumentLoaded_Renamed As Boolean
        Public Property IsDocumentLoaded() As Boolean
            Get
                Return Me.isDocumentLoaded_Renamed
            End Get
            Set(ByVal value As Boolean)
                If Me.isDocumentLoaded_Renamed <> value Then
                    Me.isDocumentLoaded_Renamed = value
                End If
            End Set
        End Property


        Private fileName_Renamed As String
        Public Property FileName() As String
            Get
                Return Me.fileName_Renamed
            End Get
            Set(ByVal value As String)
                If Me.fileName_Renamed <> value Then
                    Me.fileName_Renamed = value
                    Me.fileNameLabel.Text = value
                End If
            End Set
        End Property

        Private exportFormats_Renamed As IEnumerable(Of String)
        Public ReadOnly Property ExportFormats() As IEnumerable(Of String)
            Get
                If exportFormats_Renamed Is Nothing Then
                    exportFormats_Renamed = New String() {"Docx", "Rtf", "Txt"}
                End If

                Return exportFormats_Renamed
            End Get
        End Property

        Private Sub loadSampleDocumentButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles loadSampleDocumentButton.Click
            Using stream As Stream = FileHelper.GetSampleResourceStream(SampleDocumentFile)
                Me.Document = New DocxFormatProvider().Import(stream)
                Me.FileName = SampleDocumentFile
            End Using
        End Sub

        Private Sub saveButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles saveButton.Click
            Dim selectedFromat As String = Me.fileExtensionsDropDownList.Text
            FileHelper.SaveDocument(Me.Document, selectedFromat)
        End Sub
    End Class
End Namespace
