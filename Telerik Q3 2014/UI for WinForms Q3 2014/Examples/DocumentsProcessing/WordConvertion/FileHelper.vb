﻿Imports System.IO
Imports System.Reflection
Imports Telerik.Windows.Documents.Common.FormatProviders
Imports Telerik.Windows.Documents.Flow.FormatProviders
Imports Telerik.Windows.Documents.Flow.FormatProviders.Docx
Imports Telerik.Windows.Documents.Flow.FormatProviders.Rtf
Imports Telerik.Windows.Documents.Flow.FormatProviders.Txt
Imports Telerik.Windows.Documents.Flow.Model

Namespace WordConvertion
    Public NotInheritable Class FileHelper
        Private Sub New()
        End Sub
        Public Shared Sub SaveDocument(ByVal document As RadFlowDocument, ByVal selectedFromat As String)
            If document Is Nothing Then Exit Sub
            Dim formatProvider As IFormatProvider(Of RadFlowDocument) = Nothing
            Select Case selectedFromat
                Case "Docx"
                    formatProvider = New DocxFormatProvider()
                Case "Rtf"
                    formatProvider = New RtfFormatProvider()
                Case "Txt"
                    formatProvider = New TxtFormatProvider()
            End Select
            If formatProvider Is Nothing Then
                Return
            End If

            Dim dialog As New SaveFileDialog()
            dialog.Filter = String.Format("{0} files|*{1}|All files (*.*)|*.*", selectedFromat, formatProvider.SupportedExtensions.First())
            dialog.FilterIndex = 1

            If dialog.ShowDialog() = DialogResult.OK Then
                Using stream = dialog.OpenFile()
                    formatProvider.Export(document, stream)
                End Using
            End If
        End Sub

        Public Shared Function GetSampleResourceStream(ByVal resource As String) As Stream
            Dim [assembly] = System.Reflection.Assembly.GetExecutingAssembly()
            Dim stream As Stream = [assembly].GetManifestResourceStream(resource)
            If stream IsNot Nothing Then
                Return stream
            End If

            Return Nothing
        End Function
    End Class
End Namespace
