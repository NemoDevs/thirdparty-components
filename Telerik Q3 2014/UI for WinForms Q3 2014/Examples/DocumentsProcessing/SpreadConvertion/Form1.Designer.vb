﻿Namespace SpreadConvertion
    Partial Public Class Form1
        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Windows Form Designer generated code"

        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Dim radListDataItem1 As New Telerik.WinControls.UI.RadListDataItem()
            Dim radListDataItem2 As New Telerik.WinControls.UI.RadListDataItem()
            Dim radListDataItem3 As New Telerik.WinControls.UI.RadListDataItem()
            Me.pictureBox1 = New PictureBox()
            Me.label1 = New Label()
            Me.pictureBox2 = New PictureBox()
            Me.loadCustomDocumentButton = New Telerik.WinControls.UI.RadButton()
            Me.loadSampleDocumentButton = New Telerik.WinControls.UI.RadButton()
            Me.radLabel1 = New Telerik.WinControls.UI.RadLabel()
            Me.fileNameLabel = New Telerik.WinControls.UI.RadLabel()
            Me.radLabel2 = New Telerik.WinControls.UI.RadLabel()
            Me.fileExtensionsDropDownList = New Telerik.WinControls.UI.RadDropDownList()
            Me.saveButton = New Telerik.WinControls.UI.RadButton()
            Me.telerikMetroTheme1 = New Telerik.WinControls.Themes.TelerikMetroTheme()
            CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.pictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.loadCustomDocumentButton, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.loadSampleDocumentButton, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.fileNameLabel, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.fileExtensionsDropDownList, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.saveButton, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            ' 
            ' pictureBox1
            ' 
            Me.pictureBox1.BorderStyle = BorderStyle.FixedSingle
            Me.pictureBox1.Image = My.Resources.CustomDocumentImage
            Me.pictureBox1.Location = New Point(27, 30)
            Me.pictureBox1.Name = "pictureBox1"
            Me.pictureBox1.Size = New Size(262, 200)
            Me.pictureBox1.TabIndex = 0
            Me.pictureBox1.TabStop = False
            ' 
            ' label1
            ' 
            Me.label1.AutoSize = True
            Me.label1.Location = New Point(296, 119)
            Me.label1.Name = "label1"
            Me.label1.Size = New Size(38, 13)
            Me.label1.TabIndex = 1
            Me.label1.Text = "- OR - "
            ' 
            ' pictureBox2
            ' 
            Me.pictureBox2.BorderStyle = BorderStyle.FixedSingle
            Me.pictureBox2.Image = My.Resources.SampleDocumentImage
            Me.pictureBox2.Location = New Point(340, 30)
            Me.pictureBox2.Name = "pictureBox2"
            Me.pictureBox2.Size = New Size(262, 200)
            Me.pictureBox2.TabIndex = 2
            Me.pictureBox2.TabStop = False
            ' 
            ' loadCustomDocumentButton
            ' 
            Me.loadCustomDocumentButton.Location = New Point(89, 246)
            Me.loadCustomDocumentButton.Name = "loadCustomDocumentButton"
            Me.loadCustomDocumentButton.Size = New Size(146, 24)
            Me.loadCustomDocumentButton.TabIndex = 3
            Me.loadCustomDocumentButton.Text = "Load Custom Document"
            Me.loadCustomDocumentButton.ThemeName = "TelerikMetro"
            '			Me.loadCustomDocumentButton.Click += New System.EventHandler(Me.loadCustomDocumentButton_Click)
            ' 
            ' loadSampleDocumentButton
            ' 
            Me.loadSampleDocumentButton.Location = New Point(404, 246)
            Me.loadSampleDocumentButton.Name = "loadSampleDocumentButton"
            Me.loadSampleDocumentButton.Size = New Size(146, 24)
            Me.loadSampleDocumentButton.TabIndex = 4
            Me.loadSampleDocumentButton.Text = "Load Sample Document"
            Me.loadSampleDocumentButton.ThemeName = "TelerikMetro"
            '			Me.loadSampleDocumentButton.Click += New System.EventHandler(Me.loadSampleDocumentButton_Click)
            ' 
            ' radLabel1
            ' 
            Me.radLabel1.Font = New Font("Segoe UI", 8.25F, FontStyle.Bold)
            Me.radLabel1.Location = New Point(27, 282)
            Me.radLabel1.Name = "radLabel1"
            Me.radLabel1.Size = New Size(31, 18)
            Me.radLabel1.TabIndex = 5
            Me.radLabel1.Text = "File: "
            Me.radLabel1.ThemeName = "TelerikMetro"
            ' 
            ' fileNameLabel
            ' 
            Me.fileNameLabel.Location = New Point(64, 282)
            Me.fileNameLabel.Name = "fileNameLabel"
            Me.fileNameLabel.Size = New Size(2, 2)
            Me.fileNameLabel.TabIndex = 6
            Me.fileNameLabel.ThemeName = "TelerikMetro"
            ' 
            ' radLabel2
            ' 
            Me.radLabel2.Font = New Font("Segoe UI", 8.25F, FontStyle.Bold)
            Me.radLabel2.Location = New Point(27, 316)
            Me.radLabel2.Name = "radLabel2"
            Me.radLabel2.Size = New Size(64, 18)
            Me.radLabel2.TabIndex = 7
            Me.radLabel2.Text = "Extension: "
            Me.radLabel2.ThemeName = "TelerikMetro"
            ' 
            ' fileExtensionsDropDownList
            ' 
            Me.fileExtensionsDropDownList.AllowShowFocusCues = False
            Me.fileExtensionsDropDownList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            radListDataItem1.Text = "Xlsx"
            radListDataItem1.TextWrap = True
            radListDataItem2.Text = "Csv"
            radListDataItem2.TextWrap = True
            radListDataItem3.Text = "Txt"
            radListDataItem3.TextWrap = True
            Me.fileExtensionsDropDownList.Items.Add(radListDataItem1)
            Me.fileExtensionsDropDownList.Items.Add(radListDataItem2)
            Me.fileExtensionsDropDownList.Items.Add(radListDataItem3)
            Me.fileExtensionsDropDownList.Location = New Point(97, 314)
            Me.fileExtensionsDropDownList.Name = "fileExtensionsDropDownList"
            Me.fileExtensionsDropDownList.Size = New Size(50, 19)
            Me.fileExtensionsDropDownList.TabIndex = 8
            Me.fileExtensionsDropDownList.ThemeName = "TelerikMetro"
            ' 
            ' saveButton
            ' 
            Me.saveButton.Location = New Point(27, 350)
            Me.saveButton.Name = "saveButton"
            Me.saveButton.Size = New Size(575, 24)
            Me.saveButton.TabIndex = 9
            Me.saveButton.Text = "Save"
            Me.saveButton.ThemeName = "TelerikMetro"
            '			Me.saveButton.Click += New System.EventHandler(Me.saveButton_Click)
            ' 
            ' Form1
            ' 
            Me.AutoScaleDimensions = New SizeF(6.0F, 13.0F)
            Me.AutoScaleMode = AutoScaleMode.Font
            Me.ClientSize = New Size(636, 393)
            Me.Controls.Add(Me.saveButton)
            Me.Controls.Add(Me.fileExtensionsDropDownList)
            Me.Controls.Add(Me.radLabel2)
            Me.Controls.Add(Me.fileNameLabel)
            Me.Controls.Add(Me.radLabel1)
            Me.Controls.Add(Me.loadSampleDocumentButton)
            Me.Controls.Add(Me.loadCustomDocumentButton)
            Me.Controls.Add(Me.pictureBox2)
            Me.Controls.Add(Me.label1)
            Me.Controls.Add(Me.pictureBox1)
            Me.FormBorderStyle = FormBorderStyle.FixedSingle
            Me.MaximizeBox = False
            Me.Name = "Form1"
            ' 
            ' 
            ' 
            Me.RootElement.ApplyShapeToControl = True
            Me.Text = "Convert Documents"
            Me.ThemeName = "TelerikMetro"
            '			Me.Load += New System.EventHandler(Me.Form1_Load)
            CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.pictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.loadCustomDocumentButton, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.loadSampleDocumentButton, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.fileNameLabel, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.fileExtensionsDropDownList, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.saveButton, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private pictureBox1 As PictureBox
        Private label1 As Label
        Private pictureBox2 As PictureBox
        Private WithEvents loadCustomDocumentButton As Telerik.WinControls.UI.RadButton
        Private WithEvents loadSampleDocumentButton As Telerik.WinControls.UI.RadButton
        Private radLabel1 As Telerik.WinControls.UI.RadLabel
        Private fileNameLabel As Telerik.WinControls.UI.RadLabel
        Private radLabel2 As Telerik.WinControls.UI.RadLabel
        Private fileExtensionsDropDownList As Telerik.WinControls.UI.RadDropDownList
        Private WithEvents saveButton As Telerik.WinControls.UI.RadButton
        Private telerikMetroTheme1 As Telerik.WinControls.Themes.TelerikMetroTheme
    End Class
End Namespace

