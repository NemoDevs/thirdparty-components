﻿Imports System.IO
Imports System.Reflection
Imports System.Text
Imports Telerik.Windows.Documents.Spreadsheet.FormatProviders
Imports Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx
Imports Telerik.Windows.Documents.Spreadsheet.FormatProviders.TextBased.Csv
Imports Telerik.Windows.Documents.Spreadsheet.Model

Namespace SpreadConvertion
    Public NotInheritable Class FileHelper
        Private Sub New()
        End Sub
        Shared Sub New()
            WorkbookFormatProvidersManager.RegisterFormatProvider(New XlsxFormatProvider())
        End Sub

        Public Shared Sub SaveDocument(ByVal workbook As Workbook, ByVal selectedFormat As String)
            If workbook Is Nothing Then Exit Sub

            Dim formatProvider As IWorkbookFormatProvider = GetFormatProvider(selectedFormat)
            If formatProvider Is Nothing Then
                Return
            End If

            Dim dialog As New SaveFileDialog()
            dialog.Filter = String.Format("{0} files|*{1}|All files (*.*)|*.*", selectedFormat, formatProvider.SupportedExtensions.First())

            If dialog.ShowDialog() = DialogResult.OK Then
                Using stream = dialog.OpenFile()
                    formatProvider.Export(workbook, stream)
                End Using
            End If
        End Sub

        Private Shared Function GetFormatProvider(ByVal extension As String) As IWorkbookFormatProvider
            Dim formatProvider As IWorkbookFormatProvider
            Select Case extension
                Case "Xlsx"
                    formatProvider = WorkbookFormatProvidersManager.GetProviderByName("XlsxFormatProvider")
                Case "Csv"
                    formatProvider = WorkbookFormatProvidersManager.GetProviderByName("CsvFormatProvider")
                    TryCast(formatProvider, CsvFormatProvider).Settings.HasHeaderRow = True
                Case "Txt"
                    formatProvider = WorkbookFormatProvidersManager.GetProviderByName("TxtFormatProvider")
                Case Else
                    formatProvider = Nothing
            End Select

            Return formatProvider
        End Function

        Public Shared Function GetSampleResourceStream(ByVal resource As String) As Stream
            Dim [assembly] = System.Reflection.Assembly.GetExecutingAssembly()
            Dim stream As Stream = [assembly].GetManifestResourceStream(resource)
            If stream IsNot Nothing Then
                Return stream
            End If

            Return Nothing
        End Function
    End Class
End Namespace
