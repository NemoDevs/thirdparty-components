﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Windows.Forms
Imports Telerik.WinControls
Imports Telerik.WinControls.Themes

Namespace SpreadGeneration
    Friend NotInheritable Class Program
        ''' <summary>
        ''' The main entry point for the application.
        ''' </summary>
        Private Sub New()
        End Sub
        <STAThread>
        Shared Sub Main(ByVal args() As String)
            Application.EnableVisualStyles()
            Application.SetCompatibleTextRenderingDefault(False)
            AddHandler AppDomain.CurrentDomain.AssemblyResolve, AddressOf MyResolveEventHandler
            LoadThemes()
            If args.Length = 1 Then
                ThemeResolutionService.ApplicationThemeName = args(0)
            End If
            Application.Run(New Form1())
        End Sub

        Private Shared Function MyResolveEventHandler(ByVal sender As Object, ByVal args As ResolveEventArgs) As System.Reflection.Assembly
            Dim myAssembly, objExecutingAssemblies As System.Reflection.Assembly
            Dim strTempAssmbPath As String = ""
            objExecutingAssemblies = System.Reflection.Assembly.GetExecutingAssembly()
            Dim arrReferencedAssmbNames() As System.Reflection.AssemblyName = objExecutingAssemblies.GetReferencedAssemblies()


            For Each strAssmbName As System.Reflection.AssemblyName In arrReferencedAssmbNames
                If strAssmbName.FullName.Substring(0, strAssmbName.FullName.IndexOf(",")) = args.Name.Substring(0, args.Name.IndexOf(",")) Then
                    strTempAssmbPath = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), args.Name.Substring(0, args.Name.IndexOf(",")) & ".dll")
                    Exit For
                End If

            Next strAssmbName
            If String.IsNullOrWhiteSpace(strTempAssmbPath) Then Return Nothing

            myAssembly = System.Reflection.Assembly.LoadFrom(strTempAssmbPath)
            Return myAssembly
        End Function

        Private Shared Sub LoadThemes()
            Dim TempAquaTheme As AquaTheme = New AquaTheme()
            Dim TempBreezeTheme As BreezeTheme = New BreezeTheme()
            Dim TempDesertTheme As DesertTheme = New DesertTheme()
            Dim TempHighContrastBlackTheme As HighContrastBlackTheme = New HighContrastBlackTheme()
            Dim TempOffice2007BlackTheme As Office2007BlackTheme = New Office2007BlackTheme()
            Dim TempOffice2007SilverTheme As Office2007SilverTheme = New Office2007SilverTheme()
            Dim TempOffice2010BlackTheme As Office2010BlackTheme = New Office2010BlackTheme()
            Dim TempOffice2010SilverTheme As Office2010SilverTheme = New Office2010SilverTheme()
            Dim TempOffice2010BlueTheme As Office2010BlueTheme = New Office2010BlueTheme()
            Dim TempOffice2013DarkTheme As Office2013DarkTheme = New Office2013DarkTheme()
            Dim TempOffice2013LightTheme As Office2013LightTheme = New Office2013LightTheme()
            Dim TempTelerikMetroTheme As TelerikMetroTheme = New TelerikMetroTheme()
            Dim TempTelerikMetroBlueTheme As TelerikMetroBlueTheme = New TelerikMetroBlueTheme()
            Dim TempTelerikMetroTouchTheme As TelerikMetroTouchTheme = New TelerikMetroTouchTheme()
            Dim TempVisualStudio2012DarkTheme As VisualStudio2012DarkTheme = New VisualStudio2012DarkTheme()
            Dim TempVisualStudio2012LightTheme As VisualStudio2012LightTheme = New VisualStudio2012LightTheme()
            Dim TempWindows7Theme As Windows7Theme = New Windows7Theme()
            Dim TempWindows8Theme As Windows8Theme = New Windows8Theme()
        End Sub
    End Class
End Namespace


