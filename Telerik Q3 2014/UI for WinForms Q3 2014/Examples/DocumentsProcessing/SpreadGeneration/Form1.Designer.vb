﻿Imports System.Drawing

Namespace SpreadGeneration
    Partial Public Class Form1
        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Windows Form Designer generated code"

        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Dim gridViewTextBoxColumn5 As New Telerik.WinControls.UI.GridViewTextBoxColumn()
            Dim gridViewTextBoxColumn6 As New Telerik.WinControls.UI.GridViewTextBoxColumn()
            Dim gridViewTextBoxColumn7 As New Telerik.WinControls.UI.GridViewTextBoxColumn()
            Dim gridViewTextBoxColumn8 As New Telerik.WinControls.UI.GridViewTextBoxColumn()
            Me.telerikMetroTheme1 = New Telerik.WinControls.Themes.TelerikMetroTheme()
            Me.radGridView1 = New Telerik.WinControls.UI.RadGridView()
            Me.radLabel1 = New Telerik.WinControls.UI.RadLabel()
            Me.exportFormatDropDownList = New Telerik.WinControls.UI.RadDropDownList()
            Me.exportButton = New Telerik.WinControls.UI.RadButton()
            Me.radLabel2 = New Telerik.WinControls.UI.RadLabel()
            Me.totalSumLabel = New Telerik.WinControls.UI.RadLabel()
            CType(Me.radGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radGridView1.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.exportFormatDropDownList, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.exportButton, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.totalSumLabel, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            ' 
            ' radGridView1
            ' 
            Me.radGridView1.AllowDrop = True
            Me.radGridView1.BackColor = Color.White
            Me.radGridView1.Font = New Font("Microsoft Sans Serif", 8.25F)
            Me.radGridView1.ForeColor = SystemColors.ControlText
            Me.radGridView1.ImeMode = ImeMode.NoControl
            Me.radGridView1.Location = New Point(48, 23)
            ' 
            ' radGridView1
            ' 
            Me.radGridView1.MasterTemplate.AllowAddNewRow = False
            Me.radGridView1.MasterTemplate.AllowCellContextMenu = False
            Me.radGridView1.MasterTemplate.AllowColumnChooser = False
            Me.radGridView1.MasterTemplate.AllowColumnHeaderContextMenu = False
            Me.radGridView1.MasterTemplate.AllowColumnReorder = False
            Me.radGridView1.MasterTemplate.AllowDeleteRow = False
            Me.radGridView1.MasterTemplate.AllowDragToGroup = False
            Me.radGridView1.MasterTemplate.AllowEditRow = False
            Me.radGridView1.MasterTemplate.AllowRowReorder = True
            Me.radGridView1.MasterTemplate.AllowRowResize = False
            gridViewTextBoxColumn5.EnableExpressionEditor = False
            gridViewTextBoxColumn5.FieldName = "Name"
            gridViewTextBoxColumn5.HeaderText = "ITEM"
            gridViewTextBoxColumn5.Name = "ITEM"
            gridViewTextBoxColumn5.Width = 41
            gridViewTextBoxColumn6.EnableExpressionEditor = False
            gridViewTextBoxColumn6.FieldName = "Quantity"
            gridViewTextBoxColumn6.HeaderText = "QTY"
            gridViewTextBoxColumn6.Name = "QTY"
            gridViewTextBoxColumn6.TextAlignment = ContentAlignment.MiddleCenter
            gridViewTextBoxColumn6.Width = 36
            gridViewTextBoxColumn7.EnableExpressionEditor = False
            gridViewTextBoxColumn7.FieldName = "UnitPrice"
            gridViewTextBoxColumn7.FormatString = "{0:C}"
            gridViewTextBoxColumn7.HeaderText = "PRICE"
            gridViewTextBoxColumn7.Name = "Price"
            gridViewTextBoxColumn7.TextAlignment = ContentAlignment.MiddleRight
            gridViewTextBoxColumn7.Width = 46
            gridViewTextBoxColumn8.EnableExpressionEditor = False
            gridViewTextBoxColumn8.FieldName = "SubTotal"
            gridViewTextBoxColumn8.FormatString = "{0:C}"
            gridViewTextBoxColumn8.HeaderText = "SUB TOTAL"
            gridViewTextBoxColumn8.Name = "Sub Total"
            gridViewTextBoxColumn8.TextAlignment = ContentAlignment.MiddleRight
            gridViewTextBoxColumn8.Width = 81
            Me.radGridView1.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {gridViewTextBoxColumn5, gridViewTextBoxColumn6, gridViewTextBoxColumn7, gridViewTextBoxColumn8})
            Me.radGridView1.MasterTemplate.EnableGrouping = False
            Me.radGridView1.MasterTemplate.EnableSorting = False
            Me.radGridView1.Name = "radGridView1"
            Me.radGridView1.ReadOnly = True
            Me.radGridView1.RightToLeft = RightToLeft.No
            Me.radGridView1.Size = New Size(388, 350)
            Me.radGridView1.TabIndex = 0
            Me.radGridView1.Text = "radGridView1"
            Me.radGridView1.ThemeName = "TelerikMetro"
            ' 
            ' radLabel1
            ' 
            Me.radLabel1.Location = New Point(48, 422)
            Me.radLabel1.Name = "radLabel1"
            Me.radLabel1.Size = New Size(81, 16)
            Me.radLabel1.TabIndex = 1
            Me.radLabel1.Text = "Export Format:"
            Me.radLabel1.ThemeName = "TelerikMetro"
            ' 
            ' exportFormatDropDownList
            ' 
            Me.exportFormatDropDownList.AllowShowFocusCues = False
            Me.exportFormatDropDownList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            Me.exportFormatDropDownList.Location = New Point(135, 419)
            Me.exportFormatDropDownList.Name = "exportFormatDropDownList"
            Me.exportFormatDropDownList.Size = New Size(62, 19)
            Me.exportFormatDropDownList.TabIndex = 2
            Me.exportFormatDropDownList.Text = "radDropDownList1"
            Me.exportFormatDropDownList.ThemeName = "TelerikMetro"
            ' 
            ' exportButton
            ' 
            Me.exportButton.Location = New Point(48, 466)
            Me.exportButton.Name = "exportButton"
            Me.exportButton.Size = New Size(388, 24)
            Me.exportButton.TabIndex = 3
            Me.exportButton.Text = "Export"
            Me.exportButton.ThemeName = "TelerikMetro"
            '			Me.exportButton.Click += New System.EventHandler(Me.exportButton_Click)
            ' 
            ' radLabel2
            ' 
            Me.radLabel2.Font = New Font("Microsoft Sans Serif", 11.0F, FontStyle.Regular, GraphicsUnit.Point, (CByte(0)))
            Me.radLabel2.Location = New Point(240, 390)
            Me.radLabel2.Name = "radLabel2"
            Me.radLabel2.Size = New Size(45, 20)
            Me.radLabel2.TabIndex = 4
            Me.radLabel2.Text = "Total:"
            Me.radLabel2.ThemeName = "TelerikMetro"
            ' 
            ' totalSumLabel
            ' 
            Me.totalSumLabel.AutoSize = False
            Me.totalSumLabel.Font = New Font("Microsoft Sans Serif", 11.0F, FontStyle.Regular, GraphicsUnit.Point, (CByte(0)))
            Me.totalSumLabel.Location = New Point(300, 391)
            Me.totalSumLabel.Name = "totalSumLabel"
            Me.totalSumLabel.Size = New Size(118, 18)
            Me.totalSumLabel.TabIndex = 5
            Me.totalSumLabel.Text = "$0"
            Me.totalSumLabel.TextAlignment = ContentAlignment.MiddleRight
            Me.totalSumLabel.ThemeName = "TelerikMetro"
            ' 
            ' Form1
            ' 
            Me.AutoScaleDimensions = New SizeF(6.0F, 13.0F)
            Me.AutoScaleMode = AutoScaleMode.Font
            Me.ClientSize = New Size(495, 519)
            Me.Controls.Add(Me.totalSumLabel)
            Me.Controls.Add(Me.radLabel2)
            Me.Controls.Add(Me.exportButton)
            Me.Controls.Add(Me.exportFormatDropDownList)
            Me.Controls.Add(Me.radLabel1)
            Me.Controls.Add(Me.radGridView1)
            Me.FormBorderStyle = FormBorderStyle.FixedSingle
            Me.MaximizeBox = False
            Me.Name = "Form1"
            ' 
            ' 
            ' 
            Me.RootElement.ApplyShapeToControl = True
            Me.Text = "Generate Documents"
            Me.ThemeName = "TelerikMetro"
            '			Me.Load += New System.EventHandler(Me.Form1_Load)
            CType(Me.radGridView1.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radGridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.exportFormatDropDownList, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.exportButton, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.totalSumLabel, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private telerikMetroTheme1 As Telerik.WinControls.Themes.TelerikMetroTheme
        Private radGridView1 As Telerik.WinControls.UI.RadGridView
        Private radLabel1 As Telerik.WinControls.UI.RadLabel
        Private exportFormatDropDownList As Telerik.WinControls.UI.RadDropDownList
        Private WithEvents exportButton As Telerik.WinControls.UI.RadButton
        Private radLabel2 As Telerik.WinControls.UI.RadLabel
        Private totalSumLabel As Telerik.WinControls.UI.RadLabel
    End Class
End Namespace

