﻿Imports System.Drawing
Namespace SpreadGridIntegration
    Partial Public Class Form1
        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Windows Form Designer generated code"

        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Me.telerikMetroTheme1 = New Telerik.WinControls.Themes.TelerikMetroTheme()
            Me.radGridView1 = New Telerik.WinControls.UI.RadGridView()
            Me.headerRowColorBox = New Telerik.WinControls.UI.RadColorBox()
            Me.groupHeaderColorBox = New Telerik.WinControls.UI.RadColorBox()
            Me.dataRowColorBox = New Telerik.WinControls.UI.RadColorBox()
            Me.radLabel1 = New Telerik.WinControls.UI.RadLabel()
            Me.radLabel2 = New Telerik.WinControls.UI.RadLabel()
            Me.radLabel3 = New Telerik.WinControls.UI.RadLabel()
            Me.exportButton = New Telerik.WinControls.UI.RadButton()
            Me.exportFormatDropDownList = New Telerik.WinControls.UI.RadDropDownList()
            Me.radLabel4 = New Telerik.WinControls.UI.RadLabel()
            CType(Me.radGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radGridView1.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.headerRowColorBox, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.groupHeaderColorBox, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dataRowColorBox, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.exportButton, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.exportFormatDropDownList, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'radGridView1
            '
            Me.radGridView1.BackColor = System.Drawing.Color.White
            Me.radGridView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
            Me.radGridView1.ForeColor = System.Drawing.SystemColors.ControlText
            Me.radGridView1.Location = New System.Drawing.Point(45, 37)
            '
            'radGridView1
            '
            Me.radGridView1.MasterTemplate.AllowAddNewRow = False
            Me.radGridView1.MasterTemplate.AllowColumnChooser = False
            Me.radGridView1.MasterTemplate.AllowColumnReorder = False
            Me.radGridView1.MasterTemplate.AllowDeleteRow = False
            Me.radGridView1.MasterTemplate.AllowDragToGroup = False
            Me.radGridView1.MasterTemplate.AllowEditRow = False
            Me.radGridView1.MasterTemplate.AllowRowResize = False
            Me.radGridView1.MasterTemplate.ShowGroupedColumns = True
            Me.radGridView1.MasterTemplate.ShowRowHeaderColumn = False
            Me.radGridView1.Name = "radGridView1"
            Me.radGridView1.Size = New System.Drawing.Size(661, 361)
            Me.radGridView1.TabIndex = 0
            Me.radGridView1.Text = "radGridView1"
            Me.radGridView1.ThemeName = "TelerikMetro"
            '
            'headerRowColorBox
            '
            Me.headerRowColorBox.Location = New System.Drawing.Point(45, 440)
            Me.headerRowColorBox.Name = "headerRowColorBox"
            Me.headerRowColorBox.Size = New System.Drawing.Size(139, 23)
            Me.headerRowColorBox.TabIndex = 1
            Me.headerRowColorBox.Text = "radColorBox1"
            Me.headerRowColorBox.ThemeName = "TelerikMetro"
            '
            'groupHeaderColorBox
            '
            Me.groupHeaderColorBox.Location = New System.Drawing.Point(45, 470)
            Me.groupHeaderColorBox.Name = "groupHeaderColorBox"
            Me.groupHeaderColorBox.Size = New System.Drawing.Size(139, 23)
            Me.groupHeaderColorBox.TabIndex = 2
            Me.groupHeaderColorBox.Text = "radColorBox2"
            Me.groupHeaderColorBox.ThemeName = "TelerikMetro"
            '
            'dataRowColorBox
            '
            Me.dataRowColorBox.Location = New System.Drawing.Point(45, 500)
            Me.dataRowColorBox.Name = "dataRowColorBox"
            Me.dataRowColorBox.Size = New System.Drawing.Size(139, 23)
            Me.dataRowColorBox.TabIndex = 3
            Me.dataRowColorBox.Text = "radColorBox3"
            Me.dataRowColorBox.ThemeName = "TelerikMetro"
            '
            'radLabel1
            '
            Me.radLabel1.Location = New System.Drawing.Point(190, 443)
            Me.radLabel1.Name = "radLabel1"
            Me.radLabel1.Size = New System.Drawing.Size(106, 16)
            Me.radLabel1.TabIndex = 4
            Me.radLabel1.Text = "Header background"
            Me.radLabel1.ThemeName = "TelerikMetro"
            '
            'radLabel2
            '
            Me.radLabel2.Location = New System.Drawing.Point(190, 472)
            Me.radLabel2.Name = "radLabel2"
            Me.radLabel2.Size = New System.Drawing.Size(138, 16)
            Me.radLabel2.TabIndex = 5
            Me.radLabel2.Text = "Group header background"
            Me.radLabel2.ThemeName = "TelerikMetro"
            '
            'radLabel3
            '
            Me.radLabel3.Location = New System.Drawing.Point(190, 500)
            Me.radLabel3.Name = "radLabel3"
            Me.radLabel3.Size = New System.Drawing.Size(91, 16)
            Me.radLabel3.TabIndex = 6
            Me.radLabel3.Text = "Row background"
            Me.radLabel3.ThemeName = "TelerikMetro"
            '
            'exportButton
            '
            Me.exportButton.Location = New System.Drawing.Point(45, 542)
            Me.exportButton.Name = "exportButton"
            Me.exportButton.Size = New System.Drawing.Size(661, 24)
            Me.exportButton.TabIndex = 7
            Me.exportButton.Text = "Export"
            Me.exportButton.ThemeName = "TelerikMetro"
            '
            'exportFormatDropDownList
            '
            Me.exportFormatDropDownList.AllowShowFocusCues = False
            Me.exportFormatDropDownList.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            Me.exportFormatDropDownList.Location = New System.Drawing.Point(45, 412)
            Me.exportFormatDropDownList.Name = "exportFormatDropDownList"
            Me.exportFormatDropDownList.Size = New System.Drawing.Size(139, 19)
            Me.exportFormatDropDownList.TabIndex = 8
            Me.exportFormatDropDownList.Text = "radDropDownList1"
            Me.exportFormatDropDownList.ThemeName = "TelerikMetro"
            '
            'radLabel4
            '
            Me.radLabel4.Location = New System.Drawing.Point(190, 415)
            Me.radLabel4.Name = "radLabel4"
            Me.radLabel4.Size = New System.Drawing.Size(78, 16)
            Me.radLabel4.TabIndex = 9
            Me.radLabel4.Text = "Export Format"
            Me.radLabel4.ThemeName = "TelerikMetro"
            '
            'Form1
            '
            Me.ClientSize = New System.Drawing.Size(758, 578)
            Me.Controls.Add(Me.radLabel4)
            Me.Controls.Add(Me.exportFormatDropDownList)
            Me.Controls.Add(Me.exportButton)
            Me.Controls.Add(Me.radLabel3)
            Me.Controls.Add(Me.radLabel2)
            Me.Controls.Add(Me.radLabel1)
            Me.Controls.Add(Me.dataRowColorBox)
            Me.Controls.Add(Me.groupHeaderColorBox)
            Me.Controls.Add(Me.headerRowColorBox)
            Me.Controls.Add(Me.radGridView1)
            Me.MaximizeBox = False
            Me.Name = "Form1"
            '
            '
            '
            Me.RootElement.ApplyShapeToControl = True
            Me.Text = "Spread Grid Integration"
            Me.ThemeName = "TelerikMetro"
            CType(Me.radGridView1.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radGridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.headerRowColorBox, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.groupHeaderColorBox, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dataRowColorBox, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.exportButton, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.exportFormatDropDownList, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private telerikMetroTheme1 As Telerik.WinControls.Themes.TelerikMetroTheme
        Private radGridView1 As Telerik.WinControls.UI.RadGridView
        Private headerRowColorBox As Telerik.WinControls.UI.RadColorBox
        Private groupHeaderColorBox As Telerik.WinControls.UI.RadColorBox
        Private dataRowColorBox As Telerik.WinControls.UI.RadColorBox
        Private radLabel1 As Telerik.WinControls.UI.RadLabel
        Private radLabel2 As Telerik.WinControls.UI.RadLabel
        Private radLabel3 As Telerik.WinControls.UI.RadLabel
        Private WithEvents exportButton As Telerik.WinControls.UI.RadButton
        Private WithEvents exportFormatDropDownList As Telerik.WinControls.UI.RadDropDownList
        Private radLabel4 As Telerik.WinControls.UI.RadLabel
    End Class
End Namespace

