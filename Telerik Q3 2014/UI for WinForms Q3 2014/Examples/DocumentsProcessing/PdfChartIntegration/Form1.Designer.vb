﻿Namespace PdfChartIntegration
    Partial Class Form1
        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Dim radListDataItem1 As New Telerik.WinControls.UI.RadListDataItem()
            Dim radListDataItem2 As New Telerik.WinControls.UI.RadListDataItem()
            Dim radListDataItem3 As New Telerik.WinControls.UI.RadListDataItem()
            Dim radListDataItem4 As New Telerik.WinControls.UI.RadListDataItem()
            Dim radListDataItem5 As New Telerik.WinControls.UI.RadListDataItem()
            Dim radListDataItem6 As New Telerik.WinControls.UI.RadListDataItem()
            Me.checkBoxQ1 = New Telerik.WinControls.UI.RadCheckBox()
            Me.checkBoxQ2 = New Telerik.WinControls.UI.RadCheckBox()
            Me.checkBoxQ3 = New Telerik.WinControls.UI.RadCheckBox()
            Me.checkBoxQ4 = New Telerik.WinControls.UI.RadCheckBox()
            Me.radGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
            Me.dropDownListNumberOfProducts = New Telerik.WinControls.UI.RadDropDownList()
            Me.radLabel1 = New Telerik.WinControls.UI.RadLabel()
            Me.pictureBox1 = New System.Windows.Forms.PictureBox()
            Me.chartValueStepEditor = New Telerik.WinControls.UI.RadSpinEditor()
            Me.radLabel2 = New Telerik.WinControls.UI.RadLabel()
            Me.buttonSave = New Telerik.WinControls.UI.RadButton()
            Me.telerikMetroTheme1 = New Telerik.WinControls.Themes.TelerikMetroTheme()
            CType((Me.checkBoxQ1), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.checkBoxQ2), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.checkBoxQ3), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.checkBoxQ4), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.radGroupBox1), System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radGroupBox1.SuspendLayout()
            CType((Me.dropDownListNumberOfProducts), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.radLabel1), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.pictureBox1), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.chartValueStepEditor), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.radLabel2), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.buttonSave), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me), System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            ' 
            ' checkBoxQ1
            ' 
            Me.checkBoxQ1.Location = New System.Drawing.Point(5, 24)
            Me.checkBoxQ1.Name = "checkBoxQ1"
            Me.checkBoxQ1.Size = New System.Drawing.Size(39, 19)
            Me.checkBoxQ1.TabIndex = 0
            Me.checkBoxQ1.Text = "Q1"
            Me.checkBoxQ1.ThemeName = "TelerikMetro"
            ' 
            ' checkBoxQ2
            ' 
            Me.checkBoxQ2.Location = New System.Drawing.Point(5, 75)
            Me.checkBoxQ2.Name = "checkBoxQ2"
            Me.checkBoxQ2.Size = New System.Drawing.Size(39, 19)
            Me.checkBoxQ2.TabIndex = 1
            Me.checkBoxQ2.Text = "Q2"
            Me.checkBoxQ2.ThemeName = "TelerikMetro"
            ' 
            ' checkBoxQ3
            ' 
            Me.checkBoxQ3.Location = New System.Drawing.Point(84, 24)
            Me.checkBoxQ3.Name = "checkBoxQ3"
            Me.checkBoxQ3.Size = New System.Drawing.Size(39, 19)
            Me.checkBoxQ3.TabIndex = 2
            Me.checkBoxQ3.Text = "Q3"
            Me.checkBoxQ3.ThemeName = "TelerikMetro"
            ' 
            ' checkBoxQ4
            ' 
            Me.checkBoxQ4.Location = New System.Drawing.Point(84, 76)
            Me.checkBoxQ4.Name = "checkBoxQ4"
            Me.checkBoxQ4.Size = New System.Drawing.Size(39, 19)
            Me.checkBoxQ4.TabIndex = 3
            Me.checkBoxQ4.Text = "Q4"
            Me.checkBoxQ4.ThemeName = "TelerikMetro"
            ' 
            ' radGroupBox1
            ' 
            Me.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
            Me.radGroupBox1.Controls.Add(Me.checkBoxQ1)
            Me.radGroupBox1.Controls.Add(Me.checkBoxQ4)
            Me.radGroupBox1.Controls.Add(Me.checkBoxQ2)
            Me.radGroupBox1.Controls.Add(Me.checkBoxQ3)
            Me.radGroupBox1.HeaderText = "Quarters:"
            Me.radGroupBox1.Location = New System.Drawing.Point(25, 537)
            Me.radGroupBox1.Name = "radGroupBox1"
            Me.radGroupBox1.Size = New System.Drawing.Size(135, 108)
            Me.radGroupBox1.TabIndex = 4
            Me.radGroupBox1.Text = "Quarters:"
            Me.radGroupBox1.ThemeName = "TelerikMetro"
            ' 
            ' dropDownListNumberOfProducts
            ' 
            Me.dropDownListNumberOfProducts.AllowShowFocusCues = False
            Me.dropDownListNumberOfProducts.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            radListDataItem1.Text = "1"
            radListDataItem1.TextWrap = True
            radListDataItem2.Text = "2"
            radListDataItem2.TextWrap = True
            radListDataItem3.Text = "3"
            radListDataItem3.TextWrap = True
            radListDataItem4.Text = "4"
            radListDataItem4.TextWrap = True
            radListDataItem5.Text = "5"
            radListDataItem5.TextWrap = True
            radListDataItem6.Text = "6"
            radListDataItem6.TextWrap = True
            Me.dropDownListNumberOfProducts.Items.Add(radListDataItem1)
            Me.dropDownListNumberOfProducts.Items.Add(radListDataItem2)
            Me.dropDownListNumberOfProducts.Items.Add(radListDataItem3)
            Me.dropDownListNumberOfProducts.Items.Add(radListDataItem4)
            Me.dropDownListNumberOfProducts.Items.Add(radListDataItem5)
            Me.dropDownListNumberOfProducts.Items.Add(radListDataItem6)
            Me.dropDownListNumberOfProducts.Location = New System.Drawing.Point(194, 561)
            Me.dropDownListNumberOfProducts.Name = "dropDownListNumberOfProducts"
            Me.dropDownListNumberOfProducts.Size = New System.Drawing.Size(108, 19)
            Me.dropDownListNumberOfProducts.TabIndex = 5
            Me.dropDownListNumberOfProducts.ThemeName = "TelerikMetro"
            ' 
            ' radLabel1
            ' 
            Me.radLabel1.Location = New System.Drawing.Point(194, 537)
            Me.radLabel1.Name = "radLabel1"
            Me.radLabel1.Size = New System.Drawing.Size(107, 16)
            Me.radLabel1.TabIndex = 6
            Me.radLabel1.Text = "Number of Products"
            Me.radLabel1.ThemeName = "TelerikMetro"
            ' 
            ' pictureBox1
            ' 
            Me.pictureBox1.Location = New System.Drawing.Point(13, 13)
            Me.pictureBox1.Name = "pictureBox1"
            Me.pictureBox1.Size = New System.Drawing.Size(618, 476)
            Me.pictureBox1.TabIndex = 7
            Me.pictureBox1.TabStop = False
            ' 
            ' chartValueStepEditor
            ' 
            Me.chartValueStepEditor.Increment = New Decimal(New Integer() {500, 0, 0, 0})
            Me.chartValueStepEditor.Location = New System.Drawing.Point(194, 611)
            Me.chartValueStepEditor.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
            Me.chartValueStepEditor.Minimum = New Decimal(New Integer() {5000, 0, 0, 0})
            Me.chartValueStepEditor.Name = "chartValueStepEditor"
            Me.chartValueStepEditor.Size = New System.Drawing.Size(108, 22)
            Me.chartValueStepEditor.TabIndex = 8
            Me.chartValueStepEditor.TabStop = False
            Me.chartValueStepEditor.ThemeName = "TelerikMetro"
            Me.chartValueStepEditor.Value = New Decimal(New Integer() {5000, 0, 0, 0})
            ' 
            ' radLabel2
            ' 
            Me.radLabel2.Location = New System.Drawing.Point(194, 587)
            Me.radLabel2.Name = "radLabel2"
            Me.radLabel2.Size = New System.Drawing.Size(95, 16)
            Me.radLabel2.TabIndex = 7
            Me.radLabel2.Text = "Chart Value Step:"
            Me.radLabel2.ThemeName = "TelerikMetro"
            ' 
            ' buttonSave
            ' 
            Me.buttonSave.Location = New System.Drawing.Point(13, 684)
            Me.buttonSave.Name = "buttonSave"
            Me.buttonSave.Size = New System.Drawing.Size(618, 24)
            Me.buttonSave.TabIndex = 9
            Me.buttonSave.Text = "Save Document"
            Me.buttonSave.ThemeName = "TelerikMetro"
            ' 
            ' Form1
            ' 
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0F, 13.0F)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(645, 720)
            Me.Controls.Add(Me.buttonSave)
            Me.Controls.Add(Me.radLabel2)
            Me.Controls.Add(Me.chartValueStepEditor)
            Me.Controls.Add(Me.pictureBox1)
            Me.Controls.Add(Me.radLabel1)
            Me.Controls.Add(Me.dropDownListNumberOfProducts)
            Me.Controls.Add(Me.radGroupBox1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
            Me.MaximizeBox = False
            Me.Name = "Form1"
            ' 
            ' 
            ' 
            Me.RootElement.ApplyShapeToControl = True
            Me.Text = "Bar Chart"
            Me.ThemeName = "TelerikMetro"

            CType((Me.checkBoxQ1), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.checkBoxQ2), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.checkBoxQ3), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.checkBoxQ4), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.radGroupBox1), System.ComponentModel.ISupportInitialize).EndInit()
            Me.radGroupBox1.ResumeLayout(False)
            Me.radGroupBox1.PerformLayout()
            CType((Me.dropDownListNumberOfProducts), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.radLabel1), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.pictureBox1), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.chartValueStepEditor), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.radLabel2), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.buttonSave), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me), System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub


        Friend WithEvents checkBoxQ1 As Telerik.WinControls.UI.RadCheckBox
        Friend WithEvents checkBoxQ2 As Telerik.WinControls.UI.RadCheckBox
        Friend WithEvents checkBoxQ3 As Telerik.WinControls.UI.RadCheckBox
        Friend WithEvents checkBoxQ4 As Telerik.WinControls.UI.RadCheckBox
        Private radGroupBox1 As Telerik.WinControls.UI.RadGroupBox
        Friend WithEvents dropDownListNumberOfProducts As Telerik.WinControls.UI.RadDropDownList
        Private radLabel1 As Telerik.WinControls.UI.RadLabel
        Private pictureBox1 As System.Windows.Forms.PictureBox
        Friend WithEvents chartValueStepEditor As Telerik.WinControls.UI.RadSpinEditor
        Private radLabel2 As Telerik.WinControls.UI.RadLabel
        Friend WithEvents buttonSave As Telerik.WinControls.UI.RadButton
        Private telerikMetroTheme1 As Telerik.WinControls.Themes.TelerikMetroTheme
    End Class
End Namespace