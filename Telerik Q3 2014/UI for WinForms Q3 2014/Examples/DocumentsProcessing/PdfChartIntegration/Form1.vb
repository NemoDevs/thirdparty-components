﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.IO
Imports System.Linq
Imports System.Reflection
Imports System.Text
Imports System.Windows
Imports System.Windows.Forms
Imports Telerik.WinControls.UI
Imports Telerik.Windows.Documents.Fixed.Model
Imports Telerik.Windows.Documents.Fixed.Model.ColorSpaces
Imports Telerik.Windows.Documents.Fixed.Model.Data
Imports Telerik.Windows.Documents.Fixed.Model.Editing

Namespace PdfChartIntegration
    Partial Public Class Form1
        Inherits RadForm
        Private Shared ReadOnly chartWidth As Double = 600
        Private Shared ReadOnly chartHeight As Double = 360
        Private Shared ReadOnly markerAreaWidth As Double = 60
        Private Shared ReadOnly marginTop As Double = 200
        Private Shared ReadOnly valuesMargin As Double = 10
        Private Shared ReadOnly rectSize As Double = 5
        Private Shared ReadOnly barMargin As Double = 2
        Private Shared ReadOnly markersCount As Integer = 7

        Private productsList As Product()
        Private quartersToExport As Dictionary(Of Integer, Boolean)

        Private member_q1 As Boolean
        Private member_q2 As Boolean
        Private member_q3 As Boolean
        Private member_q4 As Boolean
        Private member_exportedProductsCount As Integer = 6
        Private member_stepValue As Double

        Public Sub New()
            InitializeComponent()
        End Sub

        Private Sub Form1_Load(sender As Object, e As System.EventArgs) Handles Me.Load
            Me.productsList = Product.GetProducts()
            Me.quartersToExport = New Dictionary(Of Integer, Boolean)()
            Me.InitializeData()
            Me.InitializeControls()
        End Sub

        Private Sub InitializeControls()
            Me.checkBoxQ1.Checked = True
            Me.checkBoxQ2.Checked = True
            Me.checkBoxQ3.Checked = True
            Me.checkBoxQ4.Checked = True

            Me.dropDownListNumberOfProducts.SelectedIndex = 5
            Me.pictureBox1.Image = Image.FromStream(GetSampleResourceStream("barChart.PNG"))
        End Sub

        Sub chartValueStepEditor_ValueChanged(sender As Object, e As System.EventArgs) Handles chartValueStepEditor.ValueChanged
            member_stepValue = CType(Me.chartValueStepEditor.Value, Double)
        End Sub

        Sub dropDownListNumberOfProducts_SelectedIndexChanged(sender As Object, e As Telerik.WinControls.UI.Data.PositionChangedEventArgs) Handles dropDownListNumberOfProducts.SelectedIndexChanged
            Me.exportedProductsCount = Integer.Parse(Me.dropDownListNumberOfProducts.Text)
        End Sub

        Sub checkBoxQ4_CheckStateChanged(sender As Object, e As System.EventArgs) Handles checkBoxQ4.CheckStateChanged
            Q4 = Me.checkBoxQ4.Checked
        End Sub

        Sub checkBoxQ3_CheckStateChanged(sender As Object, e As System.EventArgs) Handles checkBoxQ3.CheckStateChanged
            Q3 = Me.checkBoxQ3.Checked
        End Sub

        Sub checkBoxQ2_CheckStateChanged(sender As Object, e As System.EventArgs) Handles checkBoxQ2.CheckStateChanged
            Q2 = Me.checkBoxQ2.Checked
        End Sub

        Sub checkBoxQ1_CheckStateChanged(sender As Object, e As System.EventArgs) Handles checkBoxQ1.CheckStateChanged
            Q1 = Me.checkBoxQ1.Checked
        End Sub

        Private Sub InitializeData()
            Me.quartersToExport.Add(0, False)
            Me.quartersToExport.Add(1, False)
            Me.quartersToExport.Add(2, False)
            Me.quartersToExport.Add(3, False)

            Me.Q1 = True
            Me.Q2 = True
            Me.Q3 = True
            Me.Q4 = True
            Me.exportedProductsCount = Me.productsList.Length
            Me.StepValue = 5000

            Me.Products = New List(Of Integer)()
            Dim currentIndex As Integer = 0
            While currentIndex < Me.productsList.Length
                Me.Products.Add(currentIndex + 1)
                System.Math.Max(System.Threading.Interlocked.Increment(currentIndex), currentIndex - 1)
            End While
        End Sub

        Public Property Products() As List(Of Integer)

        Public Property ExportedProductsCount() As Integer
            Get
                Return Me.member_exportedProductsCount
            End Get
            Set(value As Integer)
                If Me.exportedProductsCount <> value Then
                    Me.member_exportedProductsCount = value
                End If
            End Set
        End Property

        Public Property StepValue() As Double
            Get
                Return member_stepValue

            End Get
            Set(value As Double)
                If Me.member_stepValue <> value Then
                    Me.member_stepValue = value
                End If
            End Set
        End Property

        Public Property Q1() As Boolean
            Get
                Return Me.member_q1
            End Get
            Set(value As Boolean)
                If Me.Q1 <> value Then
                    member_q1 = value
                    Me.quartersToExport(0) = Me.Q1
                    Me.buttonSave.Enabled = Me.quartersToExport.Values.Contains(True)
                End If
            End Set
        End Property

        Public Property Q2() As Boolean
            Get
                Return Me.member_q2
            End Get
            Set(value As Boolean)
                If Me.q2 <> value Then
                    Me.member_q2 = value
                    Me.quartersToExport(1) = Me.q2
                    Me.buttonSave.Enabled = Me.quartersToExport.Values.Contains(True)
                End If
            End Set
        End Property

        Public Property Q3() As Boolean
            Get
                Return Me.member_q3
            End Get
            Set(value As Boolean)
                If Me.q3 <> value Then
                    Me.member_q3 = value
                    Me.quartersToExport(2) = Me.q3
                    Me.buttonSave.Enabled = Me.quartersToExport.Values.Contains(True)
                End If
            End Set
        End Property

        Public Property Q4() As Boolean
            Get
                Return Me.member_q4
            End Get
            Set(value As Boolean)
                If Me.q4 <> value Then
                    Me.member_q4 = value
                    Me.quartersToExport(3) = Me.q4
                    Me.buttonSave.Enabled = Me.quartersToExport.Values.Contains(True)
                End If
            End Set
        End Property

        Private Sub Export()
            Dim formatProvider As New Telerik.Windows.Documents.Fixed.FormatProviders.Pdf.PdfFormatProvider()
            formatProvider.ExportSettings.ImageQuality = Telerik.Windows.Documents.Fixed.FormatProviders.Pdf.Export.ImageQuality.High

            If Not Me.quartersToExport.Values.Contains(True) Then
                Return
            End If

            Dim dialog As New SaveFileDialog()
            dialog.Filter = [String].Format("{0} files|*.{1}", "Pdf", "pdf")

            If dialog.ShowDialog() = DialogResult.OK Then
                Try
                    Using stream As Object = dialog.OpenFile()
                        Dim document As RadFixedDocument = Me.CreateDocument()
                        formatProvider.Export(document, stream)
                    End Using
                Catch ex As IOException
                    MessageBox.Show(ex.Message, "Error")
                End Try
            End If
        End Sub

        Private Function CreateDocument() As RadFixedDocument
            Dim document As New RadFixedDocument()
            Dim page As RadFixedPage = document.Pages.AddPage()
            page.Size = New System.Windows.Size(792, 1128)
            Dim editor As New FixedContentEditor(page)
            editor.GraphicProperties.IsFilled = False
            Me.DrawCompanyLogo(editor)

            Dim leftMargin As Double = (page.Size.Width - chartWidth) / 2
            Dim offsetX As Double
            Dim offsetY As Double

            Me.DrawChartFrame(leftMargin, editor, offsetX, offsetY)

            Dim offset As Double = 20
            Dim textWidth As Double = 0
            Dim rectMargin As Double = 2

            Dim i As Integer = 0
            While i < Me.ExportedProductsCount
                textWidth += rectSize + rectMargin + offset
                textWidth += editor.MeasureText(Me.productsList(i).Name).Width
                System.Math.Max(System.Threading.Interlocked.Increment(i), i - 1)
                i = i + 1
            End While

            editor.TextProperties.HorizontalAlignment = HorizontalTextAlignment.Left
            Dim colors As RgbColor() = {New RgbColor(46, 204, 113), New RgbColor(155, 89, 182), New RgbColor(52, 152, 219), New RgbColor(241, 196, 15), New RgbColor(230, 126, 34), New RgbColor(231, 76, 60)}
            offsetX = leftMargin + ((chartWidth - textWidth) / 2)
            offsetY += 20
            i = 0
            While i < Me.ExportedProductsCount
                editor.Position.Translate(offsetX, offsetY + 2)
                Dim tiling As Tiling = CreateTiling(offsetX, offsetY, rectSize, colors(i))
                editor.GraphicProperties.FillColor = tiling
                editor.DrawRectangle(New Rect(0, 0, rectSize, rectSize))

                offsetX += rectSize + rectMargin
                editor.Position.Translate(offsetX, offsetY)
                editor.GraphicProperties.FillColor = RgbColors.Black
                editor.DrawText(Me.productsList(i).Name)
                offsetX += editor.MeasureText(Me.productsList(i).Name).Width + offset
                System.Math.Max(System.Threading.Interlocked.Increment(i), i - 1)
                i = i + 1
            End While

            offsetX = leftMargin

            offsetY += 30
            Dim markerHeight As Double = (chartHeight - (offsetY - marginTop)) / markersCount
            editor.Position.Translate(offsetX, offsetY)
            editor.TextProperties.HorizontalAlignment = HorizontalTextAlignment.Right
            editor.TextProperties.TextBlockWidth = markerAreaWidth

            i = markersCount - 1
            While i >= 0
                editor.DrawText(String.Format("{0:C}", i * Me.StepValue))
                If i > 0 Then
                    offsetY += markerHeight
                    editor.Position.Translate(offsetX, offsetY)
                End If
                System.Math.Max(System.Threading.Interlocked.Decrement(i), i + 1)
                i = i + 1
            End While

            offsetX = leftMargin + markerAreaWidth + valuesMargin
            Dim center As Double = editor.MeasureText("X").Height / 2
            offsetY += center
            Dim valueHeight As Double = markerHeight / Me.StepValue
            Dim dataAreaWidth As Double = chartWidth - markerAreaWidth - 2 * valuesMargin

            Dim sectionWidth As Double = dataAreaWidth / Me.GetQuartersToExportCount()
            Dim barWidth As Double = (sectionWidth - 2 * valuesMargin - 2 * Me.ExportedProductsCount * barMargin) / Me.ExportedProductsCount
            editor.TextProperties.TextBlockWidth = sectionWidth
            editor.TextProperties.HorizontalAlignment = HorizontalTextAlignment.Center

            Dim j As Integer = 0
            While j < Me.quartersToExport.Keys.Count
                If Not Me.quartersToExport(j) Then
                    Continue While
                End If

                editor.Position.Translate(offsetX, offsetY + 5)
                editor.GraphicProperties.FillColor = RgbColors.Black
                editor.DrawText(String.Format("Q{0}", j + 1))
                editor.Position.Translate(0, 0)
                offsetX += valuesMargin
                i = 0
                While i < Me.ExportedProductsCount
                    Dim product As Product = Me.productsList(i)
                    Dim h As Double = product.Q(j) * valueHeight
                    offsetX += barMargin
                    Dim tiling As Tiling = CreateTiling(offsetX, offsetY - h, barWidth, colors(i))
                    editor.GraphicProperties.FillColor = tiling
                    editor.DrawRectangle(New Rect(offsetX, offsetY - h, barWidth, h))
                    offsetX += barWidth + barMargin
                    System.Math.Max(System.Threading.Interlocked.Increment(i), i - 1)
                    i = i + 1
                End While

                offsetX += valuesMargin
                System.Math.Max(System.Threading.Interlocked.Increment(j), j - 1)
            End While

            offsetX = leftMargin + markerAreaWidth + valuesMargin
            DrawBarLine(editor, offsetX, offsetY, dataAreaWidth)

            Return document
        End Function

        Private Sub DrawCompanyLogo(editor As FixedContentEditor)
            editor.Position.Translate(230, 80)
            Using stream As Stream = GetSampleResourceStream("PdfChartIntegration.abCompany.jpg")
                editor.DrawImage(stream, ImageFormat.Jpeg)
            End Using
            editor.Position.Translate(0, 0)
        End Sub

        Private Sub DrawChartFrame(leftMargin As Double, editor As FixedContentEditor, ByRef offsetX As Double, ByRef offsetY As Double)
            offsetX = leftMargin
            offsetY = marginTop
            editor.DrawRectangle(New Rect(offsetX, offsetY, chartWidth, chartHeight))
            offsetY += 10
            editor.Position.Translate(offsetX, offsetY)

            editor.TextProperties.TextBlockWidth = chartWidth
            editor.TextProperties.FontSize = 18
            editor.TextProperties.HorizontalAlignment = HorizontalTextAlignment.Center
            editor.TextProperties.TrySetFont(New System.Windows.Media.FontFamily("Calibri"), FontStyles.Normal, FontWeights.Bold)
            editor.DrawText("2013")

            offsetY += 30
            editor.Position.Translate(offsetX, offsetY)

            editor.TextProperties.TrySetFont(New System.Windows.Media.FontFamily("Calibri"))
            editor.TextProperties.FontSize = 10
            editor.GraphicProperties.IsFilled = True
            editor.GraphicProperties.IsStroked = False
        End Sub

        Private Shared Function CreateTiling(offsetX As Double, offsetY As Double, width As Double, color As SimpleColor) As Tiling
            Dim tiling As New Tiling(New Rect(0, 0, width, 2))
            tiling.Position.Translate(offsetX, offsetY)
            Dim tilingEditor As Object = New FixedContentEditor(tiling)
            tilingEditor.GraphicProperties.IsStroked = False
            tilingEditor.GraphicProperties.FillColor = color
            tilingEditor.DrawRectangle(New Rect(0, 0, width, 1))
            Dim gradient As New LinearGradient(New System.Windows.Point(0, 0), New System.Windows.Point(width, 0))
            gradient.StartColor = color
            gradient.EndColor = color
            gradient.GradientStops.Add(New GradientStop(RgbColors.White, 0.5))
            tilingEditor.GraphicProperties.FillColor = gradient
            tilingEditor.DrawRectangle(New Rect(0, 1, width, 1))

            Return tiling
        End Function

        Private Shared Sub DrawBarLine(editor As FixedContentEditor, offsetX As Double, offsetY As Double, width As Double)
            editor.GraphicProperties.FillColor = RgbColors.Black
            editor.GraphicProperties.StrokeThickness = 1
            editor.GraphicProperties.IsFilled = False
            editor.GraphicProperties.IsStroked = True
            editor.DrawLine(New System.Windows.Point(offsetX, offsetY), New System.Windows.Point(offsetX + width, offsetY))
        End Sub

        Private Function GetQuartersToExportCount() As Double
            Dim counter As Integer = 0
            For Each shouldExportQ As Boolean In Me.quartersToExport.Values
                If shouldExportQ Then
                    System.Math.Max(System.Threading.Interlocked.Increment(counter), counter - 1)
                End If
            Next

            Return counter
        End Function

        Public Shared Function GetSampleResourceStream(resource As String) As Stream
            Dim a As Object = Assembly.GetExecutingAssembly()
            Dim stream As Stream = a.GetManifestResourceStream(resource)
            If stream IsNot Nothing Then
                Return stream
            End If

            Return Nothing
        End Function

        Private Sub buttonSave_Click(sender As Object, e As System.EventArgs) Handles buttonSave.Click
            Me.Export()
        End Sub
    End Class
End Namespace