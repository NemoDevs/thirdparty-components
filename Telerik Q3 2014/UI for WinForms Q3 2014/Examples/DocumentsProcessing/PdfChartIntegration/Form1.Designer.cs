﻿namespace PdfChartIntegration
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            this.checkBoxQ1 = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxQ2 = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxQ3 = new Telerik.WinControls.UI.RadCheckBox();
            this.checkBoxQ4 = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.dropDownListNumberOfProducts = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.chartValueStepEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.buttonSave = new Telerik.WinControls.UI.RadButton();
            this.telerikMetroTheme1 = new Telerik.WinControls.Themes.TelerikMetroTheme();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxQ1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxQ2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxQ3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxQ4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dropDownListNumberOfProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartValueStepEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBoxQ1
            // 
            this.checkBoxQ1.Location = new System.Drawing.Point(5, 24);
            this.checkBoxQ1.Name = "checkBoxQ1";
            this.checkBoxQ1.Size = new System.Drawing.Size(39, 19);
            this.checkBoxQ1.TabIndex = 0;
            this.checkBoxQ1.Text = "Q1";
            this.checkBoxQ1.ThemeName = "TelerikMetro";
            // 
            // checkBoxQ2
            // 
            this.checkBoxQ2.Location = new System.Drawing.Point(5, 75);
            this.checkBoxQ2.Name = "checkBoxQ2";
            this.checkBoxQ2.Size = new System.Drawing.Size(39, 19);
            this.checkBoxQ2.TabIndex = 1;
            this.checkBoxQ2.Text = "Q2";
            this.checkBoxQ2.ThemeName = "TelerikMetro";
            // 
            // checkBoxQ3
            // 
            this.checkBoxQ3.Location = new System.Drawing.Point(84, 24);
            this.checkBoxQ3.Name = "checkBoxQ3";
            this.checkBoxQ3.Size = new System.Drawing.Size(39, 19);
            this.checkBoxQ3.TabIndex = 2;
            this.checkBoxQ3.Text = "Q3";
            this.checkBoxQ3.ThemeName = "TelerikMetro";
            // 
            // checkBoxQ4
            // 
            this.checkBoxQ4.Location = new System.Drawing.Point(84, 76);
            this.checkBoxQ4.Name = "checkBoxQ4";
            this.checkBoxQ4.Size = new System.Drawing.Size(39, 19);
            this.checkBoxQ4.TabIndex = 3;
            this.checkBoxQ4.Text = "Q4";
            this.checkBoxQ4.ThemeName = "TelerikMetro";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.checkBoxQ1);
            this.radGroupBox1.Controls.Add(this.checkBoxQ4);
            this.radGroupBox1.Controls.Add(this.checkBoxQ2);
            this.radGroupBox1.Controls.Add(this.checkBoxQ3);
            this.radGroupBox1.HeaderText = "Quarters:";
            this.radGroupBox1.Location = new System.Drawing.Point(25, 537);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(135, 108);
            this.radGroupBox1.TabIndex = 4;
            this.radGroupBox1.Text = "Quarters:";
            this.radGroupBox1.ThemeName = "TelerikMetro";
            // 
            // dropDownListNumberOfProducts
            // 
            this.dropDownListNumberOfProducts.AllowShowFocusCues = false;
            this.dropDownListNumberOfProducts.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "1";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "2";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "3";
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "4";
            radListDataItem4.TextWrap = true;
            radListDataItem5.Text = "5";
            radListDataItem5.TextWrap = true;
            radListDataItem6.Text = "6";
            radListDataItem6.TextWrap = true;
            this.dropDownListNumberOfProducts.Items.Add(radListDataItem1);
            this.dropDownListNumberOfProducts.Items.Add(radListDataItem2);
            this.dropDownListNumberOfProducts.Items.Add(radListDataItem3);
            this.dropDownListNumberOfProducts.Items.Add(radListDataItem4);
            this.dropDownListNumberOfProducts.Items.Add(radListDataItem5);
            this.dropDownListNumberOfProducts.Items.Add(radListDataItem6);
            this.dropDownListNumberOfProducts.Location = new System.Drawing.Point(194, 561);
            this.dropDownListNumberOfProducts.Name = "dropDownListNumberOfProducts";
            this.dropDownListNumberOfProducts.Size = new System.Drawing.Size(108, 19);
            this.dropDownListNumberOfProducts.TabIndex = 5;
            this.dropDownListNumberOfProducts.ThemeName = "TelerikMetro";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(194, 537);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(107, 16);
            this.radLabel1.TabIndex = 6;
            this.radLabel1.Text = "Number of Products";
            this.radLabel1.ThemeName = "TelerikMetro";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(13, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(618, 476);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // chartValueStepEditor
            // 
            this.chartValueStepEditor.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.chartValueStepEditor.Location = new System.Drawing.Point(194, 611);
            this.chartValueStepEditor.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.chartValueStepEditor.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.chartValueStepEditor.Name = "chartValueStepEditor";
            this.chartValueStepEditor.Size = new System.Drawing.Size(108, 22);
            this.chartValueStepEditor.TabIndex = 8;
            this.chartValueStepEditor.TabStop = false;
            this.chartValueStepEditor.ThemeName = "TelerikMetro";
            this.chartValueStepEditor.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(194, 587);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(95, 16);
            this.radLabel2.TabIndex = 7;
            this.radLabel2.Text = "Chart Value Step:";
            this.radLabel2.ThemeName = "TelerikMetro";
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(13, 684);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(618, 24);
            this.buttonSave.TabIndex = 9;
            this.buttonSave.Text = "Save Document";
            this.buttonSave.ThemeName = "TelerikMetro";
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 720);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.chartValueStepEditor);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.dropDownListNumberOfProducts);
            this.Controls.Add(this.radGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Bar Chart";
            this.ThemeName = "TelerikMetro";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxQ1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxQ2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxQ3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxQ4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dropDownListNumberOfProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartValueStepEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCheckBox checkBoxQ1;
        private Telerik.WinControls.UI.RadCheckBox checkBoxQ2;
        private Telerik.WinControls.UI.RadCheckBox checkBoxQ3;
        private Telerik.WinControls.UI.RadCheckBox checkBoxQ4;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadDropDownList dropDownListNumberOfProducts;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadSpinEditor chartValueStepEditor;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadButton buttonSave;
        private Telerik.WinControls.Themes.TelerikMetroTheme telerikMetroTheme1;
    }
}

