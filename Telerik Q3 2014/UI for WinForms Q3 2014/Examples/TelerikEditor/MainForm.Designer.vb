Namespace TelerikEditor
	Partial Class MainForm
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#region Windows Form Designer generated code

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.components = New System.ComponentModel.Container()
			Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
			Dim radListDataItem1 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem2 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem3 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem4 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem5 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem6 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem7 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem8 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem9 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem10 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem11 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem12 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem13 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem14 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem15 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem16 As New Telerik.WinControls.UI.RadListDataItem()
			Me.imageList32Size = New System.Windows.Forms.ImageList(Me.components)
			Me.office2010BlueTheme1 = New Telerik.WinControls.Themes.Office2010BlueTheme()
			Me.radRichTextBox1 = New Telerik.WinControls.RichTextBox.RadRichTextBox()
			Me.radSpellChecker1 = New Telerik.WinControls.UI.RadSpellChecker()
			Me.radRibbonBarBackstageView1 = New Telerik.WinControls.UI.RadRibbonBarBackstageView()
			Me.backstageViewPage1 = New Telerik.WinControls.UI.BackstageViewPage()
			Me.radPageView1 = New Telerik.WinControls.UI.RadPageView()
			Me.radPageViewPageSaveAsWord = New Telerik.WinControls.UI.RadPageViewPage()
			Me.documentInfoControl1 = New TelerikEditor.DocumentInfoControl()
			Me.radPageViewPageSaveAsPDF = New Telerik.WinControls.UI.RadPageViewPage()
			Me.documentInfoControl2 = New TelerikEditor.DocumentInfoControl()
			Me.radPageViewPageSaveAsHtml = New Telerik.WinControls.UI.RadPageViewPage()
			Me.documentInfoControl3 = New TelerikEditor.DocumentInfoControl()
			Me.radPageViewPageSaveAsRTF = New Telerik.WinControls.UI.RadPageViewPage()
			Me.documentInfoControl4 = New TelerikEditor.DocumentInfoControl()
			Me.radPageViewPageSaveAsText = New Telerik.WinControls.UI.RadPageViewPage()
			Me.documentInfoControl5 = New TelerikEditor.DocumentInfoControl()
			Me.radPageViewPageSaveAsXAML = New Telerik.WinControls.UI.RadPageViewPage()
			Me.documentInfoControl6 = New TelerikEditor.DocumentInfoControl()
			Me.backstageButtonOpen = New Telerik.WinControls.UI.BackstageButtonItem()
			Me.backstageButtonSave = New Telerik.WinControls.UI.BackstageButtonItem()
			Me.backstageTabSaveAs = New Telerik.WinControls.UI.BackstageTabItem()
			Me.backstageButtonNew = New Telerik.WinControls.UI.BackstageButtonItem()
			Me.backstageButtonItemExit = New Telerik.WinControls.UI.BackstageButtonItem()
			Me.radRibbonBar1 = New Telerik.WinControls.UI.RadRibbonBar()
			Me.ribbonTab1 = New Telerik.WinControls.UI.RibbonTab()
			Me.radRibbonBarGroup7 = New Telerik.WinControls.UI.RadRibbonBarGroup()
			Me.radBtnCut = New Telerik.WinControls.UI.RadButtonElement()
			Me.radBtnCopy = New Telerik.WinControls.UI.RadButtonElement()
			Me.radButtonPaste = New Telerik.WinControls.UI.RadButtonElement()
			Me.radRibbonBarGroup9 = New Telerik.WinControls.UI.RadRibbonBarGroup()
			Me.radRibbonBarButtonGroup1 = New Telerik.WinControls.UI.RadRibbonBarButtonGroup()
			Me.radDropDownListFont = New Telerik.WinControls.UI.RadDropDownListElement()
			Me.radDropDownListFontSize = New Telerik.WinControls.UI.RadDropDownListElement()
			Me.radRibbonBarButtonGroup4 = New Telerik.WinControls.UI.RadRibbonBarButtonGroup()
			Me.radBtnFontSizeIncrease = New Telerik.WinControls.UI.RadButtonElement()
			Me.radBtnFontSizeDecrease = New Telerik.WinControls.UI.RadButtonElement()
			Me.radRibbonBarButtonGroup3 = New Telerik.WinControls.UI.RadRibbonBarButtonGroup()
			Me.radBtnFormattingClear = New Telerik.WinControls.UI.RadButtonElement()
			Me.radRibbonBarButtonGroup2 = New Telerik.WinControls.UI.RadRibbonBarButtonGroup()
			Me.radRibbonBarButtonGroup5 = New Telerik.WinControls.UI.RadRibbonBarButtonGroup()
			Me.radBtnBoldStyle = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.radBtnItalicStyle = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.radBtnUnderlineStyle = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.radBtnStrikethrough = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.radRibbonBarButtonGroup6 = New Telerik.WinControls.UI.RadRibbonBarButtonGroup()
			Me.radBtnHighlight = New Telerik.WinControls.UI.RadButtonElement()
			Me.radBtnFontColor = New Telerik.WinControls.UI.RadButtonElement()
			Me.radRibbonBarButtonGroup11 = New Telerik.WinControls.UI.RadRibbonBarButtonGroup()
			Me.subscriptButtonElement = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.superscriptButtonElement = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.radRibbonBarGroup8 = New Telerik.WinControls.UI.RadRibbonBarGroup()
			Me.radRibbonBarButtonGroup7 = New Telerik.WinControls.UI.RadRibbonBarButtonGroup()
			Me.radRibbonBarButtonGroup9 = New Telerik.WinControls.UI.RadRibbonBarButtonGroup()
			Me.radBtnBulletList = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.radNumberingList = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.radRibbonBarButtonGroup10 = New Telerik.WinControls.UI.RadRibbonBarButtonGroup()
			Me.radBtnDecreaseIndent = New Telerik.WinControls.UI.RadButtonElement()
			Me.radBtnIncreaseIndent = New Telerik.WinControls.UI.RadButtonElement()
			Me.radRibbonBarButtonGroup14 = New Telerik.WinControls.UI.RadRibbonBarButtonGroup()
			Me.radBtnShowFormatting = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.radRibbonBarButtonGroup8 = New Telerik.WinControls.UI.RadRibbonBarButtonGroup()
			Me.radRibbonBarButtonGroup12 = New Telerik.WinControls.UI.RadRibbonBarButtonGroup()
			Me.radBtnAligntLeft = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.radBtnAlignCenter = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.radBtnAligntRight = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.radBtnAlignJustify = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.radRibbonBarButtonGroup13 = New Telerik.WinControls.UI.RadRibbonBarButtonGroup()
			Me.radBtnBackColor = New Telerik.WinControls.UI.RadButtonElement()
			Me.radRibbonBarGroup10 = New Telerik.WinControls.UI.RadRibbonBarGroup()
			Me.radBtnEnableSpellCheck = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.ribbonTab2 = New Telerik.WinControls.UI.RibbonTab()
			Me.radRibbonBarGroup1 = New Telerik.WinControls.UI.RadRibbonBarGroup()
			Me.radBtnPageBreak = New Telerik.WinControls.UI.RadButtonElement()
			Me.radRibbonBarGroup2 = New Telerik.WinControls.UI.RadRibbonBarGroup()
			Me.radBtnInsertPicture = New Telerik.WinControls.UI.RadButtonElement()
			Me.radRibbonBarGroup11 = New Telerik.WinControls.UI.RadRibbonBarGroup()
			Me.radDropDownButtonElementTables = New Telerik.WinControls.UI.RadDropDownButtonElement()
			Me.radMenuItemInsertTable = New Telerik.WinControls.UI.RadMenuItem()
			Me.radRibbonBarGroup3 = New Telerik.WinControls.UI.RadRibbonBarGroup()
			Me.hyperlinkButtonElement = New Telerik.WinControls.UI.RadButtonElement()
			Me.bookmarkButtonElement = New Telerik.WinControls.UI.RadButtonElement()
			Me.ribbonTab3 = New Telerik.WinControls.UI.RibbonTab()
			Me.radRibbonBarGroup4 = New Telerik.WinControls.UI.RadRibbonBarGroup()
			Me.radDropDownButtonSize = New Telerik.WinControls.UI.RadDropDownButtonElement()
			Me.radMenuItemSizeA0 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSizeA1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSizeA2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSizeA3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSizeA4 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radDropDownButtonPageMargins = New Telerik.WinControls.UI.RadDropDownButtonElement()
			Me.radMenuItemNormal = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemNarrow = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemModerate = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemOffice2003 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radDropDownButtonPageOrientation = New Telerik.WinControls.UI.RadDropDownButtonElement()
			Me.radMenuItemLandscape = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemPortrait = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemRotate180 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemRotate270 = New Telerik.WinControls.UI.RadMenuItem()
			Me.ribbonTab4 = New Telerik.WinControls.UI.RibbonTab()
			Me.radRibbonBarGroup5 = New Telerik.WinControls.UI.RadRibbonBarGroup()
			Me.radBtnWebLayout = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.radBtnPrintLayout = New Telerik.WinControls.UI.RadToggleButtonElement()
			Me.ribbonTab5 = New Telerik.WinControls.UI.RibbonTab()
			Me.radRibbonBarGroup6 = New Telerik.WinControls.UI.RadRibbonBarGroup()
			Me.radBtnSpellCheck = New Telerik.WinControls.UI.RadButtonElement()
			Me.radBtnSave = New Telerik.WinControls.UI.RadButtonElement()
			Me.radBtnUndo = New Telerik.WinControls.UI.RadButtonElement()
			Me.radBtnRedo = New Telerik.WinControls.UI.RadButtonElement()
			Me.radMenuItemNew = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemOpenDoc = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSave = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSaveAs = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSaveWordDoc = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSavePDFDocument = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSaveHtmlDocument = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenutItemSaveRTF = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSavePlainText = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSaveXAML = New Telerik.WinControls.UI.RadMenuItem()
			Me.radButtonElement1 = New Telerik.WinControls.UI.RadButtonElement()
			(CType((Me.radRichTextBox1), System.ComponentModel.ISupportInitialize)).BeginInit()
			(CType((Me.radRibbonBarBackstageView1), System.ComponentModel.ISupportInitialize)).BeginInit()
			Me.radRibbonBarBackstageView1.SuspendLayout()
			Me.backstageViewPage1.SuspendLayout()
			(CType((Me.radPageView1), System.ComponentModel.ISupportInitialize)).BeginInit()
			Me.radPageView1.SuspendLayout()
			Me.radPageViewPageSaveAsWord.SuspendLayout()
			Me.radPageViewPageSaveAsPDF.SuspendLayout()
			Me.radPageViewPageSaveAsHtml.SuspendLayout()
			Me.radPageViewPageSaveAsRTF.SuspendLayout()
			Me.radPageViewPageSaveAsText.SuspendLayout()
			Me.radPageViewPageSaveAsXAML.SuspendLayout()
			(CType((Me.radRibbonBar1), System.ComponentModel.ISupportInitialize)).BeginInit()
			(CType((Me.radDropDownListFont), System.ComponentModel.ISupportInitialize)).BeginInit()
			(CType((Me.radDropDownListFontSize), System.ComponentModel.ISupportInitialize)).BeginInit()
			(CType((Me), System.ComponentModel.ISupportInitialize)).BeginInit()
			Me.SuspendLayout()
			' 
			' imageList32Size
			' 
			Me.imageList32Size.ImageStream = (CType((resources.GetObject("imageList32Size.ImageStream")), System.Windows.Forms.ImageListStreamer))
			Me.imageList32Size.TransparentColor = System.Drawing.Color.Transparent
			Me.imageList32Size.Images.SetKeyName(0, "Hyperlink")
			Me.imageList32Size.Images.SetKeyName(1, "PageBreak")
			Me.imageList32Size.Images.SetKeyName(2, "Picture")
			Me.imageList32Size.Images.SetKeyName(3, "PageMargin")
			Me.imageList32Size.Images.SetKeyName(4, "PageOrientation")
			Me.imageList32Size.Images.SetKeyName(5, "PageSize")
			Me.imageList32Size.Images.SetKeyName(6, "WebLayout")
			Me.imageList32Size.Images.SetKeyName(7, "SpellCheck")
			Me.imageList32Size.Images.SetKeyName(8, "PageOrientationLandscape.png")
			Me.imageList32Size.Images.SetKeyName(9, "PageOrientationPortrait.png")
			Me.imageList32Size.Images.SetKeyName(10, "PageOrientationRotate180.png")
			Me.imageList32Size.Images.SetKeyName(11, "PageOrientationRotate270.png")
			Me.imageList32Size.Images.SetKeyName(12, "new.png")
			Me.imageList32Size.Images.SetKeyName(13, "save.png")
			Me.imageList32Size.Images.SetKeyName(14, "saveas.png")
			Me.imageList32Size.Images.SetKeyName(15, "Html.png")
			Me.imageList32Size.Images.SetKeyName(16, "Pdf.png")
			Me.imageList32Size.Images.SetKeyName(17, "PlainText.png")
			Me.imageList32Size.Images.SetKeyName(18, "Rtf.png")
			Me.imageList32Size.Images.SetKeyName(19, "worddoc.png")
			Me.imageList32Size.Images.SetKeyName(20, "Xaml.png")
			Me.imageList32Size.Images.SetKeyName(21, "open.png")
			Me.imageList32Size.Images.SetKeyName(22, "exit.png")
			Me.imageList32Size.Images.SetKeyName(23, "Table.png")
			' 
			' radRichTextBox1
			' 
			Me.radRichTextBox1.Dock = System.Windows.Forms.DockStyle.Fill
			Me.radRichTextBox1.LayoutMode = Telerik.WinControls.RichTextBox.Model.DocumentLayoutMode.Paged
			Me.radRichTextBox1.Location = New System.Drawing.Point(0, 154)
			Me.radRichTextBox1.Name = "radRichTextBox1"
			Me.radRichTextBox1.Size = New System.Drawing.Size(992, 457)
			Me.radRichTextBox1.TabIndex = 1
			Me.radRichTextBox1.Text = "radRichTextBox1"
			' 
			' radRibbonBarBackstageView1
			' 
			Me.radRibbonBarBackstageView1.Controls.Add(Me.backstageViewPage1)
			Me.radRibbonBarBackstageView1.ImageList = Me.imageList32Size
			Me.radRibbonBarBackstageView1.Items.AddRange(New Telerik.WinControls.RadItem() {Me.backstageButtonOpen, Me.backstageButtonSave, Me.backstageTabSaveAs, Me.backstageButtonNew, Me.backstageButtonItemExit})
			Me.radRibbonBarBackstageView1.Location = New System.Drawing.Point(0, 63)
			Me.radRibbonBarBackstageView1.Name = "radRibbonBarBackstageView1"
			Me.radRibbonBarBackstageView1.RightToLeft = System.Windows.Forms.RightToLeft.No
			Me.radRibbonBarBackstageView1.SelectedItem = Me.backstageTabSaveAs
			Me.radRibbonBarBackstageView1.Size = New System.Drawing.Size(992, 548)
			Me.radRibbonBarBackstageView1.TabIndex = 2
			' 
			' backstageViewPage1
			' 
			Me.backstageViewPage1.BackColor = System.Drawing.Color.Transparent
			Me.backstageViewPage1.Controls.Add(Me.radPageView1)
			Me.backstageViewPage1.Location = New System.Drawing.Point(132, 1)
			Me.backstageViewPage1.Name = "backstageViewPage1"
			Me.backstageViewPage1.Size = New System.Drawing.Size(860, 547)
			Me.backstageViewPage1.TabIndex = 0
			' 
			' radPageView1
			' 
			Me.radPageView1.Controls.Add(Me.radPageViewPageSaveAsWord)
			Me.radPageView1.Controls.Add(Me.radPageViewPageSaveAsPDF)
			Me.radPageView1.Controls.Add(Me.radPageViewPageSaveAsHtml)
			Me.radPageView1.Controls.Add(Me.radPageViewPageSaveAsRTF)
			Me.radPageView1.Controls.Add(Me.radPageViewPageSaveAsText)
			Me.radPageView1.Controls.Add(Me.radPageViewPageSaveAsXAML)
			Me.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill
			Me.radPageView1.Location = New System.Drawing.Point(0, 0)
			Me.radPageView1.Name = "radPageView1"
			Me.radPageView1.SelectedPage = Me.radPageViewPageSaveAsXAML
			Me.radPageView1.Size = New System.Drawing.Size(860, 547)
			Me.radPageView1.TabIndex = 0
			Me.radPageView1.Text = "radPageView1"
			Me.radPageView1.ViewMode = Telerik.WinControls.UI.PageViewMode.Backstage
			' 
			' radPageViewPageSaveAsWord
			' 
			Me.radPageViewPageSaveAsWord.Controls.Add(Me.documentInfoControl1)
			Me.radPageViewPageSaveAsWord.Image = Global.TelerikEditor.Properties.Resources.worddoc
			Me.radPageViewPageSaveAsWord.Location = New System.Drawing.Point(305, 4)
			Me.radPageViewPageSaveAsWord.Name = "radPageViewPageSaveAsWord"
			Me.radPageViewPageSaveAsWord.Size = New System.Drawing.Size(551, 546)
			Me.radPageViewPageSaveAsWord.Text = "Word Document"
			' 
			' documentInfoControl1
			' 
			Me.documentInfoControl1.Dock = System.Windows.Forms.DockStyle.Left
			Me.documentInfoControl1.Location = New System.Drawing.Point(0, 0)
			Me.documentInfoControl1.Name = "documentInfoControl1"
			Me.documentInfoControl1.Size = New System.Drawing.Size(276, 546)
			Me.documentInfoControl1.TabIndex = 0
			' 
			' radPageViewPageSaveAsPDF
			' 
			Me.radPageViewPageSaveAsPDF.Controls.Add(Me.documentInfoControl2)
			Me.radPageViewPageSaveAsPDF.Image = Global.TelerikEditor.Properties.Resources.Pdf
			Me.radPageViewPageSaveAsPDF.Location = New System.Drawing.Point(305, 4)
			Me.radPageViewPageSaveAsPDF.Name = "radPageViewPageSaveAsPDF"
			Me.radPageViewPageSaveAsPDF.Size = New System.Drawing.Size(551, 546)
			Me.radPageViewPageSaveAsPDF.Text = "PDF Document"
			' 
			' documentInfoControl2
			' 
			Me.documentInfoControl2.Dock = System.Windows.Forms.DockStyle.Left
			Me.documentInfoControl2.Location = New System.Drawing.Point(0, 0)
			Me.documentInfoControl2.Name = "documentInfoControl2"
			Me.documentInfoControl2.Size = New System.Drawing.Size(276, 546)
			Me.documentInfoControl2.TabIndex = 0
			' 
			' radPageViewPageSaveAsHtml
			' 
			Me.radPageViewPageSaveAsHtml.Controls.Add(Me.documentInfoControl3)
			Me.radPageViewPageSaveAsHtml.Image = Global.TelerikEditor.Properties.Resources.Html
			Me.radPageViewPageSaveAsHtml.Location = New System.Drawing.Point(305, 4)
			Me.radPageViewPageSaveAsHtml.Name = "radPageViewPageSaveAsHtml"
			Me.radPageViewPageSaveAsHtml.Size = New System.Drawing.Size(551, 546)
			Me.radPageViewPageSaveAsHtml.Text = "Html Document"
			' 
			' documentInfoControl3
			' 
			Me.documentInfoControl3.Dock = System.Windows.Forms.DockStyle.Left
			Me.documentInfoControl3.Location = New System.Drawing.Point(0, 0)
			Me.documentInfoControl3.Name = "documentInfoControl3"
			Me.documentInfoControl3.Size = New System.Drawing.Size(276, 546)
			Me.documentInfoControl3.TabIndex = 0
			' 
			' radPageViewPageSaveAsRTF
			' 
			Me.radPageViewPageSaveAsRTF.Controls.Add(Me.documentInfoControl4)
			Me.radPageViewPageSaveAsRTF.Image = Global.TelerikEditor.Properties.Resources.Rtf
			Me.radPageViewPageSaveAsRTF.Location = New System.Drawing.Point(305, 4)
			Me.radPageViewPageSaveAsRTF.Name = "radPageViewPageSaveAsRTF"
			Me.radPageViewPageSaveAsRTF.Size = New System.Drawing.Size(551, 546)
			Me.radPageViewPageSaveAsRTF.Text = "Rich Text Format"
			' 
			' documentInfoControl4
			' 
			Me.documentInfoControl4.Dock = System.Windows.Forms.DockStyle.Left
			Me.documentInfoControl4.Location = New System.Drawing.Point(0, 0)
			Me.documentInfoControl4.Name = "documentInfoControl4"
			Me.documentInfoControl4.Size = New System.Drawing.Size(276, 546)
			Me.documentInfoControl4.TabIndex = 0
			' 
			' radPageViewPageSaveAsText
			' 
			Me.radPageViewPageSaveAsText.Controls.Add(Me.documentInfoControl5)
			Me.radPageViewPageSaveAsText.Image = Global.TelerikEditor.Properties.Resources.PlainText
			Me.radPageViewPageSaveAsText.Location = New System.Drawing.Point(305, 4)
			Me.radPageViewPageSaveAsText.Name = "radPageViewPageSaveAsText"
			Me.radPageViewPageSaveAsText.Size = New System.Drawing.Size(551, 546)
			Me.radPageViewPageSaveAsText.Text = "Plain Text"
			' 
			' documentInfoControl5
			' 
			Me.documentInfoControl5.Dock = System.Windows.Forms.DockStyle.Left
			Me.documentInfoControl5.Location = New System.Drawing.Point(0, 0)
			Me.documentInfoControl5.Name = "documentInfoControl5"
			Me.documentInfoControl5.Size = New System.Drawing.Size(276, 546)
			Me.documentInfoControl5.TabIndex = 0
			' 
			' radPageViewPageSaveAsXAML
			' 
			Me.radPageViewPageSaveAsXAML.Controls.Add(Me.documentInfoControl6)
			Me.radPageViewPageSaveAsXAML.Image = Global.TelerikEditor.Properties.Resources.Xaml
			Me.radPageViewPageSaveAsXAML.Location = New System.Drawing.Point(305, 4)
			Me.radPageViewPageSaveAsXAML.Name = "radPageViewPageSaveAsXAML"
			Me.radPageViewPageSaveAsXAML.Size = New System.Drawing.Size(551, 539)
			Me.radPageViewPageSaveAsXAML.Text = "XAML Document"
			' 
			' documentInfoControl6
			' 
			Me.documentInfoControl6.Dock = System.Windows.Forms.DockStyle.Left
			Me.documentInfoControl6.Location = New System.Drawing.Point(0, 0)
			Me.documentInfoControl6.Name = "documentInfoControl6"
			Me.documentInfoControl6.Size = New System.Drawing.Size(276, 539)
			Me.documentInfoControl6.TabIndex = 0
			' 
			' backstageButtonOpen
			' 
			Me.backstageButtonOpen.AccessibleDescription = "Open"
			Me.backstageButtonOpen.AccessibleName = "Open"
			Me.backstageButtonOpen.Font = New System.Drawing.Font("Segoe UI", 10.5F)
			Me.backstageButtonOpen.Image = Global.TelerikEditor.Properties.Resources.open
			Me.backstageButtonOpen.ImageIndex = -1
			Me.backstageButtonOpen.Name = "backstageButtonOpen"
			Me.backstageButtonOpen.Text = "Open"
			Me.backstageButtonOpen.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.backstageButtonOpen.Click, New System.EventHandler(AddressOf(Me.radMenuItemOpenDoc_Click))
			' 
			' backstageButtonSave
			' 
			Me.backstageButtonSave.AccessibleDescription = "Save"
			Me.backstageButtonSave.AccessibleName = "Save"
			Me.backstageButtonSave.Font = New System.Drawing.Font("Segoe UI", 10.5F)
			Me.backstageButtonSave.Image = Global.TelerikEditor.Properties.Resources.save1
			Me.backstageButtonSave.ImageIndex = -1
			Me.backstageButtonSave.Name = "backstageButtonSave"
			Me.backstageButtonSave.Text = "Save"
			Me.backstageButtonSave.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft
			Me.backstageButtonSave.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.backstageButtonSave.Click, New System.EventHandler(AddressOf(Me.radBtnSave_Click))
			' 
			' backstageTabSaveAs
			' 
			Me.backstageTabSaveAs.AccessibleDescription = "Save As"
			Me.backstageTabSaveAs.AccessibleName = "Save As"
			Me.backstageTabSaveAs.Font = New System.Drawing.Font("Segoe UI", 10.5F)
			Me.backstageTabSaveAs.Image = Global.TelerikEditor.Properties.Resources.save_as
			Me.backstageTabSaveAs.ImageIndex = -1
			Me.backstageTabSaveAs.Name = "backstageTabSaveAs"
			Me.backstageTabSaveAs.Padding = New System.Windows.Forms.Padding(16, 0, 20, 0)
			Me.backstageTabSaveAs.Page = Me.backstageViewPage1
			Me.backstageTabSaveAs.Text = "Save As"
			Me.backstageTabSaveAs.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft
			Me.backstageTabSaveAs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
			Me.backstageTabSaveAs.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' backstageButtonNew
			' 
			Me.backstageButtonNew.AccessibleDescription = "New"
			Me.backstageButtonNew.AccessibleName = "New"
			Me.backstageButtonNew.Font = New System.Drawing.Font("Segoe UI", 10.5F)
			Me.backstageButtonNew.Image = Global.TelerikEditor.Properties.Resources._new
			Me.backstageButtonNew.ImageIndex = -1
			Me.backstageButtonNew.Name = "backstageButtonNew"
			Me.backstageButtonNew.Text = "New"
			Me.backstageButtonNew.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.backstageButtonNew.Click, New System.EventHandler(AddressOf(Me.radMenuItemNew_Click))
			' 
			' backstageButtonItemExit
			' 
			Me.backstageButtonItemExit.AccessibleDescription = "Exit"
			Me.backstageButtonItemExit.AccessibleName = "Exit"
			Me.backstageButtonItemExit.Font = New System.Drawing.Font("Segoe UI", 10.5F)
			Me.backstageButtonItemExit.Image = Global.TelerikEditor.Properties.Resources.[exit]
			Me.backstageButtonItemExit.ImageIndex = -1
			Me.backstageButtonItemExit.Name = "backstageButtonItemExit"
			Me.backstageButtonItemExit.Text = "Exit"
			Me.backstageButtonItemExit.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.backstageButtonItemExit.Click, New System.EventHandler(AddressOf(Me.backstageButtonItemExit_Click))
			' 
			' radRibbonBar1
			' 
			Me.radRibbonBar1.ApplicationMenuStyle = Telerik.WinControls.UI.ApplicationMenuStyle.BackstageView
			Me.radRibbonBar1.AutoSize = True
			Me.radRibbonBar1.BackstageControl = Me.radRibbonBarBackstageView1
			Me.radRibbonBar1.CommandTabs.AddRange(New Telerik.WinControls.RadItem() {Me.ribbonTab1, Me.ribbonTab2, Me.ribbonTab3, Me.ribbonTab4, Me.ribbonTab5})
			Me.radRibbonBar1.Dock = System.Windows.Forms.DockStyle.Top
			' 
			' 
			' 
			Me.radRibbonBar1.ExitButton.AccessibleDescription = "Exit"
			Me.radRibbonBar1.ExitButton.AccessibleName = "Exit"
			' 
			' 
			' 
			Me.radRibbonBar1.ExitButton.ButtonElement.AccessibleDescription = "Exit"
			Me.radRibbonBar1.ExitButton.ButtonElement.AccessibleName = "Exit"
			Me.radRibbonBar1.ExitButton.Text = "Exit"
			Me.radRibbonBar1.ImageList = Me.imageList32Size
			Me.radRibbonBar1.Location = New System.Drawing.Point(0, 0)
			Me.radRibbonBar1.Name = "radRibbonBar1"
			' 
			' 
			' 
			Me.radRibbonBar1.OptionsButton.AccessibleDescription = "Options"
			Me.radRibbonBar1.OptionsButton.AccessibleName = "Options"
			' 
			' 
			' 
			Me.radRibbonBar1.OptionsButton.ButtonElement.AccessibleDescription = "Options"
			Me.radRibbonBar1.OptionsButton.ButtonElement.AccessibleName = "Options"
			Me.radRibbonBar1.OptionsButton.Text = "Options"
			Me.radRibbonBar1.QuickAccessToolBarItems.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnSave, Me.radBtnUndo, Me.radBtnRedo})
			Me.radRibbonBar1.Size = New System.Drawing.Size(992, 154)
			Me.radRibbonBar1.StartButtonImage = (CType((resources.GetObject("radRibbonBar1.StartButtonImage")), System.Drawing.Image))
			Me.radRibbonBar1.StartMenuItems.AddRange(New Telerik.WinControls.RadItem() {Me.radMenuItemNew, Me.radMenuItemOpenDoc, Me.radMenuItemSave, Me.radMenuItemSaveAs})
			Me.radRibbonBar1.TabIndex = 0
			Me.radRibbonBar1.Text = "Telerik Editor"
			' 
			' ribbonTab1
			' 
			Me.ribbonTab1.AccessibleDescription = "Home"
			Me.ribbonTab1.AccessibleName = "Home"
			Me.ribbonTab1.[Class] = "RibbonTab"
			Me.ribbonTab1.IsSelected = True
			Me.ribbonTab1.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radRibbonBarGroup7, Me.radRibbonBarGroup9, Me.radRibbonBarGroup8, Me.radRibbonBarGroup10})
			Me.ribbonTab1.Name = "ribbonTab1"
			Me.ribbonTab1.Text = "Home"
			Me.ribbonTab1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radRibbonBarGroup7
			' 
			Me.radRibbonBarGroup7.AccessibleDescription = "Clipboard"
			Me.radRibbonBarGroup7.AccessibleName = "Clipboard"
			Me.radRibbonBarGroup7.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnCut, Me.radBtnCopy, Me.radButtonPaste})
			Me.radRibbonBarGroup7.Margin = New System.Windows.Forms.Padding(-1, -1, 0, -1)
			Me.radRibbonBarGroup7.Name = "radRibbonBarGroup7"
			Me.radRibbonBarGroup7.Orientation = System.Windows.Forms.Orientation.Vertical
			Me.radRibbonBarGroup7.Text = "Clipboard"
			Me.radRibbonBarGroup7.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnCut
			' 
			Me.radBtnCut.AccessibleDescription = "Cut"
			Me.radBtnCut.AccessibleName = "Cut"
			Me.radBtnCut.[Class] = "RibbonBarButtonElement"
			Me.radBtnCut.Image = Global.TelerikEditor.Properties.Resources.cut
			Me.radBtnCut.Name = "radBtnCut"
			Me.radBtnCut.Text = "Cut"
			Me.radBtnCut.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnCut.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
			Me.radBtnCut.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnCut.Click, New System.EventHandler(AddressOf(Me.radBtnCut_Click))
			' 
			' radBtnCopy
			' 
			Me.radBtnCopy.AccessibleDescription = "Copy"
			Me.radBtnCopy.AccessibleName = "Copy"
			Me.radBtnCopy.[Class] = "RibbonBarButtonElement"
			Me.radBtnCopy.Image = Global.TelerikEditor.Properties.Resources.copy
			Me.radBtnCopy.Name = "radBtnCopy"
			Me.radBtnCopy.Text = "Copy"
			Me.radBtnCopy.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnCopy.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
			Me.radBtnCopy.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnCopy.Click, New System.EventHandler(AddressOf(Me.radBtnCopy_Click))
			' 
			' radButtonPaste
			' 
			Me.radButtonPaste.AccessibleDescription = "Paste"
			Me.radButtonPaste.AccessibleName = "Paste"
			Me.radButtonPaste.[Class] = "RibbonBarButtonElement"
			Me.radButtonPaste.Image = Global.TelerikEditor.Properties.Resources.paste
			Me.radButtonPaste.Name = "radButtonPaste"
			Me.radButtonPaste.Text = "Paste"
			Me.radButtonPaste.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radButtonPaste.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
			Me.radButtonPaste.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radButtonPaste.Click, New System.EventHandler(AddressOf(Me.radButtonPaste_Click))
			' 
			' radRibbonBarGroup9
			' 
			Me.radRibbonBarGroup9.AccessibleDescription = "Paragraph"
			Me.radRibbonBarGroup9.AccessibleName = "Paragraph"
			Me.radRibbonBarGroup9.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radRibbonBarButtonGroup1, Me.radRibbonBarButtonGroup2})
			Me.radRibbonBarGroup9.Margin = New System.Windows.Forms.Padding(-1, -1, 0, -1)
			Me.radRibbonBarGroup9.Name = "radRibbonBarGroup9"
			Me.radRibbonBarGroup9.Orientation = System.Windows.Forms.Orientation.Vertical
			Me.radRibbonBarGroup9.Text = "Font"
			Me.radRibbonBarGroup9.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radRibbonBarButtonGroup1
			' 
			Me.radRibbonBarButtonGroup1.AccessibleDescription = "radRibbonBarButtonGroup1"
			Me.radRibbonBarButtonGroup1.AccessibleName = "radRibbonBarButtonGroup1"
			Me.radRibbonBarButtonGroup1.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radDropDownListFont, Me.radDropDownListFontSize, Me.radRibbonBarButtonGroup4, Me.radRibbonBarButtonGroup3})
			Me.radRibbonBarButtonGroup1.Margin = New System.Windows.Forms.Padding(0, 0, 0, 5)
			Me.radRibbonBarButtonGroup1.Name = "radRibbonBarButtonGroup1"
			Me.radRibbonBarButtonGroup1.Padding = New System.Windows.Forms.Padding(1)
			Me.radRibbonBarButtonGroup1.ShowBackColor = False
			Me.radRibbonBarButtonGroup1.ShowBorder = False
			Me.radRibbonBarButtonGroup1.Text = "radRibbonBarButtonGroup1"
			Me.radRibbonBarButtonGroup1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radDropDownListFont
			' 
			Me.radDropDownListFont.AccessibleDescription = "radDropDownListElement1"
			Me.radDropDownListFont.AccessibleName = "radDropDownListElement1"
			Me.radDropDownListFont.ArrowButtonMinWidth = 17
			Me.radDropDownListFont.AutoCompleteAppend = Nothing
			Me.radDropDownListFont.AutoCompleteDataSource = Nothing
			Me.radDropDownListFont.AutoCompleteDisplayMember = Nothing
			Me.radDropDownListFont.AutoCompleteSuggest = Nothing
			Me.radDropDownListFont.AutoCompleteValueMember = Nothing
			Me.radDropDownListFont.DataSource = Nothing
			Me.radDropDownListFont.DefaultItemsCountInDropDown = 6
			Me.radDropDownListFont.DefaultValue = Nothing
			Me.radDropDownListFont.DisplayMember = ""
			Me.radDropDownListFont.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad
			Me.radDropDownListFont.DropDownAnimationEnabled = True
			Me.radDropDownListFont.EditableElementText = ""
			Me.radDropDownListFont.EditorElement = Me.radDropDownListFont
			Me.radDropDownListFont.EditorManager = Nothing
			Me.radDropDownListFont.Filter = Nothing
			Me.radDropDownListFont.FilterExpression = ""
			Me.radDropDownListFont.Focusable = True
			Me.radDropDownListFont.FormatInfo = New System.Globalization.CultureInfo("en-US")
			Me.radDropDownListFont.FormatString = ""
			Me.radDropDownListFont.FormattingEnabled = True
			Me.radDropDownListFont.ItemHeight = 18
			Me.radDropDownListFont.MaxLength = 32767
			Me.radDropDownListFont.MaxValue = Nothing
			Me.radDropDownListFont.MinValue = Nothing
			Me.radDropDownListFont.Name = "radDropDownListFont"
			Me.radDropDownListFont.NullValue = Nothing
			Me.radDropDownListFont.OwnerOffset = 0
			Me.radDropDownListFont.ShowImageInEditorArea = True
			Me.radDropDownListFont.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None
			Me.radDropDownListFont.Value = Nothing
			Me.radDropDownListFont.ValueMember = ""
			Me.radDropDownListFont.Visibility = Telerik.WinControls.ElementVisibility.Visible
			Me.radDropDownListFont.SelectedIndexChanged += New Telerik.WinControls.UI.Data.PositionChangedEventHandler(Me.radDropDownListFont_SelectedIndexChanged)
			' 
			' radDropDownListFontSize
			' 
			Me.radDropDownListFontSize.AccessibleDescription = "12"
			Me.radDropDownListFontSize.AccessibleName = "12"
			Me.radDropDownListFontSize.ArrowButtonMinWidth = 17
			Me.radDropDownListFontSize.AutoCompleteAppend = Nothing
			Me.radDropDownListFontSize.AutoCompleteDataSource = Nothing
			Me.radDropDownListFontSize.AutoCompleteDisplayMember = Nothing
			Me.radDropDownListFontSize.AutoCompleteSuggest = Nothing
			Me.radDropDownListFontSize.AutoCompleteValueMember = Nothing
			Me.radDropDownListFontSize.DataSource = Nothing
			Me.radDropDownListFontSize.DefaultItemsCountInDropDown = 6
			Me.radDropDownListFontSize.DefaultValue = Nothing
			Me.radDropDownListFontSize.DisplayMember = ""
			Me.radDropDownListFontSize.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad
			Me.radDropDownListFontSize.DropDownAnimationEnabled = True
			Me.radDropDownListFontSize.EditableElementText = "8"
			Me.radDropDownListFontSize.EditorElement = Me.radDropDownListFontSize
			Me.radDropDownListFontSize.EditorManager = Nothing
			Me.radDropDownListFontSize.Filter = Nothing
			Me.radDropDownListFontSize.FilterExpression = ""
			Me.radDropDownListFontSize.Focusable = True
			Me.radDropDownListFontSize.FormatInfo = New System.Globalization.CultureInfo("en-US")
			Me.radDropDownListFontSize.FormatString = ""
			Me.radDropDownListFontSize.FormattingEnabled = True
			Me.radDropDownListFontSize.ItemHeight = 18
			radListDataItem1.Selected = True
			radListDataItem1.Text = "8"
			radListDataItem1.TextWrap = True
			radListDataItem2.Text = "9"
			radListDataItem2.TextWrap = True
			radListDataItem3.Text = "10"
			radListDataItem3.TextWrap = True
			radListDataItem4.Text = "11"
			radListDataItem4.TextWrap = True
			radListDataItem5.Text = "12"
			radListDataItem5.TextWrap = True
			radListDataItem6.Text = "14"
			radListDataItem6.TextWrap = True
			radListDataItem7.Text = "16"
			radListDataItem7.TextWrap = True
			radListDataItem8.Text = "18"
			radListDataItem8.TextWrap = True
			radListDataItem9.Text = "20"
			radListDataItem9.TextWrap = True
			radListDataItem10.Text = "22"
			radListDataItem10.TextWrap = True
			radListDataItem11.Text = "24"
			radListDataItem11.TextWrap = True
			radListDataItem12.Text = "26"
			radListDataItem12.TextWrap = True
			radListDataItem13.Text = "28"
			radListDataItem13.TextWrap = True
			radListDataItem14.Text = "36"
			radListDataItem14.TextWrap = True
			radListDataItem15.Text = "48"
			radListDataItem15.TextWrap = True
			radListDataItem16.Text = "72"
			radListDataItem16.TextWrap = True
			Me.radDropDownListFontSize.Items.Add(radListDataItem1)
			Me.radDropDownListFontSize.Items.Add(radListDataItem2)
			Me.radDropDownListFontSize.Items.Add(radListDataItem3)
			Me.radDropDownListFontSize.Items.Add(radListDataItem4)
			Me.radDropDownListFontSize.Items.Add(radListDataItem5)
			Me.radDropDownListFontSize.Items.Add(radListDataItem6)
			Me.radDropDownListFontSize.Items.Add(radListDataItem7)
			Me.radDropDownListFontSize.Items.Add(radListDataItem8)
			Me.radDropDownListFontSize.Items.Add(radListDataItem9)
			Me.radDropDownListFontSize.Items.Add(radListDataItem10)
			Me.radDropDownListFontSize.Items.Add(radListDataItem11)
			Me.radDropDownListFontSize.Items.Add(radListDataItem12)
			Me.radDropDownListFontSize.Items.Add(radListDataItem13)
			Me.radDropDownListFontSize.Items.Add(radListDataItem14)
			Me.radDropDownListFontSize.Items.Add(radListDataItem15)
			Me.radDropDownListFontSize.Items.Add(radListDataItem16)
			Me.radDropDownListFontSize.Margin = New System.Windows.Forms.Padding(5, 0, 0, 0)
			Me.radDropDownListFontSize.MaxLength = 32767
			Me.radDropDownListFontSize.MaxSize = New System.Drawing.Size(50, 0)
			Me.radDropDownListFontSize.MaxValue = Nothing
			Me.radDropDownListFontSize.MinValue = Nothing
			Me.radDropDownListFontSize.Name = "radDropDownListFontSize"
			Me.radDropDownListFontSize.NullValue = Nothing
			Me.radDropDownListFontSize.OwnerOffset = 0
			Me.radDropDownListFontSize.ShowImageInEditorArea = True
			Me.radDropDownListFontSize.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None
			Me.radDropDownListFontSize.Value = Nothing
			Me.radDropDownListFontSize.ValueMember = ""
			Me.radDropDownListFontSize.Visibility = Telerik.WinControls.ElementVisibility.Visible
			Me.radDropDownListFontSize.SelectedIndexChanged += New Telerik.WinControls.UI.Data.PositionChangedEventHandler(Me.radDropDownListFontSize_SelectedIndexChanged)
			' 
			' radRibbonBarButtonGroup4
			' 
			Me.radRibbonBarButtonGroup4.AccessibleDescription = "radRibbonBarButtonGroup4"
			Me.radRibbonBarButtonGroup4.AccessibleName = "radRibbonBarButtonGroup4"
			Me.radRibbonBarButtonGroup4.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnFontSizeIncrease, Me.radBtnFontSizeDecrease})
			Me.radRibbonBarButtonGroup4.Margin = New System.Windows.Forms.Padding(5, 0, 0, 0)
			Me.radRibbonBarButtonGroup4.Name = "radRibbonBarButtonGroup4"
			Me.radRibbonBarButtonGroup4.Padding = New System.Windows.Forms.Padding(1)
			Me.radRibbonBarButtonGroup4.ShowBackColor = False
			Me.radRibbonBarButtonGroup4.ShowBorder = False
			Me.radRibbonBarButtonGroup4.Text = "radRibbonBarButtonGroup4"
			Me.radRibbonBarButtonGroup4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnFontSizeIncrease
			' 
			Me.radBtnFontSizeIncrease.AccessibleDescription = "radButtonElement7"
			Me.radBtnFontSizeIncrease.AccessibleName = "radButtonElement7"
			Me.radBtnFontSizeIncrease.[Class] = "RibbonBarButtonElement"
			Me.radBtnFontSizeIncrease.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnFontSizeIncrease.Image = Global.TelerikEditor.Properties.Resources.font_increasesize
			Me.radBtnFontSizeIncrease.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnFontSizeIncrease.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnFontSizeIncrease.Name = "radBtnFontSizeIncrease"
			Me.radBtnFontSizeIncrease.ShowBorder = False
			Me.radBtnFontSizeIncrease.Text = "radButtonElement7"
			Me.radBtnFontSizeIncrease.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnFontSizeIncrease.Click, New System.EventHandler(AddressOf(Me.radBtnFontSizeIncrease_Click))
			' 
			' radBtnFontSizeDecrease
			' 
			Me.radBtnFontSizeDecrease.AccessibleDescription = "radButtonElement8"
			Me.radBtnFontSizeDecrease.AccessibleName = "radButtonElement8"
			Me.radBtnFontSizeDecrease.[Class] = "RibbonBarButtonElement"
			Me.radBtnFontSizeDecrease.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnFontSizeDecrease.Image = Global.TelerikEditor.Properties.Resources.font_decreasesize
			Me.radBtnFontSizeDecrease.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnFontSizeDecrease.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnFontSizeDecrease.Name = "radBtnFontSizeDecrease"
			Me.radBtnFontSizeDecrease.ShowBorder = False
			Me.radBtnFontSizeDecrease.Text = "radButtonElement8"
			Me.radBtnFontSizeDecrease.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnFontSizeDecrease.Click, New System.EventHandler(AddressOf(Me.radBtnFontSizeDecrease_Click))
			' 
			' radRibbonBarButtonGroup3
			' 
			Me.radRibbonBarButtonGroup3.AccessibleDescription = "radRibbonBarButtonGroup3"
			Me.radRibbonBarButtonGroup3.AccessibleName = "radRibbonBarButtonGroup3"
			Me.radRibbonBarButtonGroup3.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnFormattingClear})
			Me.radRibbonBarButtonGroup3.Margin = New System.Windows.Forms.Padding(5, 0, 0, 0)
			Me.radRibbonBarButtonGroup3.Name = "radRibbonBarButtonGroup3"
			Me.radRibbonBarButtonGroup3.Padding = New System.Windows.Forms.Padding(1)
			Me.radRibbonBarButtonGroup3.ShowBackColor = False
			Me.radRibbonBarButtonGroup3.ShowBorder = False
			Me.radRibbonBarButtonGroup3.Text = "radRibbonBarButtonGroup3"
			Me.radRibbonBarButtonGroup3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnFormattingClear
			' 
			Me.radBtnFormattingClear.AccessibleDescription = "radButtonElement6"
			Me.radBtnFormattingClear.AccessibleName = "radButtonElement6"
			Me.radBtnFormattingClear.[Class] = "RibbonBarButtonElement"
			Me.radBtnFormattingClear.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnFormattingClear.Image = Global.TelerikEditor.Properties.Resources.ClearFormatting16
			Me.radBtnFormattingClear.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnFormattingClear.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnFormattingClear.Name = "radBtnFormattingClear"
			Me.radBtnFormattingClear.ShowBorder = False
			Me.radBtnFormattingClear.Text = "radButtonElement6"
			Me.radBtnFormattingClear.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnFormattingClear.Click, New System.EventHandler(AddressOf(Me.radBtnFormattingClear_Click))
			' 
			' radRibbonBarButtonGroup2
			' 
			Me.radRibbonBarButtonGroup2.AccessibleDescription = "radRibbonBarButtonGroup2"
			Me.radRibbonBarButtonGroup2.AccessibleName = "radRibbonBarButtonGroup2"
			Me.radRibbonBarButtonGroup2.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radRibbonBarButtonGroup5, Me.radRibbonBarButtonGroup6, Me.radRibbonBarButtonGroup11})
			Me.radRibbonBarButtonGroup2.Name = "radRibbonBarButtonGroup2"
			Me.radRibbonBarButtonGroup2.Padding = New System.Windows.Forms.Padding(1)
			Me.radRibbonBarButtonGroup2.ShowBackColor = False
			Me.radRibbonBarButtonGroup2.ShowBorder = False
			Me.radRibbonBarButtonGroup2.Text = "radRibbonBarButtonGroup2"
			Me.radRibbonBarButtonGroup2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radRibbonBarButtonGroup5
			' 
			Me.radRibbonBarButtonGroup5.AccessibleDescription = "radRibbonBarButtonGroup5"
			Me.radRibbonBarButtonGroup5.AccessibleName = "radRibbonBarButtonGroup5"
			Me.radRibbonBarButtonGroup5.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnBoldStyle, Me.radBtnItalicStyle, Me.radBtnUnderlineStyle, Me.radBtnStrikethrough})
			Me.radRibbonBarButtonGroup5.Name = "radRibbonBarButtonGroup5"
			Me.radRibbonBarButtonGroup5.Padding = New System.Windows.Forms.Padding(1)
			Me.radRibbonBarButtonGroup5.ShowBackColor = False
			Me.radRibbonBarButtonGroup5.ShowBorder = False
			Me.radRibbonBarButtonGroup5.Text = "radRibbonBarButtonGroup5"
			Me.radRibbonBarButtonGroup5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnBoldStyle
			' 
			Me.radBtnBoldStyle.AccessibleDescription = "radButtonElement6"
			Me.radBtnBoldStyle.AccessibleName = "radButtonElement6"
			Me.radBtnBoldStyle.[Class] = ""
			Me.radBtnBoldStyle.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnBoldStyle.Image = Global.TelerikEditor.Properties.Resources.bold
			Me.radBtnBoldStyle.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnBoldStyle.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnBoldStyle.Name = "radBtnBoldStyle"
			Me.radBtnBoldStyle.Text = "radButtonElement6"
			Me.radBtnBoldStyle.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnBoldStyle.ToggleStateChanged, New Telerik.WinControls.UI.StateChangedEventHandler(AddressOf(Me.radBtnBoldStyle_ToggleStateChanged))
			' 
			' radBtnItalicStyle
			' 
			Me.radBtnItalicStyle.AccessibleDescription = "radButtonElement7"
			Me.radBtnItalicStyle.AccessibleName = "radButtonElement7"
			Me.radBtnItalicStyle.[Class] = ""
			Me.radBtnItalicStyle.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnItalicStyle.Image = Global.TelerikEditor.Properties.Resources.italic
			Me.radBtnItalicStyle.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnItalicStyle.Name = "radBtnItalicStyle"
			Me.radBtnItalicStyle.Text = "radButtonElement7"
			Me.radBtnItalicStyle.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnItalicStyle.ToggleStateChanged, New Telerik.WinControls.UI.StateChangedEventHandler(AddressOf(Me.radBtnItalicStyle_ToggleStateChanged))
			' 
			' radBtnUnderlineStyle
			' 
			Me.radBtnUnderlineStyle.AccessibleDescription = "radButtonElement8"
			Me.radBtnUnderlineStyle.AccessibleName = "radButtonElement8"
			Me.radBtnUnderlineStyle.[Class] = ""
			Me.radBtnUnderlineStyle.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnUnderlineStyle.Image = Global.TelerikEditor.Properties.Resources.underline
			Me.radBtnUnderlineStyle.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnUnderlineStyle.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnUnderlineStyle.Name = "radBtnUnderlineStyle"
			Me.radBtnUnderlineStyle.Text = "radButtonElement8"
			Me.radBtnUnderlineStyle.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnUnderlineStyle.ToggleStateChanged, New Telerik.WinControls.UI.StateChangedEventHandler(AddressOf(Me.radBtnUnderlineStyle_ToggleStateChanged))
			' 
			' radBtnStrikethrough
			' 
			Me.radBtnStrikethrough.AccessibleDescription = "radButtonElement9"
			Me.radBtnStrikethrough.AccessibleName = "radButtonElement9"
			Me.radBtnStrikethrough.[Class] = ""
			Me.radBtnStrikethrough.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnStrikethrough.Image = Global.TelerikEditor.Properties.Resources.Strikethrough16
			Me.radBtnStrikethrough.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnStrikethrough.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnStrikethrough.Name = "radBtnStrikethrough"
			Me.radBtnStrikethrough.Text = "radButtonElement9"
			Me.radBtnStrikethrough.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnStrikethrough.ToggleStateChanged, New Telerik.WinControls.UI.StateChangedEventHandler(AddressOf(Me.radBtnStrikethrough_ToggleStateChanged))
			' 
			' radRibbonBarButtonGroup6
			' 
			Me.radRibbonBarButtonGroup6.AccessibleDescription = "radRibbonBarButtonGroup6"
			Me.radRibbonBarButtonGroup6.AccessibleName = "radRibbonBarButtonGroup6"
			Me.radRibbonBarButtonGroup6.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnHighlight, Me.radBtnFontColor})
			Me.radRibbonBarButtonGroup6.Margin = New System.Windows.Forms.Padding(5, 0, 0, 0)
			Me.radRibbonBarButtonGroup6.Name = "radRibbonBarButtonGroup6"
			Me.radRibbonBarButtonGroup6.Padding = New System.Windows.Forms.Padding(1)
			Me.radRibbonBarButtonGroup6.ShowBackColor = False
			Me.radRibbonBarButtonGroup6.ShowBorder = False
			Me.radRibbonBarButtonGroup6.Text = "radRibbonBarButtonGroup6"
			Me.radRibbonBarButtonGroup6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnHighlight
			' 
			Me.radBtnHighlight.AccessibleDescription = "radButtonElement6"
			Me.radBtnHighlight.AccessibleName = "radButtonElement6"
			Me.radBtnHighlight.[Class] = "RibbonBarButtonElement"
			Me.radBtnHighlight.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnHighlight.Image = Global.TelerikEditor.Properties.Resources.highlight
			Me.radBtnHighlight.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnHighlight.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnHighlight.Name = "radBtnHighlight"
			Me.radBtnHighlight.ShowBorder = False
			Me.radBtnHighlight.Text = "radButtonElement6"
			Me.radBtnHighlight.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnHighlight.Click, New System.EventHandler(AddressOf(Me.radBtnHighlight_Click))
			' 
			' radBtnFontColor
			' 
			Me.radBtnFontColor.AccessibleDescription = "radButtonElement7"
			Me.radBtnFontColor.AccessibleName = "radButtonElement7"
			Me.radBtnFontColor.[Class] = "RibbonBarButtonElement"
			Me.radBtnFontColor.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnFontColor.Image = Global.TelerikEditor.Properties.Resources.fontcolor
			Me.radBtnFontColor.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnFontColor.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnFontColor.Name = "radBtnFontColor"
			Me.radBtnFontColor.ShowBorder = False
			Me.radBtnFontColor.Text = "radButtonElement7"
			Me.radBtnFontColor.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnFontColor.Click, New System.EventHandler(AddressOf(Me.radBtnFontColor_Click))
			' 
			' radRibbonBarButtonGroup11
			' 
			Me.radRibbonBarButtonGroup11.AccessibleDescription = "radRibbonBarButtonGroup11"
			Me.radRibbonBarButtonGroup11.AccessibleName = "radRibbonBarButtonGroup11"
			Me.radRibbonBarButtonGroup11.Items.AddRange(New Telerik.WinControls.RadItem() {Me.subscriptButtonElement, Me.superscriptButtonElement})
			Me.radRibbonBarButtonGroup11.Name = "radRibbonBarButtonGroup11"
			Me.radRibbonBarButtonGroup11.Text = "radRibbonBarButtonGroup11"
			Me.radRibbonBarButtonGroup11.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' subscriptButtonElement
			' 
			Me.subscriptButtonElement.AccessibleDescription = "subscriptButtonElement"
			Me.subscriptButtonElement.AccessibleName = "subscriptButtonElement"
			Me.subscriptButtonElement.[Class] = ""
			Me.subscriptButtonElement.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.subscriptButtonElement.Image = Global.TelerikEditor.Properties.Resources.subscript
			Me.subscriptButtonElement.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.subscriptButtonElement.MinSize = New System.Drawing.Size(20, 20)
			Me.subscriptButtonElement.Name = "subscriptButtonElement"
			Me.subscriptButtonElement.Text = "subscriptButtonElement"
			Me.subscriptButtonElement.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.subscriptButtonElement.ToggleStateChanged, New Telerik.WinControls.UI.StateChangedEventHandler(AddressOf(subscriptButtonElement_ToggleStateChanged))
			' 
			' superscriptButtonElement
			' 
			Me.superscriptButtonElement.AccessibleDescription = "superscriptButtonElement"
			Me.superscriptButtonElement.AccessibleName = "superscriptButtonElement"
			Me.superscriptButtonElement.[Class] = ""
			Me.superscriptButtonElement.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.superscriptButtonElement.Image = Global.TelerikEditor.Properties.Resources.superscript
			Me.superscriptButtonElement.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.superscriptButtonElement.MinSize = New System.Drawing.Size(20, 20)
			Me.superscriptButtonElement.Name = "superscriptButtonElement"
			Me.superscriptButtonElement.Text = "superscriptButtonElement"
			Me.superscriptButtonElement.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.superscriptButtonElement.ToggleStateChanged, New Telerik.WinControls.UI.StateChangedEventHandler(AddressOf(superscriptButtonElement_ToggleStateChanged))
			' 
			' radRibbonBarGroup8
			' 
			Me.radRibbonBarGroup8.AccessibleDescription = "Font"
			Me.radRibbonBarGroup8.AccessibleName = "Font"
			Me.radRibbonBarGroup8.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radRibbonBarButtonGroup7, Me.radRibbonBarButtonGroup8})
			Me.radRibbonBarGroup8.Margin = New System.Windows.Forms.Padding(-1, -1, 0, -1)
			Me.radRibbonBarGroup8.Name = "radRibbonBarGroup8"
			Me.radRibbonBarGroup8.Orientation = System.Windows.Forms.Orientation.Vertical
			Me.radRibbonBarGroup8.Text = "Paragraph"
			Me.radRibbonBarGroup8.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radRibbonBarButtonGroup7
			' 
			Me.radRibbonBarButtonGroup7.AccessibleDescription = "radRibbonBarButtonGroup7"
			Me.radRibbonBarButtonGroup7.AccessibleName = "radRibbonBarButtonGroup7"
			Me.radRibbonBarButtonGroup7.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radRibbonBarButtonGroup9, Me.radRibbonBarButtonGroup10, Me.radRibbonBarButtonGroup14})
			Me.radRibbonBarButtonGroup7.Name = "radRibbonBarButtonGroup7"
			Me.radRibbonBarButtonGroup7.Padding = New System.Windows.Forms.Padding(1)
			Me.radRibbonBarButtonGroup7.ShowBackColor = False
			Me.radRibbonBarButtonGroup7.ShowBorder = False
			Me.radRibbonBarButtonGroup7.Text = "radRibbonBarButtonGroup7"
			Me.radRibbonBarButtonGroup7.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radRibbonBarButtonGroup9
			' 
			Me.radRibbonBarButtonGroup9.AccessibleDescription = "radRibbonBarButtonGroup9"
			Me.radRibbonBarButtonGroup9.AccessibleName = "radRibbonBarButtonGroup9"
			Me.radRibbonBarButtonGroup9.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnBulletList, Me.radNumberingList})
			Me.radRibbonBarButtonGroup9.Name = "radRibbonBarButtonGroup9"
			Me.radRibbonBarButtonGroup9.Padding = New System.Windows.Forms.Padding(1)
			Me.radRibbonBarButtonGroup9.ShowBackColor = False
			Me.radRibbonBarButtonGroup9.ShowBorder = False
			Me.radRibbonBarButtonGroup9.Text = "radRibbonBarButtonGroup9"
			Me.radRibbonBarButtonGroup9.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnBulletList
			' 
			Me.radBtnBulletList.AccessibleDescription = "radButtonElement6"
			Me.radBtnBulletList.AccessibleName = "radButtonElement6"
			Me.radBtnBulletList.[Class] = ""
			Me.radBtnBulletList.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnBulletList.Image = Global.TelerikEditor.Properties.Resources.list_bullets
			Me.radBtnBulletList.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnBulletList.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnBulletList.Name = "radBtnBulletList"
			Me.radBtnBulletList.Text = "radButtonElement6"
			Me.radBtnBulletList.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnBulletList.ToggleStateChanged, New Telerik.WinControls.UI.StateChangedEventHandler(AddressOf(Me.radBtnBulletList_ToggleStateChanged))
			' 
			' radNumberingList
			' 
			Me.radNumberingList.AccessibleDescription = "radButtonElement7"
			Me.radNumberingList.AccessibleName = "radButtonElement7"
			Me.radNumberingList.[Class] = ""
			Me.radNumberingList.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radNumberingList.Image = Global.TelerikEditor.Properties.Resources.list_numbered
			Me.radNumberingList.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radNumberingList.MinSize = New System.Drawing.Size(20, 20)
			Me.radNumberingList.Name = "radNumberingList"
			Me.radNumberingList.Text = ""
			Me.radNumberingList.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radNumberingList.ToggleStateChanged, New Telerik.WinControls.UI.StateChangedEventHandler(AddressOf(Me.radNumberingList_ToggleStateChanged))
			' 
			' radRibbonBarButtonGroup10
			' 
			Me.radRibbonBarButtonGroup10.AccessibleDescription = "radRibbonBarButtonGroup10"
			Me.radRibbonBarButtonGroup10.AccessibleName = "radRibbonBarButtonGroup10"
			Me.radRibbonBarButtonGroup10.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnDecreaseIndent, Me.radBtnIncreaseIndent})
			Me.radRibbonBarButtonGroup10.Margin = New System.Windows.Forms.Padding(5, 0, 0, 0)
			Me.radRibbonBarButtonGroup10.Name = "radRibbonBarButtonGroup10"
			Me.radRibbonBarButtonGroup10.Padding = New System.Windows.Forms.Padding(1)
			Me.radRibbonBarButtonGroup10.ShowBackColor = False
			Me.radRibbonBarButtonGroup10.ShowBorder = False
			Me.radRibbonBarButtonGroup10.Text = "radRibbonBarButtonGroup10"
			Me.radRibbonBarButtonGroup10.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnDecreaseIndent
			' 
			Me.radBtnDecreaseIndent.AccessibleDescription = "radButtonElement7"
			Me.radBtnDecreaseIndent.AccessibleName = "radButtonElement7"
			Me.radBtnDecreaseIndent.[Class] = "RibbonBarButtonElement"
			Me.radBtnDecreaseIndent.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnDecreaseIndent.Image = Global.TelerikEditor.Properties.Resources.outdent
			Me.radBtnDecreaseIndent.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnDecreaseIndent.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnDecreaseIndent.Name = "radBtnDecreaseIndent"
			Me.radBtnDecreaseIndent.ShowBorder = False
			Me.radBtnDecreaseIndent.Text = "radButtonElement7"
			Me.radBtnDecreaseIndent.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnDecreaseIndent.Click, New System.EventHandler(AddressOf(Me.radBtnDecreaseIndent_Click))
			' 
			' radBtnIncreaseIndent
			' 
			Me.radBtnIncreaseIndent.AccessibleDescription = "radButtonElement6"
			Me.radBtnIncreaseIndent.AccessibleName = "radButtonElement6"
			Me.radBtnIncreaseIndent.[Class] = "RibbonBarButtonElement"
			Me.radBtnIncreaseIndent.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnIncreaseIndent.Image = Global.TelerikEditor.Properties.Resources.indent
			Me.radBtnIncreaseIndent.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnIncreaseIndent.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnIncreaseIndent.Name = "radBtnIncreaseIndent"
			Me.radBtnIncreaseIndent.ShowBorder = False
			Me.radBtnIncreaseIndent.Text = "radButtonElement6"
			Me.radBtnIncreaseIndent.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnIncreaseIndent.Click, New System.EventHandler(AddressOf(Me.radBtnIncreaseIndent_Click))
			' 
			' radRibbonBarButtonGroup14
			' 
			Me.radRibbonBarButtonGroup14.AccessibleDescription = "radRibbonBarButtonGroup14"
			Me.radRibbonBarButtonGroup14.AccessibleName = "radRibbonBarButtonGroup14"
			Me.radRibbonBarButtonGroup14.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnShowFormatting})
			Me.radRibbonBarButtonGroup14.Margin = New System.Windows.Forms.Padding(5, 0, 0, 0)
			Me.radRibbonBarButtonGroup14.Name = "radRibbonBarButtonGroup14"
			Me.radRibbonBarButtonGroup14.Padding = New System.Windows.Forms.Padding(1)
			Me.radRibbonBarButtonGroup14.ShowBorder = False
			Me.radRibbonBarButtonGroup14.Text = "radRibbonBarButtonGroup14"
			Me.radRibbonBarButtonGroup14.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnShowFormatting
			' 
			Me.radBtnShowFormatting.AccessibleDescription = "radToggleButtonElement1"
			Me.radBtnShowFormatting.AccessibleName = "radToggleButtonElement1"
			Me.radBtnShowFormatting.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnShowFormatting.Image = Global.TelerikEditor.Properties.Resources.formatting_symbols
			Me.radBtnShowFormatting.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnShowFormatting.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnShowFormatting.Name = "radBtnShowFormatting"
			Me.radBtnShowFormatting.Text = "radToggleButtonElement1"
			Me.radBtnShowFormatting.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnShowFormatting.Click, New System.EventHandler(AddressOf(Me.radBtnShowFormatting_Click))
			' 
			' radRibbonBarButtonGroup8
			' 
			Me.radRibbonBarButtonGroup8.AccessibleDescription = "radRibbonBarButtonGroup8"
			Me.radRibbonBarButtonGroup8.AccessibleName = "radRibbonBarButtonGroup8"
			Me.radRibbonBarButtonGroup8.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radRibbonBarButtonGroup12, Me.radRibbonBarButtonGroup13})
			Me.radRibbonBarButtonGroup8.Margin = New System.Windows.Forms.Padding(0, 5, 0, 0)
			Me.radRibbonBarButtonGroup8.Name = "radRibbonBarButtonGroup8"
			Me.radRibbonBarButtonGroup8.Padding = New System.Windows.Forms.Padding(1)
			Me.radRibbonBarButtonGroup8.ShowBackColor = False
			Me.radRibbonBarButtonGroup8.ShowBorder = False
			Me.radRibbonBarButtonGroup8.Text = "radRibbonBarButtonGroup8"
			Me.radRibbonBarButtonGroup8.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radRibbonBarButtonGroup12
			' 
			Me.radRibbonBarButtonGroup12.AccessibleDescription = "radRibbonBarButtonGroup12"
			Me.radRibbonBarButtonGroup12.AccessibleName = "radRibbonBarButtonGroup12"
			Me.radRibbonBarButtonGroup12.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnAligntLeft, Me.radBtnAlignCenter, Me.radBtnAligntRight, Me.radBtnAlignJustify})
			Me.radRibbonBarButtonGroup12.Margin = New System.Windows.Forms.Padding(0)
			Me.radRibbonBarButtonGroup12.Name = "radRibbonBarButtonGroup12"
			Me.radRibbonBarButtonGroup12.Padding = New System.Windows.Forms.Padding(1)
			Me.radRibbonBarButtonGroup12.ShowBackColor = False
			Me.radRibbonBarButtonGroup12.ShowBorder = False
			Me.radRibbonBarButtonGroup12.Text = "radRibbonBarButtonGroup12"
			Me.radRibbonBarButtonGroup12.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnAligntLeft
			' 
			Me.radBtnAligntLeft.AccessibleDescription = "radButtonElement6"
			Me.radBtnAligntLeft.AccessibleName = "radButtonElement6"
			Me.radBtnAligntLeft.[Class] = ""
			Me.radBtnAligntLeft.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnAligntLeft.Image = Global.TelerikEditor.Properties.Resources.alignleft
			Me.radBtnAligntLeft.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnAligntLeft.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnAligntLeft.Name = "radBtnAligntLeft"
			Me.radBtnAligntLeft.Text = ""
			Me.radBtnAligntLeft.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnAligntLeft.ToggleStateChanged, New Telerik.WinControls.UI.StateChangedEventHandler(AddressOf(Me.radBtnAligntLeft_ToggleStateChanged))
			' 
			' radBtnAlignCenter
			' 
			Me.radBtnAlignCenter.AccessibleDescription = "radButtonElement7"
			Me.radBtnAlignCenter.AccessibleName = "radButtonElement7"
			Me.radBtnAlignCenter.[Class] = ""
			Me.radBtnAlignCenter.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnAlignCenter.Image = Global.TelerikEditor.Properties.Resources.aligncenter
			Me.radBtnAlignCenter.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnAlignCenter.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnAlignCenter.Name = "radBtnAlignCenter"
			Me.radBtnAlignCenter.Text = ""
			Me.radBtnAlignCenter.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnAlignCenter.ToggleStateChanged, New Telerik.WinControls.UI.StateChangedEventHandler(AddressOf(Me.radBtnAlignCenter_ToggleStateChanged))
			' 
			' radBtnAligntRight
			' 
			Me.radBtnAligntRight.AccessibleDescription = "radButtonElement8"
			Me.radBtnAligntRight.AccessibleName = "radButtonElement8"
			Me.radBtnAligntRight.[Class] = ""
			Me.radBtnAligntRight.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnAligntRight.Image = Global.TelerikEditor.Properties.Resources.alignright
			Me.radBtnAligntRight.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnAligntRight.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnAligntRight.Name = "radBtnAligntRight"
			Me.radBtnAligntRight.Text = ""
			Me.radBtnAligntRight.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnAligntRight.ToggleStateChanged, New Telerik.WinControls.UI.StateChangedEventHandler(AddressOf(Me.radBtnAligntRight_ToggleStateChanged))
			' 
			' radBtnAlignJustify
			' 
			Me.radBtnAlignJustify.AccessibleDescription = "radButtonElement9"
			Me.radBtnAlignJustify.AccessibleName = "radButtonElement9"
			Me.radBtnAlignJustify.[Class] = ""
			Me.radBtnAlignJustify.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnAlignJustify.Image = Global.TelerikEditor.Properties.Resources.alignjustify
			Me.radBtnAlignJustify.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnAlignJustify.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnAlignJustify.Name = "radBtnAlignJustify"
			Me.radBtnAlignJustify.Text = ""
			Me.radBtnAlignJustify.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnAlignJustify.ToggleStateChanged, New Telerik.WinControls.UI.StateChangedEventHandler(AddressOf(Me.radBtnAlignJustify_ToggleStateChanged))
			' 
			' radRibbonBarButtonGroup13
			' 
			Me.radRibbonBarButtonGroup13.AccessibleDescription = "radRibbonBarButtonGroup13"
			Me.radRibbonBarButtonGroup13.AccessibleName = "radRibbonBarButtonGroup13"
			Me.radRibbonBarButtonGroup13.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnBackColor})
			Me.radRibbonBarButtonGroup13.Margin = New System.Windows.Forms.Padding(5, 0, 0, 0)
			Me.radRibbonBarButtonGroup13.Name = "radRibbonBarButtonGroup13"
			Me.radRibbonBarButtonGroup13.Padding = New System.Windows.Forms.Padding(1)
			Me.radRibbonBarButtonGroup13.ShowBackColor = False
			Me.radRibbonBarButtonGroup13.ShowBorder = False
			Me.radRibbonBarButtonGroup13.Text = "radRibbonBarButtonGroup13"
			Me.radRibbonBarButtonGroup13.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnBackColor
			' 
			Me.radBtnBackColor.AccessibleDescription = "radButtonElement6"
			Me.radBtnBackColor.AccessibleName = "radButtonElement6"
			Me.radBtnBackColor.[Class] = "RibbonBarButtonElement"
			Me.radBtnBackColor.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnBackColor.Image = Global.TelerikEditor.Properties.Resources.ParagraphBackgroundColor
			Me.radBtnBackColor.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnBackColor.MinSize = New System.Drawing.Size(20, 20)
			Me.radBtnBackColor.Name = "radBtnBackColor"
			Me.radBtnBackColor.ShowBorder = False
			Me.radBtnBackColor.Text = "radButtonElement6"
			Me.radBtnBackColor.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnBackColor.Click, New System.EventHandler(AddressOf(Me.radBtnBackColor_Click))
			' 
			' radRibbonBarGroup10
			' 
			Me.radRibbonBarGroup10.AccessibleDescription = "Editing & Proofing"
			Me.radRibbonBarGroup10.AccessibleName = "Editing & Proofing"
			Me.radRibbonBarGroup10.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnEnableSpellCheck})
			Me.radRibbonBarGroup10.Margin = New System.Windows.Forms.Padding(-1, -1, 0, -1)
			Me.radRibbonBarGroup10.Name = "radRibbonBarGroup10"
			Me.radRibbonBarGroup10.Orientation = System.Windows.Forms.Orientation.Vertical
			Me.radRibbonBarGroup10.Text = "Editing && Proofing"
			Me.radRibbonBarGroup10.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnEnableSpellCheck
			' 
			Me.radBtnEnableSpellCheck.AccessibleDescription = "Enable Spell Check"
			Me.radBtnEnableSpellCheck.AccessibleName = "Enable Spell Check"
			Me.radBtnEnableSpellCheck.Image = (CType((resources.GetObject("radBtnEnableSpellCheck.Image")), System.Drawing.Image))
			Me.radBtnEnableSpellCheck.Name = "radBtnEnableSpellCheck"
			Me.radBtnEnableSpellCheck.Text = "Enable Spell Check"
			Me.radBtnEnableSpellCheck.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
			Me.radBtnEnableSpellCheck.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnEnableSpellCheck.Click, New System.EventHandler(AddressOf(Me.radBtnEnableSpellCheck_Click))
			' 
			' ribbonTab2
			' 
			Me.ribbonTab2.AccessibleDescription = "Insert"
			Me.ribbonTab2.AccessibleName = "Insert"
			Me.ribbonTab2.[Class] = "RibbonTab"
			Me.ribbonTab2.IsSelected = False
			Me.ribbonTab2.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radRibbonBarGroup1, Me.radRibbonBarGroup2, Me.radRibbonBarGroup11, Me.radRibbonBarGroup3})
			Me.ribbonTab2.Name = "ribbonTab2"
			Me.ribbonTab2.Text = "Insert"
			Me.ribbonTab2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radRibbonBarGroup1
			' 
			Me.radRibbonBarGroup1.AccessibleDescription = "Pages"
			Me.radRibbonBarGroup1.AccessibleName = "Pages"
			Me.radRibbonBarGroup1.Alignment = System.Drawing.ContentAlignment.TopLeft
			Me.radRibbonBarGroup1.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnPageBreak})
			Me.radRibbonBarGroup1.Margin = New System.Windows.Forms.Padding(-1, -1, 0, -1)
			Me.radRibbonBarGroup1.Name = "radRibbonBarGroup1"
			Me.radRibbonBarGroup1.Text = "Pages"
			Me.radRibbonBarGroup1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnPageBreak
			' 
			Me.radBtnPageBreak.AccessibleDescription = "Page Break"
			Me.radBtnPageBreak.AccessibleName = "Page Break"
			Me.radBtnPageBreak.[Class] = "RibbonBarButtonElement"
			Me.radBtnPageBreak.Image = (CType((resources.GetObject("radBtnPageBreak.Image")), System.Drawing.Image))
			Me.radBtnPageBreak.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnPageBreak.ImageKey = "PageBreak"
			Me.radBtnPageBreak.Name = "radBtnPageBreak"
			Me.radBtnPageBreak.Text = "Page Break"
			Me.radBtnPageBreak.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnPageBreak.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
			Me.radBtnPageBreak.TextOrientation = System.Windows.Forms.Orientation.Horizontal
			Me.radBtnPageBreak.TextWrap = True
			Me.radBtnPageBreak.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnPageBreak.Click, New System.EventHandler(AddressOf(Me.radBtnPageBreak_Click))
			' 
			' radRibbonBarGroup2
			' 
			Me.radRibbonBarGroup2.AccessibleDescription = "Illustrations"
			Me.radRibbonBarGroup2.AccessibleName = "Illustrations"
			Me.radRibbonBarGroup2.Alignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radRibbonBarGroup2.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnInsertPicture})
			Me.radRibbonBarGroup2.Margin = New System.Windows.Forms.Padding(-1, -1, 0, -1)
			Me.radRibbonBarGroup2.Name = "radRibbonBarGroup2"
			Me.radRibbonBarGroup2.Text = "Illustrations"
			Me.radRibbonBarGroup2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnInsertPicture
			' 
			Me.radBtnInsertPicture.AccessibleDescription = "Picture"
			Me.radBtnInsertPicture.AccessibleName = "Picture"
			Me.radBtnInsertPicture.Alignment = System.Drawing.ContentAlignment.TopLeft
			Me.radBtnInsertPicture.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
			Me.radBtnInsertPicture.[Class] = "RibbonBarButtonElement"
			Me.radBtnInsertPicture.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent
			Me.radBtnInsertPicture.Image = (CType((resources.GetObject("radBtnInsertPicture.Image")), System.Drawing.Image))
			Me.radBtnInsertPicture.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnInsertPicture.ImageKey = "Picture"
			Me.radBtnInsertPicture.Name = "radBtnInsertPicture"
			Me.radBtnInsertPicture.Text = "Picture"
			Me.radBtnInsertPicture.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
			Me.radBtnInsertPicture.TextWrap = True
			Me.radBtnInsertPicture.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnInsertPicture.Click, New System.EventHandler(AddressOf(Me.radBtnInsertPicture_Click))
			' 
			' radRibbonBarGroup11
			' 
			Me.radRibbonBarGroup11.AccessibleDescription = "Tables"
			Me.radRibbonBarGroup11.AccessibleName = "Tables"
			Me.radRibbonBarGroup11.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radDropDownButtonElementTables})
			Me.radRibbonBarGroup11.Name = "radRibbonBarGroup11"
			Me.radRibbonBarGroup11.Text = "Tables"
			Me.radRibbonBarGroup11.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radDropDownButtonElementTables
			' 
			Me.radDropDownButtonElementTables.AccessibleDescription = "Table"
			Me.radDropDownButtonElementTables.AccessibleName = "Table"
			Me.radDropDownButtonElementTables.ArrowButtonMinSize = New System.Drawing.Size(12, 12)
			Me.radDropDownButtonElementTables.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down
			Me.radDropDownButtonElementTables.ExpandArrowButton = False
			Me.radDropDownButtonElementTables.Image = (CType((resources.GetObject("radDropDownButtonElementTables.Image")), System.Drawing.Image))
			Me.radDropDownButtonElementTables.ImageKey = "Table.png"
			Me.radDropDownButtonElementTables.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radMenuItemInsertTable})
			Me.radDropDownButtonElementTables.Name = "radDropDownButtonElementTables"
			Me.radDropDownButtonElementTables.Text = "Table"
			Me.radDropDownButtonElementTables.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
			Me.radDropDownButtonElementTables.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItemInsertTable
			' 
			Me.radMenuItemInsertTable.AccessibleDescription = "Insert Table..."
			Me.radMenuItemInsertTable.AccessibleName = "Insert Table..."
			Me.radMenuItemInsertTable.Image = Global.TelerikEditor.Properties.Resources.Table_16
			Me.radMenuItemInsertTable.Name = "radMenuItemInsertTable"
			Me.radMenuItemInsertTable.Text = "Insert Table..."
			Me.radMenuItemInsertTable.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radRibbonBarGroup3
			' 
			Me.radRibbonBarGroup3.AccessibleDescription = "Links"
			Me.radRibbonBarGroup3.AccessibleName = "Links"
			Me.radRibbonBarGroup3.Items.AddRange(New Telerik.WinControls.RadItem() {Me.hyperlinkButtonElement, Me.bookmarkButtonElement})
			Me.radRibbonBarGroup3.Name = "radRibbonBarGroup3"
			Me.radRibbonBarGroup3.Text = "Links"
			Me.radRibbonBarGroup3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' hyperlinkButtonElement
			' 
			Me.hyperlinkButtonElement.AccessibleDescription = "Hyperlink"
			Me.hyperlinkButtonElement.AccessibleName = "Hyperlink"
			Me.hyperlinkButtonElement.[Class] = "RibbonBarButtonElement"
			Me.hyperlinkButtonElement.Image = Global.TelerikEditor.Properties.Resources.Html
			Me.hyperlinkButtonElement.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.hyperlinkButtonElement.Name = "hyperlinkButtonElement"
			Me.hyperlinkButtonElement.Text = "Hyperlink"
			Me.hyperlinkButtonElement.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
			Me.hyperlinkButtonElement.TextWrap = True
			Me.hyperlinkButtonElement.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.hyperlinkButtonElement.Click, New System.EventHandler(AddressOf(Me.hyperlinkButtonElement_Click))
			' 
			' bookmarkButtonElement
			' 
			Me.bookmarkButtonElement.AccessibleDescription = "Bookmark"
			Me.bookmarkButtonElement.AccessibleName = "Bookmark"
			Me.bookmarkButtonElement.[Class] = "RibbonBarButtonElement"
			Me.bookmarkButtonElement.Image = Global.TelerikEditor.Properties.Resources.PageOrientationRotate180
			Me.bookmarkButtonElement.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.bookmarkButtonElement.Name = "bookmarkButtonElement"
			Me.bookmarkButtonElement.Text = "Bookmark"
			Me.bookmarkButtonElement.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
			Me.bookmarkButtonElement.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.bookmarkButtonElement.Click, New System.EventHandler(AddressOf(Me.bookmarkButtonElement_Click))
			' 
			' ribbonTab3
			' 
			Me.ribbonTab3.AccessibleDescription = "Page Layout"
			Me.ribbonTab3.AccessibleName = "Page Layout"
			Me.ribbonTab3.[Class] = "RibbonTab"
			Me.ribbonTab3.IsSelected = False
			Me.ribbonTab3.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radRibbonBarGroup4})
			Me.ribbonTab3.Name = "ribbonTab3"
			Me.ribbonTab3.Text = "Page Layout"
			Me.ribbonTab3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radRibbonBarGroup4
			' 
			Me.radRibbonBarGroup4.AccessibleDescription = "Page Setup"
			Me.radRibbonBarGroup4.AccessibleName = "Page Setup"
			Me.radRibbonBarGroup4.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radDropDownButtonSize, Me.radDropDownButtonPageMargins, Me.radDropDownButtonPageOrientation})
			Me.radRibbonBarGroup4.Margin = New System.Windows.Forms.Padding(-1, -1, 0, -1)
			Me.radRibbonBarGroup4.Name = "radRibbonBarGroup4"
			Me.radRibbonBarGroup4.Text = "Page Setup"
			Me.radRibbonBarGroup4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radDropDownButtonSize
			' 
			Me.radDropDownButtonSize.AccessibleDescription = "Size"
			Me.radDropDownButtonSize.AccessibleName = "Size"
			Me.radDropDownButtonSize.ArrowButtonMinSize = New System.Drawing.Size(12, 12)
			Me.radDropDownButtonSize.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down
			Me.radDropDownButtonSize.ExpandArrowButton = False
			Me.radDropDownButtonSize.Image = (CType((resources.GetObject("radDropDownButtonSize.Image")), System.Drawing.Image))
			Me.radDropDownButtonSize.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radDropDownButtonSize.ImageKey = "PageSize"
			Me.radDropDownButtonSize.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radMenuItemSizeA0, Me.radMenuItemSizeA1, Me.radMenuItemSizeA2, Me.radMenuItemSizeA3, Me.radMenuItemSizeA4})
			Me.radDropDownButtonSize.Name = "radDropDownButtonSize"
			Me.radDropDownButtonSize.Text = "Size"
			Me.radDropDownButtonSize.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
			Me.radDropDownButtonSize.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItemSizeA0
			' 
			Me.radMenuItemSizeA0.AccessibleDescription = "A0"
			Me.radMenuItemSizeA0.AccessibleName = "A0"
			Me.radMenuItemSizeA0.Name = "radMenuItemSizeA0"
			Me.radMenuItemSizeA0.Text = "A0"
			Me.radMenuItemSizeA0.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemSizeA0.Click, New System.EventHandler(AddressOf(Me.radMenuItemSizeA0_Click))
			' 
			' radMenuItemSizeA1
			' 
			Me.radMenuItemSizeA1.AccessibleDescription = "A1"
			Me.radMenuItemSizeA1.AccessibleName = "A1"
			Me.radMenuItemSizeA1.Name = "radMenuItemSizeA1"
			Me.radMenuItemSizeA1.Text = "A1"
			Me.radMenuItemSizeA1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemSizeA1.Click, New System.EventHandler(AddressOf(Me.radMenuItemSizeA1_Click))
			' 
			' radMenuItemSizeA2
			' 
			Me.radMenuItemSizeA2.AccessibleDescription = "A2"
			Me.radMenuItemSizeA2.AccessibleName = "A2"
			Me.radMenuItemSizeA2.Name = "radMenuItemSizeA2"
			Me.radMenuItemSizeA2.Text = "A2"
			Me.radMenuItemSizeA2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemSizeA2.Click, New System.EventHandler(AddressOf(Me.radMenuItemSizeA2_Click))
			' 
			' radMenuItemSizeA3
			' 
			Me.radMenuItemSizeA3.AccessibleDescription = "A3"
			Me.radMenuItemSizeA3.AccessibleName = "A3"
			Me.radMenuItemSizeA3.Name = "radMenuItemSizeA3"
			Me.radMenuItemSizeA3.Text = "A3"
			Me.radMenuItemSizeA3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemSizeA3.Click, New System.EventHandler(AddressOf(Me.radMenuItemSizeA3_Click))
			' 
			' radMenuItemSizeA4
			' 
			Me.radMenuItemSizeA4.AccessibleDescription = "A4"
			Me.radMenuItemSizeA4.AccessibleName = "A4"
			Me.radMenuItemSizeA4.Name = "radMenuItemSizeA4"
			Me.radMenuItemSizeA4.Text = "A4"
			Me.radMenuItemSizeA4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemSizeA4.Click, New System.EventHandler(AddressOf(Me.radMenuItemSizeA4_Click))
			' 
			' radDropDownButtonPageMargins
			' 
			Me.radDropDownButtonPageMargins.AccessibleDescription = "Margins"
			Me.radDropDownButtonPageMargins.AccessibleName = "Margins"
			Me.radDropDownButtonPageMargins.ArrowButtonMinSize = New System.Drawing.Size(12, 12)
			Me.radDropDownButtonPageMargins.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down
			Me.radDropDownButtonPageMargins.ExpandArrowButton = False
			Me.radDropDownButtonPageMargins.Image = (CType((resources.GetObject("radDropDownButtonPageMargins.Image")), System.Drawing.Image))
			Me.radDropDownButtonPageMargins.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radDropDownButtonPageMargins.ImageKey = "PageMargin"
			Me.radDropDownButtonPageMargins.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radMenuItemNormal, Me.radMenuItemNarrow, Me.radMenuItemModerate, Me.radMenuItemOffice2003})
			Me.radDropDownButtonPageMargins.Name = "radDropDownButtonPageMargins"
			Me.radDropDownButtonPageMargins.Text = "Margins"
			Me.radDropDownButtonPageMargins.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
			Me.radDropDownButtonPageMargins.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItemNormal
			' 
			Me.radMenuItemNormal.AccessibleDescription = "Normal"
			Me.radMenuItemNormal.AccessibleName = "Normal"
			Me.radMenuItemNormal.Image = Global.TelerikEditor.Properties.Resources.PageMarginNormal
			Me.radMenuItemNormal.Name = "radMenuItemNormal"
			Me.radMenuItemNormal.Text = "Normal"
			Me.radMenuItemNormal.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemNormal.Click, New System.EventHandler(AddressOf(Me.radMenuItemNormal_Click))
			' 
			' radMenuItemNarrow
			' 
			Me.radMenuItemNarrow.AccessibleDescription = "Narrow"
			Me.radMenuItemNarrow.AccessibleName = "Narrow"
			Me.radMenuItemNarrow.Image = Global.TelerikEditor.Properties.Resources.PageMarginNarrow
			Me.radMenuItemNarrow.Name = "radMenuItemNarrow"
			Me.radMenuItemNarrow.Text = "Narrow"
			Me.radMenuItemNarrow.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemNarrow.Click, New System.EventHandler(AddressOf(Me.radMenuItemNarrow_Click))
			' 
			' radMenuItemModerate
			' 
			Me.radMenuItemModerate.AccessibleDescription = "Modrate"
			Me.radMenuItemModerate.AccessibleName = "Modrate"
			Me.radMenuItemModerate.Image = Global.TelerikEditor.Properties.Resources.PageMarginModerate
			Me.radMenuItemModerate.Name = "radMenuItemModerate"
			Me.radMenuItemModerate.Text = "Modrate"
			Me.radMenuItemModerate.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemModerate.Click, New System.EventHandler(AddressOf(Me.radMenuItemModerate_Click))
			' 
			' radMenuItemOffice2003
			' 
			Me.radMenuItemOffice2003.AccessibleDescription = "Office 2003"
			Me.radMenuItemOffice2003.AccessibleName = "Office 2003"
			Me.radMenuItemOffice2003.Image = Global.TelerikEditor.Properties.Resources.PageMarginNormal
			Me.radMenuItemOffice2003.Name = "radMenuItemOffice2003"
			Me.radMenuItemOffice2003.Text = "Office 2003"
			Me.radMenuItemOffice2003.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemOffice2003.Click, New System.EventHandler(AddressOf(Me.radMenuItemOffice2003_Click))
			' 
			' radDropDownButtonPageOrientation
			' 
			Me.radDropDownButtonPageOrientation.AccessibleDescription = "Orientation"
			Me.radDropDownButtonPageOrientation.AccessibleName = "Orientation"
			Me.radDropDownButtonPageOrientation.ArrowButtonMinSize = New System.Drawing.Size(12, 12)
			Me.radDropDownButtonPageOrientation.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down
			Me.radDropDownButtonPageOrientation.ExpandArrowButton = False
			Me.radDropDownButtonPageOrientation.Image = (CType((resources.GetObject("radDropDownButtonPageOrientation.Image")), System.Drawing.Image))
			Me.radDropDownButtonPageOrientation.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radDropDownButtonPageOrientation.ImageKey = "PageOrientation"
			Me.radDropDownButtonPageOrientation.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radMenuItemLandscape, Me.radMenuItemPortrait, Me.radMenuItemRotate180, Me.radMenuItemRotate270})
			Me.radDropDownButtonPageOrientation.Name = "radDropDownButtonPageOrientation"
			Me.radDropDownButtonPageOrientation.Text = "Orientation"
			Me.radDropDownButtonPageOrientation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
			Me.radDropDownButtonPageOrientation.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItemLandscape
			' 
			Me.radMenuItemLandscape.AccessibleDescription = "Landscape"
			Me.radMenuItemLandscape.AccessibleName = "Landscape"
			Me.radMenuItemLandscape.Image = Global.TelerikEditor.Properties.Resources.PageOrientationLandscape
			Me.radMenuItemLandscape.Name = "radMenuItemLandscape"
			Me.radMenuItemLandscape.Text = "Landscape"
			Me.radMenuItemLandscape.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemLandscape.Click, New System.EventHandler(AddressOf(Me.radMenuItemLandscape_Click))
			' 
			' radMenuItemPortrait
			' 
			Me.radMenuItemPortrait.AccessibleDescription = "Portrait"
			Me.radMenuItemPortrait.AccessibleName = "Portrait"
			Me.radMenuItemPortrait.Image = Global.TelerikEditor.Properties.Resources.PageOrientationPortrait
			Me.radMenuItemPortrait.Name = "radMenuItemPortrait"
			Me.radMenuItemPortrait.Text = "Portrait"
			Me.radMenuItemPortrait.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemPortrait.Click, New System.EventHandler(AddressOf(Me.radMenuItemPortrait_Click))
			' 
			' radMenuItemRotate180
			' 
			Me.radMenuItemRotate180.AccessibleDescription = "Rotate 180"
			Me.radMenuItemRotate180.AccessibleName = "Rotate 180"
			Me.radMenuItemRotate180.Image = Global.TelerikEditor.Properties.Resources.PageOrientationRotate180
			Me.radMenuItemRotate180.Name = "radMenuItemRotate180"
			Me.radMenuItemRotate180.Text = "Rotate 180"
			Me.radMenuItemRotate180.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemRotate180.Click, New System.EventHandler(AddressOf(Me.radMenuItemRotate180_Click))
			' 
			' radMenuItemRotate270
			' 
			Me.radMenuItemRotate270.AccessibleDescription = "Rotate 270"
			Me.radMenuItemRotate270.AccessibleName = "Rotate 270"
			Me.radMenuItemRotate270.Image = Global.TelerikEditor.Properties.Resources.PageOrientationRotate270
			Me.radMenuItemRotate270.Name = "radMenuItemRotate270"
			Me.radMenuItemRotate270.Text = "Rotate 270"
			Me.radMenuItemRotate270.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemRotate270.Click, New System.EventHandler(AddressOf(Me.radMenuItemRotate270_Click))
			' 
			' ribbonTab4
			' 
			Me.ribbonTab4.AccessibleDescription = "View"
			Me.ribbonTab4.AccessibleName = "View"
			Me.ribbonTab4.[Class] = "RibbonTab"
			Me.ribbonTab4.IsSelected = False
			Me.ribbonTab4.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radRibbonBarGroup5})
			Me.ribbonTab4.Name = "ribbonTab4"
			Me.ribbonTab4.Text = "View"
			Me.ribbonTab4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radRibbonBarGroup5
			' 
			Me.radRibbonBarGroup5.AccessibleDescription = "Document Views"
			Me.radRibbonBarGroup5.AccessibleName = "Document Views"
			Me.radRibbonBarGroup5.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnWebLayout, Me.radBtnPrintLayout})
			Me.radRibbonBarGroup5.Margin = New System.Windows.Forms.Padding(-1, -1, 0, -1)
			Me.radRibbonBarGroup5.Name = "radRibbonBarGroup5"
			Me.radRibbonBarGroup5.Text = "Document Views"
			Me.radRibbonBarGroup5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnWebLayout
			' 
			Me.radBtnWebLayout.AccessibleDescription = "Web Layout"
			Me.radBtnWebLayout.AccessibleName = "Web Layout"
			Me.radBtnWebLayout.Image = (CType((resources.GetObject("radBtnWebLayout.Image")), System.Drawing.Image))
			Me.radBtnWebLayout.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnWebLayout.ImageKey = "WebLayout"
			Me.radBtnWebLayout.Name = "radBtnWebLayout"
			Me.radBtnWebLayout.Text = "Web Layout"
			Me.radBtnWebLayout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
			Me.radBtnWebLayout.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnWebLayout.ToggleStateChanged, New Telerik.WinControls.UI.StateChangedEventHandler(AddressOf(Me.radBtnWebLayout_ToggleStateChanged))
			' 
			' radBtnPrintLayout
			' 
			Me.radBtnPrintLayout.AccessibleDescription = "Print Layout"
			Me.radBtnPrintLayout.AccessibleName = "Print Layout"
			Me.radBtnPrintLayout.Image = (CType((resources.GetObject("radBtnPrintLayout.Image")), System.Drawing.Image))
			Me.radBtnPrintLayout.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnPrintLayout.ImageKey = "PageBreak"
			Me.radBtnPrintLayout.Name = "radBtnPrintLayout"
			Me.radBtnPrintLayout.Text = "Print Layout"
			Me.radBtnPrintLayout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
			Me.radBtnPrintLayout.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnPrintLayout.ToggleStateChanged, New Telerik.WinControls.UI.StateChangedEventHandler(AddressOf(Me.radBtnPrintLayout_ToggleStateChanged))
			' 
			' ribbonTab5
			' 
			Me.ribbonTab5.AccessibleDescription = "Review"
			Me.ribbonTab5.AccessibleName = "Review"
			Me.ribbonTab5.[Class] = "RibbonTab"
			Me.ribbonTab5.IsSelected = False
			Me.ribbonTab5.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radRibbonBarGroup6})
			Me.ribbonTab5.Name = "ribbonTab5"
			Me.ribbonTab5.Text = "Review"
			Me.ribbonTab5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radRibbonBarGroup6
			' 
			Me.radRibbonBarGroup6.AccessibleDescription = "Proofing"
			Me.radRibbonBarGroup6.AccessibleName = "Proofing"
			Me.radRibbonBarGroup6.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radBtnSpellCheck})
			Me.radRibbonBarGroup6.Margin = New System.Windows.Forms.Padding(-1, -1, 0, -1)
			Me.radRibbonBarGroup6.Name = "radRibbonBarGroup6"
			Me.radRibbonBarGroup6.Text = "Proofing"
			Me.radRibbonBarGroup6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radBtnSpellCheck
			' 
			Me.radBtnSpellCheck.AccessibleDescription = "Spell Check"
			Me.radBtnSpellCheck.AccessibleName = "Spell Check"
			Me.radBtnSpellCheck.[Class] = "RibbonBarButtonElement"
			Me.radBtnSpellCheck.Image = (CType((resources.GetObject("radBtnSpellCheck.Image")), System.Drawing.Image))
			Me.radBtnSpellCheck.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnSpellCheck.ImageKey = "SpellCheck"
			Me.radBtnSpellCheck.Name = "radBtnSpellCheck"
			Me.radBtnSpellCheck.Text = "Spell Check"
			Me.radBtnSpellCheck.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
			Me.radBtnSpellCheck.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radBtnSpellCheck.Click, New System.EventHandler(AddressOf(Me.radBtnSpellCheck_Click))
			' 
			' radBtnSave
			' 
			Me.radBtnSave.AccessibleDescription = "radButtonElement6"
			Me.radBtnSave.AccessibleName = "radButtonElement6"
			Me.radBtnSave.[Class] = "RibbonBarButtonElement"
			Me.radBtnSave.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnSave.Image = Global.TelerikEditor.Properties.Resources.save
			Me.radBtnSave.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnSave.MaxSize = New System.Drawing.Size(0, 18)
			Me.radBtnSave.Name = "radBtnSave"
			Me.radBtnSave.Padding = New System.Windows.Forms.Padding(2, 1, 2, 2)
			Me.radBtnSave.Text = "Save"
			Me.radBtnSave.Visibility = Telerik.WinControls.ElementVisibility.Visible
			Me.radBtnSave.ZIndex = 3
			AddHandler Me.radBtnSave.Click, New System.EventHandler(AddressOf(Me.radBtnSave_Click))
			' 
			' radBtnUndo
			' 
			Me.radBtnUndo.AccessibleDescription = "radButtonElement7"
			Me.radBtnUndo.AccessibleName = "radButtonElement7"
			Me.radBtnUndo.[Class] = "RibbonBarButtonElement"
			Me.radBtnUndo.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnUndo.Image = Global.TelerikEditor.Properties.Resources.undo
			Me.radBtnUndo.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnUndo.MaxSize = New System.Drawing.Size(0, 18)
			Me.radBtnUndo.Name = "radBtnUndo"
			Me.radBtnUndo.Padding = New System.Windows.Forms.Padding(2, 1, 2, 2)
			Me.radBtnUndo.Text = "Undo"
			Me.radBtnUndo.Visibility = Telerik.WinControls.ElementVisibility.Visible
			Me.radBtnUndo.ZIndex = 2
			AddHandler Me.radBtnUndo.Click, New System.EventHandler(AddressOf(Me.radBtnUndo_Click))
			' 
			' radBtnRedo
			' 
			Me.radBtnRedo.AccessibleDescription = "radButtonElement8"
			Me.radBtnRedo.AccessibleName = "radButtonElement8"
			Me.radBtnRedo.[Class] = "RibbonBarButtonElement"
			Me.radBtnRedo.DisplayStyle = Telerik.WinControls.DisplayStyle.Image
			Me.radBtnRedo.Image = Global.TelerikEditor.Properties.Resources.redo
			Me.radBtnRedo.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radBtnRedo.MaxSize = New System.Drawing.Size(0, 18)
			Me.radBtnRedo.Name = "radBtnRedo"
			Me.radBtnRedo.Padding = New System.Windows.Forms.Padding(2, 1, 2, 2)
			Me.radBtnRedo.Text = "Redo"
			Me.radBtnRedo.Visibility = Telerik.WinControls.ElementVisibility.Visible
			Me.radBtnRedo.ZIndex = 1
			AddHandler Me.radBtnRedo.Click, New System.EventHandler(AddressOf(Me.radBtnRedo_Click))
			' 
			' radMenuItemNew
			' 
			Me.radMenuItemNew.AccessibleDescription = "New"
			Me.radMenuItemNew.AccessibleName = "New"
			Me.radMenuItemNew.Image = (CType((resources.GetObject("radMenuItemNew.Image")), System.Drawing.Image))
			Me.radMenuItemNew.ImageKey = "new.png"
			Me.radMenuItemNew.Name = "radMenuItemNew"
			Me.radMenuItemNew.Text = "New"
			Me.radMenuItemNew.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemNew.Click, New System.EventHandler(AddressOf(Me.radMenuItemNew_Click))
			' 
			' radMenuItemOpenDoc
			' 
			Me.radMenuItemOpenDoc.AccessibleDescription = "Open"
			Me.radMenuItemOpenDoc.AccessibleName = "Open"
			Me.radMenuItemOpenDoc.Image = (CType((resources.GetObject("radMenuItemOpenDoc.Image")), System.Drawing.Image))
			Me.radMenuItemOpenDoc.ImageKey = "open.png"
			Me.radMenuItemOpenDoc.Name = "radMenuItemOpenDoc"
			Me.radMenuItemOpenDoc.Text = "Open"
			Me.radMenuItemOpenDoc.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemOpenDoc.Click, New System.EventHandler(AddressOf(Me.radMenuItemOpenDoc_Click))
			' 
			' radMenuItemSave
			' 
			Me.radMenuItemSave.AccessibleDescription = "Save"
			Me.radMenuItemSave.AccessibleName = "Save"
			Me.radMenuItemSave.Image = (CType((resources.GetObject("radMenuItemSave.Image")), System.Drawing.Image))
			Me.radMenuItemSave.ImageKey = "save.png"
			Me.radMenuItemSave.Name = "radMenuItemSave"
			Me.radMenuItemSave.Text = "Save"
			Me.radMenuItemSave.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemSave.Click, New System.EventHandler(AddressOf(Me.radBtnSave_Click))
			' 
			' radMenuItemSaveAs
			' 
			Me.radMenuItemSaveAs.AccessibleDescription = "Save As"
			Me.radMenuItemSaveAs.AccessibleName = "Save As"
			Me.radMenuItemSaveAs.Image = (CType((resources.GetObject("radMenuItemSaveAs.Image")), System.Drawing.Image))
			Me.radMenuItemSaveAs.ImageKey = "saveas.png"
			Me.radMenuItemSaveAs.Items.AddRange(New Telerik.WinControls.RadItem() {Me.radMenuItemSaveWordDoc, Me.radMenuItemSavePDFDocument, Me.radMenuItemSaveHtmlDocument, Me.radMenutItemSaveRTF, Me.radMenuItemSavePlainText, Me.radMenuItemSaveXAML})
			Me.radMenuItemSaveAs.Name = "radMenuItemSaveAs"
			Me.radMenuItemSaveAs.Text = "Save As"
			Me.radMenuItemSaveAs.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemSaveAs.Click, New System.EventHandler(AddressOf(Me.radBtnSave_Click))
			' 
			' radMenuItemSaveWordDoc
			' 
			Me.radMenuItemSaveWordDoc.AccessibleDescription = "Word Document"
			Me.radMenuItemSaveWordDoc.AccessibleName = "Word Document"
			Me.radMenuItemSaveWordDoc.Image = (CType((resources.GetObject("radMenuItemSaveWordDoc.Image")), System.Drawing.Image))
			Me.radMenuItemSaveWordDoc.ImageKey = "worddoc.png"
			Me.radMenuItemSaveWordDoc.Name = "radMenuItemSaveWordDoc"
			Me.radMenuItemSaveWordDoc.Text = "Word Document"
			Me.radMenuItemSaveWordDoc.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemSaveWordDoc.Click, New System.EventHandler(AddressOf(Me.radBtnSave_Click))
			' 
			' radMenuItemSavePDFDocument
			' 
			Me.radMenuItemSavePDFDocument.AccessibleDescription = "radMenuItem2"
			Me.radMenuItemSavePDFDocument.AccessibleName = "radMenuItem2"
			Me.radMenuItemSavePDFDocument.Image = (CType((resources.GetObject("radMenuItemSavePDFDocument.Image")), System.Drawing.Image))
			Me.radMenuItemSavePDFDocument.ImageKey = "Pdf.png"
			Me.radMenuItemSavePDFDocument.Name = "radMenuItemSavePDFDocument"
			Me.radMenuItemSavePDFDocument.Text = "PDF Document"
			Me.radMenuItemSavePDFDocument.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemSavePDFDocument.Click, New System.EventHandler(AddressOf(Me.radBtnSave_Click))
			' 
			' radMenuItemSaveHtmlDocument
			' 
			Me.radMenuItemSaveHtmlDocument.AccessibleDescription = "radMenuItem3"
			Me.radMenuItemSaveHtmlDocument.AccessibleName = "radMenuItem3"
			Me.radMenuItemSaveHtmlDocument.Image = (CType((resources.GetObject("radMenuItemSaveHtmlDocument.Image")), System.Drawing.Image))
			Me.radMenuItemSaveHtmlDocument.ImageKey = "Html.png"
			Me.radMenuItemSaveHtmlDocument.Name = "radMenuItemSaveHtmlDocument"
			Me.radMenuItemSaveHtmlDocument.Text = "Html Document"
			Me.radMenuItemSaveHtmlDocument.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemSaveHtmlDocument.Click, New System.EventHandler(AddressOf(Me.radBtnSave_Click))
			' 
			' radMenutItemSaveRTF
			' 
			Me.radMenutItemSaveRTF.AccessibleDescription = "radMenuItem4"
			Me.radMenutItemSaveRTF.AccessibleName = "radMenuItem4"
			Me.radMenutItemSaveRTF.Image = (CType((resources.GetObject("radMenutItemSaveRTF.Image")), System.Drawing.Image))
			Me.radMenutItemSaveRTF.ImageKey = "Rtf.png"
			Me.radMenutItemSaveRTF.Name = "radMenutItemSaveRTF"
			Me.radMenutItemSaveRTF.Text = "Rich Text Format"
			Me.radMenutItemSaveRTF.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenutItemSaveRTF.Click, New System.EventHandler(AddressOf(Me.radBtnSave_Click))
			' 
			' radMenuItemSavePlainText
			' 
			Me.radMenuItemSavePlainText.AccessibleDescription = "Plain Text"
			Me.radMenuItemSavePlainText.AccessibleName = "Plain Text"
			Me.radMenuItemSavePlainText.Image = (CType((resources.GetObject("radMenuItemSavePlainText.Image")), System.Drawing.Image))
			Me.radMenuItemSavePlainText.ImageKey = "PlainText.png"
			Me.radMenuItemSavePlainText.Name = "radMenuItemSavePlainText"
			Me.radMenuItemSavePlainText.Text = "Plain Text"
			Me.radMenuItemSavePlainText.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemSavePlainText.Click, New System.EventHandler(AddressOf(Me.radBtnSave_Click))
			' 
			' radMenuItemSaveXAML
			' 
			Me.radMenuItemSaveXAML.AccessibleDescription = "XAML Document"
			Me.radMenuItemSaveXAML.AccessibleName = "XAML Document"
			Me.radMenuItemSaveXAML.Image = (CType((resources.GetObject("radMenuItemSaveXAML.Image")), System.Drawing.Image))
			Me.radMenuItemSaveXAML.ImageKey = "Xaml.png"
			Me.radMenuItemSaveXAML.Name = "radMenuItemSaveXAML"
			Me.radMenuItemSaveXAML.Text = "XAML Document"
			Me.radMenuItemSaveXAML.Visibility = Telerik.WinControls.ElementVisibility.Visible
			AddHandler Me.radMenuItemSaveXAML.Click, New System.EventHandler(AddressOf(Me.radBtnSave_Click))
			' 
			' radButtonElement1
			' 
			Me.radButtonElement1.AccessibleDescription = "Picture"
			Me.radButtonElement1.AccessibleName = "Picture"
			Me.radButtonElement1.Alignment = System.Drawing.ContentAlignment.TopLeft
			Me.radButtonElement1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
			Me.radButtonElement1.[Class] = "RibbonBarButtonElement"
			Me.radButtonElement1.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent
			Me.radButtonElement1.Image = (CType((resources.GetObject("radButtonElement1.Image")), System.Drawing.Image))
			Me.radButtonElement1.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter
			Me.radButtonElement1.ImageKey = "Picture"
			Me.radButtonElement1.Name = "radButtonElement1"
			Me.radButtonElement1.Text = "Picture"
			Me.radButtonElement1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
			Me.radButtonElement1.TextWrap = True
			Me.radButtonElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' MainForm
			' 
			Me.AllowAero = False
			Me.AutoScaleDimensions = New System.Drawing.SizeF(6F, 13F)
			Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
			Me.ClientSize = New System.Drawing.Size(992, 611)
			Me.Controls.Add(Me.radRibbonBarBackstageView1)
			Me.Controls.Add(Me.radRichTextBox1)
			Me.Controls.Add(Me.radRibbonBar1)
			Me.MainMenuStrip = Nothing
			Me.Name = "MainForm"
			' 
			' 
			' 
			Me.RootElement.ApplyShapeToControl = True
			Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
			Me.Text = "Telerik Editor"
			(CType((Me.radRichTextBox1), System.ComponentModel.ISupportInitialize)).EndInit()
			(CType((Me.radRibbonBarBackstageView1), System.ComponentModel.ISupportInitialize)).EndInit()
			Me.radRibbonBarBackstageView1.ResumeLayout(False)
			Me.backstageViewPage1.ResumeLayout(False)
			(CType((Me.radPageView1), System.ComponentModel.ISupportInitialize)).EndInit()
			Me.radPageView1.ResumeLayout(False)
			Me.radPageViewPageSaveAsWord.ResumeLayout(False)
			Me.radPageViewPageSaveAsPDF.ResumeLayout(False)
			Me.radPageViewPageSaveAsHtml.ResumeLayout(False)
			Me.radPageViewPageSaveAsRTF.ResumeLayout(False)
			Me.radPageViewPageSaveAsText.ResumeLayout(False)
			Me.radPageViewPageSaveAsXAML.ResumeLayout(False)
			(CType((Me.radRibbonBar1), System.ComponentModel.ISupportInitialize)).EndInit()
			(CType((Me.radDropDownListFont), System.ComponentModel.ISupportInitialize)).EndInit()
			(CType((Me.radDropDownListFontSize), System.ComponentModel.ISupportInitialize)).EndInit()
			(CType((Me), System.ComponentModel.ISupportInitialize)).EndInit()
			Me.ResumeLayout(False)
			Me.PerformLayout()

		End Sub

		#endregion

		Private radRibbonBar1 As Telerik.WinControls.UI.RadRibbonBar
		Private radRichTextBox1 As Telerik.WinControls.RichTextBox.RadRichTextBox
		Private ribbonTab1 As Telerik.WinControls.UI.RibbonTab
		Private ribbonTab2 As Telerik.WinControls.UI.RibbonTab
		Private radRibbonBarGroup1 As Telerik.WinControls.UI.RadRibbonBarGroup
		Private radBtnPageBreak As Telerik.WinControls.UI.RadButtonElement
		Private radRibbonBarGroup2 As Telerik.WinControls.UI.RadRibbonBarGroup
		Private ribbonTab3 As Telerik.WinControls.UI.RibbonTab
		Private ribbonTab4 As Telerik.WinControls.UI.RibbonTab
		Private ribbonTab5 As Telerik.WinControls.UI.RibbonTab
		Private imageList32Size As System.Windows.Forms.ImageList
		Private radBtnInsertPicture As Telerik.WinControls.UI.RadButtonElement
		Private radRibbonBarGroup4 As Telerik.WinControls.UI.RadRibbonBarGroup
		Private radDropDownButtonSize As Telerik.WinControls.UI.RadDropDownButtonElement
		Private radDropDownButtonPageMargins As Telerik.WinControls.UI.RadDropDownButtonElement
		Private radDropDownButtonPageOrientation As Telerik.WinControls.UI.RadDropDownButtonElement
		Private radRibbonBarGroup5 As Telerik.WinControls.UI.RadRibbonBarGroup
		Private radBtnWebLayout As Telerik.WinControls.UI.RadToggleButtonElement
		Private radBtnPrintLayout As Telerik.WinControls.UI.RadToggleButtonElement
		Private radRibbonBarGroup6 As Telerik.WinControls.UI.RadRibbonBarGroup
		Private radBtnSpellCheck As Telerik.WinControls.UI.RadButtonElement
		Private radRibbonBarGroup7 As Telerik.WinControls.UI.RadRibbonBarGroup
		Private radBtnCut As Telerik.WinControls.UI.RadButtonElement
		Private radBtnCopy As Telerik.WinControls.UI.RadButtonElement
		Private radButtonPaste As Telerik.WinControls.UI.RadButtonElement
		Private radRibbonBarGroup8 As Telerik.WinControls.UI.RadRibbonBarGroup
		Private radRibbonBarGroup9 As Telerik.WinControls.UI.RadRibbonBarGroup
		Private radRibbonBarGroup10 As Telerik.WinControls.UI.RadRibbonBarGroup
		Private radRibbonBarButtonGroup1 As Telerik.WinControls.UI.RadRibbonBarButtonGroup
		Private radDropDownListFont As Telerik.WinControls.UI.RadDropDownListElement
		Private radDropDownListFontSize As Telerik.WinControls.UI.RadDropDownListElement
		Private radRibbonBarButtonGroup2 As Telerik.WinControls.UI.RadRibbonBarButtonGroup
		Private radRibbonBarButtonGroup4 As Telerik.WinControls.UI.RadRibbonBarButtonGroup
		Private radBtnFontSizeIncrease As Telerik.WinControls.UI.RadButtonElement
		Private radBtnFontSizeDecrease As Telerik.WinControls.UI.RadButtonElement
		Private radRibbonBarButtonGroup3 As Telerik.WinControls.UI.RadRibbonBarButtonGroup
		Private radBtnFormattingClear As Telerik.WinControls.UI.RadButtonElement
		Private radRibbonBarButtonGroup5 As Telerik.WinControls.UI.RadRibbonBarButtonGroup
		Private radRibbonBarButtonGroup6 As Telerik.WinControls.UI.RadRibbonBarButtonGroup
		Private radBtnBoldStyle As Telerik.WinControls.UI.RadToggleButtonElement
		Private radBtnItalicStyle As Telerik.WinControls.UI.RadToggleButtonElement
		Private radBtnUnderlineStyle As Telerik.WinControls.UI.RadToggleButtonElement
		Private radBtnStrikethrough As Telerik.WinControls.UI.RadToggleButtonElement
		Private radBtnHighlight As Telerik.WinControls.UI.RadButtonElement
		Private radBtnFontColor As Telerik.WinControls.UI.RadButtonElement
		Private radRibbonBarButtonGroup7 As Telerik.WinControls.UI.RadRibbonBarButtonGroup
		Private radRibbonBarButtonGroup9 As Telerik.WinControls.UI.RadRibbonBarButtonGroup
		Private radRibbonBarButtonGroup10 As Telerik.WinControls.UI.RadRibbonBarButtonGroup
		Private radRibbonBarButtonGroup8 As Telerik.WinControls.UI.RadRibbonBarButtonGroup
		Private radRibbonBarButtonGroup12 As Telerik.WinControls.UI.RadRibbonBarButtonGroup
		Private radRibbonBarButtonGroup13 As Telerik.WinControls.UI.RadRibbonBarButtonGroup
		Private radBtnBulletList As Telerik.WinControls.UI.RadToggleButtonElement
		Private radNumberingList As Telerik.WinControls.UI.RadToggleButtonElement
		Private radBtnDecreaseIndent As Telerik.WinControls.UI.RadButtonElement
		Private radBtnIncreaseIndent As Telerik.WinControls.UI.RadButtonElement
		Private radBtnBackColor As Telerik.WinControls.UI.RadButtonElement
		Private radBtnAligntLeft As Telerik.WinControls.UI.RadToggleButtonElement
		Private radBtnAlignCenter As Telerik.WinControls.UI.RadToggleButtonElement
		Private radBtnAligntRight As Telerik.WinControls.UI.RadToggleButtonElement
		Private radBtnAlignJustify As Telerik.WinControls.UI.RadToggleButtonElement
		Private radBtnSave As Telerik.WinControls.UI.RadButtonElement
		Private radBtnUndo As Telerik.WinControls.UI.RadButtonElement
		Private radBtnRedo As Telerik.WinControls.UI.RadButtonElement
		Private radBtnEnableSpellCheck As Telerik.WinControls.UI.RadToggleButtonElement
		Private radSpellChecker1 As Telerik.WinControls.UI.RadSpellChecker
		Private radMenuItemNew As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSave As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSaveAs As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSaveWordDoc As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSavePDFDocument As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSaveHtmlDocument As Telerik.WinControls.UI.RadMenuItem
		Private radMenutItemSaveRTF As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSavePlainText As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSaveXAML As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemOpenDoc As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemLandscape As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemPortrait As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemRotate180 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemRotate270 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSizeA0 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSizeA1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSizeA2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSizeA3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSizeA4 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemNormal As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemNarrow As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemModerate As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemOffice2003 As Telerik.WinControls.UI.RadMenuItem
		Private office2010BlueTheme1 As Telerik.WinControls.Themes.Office2010BlueTheme
		Private radRibbonBarBackstageView1 As Telerik.WinControls.UI.RadRibbonBarBackstageView
		Private backstageButtonNew As Telerik.WinControls.UI.BackstageButtonItem
		Private backstageButtonOpen As Telerik.WinControls.UI.BackstageButtonItem
		Private backstageButtonSave As Telerik.WinControls.UI.BackstageButtonItem
		Private backstageViewPage1 As Telerik.WinControls.UI.BackstageViewPage
		Private radPageView1 As Telerik.WinControls.UI.RadPageView
		Private backstageTabSaveAs As Telerik.WinControls.UI.BackstageTabItem
		Private radPageViewPageSaveAsWord As Telerik.WinControls.UI.RadPageViewPage
		Private radPageViewPageSaveAsPDF As Telerik.WinControls.UI.RadPageViewPage
		Private radPageViewPageSaveAsHtml As Telerik.WinControls.UI.RadPageViewPage
		Private radPageViewPageSaveAsRTF As Telerik.WinControls.UI.RadPageViewPage
		Private radPageViewPageSaveAsText As Telerik.WinControls.UI.RadPageViewPage
		Private radPageViewPageSaveAsXAML As Telerik.WinControls.UI.RadPageViewPage
		Private backstageButtonItemExit As Telerik.WinControls.UI.BackstageButtonItem
		Private documentInfoControl1 As DocumentInfoControl
		Private documentInfoControl2 As DocumentInfoControl
		Private documentInfoControl3 As DocumentInfoControl
		Private documentInfoControl4 As DocumentInfoControl
		Private documentInfoControl5 As DocumentInfoControl
		Private documentInfoControl6 As DocumentInfoControl
		Private radRibbonBarButtonGroup14 As Telerik.WinControls.UI.RadRibbonBarButtonGroup
		Private radBtnShowFormatting As Telerik.WinControls.UI.RadToggleButtonElement
		Private radRibbonBarGroup3 As Telerik.WinControls.UI.RadRibbonBarGroup
		Private hyperlinkButtonElement As Telerik.WinControls.UI.RadButtonElement
		Private bookmarkButtonElement As Telerik.WinControls.UI.RadButtonElement
		Private radButtonElement1 As Telerik.WinControls.UI.RadButtonElement
		Private radRibbonBarButtonGroup11 As Telerik.WinControls.UI.RadRibbonBarButtonGroup
		Private subscriptButtonElement As Telerik.WinControls.UI.RadToggleButtonElement
		Private superscriptButtonElement As Telerik.WinControls.UI.RadToggleButtonElement
		Private radRibbonBarGroup11 As Telerik.WinControls.UI.RadRibbonBarGroup
		Private radDropDownButtonElementTables As Telerik.WinControls.UI.RadDropDownButtonElement
		Private radMenuItemInsertTable As Telerik.WinControls.UI.RadMenuItem
	End Class
End Namespace