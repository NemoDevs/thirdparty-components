Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Drawing
Imports System.Drawing.Text
Imports System.IO
Imports System.Threading
Imports System.Windows.Forms
Imports Telerik.WinControls
Imports Telerik.WinControls.Enumerations
Imports Telerik.WinControls.RichTextBox
Imports Telerik.WinControls.RichTextBox.FileFormats.Html
Imports Telerik.WinControls.RichTextBox.FileFormats.OpenXml.Docx
Imports Telerik.WinControls.RichTextBox.FileFormats.Pdf
Imports Telerik.WinControls.RichTextBox.FileFormats.Rtf
Imports Telerik.WinControls.RichTextBox.FileFormats.Xaml
Imports Telerik.WinControls.RichTextBox.FormatProviders
Imports Telerik.WinControls.RichTextBox.FormatProviders.Txt
Imports Telerik.WinControls.RichTextBox.Layout
Imports Telerik.WinControls.RichTextBox.Lists
Imports Telerik.WinControls.RichTextBox.Model
Imports Telerik.WinControls.RichTextBox.Model.Styles
Imports Telerik.WinControls.RichTextBox.UI
Imports Telerik.WinControls.UI
Imports TelerikEditor.Properties

Namespace TelerikEditor
	Public Partial Class MainForm
		Inherits RadRibbonForm
		Private stylesInitializing As Boolean = False
		Private firstShow As Boolean = True
		Private singleInstance As Boolean
		Private closeTimer As System.Windows.Forms.Timer

		Public Sub New()
			Initialize()
		End Sub

		Public Sub New(singleInstance As Boolean)
			' TODO: Complete member initialization
			Me.singleInstance = singleInstance
			Me.Opacity = 0
			Me.ShowInTaskbar = False

			Initialize()
		End Sub

		Private Sub Initialize()
			InitializeComponent()


			Dim insertTableBoxItem As New RadMenuInsertTableItem()
			Me.radDropDownButtonElementTables.Items.Insert(0, insertTableBoxItem)
			insertTableBoxItem.SelectionChanged += New EventHandler(Of TableSelectionChangedEventArgs)(OnInsertTableSelectionChanged)

			AddHandler Me.radMenuItemInsertTable.Click, New EventHandler(AddressOf(radMenuItemInsertTable_Click))
			ThemeResolutionService.ApplicationThemeName = "Office2010Blue"

			Me.radRibbonBar1.RibbonBarElement.ApplicationButtonElement.Text = "File"
			Me.radRibbonBar1.RibbonBarElement.ApplicationButtonElement.ForeColor = Color.White
			Me.radRibbonBar1.RibbonBarElement.ApplicationButtonElement.DisplayStyle = DisplayStyle.Text

			Me.radDropDownListFontSize.DropDownStyle = RadDropDownStyle.DropDownList
			Me.radDropDownListFont.DropDownStyle = RadDropDownStyle.DropDownList

			Me.radPageView1.SelectedPage = Me.radPageViewPageSaveAsWord

			Dim docxProvider As New DocxFormatProvider()
			Dim document As RadDocument = docxProvider.Import(Resources.RichTextBox_for_WinForms)
			Me.AttachDocument(document)
			Me.radRichTextBox1.Document = document
			Me.radBtnPrintLayout.ToggleState = ToggleState.[On]
			AddHandler Me.radRichTextBox1.CurrentEditingStyleChanged, New EventHandler(AddressOf(radRichTextBox1_CurrentEditingStyleChanged))

			Me.AddHandler radPageViewPageSaveAsHtml.Item.Click, New EventHandler(AddressOf(Me.radBtnSave_Click))
			Me.AddHandler radPageViewPageSaveAsPDF.Item.Click, New EventHandler(AddressOf(Me.radBtnSave_Click))
			Me.AddHandler radPageViewPageSaveAsRTF.Item.Click, New EventHandler(AddressOf(Me.radBtnSave_Click))
			Me.AddHandler radPageViewPageSaveAsText.Item.Click, New EventHandler(AddressOf(Me.radBtnSave_Click))
			Me.AddHandler radPageViewPageSaveAsWord.Item.Click, New EventHandler(AddressOf(Me.radBtnSave_Click))
			Me.AddHandler radPageViewPageSaveAsXAML.Item.Click, New EventHandler(AddressOf(Me.radBtnSave_Click))

			Me.radDropDownListFont.DropDownAnimationEnabled = False
			Me.radDropDownListFontSize.DropDownAnimationEnabled = False

			Me.closeTimer = New System.Windows.Forms.Timer()
			Me.closeTimer.Interval = 3000
			AddHandler Me.closeTimer.Tick, New EventHandler(AddressOf(closeTimer_Tick))
			Me.closeTimer.Start()
		End Sub

		Private Sub InitializeUI()
			Me.stylesInitializing = True
			Dim styleDefinition As StyleDefinition = Me.radRichTextBox1.CurrentEditingStyle
			Me.InitializeCurrentFontStyles(styleDefinition)
			Me.InitializeCurrentParagraphStyles(styleDefinition)

			Dim baselineAlignment As BaselineAlignment = CType(styleDefinition.GetPropertyValue(Span.BaselineAlignmentProperty), BaselineAlignment)
			Me.subscriptButtonElement.ToggleState = If(baselineAlignment = BaselineAlignment.Subscript, ToggleState.[On], ToggleState.Off)
			Me.superscriptButtonElement.ToggleState = If(baselineAlignment = BaselineAlignment.Superscript, ToggleState.[On], ToggleState.Off)

			Me.stylesInitializing = False
		End Sub

		Private Sub InitializeCurrentFontStyles(styleDefinition As StyleDefinition)
			Dim underlineType As UnderlineType = CType(styleDefinition.GetPropertyValue(Span.UnderlineTypeProperty), UnderlineType)
			Me.radBtnUnderlineStyle.ToggleState = If(underlineType <> UnderlineType.None, ToggleState.[On], ToggleState.Off)

			Dim fontFamiliy As String = CType(styleDefinition.GetPropertyValue(Span.FontFamilyProperty), String)
			Me.radDropDownListFont.SuspendSelectionEvents = True
			Me.radDropDownListFont.Text = fontFamiliy
			Me.radDropDownListFont.SelectedValue = fontFamiliy
			Me.radDropDownListFont.SuspendSelectionEvents = False

			Dim fontSize As Single = CType(styleDefinition.GetPropertyValue(Span.FontSizeProperty), Single)
			fontSize = CType(Math.Round(Unit.DipToPoint(fontSize), 1), Single)
			Me.EnsureFontSize(fontSize.ToString())

			Dim fontStyle As TextStyle = CType(styleDefinition.GetPropertyValue(Span.FontStyleProperty), TextStyle)

			Me.radBtnBoldStyle.ToggleState = If(fontStyle.HasFlag(TextStyle.Bold), ToggleState.[On], ToggleState.Off)
			Me.radBtnItalicStyle.ToggleState = If(fontStyle.HasFlag(TextStyle.Italic), ToggleState.[On], ToggleState.Off)

			Dim strikeThrough As Boolean = CType(styleDefinition.GetPropertyValue(Span.StrikethroughProperty), Boolean)
			Me.radBtnStrikethrough.ToggleState = If(strikeThrough, ToggleState.[On], ToggleState.Off)
		End Sub

		Private Sub InitializeCurrentParagraphStyles(styleDefinition As StyleDefinition)
			Dim paragraph As Paragraph = Me.radRichTextBox1.Document.CaretPosition.GetCurrentParagraphBox().AssociatedParagraph
			Dim textAlignment As RadTextAlignment = paragraph.TextAlignment

			Me.radBtnAligntLeft.ToggleState = If(textAlignment = RadTextAlignment.Left, ToggleState.[On], ToggleState.Off)
			Me.radBtnAlignCenter.ToggleState = If(textAlignment = RadTextAlignment.Center, ToggleState.[On], ToggleState.Off)
			Me.radBtnAligntRight.ToggleState = If(textAlignment = RadTextAlignment.Right, ToggleState.[On], ToggleState.Off)
			Me.radBtnAlignJustify.ToggleState = If(textAlignment = RadTextAlignment.Justify, ToggleState.[On], ToggleState.Off)

			Dim format As System.Nullable(Of ListNumberingFormat) = Nothing

			If paragraph.IsInList Then
				format = paragraph.ListItemInfo.List.Style.Levels(paragraph.ListItemInfo.ListLevel).NumberingFormat
			End If

			Me.radBtnBulletList.ToggleState = If(format = ListNumberingFormat.Bullet, ToggleState.[On], ToggleState.Off)

			Me.radNumberingList.ToggleState = If(format = ListNumberingFormat.[Decimal], ToggleState.[On], ToggleState.Off)
		End Sub

		Private Sub AttachDocument(document As RadDocument)
			AddHandler document.PropertyChanged, New System.ComponentModel.PropertyChangedEventHandler(AddressOf(Document_PropertyChanged))
		End Sub

		Private Sub DetachDocument(document As RadDocument)
			document.PropertyChanged -= New System.ComponentModel.PropertyChangedEventHandler(Document_PropertyChanged)
		End Sub

		Private Shared Sub ExitApp(sync As Object)
			Dim mutex As Mutex = TryCast(sync, Mutex)
			mutex.WaitOne()

			Application.[Exit]()
		End Sub

		Private Sub EnsureFontSize(fontSize As String)
			Me.radDropDownListFontSize.SuspendSelectionEvents = True

			For Each item As RadListDataItem In Me.radDropDownListFontSize.Items
				If String.Compare(item.Text, fontSize) = 0 Then
					Me.radDropDownListFontSize.SelectedIndex = item.RowIndex
					Me.radDropDownListFontSize.Text = item.Text
					Me.radDropDownListFontSize.SuspendSelectionEvents = False
					Return
				End If
			Next

			Me.radDropDownListFontSize.Text = fontSize
			Me.radDropDownListFontSize.SelectedIndex = -1

			Me.radDropDownListFontSize.SuspendSelectionEvents = False
		End Sub

		Private Sub OpenDocument()
			Using openDialog As New OpenFileDialog()
				openDialog.Filter = "Word Documents (*.docx)|*.docx|Web Pages (*.htm,*.html)|*.htm;*.html|Rich Text Format (*.rtf)|*.rtf|Text Files (*.txt)|*.txt|XAML Files (*.xaml)|*.xaml"

				If openDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
					Dim extension As String = Path.GetExtension(openDialog.SafeFileName).ToLower()

					Dim provider As IDocumentFormatProvider = GetProviderByExtension(extension)

					If provider = Nothing Then
						RadMessageBox.Show("Unable to find format provider for extension " + extension, "Error")
						Return
					End If

					Using stream As Stream = openDialog.OpenFile()
						Dim document As RadDocument = provider.Import(stream)
						Me.DetachDocument(Me.radRichTextBox1.Document)
						Me.radRichTextBox1.Document = document
						Me.AttachDocument(document)
						document.LayoutMode = DocumentLayoutMode.Paged
					End Using
				End If

				Me.radRichTextBox1.Focus()
			End Using
		End Sub

		Private Sub SaveDocument(format As String)
			Using saveDialog As New SaveFileDialog()
				If format = "docx" Then
					saveDialog.Filter = "Word Document (*.docx)|*.docx"
				ElseIf format = "rtf" Then
					saveDialog.Filter = "Rich Text Format (*.rtf)|*.rtf"
				ElseIf format = "html" Then
					saveDialog.Filter = "Web Page (*.html)|*.html"
				ElseIf format = "xaml" Then
					saveDialog.Filter = "XAML File (*.xaml)|*.xaml"
				ElseIf format = "txt" Then
					saveDialog.Filter = "Text File (*.txt)|*.txt"
				ElseIf format = "pdf" Then
					saveDialog.Filter = "PDF File (*.pdf)|*.pdf"
				Else
					saveDialog.Filter = "Word Document (*.docx)|*.docx|PDF File (*.pdf)|*.pdf|Web Page (*.html)|*.html|Rich Text Format (*.rtf)|*.rtf|Text File (*.txt)|*.txt|XAML File (*.xaml)|*.xaml"
				End If

				If saveDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
					Dim extension As String = System.IO.Path.GetExtension(saveDialog.FileName)

					Dim provider As IDocumentFormatProvider = GetProviderByExtension(extension)

					If provider = Nothing Then
						RadMessageBox.Show("Unable to find format provider for extension " + extension, "Error")
						Return
					End If

					Using output As Stream = saveDialog.OpenFile()
						provider.Export(Me.radRichTextBox1.Document, output)
					End Using
				End If

				Me.radRichTextBox1.Focus()
			End Using
		End Sub

		Private Function GetProviderByExtension(extension As String) As IDocumentFormatProvider
			If extension = ".xaml" Then
				Return New XamlFormatProvider()
			End If

			If extension = ".docx" Then
				Return New DocxFormatProvider()
			End If

			If extension = ".rtf" Then
				Return New RtfFormatProvider()
			End If

			If extension = ".html" OrElse extension = ".htm" Then
				Return New HtmlFormatProvider()
			End If

			If extension = ".txt" Then
				Return New TxtFormatProvider()
			End If

			If extension = ".pdf" Then
				Return New PdfFormatProvider()
			End If

			Return Nothing
		End Function

		#region Event handlers

		Protected Overrides Sub OnLoad(e As System.EventArgs)
			MyBase.OnLoad(e)

			Dim font As New InstalledFontCollection()

			Dim families As New List(Of String)()

			For Each familiy As FontFamily In font.Families
				If familiy.IsStyleAvailable(FontStyle.Regular) AndAlso familiy.IsStyleAvailable(FontStyle.Italic) AndAlso familiy.IsStyleAvailable(FontStyle.Bold) Then
					families.Add(familiy.Name)
				End If
			Next

			Me.radDropDownListFont.DataSource = families

			Me.InitializeUI()
		End Sub

		Protected Overrides Sub OnShown(e As EventArgs)
			MyBase.OnShown(e)

			If Me.singleInstance Then
				If firstShow Then
					Me.Visible = False
					Me.firstShow = False
					Me.Opacity = 1
				Else
					Me.ShowInTaskbar = True
				End If
			End If
		End Sub

		Protected Overrides Sub OnClosing(e As CancelEventArgs)
			If singleInstance Then
				e.Cancel = True
				Me.Hide()
			End If

			MyBase.OnClosing(e)
		End Sub

		Private Sub OnInsertTableSelectionChanged(sender As Object, e As TableSelectionChangedEventArgs)
			If e.RowIndex >= 0 AndAlso e.ColumnIndex >= 0 Then
				Me.radRichTextBox1.InsertTable(e.RowIndex + 1, e.ColumnIndex + 1)
			End If
		End Sub

		Private Sub closeTimer_Tick(sender As Object, e As System.EventArgs) Handles closeTimer.Tick
			Dim process As Process() = Process.GetProcessesByName("TelerikExamples")
			If process.Length = 0 Then
				Me.closeTimer.[Stop]()
				Me.closeTimer.Dispose()

				If Me.Visible Then
					Me.singleInstance = False
				Else
					Application.[Exit]()
				End If
			End If
		End Sub

		Private Sub radRichTextBox1_CurrentEditingStyleChanged(sender As Object, e As EventArgs)
			Me.InitializeUI()
		End Sub

		Private Sub Document_PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs)
			Dim document As RadDocument = TryCast(sender, RadDocument)
			Me.radBtnPrintLayout.ToggleState = If(document.LayoutMode = DocumentLayoutMode.Paged, ToggleState.[On], ToggleState.Off)
			Me.radBtnWebLayout.ToggleState = If(document.LayoutMode = DocumentLayoutMode.Flow, ToggleState.[On], ToggleState.Off)
		End Sub

		Private Sub radDropDownListFont_SelectedIndexChanged(sender As Object, e As Telerik.WinControls.UI.Data.PositionChangedEventArgs)
			Me.radRichTextBox1.ChangeFontFamily(Convert.ToString(Me.radDropDownListFont.SelectedValue))
			Me.radDropDownListFont.ClosePopup()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radDropDownListFontSize_SelectedIndexChanged(sender As Object, e As Telerik.WinControls.UI.Data.PositionChangedEventArgs)
			If Me.radDropDownListFontSize.SelectedItem IsNot Nothing Then
				Dim selectedText As String = Me.radDropDownListFontSize.SelectedItem.Text
				Dim fontSize As Single = Single.Parse(selectedText)
				fontSize = CType(Math.Round(Unit.PointToDip(fontSize), 1), Single)
				Me.radRichTextBox1.ChangeFontSize(fontSize)
				Me.radDropDownListFontSize.ClosePopup()
				Me.radRichTextBox1.Focus()
			End If
		End Sub

		Private Sub radMenuItemOpenDoc_Click(sender As Object, e As System.EventArgs) Handles radMenuItemOpenDoc.Click
			Me.radRibbonBar1.BackstageControl.HidePopup()
			Me.OpenDocument()
		End Sub

		Private Sub radBtnSave_Click(sender As Object, e As System.EventArgs) Handles radBtnSave.Click
			Dim format As String = Nothing

			If sender = Me.radMenuItemSaveHtmlDocument OrElse sender = Me.radPageViewPageSaveAsHtml.Item Then
				format = "html"
			ElseIf sender = Me.radMenuItemSavePDFDocument OrElse sender = Me.radPageViewPageSaveAsPDF.Item Then
				format = "pdf"
			ElseIf sender = Me.radMenuItemSavePlainText OrElse sender = Me.radPageViewPageSaveAsText.Item Then
				format = "txt"
			ElseIf sender = Me.radMenuItemSaveWordDoc OrElse sender = Me.radPageViewPageSaveAsWord.Item Then
				format = "docx"
			ElseIf sender = Me.radMenuItemSaveXAML OrElse sender = Me.radPageViewPageSaveAsXAML.Item Then
				format = "xaml"
			ElseIf sender = Me.radMenutItemSaveRTF OrElse sender = Me.radPageViewPageSaveAsRTF.Item Then
				format = "rtf"
			End If

			Me.radRibbonBar1.BackstageControl.HidePopup()
			Me.SaveDocument(format)
		End Sub

		Private Sub radMenuItemNew_Click(sender As Object, e As System.EventArgs) Handles radMenuItemNew.Click
			If RadMessageBox.Show("Do you want to save changes you made to Document1?", "Telerik Word", MessageBoxButtons.YesNo, RadMessageIcon.Question) = System.Windows.Forms.DialogResult.Yes Then
				Me.SaveDocument(Nothing)
			End If

			Me.DetachDocument(Me.radRichTextBox1.Document)
			Me.radRichTextBox1.Document = New RadDocument()
			Me.AttachDocument(Me.radRichTextBox1.Document)
			Me.radRichTextBox1.LayoutMode = DocumentLayoutMode.Paged
			Me.radRibbonBar1.BackstageControl.HidePopup()
		End Sub

		Private Sub radMenuItemLandscape_Click(sender As Object, e As System.EventArgs) Handles radMenuItemLandscape.Click
			Me.radRichTextBox1.ChangeSectionPageOrientation(PageOrientation.Landscape)
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radMenuItemPortrait_Click(sender As Object, e As System.EventArgs) Handles radMenuItemPortrait.Click
			Me.radRichTextBox1.ChangeSectionPageOrientation(PageOrientation.Portrait)
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radMenuItemRotate180_Click(sender As Object, e As System.EventArgs) Handles radMenuItemRotate180.Click
			Me.radRichTextBox1.ChangeSectionPageOrientation(PageOrientation.Rotate180)
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radMenuItemRotate270_Click(sender As Object, e As System.EventArgs) Handles radMenuItemRotate270.Click
			Me.radRichTextBox1.ChangeSectionPageOrientation(PageOrientation.Rotate270)
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radMenuItemModerate_Click(sender As Object, e As System.EventArgs) Handles radMenuItemModerate.Click
			Me.radRichTextBox1.ChangeSectionMargin(PageMarginTypesConverter.ToPadding(PageMarginTypes.Moderate))
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radMenuItemNarrow_Click(sender As Object, e As System.EventArgs) Handles radMenuItemNarrow.Click
			Me.radRichTextBox1.ChangeSectionMargin(PageMarginTypesConverter.ToPadding(PageMarginTypes.Narrow))
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radMenuItemNormal_Click(sender As Object, e As System.EventArgs) Handles radMenuItemNormal.Click
			Me.radRichTextBox1.ChangeSectionMargin(PageMarginTypesConverter.ToPadding(PageMarginTypes.Normal))
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radMenuItemOffice2003_Click(sender As Object, e As System.EventArgs) Handles radMenuItemOffice2003.Click
			Me.radRichTextBox1.ChangeSectionMargin(PageMarginTypesConverter.ToPadding(PageMarginTypes.Office2003))
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radMenuItemSizeA0_Click(sender As Object, e As System.EventArgs) Handles radMenuItemSizeA0.Click
			Me.radRichTextBox1.ChangePageSize(PaperTypeConverter.ToSize(PaperTypes.A0))
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radMenuItemSizeA1_Click(sender As Object, e As System.EventArgs) Handles radMenuItemSizeA1.Click
			Me.radRichTextBox1.ChangePageSize(PaperTypeConverter.ToSize(PaperTypes.A1))
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radMenuItemSizeA2_Click(sender As Object, e As System.EventArgs) Handles radMenuItemSizeA2.Click
			Me.radRichTextBox1.ChangePageSize(PaperTypeConverter.ToSize(PaperTypes.A2))
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radMenuItemSizeA3_Click(sender As Object, e As System.EventArgs) Handles radMenuItemSizeA3.Click
			Me.radRichTextBox1.ChangePageSize(PaperTypeConverter.ToSize(PaperTypes.A3))
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radMenuItemSizeA4_Click(sender As Object, e As System.EventArgs) Handles radMenuItemSizeA4.Click
			Me.radRichTextBox1.ChangePageSize(PaperTypeConverter.ToSize(PaperTypes.A4))
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub backstageButtonItemExit_Click(sender As Object, e As System.EventArgs) Handles backstageButtonItemExit.Click
			Me.Close()
		End Sub

		Private Sub hyperlinkButtonElement_Click(sender As Object, e As System.EventArgs) Handles hyperlinkButtonElement.Click
			Me.radRichTextBox1.DocumentView.ShowInsertHyperlinkDialog()
		End Sub

		Private Sub bookmarkButtonElement_Click(sender As Object, e As System.EventArgs) Handles bookmarkButtonElement.Click
			Me.radRichTextBox1.DocumentView.ShowManageBookmarksDialog()
		End Sub

		Private Sub radBtnBackColor_Click(sender As Object, e As System.EventArgs) Handles radBtnBackColor.Click
			Dim colorDialog As New RadColorDialog()

			If colorDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
				Me.radRichTextBox1.ChangeParagraphBackground(colorDialog.SelectedColor)
			End If

			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnEnableSpellCheck_Click(sender As Object, e As System.EventArgs) Handles radBtnEnableSpellCheck.Click
			Dim enabled As Boolean = Me.radRichTextBox1.IsSpellCheckingEnabled
			Me.radRichTextBox1.IsSpellCheckingEnabled = Not enabled
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnPageBreak_Click(sender As Object, e As System.EventArgs) Handles radBtnPageBreak.Click
			Me.radRichTextBox1.InsertPageBreak()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnInsertPicture_Click(sender As Object, e As System.EventArgs) Handles radBtnInsertPicture.Click
			Using openDialog As New OpenFileDialog()
				openDialog.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif"
				If openDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
					Dim extension As String = Path.GetExtension(openDialog.SafeFileName)
					Me.radRichTextBox1.InsertImage(openDialog.OpenFile())
				End If

				Me.radRichTextBox1.Focus()
			End Using
		End Sub

		Private Sub radBtnSpellCheck_Click(sender As Object, e As System.EventArgs) Handles radBtnSpellCheck.Click
			Me.radSpellChecker1.Check(Me.radRichTextBox1)
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnDecreaseIndent_Click(sender As Object, e As System.EventArgs) Handles radBtnDecreaseIndent.Click
			Me.radRichTextBox1.DecrementParagraphLeftIndent()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnIncreaseIndent_Click(sender As Object, e As System.EventArgs) Handles radBtnIncreaseIndent.Click
			Me.radRichTextBox1.IncrementParagraphLeftIndent()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnShowFormatting_Click(sender As Object, e As System.EventArgs) Handles radBtnShowFormatting.Click
			Me.radRichTextBox1.ToggleFormattingSymbols()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnFontSizeIncrease_Click(sender As Object, e As System.EventArgs) Handles radBtnFontSizeIncrease.Click
			Me.radRichTextBox1.IncrementFontSize()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnFontSizeDecrease_Click(sender As Object, e As System.EventArgs) Handles radBtnFontSizeDecrease.Click
			Me.radRichTextBox1.DecrementFontSize()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnFormattingClear_Click(sender As Object, e As System.EventArgs) Handles radBtnFormattingClear.Click
			Me.radRichTextBox1.ClearFormatting()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnUndo_Click(sender As Object, e As System.EventArgs) Handles radBtnUndo.Click
			Me.radRichTextBox1.Undo()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnRedo_Click(sender As Object, e As System.EventArgs) Handles radBtnRedo.Click
			Me.radRichTextBox1.Redo()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnCut_Click(sender As Object, e As System.EventArgs) Handles radBtnCut.Click
			Me.radRichTextBox1.Cut()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnCopy_Click(sender As Object, e As System.EventArgs) Handles radBtnCopy.Click
			Me.radRichTextBox1.Copy()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radButtonPaste_Click(sender As Object, e As System.EventArgs) Handles radButtonPaste.Click
			Me.radRichTextBox1.Paste()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnHighlight_Click(sender As Object, e As System.EventArgs) Handles radBtnHighlight.Click
			Dim colorDialog As New RadColorDialog()

			If colorDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
				Me.radRichTextBox1.ChangeTextHighlightColor(colorDialog.SelectedColor)
			End If

			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnFontColor_Click(sender As Object, e As System.EventArgs) Handles radBtnFontColor.Click
			Dim colorDialog As New RadColorDialog()

			If colorDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK Then
				Me.radRichTextBox1.ChangeTextForeColor(colorDialog.SelectedColor)
			End If

			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radMenuItemInsertTable_Click(sender As Object, e As System.EventArgs) Handles radMenuItemInsertTable.Click
			Using insertForm As New InsertTableForm()
				insertForm.Owner = Me

				If insertForm.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
					Me.radRichTextBox1.InsertTable(insertForm.RowsCount, insertForm.ColumnsCount)
				End If
			End Using
		End Sub

		Private Sub radBtnBoldStyle_ToggleStateChanged(sender As Object, args As StateChangedEventArgs)
			If stylesInitializing Then
				Return
			End If

			Me.radRichTextBox1.ToggleBold()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnUnderlineStyle_ToggleStateChanged(sender As Object, args As StateChangedEventArgs)
			If stylesInitializing Then
				Return
			End If

			Me.radRichTextBox1.ToggleUnderline()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnItalicStyle_ToggleStateChanged(sender As Object, args As StateChangedEventArgs)
			If stylesInitializing Then
				Return
			End If

			Me.radRichTextBox1.ToggleItalic()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnStrikethrough_ToggleStateChanged(sender As Object, args As StateChangedEventArgs)
			If stylesInitializing Then
				Return
			End If

			Me.radRichTextBox1.ToggleStrikethrough()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnBulletList_ToggleStateChanged(sender As Object, args As Telerik.WinControls.UI.StateChangedEventArgs)
			If stylesInitializing Then
				Return
			End If

			Dim style As ListStyle = DefaultListStyles.None
			If args.ToggleState = ToggleState.[On] Then
				style = DefaultListStyles.Bulleted
			End If

			Me.radRichTextBox1.ChangeListStyle(style)
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radNumberingList_ToggleStateChanged(sender As Object, args As Telerik.WinControls.UI.StateChangedEventArgs)
			If stylesInitializing Then
				Return
			End If

			Dim style As ListStyle = DefaultListStyles.None
			If args.ToggleState = ToggleState.[On] Then
				style = DefaultListStyles.Numbered
			End If

			Me.radRichTextBox1.ChangeListStyle(style)
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnAligntLeft_ToggleStateChanged(sender As Object, args As StateChangedEventArgs)
			If stylesInitializing Then
				Return
			End If

			Dim textAlignment As RadTextAlignment = RadTextAlignment.Justify
			If args.ToggleState = ToggleState.[On] Then
				textAlignment = RadTextAlignment.Left
			End If

			Me.radRichTextBox1.ChangeTextAlignment(textAlignment)
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnAlignCenter_ToggleStateChanged(sender As Object, args As StateChangedEventArgs)
			If stylesInitializing Then
				Return
			End If

			Dim textAlignment As RadTextAlignment = RadTextAlignment.Left
			If args.ToggleState = ToggleState.[On] Then
				textAlignment = RadTextAlignment.Center
			End If

			Me.radRichTextBox1.ChangeTextAlignment(textAlignment)
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnAligntRight_ToggleStateChanged(sender As Object, args As StateChangedEventArgs)
			If stylesInitializing Then
				Return
			End If

			Dim textAlignment As RadTextAlignment = RadTextAlignment.Left
			If args.ToggleState = ToggleState.[On] Then
				textAlignment = RadTextAlignment.Right
			End If

			Me.radRichTextBox1.ChangeTextAlignment(textAlignment)
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnAlignJustify_ToggleStateChanged(sender As Object, args As StateChangedEventArgs)
			If stylesInitializing Then
				Return
			End If

			Dim textAlignment As RadTextAlignment = RadTextAlignment.Left
			If args.ToggleState = ToggleState.[On] Then
				textAlignment = RadTextAlignment.Justify
			End If

			Me.radRichTextBox1.ChangeTextAlignment(textAlignment)
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnWebLayout_ToggleStateChanged(sender As Object, args As Telerik.WinControls.UI.StateChangedEventArgs)
			If stylesInitializing Then
				Return
			End If

			Dim layout As DocumentLayoutMode = DocumentLayoutMode.Paged
			If args.ToggleState = ToggleState.[On] Then
				layout = DocumentLayoutMode.Flow
			End If

			Me.radRichTextBox1.LayoutMode = layout
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub radBtnPrintLayout_ToggleStateChanged(sender As Object, args As Telerik.WinControls.UI.StateChangedEventArgs)
			If stylesInitializing Then
				Return
			End If

			Dim layout As DocumentLayoutMode = DocumentLayoutMode.Flow
			If args.ToggleState = ToggleState.[On] Then
				layout = DocumentLayoutMode.Paged
			End If

			Me.radRichTextBox1.LayoutMode = layout
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub subscriptButtonElement_ToggleStateChanged(sender As Object, args As Telerik.WinControls.UI.StateChangedEventArgs)
			If stylesInitializing Then
				Return
			End If

			Me.radRichTextBox1.ToggleSubscript()
			Me.radRichTextBox1.Focus()
		End Sub

		Private Sub superscriptButtonElement_ToggleStateChanged(sender As Object, args As Telerik.WinControls.UI.StateChangedEventArgs)
			If stylesInitializing Then
				Return
			End If

			Me.radRichTextBox1.ToggleSuperscript()
			Me.radRichTextBox1.Focus()
		End Sub

		#endregion
	End Class
End Namespace