Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports Telerik.WinControls
Imports Telerik.WinControls.UI
Imports Telerik.WinControls.Layouts

Namespace TelerikEditor
	Public Class RadMenuInsertTableItem
		Inherits RadMenuItemBase
		#region Constants

		Private Const HeaderText As String = "Insert Table"

		#endregion

		#region Fields

		Private uniformGrid As UniformGrid
		Private header As LightVisualElement
		Private stackLayout As StackLayoutElement
		Private currentColumnIndex As Integer = -1
		Private currentRowIndex As Integer = -1

		#endregion

		#region Intialization

		Protected Overrides Sub InitializeFields()
			MyBase.InitializeFields()
			Me.ShouldHandleMouseInput = True
		End Sub

		Protected Overrides Sub CreateChildElements()
			MyBase.CreateChildElements()

			Dim rowsCount As Integer = 8
			Dim columnsCount As Integer = 10
			Dim boxSize As New Size(16, 16)

			Me.stackLayout = New StackLayoutElement()
			Me.stackLayout.Orientation = Orientation.Vertical
			Me.stackLayout.StretchHorizontally = True
			Me.stackLayout.StretchVertically = True
			Me.stackLayout.NotifyParentOnMouseInput = True
			Me.Children.Add(Me.stackLayout)

			Me.header = New LightVisualElement()
			Me.header.TextAlignment = ContentAlignment.MiddleLeft
			Me.header.StretchVertically = False
			Me.header.Text = RadMenuInsertTableItem.HeaderText
			Me.header.DrawFill = True
			Me.header.Font = New Font("Segoe UI", 9, FontStyle.Bold)
			Me.header.GradientStyle = GradientStyles.Solid
			Me.header.BackColor = Color.FromArgb(240, 242, 245)
			Me.stackLayout.Children.Add(Me.header)

			Me.uniformGrid = New UniformGrid()
			Me.uniformGrid.StretchVertically = True
			Me.uniformGrid.NotifyParentOnMouseInput = True
			Me.uniformGrid.Rows = rowsCount
			Me.uniformGrid.Columns = columnsCount
			Me.uniformGrid.MinSize = New Size(160, 140)
			Me.uniformGrid.Margin = New Padding(5, 5, 0, 0)
			Me.stackLayout.Children.Add(Me.uniformGrid)

			Dim i As Integer = 1
			While i <= rowsCount * columnsCount
				Dim box As New LightVisualElement()
				box.DrawBorder = True
				box.BorderGradientStyle = GradientStyles.Solid
				box.BorderBoxStyle = BorderBoxStyle.OuterInnerBorders
				box.MaxSize = boxSize
				box.MinSize = boxSize
				box.NotifyParentOnMouseInput = True
				box.MouseMove += New MouseEventHandler(OnBoxMouseMove)
				box.MouseDown += New MouseEventHandler(OnBoxMouseDown)
				Me.uniformGrid.Children.Add(box)
				System.Math.Max(System.Threading.Interlocked.Increment(i),i - 1)
			End While
		End Sub

		#endregion

		#region Properties

		Protected Overrides ReadOnly Property ThemeEffectiveType() As Type
			Get
				Return GetType(RadMenuItem)
			End Get
		End Property

		#endregion

		#region Events

		Public Event SelectionChanged As EventHandler(Of TableSelectionChangedEventArgs)

		Protected Overridable Sub OnSelectionChanged(e As TableSelectionChangedEventArgs)
			Dim handler As EventHandler(Of TableSelectionChangedEventArgs) = Me.SelectionChanged

			If handler IsNot Nothing Then
				handler(Me, e)
			End If

			Dim buttonElement As RadDropDownButtonElement = TryCast(Me.Owner, RadDropDownButtonElement)

			If buttonElement IsNot Nothing Then
				buttonElement.DropDownMenu.ClosePopup(RadPopupCloseReason.Mouse)
				Me.ResetVisuals()
			End If
		End Sub

		Private Sub ResetVisuals()
			Me.header.Text = RadMenuInsertTableItem.HeaderText

			For Each element As LightVisualElement In Me.uniformGrid.Children
				element.ResetValue(LightVisualElement.BorderColorProperty, ValueResetFlags.Local)
				element.ResetValue(LightVisualElement.BorderInnerColorProperty, ValueResetFlags.Local)
			Next
		End Sub

		Private Sub OnBoxMouseMove(sender As Object, e As MouseEventArgs)
			Me.SelectBoxes(e.Location)
		End Sub

		Protected Overrides Sub OnMouseMove(e As MouseEventArgs)
			MyBase.OnMouseMove(e)
			Me.SelectBoxes(e.Location)
		End Sub

		Private Sub OnBoxMouseDown(sender As Object, e As MouseEventArgs)
			Me.OnSelectionChanged(New TableSelectionChangedEventArgs(currentColumnIndex, currentRowIndex))
		End Sub

		Protected Overrides Sub OnMouseDown(e As MouseEventArgs)
			MyBase.OnMouseDown(e)

			If e.Button = MouseButtons.Left Then
				Me.OnSelectionChanged(New TableSelectionChangedEventArgs(currentColumnIndex, currentRowIndex))
			End If
		End Sub

		Protected Overrides Sub OnPropertyChanged(e As RadPropertyChangedEventArgs)
			MyBase.OnPropertyChanged(e)

			If e.[Property] = LightVisualElement.ContainsMouseProperty Then
				If Not CType(e.NewValue, Boolean) Then
					Me.ResetVisuals()
				End If
			End If
		End Sub

		#endregion

		#region Methods

		Private Sub SelectBoxes(location As Point)
			Me.currentColumnIndex = -1
			Me.currentRowIndex = -1

			For Each element As LightVisualElement In Me.uniformGrid.Children
				Dim elementBounds As RectangleF = element.ControlBoundingRectangle

				If elementBounds.X <= location.X AndAlso elementBounds.Y <= location.Y Then
					Dim columnIndex As Integer = UniformGrid.GetColumnIndex(element)
					Dim rowIndex As Integer = UniformGrid.GetRowIndex(element)

					currentColumnIndex = Math.Max(currentColumnIndex, columnIndex)
					currentRowIndex = Math.Max(currentRowIndex, rowIndex)

					element.BorderColor = Color.DarkOrange
					element.BorderInnerColor = Color.OrangeRed
				Else
					element.ResetValue(LightVisualElement.BorderColorProperty, ValueResetFlags.Local)
					element.ResetValue(LightVisualElement.BorderInnerColorProperty, ValueResetFlags.Local)
				End If
			Next

			Dim text As String = RadMenuInsertTableItem.HeaderText

			If currentColumnIndex >= 0 AndAlso currentRowIndex >= 0 Then
				text = [String].Format("{0}x{1}", currentColumnIndex + 1, currentRowIndex + 1)
			End If

			Me.header.Text = text
		End Sub

		#endregion

		#region Layout

		Protected Overrides Function ArrangeOverride(finalSize As SizeF) As SizeF
			Dim menuLayout As RadDropDownMenuLayout = TryCast(Me.Parent, RadDropDownMenuLayout)
			Dim x As Single = Math.Max(menuLayout.LeftColumnWidth, menuLayout.LeftColumnMinWidth) + menuLayout.LeftColumnMaxPadding

			Dim size As SizeF = finalSize
			size.Width -= x

			Dim finalRect As New RectangleF(New PointF(x, 0), size)

			Me.stackLayout.Arrange(finalRect)
			Return finalSize
		End Function

		#endregion
	End Class
End Namespace