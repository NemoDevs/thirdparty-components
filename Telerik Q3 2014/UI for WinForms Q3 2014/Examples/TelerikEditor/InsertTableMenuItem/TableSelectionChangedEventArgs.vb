Imports System

Namespace TelerikEditor
	Public Class TableSelectionChangedEventArgs
		Inherits EventArgs
		Private rowIndex As Integer
		Private columnIndex As Integer

		Public Sub New(columnIndex As Integer, rowIndex As Integer)
			Me.rowIndex = rowIndex
			Me.columnIndex = columnIndex
		End Sub

		Public ReadOnly Property RowIndex() As Integer
			Get
				Return Me.rowIndex
			End Get
		End Property

		Public ReadOnly Property ColumnIndex() As Integer
			Get
				Return Me.columnIndex
			End Get
		End Property
	End Class
End Namespace