Imports System
Imports System.Diagnostics
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Windows.Forms

Namespace TelerikEditor
	Module Program

		Private  requiredString As String

		Friend Module NativeMethods
			<DllImport("user32.dll")> _
			Public  Function ShowWindowAsync(hWnd As IntPtr, nCmdShow As Integer) As Boolean
			End Function

			<DllImport("user32.dll")> _
			Public  Function SetForegroundWindow(hWnd As IntPtr) As Boolean
			End Function

			<DllImport("user32.dll")> _
			Public  Function EnumWindows(lpEnumFunc As EnumWindowsProcDel, lParam As Int32) As Boolean
			End Function

			<DllImport("user32.dll")> _
			Public  Function GetWindowThreadProcessId(hWnd As IntPtr, ByRef lpdwProcessId As Int32) As Integer
			End Function

			<DllImport("user32.dll")> _
			Public  Function GetWindowText(hWnd As IntPtr, lpString As StringBuilder, nMaxCount As Int32) As Integer
			End Function

			Public Const SW_SHOWNORMAL As Integer = 1
		End Class

		Public Delegate Function EnumWindowsProcDel(hWnd As IntPtr, lParam As Int32) As Boolean

		Private  Function EnumWindowsProc(hWnd As IntPtr, lParam As Int32) As Boolean
			Dim processId As Integer = 0
			NativeMethods.GetWindowThreadProcessId(hWnd, processId)

			Dim caption As New StringBuilder(1024)
			NativeMethods.GetWindowText(hWnd, caption, 1024)

			' Use IndexOf to make sure our required string is in the title.
			If processId = lParam AndAlso (caption.ToString().IndexOf(requiredString, StringComparison.OrdinalIgnoreCase) <> -1) Then
				' Restore the window.
				NativeMethods.ShowWindowAsync(hWnd, NativeMethods.SW_SHOWNORMAL)
				NativeMethods.SetForegroundWindow(hWnd)
			End If
			Return True
		End Function

		Public  Function ProcessExist(forceTitle As String) As Boolean
			requiredString = forceTitle
			For Each proc As Process In Process.GetProcessesByName(Application.ProductName)
				If proc.Id <> Process.GetCurrentProcess().Id Then
					NativeMethods.EnumWindows(New EnumWindowsProcDel(EnumWindowsProc), proc.Id)
					Return True
				End If
			Next

			Return False
		End Function


		''' <summary>
		''' The main entry point for the application.
		''' </summary>
		<STAThread> _
		 Sub Main(args As String())
			Dim singleInstance As Boolean = False
			Dim i As Integer = 0
			While i < args.Length
				If args(i).Equals("singleinstance", StringComparison.InvariantCultureIgnoreCase) Then
					singleInstance = True
					Exit While
				End If
				System.Math.Max(System.Threading.Interlocked.Increment(i),i - 1)
			End While

			If singleInstance Then
				If Not ProcessExist("Telerik Editor") Then
					Application.EnableVisualStyles()
					Application.SetCompatibleTextRenderingDefault(False)
					Application.Run(New MainForm(True))
				End If

				Return
			End If

			Application.EnableVisualStyles()
			Application.SetCompatibleTextRenderingDefault(False)
			Application.Run(New MainForm())
		End Sub
	End Class
End Namespace