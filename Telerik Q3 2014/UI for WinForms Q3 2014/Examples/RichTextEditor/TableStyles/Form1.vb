Imports Microsoft.VisualBasic
Imports System
Imports System.IO
Imports System.Linq
Imports Telerik.WinControls
Imports Telerik.WinControls.UI
Imports Telerik.WinForms.Documents.FormatProviders.Xaml
Imports Telerik.WinForms.Documents.Model

Namespace RichTextEditor.TableStyles
	Public Partial Class Form1
		Inherits RadRibbonForm
		Private Const SampleDocumentPath As String = "TableStylesGalleryDemo.xaml"

		Public Sub New()
			InitializeComponent()

			ThemeResolutionService.ApplicationThemeName = Program.ThemeName
		End Sub

        Protected Overrides Sub OnLoad(ByVal e As EventArgs)
            MyBase.OnLoad(e)
            Using stream As Stream = GetType(Form1).Assembly.GetManifestResourceStream(SampleDocumentPath)
                Me.radRichTextEditor1.Document = New XamlFormatProvider().Import(stream)
            End Using

            Dim table As Table = Me.radRichTextEditor1.Document.EnumerateChildrenOfType(Of Table)().FirstOrDefault()

            If Not table Is Nothing Then
                Me.radRichTextEditor1.Document.CaretPosition.MoveToStartOfDocumentElement(table)
            End If

            If Program.ThemeName = "VisualStudio2012Dark" OrElse Program.ThemeName = "HighContrastBlack" Then
                Me.radRichTextEditor1.Document.StyleRepository("Normal").SpanProperties.ForeColor = Telerik.WinControls.RichTextEditor.UI.Colors.White
            End If
        End Sub
	End Class
End Namespace
