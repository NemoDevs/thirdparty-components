﻿using System;
using System.IO;
using System.Linq;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinForms.Documents.FormatProviders.Xaml;
using Telerik.WinForms.Documents.Model;

namespace RichTextEditor.TableStyles
{
    public partial class Form1 : RadRibbonForm
    {
        private const string SampleDocumentPath = "TableStylesGalleryDemo.xaml";

        public Form1()
        {
            InitializeComponent();

            ThemeResolutionService.ApplicationThemeName = Program.ThemeName;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            using (Stream stream = typeof(Form1).Assembly.GetManifestResourceStream(String.Format("RichTextEditor.SampleDocuments.{0}", SampleDocumentPath)))
            {
                this.radRichTextEditor1.Document = new XamlFormatProvider().Import(stream);
            }

            Table table = this.radRichTextEditor1.Document.EnumerateChildrenOfType<Table>().FirstOrDefault();

            if (table != null)
            {
                this.radRichTextEditor1.Document.CaretPosition.MoveToStartOfDocumentElement(table);
            }

            if (Program.ThemeName == "VisualStudio2012Dark" || Program.ThemeName == "HighContrastBlack")
            {
                this.radRichTextEditor1.Document.StyleRepository["Normal"].SpanProperties.ForeColor = Telerik.WinControls.RichTextEditor.UI.Colors.White;
            }
        }
    }
}
