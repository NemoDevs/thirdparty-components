Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.IO
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Imports Telerik.WinControls
Imports Telerik.WinControls.Themes
Imports Telerik.WinControls.UI
Imports Telerik.WinForms.Documents.FormatProviders
Imports Telerik.WinForms.Documents.FormatProviders.OpenXml.Docx
Imports Telerik.WinForms.Documents.FormatProviders.Txt

Namespace RichTextEditor.FirstLook
	Public Partial Class Form1
		Inherits RadRibbonForm
		Public Sub New()
			InitializeComponent()
			Me.LoadFile("overview.docx")
		End Sub

        Private Sub LoadFile(ByVal file As String)
            ThemeResolutionService.ApplicationThemeName = Program.ThemeName
            Dim provider As DocumentFormatProviderBase = New DocxFormatProvider()

            Using stream As Stream = GetType(Form1).Assembly.GetManifestResourceStream(file)
                Me.radRichTextEditor1.Document = provider.Import(stream)
            End Using


            If Program.ThemeName = "VisualStudio2012Dark" OrElse Program.ThemeName = "HighContrastBlack" Then
                Me.radRichTextEditor1.Document.StyleRepository("Normal").SpanProperties.ForeColor = Telerik.WinControls.RichTextEditor.UI.Colors.White
            ElseIf Program.ThemeName = "TelerikMetro" OrElse Program.ThemeName = "TelerikMetroBlue" Then
                AddHandler Me.radRichTextEditor1.ProviderUILayerInitialized, AddressOf RichTextBoxElement_ProviderUILayerInitialized
            End If
        End Sub

        Private Sub RichTextBoxElement_ProviderUILayerInitialized(sender As Object, e As ProviderUILayerInitilizedEventArgs)
            If e.Layer.Name = "PagesLayer" Then
                For Each element As Telerik.WinControls.RichTextEditor.UI.UIElement In e.Container.Children
                    element.BackColor = Color.White
                Next
            End If
        End Sub

	End Class
End Namespace
