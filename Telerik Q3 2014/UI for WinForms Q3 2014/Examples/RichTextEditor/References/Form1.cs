﻿using System;
using System.IO;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.RichTextEditor.UI;
using Telerik.WinControls.UI;
using Telerik.WinForms.Documents.FormatProviders.Xaml;

namespace RichTextEditor.References
{
    public partial class Form1 : RadRibbonForm
    {
        private const string FootnoteDocumentPath = "ReferencesFootnote.xaml";
        private const string EndnoteDocumentPath = "ReferencesEndnote.xaml";
        private const string BibliographyDocumentPath = "ReferencesBibliography.xaml";

        public Form1()
        {
            InitializeComponent();

            RibbonTab referencesTab = (RibbonTab)this.richTextEditorRibbonBar1.CommandTabs[3];

            RadRibbonBarButtonGroup group = new RadRibbonBarButtonGroup();
            group.Orientation = Orientation.Vertical;
            group.ShowBackColor = false;
            group.ShowBorder = false;

            group.Items.Add(this.CreateButton("Footnotes", LoadFootnotes));
            group.Items.Add(this.CreateButton("EndNotes", LoadEndnotes));
            group.Items.Add(this.CreateButton("Citations && Bibliography", LoadBibliography));

            referencesTab.Items.Add(group);

            RadRibbonBarGroup ribbonGroup = new RadRibbonBarGroup();
            ribbonGroup.Margin = new System.Windows.Forms.Padding(-1, -1, 0, -1);
            ribbonGroup.Orientation = System.Windows.Forms.Orientation.Vertical;
            ribbonGroup.Text = "Load document";
            ribbonGroup.Items.Add(group);

            referencesTab.Items.Insert(0, ribbonGroup);

            ThemeResolutionService.ApplicationThemeName = Program.ThemeName;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            LoadDocument(FootnoteDocumentPath);
        }

        private RadButtonElement CreateButton(string text, EventHandler OnClick)
        {
            RadButtonElement button = new RadButtonElement();
            button.DisplayStyle = DisplayStyle.Text;
            button.MinSize = new System.Drawing.Size(20, 20);
            button.Text = text;
            button.ShowBorder = false;
            button.Click += OnClick;

            return button;
        }

        private void LoadFootnotes(object sender, EventArgs e)
        {
            LoadDocument(FootnoteDocumentPath);
        }

        private void LoadEndnotes(object sender, EventArgs e)
        {
            LoadDocument(EndnoteDocumentPath);
        }

        private void LoadBibliography(object sender, EventArgs e)
        {
            LoadDocument(BibliographyDocumentPath);
        }

        private void LoadDocument(string path)
        {
            using (Stream stream = typeof(Form1).Assembly.GetManifestResourceStream(String.Format("RichTextEditor.SampleDocuments.{0}", path)))
            {
                this.radRichTextEditor1.Document = new XamlFormatProvider().Import(stream);
            }

            this.richTextEditorRibbonBar1.RibbonBarElement.TabStripElement.SelectedItem = this.richTextEditorRibbonBar1.RibbonBarElement.TabStripElement.Items[3];

            if (Program.ThemeName == "VisualStudio2012Dark" || Program.ThemeName == "HighContrastBlack")
            {
                this.radRichTextEditor1.Document.StyleRepository["Normal"].SpanProperties.ForeColor = Colors.White;
            }
            else if (Program.ThemeName == "TelerikMetro" || Program.ThemeName == "TelerikMetroBlue")
            {
                this.radRichTextEditor1.ProviderUILayerInitialized += RichTextBoxElement_ProviderUILayerInitialized;
            }
        }

        void RichTextBoxElement_ProviderUILayerInitialized(object sender, ProviderUILayerInitilizedEventArgs e)
        {
            if (e.Layer.Name == "PagesLayer")
            {
                foreach (Telerik.WinControls.RichTextEditor.UI.UIElement element in e.Container.Children)
                {
                    element.BackColor = Colors.White;
                }
            }
        }
    }
}
