Imports System
Imports System.Drawing
Imports System.IO
Imports System.Linq
Imports Telerik.WinControls
Imports Telerik.WinControls.UI
Imports Telerik.WinForms.Documents.FormatProviders.Xaml
Imports Telerik.WinForms.Documents.Model
Imports RichTextEditor.SampleData

Namespace RichTextEditor.DocumentProtection
    Partial Public Class Form1
        Inherits RadRibbonForm
        Private Const SampleDocumentPath As String = "RadRichTextBoxProtection.xaml"
        Private comboBoxLoggedUser As RadDropDownListElement

        Public Sub New()
            InitializeComponent()

            ThemeResolutionService.ApplicationThemeName = Program.ThemeName

            Dim dataContext As ExamplesDataContext = New ExamplesDataContext()

            Me.radRichTextEditor1.Users = dataContext.Users

            Dim reviewTab As RibbonTab = CType(Me.richTextEditorRibbonBar1.CommandTabs(Me.richTextEditorRibbonBar1.CommandTabs.Count - 2), RibbonTab)
            reviewTab.Items(0).Visibility = Telerik.WinControls.ElementVisibility.Collapsed
            reviewTab.Items(1).Visibility = Telerik.WinControls.ElementVisibility.Collapsed
            reviewTab.Items(2).Visibility = Telerik.WinControls.ElementVisibility.Collapsed

            Me.comboBoxLoggedUser = New RadDropDownListElement()
            CType(reviewTab.Items.Last, RadRibbonBarGroup).Items.Add(Me.comboBoxLoggedUser)
            Me.comboBoxLoggedUser.DataSource = dataContext.CurrentUsers
            Me.comboBoxLoggedUser.DisplayMember = "RealName"
            Me.comboBoxLoggedUser.SelectedIndex = 0
            Me.radRichTextEditor1.CurrentUser = TryCast(Me.comboBoxLoggedUser.SelectedValue, UserInfo)
            Me.comboBoxLoggedUser.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            Me.comboBoxLoggedUser.Alignment = ContentAlignment.MiddleCenter
            AddHandler Me.comboBoxLoggedUser.SelectedValueChanged, AddressOf comboBoxLoggedUser_SelectedValueChanged
        End Sub

        Private Sub comboBoxLoggedUser_SelectedValueChanged(ByVal sender As Object, ByVal e As Telerik.WinControls.UI.Data.ValueChangedEventArgs)
            Me.radRichTextEditor1.CurrentUser = TryCast(Me.comboBoxLoggedUser.SelectedValue, UserInfo)
        End Sub
       
        Protected Overrides Sub OnLoad(ByVal e As EventArgs)
            MyBase.OnLoad(e)
            Using stream As Stream = GetType(Form1).Assembly.GetManifestResourceStream(SampleDocumentPath)
                Me.radRichTextEditor1.Document = New XamlFormatProvider().Import(stream)
            End Using

            If Program.ThemeName = "VisualStudio2012Dark" OrElse Program.ThemeName = "HighContrastBlack" Then
                Me.radRichTextEditor1.Document.StyleRepository("Normal").SpanProperties.ForeColor = Telerik.WinControls.RichTextEditor.UI.Colors.White
            End If
        End Sub
    End Class
End Namespace
