﻿namespace BugTracker
{
    partial class RadRibbonForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RadRibbonForm1));
            this.radRibbonBar1 = new Telerik.WinControls.UI.RadRibbonBar();
            this.homeTab = new Telerik.WinControls.UI.RibbonTab();
            this.databaseGroup = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.loadDatabaseButton = new Telerik.WinControls.UI.RadButtonElement();
            this.saveDatabaseButton = new Telerik.WinControls.UI.RadButtonElement();
            this.editGroup = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.addButton = new Telerik.WinControls.UI.RadButtonElement();
            this.removeButton = new Telerik.WinControls.UI.RadButtonElement();
            this.editButton = new Telerik.WinControls.UI.RadButtonElement();
            this.viewGroup = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.sortDropDownButton = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.sortNoneMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.sortDateMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.dateAscendingMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.dateDescendingMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.sortStatusMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.statusAscendingMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.statusDescendingMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.sortOwnerMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.ownerAscendingMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.ownerDescendingMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radRibbonBarButtonGroup1 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radRibbonBarButtonGroup2 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.openCheckBox = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.readyForTestCheckBox = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.doneCheckBox = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radRibbonBarButtonGroup3 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.unassignedCheckBox = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.newCheckBox = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.viewTab = new Telerik.WinControls.UI.RibbonTab();
            this.layoutGroup = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.loadLayoutButton = new Telerik.WinControls.UI.RadButtonElement();
            this.saveLayoutButton = new Telerik.WinControls.UI.RadButtonElement();
            this.panelsGroup = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.projectsButton = new Telerik.WinControls.UI.RadButtonElement();
            this.detailsButton = new Telerik.WinControls.UI.RadButtonElement();
            this.bugsButton = new Telerik.WinControls.UI.RadButtonElement();
            this.featuresButton = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup1 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radGalleryElement1 = new Telerik.WinControls.UI.RadGalleryElement();
            this.office2010BlackButton = new Telerik.WinControls.UI.RadGalleryItem();
            this.office2010BlueButton = new Telerik.WinControls.UI.RadGalleryItem();
            this.office2010SilverButton = new Telerik.WinControls.UI.RadGalleryItem();
            this.controlDefaultButton = new Telerik.WinControls.UI.RadGalleryItem();
            this.telerikButton = new Telerik.WinControls.UI.RadGalleryItem();
            this.loadDatabaseMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.addMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.removeMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.editMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem2 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.aboutMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.exitMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radDock1 = new Telerik.WinControls.UI.Docking.RadDock();
            this.projectsToolWindow = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.projectsTreeView = new Telerik.WinControls.UI.RadTreeView();
            this.toolTabStrip3 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.documentContainer1 = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.documentTabStrip2 = new Telerik.WinControls.UI.Docking.DocumentTabStrip();
            this.bugDocumentWindow = new Telerik.WinControls.UI.Docking.DocumentWindow();
            this.bugsGrid = new Telerik.WinControls.UI.RadGridView();
            this.featureDocumentWindow = new Telerik.WinControls.UI.Docking.DocumentWindow();
            this.featuresGrid = new Telerik.WinControls.UI.RadGridView();
            this.toolTabStrip2 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.detailsToolWindow = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.detailsPageView = new Telerik.WinControls.UI.RadPageView();
            this.generalPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.ownerDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.ownerLabel = new Telerik.WinControls.UI.RadLabel();
            this.idSpinEditor = new Telerik.WinControls.UI.RadSpinEditor();
            this.priorityDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.dateDateTimePicker = new Telerik.WinControls.UI.RadDateTimePicker();
            this.statusDropDownList = new Telerik.WinControls.UI.RadDropDownList();
            this.titleTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.priorityLabel = new Telerik.WinControls.UI.RadLabel();
            this.dateLabel = new Telerik.WinControls.UI.RadLabel();
            this.statusLabel = new Telerik.WinControls.UI.RadLabel();
            this.titleLabel = new Telerik.WinControls.UI.RadLabel();
            this.idLabel = new Telerik.WinControls.UI.RadLabel();
            this.descriptionPageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.descriptionTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.stepsToReproducePageViewPage = new Telerik.WinControls.UI.RadPageViewPage();
            this.stepsToReproduceTextBox = new Telerik.WinControls.UI.RadTextBox();
            this.office2010BlackTheme1 = new Telerik.WinControls.Themes.Office2010BlackTheme();
            this.office2010BlueTheme1 = new Telerik.WinControls.Themes.Office2010BlueTheme();
            this.office2010SilverTheme1 = new Telerik.WinControls.Themes.Office2010SilverTheme();
            this.telerikMetroTheme1 = new Telerik.WinControls.Themes.TelerikMetroTheme();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDock1)).BeginInit();
            this.radDock1.SuspendLayout();
            this.projectsToolWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.projectsTreeView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip3)).BeginInit();
            this.toolTabStrip3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).BeginInit();
            this.documentContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentTabStrip2)).BeginInit();
            this.documentTabStrip2.SuspendLayout();
            this.bugDocumentWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bugsGrid)).BeginInit();
            this.featureDocumentWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.featuresGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip2)).BeginInit();
            this.toolTabStrip2.SuspendLayout();
            this.detailsToolWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detailsPageView)).BeginInit();
            this.detailsPageView.SuspendLayout();
            this.generalPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ownerDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ownerLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.idSpinEditor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.priorityDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateDateTimePicker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusDropDownList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.titleTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.priorityLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.titleLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.idLabel)).BeginInit();
            this.descriptionPageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.descriptionTextBox)).BeginInit();
            this.stepsToReproducePageViewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stepsToReproduceTextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radRibbonBar1
            // 
            this.radRibbonBar1.AutoSize = true;
            this.radRibbonBar1.CommandTabs.AddRange(new Telerik.WinControls.RadItem[] {
            this.homeTab,
            this.viewTab});
            this.radRibbonBar1.Dock = System.Windows.Forms.DockStyle.Top;
            // 
            // 
            // 
            this.radRibbonBar1.ExitButton.AccessibleDescription = "Exit";
            this.radRibbonBar1.ExitButton.AccessibleName = "Exit";
            // 
            // 
            // 
            this.radRibbonBar1.ExitButton.ButtonElement.AccessibleDescription = "Exit";
            this.radRibbonBar1.ExitButton.ButtonElement.AccessibleName = "Exit";
            this.radRibbonBar1.ExitButton.ButtonElement.Class = "RadMenuButtonElement";
            this.radRibbonBar1.ExitButton.ButtonElement.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radRibbonBar1.ExitButton.Text = "Exit";
            this.radRibbonBar1.Location = new System.Drawing.Point(0, 0);
            this.radRibbonBar1.Name = "radRibbonBar1";
            // 
            // 
            // 
            this.radRibbonBar1.OptionsButton.AccessibleDescription = "Options";
            this.radRibbonBar1.OptionsButton.AccessibleName = "Options";
            // 
            // 
            // 
            this.radRibbonBar1.OptionsButton.ButtonElement.AccessibleDescription = "Options";
            this.radRibbonBar1.OptionsButton.ButtonElement.AccessibleName = "Options";
            this.radRibbonBar1.OptionsButton.ButtonElement.Class = "RadMenuButtonElement";
            this.radRibbonBar1.OptionsButton.ButtonElement.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radRibbonBar1.OptionsButton.Text = "Options";
            // 
            // 
            // 
            this.radRibbonBar1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radRibbonBar1.Size = new System.Drawing.Size(1008, 154);
            this.radRibbonBar1.StartButtonImage = null;
            this.radRibbonBar1.StartMenuItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.loadDatabaseMenuItem,
            this.radMenuSeparatorItem1,
            this.addMenuItem,
            this.removeMenuItem,
            this.editMenuItem,
            this.radMenuSeparatorItem2,
            this.aboutMenuItem,
            this.exitMenuItem});
            this.radRibbonBar1.TabIndex = 0;
            this.radRibbonBar1.Text = "Bug Tracker";
            // 
            // homeTab
            // 
            this.homeTab.AccessibleDescription = "Home";
            this.homeTab.AccessibleName = "Home";
            this.homeTab.Alignment = System.Drawing.ContentAlignment.BottomLeft;
            this.homeTab.Class = "RibbonTab";
            this.homeTab.IsSelected = true;
            this.homeTab.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.databaseGroup,
            this.editGroup,
            this.viewGroup});
            this.homeTab.Name = "homeTab";
            this.homeTab.StretchHorizontally = false;
            this.homeTab.Text = "Home";
            this.homeTab.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // databaseGroup
            // 
            this.databaseGroup.AccessibleDescription = "Database";
            this.databaseGroup.AccessibleName = "Database";
            this.databaseGroup.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.loadDatabaseButton,
            this.saveDatabaseButton});
            this.databaseGroup.Name = "databaseGroup";
            this.databaseGroup.Text = "Database";
            this.databaseGroup.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // loadDatabaseButton
            // 
            this.loadDatabaseButton.AccessibleDescription = "Load";
            this.loadDatabaseButton.AccessibleName = "Load";
            this.loadDatabaseButton.Class = "RibbonBarButtonElement";
            this.loadDatabaseButton.Image = global::BugTracker.Properties.Resources.load;
            this.loadDatabaseButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.loadDatabaseButton.MinSize = new System.Drawing.Size(48, 0);
            this.loadDatabaseButton.Name = "loadDatabaseButton";
            this.loadDatabaseButton.Text = "Save";
            this.loadDatabaseButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.loadDatabaseButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.loadDatabaseButton.Click += new System.EventHandler(this.loadDatabaseButton_Click);
            // 
            // saveDatabaseButton
            // 
            this.saveDatabaseButton.AccessibleDescription = "Save";
            this.saveDatabaseButton.AccessibleName = "Save";
            this.saveDatabaseButton.Class = "RibbonBarButtonElement";
            this.saveDatabaseButton.Image = global::BugTracker.Properties.Resources.save;
            this.saveDatabaseButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.saveDatabaseButton.MinSize = new System.Drawing.Size(48, 0);
            this.saveDatabaseButton.Name = "saveDatabaseButton";
            this.saveDatabaseButton.Text = "Save";
            this.saveDatabaseButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.saveDatabaseButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.saveDatabaseButton.Click += new System.EventHandler(this.saveDatabaseButton_Click);
            // 
            // editGroup
            // 
            this.editGroup.AccessibleDescription = "Edit";
            this.editGroup.AccessibleName = "Edit";
            this.editGroup.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.addButton,
            this.removeButton,
            this.editButton});
            this.editGroup.Name = "editGroup";
            this.editGroup.Text = "Edit";
            this.editGroup.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // addButton
            // 
            this.addButton.AccessibleDescription = "Add";
            this.addButton.AccessibleName = "Add";
            this.addButton.Class = "RibbonBarButtonElement";
            this.addButton.Image = global::BugTracker.Properties.Resources.add;
            this.addButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.addButton.MinSize = new System.Drawing.Size(48, 0);
            this.addButton.Name = "addButton";
            this.addButton.Text = "Add";
            this.addButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.addButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.AccessibleDescription = "Remove";
            this.removeButton.AccessibleName = "Remove";
            this.removeButton.Class = "RibbonBarButtonElement";
            this.removeButton.Image = global::BugTracker.Properties.Resources.remove;
            this.removeButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.removeButton.MinSize = new System.Drawing.Size(48, 0);
            this.removeButton.Name = "removeButton";
            this.removeButton.Text = "Remove";
            this.removeButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.removeButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // editButton
            // 
            this.editButton.AccessibleDescription = "Edit";
            this.editButton.AccessibleName = "Edit";
            this.editButton.Class = "RibbonBarButtonElement";
            this.editButton.Image = global::BugTracker.Properties.Resources.edit;
            this.editButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.editButton.MinSize = new System.Drawing.Size(48, 0);
            this.editButton.Name = "editButton";
            this.editButton.Text = "Edit";
            this.editButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.editButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // viewGroup
            // 
            this.viewGroup.AccessibleDescription = "View";
            this.viewGroup.AccessibleName = "View";
            this.viewGroup.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.sortDropDownButton,
            this.radRibbonBarButtonGroup1});
            this.viewGroup.Margin = new System.Windows.Forms.Padding(0);
            this.viewGroup.MaxSize = new System.Drawing.Size(0, 0);
            this.viewGroup.MinSize = new System.Drawing.Size(0, 0);
            this.viewGroup.Name = "viewGroup";
            this.viewGroup.Text = "View";
            this.viewGroup.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // sortDropDownButton
            // 
            this.sortDropDownButton.AccessibleDescription = "Sort By";
            this.sortDropDownButton.AccessibleName = "Sort By";
            this.sortDropDownButton.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.sortDropDownButton.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.sortDropDownButton.ExpandArrowButton = false;
            this.sortDropDownButton.Image = global::BugTracker.Properties.Resources.sort;
            this.sortDropDownButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.sortDropDownButton.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.sortNoneMenuItem,
            this.sortDateMenuItem,
            this.sortStatusMenuItem,
            this.sortOwnerMenuItem});
            this.sortDropDownButton.Name = "sortDropDownButton";
            this.sortDropDownButton.Text = "Sort By";
            this.sortDropDownButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sortDropDownButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // sortNoneMenuItem
            // 
            this.sortNoneMenuItem.AccessibleDescription = "None";
            this.sortNoneMenuItem.AccessibleName = "None";
            this.sortNoneMenuItem.Name = "sortNoneMenuItem";
            this.sortNoneMenuItem.Text = "None";
            this.sortNoneMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // sortDateMenuItem
            // 
            this.sortDateMenuItem.AccessibleDescription = "Date";
            this.sortDateMenuItem.AccessibleName = "Date";
            this.sortDateMenuItem.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dateAscendingMenuItem,
            this.dateDescendingMenuItem});
            this.sortDateMenuItem.Name = "sortDateMenuItem";
            this.sortDateMenuItem.Text = "Date";
            this.sortDateMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // dateAscendingMenuItem
            // 
            this.dateAscendingMenuItem.AccessibleDescription = "Ascending";
            this.dateAscendingMenuItem.AccessibleName = "Ascending";
            this.dateAscendingMenuItem.Name = "dateAscendingMenuItem";
            this.dateAscendingMenuItem.Text = "Ascending";
            this.dateAscendingMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // dateDescendingMenuItem
            // 
            this.dateDescendingMenuItem.AccessibleDescription = "Descending";
            this.dateDescendingMenuItem.AccessibleName = "Descending";
            this.dateDescendingMenuItem.Name = "dateDescendingMenuItem";
            this.dateDescendingMenuItem.Text = "Descending";
            this.dateDescendingMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // sortStatusMenuItem
            // 
            this.sortStatusMenuItem.AccessibleDescription = "Status";
            this.sortStatusMenuItem.AccessibleName = "Status";
            this.sortStatusMenuItem.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.statusAscendingMenuItem,
            this.statusDescendingMenuItem});
            this.sortStatusMenuItem.Name = "sortStatusMenuItem";
            this.sortStatusMenuItem.Text = "Status";
            this.sortStatusMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // statusAscendingMenuItem
            // 
            this.statusAscendingMenuItem.AccessibleDescription = "Ascending";
            this.statusAscendingMenuItem.AccessibleName = "Ascending";
            this.statusAscendingMenuItem.Name = "statusAscendingMenuItem";
            this.statusAscendingMenuItem.Text = "Ascending";
            this.statusAscendingMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // statusDescendingMenuItem
            // 
            this.statusDescendingMenuItem.AccessibleDescription = "Descending";
            this.statusDescendingMenuItem.AccessibleName = "Descending";
            this.statusDescendingMenuItem.Name = "statusDescendingMenuItem";
            this.statusDescendingMenuItem.Text = "Descending";
            this.statusDescendingMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // sortOwnerMenuItem
            // 
            this.sortOwnerMenuItem.AccessibleDescription = "Owner";
            this.sortOwnerMenuItem.AccessibleName = "Owner";
            this.sortOwnerMenuItem.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.ownerAscendingMenuItem,
            this.ownerDescendingMenuItem});
            this.sortOwnerMenuItem.Name = "sortOwnerMenuItem";
            this.sortOwnerMenuItem.Text = "Owner";
            this.sortOwnerMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ownerAscendingMenuItem
            // 
            this.ownerAscendingMenuItem.AccessibleDescription = "Ascending";
            this.ownerAscendingMenuItem.AccessibleName = "Ascending";
            this.ownerAscendingMenuItem.Name = "ownerAscendingMenuItem";
            this.ownerAscendingMenuItem.Text = "Ascending";
            this.ownerAscendingMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ownerDescendingMenuItem
            // 
            this.ownerDescendingMenuItem.AccessibleDescription = "Descending";
            this.ownerDescendingMenuItem.AccessibleName = "Descending";
            this.ownerDescendingMenuItem.Name = "ownerDescendingMenuItem";
            this.ownerDescendingMenuItem.Text = "Descending";
            this.ownerDescendingMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarButtonGroup1
            // 
            this.radRibbonBarButtonGroup1.AccessibleDescription = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.AccessibleName = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.AutoSize = true;
            this.radRibbonBarButtonGroup1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radRibbonBarButtonGroup1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup2,
            this.radRibbonBarButtonGroup3});
            this.radRibbonBarButtonGroup1.MaxSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarButtonGroup1.Name = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.radRibbonBarButtonGroup1.StretchVertically = true;
            this.radRibbonBarButtonGroup1.Text = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarButtonGroup2
            // 
            this.radRibbonBarButtonGroup2.AccessibleDescription = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.AccessibleName = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.openCheckBox,
            this.readyForTestCheckBox,
            this.doneCheckBox});
            this.radRibbonBarButtonGroup2.Name = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup2.StretchVertically = true;
            this.radRibbonBarButtonGroup2.Text = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // openCheckBox
            // 
            this.openCheckBox.AccessibleDescription = "Open";
            this.openCheckBox.AccessibleName = "Open";
            this.openCheckBox.Checked = false;
            this.openCheckBox.Name = "openCheckBox";
            this.openCheckBox.Text = "Open";
            this.openCheckBox.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // readyForTestCheckBox
            // 
            this.readyForTestCheckBox.AccessibleDescription = "Ready For Test";
            this.readyForTestCheckBox.AccessibleName = "Ready For Test";
            this.readyForTestCheckBox.Checked = false;
            this.readyForTestCheckBox.Name = "readyForTestCheckBox";
            this.readyForTestCheckBox.Text = "Ready For Test";
            this.readyForTestCheckBox.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // doneCheckBox
            // 
            this.doneCheckBox.AccessibleDescription = "Done";
            this.doneCheckBox.AccessibleName = "Done";
            this.doneCheckBox.Checked = false;
            this.doneCheckBox.Name = "doneCheckBox";
            this.doneCheckBox.Text = "Done";
            this.doneCheckBox.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarButtonGroup3
            // 
            this.radRibbonBarButtonGroup3.AccessibleDescription = "radRibbonBarButtonGroup3";
            this.radRibbonBarButtonGroup3.AccessibleName = "radRibbonBarButtonGroup3";
            this.radRibbonBarButtonGroup3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.unassignedCheckBox,
            this.newCheckBox});
            this.radRibbonBarButtonGroup3.Name = "radRibbonBarButtonGroup3";
            this.radRibbonBarButtonGroup3.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup3.StretchVertically = true;
            this.radRibbonBarButtonGroup3.Text = "radRibbonBarButtonGroup3";
            this.radRibbonBarButtonGroup3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // unassignedCheckBox
            // 
            this.unassignedCheckBox.AccessibleDescription = "Unassigned";
            this.unassignedCheckBox.AccessibleName = "Unassigned";
            this.unassignedCheckBox.Checked = false;
            this.unassignedCheckBox.Name = "unassignedCheckBox";
            this.unassignedCheckBox.Text = "Unassigned";
            this.unassignedCheckBox.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // newCheckBox
            // 
            this.newCheckBox.AccessibleDescription = "New";
            this.newCheckBox.AccessibleName = "New";
            this.newCheckBox.Checked = false;
            this.newCheckBox.Name = "newCheckBox";
            this.newCheckBox.Text = "New";
            this.newCheckBox.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // viewTab
            // 
            this.viewTab.AccessibleDescription = "View";
            this.viewTab.AccessibleName = "View";
            this.viewTab.Alignment = System.Drawing.ContentAlignment.BottomLeft;
            this.viewTab.Class = "RibbonTab";
            this.viewTab.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.layoutGroup,
            this.panelsGroup,
            this.radRibbonBarGroup1});
            this.viewTab.Name = "viewTab";
            this.viewTab.StretchHorizontally = false;
            this.viewTab.Text = "View";
            this.viewTab.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // layoutGroup
            // 
            this.layoutGroup.AccessibleDescription = "Layout";
            this.layoutGroup.AccessibleName = "Layout";
            this.layoutGroup.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.loadLayoutButton,
            this.saveLayoutButton});
            this.layoutGroup.Name = "layoutGroup";
            this.layoutGroup.Text = "Layout";
            this.layoutGroup.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // loadLayoutButton
            // 
            this.loadLayoutButton.AccessibleDescription = "Load";
            this.loadLayoutButton.AccessibleName = "Load";
            this.loadLayoutButton.Class = "RibbonBarButtonElement";
            this.loadLayoutButton.Image = global::BugTracker.Properties.Resources.layout_load;
            this.loadLayoutButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.loadLayoutButton.MinSize = new System.Drawing.Size(48, 0);
            this.loadLayoutButton.Name = "loadLayoutButton";
            this.loadLayoutButton.Text = "Load";
            this.loadLayoutButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.loadLayoutButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.loadLayoutButton.Click += new System.EventHandler(this.loadLayoutButton_Click);
            // 
            // saveLayoutButton
            // 
            this.saveLayoutButton.AccessibleDescription = "Save";
            this.saveLayoutButton.AccessibleName = "Save";
            this.saveLayoutButton.Class = "RibbonBarButtonElement";
            this.saveLayoutButton.Image = global::BugTracker.Properties.Resources.layout_save;
            this.saveLayoutButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.saveLayoutButton.MinSize = new System.Drawing.Size(48, 0);
            this.saveLayoutButton.Name = "saveLayoutButton";
            this.saveLayoutButton.Text = "Save";
            this.saveLayoutButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.saveLayoutButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.saveLayoutButton.Click += new System.EventHandler(this.saveLayoutButton_Click);
            // 
            // panelsGroup
            // 
            this.panelsGroup.AccessibleDescription = "Panels";
            this.panelsGroup.AccessibleName = "Panels";
            this.panelsGroup.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.projectsButton,
            this.detailsButton,
            this.bugsButton,
            this.featuresButton});
            this.panelsGroup.Name = "panelsGroup";
            this.panelsGroup.Text = "Panels";
            this.panelsGroup.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // projectsButton
            // 
            this.projectsButton.AccessibleDescription = "Projects";
            this.projectsButton.AccessibleName = "Projects";
            this.projectsButton.Class = "RibbonBarButtonElement";
            this.projectsButton.Image = global::BugTracker.Properties.Resources.panels;
            this.projectsButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.projectsButton.MinSize = new System.Drawing.Size(48, 0);
            this.projectsButton.Name = "projectsButton";
            this.projectsButton.Text = "Projects";
            this.projectsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.projectsButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // detailsButton
            // 
            this.detailsButton.AccessibleDescription = "Details";
            this.detailsButton.AccessibleName = "Details";
            this.detailsButton.Class = "RibbonBarButtonElement";
            this.detailsButton.Image = global::BugTracker.Properties.Resources.panels;
            this.detailsButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.detailsButton.MinSize = new System.Drawing.Size(48, 0);
            this.detailsButton.Name = "detailsButton";
            this.detailsButton.Text = "Details";
            this.detailsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.detailsButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // bugsButton
            // 
            this.bugsButton.AccessibleDescription = "Bugs";
            this.bugsButton.AccessibleName = "Bugs";
            this.bugsButton.Class = "RibbonBarButtonElement";
            this.bugsButton.Image = global::BugTracker.Properties.Resources.panels;
            this.bugsButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.bugsButton.MinSize = new System.Drawing.Size(48, 0);
            this.bugsButton.Name = "bugsButton";
            this.bugsButton.Text = "Bugs";
            this.bugsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.bugsButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // featuresButton
            // 
            this.featuresButton.AccessibleDescription = "Features";
            this.featuresButton.AccessibleName = "Features";
            this.featuresButton.Class = "RibbonBarButtonElement";
            this.featuresButton.Image = global::BugTracker.Properties.Resources.panels;
            this.featuresButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.featuresButton.MinSize = new System.Drawing.Size(48, 0);
            this.featuresButton.Name = "featuresButton";
            this.featuresButton.Text = "Features";
            this.featuresButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.featuresButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup1
            // 
            this.radRibbonBarGroup1.AccessibleDescription = "Themes";
            this.radRibbonBarGroup1.AccessibleName = "Themes";
            this.radRibbonBarGroup1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radGalleryElement1});
            this.radRibbonBarGroup1.Name = "radRibbonBarGroup1";
            this.radRibbonBarGroup1.Text = "Themes";
            this.radRibbonBarGroup1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGalleryElement1
            // 
            this.radGalleryElement1.AccessibleDescription = "radGalleryElement1";
            this.radGalleryElement1.AccessibleName = "radGalleryElement1";
            this.radGalleryElement1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.office2010BlackButton,
            this.office2010BlueButton,
            this.office2010SilverButton,
            this.controlDefaultButton,
            this.telerikButton});
            this.radGalleryElement1.Name = "radGalleryElement1";
            this.radGalleryElement1.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            this.radGalleryElement1.Text = "radGalleryElement1";
            this.radGalleryElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // office2010BlackButton
            // 
            this.office2010BlackButton.AccessibleDescription = "Office2010Black";
            this.office2010BlackButton.AccessibleName = "Office2010Black";
            this.office2010BlackButton.DescriptionText = "";
            this.office2010BlackButton.Image = global::BugTracker.Properties.Resources.Office2010Black;
            this.office2010BlackButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.office2010BlackButton.Name = "office2010BlackButton";
            this.office2010BlackButton.StretchHorizontally = false;
            this.office2010BlackButton.StretchVertically = false;
            this.office2010BlackButton.Text = "Office2010Black";
            this.office2010BlackButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.office2010BlackButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // office2010BlueButton
            // 
            this.office2010BlueButton.AccessibleDescription = "Office2010Blue";
            this.office2010BlueButton.AccessibleName = "Office2010Blue";
            this.office2010BlueButton.DescriptionText = "";
            this.office2010BlueButton.Image = global::BugTracker.Properties.Resources.Office2010Blue;
            this.office2010BlueButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.office2010BlueButton.Name = "office2010BlueButton";
            this.office2010BlueButton.StretchHorizontally = false;
            this.office2010BlueButton.StretchVertically = false;
            this.office2010BlueButton.Text = "Office2010Blue";
            this.office2010BlueButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.office2010BlueButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // office2010SilverButton
            // 
            this.office2010SilverButton.AccessibleDescription = "Office2010Silver";
            this.office2010SilverButton.AccessibleName = "Office2010Silver";
            this.office2010SilverButton.DescriptionText = "";
            this.office2010SilverButton.Image = global::BugTracker.Properties.Resources.Office2010Silver;
            this.office2010SilverButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.office2010SilverButton.Name = "office2010SilverButton";
            this.office2010SilverButton.StretchHorizontally = false;
            this.office2010SilverButton.StretchVertically = false;
            this.office2010SilverButton.Text = "Office2010Silver";
            this.office2010SilverButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.office2010SilverButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // controlDefaultButton
            // 
            this.controlDefaultButton.AccessibleDescription = "ControlDefault";
            this.controlDefaultButton.AccessibleName = "ControlDefault";
            this.controlDefaultButton.DescriptionText = "";
            this.controlDefaultButton.Image = global::BugTracker.Properties.Resources.ControlDefault;
            this.controlDefaultButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.controlDefaultButton.Name = "controlDefaultButton";
            this.controlDefaultButton.StretchHorizontally = false;
            this.controlDefaultButton.StretchVertically = false;
            this.controlDefaultButton.Text = "ControlDefault";
            this.controlDefaultButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.controlDefaultButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // telerikButton
            // 
            this.telerikButton.AccessibleDescription = "TelerikMetro";
            this.telerikButton.AccessibleName = "TelerikMetro";
            this.telerikButton.DescriptionText = "";
            this.telerikButton.Image = global::BugTracker.Properties.Resources.Telerik;
            this.telerikButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.telerikButton.Name = "telerikButton";
            this.telerikButton.StretchHorizontally = false;
            this.telerikButton.StretchVertically = false;
            this.telerikButton.Text = "TelerikMetro";
            this.telerikButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.telerikButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // loadDatabaseMenuItem
            // 
            this.loadDatabaseMenuItem.AccessibleDescription = "Load Database";
            this.loadDatabaseMenuItem.AccessibleName = "Load Database";
            this.loadDatabaseMenuItem.Name = "loadDatabaseMenuItem";
            this.loadDatabaseMenuItem.Text = "Load Database";
            this.loadDatabaseMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem1
            // 
            this.radMenuSeparatorItem1.AccessibleDescription = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.AccessibleName = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Text = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // addMenuItem
            // 
            this.addMenuItem.AccessibleDescription = "Add item";
            this.addMenuItem.AccessibleName = "Add item";
            this.addMenuItem.Name = "addMenuItem";
            this.addMenuItem.Text = "Add item";
            this.addMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // removeMenuItem
            // 
            this.removeMenuItem.AccessibleDescription = "Remove item";
            this.removeMenuItem.AccessibleName = "Remove item";
            this.removeMenuItem.Name = "removeMenuItem";
            this.removeMenuItem.Text = "Remove item";
            this.removeMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // editMenuItem
            // 
            this.editMenuItem.AccessibleDescription = "Edit item";
            this.editMenuItem.AccessibleName = "Edit item";
            this.editMenuItem.Name = "editMenuItem";
            this.editMenuItem.Text = "Edit item";
            this.editMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem2
            // 
            this.radMenuSeparatorItem2.AccessibleDescription = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.AccessibleName = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Name = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Text = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // aboutMenuItem
            // 
            this.aboutMenuItem.AccessibleDescription = "About";
            this.aboutMenuItem.AccessibleName = "About";
            this.aboutMenuItem.Name = "aboutMenuItem";
            this.aboutMenuItem.Text = "About";
            this.aboutMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.AccessibleDescription = "Exit";
            this.exitMenuItem.AccessibleName = "Exit";
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.Text = "Exit";
            this.exitMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radDock1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 154);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1008, 558);
            this.panel1.TabIndex = 2;
            // 
            // radDock1
            // 
            this.radDock1.ActiveWindow = this.projectsToolWindow;
            this.radDock1.CausesValidation = false;
            this.radDock1.Controls.Add(this.toolTabStrip3);
            this.radDock1.Controls.Add(this.radSplitContainer2);
            this.radDock1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDock1.DocumentManager.DocumentInsertOrder = Telerik.WinControls.UI.Docking.DockWindowInsertOrder.InFront;
            this.radDock1.IsCleanUpTarget = true;
            this.radDock1.Location = new System.Drawing.Point(0, 0);
            this.radDock1.MainDocumentContainer = this.documentContainer1;
            this.radDock1.Name = "radDock1";
            this.radDock1.Padding = new System.Windows.Forms.Padding(5);
            // 
            // 
            // 
            this.radDock1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radDock1.RootElement.Padding = new System.Windows.Forms.Padding(5);
            this.radDock1.Size = new System.Drawing.Size(1008, 558);
            this.radDock1.SplitterWidth = 4;
            this.radDock1.TabIndex = 0;
            this.radDock1.TabStop = false;
            this.radDock1.Text = "radDock1";
            // 
            // projectsToolWindow
            // 
            this.projectsToolWindow.Caption = null;
            this.projectsToolWindow.Controls.Add(this.projectsTreeView);
            this.projectsToolWindow.Location = new System.Drawing.Point(1, 24);
            this.projectsToolWindow.Name = "projectsToolWindow";
            this.projectsToolWindow.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.projectsToolWindow.Size = new System.Drawing.Size(198, 522);
            this.projectsToolWindow.Text = "Projects";
            // 
            // projectsTreeView
            // 
            this.projectsTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.projectsTreeView.Location = new System.Drawing.Point(0, 0);
            this.projectsTreeView.Name = "projectsTreeView";
            this.projectsTreeView.Size = new System.Drawing.Size(198, 522);
            this.projectsTreeView.SpacingBetweenNodes = -1;
            this.projectsTreeView.TabIndex = 0;
            this.projectsTreeView.Text = "radTreeView1";
            // 
            // toolTabStrip3
            // 
            this.toolTabStrip3.CausesValidation = false;
            this.toolTabStrip3.Controls.Add(this.projectsToolWindow);
            this.toolTabStrip3.Location = new System.Drawing.Point(5, 5);
            this.toolTabStrip3.Name = "toolTabStrip3";
            // 
            // 
            // 
            this.toolTabStrip3.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.toolTabStrip3.SelectedIndex = 0;
            this.toolTabStrip3.Size = new System.Drawing.Size(200, 548);
            this.toolTabStrip3.TabIndex = 1;
            this.toolTabStrip3.TabStop = false;
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.documentContainer1);
            this.radSplitContainer2.Controls.Add(this.toolTabStrip2);
            this.radSplitContainer2.IsCleanUpTarget = true;
            this.radSplitContainer2.Location = new System.Drawing.Point(209, 5);
            this.radSplitContainer2.Name = "radSplitContainer2";
            this.radSplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer2.Size = new System.Drawing.Size(794, 548);
            this.radSplitContainer2.SplitterWidth = 4;
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            // 
            // documentContainer1
            // 
            this.documentContainer1.Controls.Add(this.documentTabStrip2);
            this.documentContainer1.Location = new System.Drawing.Point(0, 0);
            this.documentContainer1.Name = "documentContainer1";
            // 
            // 
            // 
            this.documentContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.documentContainer1.Size = new System.Drawing.Size(794, 383);
            this.documentContainer1.SizeInfo.AbsoluteSize = new System.Drawing.Size(200, 387);
            this.documentContainer1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            this.documentContainer1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 39);
            this.documentContainer1.SplitterWidth = 4;
            this.documentContainer1.TabIndex = 0;
            this.documentContainer1.TabStop = false;
            // 
            // documentTabStrip2
            // 
            this.documentTabStrip2.Controls.Add(this.bugDocumentWindow);
            this.documentTabStrip2.Controls.Add(this.featureDocumentWindow);
            this.documentTabStrip2.Location = new System.Drawing.Point(0, 0);
            this.documentTabStrip2.Name = "documentTabStrip2";
            // 
            // 
            // 
            this.documentTabStrip2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.documentTabStrip2.SelectedIndex = 0;
            this.documentTabStrip2.Size = new System.Drawing.Size(794, 383);
            this.documentTabStrip2.TabIndex = 0;
            this.documentTabStrip2.TabStop = false;
            // 
            // bugDocumentWindow
            // 
            this.bugDocumentWindow.Controls.Add(this.bugsGrid);
            this.bugDocumentWindow.Location = new System.Drawing.Point(6, 31);
            this.bugDocumentWindow.Name = "bugDocumentWindow";
            this.bugDocumentWindow.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.TabbedDocument;
            this.bugDocumentWindow.Size = new System.Drawing.Size(782, 346);
            this.bugDocumentWindow.Text = "Bugs";
            // 
            // bugsGrid
            // 
            this.bugsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bugsGrid.Location = new System.Drawing.Point(0, 0);
            this.bugsGrid.Name = "bugsGrid";
            this.bugsGrid.Size = new System.Drawing.Size(782, 346);
            this.bugsGrid.TabIndex = 0;
            this.bugsGrid.Text = "radGridView1";
            // 
            // featureDocumentWindow
            // 
            this.featureDocumentWindow.Controls.Add(this.featuresGrid);
            this.featureDocumentWindow.Location = new System.Drawing.Point(6, 30);
            this.featureDocumentWindow.Name = "featureDocumentWindow";
            this.featureDocumentWindow.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.TabbedDocument;
            this.featureDocumentWindow.Size = new System.Drawing.Size(782, 351);
            this.featureDocumentWindow.Text = "Features";
            // 
            // featuresGrid
            // 
            this.featuresGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.featuresGrid.Location = new System.Drawing.Point(0, 0);
            this.featuresGrid.Name = "featuresGrid";
            this.featuresGrid.Size = new System.Drawing.Size(782, 351);
            this.featuresGrid.TabIndex = 0;
            this.featuresGrid.Text = "radGridView1";
            // 
            // toolTabStrip2
            // 
            this.toolTabStrip2.Controls.Add(this.detailsToolWindow);
            this.toolTabStrip2.Location = new System.Drawing.Point(0, 387);
            this.toolTabStrip2.Name = "toolTabStrip2";
            // 
            // 
            // 
            this.toolTabStrip2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.toolTabStrip2.SelectedIndex = 0;
            this.toolTabStrip2.Size = new System.Drawing.Size(794, 161);
            this.toolTabStrip2.SizeInfo.AbsoluteSize = new System.Drawing.Size(200, 161);
            this.toolTabStrip2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -39);
            this.toolTabStrip2.TabIndex = 1;
            this.toolTabStrip2.TabStop = false;
            // 
            // detailsToolWindow
            // 
            this.detailsToolWindow.Caption = null;
            this.detailsToolWindow.Controls.Add(this.detailsPageView);
            this.detailsToolWindow.Location = new System.Drawing.Point(1, 24);
            this.detailsToolWindow.Name = "detailsToolWindow";
            this.detailsToolWindow.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.detailsToolWindow.Size = new System.Drawing.Size(792, 135);
            this.detailsToolWindow.Text = "Details";
            // 
            // detailsPageView
            // 
            this.detailsPageView.Controls.Add(this.generalPageViewPage);
            this.detailsPageView.Controls.Add(this.descriptionPageViewPage);
            this.detailsPageView.Controls.Add(this.stepsToReproducePageViewPage);
            this.detailsPageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailsPageView.Location = new System.Drawing.Point(0, 0);
            this.detailsPageView.Name = "detailsPageView";
            this.detailsPageView.SelectedPage = this.generalPageViewPage;
            this.detailsPageView.Size = new System.Drawing.Size(792, 135);
            this.detailsPageView.TabIndex = 0;
            this.detailsPageView.Text = "radPageView1";
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.detailsPageView.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // generalPageViewPage
            // 
            this.generalPageViewPage.Controls.Add(this.ownerDropDownList);
            this.generalPageViewPage.Controls.Add(this.ownerLabel);
            this.generalPageViewPage.Controls.Add(this.idSpinEditor);
            this.generalPageViewPage.Controls.Add(this.priorityDropDownList);
            this.generalPageViewPage.Controls.Add(this.dateDateTimePicker);
            this.generalPageViewPage.Controls.Add(this.statusDropDownList);
            this.generalPageViewPage.Controls.Add(this.titleTextBox);
            this.generalPageViewPage.Controls.Add(this.priorityLabel);
            this.generalPageViewPage.Controls.Add(this.dateLabel);
            this.generalPageViewPage.Controls.Add(this.statusLabel);
            this.generalPageViewPage.Controls.Add(this.titleLabel);
            this.generalPageViewPage.Controls.Add(this.idLabel);
            this.generalPageViewPage.Location = new System.Drawing.Point(10, 37);
            this.generalPageViewPage.Name = "generalPageViewPage";
            this.generalPageViewPage.Size = new System.Drawing.Size(771, 87);
            this.generalPageViewPage.Text = "General";
            // 
            // ownerDropDownList
            // 
            this.ownerDropDownList.DropDownAnimationEnabled = true;
            this.ownerDropDownList.Location = new System.Drawing.Point(441, 58);
            this.ownerDropDownList.Name = "ownerDropDownList";
            this.ownerDropDownList.ShowImageInEditorArea = true;
            this.ownerDropDownList.Size = new System.Drawing.Size(323, 20);
            this.ownerDropDownList.TabIndex = 8;
            // 
            // ownerLabel
            // 
            this.ownerLabel.Location = new System.Drawing.Point(393, 58);
            this.ownerLabel.Name = "ownerLabel";
            this.ownerLabel.Size = new System.Drawing.Size(39, 18);
            this.ownerLabel.TabIndex = 2;
            this.ownerLabel.Text = "Owner";
            // 
            // idSpinEditor
            // 
            this.idSpinEditor.Location = new System.Drawing.Point(51, 29);
            this.idSpinEditor.Name = "idSpinEditor";
            // 
            // 
            // 
            this.idSpinEditor.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.idSpinEditor.ShowBorder = true;
            this.idSpinEditor.Size = new System.Drawing.Size(326, 20);
            this.idSpinEditor.TabIndex = 7;
            this.idSpinEditor.TabStop = false;
            // 
            // priorityDropDownList
            // 
            this.priorityDropDownList.DropDownAnimationEnabled = true;
            this.priorityDropDownList.Location = new System.Drawing.Point(441, 31);
            this.priorityDropDownList.Name = "priorityDropDownList";
            this.priorityDropDownList.ShowImageInEditorArea = true;
            this.priorityDropDownList.Size = new System.Drawing.Size(327, 20);
            this.priorityDropDownList.TabIndex = 6;
            // 
            // dateDateTimePicker
            // 
            this.dateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dateDateTimePicker.Location = new System.Drawing.Point(441, 3);
            this.dateDateTimePicker.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dateDateTimePicker.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dateDateTimePicker.Name = "dateDateTimePicker";
            this.dateDateTimePicker.NullableValue = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dateDateTimePicker.NullDate = new System.DateTime(((long)(0)));
            this.dateDateTimePicker.Size = new System.Drawing.Size(326, 20);
            this.dateDateTimePicker.TabIndex = 5;
            this.dateDateTimePicker.TabStop = false;
            this.dateDateTimePicker.Value = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            // 
            // statusDropDownList
            // 
            this.statusDropDownList.DropDownAnimationEnabled = true;
            this.statusDropDownList.Location = new System.Drawing.Point(51, 58);
            this.statusDropDownList.Name = "statusDropDownList";
            this.statusDropDownList.ShowImageInEditorArea = true;
            this.statusDropDownList.Size = new System.Drawing.Size(327, 20);
            this.statusDropDownList.TabIndex = 4;
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(51, 3);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(326, 20);
            this.titleTextBox.TabIndex = 3;
            this.titleTextBox.TabStop = false;
            // 
            // priorityLabel
            // 
            this.priorityLabel.Location = new System.Drawing.Point(393, 31);
            this.priorityLabel.Name = "priorityLabel";
            this.priorityLabel.Size = new System.Drawing.Size(42, 18);
            this.priorityLabel.TabIndex = 1;
            this.priorityLabel.Text = "Priority";
            // 
            // dateLabel
            // 
            this.dateLabel.Location = new System.Drawing.Point(393, 5);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(30, 18);
            this.dateLabel.TabIndex = 1;
            this.dateLabel.Text = "Date";
            // 
            // statusLabel
            // 
            this.statusLabel.Location = new System.Drawing.Point(3, 58);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(37, 18);
            this.statusLabel.TabIndex = 1;
            this.statusLabel.Text = "Status";
            // 
            // titleLabel
            // 
            this.titleLabel.Location = new System.Drawing.Point(3, 3);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(27, 18);
            this.titleLabel.TabIndex = 1;
            this.titleLabel.Text = "Title";
            // 
            // idLabel
            // 
            this.idLabel.Location = new System.Drawing.Point(3, 29);
            this.idLabel.Name = "idLabel";
            this.idLabel.Size = new System.Drawing.Size(16, 18);
            this.idLabel.TabIndex = 0;
            this.idLabel.Text = "Id";
            // 
            // descriptionPageViewPage
            // 
            this.descriptionPageViewPage.Controls.Add(this.descriptionTextBox);
            this.descriptionPageViewPage.Location = new System.Drawing.Point(10, 37);
            this.descriptionPageViewPage.Name = "descriptionPageViewPage";
            this.descriptionPageViewPage.Size = new System.Drawing.Size(771, 79);
            this.descriptionPageViewPage.Text = "Description";
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.descriptionTextBox.Location = new System.Drawing.Point(0, 0);
            this.descriptionTextBox.Multiline = true;
            this.descriptionTextBox.Name = "descriptionTextBox";
            // 
            // 
            // 
            this.descriptionTextBox.RootElement.StretchVertically = true;
            this.descriptionTextBox.Size = new System.Drawing.Size(771, 79);
            this.descriptionTextBox.TabIndex = 1;
            this.descriptionTextBox.TabStop = false;
            // 
            // stepsToReproducePageViewPage
            // 
            this.stepsToReproducePageViewPage.Controls.Add(this.stepsToReproduceTextBox);
            this.stepsToReproducePageViewPage.Location = new System.Drawing.Point(10, 37);
            this.stepsToReproducePageViewPage.Name = "stepsToReproducePageViewPage";
            this.stepsToReproducePageViewPage.Size = new System.Drawing.Size(771, 79);
            this.stepsToReproducePageViewPage.Text = "Steps to reproduce";
            // 
            // stepsToReproduceTextBox
            // 
            this.stepsToReproduceTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stepsToReproduceTextBox.Location = new System.Drawing.Point(0, 0);
            this.stepsToReproduceTextBox.Multiline = true;
            this.stepsToReproduceTextBox.Name = "stepsToReproduceTextBox";
            // 
            // 
            // 
            this.stepsToReproduceTextBox.RootElement.StretchVertically = true;
            this.stepsToReproduceTextBox.Size = new System.Drawing.Size(771, 79);
            this.stepsToReproduceTextBox.TabIndex = 2;
            this.stepsToReproduceTextBox.TabStop = false;
            // 
            // RadRibbonForm1
            // 
            this.AllowAero = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 712);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.radRibbonBar1);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Name = "RadRibbonForm1";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Bug Tracker";
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radDock1)).EndInit();
            this.radDock1.ResumeLayout(false);
            this.projectsToolWindow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.projectsTreeView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip3)).EndInit();
            this.toolTabStrip3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).EndInit();
            this.documentContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentTabStrip2)).EndInit();
            this.documentTabStrip2.ResumeLayout(false);
            this.bugDocumentWindow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bugsGrid)).EndInit();
            this.featureDocumentWindow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.featuresGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip2)).EndInit();
            this.toolTabStrip2.ResumeLayout(false);
            this.detailsToolWindow.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.detailsPageView)).EndInit();
            this.detailsPageView.ResumeLayout(false);
            this.generalPageViewPage.ResumeLayout(false);
            this.generalPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ownerDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ownerLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.idSpinEditor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.priorityDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateDateTimePicker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusDropDownList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.titleTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.priorityLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.titleLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.idLabel)).EndInit();
            this.descriptionPageViewPage.ResumeLayout(false);
            this.descriptionPageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.descriptionTextBox)).EndInit();
            this.stepsToReproducePageViewPage.ResumeLayout(false);
            this.stepsToReproducePageViewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stepsToReproduceTextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        

        #endregion

        private Telerik.WinControls.UI.RadRibbonBar radRibbonBar1;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.Docking.RadDock radDock1;
        private Telerik.WinControls.UI.Docking.DocumentWindow featureDocumentWindow;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip3;
        private Telerik.WinControls.UI.Docking.ToolWindow projectsToolWindow;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.Docking.DocumentContainer documentContainer1;
        private Telerik.WinControls.UI.Docking.DocumentTabStrip documentTabStrip2;
        private Telerik.WinControls.UI.Docking.DocumentWindow bugDocumentWindow;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip2;
        private Telerik.WinControls.UI.Docking.ToolWindow detailsToolWindow;
        private Telerik.WinControls.UI.RadPageView detailsPageView;
        private Telerik.WinControls.UI.RadPageViewPage generalPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage descriptionPageViewPage;
        private Telerik.WinControls.UI.RadPageViewPage stepsToReproducePageViewPage;
        private Telerik.WinControls.UI.RadGridView featuresGrid;
        private Telerik.WinControls.UI.RadTreeView projectsTreeView;
        private Telerik.WinControls.UI.RibbonTab homeTab;
        private Telerik.WinControls.UI.RadRibbonBarGroup databaseGroup;
        private Telerik.WinControls.UI.RadButtonElement loadDatabaseButton;
        private Telerik.WinControls.UI.RadRibbonBarGroup editGroup;
        private Telerik.WinControls.UI.RadButtonElement addButton;
        private Telerik.WinControls.UI.RadButtonElement removeButton;
        private Telerik.WinControls.UI.RadButtonElement editButton;
        private Telerik.WinControls.UI.RadRibbonBarGroup viewGroup;
        private Telerik.WinControls.UI.RadDropDownButtonElement sortDropDownButton;
        private Telerik.WinControls.UI.RadMenuItem sortNoneMenuItem;
        private Telerik.WinControls.UI.RadMenuItem sortDateMenuItem;
        private Telerik.WinControls.UI.RadMenuItem sortStatusMenuItem;
        private Telerik.WinControls.UI.RadMenuItem sortOwnerMenuItem;
        private Telerik.WinControls.UI.RibbonTab viewTab;
        private Telerik.WinControls.UI.RadRibbonBarGroup layoutGroup;
        private Telerik.WinControls.UI.RadButtonElement loadLayoutButton;
        private Telerik.WinControls.UI.RadButtonElement saveLayoutButton;
        private Telerik.WinControls.UI.RadRibbonBarGroup panelsGroup;
        private Telerik.WinControls.UI.RadGridView bugsGrid;
        private Telerik.WinControls.UI.RadMenuItem loadDatabaseMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem1;
        private Telerik.WinControls.UI.RadMenuItem addMenuItem;
        private Telerik.WinControls.UI.RadMenuItem removeMenuItem;
        private Telerik.WinControls.UI.RadMenuItem editMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem2;
        private Telerik.WinControls.UI.RadMenuItem aboutMenuItem;
        private Telerik.WinControls.UI.RadMenuItem exitMenuItem;
        private Telerik.WinControls.UI.RadTextBox descriptionTextBox;
        private Telerik.WinControls.UI.RadTextBox stepsToReproduceTextBox;
        private Telerik.WinControls.UI.RadSpinEditor idSpinEditor;
        private Telerik.WinControls.UI.RadDropDownList priorityDropDownList;
        private Telerik.WinControls.UI.RadDateTimePicker dateDateTimePicker;
        private Telerik.WinControls.UI.RadDropDownList statusDropDownList;
        private Telerik.WinControls.UI.RadTextBox titleTextBox;
        private Telerik.WinControls.UI.RadLabel priorityLabel;
        private Telerik.WinControls.UI.RadLabel dateLabel;
        private Telerik.WinControls.UI.RadLabel statusLabel;
        private Telerik.WinControls.UI.RadLabel titleLabel;
        private Telerik.WinControls.UI.RadLabel idLabel;
        private Telerik.WinControls.UI.RadButtonElement saveDatabaseButton;
        private Telerik.WinControls.Themes.Office2010BlackTheme office2010BlackTheme1;
        private Telerik.WinControls.Themes.Office2010BlueTheme office2010BlueTheme1;
        private Telerik.WinControls.Themes.Office2010SilverTheme office2010SilverTheme1;
        private Telerik.WinControls.Themes.TelerikMetroTheme telerikMetroTheme1;
        private Telerik.WinControls.UI.RadMenuItem dateAscendingMenuItem;
        private Telerik.WinControls.UI.RadMenuItem dateDescendingMenuItem;
        private Telerik.WinControls.UI.RadMenuItem statusAscendingMenuItem;
        private Telerik.WinControls.UI.RadMenuItem statusDescendingMenuItem;
        private Telerik.WinControls.UI.RadMenuItem ownerAscendingMenuItem;
        private Telerik.WinControls.UI.RadMenuItem ownerDescendingMenuItem;
        private Telerik.WinControls.UI.RadButtonElement projectsButton;
        private Telerik.WinControls.UI.RadButtonElement detailsButton;
        private Telerik.WinControls.UI.RadButtonElement bugsButton;
        private Telerik.WinControls.UI.RadButtonElement featuresButton;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup1;
        private Telerik.WinControls.UI.RadDropDownList ownerDropDownList;
        private Telerik.WinControls.UI.RadLabel ownerLabel;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup1;
        private Telerik.WinControls.UI.RadGalleryElement radGalleryElement1;
        private Telerik.WinControls.UI.RadGalleryItem telerikButton;
        private Telerik.WinControls.UI.RadGalleryItem office2010BlackButton;
        private Telerik.WinControls.UI.RadGalleryItem office2010BlueButton;
        private Telerik.WinControls.UI.RadGalleryItem office2010SilverButton;
        private Telerik.WinControls.UI.RadGalleryItem controlDefaultButton;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup2;
        private Telerik.WinControls.UI.RadCheckBoxElement openCheckBox;
        private Telerik.WinControls.UI.RadCheckBoxElement readyForTestCheckBox;
        private Telerik.WinControls.UI.RadCheckBoxElement doneCheckBox;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup3;
        private Telerik.WinControls.UI.RadCheckBoxElement unassignedCheckBox;
        private Telerik.WinControls.UI.RadCheckBoxElement newCheckBox;
    }
}
