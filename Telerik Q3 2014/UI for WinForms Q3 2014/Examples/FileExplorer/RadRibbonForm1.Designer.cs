﻿namespace FileExplorer
{
    partial class RadRibbonForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.radBreadCrumb1 = new Telerik.WinControls.UI.RadBreadCrumb();
            this.radTreeView1 = new Telerik.WinControls.UI.RadTreeView();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radListView1 = new Telerik.WinControls.UI.RadListView();
            this.office2010BlackTheme1 = new Telerik.WinControls.Themes.Office2010BlackTheme();
            this.office2010BlueTheme1 = new Telerik.WinControls.Themes.Office2010BlueTheme();
            this.office2010SilverTheme1 = new Telerik.WinControls.Themes.Office2010SilverTheme();
            this.telerikMetroTheme1 = new Telerik.WinControls.Themes.TelerikMetroTheme();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.directoryInfoLabel = new Telerik.WinControls.UI.RadLabelElement();
            this.radCommandBarSeparatorItem1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.selectedItemInfoLabel = new Telerik.WinControls.UI.RadLabelElement();
            this.separatorLabel = new Telerik.WinControls.UI.RadLabelElement();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.historyButton = new Telerik.WinControls.UI.RadDropDownButton();
            this.upButton = new Telerik.WinControls.UI.RadButton();
            this.refreshButton = new Telerik.WinControls.UI.RadButton();
            this.forwardButton = new Telerik.WinControls.UI.RadButton();
            this.backButton = new Telerik.WinControls.UI.RadButton();
            this.radRibbonBar1 = new Telerik.WinControls.UI.RadRibbonBar();
            this.ribbonTab1 = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup1 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radDropDownButtonElement1 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.pasteMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.pasteAsShortcutMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radRibbonBarButtonGroup1 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.cutButton = new Telerik.WinControls.UI.RadButtonElement();
            this.copyButton = new Telerik.WinControls.UI.RadButtonElement();
            this.copyAsPathButton = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup2 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.newFolderButton = new Telerik.WinControls.UI.RadButtonElement();
            this.radDropDownButtonElement2 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radRibbonBarGroup3 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radDropDownButtonElement3 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.deleteSelectionMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.deleteAllMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radRibbonBarButtonGroup2 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.copyToFolderButton = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.renameButton = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement6 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup4 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radGalleryElement1 = new Telerik.WinControls.UI.RadGalleryElement();
            this.radGalleryItem1 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radGalleryItem2 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radGalleryItem3 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radGalleryItem4 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radGalleryItem5 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radGalleryItem6 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radRibbonBarGroup5 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radDropDownButtonElement5 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radButtonElement7 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarButtonGroup3 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radDropDownButtonElement6 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radButtonElement9 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement10 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup6 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.selectAllButton = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarButtonGroup4 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement11 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement12 = new Telerik.WinControls.UI.RadButtonElement();
            this.ribbonTab2 = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup7 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElement13 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement14 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement15 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup8 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radGalleryElement2 = new Telerik.WinControls.UI.RadGalleryElement();
            this.radGalleryItem7 = new Telerik.WinControls.UI.RadGalleryItem();
            this.radRibbonBarGroup9 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup5 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radButtonElement16 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement17 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup10 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radDropDownButtonElement7 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.ribbonTab3 = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup11 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.refreshButtonInRibbon = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup12 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radGalleryElement3 = new Telerik.WinControls.UI.RadGalleryElement();
            this.extraLargeIconsGaleryItem = new Telerik.WinControls.UI.RadGalleryItem();
            this.mediumIconsGaleryItem = new Telerik.WinControls.UI.RadGalleryItem();
            this.listGaleryItem = new Telerik.WinControls.UI.RadGalleryItem();
            this.largeIconsGaleryItem = new Telerik.WinControls.UI.RadGalleryItem();
            this.smallIconsGaleryItem = new Telerik.WinControls.UI.RadGalleryItem();
            this.detailsGaleryItem = new Telerik.WinControls.UI.RadGalleryItem();
            this.radRibbonBarGroup13 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.sortByButton = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.sortByNameMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.sortByDateModifiedMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.sortByTypeMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.sortBySizeMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radRibbonBarButtonGroup6 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radDropDownButtonElement9 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radDropDownButtonElement10 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radButtonElement19 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup14 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radDropDownButtonElement11 = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.radButtonElement20 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElement21 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup15 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup7 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radCheckBoxElement1 = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radCheckBoxElement2 = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radCheckBoxElement3 = new Telerik.WinControls.UI.RadCheckBoxElement();
            this.radButtonElement22 = new Telerik.WinControls.UI.RadButtonElement();
            this.ribbonTab4 = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup16 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radGalleryElement4 = new Telerik.WinControls.UI.RadGalleryElement();
            this.office2010BlackButton = new Telerik.WinControls.UI.RadGalleryItem();
            this.office2010BlueButton = new Telerik.WinControls.UI.RadGalleryItem();
            this.office2010SilverButton = new Telerik.WinControls.UI.RadGalleryItem();
            this.controlDefaultButton = new Telerik.WinControls.UI.RadGalleryItem();
            this.telerikMetroButton = new Telerik.WinControls.UI.RadGalleryItem();
            this.openNewWindowMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.openCommandPromptMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.addSelectedFolderToFavoritesMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.includeInLibraryMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.mapSelectedFolderAsDriveMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem2 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.clearHistoryMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem3 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.printMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem4 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.syncMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem5 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.folderOptionsMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem6 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.helpMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem7 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.closeMenuItem = new Telerik.WinControls.UI.RadMenuItem();
            this.frequentPlacesMenuItem = new Telerik.WinControls.UI.RadMenuHeaderItem();
            this.radMenuSeparatorItem8 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBreadCrumb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radListView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.historyButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.forwardButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.historyButton);
            this.radPanel1.Controls.Add(this.upButton);
            this.radPanel1.Controls.Add(this.radTextBox1);
            this.radPanel1.Controls.Add(this.refreshButton);
            this.radPanel1.Controls.Add(this.radBreadCrumb1);
            this.radPanel1.Controls.Add(this.forwardButton);
            this.radPanel1.Controls.Add(this.backButton);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 158);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(1065, 40);
            this.radPanel1.TabIndex = 0;
            // 
            // radTextBox1
            // 
            this.radTextBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.radTextBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.radTextBox1.Location = new System.Drawing.Point(851, 10);
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.Size = new System.Drawing.Size(202, 20);
            this.radTextBox1.TabIndex = 5;
            this.radTextBox1.TabStop = false;
            // 
            // radBreadCrumb1
            // 
            this.radBreadCrumb1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radBreadCrumb1.Location = new System.Drawing.Point(125, 6);
            this.radBreadCrumb1.Name = "radBreadCrumb1";
            this.radBreadCrumb1.Size = new System.Drawing.Size(694, 28);
            this.radBreadCrumb1.TabIndex = 3;
            this.radBreadCrumb1.Text = "radBreadCrumb1";
            this.radBreadCrumb1.ThemeName = "Office2007Black";
            // 
            // radTreeView1
            // 
            this.radTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTreeView1.Location = new System.Drawing.Point(0, 0);
            this.radTreeView1.Name = "radTreeView1";
            this.radTreeView1.Size = new System.Drawing.Size(307, 385);
            this.radTreeView1.SpacingBetweenNodes = -1;
            this.radTreeView1.TabIndex = 2;
            this.radTreeView1.Text = "radTreeView1";
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 198);
            this.radSplitContainer1.Name = "radSplitContainer1";
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radSplitContainer1.Size = new System.Drawing.Size(1065, 385);
            this.radSplitContainer1.TabIndex = 4;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radTreeView1);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel1.Size = new System.Drawing.Size(307, 385);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.2112583F, 0F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(-160, 0);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radListView1);
            this.splitPanel2.Location = new System.Drawing.Point(310, 0);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel2.Size = new System.Drawing.Size(755, 385);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.2112583F, 0F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(160, 0);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radListView1
            // 
            this.radListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radListView1.GroupItemSize = new System.Drawing.Size(200, 20);
            this.radListView1.ItemSize = new System.Drawing.Size(200, 20);
            this.radListView1.Location = new System.Drawing.Point(0, 0);
            this.radListView1.Name = "radListView1";
            this.radListView1.Size = new System.Drawing.Size(755, 385);
            this.radListView1.TabIndex = 0;
            this.radListView1.Text = "radListView1";
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.AutoSize = true;
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.directoryInfoLabel,
            this.radCommandBarSeparatorItem1,
            this.selectedItemInfoLabel,
            this.separatorLabel});
            this.radStatusStrip1.LayoutStyle = Telerik.WinControls.UI.RadStatusBarLayoutStyle.Stack;
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 583);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Padding = new System.Windows.Forms.Padding(3);
            // 
            // 
            // 
            this.radStatusStrip1.RootElement.Padding = new System.Windows.Forms.Padding(3);
            this.radStatusStrip1.Size = new System.Drawing.Size(1065, 27);
            this.radStatusStrip1.TabIndex = 0;
            this.radStatusStrip1.Text = "radStatusStrip1";
            // 
            // directoryInfoLabel
            // 
            this.directoryInfoLabel.Margin = new System.Windows.Forms.Padding(1);
            this.directoryInfoLabel.Name = "directoryInfoLabel";
            this.radStatusStrip1.SetSpring(this.directoryInfoLabel, false);
            this.directoryInfoLabel.Text = "";
            this.directoryInfoLabel.TextWrap = true;
            this.directoryInfoLabel.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radToolStripSeparatorItem1
            // 
            this.radCommandBarSeparatorItem1.AccessibleDescription = "radToolStripSeparatorItem1";
            this.radCommandBarSeparatorItem1.AccessibleName = "radToolStripSeparatorItem1";
            this.radCommandBarSeparatorItem1.Margin = new System.Windows.Forms.Padding(1);
            this.radCommandBarSeparatorItem1.Name = "radToolStripSeparatorItem1";
            this.radStatusStrip1.SetSpring(this.radCommandBarSeparatorItem1, false);
            this.radCommandBarSeparatorItem1.Text = "radToolStripSeparatorItem1";
            this.radCommandBarSeparatorItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // selectedItemInfoLabel
            // 
            this.selectedItemInfoLabel.Margin = new System.Windows.Forms.Padding(1);
            this.selectedItemInfoLabel.Name = "selectedItemInfoLabel";
            this.radStatusStrip1.SetSpring(this.selectedItemInfoLabel, false);
            this.selectedItemInfoLabel.Text = "";
            this.selectedItemInfoLabel.TextWrap = true;
            this.selectedItemInfoLabel.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // separatorLabel
            // 
            this.separatorLabel.Margin = new System.Windows.Forms.Padding(1);
            this.separatorLabel.Name = "separatorLabel";
            this.radStatusStrip1.SetSpring(this.separatorLabel, true);
            this.separatorLabel.Text = "";
            this.separatorLabel.TextWrap = true;
            this.separatorLabel.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // historyButton
            // 
            this.historyButton.Image = global::FileExplorer.Properties.Resources.history;
            this.historyButton.Location = new System.Drawing.Point(79, 12);
            this.historyButton.Name = "historyButton";
            this.historyButton.Size = new System.Drawing.Size(16, 16);
            this.historyButton.TabIndex = 7;
            this.historyButton.Click += new System.EventHandler(this.historyButton_Click);
            // 
            // upButton
            // 
            this.upButton.AutoSize = true;
            this.upButton.Image = global::FileExplorer.Properties.Resources.up;
            this.upButton.Location = new System.Drawing.Point(101, 12);
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size(18, 18);
            this.upButton.TabIndex = 6;
            this.upButton.Click += new System.EventHandler(this.upButton_Click);
            // 
            // refreshButton
            // 
            this.refreshButton.AutoSize = true;
            this.refreshButton.Image = global::FileExplorer.Properties.Resources.refresh;
            this.refreshButton.Location = new System.Drawing.Point(825, 12);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(18, 18);
            this.refreshButton.TabIndex = 4;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // forwardButton
            // 
            this.forwardButton.Image = global::FileExplorer.Properties.Resources.foward;
            this.forwardButton.Location = new System.Drawing.Point(46, 4);
            this.forwardButton.Name = "forwardButton";
            this.forwardButton.Size = new System.Drawing.Size(32, 32);
            this.forwardButton.TabIndex = 1;
            this.forwardButton.Click += new System.EventHandler(this.forwardButton_Click);
            // 
            // backButton
            // 
            this.backButton.Image = global::FileExplorer.Properties.Resources.back;
            this.backButton.Location = new System.Drawing.Point(12, 4);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(32, 32);
            this.backButton.TabIndex = 0;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // radRibbonBar1
            // 
            this.radRibbonBar1.AutoSize = true;
            this.radRibbonBar1.CommandTabs.AddRange(new Telerik.WinControls.RadItem[] {
            this.ribbonTab1,
            this.ribbonTab2,
            this.ribbonTab3,
            this.ribbonTab4});
            this.radRibbonBar1.Dock = System.Windows.Forms.DockStyle.Top;
            // 
            // 
            // 
            this.radRibbonBar1.ExitButton.AccessibleDescription = "Exit";
            this.radRibbonBar1.ExitButton.AccessibleName = "Exit";
            // 
            // 
            // 
            this.radRibbonBar1.ExitButton.ButtonElement.AccessibleDescription = "Exit";
            this.radRibbonBar1.ExitButton.ButtonElement.AccessibleName = "Exit";
            this.radRibbonBar1.ExitButton.Text = "Exit";
            this.radRibbonBar1.Location = new System.Drawing.Point(0, 0);
            this.radRibbonBar1.Name = "radRibbonBar1";
            // 
            // 
            // 
            this.radRibbonBar1.OptionsButton.AccessibleDescription = "Options";
            this.radRibbonBar1.OptionsButton.AccessibleName = "Options";
            // 
            // 
            // 
            this.radRibbonBar1.OptionsButton.ButtonElement.AccessibleDescription = "Options";
            this.radRibbonBar1.OptionsButton.ButtonElement.AccessibleName = "Options";
            this.radRibbonBar1.OptionsButton.Text = "Options";
            this.radRibbonBar1.Size = new System.Drawing.Size(1065, 158);
            this.radRibbonBar1.StartMenuItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.openNewWindowMenuItem,
            this.openCommandPromptMenuItem,
            this.radMenuSeparatorItem1,
            this.addSelectedFolderToFavoritesMenuItem,
            this.includeInLibraryMenuItem,
            this.mapSelectedFolderAsDriveMenuItem,
            this.radMenuSeparatorItem2,
            this.clearHistoryMenuItem,
            this.radMenuSeparatorItem3,
            this.printMenuItem,
            this.radMenuSeparatorItem4,
            this.syncMenuItem,
            this.radMenuSeparatorItem5,
            this.folderOptionsMenuItem,
            this.radMenuSeparatorItem6,
            this.helpMenuItem,
            this.radMenuSeparatorItem7,
            this.closeMenuItem});
            this.radRibbonBar1.StartMenuRightColumnItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.frequentPlacesMenuItem,
            this.radMenuSeparatorItem8});
            this.radRibbonBar1.TabIndex = 5;
            this.radRibbonBar1.Text = "RadForm1";
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.AccessibleDescription = "Home";
            this.ribbonTab1.AccessibleName = "Home";
            this.ribbonTab1.Alignment = System.Drawing.ContentAlignment.BottomLeft;
            this.ribbonTab1.Class = "RibbonTab";
            this.ribbonTab1.IsSelected = true;
            this.ribbonTab1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup1,
            this.radRibbonBarGroup2,
            this.radRibbonBarGroup3,
            this.radRibbonBarGroup4,
            this.radRibbonBarGroup5,
            this.radRibbonBarGroup6});
            this.ribbonTab1.Name = "ribbonTab1";
            this.ribbonTab1.StretchHorizontally = false;
            this.ribbonTab1.Text = "Home";
            this.ribbonTab1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup1
            // 
            this.radRibbonBarGroup1.AccessibleDescription = "Clipboard";
            this.radRibbonBarGroup1.AccessibleName = "Clipboard";
            this.radRibbonBarGroup1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radDropDownButtonElement1,
            this.radRibbonBarButtonGroup1});
            this.radRibbonBarGroup1.Name = "radRibbonBarGroup1";
            this.radRibbonBarGroup1.Text = "Clipboard";
            this.radRibbonBarGroup1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radDropDownButtonElement1
            // 
            this.radDropDownButtonElement1.AccessibleDescription = "Paste";
            this.radDropDownButtonElement1.AccessibleName = "Paste";
            this.radDropDownButtonElement1.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement1.ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            this.radDropDownButtonElement1.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement1.ExpandArrowButton = false;
            this.radDropDownButtonElement1.Image = global::FileExplorer.Properties.Resources._1_paste;
            this.radDropDownButtonElement1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.pasteMenuItem,
            this.pasteAsShortcutMenuItem});
            this.radDropDownButtonElement1.Name = "radDropDownButtonElement1";
            this.radDropDownButtonElement1.Text = "Paste";
            this.radDropDownButtonElement1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radDropDownButtonElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // pasteMenuItem
            // 
            this.pasteMenuItem.AccessibleDescription = "Paste";
            this.pasteMenuItem.AccessibleName = "Paste";
            this.pasteMenuItem.Name = "pasteMenuItem";
            this.pasteMenuItem.Text = "Paste";
            this.pasteMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // pasteAsShortcutMenuItem
            // 
            this.pasteAsShortcutMenuItem.AccessibleDescription = "Paste as shortcut";
            this.pasteAsShortcutMenuItem.AccessibleName = "Paste as shortcut";
            this.pasteAsShortcutMenuItem.Name = "pasteAsShortcutMenuItem";
            this.pasteAsShortcutMenuItem.Text = "Paste as shortcut";
            this.pasteAsShortcutMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarButtonGroup1
            // 
            this.radRibbonBarButtonGroup1.AccessibleDescription = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.AccessibleName = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.cutButton,
            this.copyButton,
            this.copyAsPathButton});
            this.radRibbonBarButtonGroup1.Name = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup1.StretchVertically = true;
            this.radRibbonBarButtonGroup1.Text = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // cutButton
            // 
            this.cutButton.AccessibleDescription = "Cut";
            this.cutButton.AccessibleName = "Cut";
            this.cutButton.Class = "ButtonElement";
            this.cutButton.Image = global::FileExplorer.Properties.Resources._1_cut;
            this.cutButton.Name = "cutButton";
            this.cutButton.ShowBorder = false;
            this.cutButton.Text = "Cut";
            this.cutButton.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.cutButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cutButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // copyButton
            // 
            this.copyButton.AccessibleDescription = "Copy";
            this.copyButton.AccessibleName = "Copy";
            this.copyButton.Class = "ButtonElement";
            this.copyButton.Image = global::FileExplorer.Properties.Resources._1_copy;
            this.copyButton.Name = "copyButton";
            this.copyButton.ShowBorder = false;
            this.copyButton.Text = "Copy";
            this.copyButton.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.copyButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.copyButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.copyButton.Click += new System.EventHandler(this.copyButton_Click);
            // 
            // copyAsPathButton
            // 
            this.copyAsPathButton.AccessibleDescription = "Copy as path";
            this.copyAsPathButton.AccessibleName = "Copy as path";
            this.copyAsPathButton.Class = "ButtonElement";
            this.copyAsPathButton.Image = global::FileExplorer.Properties.Resources._1_copy_as_path;
            this.copyAsPathButton.Name = "copyAsPathButton";
            this.copyAsPathButton.ShowBorder = false;
            this.copyAsPathButton.Text = "Copy as path";
            this.copyAsPathButton.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.copyAsPathButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.copyAsPathButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.copyAsPathButton.Click += new System.EventHandler(this.copyAsPathButton_Click);
            // 
            // radRibbonBarGroup2
            // 
            this.radRibbonBarGroup2.AccessibleDescription = "New";
            this.radRibbonBarGroup2.AccessibleName = "New";
            this.radRibbonBarGroup2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.newFolderButton,
            this.radDropDownButtonElement2});
            this.radRibbonBarGroup2.Name = "radRibbonBarGroup2";
            this.radRibbonBarGroup2.Text = "New";
            this.radRibbonBarGroup2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // newFolderButton
            // 
            this.newFolderButton.AccessibleDescription = "New folder";
            this.newFolderButton.AccessibleName = "New folder";
            this.newFolderButton.Class = "RibbonBarButtonElement";
            this.newFolderButton.Image = global::FileExplorer.Properties.Resources._1_new_folder;
            this.newFolderButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.newFolderButton.Name = "newFolderButton";
            this.newFolderButton.Text = "New folder";
            this.newFolderButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.newFolderButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.newFolderButton.Click += new System.EventHandler(this.newFolderButton_Click);
            // 
            // radDropDownButtonElement2
            // 
            this.radDropDownButtonElement2.AccessibleDescription = "New items";
            this.radDropDownButtonElement2.AccessibleName = "New items";
            this.radDropDownButtonElement2.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement2.ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            this.radDropDownButtonElement2.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement2.ExpandArrowButton = false;
            this.radDropDownButtonElement2.Image = global::FileExplorer.Properties.Resources._1_new_items;
            this.radDropDownButtonElement2.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radDropDownButtonElement2.Name = "radDropDownButtonElement2";
            this.radDropDownButtonElement2.Text = "New items";
            this.radDropDownButtonElement2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radDropDownButtonElement2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup3
            // 
            this.radRibbonBarGroup3.AccessibleDescription = "Organize";
            this.radRibbonBarGroup3.AccessibleName = "Organize";
            this.radRibbonBarGroup3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radDropDownButtonElement3,
            this.radRibbonBarButtonGroup2});
            this.radRibbonBarGroup3.Name = "radRibbonBarGroup3";
            this.radRibbonBarGroup3.Text = "Organize";
            this.radRibbonBarGroup3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radDropDownButtonElement3
            // 
            this.radDropDownButtonElement3.AccessibleDescription = "Delete";
            this.radDropDownButtonElement3.AccessibleName = "Delete";
            this.radDropDownButtonElement3.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement3.ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            this.radDropDownButtonElement3.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement3.ExpandArrowButton = false;
            this.radDropDownButtonElement3.Image = global::FileExplorer.Properties.Resources._1_delete;
            this.radDropDownButtonElement3.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radDropDownButtonElement3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.deleteSelectionMenuItem,
            this.deleteAllMenuItem});
            this.radDropDownButtonElement3.Name = "radDropDownButtonElement3";
            this.radDropDownButtonElement3.Text = "Delete";
            this.radDropDownButtonElement3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radDropDownButtonElement3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // deleteSelectionMenuItem
            // 
            this.deleteSelectionMenuItem.AccessibleDescription = "Delete selection";
            this.deleteSelectionMenuItem.AccessibleName = "Delete selection";
            this.deleteSelectionMenuItem.Name = "deleteSelectionMenuItem";
            this.deleteSelectionMenuItem.Text = "Delete selection";
            this.deleteSelectionMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // deleteAllMenuItem
            // 
            this.deleteAllMenuItem.AccessibleDescription = "Delete all";
            this.deleteAllMenuItem.AccessibleName = "Delete all";
            this.deleteAllMenuItem.Name = "deleteAllMenuItem";
            this.deleteAllMenuItem.Text = "Delete all";
            this.deleteAllMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarButtonGroup2
            // 
            this.radRibbonBarButtonGroup2.AccessibleDescription = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.AccessibleName = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.copyToFolderButton,
            this.renameButton,
            this.radButtonElement6});
            this.radRibbonBarButtonGroup2.Name = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup2.StretchVertically = true;
            this.radRibbonBarButtonGroup2.Text = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // copyToFolderButton
            // 
            this.copyToFolderButton.AccessibleDescription = "Copy to folder";
            this.copyToFolderButton.AccessibleName = "Copy to folder";
            this.copyToFolderButton.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.copyToFolderButton.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.copyToFolderButton.ExpandArrowButton = false;
            this.copyToFolderButton.Image = global::FileExplorer.Properties.Resources._1_copy_to_folder;
            this.copyToFolderButton.Name = "copyToFolderButton";
            this.copyToFolderButton.Text = "Copy to folder";
            this.copyToFolderButton.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.copyToFolderButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.copyToFolderButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.copyToFolderButton.Click += new System.EventHandler(this.copyToFolderButton_Click);
            // 
            // renameButton
            // 
            this.renameButton.AccessibleDescription = "Rename";
            this.renameButton.AccessibleName = "Rename";
            this.renameButton.Class = "ButtonElement";
            this.renameButton.Image = global::FileExplorer.Properties.Resources._1_rename;
            this.renameButton.Name = "renameButton";
            this.renameButton.ShowBorder = false;
            this.renameButton.Text = "Rename";
            this.renameButton.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.renameButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.renameButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.renameButton.Click += new System.EventHandler(this.renameButton_Click);
            // 
            // radButtonElement6
            // 
            this.radButtonElement6.AccessibleDescription = "Zip";
            this.radButtonElement6.AccessibleName = "Zip";
            this.radButtonElement6.Class = "ButtonElement";
            this.radButtonElement6.Image = global::FileExplorer.Properties.Resources._1_zip;
            this.radButtonElement6.Name = "radButtonElement6";
            this.radButtonElement6.ShowBorder = false;
            this.radButtonElement6.Text = "Zip";
            this.radButtonElement6.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButtonElement6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButtonElement6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup4
            // 
            this.radRibbonBarGroup4.AccessibleDescription = "Move to folder";
            this.radRibbonBarGroup4.AccessibleName = "Move to folder";
            this.radRibbonBarGroup4.AutoSize = true;
            this.radRibbonBarGroup4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radGalleryElement1});
            this.radRibbonBarGroup4.Margin = new System.Windows.Forms.Padding(0);
            this.radRibbonBarGroup4.MaxSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup4.MinSize = new System.Drawing.Size(0, 0);
            this.radRibbonBarGroup4.Name = "radRibbonBarGroup4";
            this.radRibbonBarGroup4.Text = "Move to folder";
            this.radRibbonBarGroup4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGalleryElement1
            // 
            this.radGalleryElement1.AccessibleDescription = "radGalleryElement1";
            this.radGalleryElement1.AccessibleName = "radGalleryElement1";
            this.radGalleryElement1.AutoSize = false;
            this.radGalleryElement1.Bounds = new System.Drawing.Rectangle(0, 0, 260, 69);
            this.radGalleryElement1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radGalleryItem1,
            this.radGalleryItem2,
            this.radGalleryItem3,
            this.radGalleryItem4,
            this.radGalleryItem5,
            this.radGalleryItem6});
            this.radGalleryElement1.Name = "radGalleryElement1";
            this.radGalleryElement1.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            this.radGalleryElement1.Text = "radGalleryElement1";
            this.radGalleryElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGalleryItem1
            // 
            this.radGalleryItem1.AccessibleDescription = "system32";
            this.radGalleryItem1.AccessibleName = "system32";
            this.radGalleryItem1.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGalleryItem1.DescriptionText = "";
            this.radGalleryItem1.Image = global::FileExplorer.Properties.Resources._1_system_32;
            this.radGalleryItem1.Name = "radGalleryItem1";
            this.radGalleryItem1.StretchHorizontally = false;
            this.radGalleryItem1.StretchVertically = false;
            this.radGalleryItem1.Text = "system32";
            this.radGalleryItem1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radGalleryItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radGalleryItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGalleryItem2
            // 
            this.radGalleryItem2.AccessibleDescription = "Local Disk";
            this.radGalleryItem2.AccessibleName = "Local Disk";
            this.radGalleryItem2.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGalleryItem2.DescriptionText = "";
            this.radGalleryItem2.Image = global::FileExplorer.Properties.Resources._1_local_disc;
            this.radGalleryItem2.Name = "radGalleryItem2";
            this.radGalleryItem2.StretchHorizontally = false;
            this.radGalleryItem2.StretchVertically = false;
            this.radGalleryItem2.Text = "Local Disk";
            this.radGalleryItem2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radGalleryItem2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radGalleryItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGalleryItem3
            // 
            this.radGalleryItem3.AccessibleDescription = "Desktop";
            this.radGalleryItem3.AccessibleName = "Desktop";
            this.radGalleryItem3.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGalleryItem3.DescriptionText = "";
            this.radGalleryItem3.Image = global::FileExplorer.Properties.Resources._1_desktop;
            this.radGalleryItem3.Name = "radGalleryItem3";
            this.radGalleryItem3.StretchHorizontally = false;
            this.radGalleryItem3.StretchVertically = false;
            this.radGalleryItem3.Text = "Desktop";
            this.radGalleryItem3.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radGalleryItem3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radGalleryItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGalleryItem4
            // 
            this.radGalleryItem4.AccessibleDescription = "Downloads";
            this.radGalleryItem4.AccessibleName = "Downloads";
            this.radGalleryItem4.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGalleryItem4.DescriptionText = "";
            this.radGalleryItem4.Image = global::FileExplorer.Properties.Resources._1_downloads;
            this.radGalleryItem4.Name = "radGalleryItem4";
            this.radGalleryItem4.StretchHorizontally = false;
            this.radGalleryItem4.StretchVertically = false;
            this.radGalleryItem4.Text = "Downloads";
            this.radGalleryItem4.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radGalleryItem4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radGalleryItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGalleryItem5
            // 
            this.radGalleryItem5.AccessibleDescription = "Documents";
            this.radGalleryItem5.AccessibleName = "Documents";
            this.radGalleryItem5.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGalleryItem5.DescriptionText = "";
            this.radGalleryItem5.Image = global::FileExplorer.Properties.Resources._1_documents;
            this.radGalleryItem5.Name = "radGalleryItem5";
            this.radGalleryItem5.StretchHorizontally = false;
            this.radGalleryItem5.StretchVertically = false;
            this.radGalleryItem5.Text = "Documents";
            this.radGalleryItem5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radGalleryItem5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radGalleryItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGalleryItem6
            // 
            this.radGalleryItem6.AccessibleDescription = "Music";
            this.radGalleryItem6.AccessibleName = "Music";
            this.radGalleryItem6.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radGalleryItem6.DescriptionText = "";
            this.radGalleryItem6.Image = global::FileExplorer.Properties.Resources._1_music;
            this.radGalleryItem6.Name = "radGalleryItem6";
            this.radGalleryItem6.StretchHorizontally = false;
            this.radGalleryItem6.StretchVertically = false;
            this.radGalleryItem6.Text = "Music";
            this.radGalleryItem6.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radGalleryItem6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radGalleryItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup5
            // 
            this.radRibbonBarGroup5.AccessibleDescription = "Open";
            this.radRibbonBarGroup5.AccessibleName = "Open";
            this.radRibbonBarGroup5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radDropDownButtonElement5,
            this.radButtonElement7,
            this.radRibbonBarButtonGroup3});
            this.radRibbonBarGroup5.Name = "radRibbonBarGroup5";
            this.radRibbonBarGroup5.Text = "Open";
            this.radRibbonBarGroup5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radDropDownButtonElement5
            // 
            this.radDropDownButtonElement5.AccessibleDescription = "Properties";
            this.radDropDownButtonElement5.AccessibleName = "Properties";
            this.radDropDownButtonElement5.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement5.ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            this.radDropDownButtonElement5.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement5.ExpandArrowButton = false;
            this.radDropDownButtonElement5.Image = global::FileExplorer.Properties.Resources._1_properties;
            this.radDropDownButtonElement5.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radDropDownButtonElement5.Name = "radDropDownButtonElement5";
            this.radDropDownButtonElement5.Text = "Properties";
            this.radDropDownButtonElement5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radDropDownButtonElement5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElement7
            // 
            this.radButtonElement7.AccessibleDescription = "Open";
            this.radButtonElement7.AccessibleName = "Open";
            this.radButtonElement7.Class = "RibbonBarButtonElement";
            this.radButtonElement7.Image = global::FileExplorer.Properties.Resources._1_open;
            this.radButtonElement7.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement7.Name = "radButtonElement7";
            this.radButtonElement7.Text = "Open";
            this.radButtonElement7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarButtonGroup3
            // 
            this.radRibbonBarButtonGroup3.AccessibleDescription = "radRibbonBarButtonGroup3";
            this.radRibbonBarButtonGroup3.AccessibleName = "radRibbonBarButtonGroup3";
            this.radRibbonBarButtonGroup3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radDropDownButtonElement6,
            this.radButtonElement9,
            this.radButtonElement10});
            this.radRibbonBarButtonGroup3.Name = "radRibbonBarButtonGroup3";
            this.radRibbonBarButtonGroup3.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup3.StretchVertically = true;
            this.radRibbonBarButtonGroup3.Text = "radRibbonBarButtonGroup3";
            this.radRibbonBarButtonGroup3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radDropDownButtonElement6
            // 
            this.radDropDownButtonElement6.AccessibleDescription = "Open with";
            this.radDropDownButtonElement6.AccessibleName = "Open with";
            this.radDropDownButtonElement6.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement6.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement6.ExpandArrowButton = false;
            this.radDropDownButtonElement6.Image = global::FileExplorer.Properties.Resources._1_open_with;
            this.radDropDownButtonElement6.Name = "radDropDownButtonElement6";
            this.radDropDownButtonElement6.Text = "Open with";
            this.radDropDownButtonElement6.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radDropDownButtonElement6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radDropDownButtonElement6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElement9
            // 
            this.radButtonElement9.AccessibleDescription = "History";
            this.radButtonElement9.AccessibleName = "History";
            this.radButtonElement9.Class = "ButtonElement";
            this.radButtonElement9.Image = global::FileExplorer.Properties.Resources._1_history;
            this.radButtonElement9.Name = "radButtonElement9";
            this.radButtonElement9.ShowBorder = false;
            this.radButtonElement9.Text = "History";
            this.radButtonElement9.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButtonElement9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButtonElement9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElement10
            // 
            this.radButtonElement10.AccessibleDescription = "Edit";
            this.radButtonElement10.AccessibleName = "Edit";
            this.radButtonElement10.Class = "ButtonElement";
            this.radButtonElement10.Image = global::FileExplorer.Properties.Resources._1_edit;
            this.radButtonElement10.Name = "radButtonElement10";
            this.radButtonElement10.ShowBorder = false;
            this.radButtonElement10.Text = "Edit";
            this.radButtonElement10.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButtonElement10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButtonElement10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup6
            // 
            this.radRibbonBarGroup6.AccessibleDescription = "Select";
            this.radRibbonBarGroup6.AccessibleName = "Select";
            this.radRibbonBarGroup6.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.selectAllButton,
            this.radRibbonBarButtonGroup4});
            this.radRibbonBarGroup6.Name = "radRibbonBarGroup6";
            this.radRibbonBarGroup6.Text = "Select";
            this.radRibbonBarGroup6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // selectAllButton
            // 
            this.selectAllButton.AccessibleDescription = "Select all";
            this.selectAllButton.AccessibleName = "Select all";
            this.selectAllButton.Class = "RibbonBarButtonElement";
            this.selectAllButton.Image = global::FileExplorer.Properties.Resources._1_select_all;
            this.selectAllButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.selectAllButton.Name = "selectAllButton";
            this.selectAllButton.Text = "Select all";
            this.selectAllButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.selectAllButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarButtonGroup4
            // 
            this.radRibbonBarButtonGroup4.AccessibleDescription = "radRibbonBarButtonGroup4";
            this.radRibbonBarButtonGroup4.AccessibleName = "radRibbonBarButtonGroup4";
            this.radRibbonBarButtonGroup4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement11,
            this.radButtonElement12});
            this.radRibbonBarButtonGroup4.Name = "radRibbonBarButtonGroup4";
            this.radRibbonBarButtonGroup4.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup4.StretchVertically = true;
            this.radRibbonBarButtonGroup4.Text = "radRibbonBarButtonGroup4";
            this.radRibbonBarButtonGroup4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElement11
            // 
            this.radButtonElement11.AccessibleDescription = "Selected none";
            this.radButtonElement11.AccessibleName = "Selected none";
            this.radButtonElement11.Class = "ButtonElement";
            this.radButtonElement11.Image = global::FileExplorer.Properties.Resources._1_select_none;
            this.radButtonElement11.Name = "radButtonElement11";
            this.radButtonElement11.ShowBorder = false;
            this.radButtonElement11.Text = "Selected none";
            this.radButtonElement11.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButtonElement11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButtonElement11.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElement12
            // 
            this.radButtonElement12.AccessibleDescription = "Invert selection";
            this.radButtonElement12.AccessibleName = "Invert selection";
            this.radButtonElement12.Class = "ButtonElement";
            this.radButtonElement12.Image = global::FileExplorer.Properties.Resources._1_invert_selection;
            this.radButtonElement12.Name = "radButtonElement12";
            this.radButtonElement12.ShowBorder = false;
            this.radButtonElement12.Text = "Invert selection";
            this.radButtonElement12.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButtonElement12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButtonElement12.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.AccessibleDescription = "Share";
            this.ribbonTab2.AccessibleName = "Share";
            this.ribbonTab2.Alignment = System.Drawing.ContentAlignment.BottomLeft;
            this.ribbonTab2.Class = "RibbonTab";
            this.ribbonTab2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup7,
            this.radRibbonBarGroup8,
            this.radRibbonBarGroup9,
            this.radRibbonBarGroup10});
            this.ribbonTab2.Name = "ribbonTab2";
            this.ribbonTab2.StretchHorizontally = false;
            this.ribbonTab2.Text = "Share";
            this.ribbonTab2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup7
            // 
            this.radRibbonBarGroup7.AccessibleDescription = "Send";
            this.radRibbonBarGroup7.AccessibleName = "Send";
            this.radRibbonBarGroup7.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement13,
            this.radButtonElement14,
            this.radButtonElement15});
            this.radRibbonBarGroup7.Name = "radRibbonBarGroup7";
            this.radRibbonBarGroup7.Text = "Send";
            this.radRibbonBarGroup7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElement13
            // 
            this.radButtonElement13.AccessibleDescription = "Burn";
            this.radButtonElement13.AccessibleName = "Burn";
            this.radButtonElement13.Class = "RibbonBarButtonElement";
            this.radButtonElement13.Image = global::FileExplorer.Properties.Resources._2_burn;
            this.radButtonElement13.Name = "radButtonElement13";
            this.radButtonElement13.Text = "Burn";
            this.radButtonElement13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement13.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElement14
            // 
            this.radButtonElement14.AccessibleDescription = "Email";
            this.radButtonElement14.AccessibleName = "Email";
            this.radButtonElement14.Class = "RibbonBarButtonElement";
            this.radButtonElement14.Image = global::FileExplorer.Properties.Resources._2_mail;
            this.radButtonElement14.Name = "radButtonElement14";
            this.radButtonElement14.Text = "Email";
            this.radButtonElement14.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement14.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElement15
            // 
            this.radButtonElement15.AccessibleDescription = "Fax";
            this.radButtonElement15.AccessibleName = "Fax";
            this.radButtonElement15.Class = "RibbonBarButtonElement";
            this.radButtonElement15.Image = global::FileExplorer.Properties.Resources._2_fax;
            this.radButtonElement15.Name = "radButtonElement15";
            this.radButtonElement15.Text = "Fax";
            this.radButtonElement15.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement15.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup8
            // 
            this.radRibbonBarGroup8.AccessibleDescription = "Sharing shortcuts";
            this.radRibbonBarGroup8.AccessibleName = "Sharing shortcuts";
            this.radRibbonBarGroup8.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radGalleryElement2});
            this.radRibbonBarGroup8.Name = "radRibbonBarGroup8";
            this.radRibbonBarGroup8.Text = "Sharing shortcuts";
            this.radRibbonBarGroup8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGalleryElement2
            // 
            this.radGalleryElement2.AccessibleDescription = "radGalleryElement2";
            this.radGalleryElement2.AccessibleName = "radGalleryElement2";
            this.radGalleryElement2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radGalleryItem7});
            this.radGalleryElement2.Name = "radGalleryElement2";
            this.radGalleryElement2.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            this.radGalleryElement2.Text = "radGalleryElement2";
            this.radGalleryElement2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGalleryItem7
            // 
            this.radGalleryItem7.AccessibleDescription = "Advanced sharing...";
            this.radGalleryItem7.AccessibleName = "Advanced sharing...";
            this.radGalleryItem7.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.radGalleryItem7.DescriptionText = "";
            this.radGalleryItem7.Name = "radGalleryItem7";
            this.radGalleryItem7.StretchHorizontally = false;
            this.radGalleryItem7.StretchVertically = false;
            this.radGalleryItem7.Text = "Advanced sharing...";
            this.radGalleryItem7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup9
            // 
            this.radRibbonBarGroup9.AccessibleDescription = "Security";
            this.radRibbonBarGroup9.AccessibleName = "Security";
            this.radRibbonBarGroup9.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup5});
            this.radRibbonBarGroup9.Name = "radRibbonBarGroup9";
            this.radRibbonBarGroup9.Text = "Security";
            this.radRibbonBarGroup9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarButtonGroup5
            // 
            this.radRibbonBarButtonGroup5.AccessibleDescription = "radRibbonBarButtonGroup5";
            this.radRibbonBarButtonGroup5.AccessibleName = "radRibbonBarButtonGroup5";
            this.radRibbonBarButtonGroup5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement16,
            this.radButtonElement17});
            this.radRibbonBarButtonGroup5.Name = "radRibbonBarButtonGroup5";
            this.radRibbonBarButtonGroup5.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup5.StretchVertically = true;
            this.radRibbonBarButtonGroup5.Text = "radRibbonBarButtonGroup5";
            this.radRibbonBarButtonGroup5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElement16
            // 
            this.radButtonElement16.AccessibleDescription = "Share with others";
            this.radButtonElement16.AccessibleName = "Share with others";
            this.radButtonElement16.Class = "ButtonElement";
            this.radButtonElement16.Image = global::FileExplorer.Properties.Resources._2_share_with_others;
            this.radButtonElement16.Name = "radButtonElement16";
            this.radButtonElement16.ShowBorder = false;
            this.radButtonElement16.Text = "Share with others";
            this.radButtonElement16.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButtonElement16.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElement17
            // 
            this.radButtonElement17.AccessibleDescription = "Advanced Security";
            this.radButtonElement17.AccessibleName = "Advanced Security";
            this.radButtonElement17.Class = "ButtonElement";
            this.radButtonElement17.Image = global::FileExplorer.Properties.Resources._2_advanced_security;
            this.radButtonElement17.Name = "radButtonElement17";
            this.radButtonElement17.ShowBorder = false;
            this.radButtonElement17.Text = "Advanced Security";
            this.radButtonElement17.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButtonElement17.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup10
            // 
            this.radRibbonBarGroup10.AccessibleDescription = "Add-ins";
            this.radRibbonBarGroup10.AccessibleName = "Add-ins";
            this.radRibbonBarGroup10.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radDropDownButtonElement7});
            this.radRibbonBarGroup10.Name = "radRibbonBarGroup10";
            this.radRibbonBarGroup10.Text = "Add-ins";
            this.radRibbonBarGroup10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radDropDownButtonElement7
            // 
            this.radDropDownButtonElement7.AccessibleDescription = "Web sharing";
            this.radDropDownButtonElement7.AccessibleName = "Web sharing";
            this.radDropDownButtonElement7.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement7.ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            this.radDropDownButtonElement7.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement7.ExpandArrowButton = false;
            this.radDropDownButtonElement7.Image = global::FileExplorer.Properties.Resources._2_web_sharing;
            this.radDropDownButtonElement7.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radDropDownButtonElement7.Name = "radDropDownButtonElement7";
            this.radDropDownButtonElement7.Text = "Web sharing";
            this.radDropDownButtonElement7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radDropDownButtonElement7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ribbonTab3
            // 
            this.ribbonTab3.AccessibleDescription = "View";
            this.ribbonTab3.AccessibleName = "View";
            this.ribbonTab3.Alignment = System.Drawing.ContentAlignment.BottomLeft;
            this.ribbonTab3.Class = "RibbonTab";
            this.ribbonTab3.IsSelected = false;
            this.ribbonTab3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup11,
            this.radRibbonBarGroup12,
            this.radRibbonBarGroup13,
            this.radRibbonBarGroup14,
            this.radRibbonBarGroup15});
            this.ribbonTab3.Name = "ribbonTab3";
            this.ribbonTab3.StretchHorizontally = false;
            this.ribbonTab3.Text = "View";
            this.ribbonTab3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup11
            // 
            this.radRibbonBarGroup11.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.refreshButtonInRibbon});
            this.radRibbonBarGroup11.Name = "radRibbonBarGroup11";
            this.radRibbonBarGroup11.Text = "";
            this.radRibbonBarGroup11.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // refreshButtonInRibbon
            // 
            this.refreshButtonInRibbon.AccessibleDescription = "Refresh";
            this.refreshButtonInRibbon.AccessibleName = "Refresh";
            this.refreshButtonInRibbon.Class = "RibbonBarButtonElement";
            this.refreshButtonInRibbon.Image = global::FileExplorer.Properties.Resources._3_refresh;
            this.refreshButtonInRibbon.Name = "refreshButtonInRibbon";
            this.refreshButtonInRibbon.Text = "Refresh";
            this.refreshButtonInRibbon.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.refreshButtonInRibbon.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.refreshButtonInRibbon.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // radRibbonBarGroup12
            // 
            this.radRibbonBarGroup12.AccessibleDescription = "Icon Size";
            this.radRibbonBarGroup12.AccessibleName = "Icon Size";
            this.radRibbonBarGroup12.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radGalleryElement3});
            this.radRibbonBarGroup12.Name = "radRibbonBarGroup12";
            this.radRibbonBarGroup12.Text = "Icon Size";
            this.radRibbonBarGroup12.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGalleryElement3
            // 
            this.radGalleryElement3.AccessibleDescription = "radGalleryElement3";
            this.radGalleryElement3.AccessibleName = "radGalleryElement3";
            this.radGalleryElement3.AutoSize = false;
            this.radGalleryElement3.Bounds = new System.Drawing.Rectangle(0, 0, 350, 67);
            this.radGalleryElement3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.extraLargeIconsGaleryItem,
            this.mediumIconsGaleryItem,
            this.listGaleryItem,
            this.largeIconsGaleryItem,
            this.smallIconsGaleryItem,
            this.detailsGaleryItem});
            this.radGalleryElement3.Name = "radGalleryElement3";
            this.radGalleryElement3.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            this.radGalleryElement3.Text = "radGalleryElement3";
            this.radGalleryElement3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // extraLargeIconsGaleryItem
            // 
            this.extraLargeIconsGaleryItem.AccessibleDescription = "Extra Large Icons";
            this.extraLargeIconsGaleryItem.AccessibleName = "Extra Large Icons";
            this.extraLargeIconsGaleryItem.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.extraLargeIconsGaleryItem.DescriptionText = "";
            this.extraLargeIconsGaleryItem.Image = global::FileExplorer.Properties.Resources._3_extra_large_icons;
            this.extraLargeIconsGaleryItem.Name = "extraLargeIconsGaleryItem";
            this.extraLargeIconsGaleryItem.StretchHorizontally = false;
            this.extraLargeIconsGaleryItem.StretchVertically = false;
            this.extraLargeIconsGaleryItem.Text = "Extra Large Icons";
            this.extraLargeIconsGaleryItem.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.extraLargeIconsGaleryItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.extraLargeIconsGaleryItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // mediumIconsGaleryItem
            // 
            this.mediumIconsGaleryItem.AccessibleDescription = "Medium Icons";
            this.mediumIconsGaleryItem.AccessibleName = "Medium Icons";
            this.mediumIconsGaleryItem.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.mediumIconsGaleryItem.DescriptionText = "";
            this.mediumIconsGaleryItem.Image = global::FileExplorer.Properties.Resources._3_medium_icons;
            this.mediumIconsGaleryItem.Name = "mediumIconsGaleryItem";
            this.mediumIconsGaleryItem.StretchHorizontally = false;
            this.mediumIconsGaleryItem.StretchVertically = false;
            this.mediumIconsGaleryItem.Text = "Medium Icons";
            this.mediumIconsGaleryItem.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.mediumIconsGaleryItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.mediumIconsGaleryItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.mediumIconsGaleryItem.Click += new System.EventHandler(this.mediumIconsGaleryItem_Click);
            // 
            // listGaleryItem
            // 
            this.listGaleryItem.AccessibleDescription = "List";
            this.listGaleryItem.AccessibleName = "List";
            this.listGaleryItem.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.listGaleryItem.DescriptionText = "";
            this.listGaleryItem.Image = global::FileExplorer.Properties.Resources._3_list;
            this.listGaleryItem.Name = "listGaleryItem";
            this.listGaleryItem.StretchHorizontally = false;
            this.listGaleryItem.StretchVertically = false;
            this.listGaleryItem.Text = "List";
            this.listGaleryItem.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.listGaleryItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.listGaleryItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.listGaleryItem.Click += new System.EventHandler(this.listGaleryItem_Click);
            // 
            // largeIconsGaleryItem
            // 
            this.largeIconsGaleryItem.AccessibleDescription = "Large Icons";
            this.largeIconsGaleryItem.AccessibleName = "Large Icons";
            this.largeIconsGaleryItem.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.largeIconsGaleryItem.DescriptionText = "";
            this.largeIconsGaleryItem.Image = global::FileExplorer.Properties.Resources._3_large_icons;
            this.largeIconsGaleryItem.Name = "largeIconsGaleryItem";
            this.largeIconsGaleryItem.StretchHorizontally = false;
            this.largeIconsGaleryItem.StretchVertically = false;
            this.largeIconsGaleryItem.Text = "Large Icons";
            this.largeIconsGaleryItem.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.largeIconsGaleryItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.largeIconsGaleryItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // smallIconsGaleryItem
            // 
            this.smallIconsGaleryItem.AccessibleDescription = "Small Icons";
            this.smallIconsGaleryItem.AccessibleName = "Small Icons";
            this.smallIconsGaleryItem.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.smallIconsGaleryItem.DescriptionText = "";
            this.smallIconsGaleryItem.Image = global::FileExplorer.Properties.Resources._3_small_cions;
            this.smallIconsGaleryItem.Name = "smallIconsGaleryItem";
            this.smallIconsGaleryItem.StretchHorizontally = false;
            this.smallIconsGaleryItem.StretchVertically = false;
            this.smallIconsGaleryItem.Text = "Small Icons";
            this.smallIconsGaleryItem.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.smallIconsGaleryItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.smallIconsGaleryItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.smallIconsGaleryItem.Click += new System.EventHandler(this.smallIconsGaleryItem_Click);
            // 
            // detailsGaleryItem
            // 
            this.detailsGaleryItem.AccessibleDescription = "Details";
            this.detailsGaleryItem.AccessibleName = "Details";
            this.detailsGaleryItem.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.detailsGaleryItem.DescriptionText = "";
            this.detailsGaleryItem.Image = global::FileExplorer.Properties.Resources._3_details;
            this.detailsGaleryItem.Name = "detailsGaleryItem";
            this.detailsGaleryItem.StretchHorizontally = false;
            this.detailsGaleryItem.StretchVertically = false;
            this.detailsGaleryItem.Text = "Details";
            this.detailsGaleryItem.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.detailsGaleryItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.detailsGaleryItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.detailsGaleryItem.Click += new System.EventHandler(this.detailsGaleryItem_Click);
            // 
            // radRibbonBarGroup13
            // 
            this.radRibbonBarGroup13.AccessibleDescription = "Current View";
            this.radRibbonBarGroup13.AccessibleName = "Current View";
            this.radRibbonBarGroup13.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.sortByButton,
            this.radRibbonBarButtonGroup6});
            this.radRibbonBarGroup13.Name = "radRibbonBarGroup13";
            this.radRibbonBarGroup13.Text = "Current View";
            this.radRibbonBarGroup13.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // sortByButton
            // 
            this.sortByButton.AccessibleDescription = "Sort by";
            this.sortByButton.AccessibleName = "Sort by";
            this.sortByButton.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.sortByButton.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.sortByButton.ExpandArrowButton = false;
            this.sortByButton.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.sortByNameMenuItem,
            this.sortByDateModifiedMenuItem,
            this.sortByTypeMenuItem,
            this.sortBySizeMenuItem});
            this.sortByButton.Name = "sortByButton";
            this.sortByButton.Text = "Sort by";
            this.sortByButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.sortByButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // sortByNameMenuItem
            // 
            this.sortByNameMenuItem.AccessibleDescription = "Name";
            this.sortByNameMenuItem.AccessibleName = "Name";
            this.sortByNameMenuItem.Name = "sortByNameMenuItem";
            this.sortByNameMenuItem.Text = "Name";
            this.sortByNameMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // sortByDateModifiedMenuItem
            // 
            this.sortByDateModifiedMenuItem.AccessibleDescription = "Date Modified";
            this.sortByDateModifiedMenuItem.AccessibleName = "Date Modified";
            this.sortByDateModifiedMenuItem.Name = "sortByDateModifiedMenuItem";
            this.sortByDateModifiedMenuItem.Text = "Date Modified";
            this.sortByDateModifiedMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // sortByTypeMenuItem
            // 
            this.sortByTypeMenuItem.AccessibleDescription = "Type";
            this.sortByTypeMenuItem.AccessibleName = "Type";
            this.sortByTypeMenuItem.Name = "sortByTypeMenuItem";
            this.sortByTypeMenuItem.Text = "Type";
            this.sortByTypeMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // sortBySizeMenuItem
            // 
            this.sortBySizeMenuItem.AccessibleDescription = "Size";
            this.sortBySizeMenuItem.AccessibleName = "Size";
            this.sortBySizeMenuItem.Name = "sortBySizeMenuItem";
            this.sortBySizeMenuItem.Text = "Size";
            this.sortBySizeMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarButtonGroup6
            // 
            this.radRibbonBarButtonGroup6.AccessibleDescription = "radRibbonBarButtonGroup6";
            this.radRibbonBarButtonGroup6.AccessibleName = "radRibbonBarButtonGroup6";
            this.radRibbonBarButtonGroup6.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radDropDownButtonElement9,
            this.radDropDownButtonElement10,
            this.radButtonElement19});
            this.radRibbonBarButtonGroup6.Name = "radRibbonBarButtonGroup6";
            this.radRibbonBarButtonGroup6.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup6.StretchVertically = true;
            this.radRibbonBarButtonGroup6.Text = "radRibbonBarButtonGroup6";
            this.radRibbonBarButtonGroup6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radDropDownButtonElement9
            // 
            this.radDropDownButtonElement9.AccessibleDescription = "Group by";
            this.radDropDownButtonElement9.AccessibleName = "Group by";
            this.radDropDownButtonElement9.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement9.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement9.ExpandArrowButton = false;
            this.radDropDownButtonElement9.Image = global::FileExplorer.Properties.Resources._3_group_by;
            this.radDropDownButtonElement9.Name = "radDropDownButtonElement9";
            this.radDropDownButtonElement9.Text = "Group by";
            this.radDropDownButtonElement9.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radDropDownButtonElement9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radDropDownButtonElement9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radDropDownButtonElement10
            // 
            this.radDropDownButtonElement10.AccessibleDescription = "Add columns";
            this.radDropDownButtonElement10.AccessibleName = "Add columns";
            this.radDropDownButtonElement10.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement10.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement10.ExpandArrowButton = false;
            this.radDropDownButtonElement10.Image = global::FileExplorer.Properties.Resources._3_add_column;
            this.radDropDownButtonElement10.Name = "radDropDownButtonElement10";
            this.radDropDownButtonElement10.Text = "Add columns";
            this.radDropDownButtonElement10.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radDropDownButtonElement10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radDropDownButtonElement10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElement19
            // 
            this.radButtonElement19.AccessibleDescription = "Size all columns";
            this.radButtonElement19.AccessibleName = "Size all columns";
            this.radButtonElement19.Class = "ButtonElement";
            this.radButtonElement19.Image = global::FileExplorer.Properties.Resources._3_size_all_columns;
            this.radButtonElement19.Name = "radButtonElement19";
            this.radButtonElement19.ShowBorder = false;
            this.radButtonElement19.Text = "Size all columns";
            this.radButtonElement19.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButtonElement19.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButtonElement19.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup14
            // 
            this.radRibbonBarGroup14.AccessibleDescription = "Layout";
            this.radRibbonBarGroup14.AccessibleName = "Layout";
            this.radRibbonBarGroup14.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radDropDownButtonElement11,
            this.radButtonElement20,
            this.radButtonElement21});
            this.radRibbonBarGroup14.Name = "radRibbonBarGroup14";
            this.radRibbonBarGroup14.Text = "Layout";
            this.radRibbonBarGroup14.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radDropDownButtonElement11
            // 
            this.radDropDownButtonElement11.AccessibleDescription = "Navigation pane";
            this.radDropDownButtonElement11.AccessibleName = "Navigation pane";
            this.radDropDownButtonElement11.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.radDropDownButtonElement11.ArrowPosition = Telerik.WinControls.UI.DropDownButtonArrowPosition.Bottom;
            this.radDropDownButtonElement11.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.radDropDownButtonElement11.ExpandArrowButton = false;
            this.radDropDownButtonElement11.Image = global::FileExplorer.Properties.Resources._3_navigation_pane;
            this.radDropDownButtonElement11.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radDropDownButtonElement11.Name = "radDropDownButtonElement11";
            this.radDropDownButtonElement11.Text = "Navigation pane";
            this.radDropDownButtonElement11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radDropDownButtonElement11.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElement20
            // 
            this.radButtonElement20.AccessibleDescription = "Show previews";
            this.radButtonElement20.AccessibleName = "Show previews";
            this.radButtonElement20.Class = "RibbonBarButtonElement";
            this.radButtonElement20.Image = global::FileExplorer.Properties.Resources._3_preview_pane;
            this.radButtonElement20.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement20.Name = "radButtonElement20";
            this.radButtonElement20.Text = "Show previews";
            this.radButtonElement20.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement20.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElement21
            // 
            this.radButtonElement21.AccessibleDescription = "Show details";
            this.radButtonElement21.AccessibleName = "Show details";
            this.radButtonElement21.Class = "RibbonBarButtonElement";
            this.radButtonElement21.Image = global::FileExplorer.Properties.Resources._3_detail_pane;
            this.radButtonElement21.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement21.Name = "radButtonElement21";
            this.radButtonElement21.Text = "Show details";
            this.radButtonElement21.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement21.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup15
            // 
            this.radRibbonBarGroup15.AccessibleDescription = "Show/hide";
            this.radRibbonBarGroup15.AccessibleName = "Show/hide";
            this.radRibbonBarGroup15.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup7,
            this.radButtonElement22});
            this.radRibbonBarGroup15.Name = "radRibbonBarGroup15";
            this.radRibbonBarGroup15.Text = "Show/hide";
            this.radRibbonBarGroup15.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarButtonGroup7
            // 
            this.radRibbonBarButtonGroup7.AccessibleDescription = "radRibbonBarButtonGroup7";
            this.radRibbonBarButtonGroup7.AccessibleName = "radRibbonBarButtonGroup7";
            this.radRibbonBarButtonGroup7.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radCheckBoxElement1,
            this.radCheckBoxElement2,
            this.radCheckBoxElement3});
            this.radRibbonBarButtonGroup7.Name = "radRibbonBarButtonGroup7";
            this.radRibbonBarButtonGroup7.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarButtonGroup7.StretchVertically = true;
            this.radRibbonBarButtonGroup7.Text = "radRibbonBarButtonGroup7";
            this.radRibbonBarButtonGroup7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCheckBoxElement1
            // 
            this.radCheckBoxElement1.AccessibleDescription = "Picture names";
            this.radCheckBoxElement1.AccessibleName = "Picture names";
            this.radCheckBoxElement1.Checked = false;
            this.radCheckBoxElement1.Name = "radCheckBoxElement1";
            this.radCheckBoxElement1.Text = "Picture names";
            this.radCheckBoxElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCheckBoxElement2
            // 
            this.radCheckBoxElement2.AccessibleDescription = "File extensions";
            this.radCheckBoxElement2.AccessibleName = "File extensions";
            this.radCheckBoxElement2.Checked = false;
            this.radCheckBoxElement2.Name = "radCheckBoxElement2";
            this.radCheckBoxElement2.Text = "File extensions";
            this.radCheckBoxElement2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radCheckBoxElement3
            // 
            this.radCheckBoxElement3.AccessibleDescription = "Hidden files";
            this.radCheckBoxElement3.AccessibleName = "Hidden files";
            this.radCheckBoxElement3.Checked = false;
            this.radCheckBoxElement3.Name = "radCheckBoxElement3";
            this.radCheckBoxElement3.Text = "Hidden files";
            this.radCheckBoxElement3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElement22
            // 
            this.radButtonElement22.AccessibleDescription = "Hide selected file";
            this.radButtonElement22.AccessibleName = "Hide selected file";
            this.radButtonElement22.Class = "RibbonBarButtonElement";
            this.radButtonElement22.Image = global::FileExplorer.Properties.Resources._3_hide_selected_file;
            this.radButtonElement22.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElement22.Name = "radButtonElement22";
            this.radButtonElement22.Text = "Hide selected file";
            this.radButtonElement22.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElement22.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // ribbonTab4
            // 
            this.ribbonTab4.AccessibleDescription = "Themes";
            this.ribbonTab4.AccessibleName = "Themes";
            this.ribbonTab4.Class = "RibbonTab";
            this.ribbonTab4.IsSelected = false;
            this.ribbonTab4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup16});
            this.ribbonTab4.Name = "ribbonTab4";
            this.ribbonTab4.Text = "Themes";
            this.ribbonTab4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup16
            // 
            this.radRibbonBarGroup16.AccessibleDescription = "Themes";
            this.radRibbonBarGroup16.AccessibleName = "Themes";
            this.radRibbonBarGroup16.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radGalleryElement4});
            this.radRibbonBarGroup16.Name = "radRibbonBarGroup16";
            this.radRibbonBarGroup16.Text = "Themes";
            this.radRibbonBarGroup16.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radGalleryElement4
            // 
            this.radGalleryElement4.AccessibleDescription = "radGalleryElement4";
            this.radGalleryElement4.AccessibleName = "radGalleryElement4";
            this.radGalleryElement4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.office2010BlackButton,
            this.office2010BlueButton,
            this.office2010SilverButton,
            this.controlDefaultButton,
            this.telerikMetroButton});
            this.radGalleryElement4.Name = "radGalleryElement4";
            this.radGalleryElement4.Padding = new System.Windows.Forms.Padding(2, 2, 0, 0);
            this.radGalleryElement4.Text = "radGalleryElement4";
            this.radGalleryElement4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // office2010BlackButton
            // 
            this.office2010BlackButton.AccessibleDescription = "Office2010Black";
            this.office2010BlackButton.AccessibleName = "Office2010Black";
            this.office2010BlackButton.DescriptionText = "";
            this.office2010BlackButton.Image = global::FileExplorer.Properties.Resources.Office2010Black;
            this.office2010BlackButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.office2010BlackButton.Name = "office2010BlackButton";
            this.office2010BlackButton.StretchHorizontally = false;
            this.office2010BlackButton.StretchVertically = false;
            this.office2010BlackButton.Text = "Office2010Black";
            this.office2010BlackButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.office2010BlackButton.ToolTipText = "Office2010Black";
            this.office2010BlackButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // office2010BlueButton
            // 
            this.office2010BlueButton.AccessibleDescription = "Office2010Blue";
            this.office2010BlueButton.AccessibleName = "Office2010Blue";
            this.office2010BlueButton.DescriptionText = "";
            this.office2010BlueButton.Image = global::FileExplorer.Properties.Resources.Office2010Blue;
            this.office2010BlueButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.office2010BlueButton.Name = "office2010BlueButton";
            this.office2010BlueButton.StretchHorizontally = false;
            this.office2010BlueButton.StretchVertically = false;
            this.office2010BlueButton.Text = "Office2010Blue";
            this.office2010BlueButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.office2010BlueButton.ToolTipText = "Office2010Blue";
            this.office2010BlueButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // office2010SilverButton
            // 
            this.office2010SilverButton.AccessibleDescription = "Office2010Silver";
            this.office2010SilverButton.AccessibleName = "Office2010Silver";
            this.office2010SilverButton.DescriptionText = "";
            this.office2010SilverButton.Image = global::FileExplorer.Properties.Resources.Office2010Silver;
            this.office2010SilverButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.office2010SilverButton.Name = "office2010SilverButton";
            this.office2010SilverButton.StretchHorizontally = false;
            this.office2010SilverButton.StretchVertically = false;
            this.office2010SilverButton.Text = "Office2010Silver";
            this.office2010SilverButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.office2010SilverButton.ToolTipText = "Office2010Silver";
            this.office2010SilverButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // controlDefaultButton
            // 
            this.controlDefaultButton.AccessibleDescription = "ControlDefault";
            this.controlDefaultButton.AccessibleName = "ControlDefault";
            this.controlDefaultButton.DescriptionText = "";
            this.controlDefaultButton.Image = global::FileExplorer.Properties.Resources.ControlDefault;
            this.controlDefaultButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.controlDefaultButton.Name = "controlDefaultButton";
            this.controlDefaultButton.StretchHorizontally = false;
            this.controlDefaultButton.StretchVertically = false;
            this.controlDefaultButton.Text = "ControlDefault";
            this.controlDefaultButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.controlDefaultButton.ToolTipText = "ControlDefault";
            this.controlDefaultButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // telerikMetroButton
            // 
            this.telerikMetroButton.AccessibleDescription = "TelerikMetro";
            this.telerikMetroButton.AccessibleName = "TelerikMetro";
            this.telerikMetroButton.DescriptionText = "";
            this.telerikMetroButton.Image = global::FileExplorer.Properties.Resources.Telerik;
            this.telerikMetroButton.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.telerikMetroButton.Name = "telerikMetroButton";
            this.telerikMetroButton.StretchHorizontally = false;
            this.telerikMetroButton.StretchVertically = false;
            this.telerikMetroButton.Text = "TelerikMetro";
            this.telerikMetroButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.telerikMetroButton.ToolTipText = "TelerikMetro";
            this.telerikMetroButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // openNewWindowMenuItem
            // 
            this.openNewWindowMenuItem.AccessibleDescription = "Open New Window";
            this.openNewWindowMenuItem.AccessibleName = "Open New Window";
            this.openNewWindowMenuItem.Name = "openNewWindowMenuItem";
            this.openNewWindowMenuItem.Text = "Open New Window";
            this.openNewWindowMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // openCommandPromptMenuItem
            // 
            this.openCommandPromptMenuItem.AccessibleDescription = "Open command prompt";
            this.openCommandPromptMenuItem.AccessibleName = "Open command prompt";
            this.openCommandPromptMenuItem.Name = "openCommandPromptMenuItem";
            this.openCommandPromptMenuItem.Text = "Open command prompt";
            this.openCommandPromptMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem1
            // 
            this.radMenuSeparatorItem1.AccessibleDescription = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.AccessibleName = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Text = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // addSelectedFolderToFavoritesMenuItem
            // 
            this.addSelectedFolderToFavoritesMenuItem.AccessibleDescription = "Add selected folder to favorites";
            this.addSelectedFolderToFavoritesMenuItem.AccessibleName = "Add selected folder to favorites";
            this.addSelectedFolderToFavoritesMenuItem.Name = "addSelectedFolderToFavoritesMenuItem";
            this.addSelectedFolderToFavoritesMenuItem.Text = "Add selected folder to favorites";
            this.addSelectedFolderToFavoritesMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // includeInLibraryMenuItem
            // 
            this.includeInLibraryMenuItem.AccessibleDescription = "Include in library";
            this.includeInLibraryMenuItem.AccessibleName = "Include in library";
            this.includeInLibraryMenuItem.Name = "includeInLibraryMenuItem";
            this.includeInLibraryMenuItem.Text = "Include in library";
            this.includeInLibraryMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // mapSelectedFolderAsDriveMenuItem
            // 
            this.mapSelectedFolderAsDriveMenuItem.AccessibleDescription = "Map selected folder as drive";
            this.mapSelectedFolderAsDriveMenuItem.AccessibleName = "Map selected folder as drive";
            this.mapSelectedFolderAsDriveMenuItem.Name = "mapSelectedFolderAsDriveMenuItem";
            this.mapSelectedFolderAsDriveMenuItem.Text = "Map selected folder as drive";
            this.mapSelectedFolderAsDriveMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem2
            // 
            this.radMenuSeparatorItem2.AccessibleDescription = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.AccessibleName = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Name = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Text = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // clearHistoryMenuItem
            // 
            this.clearHistoryMenuItem.AccessibleDescription = "Clear history";
            this.clearHistoryMenuItem.AccessibleName = "Clear history";
            this.clearHistoryMenuItem.Name = "clearHistoryMenuItem";
            this.clearHistoryMenuItem.Text = "Clear history";
            this.clearHistoryMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem3
            // 
            this.radMenuSeparatorItem3.AccessibleDescription = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.AccessibleName = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Name = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Text = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // printMenuItem
            // 
            this.printMenuItem.AccessibleDescription = "Print";
            this.printMenuItem.AccessibleName = "Print";
            this.printMenuItem.Name = "printMenuItem";
            this.printMenuItem.Text = "Print";
            this.printMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem4
            // 
            this.radMenuSeparatorItem4.AccessibleDescription = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.AccessibleName = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Name = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Text = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // syncMenuItem
            // 
            this.syncMenuItem.AccessibleDescription = "Sync";
            this.syncMenuItem.AccessibleName = "Sync";
            this.syncMenuItem.Name = "syncMenuItem";
            this.syncMenuItem.Text = "Sync";
            this.syncMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem5
            // 
            this.radMenuSeparatorItem5.AccessibleDescription = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.AccessibleName = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.Name = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.Text = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // folderOptionsMenuItem
            // 
            this.folderOptionsMenuItem.AccessibleDescription = "Folder options";
            this.folderOptionsMenuItem.AccessibleName = "Folder options";
            this.folderOptionsMenuItem.Name = "folderOptionsMenuItem";
            this.folderOptionsMenuItem.Text = "Folder options";
            this.folderOptionsMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem6
            // 
            this.radMenuSeparatorItem6.AccessibleDescription = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.AccessibleName = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.Name = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.Text = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // helpMenuItem
            // 
            this.helpMenuItem.AccessibleDescription = "Help";
            this.helpMenuItem.AccessibleName = "Help";
            this.helpMenuItem.Name = "helpMenuItem";
            this.helpMenuItem.Text = "Help";
            this.helpMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem7
            // 
            this.radMenuSeparatorItem7.AccessibleDescription = "radMenuSeparatorItem7";
            this.radMenuSeparatorItem7.AccessibleName = "radMenuSeparatorItem7";
            this.radMenuSeparatorItem7.Name = "radMenuSeparatorItem7";
            this.radMenuSeparatorItem7.Text = "radMenuSeparatorItem7";
            this.radMenuSeparatorItem7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // closeMenuItem
            // 
            this.closeMenuItem.AccessibleDescription = "Close";
            this.closeMenuItem.AccessibleName = "Close";
            this.closeMenuItem.Name = "closeMenuItem";
            this.closeMenuItem.Text = "Close";
            this.closeMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // frequentPlacesMenuItem
            // 
            this.frequentPlacesMenuItem.AccessibleDescription = "Frequent Places";
            this.frequentPlacesMenuItem.AccessibleName = "Frequent Places";
            this.frequentPlacesMenuItem.Name = "frequentPlacesMenuItem";
            this.frequentPlacesMenuItem.Text = "Frequent Places";
            this.frequentPlacesMenuItem.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem8
            // 
            this.radMenuSeparatorItem8.AccessibleDescription = "radMenuSeparatorItem8";
            this.radMenuSeparatorItem8.AccessibleName = "radMenuSeparatorItem8";
            this.radMenuSeparatorItem8.Name = "radMenuSeparatorItem8";
            this.radMenuSeparatorItem8.Text = "radMenuSeparatorItem8";
            this.radMenuSeparatorItem8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // RadRibbonForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1065, 610);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.radStatusStrip1);
            this.Controls.Add(this.radPanel1);
            this.Controls.Add(this.radRibbonBar1);
            this.MainMenuStrip = null;
            this.Name = "RadRibbonForm1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "File Explorer";
            this.ThemeName = "ControlDefault";
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBreadCrumb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radListView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.historyButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.forwardButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox1;
        private Telerik.WinControls.UI.RadButton refreshButton;
        private Telerik.WinControls.UI.RadBreadCrumb radBreadCrumb1;
        private Telerik.WinControls.UI.RadButton forwardButton;
        private Telerik.WinControls.UI.RadButton backButton;
        private Telerik.WinControls.UI.RadTreeView radTreeView1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.Themes.Office2010BlackTheme office2010BlackTheme1;
        private Telerik.WinControls.Themes.Office2010BlueTheme office2010BlueTheme1;
        private Telerik.WinControls.Themes.Office2010SilverTheme office2010SilverTheme1;
        private Telerik.WinControls.Themes.TelerikMetroTheme telerikMetroTheme1;
        private Telerik.WinControls.UI.RadRibbonBar radRibbonBar1;
        private Telerik.WinControls.UI.RibbonTab ribbonTab1;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup1;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement1;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup1;
        private Telerik.WinControls.UI.RadButtonElement cutButton;
        private Telerik.WinControls.UI.RadButtonElement copyButton;
        private Telerik.WinControls.UI.RadButtonElement copyAsPathButton;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup2;
        private Telerik.WinControls.UI.RadButtonElement newFolderButton;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement2;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup3;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement3;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup2;
        private Telerik.WinControls.UI.RadDropDownButtonElement copyToFolderButton;
        private Telerik.WinControls.UI.RadButtonElement renameButton;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement6;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup4;
        private Telerik.WinControls.UI.RadGalleryElement radGalleryElement1;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem1;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem2;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem3;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem4;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem5;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem6;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup5;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement5;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement7;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup3;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement6;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement9;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement10;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup6;
        private Telerik.WinControls.UI.RadButtonElement selectAllButton;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup4;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement11;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement12;
        private Telerik.WinControls.UI.RibbonTab ribbonTab2;
        private Telerik.WinControls.UI.RibbonTab ribbonTab3;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup7;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement13;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement14;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement15;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup8;
        private Telerik.WinControls.UI.RadGalleryElement radGalleryElement2;
        private Telerik.WinControls.UI.RadGalleryItem radGalleryItem7;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup9;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup5;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement16;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement17;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup10;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement7;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup11;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup12;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup13;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup14;
        private Telerik.WinControls.UI.RadButtonElement refreshButtonInRibbon;
        private Telerik.WinControls.UI.RadGalleryElement radGalleryElement3;
        private Telerik.WinControls.UI.RadGalleryItem extraLargeIconsGaleryItem;
        private Telerik.WinControls.UI.RadGalleryItem mediumIconsGaleryItem;
        private Telerik.WinControls.UI.RadGalleryItem listGaleryItem;
        private Telerik.WinControls.UI.RadGalleryItem largeIconsGaleryItem;
        private Telerik.WinControls.UI.RadGalleryItem smallIconsGaleryItem;
        private Telerik.WinControls.UI.RadGalleryItem detailsGaleryItem;
        private Telerik.WinControls.UI.RadDropDownButtonElement sortByButton;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup6;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement9;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement10;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement19;
        private Telerik.WinControls.UI.RadDropDownButtonElement radDropDownButtonElement11;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement20;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement21;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup15;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup7;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement1;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement2;
        private Telerik.WinControls.UI.RadCheckBoxElement radCheckBoxElement3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement22;
        private Telerik.WinControls.UI.RadButton upButton;
        private Telerik.WinControls.UI.RadMenuItem openNewWindowMenuItem;
        private Telerik.WinControls.UI.RadMenuItem openCommandPromptMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem1;
        private Telerik.WinControls.UI.RadMenuItem addSelectedFolderToFavoritesMenuItem;
        private Telerik.WinControls.UI.RadMenuItem includeInLibraryMenuItem;
        private Telerik.WinControls.UI.RadMenuItem mapSelectedFolderAsDriveMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem2;
        private Telerik.WinControls.UI.RadMenuItem clearHistoryMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem3;
        private Telerik.WinControls.UI.RadMenuItem printMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem4;
        private Telerik.WinControls.UI.RadMenuItem syncMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem5;
        private Telerik.WinControls.UI.RadMenuItem folderOptionsMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem6;
        private Telerik.WinControls.UI.RadMenuItem helpMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem7;
        private Telerik.WinControls.UI.RadMenuItem closeMenuItem;
        private Telerik.WinControls.UI.RadMenuHeaderItem frequentPlacesMenuItem;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem8;
        private Telerik.WinControls.UI.RadDropDownButton historyButton;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadLabelElement directoryInfoLabel;
        private Telerik.WinControls.UI.CommandBarSeparator radCommandBarSeparatorItem1;
        private Telerik.WinControls.UI.RadLabelElement selectedItemInfoLabel;
        private Telerik.WinControls.UI.RadLabelElement separatorLabel;
        private Telerik.WinControls.UI.RadListView radListView1;
        private Telerik.WinControls.UI.RadMenuItem pasteMenuItem;
        private Telerik.WinControls.UI.RadMenuItem pasteAsShortcutMenuItem;
        private Telerik.WinControls.UI.RadMenuItem deleteSelectionMenuItem;
        private Telerik.WinControls.UI.RadMenuItem deleteAllMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private Telerik.WinControls.UI.RadMenuItem sortByTypeMenuItem;
        private Telerik.WinControls.UI.RadMenuItem sortByDateModifiedMenuItem;
        private Telerik.WinControls.UI.RadMenuItem sortBySizeMenuItem;
        private Telerik.WinControls.UI.RadMenuItem sortByNameMenuItem;
        private Telerik.WinControls.UI.RibbonTab ribbonTab4;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup16;
        private Telerik.WinControls.UI.RadGalleryElement radGalleryElement4;
        private Telerik.WinControls.UI.RadGalleryItem office2010BlackButton;
        private Telerik.WinControls.UI.RadGalleryItem office2010BlueButton;
        private Telerik.WinControls.UI.RadGalleryItem office2010SilverButton;
        private Telerik.WinControls.UI.RadGalleryItem controlDefaultButton;
        private Telerik.WinControls.UI.RadGalleryItem telerikMetroButton;
    }
}
