﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookShelf
{
    public enum PageID
    {
        NotInitialized,
        Main,
        AllCategories,
        AllCategoriesZoomedOut,
        SingleCategory,
        SingleCategoryZommedOut,
        Details,
        Search
    }
}
