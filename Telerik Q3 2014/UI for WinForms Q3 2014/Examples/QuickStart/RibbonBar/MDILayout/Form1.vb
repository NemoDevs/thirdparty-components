Imports System.ComponentModel
Imports System.Drawing.Drawing2D
Imports System.Text
Imports Telerik.WinControls
Imports Telerik.WinControls.UI
Imports Telerik.QuickStart.WinControls
Imports Telerik.WinControls.Themes

Namespace Telerik.Examples.WinControls.RibbonBar.MDILayout
	Partial Public Class Form1
		Inherits Telerik.WinControls.UI.RadRibbonForm
		Private mdiChildCount As Integer = 0

		Public Sub New()
			InitializeComponent()

			WireEvents()

			Me.AllowAero = False

            Me.radRibbonBar1.RibbonBarElement.TabStripElement.SelectedItem = Me.radRibbonBar1.RibbonBarElement.TabStripElement.Items(0)
            Me.radRibbonBar1.StartButtonImage = Telerik.WinControls.ResFinder.LogoBlack16
			Me.IsMdiContainer = True
			Me.MinimumSize = New Size(210, 140)
		End Sub

		Private Sub radRibbonBarChunk2_Click(ByVal sender As Object, ByVal e As EventArgs)
			Me.AddNewChildForm()
		End Sub

		Private Sub WireEvents()
			AddHandler radButtonElement5.Click, AddressOf radButtonElement5_Click
			AddHandler radRibbonBarChunk2.Click, AddressOf radRibbonBarChunk2_Click
			AddHandler radButtonElement29.Click, AddressOf radButtonElement29_Click
			AddHandler radButtonElement2.Click, AddressOf radButtonElement2_Click
			AddHandler radButtonElement3.Click, AddressOf radButtonElement3_Click
			AddHandler radButtonElement1.Click, AddressOf radButtonElement1_Click
		End Sub

		Private Sub PrepareMDIContainerForThemeChange()
			If Me.ActiveMdiChild Is Nothing Then
				Return
			End If
			If Me.ActiveMdiChild.WindowState = FormWindowState.Maximized Then
				Me.ActiveMdiChild.WindowState = FormWindowState.Normal
			End If
		End Sub

		Private Sub AddNewChildForm()
			Dim form As Form = If(Me.MdiChildren.Length Mod 2 = 0, New Form(), New Telerik.WinControls.UI.RadForm())
			form.MdiParent = Me
'INSTANT VB TODO TASK: Assignments within expressions are not supported in VB.NET
            'ORIGINAL LINE: form.Text = "MDI Child Form " + (Me.mdiChildCount += 1);
            Me.mdiChildCount = Me.mdiChildCount + 1
            form.Text = "MDI Child Form " & Me.mdiChildCount
			form.Show()
		End Sub

		Protected Overrides Overloads Sub OnLoad(ByVal e As EventArgs)
			MyBase.OnLoad(e)

			AddNewChildForm()
			AddNewChildForm()
			AddNewChildForm()
			AddNewChildForm()
		End Sub

		Private Sub radButtonElement5_Click(ByVal sender As Object, ByVal e As EventArgs)
			Me.AddNewChildForm()
		End Sub

		Private Sub radButtonElement29_Click(ByVal sender As Object, ByVal e As EventArgs)
			If Me.ActiveMdiChild IsNot Nothing Then
				Me.ActiveMdiChild.Close()
			End If
		End Sub

		Private Sub radButtonElement1_Click(ByVal sender As Object, ByVal e As EventArgs)
			Me.PrepareMDIContainerForThemeChange()
			ThemeResolutionService.ApplyThemeToControlTree(Me, "ControlDefault")
		End Sub

		Private Sub radButtonElement2_Click(ByVal sender As Object, ByVal e As EventArgs)
			Me.PrepareMDIContainerForThemeChange()
			ThemeResolutionService.ApplyThemeToControlTree(Me, "Office2010Black")
		End Sub

		Private Sub radButtonElement3_Click(ByVal sender As Object, ByVal e As EventArgs)
			Me.PrepareMDIContainerForThemeChange()
			ThemeResolutionService.ApplyThemeToControlTree(Me, "Office2010Silver")
		End Sub
	End Class
End Namespace