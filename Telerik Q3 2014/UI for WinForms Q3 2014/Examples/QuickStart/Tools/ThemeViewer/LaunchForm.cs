﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Telerik.Examples.WinControls.Tools.ThemeViewer
{
    public partial class LaunchForm : Telerik.QuickStart.WinControls.ExamplesLaucherForm
    {
        public LaunchForm()
        {
            InitializeComponent();

            this.pictureBoxLaunchExample.Image = Telerik.Examples.WinControls.Properties.Resources.launch ;

        }
    }
}
