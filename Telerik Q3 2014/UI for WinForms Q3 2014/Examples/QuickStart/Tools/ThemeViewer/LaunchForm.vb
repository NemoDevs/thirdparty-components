﻿Namespace Telerik.Examples.WinControls.Tools.ThemeViewer
    Partial Public Class LaunchForm
        Private Sub LaunchForm_Load(sender As Object, e As System.EventArgs) Handles Me.Load
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LaunchForm))
            Me.pictureBoxLaunchExample.Image = CType(Resources.GetObject("pictureBoxLaunchExample.Image"), System.Drawing.Image)
        End Sub
    End Class
End Namespace
