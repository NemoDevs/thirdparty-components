﻿Imports System.Reflection
Imports System.Resources
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

<Assembly: AssemblyTitle("Examples")>

<Assembly: AssemblyDescription("")>
<Assembly: AssemblyConfiguration("")>
<Assembly: AssemblyCompany(Telerik.WinControls.VersionNumber.CompanyName)>
<Assembly: AssemblyProduct("Examples")>
<Assembly: AssemblyCopyright(Telerik.WinControls.VersionNumber.CopyrightText)>
<Assembly: AssemblyTrademark("")>
<Assembly: AssemblyCulture("")>

' Setting ComVisible to false makes the types in this assembly not visible 
' to COM components.  If you need to access a type in this assembly from 
' COM, set the ComVisible attribute to true on that type.
<Assembly: ComVisible(False)> 

#If NET2 Then

<Assembly: CLSCompliant(True)> 

#End If

#If NET4 Then

<Assembly: CLSCompliant(False)> 

#End If



' The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("15243bcc-c612-44fa-a5d2-f936ca1f71e0")>
<Assembly: NeutralResourcesLanguage("en-US")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
<Assembly: AssemblyVersion(Telerik.WinControls.VersionNumber.Number)>
<Assembly: AssemblyFileVersion(Telerik.WinControls.VersionNumber.Number)>
