﻿namespace Telerik.Examples.WinControls.DesktopAlert.Settings
{
    partial class Form1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem13 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem15 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem16 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem17 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.spinOpacity = new Telerik.WinControls.UI.RadSpinEditor();
            this.optionsButtonCheck = new Telerik.WinControls.UI.RadCheckBox();
            this.pinButtonCheck = new Telerik.WinControls.UI.RadCheckBox();
            this.closeButtonCheck = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.spinEditorHeight = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.spinEditorWidth = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.ddScreenPosition = new Telerik.WinControls.UI.RadDropDownList();
            this.ddThemeName = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.ddAnimationDirection = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.checkPopupAnimation = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.fadeOutCheck = new Telerik.WinControls.UI.RadCheckBox();
            this.fadeInCheck = new Telerik.WinControls.UI.RadCheckBox();
            this.spinPopupAnimationFrames = new Telerik.WinControls.UI.RadSpinEditor();
            this.autoCloseDelaySpin = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.autoCloseCheck = new Telerik.WinControls.UI.RadCheckBox();
            this.spinFadeDuration = new Telerik.WinControls.UI.RadSpinEditor();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.txtContent = new Telerik.WinControls.UI.RadTextBox();
            this.txtCaption = new Telerik.WinControls.UI.RadTextBox();
            this.btnPreview = new Telerik.WinControls.UI.RadButton();
            this.radDesktopAlert1 = new Telerik.WinControls.UI.RadDesktopAlert(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).BeginInit();
            this.radPanelDemoHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinOpacity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optionsButtonCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pinButtonCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeButtonCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddScreenPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddThemeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddAnimationDirection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPopupAnimation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fadeOutCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fadeInCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinPopupAnimationFrames)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoCloseDelaySpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoCloseCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinFadeDuration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanelDemoHolder
            // 
            this.radPanelDemoHolder.Controls.Add(this.btnPreview);
            this.radPanelDemoHolder.Controls.Add(this.radGroupBox3);
            this.radPanelDemoHolder.Controls.Add(this.radGroupBox2);
            this.radPanelDemoHolder.Controls.Add(this.radGroupBox1);
            this.radPanelDemoHolder.ForeColor = System.Drawing.Color.Black;
            this.radPanelDemoHolder.Size = new System.Drawing.Size(652, 375);
            this.radPanelDemoHolder.ThemeName = "TelerikMetro";
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(717, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 730);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.spinOpacity);
            this.radGroupBox1.Controls.Add(this.optionsButtonCheck);
            this.radGroupBox1.Controls.Add(this.pinButtonCheck);
            this.radGroupBox1.Controls.Add(this.closeButtonCheck);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.radLabel12);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.spinEditorHeight);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.spinEditorWidth);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.ddScreenPosition);
            this.radGroupBox1.Controls.Add(this.ddThemeName);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.HeaderText = "Appearance";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox1.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox1.Size = new System.Drawing.Size(200, 314);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "Appearance";
            // 
            // spinOpacity
            // 
            this.spinOpacity.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinOpacity.DecimalPlaces = 1;
            this.spinOpacity.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.spinOpacity.Location = new System.Drawing.Point(93, 277);
            this.spinOpacity.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinOpacity.Name = "spinOpacity";
            // 
            // 
            // 
            this.spinOpacity.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.spinOpacity.Size = new System.Drawing.Size(93, 20);
            this.spinOpacity.TabIndex = 1;
            this.spinOpacity.TabStop = false;
            this.spinOpacity.Value = new decimal(new int[] {
            8,
            0,
            0,
            65536});
    
            // 
            // optionsButtonCheck
            // 
            this.optionsButtonCheck.AutoSize = false;
            this.optionsButtonCheck.Location = new System.Drawing.Point(17, 253);
            this.optionsButtonCheck.Name = "optionsButtonCheck";
            this.optionsButtonCheck.Size = new System.Drawing.Size(120, 18);
            this.optionsButtonCheck.TabIndex = 4;
            this.optionsButtonCheck.Text = "Options button";
            this.optionsButtonCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
    
            // 
            // pinButtonCheck
            // 
            this.pinButtonCheck.AutoSize = false;
            this.pinButtonCheck.Location = new System.Drawing.Point(107, 229);
            this.pinButtonCheck.Name = "pinButtonCheck";
            this.pinButtonCheck.Size = new System.Drawing.Size(90, 18);
            this.pinButtonCheck.TabIndex = 4;
            this.pinButtonCheck.Text = "Pin button";
            this.pinButtonCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
     
            // 
            // closeButtonCheck
            // 
            this.closeButtonCheck.AutoSize = false;
            this.closeButtonCheck.Location = new System.Drawing.Point(17, 229);
            this.closeButtonCheck.Name = "closeButtonCheck";
            this.closeButtonCheck.Size = new System.Drawing.Size(105, 18);
            this.closeButtonCheck.TabIndex = 4;
            this.closeButtonCheck.Text = "Close button";
            this.closeButtonCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(32, 192);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(42, 18);
            this.radLabel5.TabIndex = 3;
            this.radLabel5.Text = "Height:";
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(13, 277);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(72, 18);
            this.radLabel12.TabIndex = 3;
            this.radLabel12.Text = "Alert opacity:";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(32, 168);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(39, 18);
            this.radLabel4.TabIndex = 3;
            this.radLabel4.Text = "Width:";
            // 
            // spinEditorHeight
            // 
            this.spinEditorHeight.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditorHeight.Location = new System.Drawing.Point(79, 192);
            this.spinEditorHeight.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.spinEditorHeight.Minimum = new decimal(new int[] {
            70,
            0,
            0,
            0});
            this.spinEditorHeight.Name = "spinEditorHeight";
            // 
            // 
            // 
            this.spinEditorHeight.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.spinEditorHeight.Size = new System.Drawing.Size(107, 20);
            this.spinEditorHeight.TabIndex = 1;
            this.spinEditorHeight.TabStop = false;
            this.spinEditorHeight.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
       
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(13, 144);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(54, 18);
            this.radLabel3.TabIndex = 3;
            this.radLabel3.Text = "Alert size:";
            // 
            // spinEditorWidth
            // 
            this.spinEditorWidth.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinEditorWidth.Location = new System.Drawing.Point(79, 168);
            this.spinEditorWidth.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.spinEditorWidth.Minimum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.spinEditorWidth.Name = "spinEditorWidth";
            // 
            // 
            // 
            this.spinEditorWidth.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.spinEditorWidth.Size = new System.Drawing.Size(107, 20);
            this.spinEditorWidth.TabIndex = 1;
            this.spinEditorWidth.TabStop = false;
            this.spinEditorWidth.Value = new decimal(new int[] {
            329,
            0,
            0,
            0});
      
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(14, 89);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(90, 18);
            this.radLabel2.TabIndex = 3;
            this.radLabel2.Text = "Choose position:";
            // 
            // ddScreenPosition
            // 
            this.ddScreenPosition.DropDownAnimationEnabled = false;
            this.ddScreenPosition.DropDownHeight = 0;
            radListDataItem5.Text = "TopLeft";
            radListDataItem5.TextWrap = true;
            radListDataItem6.Text = "TopCenter";
            radListDataItem6.TextWrap = true;
            radListDataItem7.Text = "TopRight";
            radListDataItem7.TextWrap = true;
            radListDataItem8.Text = "BottomLeft";
            radListDataItem8.TextWrap = true;
            radListDataItem9.Text = "BottomCenter";
            radListDataItem9.TextWrap = true;
            radListDataItem10.Selected = true;
            radListDataItem10.Text = "BottomRight";
            radListDataItem10.TextWrap = true;
            this.ddScreenPosition.Items.Add(radListDataItem5);
            this.ddScreenPosition.Items.Add(radListDataItem6);
            this.ddScreenPosition.Items.Add(radListDataItem7);
            this.ddScreenPosition.Items.Add(radListDataItem8);
            this.ddScreenPosition.Items.Add(radListDataItem9);
            this.ddScreenPosition.Items.Add(radListDataItem10);
            this.ddScreenPosition.Location = new System.Drawing.Point(14, 113);
            this.ddScreenPosition.Name = "ddScreenPosition";
            this.ddScreenPosition.Size = new System.Drawing.Size(170, 20);
            this.ddScreenPosition.TabIndex = 2;
            this.ddScreenPosition.Text = "BottomRight";
            this.ddScreenPosition.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
      
            // 
            // ddThemeName
            // 
            this.ddThemeName.DropDownAnimationEnabled = false;
            this.ddThemeName.DropDownHeight = 0;
            radListDataItem11.Text = "Office2010Black";
            radListDataItem11.TextWrap = true;
            radListDataItem12.Text = "Office2010Silver";
            radListDataItem12.TextWrap = true;
            radListDataItem13.Text = "Office2010Blue";
            radListDataItem13.TextWrap = true;         
            radListDataItem15.Text = "ControlDefault";
            radListDataItem15.TextWrap = true;
            radListDataItem16.Text = "TelerikMetro";
            radListDataItem16.TextWrap = true;
            radListDataItem17.Text = "TelerikMetroBlue";
            radListDataItem17.TextWrap = true;
            radListDataItem16.Selected = true;
            this.ddThemeName.Items.Add(radListDataItem11);
            this.ddThemeName.Items.Add(radListDataItem12);
            this.ddThemeName.Items.Add(radListDataItem13);
            this.ddThemeName.Items.Add(radListDataItem15);
            this.ddThemeName.Items.Add(radListDataItem16);
            this.ddThemeName.Items.Add(radListDataItem17);
            this.ddThemeName.Location = new System.Drawing.Point(14, 61);
            this.ddThemeName.Name = "ddThemeName";
            this.ddThemeName.Size = new System.Drawing.Size(170, 20);
            this.ddThemeName.TabIndex = 2;
            this.ddThemeName.Text = "TelerikMetro";
            this.ddThemeName.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
     
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(14, 36);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(81, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Choose theme:";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.radLabel11);
            this.radGroupBox2.Controls.Add(this.ddAnimationDirection);
            this.radGroupBox2.Controls.Add(this.radLabel8);
            this.radGroupBox2.Controls.Add(this.checkPopupAnimation);
            this.radGroupBox2.Controls.Add(this.radLabel7);
            this.radGroupBox2.Controls.Add(this.fadeOutCheck);
            this.radGroupBox2.Controls.Add(this.fadeInCheck);
            this.radGroupBox2.Controls.Add(this.spinPopupAnimationFrames);
            this.radGroupBox2.Controls.Add(this.autoCloseDelaySpin);
            this.radGroupBox2.Controls.Add(this.radLabel6);
            this.radGroupBox2.Controls.Add(this.autoCloseCheck);
            this.radGroupBox2.Controls.Add(this.spinFadeDuration);
            this.radGroupBox2.HeaderText = "Behavior";
            this.radGroupBox2.Location = new System.Drawing.Point(210, 0);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox2.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox2.Size = new System.Drawing.Size(200, 314);
            this.radGroupBox2.TabIndex = 0;
            this.radGroupBox2.Text = "Behavior";
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(13, 254);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(97, 18);
            this.radLabel11.TabIndex = 8;
            this.radLabel11.Text = "Animation frames:";
            // 
            // ddAnimationDirection
            // 
            this.ddAnimationDirection.DropDownAnimationEnabled = false;
            this.ddAnimationDirection.DropDownHeight = 0;
            radListDataItem1.Text = "Right";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "Up";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "Left";
            radListDataItem3.TextWrap = true;
            radListDataItem4.Selected = true;
            radListDataItem4.Text = "Down";
            radListDataItem4.TextWrap = true;
            this.ddAnimationDirection.Items.Add(radListDataItem1);
            this.ddAnimationDirection.Items.Add(radListDataItem2);
            this.ddAnimationDirection.Items.Add(radListDataItem3);
            this.ddAnimationDirection.Items.Add(radListDataItem4);
            this.ddAnimationDirection.Location = new System.Drawing.Point(13, 228);
            this.ddAnimationDirection.Name = "ddAnimationDirection";
            this.ddAnimationDirection.Size = new System.Drawing.Size(167, 20);
            this.ddAnimationDirection.TabIndex = 7;
            this.ddAnimationDirection.Text = "Down";
            this.ddAnimationDirection.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(13, 204);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(142, 18);
            this.radLabel8.TabIndex = 6;
            this.radLabel8.Text = "Popup animation direction:";
            // 
            // checkPopupAnimation
            // 
            this.checkPopupAnimation.AutoSize = false;
            this.checkPopupAnimation.Location = new System.Drawing.Point(43, 180);
            this.checkPopupAnimation.Name = "checkPopupAnimation";
            this.checkPopupAnimation.Size = new System.Drawing.Size(140, 18);
            this.checkPopupAnimation.TabIndex = 5;
            this.checkPopupAnimation.Text = "Popup animation";

            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(13, 120);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(127, 18);
            this.radLabel7.TabIndex = 4;
            this.radLabel7.Text = "Fade duration in frames:";
            // 
            // fadeOutCheck
            // 
            this.fadeOutCheck.AutoSize = false;
            this.fadeOutCheck.Location = new System.Drawing.Point(116, 93);
            this.fadeOutCheck.Name = "fadeOutCheck";
            this.fadeOutCheck.Size = new System.Drawing.Size(80, 18);
            this.fadeOutCheck.TabIndex = 3;
            this.fadeOutCheck.Text = "Fade-out";
            this.fadeOutCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
          
            // 
            // fadeInCheck
            // 
            this.fadeInCheck.AutoSize = false;
            this.fadeInCheck.Location = new System.Drawing.Point(32, 93);
            this.fadeInCheck.Name = "fadeInCheck";
            this.fadeInCheck.Size = new System.Drawing.Size(70, 18);
            this.fadeInCheck.TabIndex = 3;
            this.fadeInCheck.Text = "Fade-in";
            this.fadeInCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;

            // 
            // spinPopupAnimationFrames
            // 
            this.spinPopupAnimationFrames.Location = new System.Drawing.Point(14, 277);
            this.spinPopupAnimationFrames.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.spinPopupAnimationFrames.Name = "spinPopupAnimationFrames";
            // 
            // 
            // 
            this.spinPopupAnimationFrames.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.spinPopupAnimationFrames.Size = new System.Drawing.Size(166, 20);
            this.spinPopupAnimationFrames.TabIndex = 2;
            this.spinPopupAnimationFrames.TabStop = false;
            this.spinPopupAnimationFrames.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});

            // 
            // autoCloseDelaySpin
            // 
            this.autoCloseDelaySpin.Location = new System.Drawing.Point(70, 57);
            this.autoCloseDelaySpin.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.autoCloseDelaySpin.Name = "autoCloseDelaySpin";
            // 
            // 
            // 
            this.autoCloseDelaySpin.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.autoCloseDelaySpin.Size = new System.Drawing.Size(110, 20);
            this.autoCloseDelaySpin.TabIndex = 2;
            this.autoCloseDelaySpin.TabStop = false;
            this.autoCloseDelaySpin.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});

            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(13, 55);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(51, 18);
            this.radLabel6.TabIndex = 1;
            this.radLabel6.Text = "Delay (s):";
            // 
            // autoCloseCheck
            // 
            this.autoCloseCheck.AutoSize = false;
            this.autoCloseCheck.Location = new System.Drawing.Point(60, 31);
            this.autoCloseCheck.Name = "autoCloseCheck";
            this.autoCloseCheck.Size = new System.Drawing.Size(90, 18);
            this.autoCloseCheck.TabIndex = 0;
            this.autoCloseCheck.Text = "Auto close";
            this.autoCloseCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
    
            // 
            // spinFadeDuration
            // 
            this.spinFadeDuration.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.spinFadeDuration.Location = new System.Drawing.Point(18, 144);
            this.spinFadeDuration.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spinFadeDuration.Name = "spinFadeDuration";
            // 
            // 
            // 
            this.spinFadeDuration.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.spinFadeDuration.Size = new System.Drawing.Size(166, 20);
            this.spinFadeDuration.TabIndex = 1;
            this.spinFadeDuration.TabStop = false;
            this.spinFadeDuration.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});

            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Controls.Add(this.radLabel10);
            this.radGroupBox3.Controls.Add(this.radLabel9);
            this.radGroupBox3.Controls.Add(this.txtContent);
            this.radGroupBox3.Controls.Add(this.txtCaption);
            this.radGroupBox3.HeaderText = "Alert Content";
            this.radGroupBox3.Location = new System.Drawing.Point(420, 0);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox3.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox3.Size = new System.Drawing.Size(200, 314);
            this.radGroupBox3.TabIndex = 1;
            this.radGroupBox3.Text = "Alert Content";
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(13, 158);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(74, 18);
            this.radLabel10.TabIndex = 1;
            this.radLabel10.Text = "Alert content:";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(13, 36);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(73, 18);
            this.radLabel9.TabIndex = 1;
            this.radLabel9.Text = "Alert caption:";
            // 
            // txtContent
            // 
            this.txtContent.AutoSize = false;
            this.txtContent.Location = new System.Drawing.Point(13, 182);
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(173, 119);
            this.txtContent.TabIndex = 0;
            this.txtContent.TabStop = false;
            this.txtContent.Text = "<html><i>This will be the alert\'s content text</i>.<b><span><color=Blue>You can p" +
    "lace HTML formatted text here as well.</span></b></html>";
            // 
            // txtCaption
            // 
            this.txtCaption.AutoSize = false;
            this.txtCaption.Location = new System.Drawing.Point(13, 59);
            this.txtCaption.Multiline = true;
            this.txtCaption.Name = "txtCaption";
            this.txtCaption.Size = new System.Drawing.Size(173, 84);
            this.txtCaption.TabIndex = 0;
            this.txtCaption.TabStop = false;
            this.txtCaption.Text = "This will be the alert\'s caption text";
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(490, 329);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(130, 24);
            this.btnPreview.TabIndex = 2;
            this.btnPreview.Text = "Preview Alert";

            // 
            // radDesktopAlert1
            // 
            this.radDesktopAlert1.ContentImage = global::Telerik.Examples.WinControls.Properties.Resources.Reminder48;
            this.radDesktopAlert1.FixedSize = new System.Drawing.Size(329, 100);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1142, 516);
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).EndInit();
            this.radPanelDemoHolder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinOpacity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optionsButtonCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pinButtonCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeButtonCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditorWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddScreenPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddThemeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddAnimationDirection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkPopupAnimation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fadeOutCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fadeInCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinPopupAnimationFrames)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoCloseDelaySpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoCloseCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinFadeDuration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPreview)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadDropDownList ddThemeName;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList ddScreenPosition;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadSpinEditor spinEditorHeight;
        private Telerik.WinControls.UI.RadSpinEditor spinEditorWidth;
        private Telerik.WinControls.UI.RadCheckBox optionsButtonCheck;
        private Telerik.WinControls.UI.RadCheckBox pinButtonCheck;
        private Telerik.WinControls.UI.RadCheckBox closeButtonCheck;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadCheckBox autoCloseCheck;
        private Telerik.WinControls.UI.RadSpinEditor autoCloseDelaySpin;
        private Telerik.WinControls.UI.RadCheckBox fadeOutCheck;
        private Telerik.WinControls.UI.RadCheckBox fadeInCheck;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadSpinEditor spinFadeDuration;
        private Telerik.WinControls.UI.RadCheckBox checkPopupAnimation;
        private Telerik.WinControls.UI.RadDropDownList ddAnimationDirection;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadTextBox txtContent;
        private Telerik.WinControls.UI.RadTextBox txtCaption;
        private Telerik.WinControls.UI.RadButton btnPreview;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadSpinEditor spinPopupAnimationFrames;
        private Telerik.WinControls.UI.RadSpinEditor spinOpacity;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadDesktopAlert radDesktopAlert1;
    }
}
