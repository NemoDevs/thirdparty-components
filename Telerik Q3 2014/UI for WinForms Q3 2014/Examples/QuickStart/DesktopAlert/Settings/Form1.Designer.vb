﻿Namespace Telerik.Examples.WinControls.DesktopAlert.Settings
	Partial Public Class Form1
		''' <summary> 
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary> 
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Component Designer generated code"

		''' <summary> 
		''' Required method for Designer support - do not modify 
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.components = New System.ComponentModel.Container()
			Dim radListDataItem5 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem6 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem7 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem8 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem9 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem10 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem11 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem12 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem13 As New Telerik.WinControls.UI.RadListDataItem()
            Dim radListDataItem15 As New Telerik.WinControls.UI.RadListDataItem()
            Dim radListDataItem16 As New Telerik.WinControls.UI.RadListDataItem()
            Dim radListDataItem17 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem1 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem2 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem3 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem4 As New Telerik.WinControls.UI.RadListDataItem()
			Me.radGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
			Me.spinOpacity = New Telerik.WinControls.UI.RadSpinEditor()
			Me.optionsButtonCheck = New Telerik.WinControls.UI.RadCheckBox()
			Me.pinButtonCheck = New Telerik.WinControls.UI.RadCheckBox()
			Me.closeButtonCheck = New Telerik.WinControls.UI.RadCheckBox()
			Me.radLabel5 = New Telerik.WinControls.UI.RadLabel()
			Me.radLabel12 = New Telerik.WinControls.UI.RadLabel()
			Me.radLabel4 = New Telerik.WinControls.UI.RadLabel()
			Me.spinEditorHeight = New Telerik.WinControls.UI.RadSpinEditor()
			Me.radLabel3 = New Telerik.WinControls.UI.RadLabel()
			Me.spinEditorWidth = New Telerik.WinControls.UI.RadSpinEditor()
			Me.radLabel2 = New Telerik.WinControls.UI.RadLabel()
			Me.ddScreenPosition = New Telerik.WinControls.UI.RadDropDownList()
			Me.ddThemeName = New Telerik.WinControls.UI.RadDropDownList()
			Me.radLabel1 = New Telerik.WinControls.UI.RadLabel()
			Me.radGroupBox2 = New Telerik.WinControls.UI.RadGroupBox()
			Me.radLabel11 = New Telerik.WinControls.UI.RadLabel()
			Me.ddAnimationDirection = New Telerik.WinControls.UI.RadDropDownList()
			Me.radLabel8 = New Telerik.WinControls.UI.RadLabel()
			Me.checkPopupAnimation = New Telerik.WinControls.UI.RadCheckBox()
			Me.radLabel7 = New Telerik.WinControls.UI.RadLabel()
			Me.fadeOutCheck = New Telerik.WinControls.UI.RadCheckBox()
			Me.fadeInCheck = New Telerik.WinControls.UI.RadCheckBox()
			Me.spinPopupAnimationFrames = New Telerik.WinControls.UI.RadSpinEditor()
			Me.autoCloseDelaySpin = New Telerik.WinControls.UI.RadSpinEditor()
			Me.radLabel6 = New Telerik.WinControls.UI.RadLabel()
			Me.autoCloseCheck = New Telerik.WinControls.UI.RadCheckBox()
			Me.spinFadeDuration = New Telerik.WinControls.UI.RadSpinEditor()
			Me.radGroupBox3 = New Telerik.WinControls.UI.RadGroupBox()
			Me.radLabel10 = New Telerik.WinControls.UI.RadLabel()
			Me.radLabel9 = New Telerik.WinControls.UI.RadLabel()
			Me.txtContent = New Telerik.WinControls.UI.RadTextBox()
			Me.txtCaption = New Telerik.WinControls.UI.RadTextBox()
			Me.btnPreview = New Telerik.WinControls.UI.RadButton()
			Me.radDesktopAlert1 = New Telerik.WinControls.UI.RadDesktopAlert(Me.components)
			CType(Me.radPanelDemoHolder, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.radPanelDemoHolder.SuspendLayout()
			CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.radGroupBox1.SuspendLayout()
			CType(Me.spinOpacity, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.optionsButtonCheck, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.pinButtonCheck, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.closeButtonCheck, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel5, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel12, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel4, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.spinEditorHeight, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.spinEditorWidth, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.ddScreenPosition, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.ddThemeName, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.radGroupBox2.SuspendLayout()
			CType(Me.radLabel11, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.ddAnimationDirection, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel8, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.checkPopupAnimation, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel7, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.fadeOutCheck, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.fadeInCheck, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.spinPopupAnimationFrames, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.autoCloseDelaySpin, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel6, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.autoCloseCheck, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.spinFadeDuration, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radGroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.radGroupBox3.SuspendLayout()
			CType(Me.radLabel10, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel9, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.txtContent, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.txtCaption, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.btnPreview, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.SuspendLayout()
			' 
			' radPanelDemoHolder
			' 
			Me.radPanelDemoHolder.Controls.Add(Me.btnPreview)
			Me.radPanelDemoHolder.Controls.Add(Me.radGroupBox3)
			Me.radPanelDemoHolder.Controls.Add(Me.radGroupBox2)
			Me.radPanelDemoHolder.Controls.Add(Me.radGroupBox1)
			Me.radPanelDemoHolder.ForeColor = Color.Black
			Me.radPanelDemoHolder.Size = New Size(652, 375)
			Me.radPanelDemoHolder.ThemeName = "ControlDefault"
			' 
			' settingsPanel
			' 
			Me.settingsPanel.Location = New Point(717, 1)
			Me.settingsPanel.Size = New Size(200, 730)
			' 
			' radGroupBox1
			' 
			Me.radGroupBox1.AccessibleRole = AccessibleRole.Grouping
			Me.radGroupBox1.Controls.Add(Me.spinOpacity)
			Me.radGroupBox1.Controls.Add(Me.optionsButtonCheck)
			Me.radGroupBox1.Controls.Add(Me.pinButtonCheck)
			Me.radGroupBox1.Controls.Add(Me.closeButtonCheck)
			Me.radGroupBox1.Controls.Add(Me.radLabel5)
			Me.radGroupBox1.Controls.Add(Me.radLabel12)
			Me.radGroupBox1.Controls.Add(Me.radLabel4)
			Me.radGroupBox1.Controls.Add(Me.spinEditorHeight)
			Me.radGroupBox1.Controls.Add(Me.radLabel3)
			Me.radGroupBox1.Controls.Add(Me.spinEditorWidth)
			Me.radGroupBox1.Controls.Add(Me.radLabel2)
			Me.radGroupBox1.Controls.Add(Me.ddScreenPosition)
			Me.radGroupBox1.Controls.Add(Me.ddThemeName)
			Me.radGroupBox1.Controls.Add(Me.radLabel1)
			Me.radGroupBox1.HeaderText = "Appearance"
			Me.radGroupBox1.Location = New Point(0, 0)
			Me.radGroupBox1.Name = "radGroupBox1"
			Me.radGroupBox1.Padding = New Padding(10, 20, 10, 10)
			' 
			' 
			' 
			Me.radGroupBox1.RootElement.Padding = New Padding(10, 20, 10, 10)
			Me.radGroupBox1.Size = New Size(200, 314)
			Me.radGroupBox1.TabIndex = 0
			Me.radGroupBox1.Text = "Appearance"
			' 
			' spinOpacity
			' 
			Me.spinOpacity.Anchor = AnchorStyles.Top
			Me.spinOpacity.DecimalPlaces = 1
			Me.spinOpacity.Increment = New Decimal(New Integer() { 1, 0, 0, 65536})
			Me.spinOpacity.Location = New Point(93, 277)
			Me.spinOpacity.Maximum = New Decimal(New Integer() { 1, 0, 0, 0})
			Me.spinOpacity.Name = "spinOpacity"
			' 
			' 
			' 
			Me.spinOpacity.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
			Me.spinOpacity.Size = New Size(93, 20)
			Me.spinOpacity.TabIndex = 1
			Me.spinOpacity.TabStop = False
			Me.spinOpacity.Value = New Decimal(New Integer() { 8, 0, 0, 65536})

			' 
			' optionsButtonCheck
            ' 
            Me.optionsButtonCheck.AutoSize = False
			Me.optionsButtonCheck.Location = New Point(17, 253)
			Me.optionsButtonCheck.Name = "optionsButtonCheck"
            Me.optionsButtonCheck.Size = New Size(120, 18)
			Me.optionsButtonCheck.TabIndex = 4
			Me.optionsButtonCheck.Text = "Options button"
			Me.optionsButtonCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On

			' 
			' pinButtonCheck
            ' 
            Me.pinButtonCheck.AutoSize = False
			Me.pinButtonCheck.Location = New Point(107, 229)
			Me.pinButtonCheck.Name = "pinButtonCheck"
            Me.pinButtonCheck.Size = New Size(90, 18)
			Me.pinButtonCheck.TabIndex = 4
			Me.pinButtonCheck.Text = "Pin button"
			Me.pinButtonCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On

			' 
			' closeButtonCheck
            ' 
            Me.closeButtonCheck.AutoSize = False
            Me.closeButtonCheck.Location = New Point(17, 229)
			Me.closeButtonCheck.Name = "closeButtonCheck"
            Me.closeButtonCheck.Size = New Size(105, 18)
			Me.closeButtonCheck.TabIndex = 4
			Me.closeButtonCheck.Text = "Close button"
			Me.closeButtonCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On

			' 
			' radLabel5
			' 
			Me.radLabel5.Location = New Point(32, 192)
			Me.radLabel5.Name = "radLabel5"
			Me.radLabel5.Size = New Size(42, 18)
			Me.radLabel5.TabIndex = 3
			Me.radLabel5.Text = "Height:"
			' 
			' radLabel12
			' 
			Me.radLabel12.Location = New Point(13, 277)
			Me.radLabel12.Name = "radLabel12"
			Me.radLabel12.Size = New Size(72, 18)
			Me.radLabel12.TabIndex = 3
			Me.radLabel12.Text = "Alert opacity:"
			' 
			' radLabel4
			' 
			Me.radLabel4.Location = New Point(32, 168)
			Me.radLabel4.Name = "radLabel4"
			Me.radLabel4.Size = New Size(39, 18)
			Me.radLabel4.TabIndex = 3
			Me.radLabel4.Text = "Width:"
			' 
			' spinEditorHeight
			' 
			Me.spinEditorHeight.Anchor = AnchorStyles.Top
			Me.spinEditorHeight.Location = New Point(79, 192)
			Me.spinEditorHeight.Maximum = New Decimal(New Integer() { 300, 0, 0, 0})
			Me.spinEditorHeight.Minimum = New Decimal(New Integer() { 70, 0, 0, 0})
			Me.spinEditorHeight.Name = "spinEditorHeight"
			' 
			' 
			' 
			Me.spinEditorHeight.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
			Me.spinEditorHeight.Size = New Size(107, 20)
			Me.spinEditorHeight.TabIndex = 1
			Me.spinEditorHeight.TabStop = False
			Me.spinEditorHeight.Value = New Decimal(New Integer() { 100, 0, 0, 0})

			' 
			' radLabel3
			' 
			Me.radLabel3.Location = New Point(13, 144)
			Me.radLabel3.Name = "radLabel3"
			Me.radLabel3.Size = New Size(54, 18)
			Me.radLabel3.TabIndex = 3
			Me.radLabel3.Text = "Alert size:"
			' 
			' spinEditorWidth
			' 
			Me.spinEditorWidth.Anchor = AnchorStyles.Top
			Me.spinEditorWidth.Location = New Point(79, 168)
			Me.spinEditorWidth.Maximum = New Decimal(New Integer() { 500, 0, 0, 0})
			Me.spinEditorWidth.Minimum = New Decimal(New Integer() { 150, 0, 0, 0})
			Me.spinEditorWidth.Name = "spinEditorWidth"
			' 
			' 
			' 
			Me.spinEditorWidth.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
			Me.spinEditorWidth.Size = New Size(107, 20)
			Me.spinEditorWidth.TabIndex = 1
			Me.spinEditorWidth.TabStop = False
			Me.spinEditorWidth.Value = New Decimal(New Integer() { 329, 0, 0, 0})

			' 
			' radLabel2
			' 
			Me.radLabel2.Location = New Point(14, 89)
			Me.radLabel2.Name = "radLabel2"
			Me.radLabel2.Size = New Size(90, 18)
			Me.radLabel2.TabIndex = 3
			Me.radLabel2.Text = "Choose position:"
			' 
			' ddScreenPosition
			' 
			Me.ddScreenPosition.DropDownAnimationEnabled = False
			Me.ddScreenPosition.DropDownHeight = 0
			radListDataItem5.Text = "TopLeft"
			radListDataItem5.TextWrap = True
			radListDataItem6.Text = "TopCenter"
			radListDataItem6.TextWrap = True
			radListDataItem7.Text = "TopRight"
			radListDataItem7.TextWrap = True
			radListDataItem8.Text = "BottomLeft"
			radListDataItem8.TextWrap = True
			radListDataItem9.Text = "BottomCenter"
			radListDataItem9.TextWrap = True
			radListDataItem10.Selected = True
			radListDataItem10.Text = "BottomRight"
			radListDataItem10.TextWrap = True
			Me.ddScreenPosition.Items.Add(radListDataItem5)
			Me.ddScreenPosition.Items.Add(radListDataItem6)
			Me.ddScreenPosition.Items.Add(radListDataItem7)
			Me.ddScreenPosition.Items.Add(radListDataItem8)
			Me.ddScreenPosition.Items.Add(radListDataItem9)
			Me.ddScreenPosition.Items.Add(radListDataItem10)
			Me.ddScreenPosition.Location = New Point(14, 113)
			Me.ddScreenPosition.Name = "ddScreenPosition"
			Me.ddScreenPosition.Size = New Size(170, 20)
			Me.ddScreenPosition.TabIndex = 2
			Me.ddScreenPosition.Text = "BottomRight"
            Me.ddScreenPosition.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            ' 
			' ddThemeName
			' 
			Me.ddThemeName.DropDownAnimationEnabled = False
			Me.ddThemeName.DropDownHeight = 0
			radListDataItem11.Text = "Office2010Black"
			radListDataItem11.TextWrap = True
			radListDataItem12.Text = "Office2010Silver"
			radListDataItem12.TextWrap = True
			radListDataItem13.Text = "Office2010Blue"
			radListDataItem13.TextWrap = True
			radListDataItem15.Text = "ControlDefault"
            radListDataItem15.TextWrap = True
            radListDataItem16.Text = "TelerikMetro"
            radListDataItem16.TextWrap = True
            radListDataItem17.Text = "TelerikMetroBlue"
            radListDataItem17.TextWrap = True
            radListDataItem16.Selected = True
			Me.ddThemeName.Items.Add(radListDataItem11)
			Me.ddThemeName.Items.Add(radListDataItem12)
			Me.ddThemeName.Items.Add(radListDataItem13)
            Me.ddThemeName.Items.Add(radListDataItem15)
            Me.ddThemeName.Items.Add(radListDataItem16)
            Me.ddThemeName.Items.Add(radListDataItem17)
			Me.ddThemeName.Location = New Point(14, 61)
			Me.ddThemeName.Name = "ddThemeName"
			Me.ddThemeName.Size = New Size(170, 20)
			Me.ddThemeName.TabIndex = 2
            Me.ddThemeName.Text = "TelerikMetro"
            Me.ddThemeName.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
			' 
			' radLabel1
			' 
			Me.radLabel1.Location = New Point(14, 36)
			Me.radLabel1.Name = "radLabel1"
			Me.radLabel1.Size = New Size(81, 18)
			Me.radLabel1.TabIndex = 1
			Me.radLabel1.Text = "Choose theme:"
			' 
			' radGroupBox2
			' 
			Me.radGroupBox2.AccessibleRole = AccessibleRole.Grouping
			Me.radGroupBox2.Controls.Add(Me.radLabel11)
			Me.radGroupBox2.Controls.Add(Me.ddAnimationDirection)
			Me.radGroupBox2.Controls.Add(Me.radLabel8)
			Me.radGroupBox2.Controls.Add(Me.checkPopupAnimation)
			Me.radGroupBox2.Controls.Add(Me.radLabel7)
			Me.radGroupBox2.Controls.Add(Me.fadeOutCheck)
			Me.radGroupBox2.Controls.Add(Me.fadeInCheck)
			Me.radGroupBox2.Controls.Add(Me.spinPopupAnimationFrames)
			Me.radGroupBox2.Controls.Add(Me.autoCloseDelaySpin)
			Me.radGroupBox2.Controls.Add(Me.radLabel6)
			Me.radGroupBox2.Controls.Add(Me.autoCloseCheck)
			Me.radGroupBox2.Controls.Add(Me.spinFadeDuration)
			Me.radGroupBox2.HeaderText = "Behavior"
			Me.radGroupBox2.Location = New Point(210, 0)
			Me.radGroupBox2.Name = "radGroupBox2"
			Me.radGroupBox2.Padding = New Padding(10, 20, 10, 10)
			' 
			' 
			' 
			Me.radGroupBox2.RootElement.Padding = New Padding(10, 20, 10, 10)
			Me.radGroupBox2.Size = New Size(200, 314)
			Me.radGroupBox2.TabIndex = 0
			Me.radGroupBox2.Text = "Behavior"
			' 
			' radLabel11
			' 
			Me.radLabel11.Location = New Point(13, 254)
			Me.radLabel11.Name = "radLabel11"
			Me.radLabel11.Size = New Size(97, 18)
			Me.radLabel11.TabIndex = 8
			Me.radLabel11.Text = "Animation frames:"
			' 
			' ddAnimationDirection
			' 
			Me.ddAnimationDirection.DropDownAnimationEnabled = False
			Me.ddAnimationDirection.DropDownHeight = 0
			radListDataItem1.Text = "Right"
			radListDataItem1.TextWrap = True
			radListDataItem2.Text = "Up"
			radListDataItem2.TextWrap = True
			radListDataItem3.Text = "Left"
			radListDataItem3.TextWrap = True
			radListDataItem4.Selected = True
			radListDataItem4.Text = "Down"
			radListDataItem4.TextWrap = True
			Me.ddAnimationDirection.Items.Add(radListDataItem1)
			Me.ddAnimationDirection.Items.Add(radListDataItem2)
			Me.ddAnimationDirection.Items.Add(radListDataItem3)
			Me.ddAnimationDirection.Items.Add(radListDataItem4)
			Me.ddAnimationDirection.Location = New Point(13, 228)
			Me.ddAnimationDirection.Name = "ddAnimationDirection"
			Me.ddAnimationDirection.Size = New Size(167, 20)
			Me.ddAnimationDirection.TabIndex = 7
            Me.ddAnimationDirection.Text = "Down"
            Me.ddAnimationDirection.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
			' 
			' radLabel8
			' 
			Me.radLabel8.Location = New Point(13, 204)
			Me.radLabel8.Name = "radLabel8"
			Me.radLabel8.Size = New Size(142, 18)
			Me.radLabel8.TabIndex = 6
			Me.radLabel8.Text = "Popup animation direction:"
			' 
			' checkPopupAnimation
            ' 
            Me.checkPopupAnimation.AutoSize = False
			Me.checkPopupAnimation.Location = New Point(43, 180)
			Me.checkPopupAnimation.Name = "checkPopupAnimation"
            Me.checkPopupAnimation.Size = New Size(140, 18)
			Me.checkPopupAnimation.TabIndex = 5
			Me.checkPopupAnimation.Text = "Popup animation"

			' 
			' radLabel7
			' 
			Me.radLabel7.Location = New Point(13, 120)
			Me.radLabel7.Name = "radLabel7"
			Me.radLabel7.Size = New Size(127, 18)
			Me.radLabel7.TabIndex = 4
			Me.radLabel7.Text = "Fade duration in frames:"
			' 
			' fadeOutCheck
            ' 
            Me.fadeOutCheck.AutoSize = False
			Me.fadeOutCheck.Location = New Point(96, 93)
			Me.fadeOutCheck.Name = "fadeOutCheck"
            Me.fadeOutCheck.Size = New Size(85, 18)
			Me.fadeOutCheck.TabIndex = 3
			Me.fadeOutCheck.Text = "Fade-out"
			Me.fadeOutCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On

			' 
			' fadeInCheck
            ' 
            Me.fadeInCheck.AutoSize = False
			Me.fadeInCheck.Location = New Point(32, 93)
			Me.fadeInCheck.Name = "fadeInCheck"
            Me.fadeInCheck.Size = New Size(70, 18)
			Me.fadeInCheck.TabIndex = 3
			Me.fadeInCheck.Text = "Fade-in"
			Me.fadeInCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On

			' 
			' spinPopupAnimationFrames
			' 
			Me.spinPopupAnimationFrames.Location = New Point(14, 277)
			Me.spinPopupAnimationFrames.Minimum = New Decimal(New Integer() { 5, 0, 0, 0})
			Me.spinPopupAnimationFrames.Name = "spinPopupAnimationFrames"
			' 
			' 
			' 
			Me.spinPopupAnimationFrames.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
			Me.spinPopupAnimationFrames.Size = New Size(166, 20)
			Me.spinPopupAnimationFrames.TabIndex = 2
			Me.spinPopupAnimationFrames.TabStop = False
			Me.spinPopupAnimationFrames.Value = New Decimal(New Integer() { 50, 0, 0, 0})

			' 
			' autoCloseDelaySpin
			' 
			Me.autoCloseDelaySpin.Location = New Point(70, 57)
			Me.autoCloseDelaySpin.Minimum = New Decimal(New Integer() { 5, 0, 0, 0})
			Me.autoCloseDelaySpin.Name = "autoCloseDelaySpin"
			' 
			' 
			' 
			Me.autoCloseDelaySpin.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
			Me.autoCloseDelaySpin.Size = New Size(110, 20)
			Me.autoCloseDelaySpin.TabIndex = 2
			Me.autoCloseDelaySpin.TabStop = False
			Me.autoCloseDelaySpin.Value = New Decimal(New Integer() { 10, 0, 0, 0})

			' 
			' radLabel6
			' 
			Me.radLabel6.Location = New Point(13, 55)
			Me.radLabel6.Name = "radLabel6"
			Me.radLabel6.Size = New Size(51, 18)
			Me.radLabel6.TabIndex = 1
			Me.radLabel6.Text = "Delay (s):"
			' 
			' autoCloseCheck
            ' 
            Me.autoCloseCheck.AutoSize = False
			Me.autoCloseCheck.Location = New Point(60, 31)
			Me.autoCloseCheck.Name = "autoCloseCheck"
            Me.autoCloseCheck.Size = New Size(90, 18)
			Me.autoCloseCheck.TabIndex = 0
			Me.autoCloseCheck.Text = "Auto close"
			Me.autoCloseCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On

			' 
			' spinFadeDuration
			' 
			Me.spinFadeDuration.Anchor = AnchorStyles.Top
			Me.spinFadeDuration.Location = New Point(18, 144)
			Me.spinFadeDuration.Minimum = New Decimal(New Integer() { 10, 0, 0, 0})
			Me.spinFadeDuration.Name = "spinFadeDuration"
			' 
			' 
			' 
			Me.spinFadeDuration.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
			Me.spinFadeDuration.Size = New Size(166, 20)
			Me.spinFadeDuration.TabIndex = 1
			Me.spinFadeDuration.TabStop = False
			Me.spinFadeDuration.Value = New Decimal(New Integer() { 80, 0, 0, 0})

			' 
			' radGroupBox3
			' 
			Me.radGroupBox3.AccessibleRole = AccessibleRole.Grouping
			Me.radGroupBox3.Controls.Add(Me.radLabel10)
			Me.radGroupBox3.Controls.Add(Me.radLabel9)
			Me.radGroupBox3.Controls.Add(Me.txtContent)
			Me.radGroupBox3.Controls.Add(Me.txtCaption)
			Me.radGroupBox3.HeaderText = "Alert Content"
			Me.radGroupBox3.Location = New Point(420, 0)
			Me.radGroupBox3.Name = "radGroupBox3"
			Me.radGroupBox3.Padding = New Padding(10, 20, 10, 10)
			' 
			' 
			' 
			Me.radGroupBox3.RootElement.Padding = New Padding(10, 20, 10, 10)
			Me.radGroupBox3.Size = New Size(200, 314)
			Me.radGroupBox3.TabIndex = 1
			Me.radGroupBox3.Text = "Alert Content"
			' 
			' radLabel10
			' 
			Me.radLabel10.Location = New Point(13, 158)
			Me.radLabel10.Name = "radLabel10"
			Me.radLabel10.Size = New Size(74, 18)
			Me.radLabel10.TabIndex = 1
			Me.radLabel10.Text = "Alert content:"
			' 
			' radLabel9
			' 
			Me.radLabel9.Location = New Point(13, 36)
			Me.radLabel9.Name = "radLabel9"
			Me.radLabel9.Size = New Size(73, 18)
			Me.radLabel9.TabIndex = 1
			Me.radLabel9.Text = "Alert caption:"
			' 
			' txtContent
			' 
			Me.txtContent.AutoSize = False
			Me.txtContent.Location = New Point(13, 182)
			Me.txtContent.Multiline = True
			Me.txtContent.Name = "txtContent"
			Me.txtContent.Size = New Size(173, 119)
			Me.txtContent.TabIndex = 0
			Me.txtContent.TabStop = False
			Me.txtContent.Text = "<html><i>This will be the alert's content text</i>.<b><span><color=Blue>You can p" & "lace HTML formatted text here as well.</span></b></html>"
			' 
			' txtCaption
			' 
			Me.txtCaption.AutoSize = False
			Me.txtCaption.Location = New Point(13, 59)
			Me.txtCaption.Multiline = True
			Me.txtCaption.Name = "txtCaption"
			Me.txtCaption.Size = New Size(173, 84)
			Me.txtCaption.TabIndex = 0
			Me.txtCaption.TabStop = False
			Me.txtCaption.Text = "This will be the alert's caption text"
			' 
			' btnPreview
			' 
			Me.btnPreview.Location = New Point(490, 329)
			Me.btnPreview.Name = "btnPreview"
			Me.btnPreview.Size = New Size(130, 24)
			Me.btnPreview.TabIndex = 2
			Me.btnPreview.Text = "Preview Alert"

			' 
			' radDesktopAlert1
			' 
            Me.radDesktopAlert1.ContentImage = My.Resources.Reminder48
			Me.radDesktopAlert1.FixedSize = New Size(329, 100)
			' 
			' Form1
			' 
			Me.AutoScaleDimensions = New SizeF(6F, 13F)
			Me.AutoScaleMode = AutoScaleMode.Font
			Me.Name = "Form1"
			Me.Size = New Size(1142, 516)
			CType(Me.radPanelDemoHolder, System.ComponentModel.ISupportInitialize).EndInit()
			Me.radPanelDemoHolder.ResumeLayout(False)
			CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
			Me.radGroupBox1.ResumeLayout(False)
			Me.radGroupBox1.PerformLayout()
			CType(Me.spinOpacity, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.optionsButtonCheck, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.pinButtonCheck, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.closeButtonCheck, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel5, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel12, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel4, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.spinEditorHeight, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel3, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.spinEditorWidth, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.ddScreenPosition, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.ddThemeName, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
			Me.radGroupBox2.ResumeLayout(False)
			Me.radGroupBox2.PerformLayout()
			CType(Me.radLabel11, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.ddAnimationDirection, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel8, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.checkPopupAnimation, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel7, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.fadeOutCheck, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.fadeInCheck, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.spinPopupAnimationFrames, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.autoCloseDelaySpin, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel6, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.autoCloseCheck, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.spinFadeDuration, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radGroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
			Me.radGroupBox3.ResumeLayout(False)
			Me.radGroupBox3.PerformLayout()
			CType(Me.radLabel10, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel9, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.txtContent, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.txtCaption, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.btnPreview, System.ComponentModel.ISupportInitialize).EndInit()
			Me.ResumeLayout(False)

		End Sub

		#End Region

		Private radGroupBox2 As Telerik.WinControls.UI.RadGroupBox
		Private radGroupBox1 As Telerik.WinControls.UI.RadGroupBox
		Private ddThemeName As Telerik.WinControls.UI.RadDropDownList
		Private radLabel1 As Telerik.WinControls.UI.RadLabel
		Private radLabel2 As Telerik.WinControls.UI.RadLabel
		Private ddScreenPosition As Telerik.WinControls.UI.RadDropDownList
		Private radLabel3 As Telerik.WinControls.UI.RadLabel
		Private radLabel5 As Telerik.WinControls.UI.RadLabel
		Private radLabel4 As Telerik.WinControls.UI.RadLabel
		Private spinEditorHeight As Telerik.WinControls.UI.RadSpinEditor
		Private spinEditorWidth As Telerik.WinControls.UI.RadSpinEditor
		Private optionsButtonCheck As Telerik.WinControls.UI.RadCheckBox
		Private pinButtonCheck As Telerik.WinControls.UI.RadCheckBox
		Private closeButtonCheck As Telerik.WinControls.UI.RadCheckBox
		Private radLabel6 As Telerik.WinControls.UI.RadLabel
		Private autoCloseCheck As Telerik.WinControls.UI.RadCheckBox
		Private autoCloseDelaySpin As Telerik.WinControls.UI.RadSpinEditor
		Private fadeOutCheck As Telerik.WinControls.UI.RadCheckBox
		Private fadeInCheck As Telerik.WinControls.UI.RadCheckBox
		Private radLabel7 As Telerik.WinControls.UI.RadLabel
		Private spinFadeDuration As Telerik.WinControls.UI.RadSpinEditor
		Private checkPopupAnimation As Telerik.WinControls.UI.RadCheckBox
		Private ddAnimationDirection As Telerik.WinControls.UI.RadDropDownList
		Private radLabel8 As Telerik.WinControls.UI.RadLabel
		Private radGroupBox3 As Telerik.WinControls.UI.RadGroupBox
		Private radLabel9 As Telerik.WinControls.UI.RadLabel
		Private txtContent As Telerik.WinControls.UI.RadTextBox
		Private txtCaption As Telerik.WinControls.UI.RadTextBox
		Private btnPreview As Telerik.WinControls.UI.RadButton
		Private radLabel10 As Telerik.WinControls.UI.RadLabel
		Private radLabel11 As Telerik.WinControls.UI.RadLabel
		Private spinPopupAnimationFrames As Telerik.WinControls.UI.RadSpinEditor
		Private spinOpacity As Telerik.WinControls.UI.RadSpinEditor
		Private radLabel12 As Telerik.WinControls.UI.RadLabel
		Private radDesktopAlert1 As Telerik.WinControls.UI.RadDesktopAlert
	End Class
End Namespace
