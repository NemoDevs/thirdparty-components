﻿Imports Telerik.Examples.WinControls.Editors.ComboBox
Imports Telerik.WinControls.UI

Namespace Telerik.Examples.WinControls.DesktopAlert.Settings
	Partial Public Class Form1
		Inherits EditorExampleBaseForm
		Public Sub New()
            InitializeComponent()
            Me.radDesktopAlert1.ThemeName = Me.ddThemeName.SelectedItem.Text
		End Sub

		Protected Overrides Sub WireEvents()
			AddHandler closeButtonCheck.ToggleStateChanged, AddressOf closeButtonCheck_ToggleStateChanged
			AddHandler pinButtonCheck.ToggleStateChanged, AddressOf pinButtonCheck_ToggleStateChanged
			AddHandler ddScreenPosition.SelectedIndexChanged, AddressOf OnPositionList_IndexChaned
			AddHandler ddThemeName.SelectedIndexChanged, AddressOf OnThemesList_IndexChanged
			AddHandler fadeOutCheck.ToggleStateChanged, AddressOf fadeOutCheck_ToggleStateChanged
			AddHandler checkPopupAnimation.ToggleStateChanged, AddressOf checkPopupAnimation_ToggleStateChanged
			AddHandler fadeInCheck.ToggleStateChanged, AddressOf fadeInCheck_ToggleStateChanged
			AddHandler spinPopupAnimationFrames.ValueChanged, AddressOf spinPopupAnimationFrames_ValueChanged
			AddHandler btnPreview.Click, AddressOf btnPreview_Click
			AddHandler spinFadeDuration.ValueChanged, AddressOf spinFadeDuration_ValueChanged
			AddHandler autoCloseCheck.ToggleStateChanged, AddressOf autoCloseCheck_ToggleStateChanged
			AddHandler spinFadeDuration.ValueChanged, AddressOf spinFadeDuration_ValueChanged
			AddHandler spinOpacity.ValueChanged, AddressOf SpinOpacity_ValueChanged
			AddHandler optionsButtonCheck.ToggleStateChanged, AddressOf optionsButtonCheck_ToggleStateChanged
			AddHandler spinEditorHeight.ValueChanged, AddressOf SpinEditorHeight_ValueChanged
			AddHandler spinEditorWidth.ValueChanged, AddressOf SpinEditorWidth_ValueChanged
			AddHandler autoCloseDelaySpin.ValueChanged, AddressOf autoCloseDelaySpin_ValueChanged
			AddHandler autoCloseCheck.ToggleStateChanged, AddressOf autoCloseCheck_ToggleStateChanged
			AddHandler ddAnimationDirection.SelectedIndexChanged, AddressOf ddAnimationDirection_SelectedIndexChanged
		End Sub

		#Region "Event handling"

		Private Sub OnThemesList_IndexChanged(ByVal sender As Object, ByVal e As Telerik.WinControls.UI.Data.PositionChangedEventArgs)
			Me.radDesktopAlert1.ThemeName = Me.ddThemeName.SelectedItem.Text
		End Sub

		Private Sub OnPositionList_IndexChaned(ByVal sender As Object, ByVal e As Telerik.WinControls.UI.Data.PositionChangedEventArgs)
			Me.radDesktopAlert1.ScreenPosition = CType(System.Enum.Parse(GetType(AlertScreenPosition), Me.ddScreenPosition.SelectedItem.Text), AlertScreenPosition)
		End Sub

		Private Sub SpinEditorWidth_ValueChanged(ByVal sender As Object, ByVal e As EventArgs)
			Me.radDesktopAlert1.FixedSize = New Size(CInt(Fix(Me.spinEditorWidth.Value)), CInt(Fix(Me.spinEditorHeight.Value)))
		End Sub
		Private Sub SpinEditorHeight_ValueChanged(ByVal sender As Object, ByVal e As EventArgs)
			Me.radDesktopAlert1.FixedSize = New Size(CInt(Fix(Me.spinEditorWidth.Value)), CInt(Fix(Me.spinEditorHeight.Value)))
		End Sub

		Private Sub fadeInCheck_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs)
			If Me.fadeInCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				Me.radDesktopAlert1.FadeAnimationType = Me.radDesktopAlert1.FadeAnimationType Or FadeAnimationType.FadeIn
			Else
				Me.radDesktopAlert1.FadeAnimationType = Me.radDesktopAlert1.FadeAnimationType And Not FadeAnimationType.FadeIn
			End If
		End Sub

		Private Sub fadeOutCheck_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs)
			If Me.fadeOutCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				Me.radDesktopAlert1.FadeAnimationType = Me.radDesktopAlert1.FadeAnimationType Or FadeAnimationType.FadeOut
			Else
				Me.radDesktopAlert1.FadeAnimationType = (Me.radDesktopAlert1.FadeAnimationType And (Not FadeAnimationType.FadeOut))
			End If
		End Sub

		Private Sub spinFadeDuration_ValueChanged(ByVal sender As Object, ByVal e As EventArgs)
			Me.radDesktopAlert1.FadeAnimationFrames = CInt(Fix(Me.spinFadeDuration.Value))
		End Sub

		Private Sub autoCloseCheck_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs)
			Me.radDesktopAlert1.AutoClose = Me.autoCloseCheck.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
		End Sub

		Private Sub closeButtonCheck_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs)
			Me.radDesktopAlert1.ShowCloseButton = args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
		End Sub

		Private Sub pinButtonCheck_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs)
			Me.radDesktopAlert1.ShowPinButton = args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
		End Sub

		Private Sub optionsButtonCheck_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs)
			Me.radDesktopAlert1.ShowOptionsButton = args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
		End Sub

		Private Sub checkPopupAnimation_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs)
			Me.radDesktopAlert1.PopupAnimation = args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
		End Sub

		Private Sub ddAnimationDirection_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.WinControls.UI.Data.PositionChangedEventArgs)
			Me.radDesktopAlert1.PopupAnimationDirection = CType(System.Enum.Parse(GetType(RadDirection), Me.ddAnimationDirection.SelectedItem.Text), RadDirection)
		End Sub

		Private Sub spinPopupAnimationFrames_ValueChanged(ByVal sender As Object, ByVal e As EventArgs)
			Me.radDesktopAlert1.PopupAnimationFrames = CInt(Fix(Me.spinPopupAnimationFrames.Value))
		End Sub

		Private Sub btnPreview_Click(ByVal sender As Object, ByVal e As EventArgs)
			Me.radDesktopAlert1.CaptionText = Me.txtCaption.Text
			Me.radDesktopAlert1.ContentText = Me.txtContent.Text
			Me.radDesktopAlert1.Show()
		End Sub

		Private Sub SpinOpacity_ValueChanged(ByVal sender As Object, ByVal e As EventArgs)
			Me.radDesktopAlert1.Opacity = CSng(Me.spinOpacity.Value)
		End Sub

		Private Sub autoCloseDelaySpin_ValueChanged(ByVal sender As Object, ByVal e As EventArgs)
			Me.radDesktopAlert1.AutoCloseDelay = CInt(Fix(Me.autoCloseDelaySpin.Value))
		End Sub

		#End Region
	End Class
End Namespace
