﻿using Telerik.WinControls;
namespace Telerik.Examples.WinControls.PanelsLabels.GroupBox
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioButton2 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton1 = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioButton3 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton5 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton6 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton4 = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupBox4 = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioButton11 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton10 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton9 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton8 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton7 = new Telerik.WinControls.UI.RadRadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).BeginInit();
            this.radGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton7)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(1052, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 830);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radGroupBox2);
            this.radGroupBox1.Controls.Add(this.radGroupBox3);
            this.radGroupBox1.Controls.Add(this.radGroupBox4);
            this.radGroupBox1.HeaderImage = global::Telerik.Examples.WinControls.Properties.Resources.info2;
            this.radGroupBox1.HeaderText = "Telerik Groupbox ";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox1.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox1.Size = new System.Drawing.Size(439, 271);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = "Telerik Groupbox ";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.radRadioButton2);
            this.radGroupBox2.Controls.Add(this.radRadioButton1);
            this.radGroupBox2.HeaderText = "Groupbox style";
            this.radGroupBox2.Location = new System.Drawing.Point(52, 187);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox2.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox2.Size = new System.Drawing.Size(313, 47);
            this.radGroupBox2.TabIndex = 0;
            this.radGroupBox2.Text = "Groupbox style";
            // 
            // radRadioButton2
            // 
            this.radRadioButton2.Location = new System.Drawing.Point(12, 24);
            this.radRadioButton2.Name = "radRadioButton2";
            this.radRadioButton2.Size = new System.Drawing.Size(65, 18);
            this.radRadioButton2.TabIndex = 1;
            this.radRadioButton2.Text = "Standard";
            // 
            // radRadioButton1
            // 
            this.radRadioButton1.Location = new System.Drawing.Point(128, 24);
            this.radRadioButton1.Name = "radRadioButton1";
            this.radRadioButton1.Size = new System.Drawing.Size(50, 18);
            this.radRadioButton1.TabIndex = 0;
            this.radRadioButton1.Text = "Office";
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Controls.Add(this.radRadioButton3);
            this.radGroupBox3.Controls.Add(this.radRadioButton5);
            this.radGroupBox3.Controls.Add(this.radRadioButton6);
            this.radGroupBox3.Controls.Add(this.radRadioButton4);
            this.radGroupBox3.HeaderText = "Header Position";
            this.radGroupBox3.Location = new System.Drawing.Point(52, 43);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox3.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox3.Size = new System.Drawing.Size(116, 138);
            this.radGroupBox3.TabIndex = 1;
            this.radGroupBox3.Text = "Header Position";
            // 
            // radRadioButton3
            // 
            this.radRadioButton3.Location = new System.Drawing.Point(12, 21);
            this.radRadioButton3.Name = "radRadioButton3";
            this.radRadioButton3.Size = new System.Drawing.Size(39, 18);
            this.radRadioButton3.TabIndex = 0;
            this.radRadioButton3.Text = "Top";
            // 
            // radRadioButton5
            // 
            this.radRadioButton5.Location = new System.Drawing.Point(12, 65);
            this.radRadioButton5.Name = "radRadioButton5";
            this.radRadioButton5.Size = new System.Drawing.Size(57, 18);
            this.radRadioButton5.TabIndex = 2;
            this.radRadioButton5.Text = "Bottom";
            // 
            // radRadioButton6
            // 
            this.radRadioButton6.Location = new System.Drawing.Point(12, 87);
            this.radRadioButton6.Name = "radRadioButton6";
            this.radRadioButton6.Size = new System.Drawing.Size(39, 18);
            this.radRadioButton6.TabIndex = 3;
            this.radRadioButton6.Text = "Left";
            // 
            // radRadioButton4
            // 
            this.radRadioButton4.Location = new System.Drawing.Point(12, 43);
            this.radRadioButton4.Name = "radRadioButton4";
            this.radRadioButton4.Size = new System.Drawing.Size(47, 18);
            this.radRadioButton4.TabIndex = 1;
            this.radRadioButton4.Text = "Right";
            // 
            // radGroupBox4
            // 
            this.radGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox4.Controls.Add(this.radRadioButton11);
            this.radGroupBox4.Controls.Add(this.radRadioButton10);
            this.radGroupBox4.Controls.Add(this.radRadioButton9);
            this.radGroupBox4.Controls.Add(this.radRadioButton8);
            this.radGroupBox4.Controls.Add(this.radRadioButton7);
            this.radGroupBox4.HeaderText = "Text and Image Relation";
            this.radGroupBox4.Location = new System.Drawing.Point(176, 43);
            this.radGroupBox4.Name = "radGroupBox4";
            this.radGroupBox4.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox4.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox4.Size = new System.Drawing.Size(189, 138);
            this.radGroupBox4.TabIndex = 2;
            this.radGroupBox4.Text = "Text and Image Relation";
            // 
            // radRadioButton11
            // 
            this.radRadioButton11.Location = new System.Drawing.Point(16, 109);
            this.radRadioButton11.Name = "radRadioButton11";
            this.radRadioButton11.Size = new System.Drawing.Size(111, 18);
            this.radRadioButton11.TabIndex = 4;
            this.radRadioButton11.Text = "Text before image";
            // 
            // radRadioButton10
            // 
            this.radRadioButton10.Location = new System.Drawing.Point(16, 87);
            this.radRadioButton10.Name = "radRadioButton10";
            this.radRadioButton10.Size = new System.Drawing.Size(109, 18);
            this.radRadioButton10.TabIndex = 3;
            this.radRadioButton10.Text = "Text above image";
            // 
            // radRadioButton9
            // 
            this.radRadioButton9.Location = new System.Drawing.Point(16, 65);
            this.radRadioButton9.Name = "radRadioButton9";
            this.radRadioButton9.Size = new System.Drawing.Size(58, 18);
            this.radRadioButton9.TabIndex = 2;
            this.radRadioButton9.Text = "Overlay";
            // 
            // radRadioButton8
            // 
            this.radRadioButton8.Location = new System.Drawing.Point(16, 43);
            this.radRadioButton8.Name = "radRadioButton8";
            this.radRadioButton8.Size = new System.Drawing.Size(109, 18);
            this.radRadioButton8.TabIndex = 1;
            this.radRadioButton8.Text = "Image before text";
            // 
            // radRadioButton7
            // 
            this.radRadioButton7.Location = new System.Drawing.Point(16, 21);
            this.radRadioButton7.Name = "radRadioButton7";
            this.radRadioButton7.Size = new System.Drawing.Size(107, 18);
            this.radRadioButton7.TabIndex = 0;
            this.radRadioButton7.Text = "Image above text";
            // 
            // Form1
            // 
            this.Controls.Add(this.radGroupBox1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1223, 659);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.radGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).EndInit();
            this.radGroupBox4.ResumeLayout(false);
            this.radGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox4;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton2;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton1;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton3;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton6;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton5;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton4;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton7;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton8;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton9;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton10;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton11;

    }
}
