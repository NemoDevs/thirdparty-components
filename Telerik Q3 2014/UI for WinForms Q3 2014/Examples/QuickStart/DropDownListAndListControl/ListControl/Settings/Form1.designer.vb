Namespace Telerik.Examples.WinControls.DropDownListAndListControl.ListControl.Settings
	Partial Public Class Form1
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
            Dim RadListDataItem28 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem29 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem30 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem31 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem32 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem33 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem34 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem3 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem4 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem5 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem6 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem7 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem8 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem9 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem10 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem11 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem12 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem13 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem14 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem15 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem16 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem17 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem18 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem19 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem20 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem21 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem22 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem23 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem24 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem25 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem26 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem27 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem1 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem2 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Me.imageList1 = New System.Windows.Forms.ImageList(Me.components)
            Me.radListBoxDemo = New Telerik.WinControls.UI.RadListControl()
            Me.radGroupSettings = New Telerik.WinControls.UI.RadGroupBox()
            Me.radCheckBox1 = New Telerik.WinControls.UI.RadCheckBox()
            Me.radComboSortStyle = New Telerik.WinControls.UI.RadDropDownList()
            Me.radComboSelectionMode = New Telerik.WinControls.UI.RadDropDownList()
            Me.radLblSort = New Telerik.WinControls.UI.RadLabel()
            Me.radLblSelection = New Telerik.WinControls.UI.RadLabel()
            Me.radGroupItem = New Telerik.WinControls.UI.RadGroupBox()
            Me.radComboTextOrientation = New Telerik.WinControls.UI.RadDropDownList()
            Me.radComboTextAlign = New Telerik.WinControls.UI.RadDropDownList()
            Me.radComboImageAlign = New Telerik.WinControls.UI.RadDropDownList()
            Me.radComboTextImage = New Telerik.WinControls.UI.RadDropDownList()
            Me.radLblTextImage = New Telerik.WinControls.UI.RadLabel()
            Me.radLblTextOrientation = New Telerik.WinControls.UI.RadLabel()
            Me.radLblTextAlignment = New Telerik.WinControls.UI.RadLabel()
            Me.radLblImageAlign = New Telerik.WinControls.UI.RadLabel()
            Me.radDropDownList1 = New Telerik.WinControls.UI.RadDropDownList()
            Me.radLabel1 = New Telerik.WinControls.UI.RadLabel()
            CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.settingsPanel.SuspendLayout()
            CType(Me.themePanel, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radListBoxDemo, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radGroupSettings, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radGroupSettings.SuspendLayout()
            CType(Me.radCheckBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radComboSortStyle, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radComboSelectionMode, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblSort, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblSelection, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radGroupItem, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radGroupItem.SuspendLayout()
            CType(Me.radComboTextOrientation, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radComboTextAlign, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radComboImageAlign, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radComboTextImage, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblTextImage, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblTextOrientation, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblTextAlignment, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblImageAlign, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radDropDownList1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'settingsPanel
            '
            Me.settingsPanel.Controls.Add(Me.radLabel1)
            Me.settingsPanel.Controls.Add(Me.radDropDownList1)
            Me.settingsPanel.Controls.Add(Me.radGroupItem)
            Me.settingsPanel.Controls.Add(Me.radGroupSettings)
            Me.settingsPanel.Location = New System.Drawing.Point(1101, 1)
            Me.settingsPanel.Size = New System.Drawing.Size(255, 747)
            Me.settingsPanel.Controls.SetChildIndex(Me.radGroupSettings, 0)
            Me.settingsPanel.Controls.SetChildIndex(Me.radGroupItem, 0)
            Me.settingsPanel.Controls.SetChildIndex(Me.radDropDownList1, 0)
            Me.settingsPanel.Controls.SetChildIndex(Me.radLabel1, 0)
            '
            'imageList1
            '
            Me.imageList1.ImageStream = CType(resources.GetObject("imageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
            Me.imageList1.TransparentColor = System.Drawing.Color.Fuchsia
            Me.imageList1.Images.SetKeyName(0, "")
            Me.imageList1.Images.SetKeyName(1, "")
            Me.imageList1.Images.SetKeyName(2, "")
            Me.imageList1.Images.SetKeyName(3, "")
            Me.imageList1.Images.SetKeyName(4, "")
            Me.imageList1.Images.SetKeyName(5, "MPEG.gif")
            Me.imageList1.Images.SetKeyName(6, "Quicktime.gif")
            Me.imageList1.Images.SetKeyName(7, "AudioCD.gif")
            Me.imageList1.Images.SetKeyName(8, "Folder.gif")
            Me.imageList1.Images.SetKeyName(9, "Help.gif")
            '
            'radListBoxDemo
            '
            Me.radListBoxDemo.AutoSizeItems = True
            Me.radListBoxDemo.BackColor = System.Drawing.Color.Transparent
            Me.radListBoxDemo.Location = New System.Drawing.Point(0, 0)
            Me.radListBoxDemo.Name = "radListBoxDemo"
            Me.radListBoxDemo.Size = New System.Drawing.Size(329, 368)
            Me.radListBoxDemo.TabIndex = 3
            Me.radListBoxDemo.Text = "(NONE)"
            '
            'radGroupSettings
            '
            Me.radGroupSettings.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
            Me.radGroupSettings.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radGroupSettings.Controls.Add(Me.radCheckBox1)
            Me.radGroupSettings.Controls.Add(Me.radComboSortStyle)
            Me.radGroupSettings.Controls.Add(Me.radComboSelectionMode)
            Me.radGroupSettings.Controls.Add(Me.radLblSort)
            Me.radGroupSettings.Controls.Add(Me.radLblSelection)
            Me.radGroupSettings.FooterText = ""
            Me.radGroupSettings.HeaderText = "ListBox General Settings"
            Me.radGroupSettings.Location = New System.Drawing.Point(10, 54)
            Me.radGroupSettings.Name = "radGroupSettings"
            Me.radGroupSettings.Padding = New System.Windows.Forms.Padding(10, 20, 10, 10)
            Me.radGroupSettings.Size = New System.Drawing.Size(235, 141)
            Me.radGroupSettings.TabIndex = 0
            Me.radGroupSettings.Text = "ListBox General Settings"
            '
            'radCheckBox1
            '
            Me.radCheckBox1.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radCheckBox1.Location = New System.Drawing.Point(5, 115)
            Me.radCheckBox1.Name = "radCheckBox1"
            Me.radCheckBox1.Size = New System.Drawing.Size(129, 18)
            Me.radCheckBox1.TabIndex = 2
            Me.radCheckBox1.Text = "Alternating item color"
            '
            'radComboSortStyle
            '
            Me.radComboSortStyle.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radComboSortStyle.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            RadListDataItem28.Text = "Ascending"
            RadListDataItem29.Text = "Descending"
            RadListDataItem30.Text = "None"
            Me.radComboSortStyle.Items.Add(RadListDataItem28)
            Me.radComboSortStyle.Items.Add(RadListDataItem29)
            Me.radComboSortStyle.Items.Add(RadListDataItem30)
            Me.radComboSortStyle.Location = New System.Drawing.Point(5, 85)
            Me.radComboSortStyle.Name = "radComboSortStyle"
            '
            '
            '
            Me.radComboSortStyle.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.radComboSortStyle.Size = New System.Drawing.Size(225, 20)
            Me.radComboSortStyle.TabIndex = 1
            '
            'radComboSelectionMode
            '
            Me.radComboSelectionMode.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radComboSelectionMode.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            RadListDataItem31.Text = "None"
            RadListDataItem32.Text = "One"
            RadListDataItem33.Text = "MultiSimple"
            RadListDataItem34.Text = "MultiExtended"
            Me.radComboSelectionMode.Items.Add(RadListDataItem31)
            Me.radComboSelectionMode.Items.Add(RadListDataItem32)
            Me.radComboSelectionMode.Items.Add(RadListDataItem33)
            Me.radComboSelectionMode.Items.Add(RadListDataItem34)
            Me.radComboSelectionMode.Location = New System.Drawing.Point(5, 39)
            Me.radComboSelectionMode.Name = "radComboSelectionMode"
            '
            '
            '
            Me.radComboSelectionMode.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.radComboSelectionMode.Size = New System.Drawing.Size(225, 20)
            Me.radComboSelectionMode.TabIndex = 1
            '
            'radLblSort
            '
            Me.radLblSort.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLblSort.Location = New System.Drawing.Point(5, 65)
            Me.radLblSort.Name = "radLblSort"
            Me.radLblSort.Size = New System.Drawing.Size(55, 18)
            Me.radLblSort.TabIndex = 0
            Me.radLblSort.Text = "Sort Style:"
            '
            'radLblSelection
            '
            Me.radLblSelection.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLblSelection.Location = New System.Drawing.Point(5, 19)
            Me.radLblSelection.Name = "radLblSelection"
            Me.radLblSelection.Size = New System.Drawing.Size(86, 18)
            Me.radLblSelection.TabIndex = 0
            Me.radLblSelection.Text = "Selection Mode:"
            '
            'radGroupItem
            '
            Me.radGroupItem.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
            Me.radGroupItem.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radGroupItem.Controls.Add(Me.radComboTextOrientation)
            Me.radGroupItem.Controls.Add(Me.radComboTextAlign)
            Me.radGroupItem.Controls.Add(Me.radComboImageAlign)
            Me.radGroupItem.Controls.Add(Me.radComboTextImage)
            Me.radGroupItem.Controls.Add(Me.radLblTextImage)
            Me.radGroupItem.Controls.Add(Me.radLblTextOrientation)
            Me.radGroupItem.Controls.Add(Me.radLblTextAlignment)
            Me.radGroupItem.Controls.Add(Me.radLblImageAlign)
            Me.radGroupItem.FooterText = ""
            Me.radGroupItem.HeaderText = "Item Settings"
            Me.radGroupItem.Location = New System.Drawing.Point(10, 201)
            Me.radGroupItem.Name = "radGroupItem"
            Me.radGroupItem.Padding = New System.Windows.Forms.Padding(10, 20, 10, 10)
            Me.radGroupItem.Size = New System.Drawing.Size(235, 225)
            Me.radGroupItem.TabIndex = 0
            Me.radGroupItem.Text = "Item Settings"
            '
            'radComboTextOrientation
            '
            Me.radComboTextOrientation.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radComboTextOrientation.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            RadListDataItem3.Text = "Horizontal"
            RadListDataItem4.Text = "Vertical"
            Me.radComboTextOrientation.Items.Add(RadListDataItem3)
            Me.radComboTextOrientation.Items.Add(RadListDataItem4)
            Me.radComboTextOrientation.Location = New System.Drawing.Point(5, 182)
            Me.radComboTextOrientation.Name = "radComboTextOrientation"
            '
            '
            '
            Me.radComboTextOrientation.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.radComboTextOrientation.Size = New System.Drawing.Size(225, 20)
            Me.radComboTextOrientation.TabIndex = 1
            '
            'radComboTextAlign
            '
            Me.radComboTextAlign.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radComboTextAlign.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            RadListDataItem5.Text = "TopLeft"
            RadListDataItem6.Text = "TopCenter"
            RadListDataItem7.Text = "TopRight"
            RadListDataItem8.Text = "MiddleLeft"
            RadListDataItem9.Text = "MiddleCenter"
            RadListDataItem10.Text = "MiddleRight"
            RadListDataItem11.Text = "BottomLeft"
            RadListDataItem12.Text = "BottomCenter"
            RadListDataItem13.Text = "BottomRight"
            Me.radComboTextAlign.Items.Add(RadListDataItem5)
            Me.radComboTextAlign.Items.Add(RadListDataItem6)
            Me.radComboTextAlign.Items.Add(RadListDataItem7)
            Me.radComboTextAlign.Items.Add(RadListDataItem8)
            Me.radComboTextAlign.Items.Add(RadListDataItem9)
            Me.radComboTextAlign.Items.Add(RadListDataItem10)
            Me.radComboTextAlign.Items.Add(RadListDataItem11)
            Me.radComboTextAlign.Items.Add(RadListDataItem12)
            Me.radComboTextAlign.Items.Add(RadListDataItem13)
            Me.radComboTextAlign.Location = New System.Drawing.Point(5, 91)
            Me.radComboTextAlign.Name = "radComboTextAlign"
            '
            '
            '
            Me.radComboTextAlign.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.radComboTextAlign.Size = New System.Drawing.Size(225, 20)
            Me.radComboTextAlign.TabIndex = 1
            '
            'radComboImageAlign
            '
            Me.radComboImageAlign.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radComboImageAlign.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            RadListDataItem14.Text = "TopLeft"
            RadListDataItem15.Text = "TopCenter"
            RadListDataItem16.Text = "TopRight"
            RadListDataItem17.Text = "MiddleLeft"
            RadListDataItem18.Text = "MiddleCenter"
            RadListDataItem19.Text = "MiddleRight"
            RadListDataItem20.Text = "BottomLeft"
            RadListDataItem21.Text = "BottomCenter"
            RadListDataItem22.Text = "BottomRight"
            Me.radComboImageAlign.Items.Add(RadListDataItem14)
            Me.radComboImageAlign.Items.Add(RadListDataItem15)
            Me.radComboImageAlign.Items.Add(RadListDataItem16)
            Me.radComboImageAlign.Items.Add(RadListDataItem17)
            Me.radComboImageAlign.Items.Add(RadListDataItem18)
            Me.radComboImageAlign.Items.Add(RadListDataItem19)
            Me.radComboImageAlign.Items.Add(RadListDataItem20)
            Me.radComboImageAlign.Items.Add(RadListDataItem21)
            Me.radComboImageAlign.Items.Add(RadListDataItem22)
            Me.radComboImageAlign.Location = New System.Drawing.Point(5, 44)
            Me.radComboImageAlign.Name = "radComboImageAlign"
            '
            '
            '
            Me.radComboImageAlign.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.radComboImageAlign.Size = New System.Drawing.Size(225, 20)
            Me.radComboImageAlign.TabIndex = 1
            '
            'radComboTextImage
            '
            Me.radComboTextImage.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radComboTextImage.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            RadListDataItem23.Text = "ImageAboveText"
            RadListDataItem24.Text = "ImageBeforeText"
            RadListDataItem25.Text = "TextAboveImage"
            RadListDataItem26.Text = "TextBeforeImage"
            RadListDataItem27.Text = "Overlay"
            Me.radComboTextImage.Items.Add(RadListDataItem23)
            Me.radComboTextImage.Items.Add(RadListDataItem24)
            Me.radComboTextImage.Items.Add(RadListDataItem25)
            Me.radComboTextImage.Items.Add(RadListDataItem26)
            Me.radComboTextImage.Items.Add(RadListDataItem27)
            Me.radComboTextImage.Location = New System.Drawing.Point(5, 137)
            Me.radComboTextImage.Name = "radComboTextImage"
            '
            '
            '
            Me.radComboTextImage.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.radComboTextImage.Size = New System.Drawing.Size(225, 20)
            Me.radComboTextImage.TabIndex = 1
            '
            'radLblTextImage
            '
            Me.radLblTextImage.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLblTextImage.Location = New System.Drawing.Point(5, 117)
            Me.radLblTextImage.Name = "radLblTextImage"
            Me.radLblTextImage.Size = New System.Drawing.Size(108, 18)
            Me.radLblTextImage.TabIndex = 0
            Me.radLblTextImage.Text = "Text-Image Relation:"
            '
            'radLblTextOrientation
            '
            Me.radLblTextOrientation.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLblTextOrientation.Location = New System.Drawing.Point(5, 162)
            Me.radLblTextOrientation.Name = "radLblTextOrientation"
            Me.radLblTextOrientation.Size = New System.Drawing.Size(88, 18)
            Me.radLblTextOrientation.TabIndex = 0
            Me.radLblTextOrientation.Text = "Text Orientation:"
            '
            'radLblTextAlignment
            '
            Me.radLblTextAlignment.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLblTextAlignment.Location = New System.Drawing.Point(5, 70)
            Me.radLblTextAlignment.Name = "radLblTextAlignment"
            Me.radLblTextAlignment.Size = New System.Drawing.Size(83, 18)
            Me.radLblTextAlignment.TabIndex = 0
            Me.radLblTextAlignment.Text = "Text Alignment:"
            '
            'radLblImageAlign
            '
            Me.radLblImageAlign.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLblImageAlign.Location = New System.Drawing.Point(5, 23)
            Me.radLblImageAlign.Name = "radLblImageAlign"
            Me.radLblImageAlign.Size = New System.Drawing.Size(94, 18)
            Me.radLblImageAlign.TabIndex = 0
            Me.radLblImageAlign.Text = "Image Alignment:"
            '
            'radDropDownList1
            '
            Me.radDropDownList1.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radDropDownList1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            RadListDataItem1.Text = "RadListDataItems"
            RadListDataItem2.Text = "DescriptionTextListDataItems"
            Me.radDropDownList1.Items.Add(RadListDataItem1)
            Me.radDropDownList1.Items.Add(RadListDataItem2)
            Me.radDropDownList1.Location = New System.Drawing.Point(10, 25)
            Me.radDropDownList1.Name = "radDropDownList1"
            Me.radDropDownList1.Size = New System.Drawing.Size(235, 20)
            Me.radDropDownList1.TabIndex = 1
            '
            'radLabel1
            '
            Me.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLabel1.Location = New System.Drawing.Point(10, 2)
            Me.radLabel1.Name = "radLabel1"
            Me.radLabel1.Size = New System.Drawing.Size(74, 18)
            Me.radLabel1.TabIndex = 2
            Me.radLabel1.Text = "Populate with"
            '
            'Form1
            '
            Me.Controls.Add(Me.radListBoxDemo)
            Me.Name = "Form1"
            Me.Padding = New System.Windows.Forms.Padding(2, 35, 2, 4)
            Me.Size = New System.Drawing.Size(1399, 917)
            Me.Controls.SetChildIndex(Me.themePanel, 0)
            Me.Controls.SetChildIndex(Me.radListBoxDemo, 0)
            Me.Controls.SetChildIndex(Me.settingsPanel, 0)
            CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).EndInit()
            Me.settingsPanel.ResumeLayout(False)
            Me.settingsPanel.PerformLayout()
            CType(Me.themePanel, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radListBoxDemo, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radGroupSettings, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radGroupSettings.ResumeLayout(False)
            Me.radGroupSettings.PerformLayout()
            CType(Me.radCheckBox1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radComboSortStyle, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radComboSelectionMode, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblSort, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblSelection, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radGroupItem, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radGroupItem.ResumeLayout(False)
            Me.radGroupItem.PerformLayout()
            CType(Me.radComboTextOrientation, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radComboTextAlign, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radComboImageAlign, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radComboTextImage, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblTextImage, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblTextOrientation, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblTextAlignment, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblImageAlign, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radDropDownList1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

		#End Region

		Private radListBoxDemo As Telerik.WinControls.UI.RadListControl
		Private imageList1 As ImageList
		Private radGroupSettings As Telerik.WinControls.UI.RadGroupBox
		Private radComboSortStyle As Telerik.WinControls.UI.RadDropDownList
		Private radComboSelectionMode As Telerik.WinControls.UI.RadDropDownList
		Private radLblSort As Telerik.WinControls.UI.RadLabel
		Private radLblSelection As Telerik.WinControls.UI.RadLabel
		Private radGroupItem As Telerik.WinControls.UI.RadGroupBox
		Private radComboTextOrientation As Telerik.WinControls.UI.RadDropDownList
		Private radComboTextImage As Telerik.WinControls.UI.RadDropDownList
		Private radLblTextOrientation As Telerik.WinControls.UI.RadLabel
		Private radComboImageAlign As Telerik.WinControls.UI.RadDropDownList
		Private radLblImageAlign As Telerik.WinControls.UI.RadLabel
		Private radComboTextAlign As Telerik.WinControls.UI.RadDropDownList
		Private radLblTextAlignment As Telerik.WinControls.UI.RadLabel
		Private radLblTextImage As Telerik.WinControls.UI.RadLabel
		Private radDropDownList1 As Telerik.WinControls.UI.RadDropDownList
        Private radLabel1 As Telerik.WinControls.UI.RadLabel
        Friend WithEvents radCheckBox1 As Telerik.WinControls.UI.RadCheckBox
	End Class
End Namespace