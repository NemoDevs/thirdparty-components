﻿using System;
using System.Linq;
using Telerik.QuickStart.WinControls;

namespace Telerik.Examples.WinControls.DropDownListAndListControl.CheckedDropDownList
{
    public partial class Form1 : ExamplesForm
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            Telerik.WinControls.RadMessageBox.Instance.ThemeName = CurrentThemeName;
            Telerik.WinControls.RadMessageBox.Show("Appointment created.", "Message");
        }
    }
}
