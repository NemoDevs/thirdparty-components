﻿Imports System
Imports System.Linq
Imports Telerik.QuickStart.WinControls

Namespace Telerik.Examples.WinControls.DropDownListAndListControl.CheckedDropDownList
    Partial Public Class Form1
        Inherits ExamplesForm
        Public Sub New()
            InitializeComponent()
        End Sub

        Private Sub radButton1_Click(sender As Object, e As System.EventArgs) Handles radButton1.Click
            Telerik.WinControls.RadMessageBox.Instance.ThemeName = CurrentThemeName
            Telerik.WinControls.RadMessageBox.Show("Appointment created.", "Message")
        End Sub
    End Class
End Namespace