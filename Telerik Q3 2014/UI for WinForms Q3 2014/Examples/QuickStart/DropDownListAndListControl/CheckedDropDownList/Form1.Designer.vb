﻿Namespace Telerik.Examples.WinControls.DropDownListAndListControl.CheckedDropDownList
    Partial Class Form1
        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub



        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Dim radCheckedListDataItem14 As New Telerik.WinControls.UI.RadCheckedListDataItem()
            Dim radCheckedListDataItem15 As New Telerik.WinControls.UI.RadCheckedListDataItem()
            Dim radCheckedListDataItem16 As New Telerik.WinControls.UI.RadCheckedListDataItem()
            Dim radCheckedListDataItem17 As New Telerik.WinControls.UI.RadCheckedListDataItem()
            Dim radCheckedListDataItem18 As New Telerik.WinControls.UI.RadCheckedListDataItem()
            Dim radCheckedListDataItem19 As New Telerik.WinControls.UI.RadCheckedListDataItem()
            Dim radCheckedListDataItem1 As New Telerik.WinControls.UI.RadCheckedListDataItem()
            Dim radCheckedListDataItem2 As New Telerik.WinControls.UI.RadCheckedListDataItem()
            Dim radCheckedListDataItem3 As New Telerik.WinControls.UI.RadCheckedListDataItem()
            Dim radCheckedListDataItem4 As New Telerik.WinControls.UI.RadCheckedListDataItem()
            Dim radCheckedListDataItem5 As New Telerik.WinControls.UI.RadCheckedListDataItem()
            Dim radCheckedListDataItem6 As New Telerik.WinControls.UI.RadCheckedListDataItem()
            Dim radCheckedListDataItem20 As New Telerik.WinControls.UI.RadCheckedListDataItem()
            Me.radLabel1 = New Telerik.WinControls.UI.RadLabel()
            Me.radLabel2 = New Telerik.WinControls.UI.RadLabel()
            Me.radCheckedDropDownList1 = New Telerik.WinControls.UI.RadCheckedDropDownList()
            Me.radLabel3 = New Telerik.WinControls.UI.RadLabel()
            Me.radLabel4 = New Telerik.WinControls.UI.RadLabel()
            Me.radCheckedDropDownList2 = New Telerik.WinControls.UI.RadCheckedDropDownList()
            Me.radTimePicker1 = New Telerik.WinControls.UI.RadTimePicker()
            Me.radTextBox1 = New Telerik.WinControls.UI.RadTextBox()
            Me.radButton1 = New Telerik.WinControls.UI.RadButton()
            CType((Me.settingsPanel), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.themePanel), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.radLabel1), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.radLabel2), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.radCheckedDropDownList1), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.radLabel3), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.radLabel4), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.radCheckedDropDownList2), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.radTimePicker1), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.radTextBox1), System.ComponentModel.ISupportInitialize).BeginInit()
            CType((Me.radButton1), System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            ' 
            ' settingsPanel
            ' 
            Me.settingsPanel.Padding = New System.Windows.Forms.Padding(3)
            ' 
            ' themePanel
            ' 
            Me.themePanel.Padding = New System.Windows.Forms.Padding(3)
            ' 
            ' radLabel1
            ' 
            Me.radLabel1.Font = New System.Drawing.Font("Segoe UI", 10.0F)
            Me.radLabel1.Location = New System.Drawing.Point(0, 27)
            Me.radLabel1.Name = "radLabel1"
            Me.radLabel1.Size = New System.Drawing.Size(62, 21)
            Me.radLabel1.TabIndex = 2
            Me.radLabel1.Text = "New task"
            ' 
            ' radLabel2
            ' 
            Me.radLabel2.Location = New System.Drawing.Point(0, 102)
            Me.radLabel2.Name = "radLabel2"
            Me.radLabel2.Size = New System.Drawing.Size(51, 18)
            Me.radLabel2.TabIndex = 4
            Me.radLabel2.Text = "Category"
            ' 
            ' radCheckedDropDownList1
            ' 
            Me.radCheckedDropDownList1.CheckedMember = Nothing
            radCheckedListDataItem14.Checked = True
            radCheckedListDataItem14.Text = "work"
            radCheckedListDataItem15.Checked = True
            radCheckedListDataItem15.Text = "to do"
            radCheckedListDataItem16.Checked = True
            radCheckedListDataItem16.Text = "emails"
            radCheckedListDataItem17.Text = "forwarded"
            radCheckedListDataItem18.Text = "replied"
            radCheckedListDataItem19.Text = "duplicate"
            Me.radCheckedDropDownList1.Items.Add(radCheckedListDataItem14)
            Me.radCheckedDropDownList1.Items.Add(radCheckedListDataItem15)
            Me.radCheckedDropDownList1.Items.Add(radCheckedListDataItem16)
            Me.radCheckedDropDownList1.Items.Add(radCheckedListDataItem17)
            Me.radCheckedDropDownList1.Items.Add(radCheckedListDataItem18)
            Me.radCheckedDropDownList1.Items.Add(radCheckedListDataItem19)
            Me.radCheckedDropDownList1.Location = New System.Drawing.Point(0, 121)
            Me.radCheckedDropDownList1.Name = "radCheckedDropDownList1"
            Me.radCheckedDropDownList1.Size = New System.Drawing.Size(300, 22)
            Me.radCheckedDropDownList1.TabIndex = 5
            Me.radCheckedDropDownList1.Text = "work;to do;emails;"
            ' 
            ' radLabel3
            ' 
            Me.radLabel3.Location = New System.Drawing.Point(0, 154)
            Me.radLabel3.Name = "radLabel3"
            Me.radLabel3.Size = New System.Drawing.Size(54, 18)
            Me.radLabel3.TabIndex = 6
            Me.radLabel3.Text = "Reminder"
            ' 
            ' radLabel4
            ' 
            Me.radLabel4.Location = New System.Drawing.Point(0, 210)
            Me.radLabel4.Name = "radLabel4"
            Me.radLabel4.Size = New System.Drawing.Size(98, 18)
            Me.radLabel4.TabIndex = 7
            Me.radLabel4.Text = "Appointment time"
            ' 
            ' radCheckedDropDownList2
            ' 
            Me.radCheckedDropDownList2.CheckedMember = Nothing
            radCheckedListDataItem1.Checked = True
            radCheckedListDataItem1.Text = "Monday"
            radCheckedListDataItem2.Checked = True
            radCheckedListDataItem2.Text = "Tuesday"
            radCheckedListDataItem3.Text = "Wednesday"
            radCheckedListDataItem4.Text = "Thursday"
            radCheckedListDataItem5.Text = "Friday"
            radCheckedListDataItem6.Text = "Saturday"
            radCheckedListDataItem20.Text = "Sunday"
            Me.radCheckedDropDownList2.Items.Add(radCheckedListDataItem1)
            Me.radCheckedDropDownList2.Items.Add(radCheckedListDataItem2)
            Me.radCheckedDropDownList2.Items.Add(radCheckedListDataItem3)
            Me.radCheckedDropDownList2.Items.Add(radCheckedListDataItem4)
            Me.radCheckedDropDownList2.Items.Add(radCheckedListDataItem5)
            Me.radCheckedDropDownList2.Items.Add(radCheckedListDataItem6)
            Me.radCheckedDropDownList2.Items.Add(radCheckedListDataItem20)
            Me.radCheckedDropDownList2.Location = New System.Drawing.Point(0, 173)
            Me.radCheckedDropDownList2.Name = "radCheckedDropDownList2"
            Me.radCheckedDropDownList2.Size = New System.Drawing.Size(300, 22)
            Me.radCheckedDropDownList2.TabIndex = 8
            Me.radCheckedDropDownList2.Text = "Monday;Tuesday;"
            ' 
            ' radTimePicker1
            ' 
            Me.radTimePicker1.Location = New System.Drawing.Point(0, 230)
            Me.radTimePicker1.Name = "radTimePicker1"
            Me.radTimePicker1.Size = New System.Drawing.Size(298, 20)
            Me.radTimePicker1.TabIndex = 9
            Me.radTimePicker1.TabStop = False
            Me.radTimePicker1.Text = "radTimePicker1"
            Me.radTimePicker1.Value = New System.DateTime(2014, 9, 3, 10, 43, 22, _
                508)
            ' 
            ' radTextBox1
            ' 
            Me.radTextBox1.Location = New System.Drawing.Point(0, 70)
            Me.radTextBox1.Name = "radTextBox1"
            Me.radTextBox1.NullText = "Task name"
            Me.radTextBox1.Size = New System.Drawing.Size(300, 20)
            Me.radTextBox1.TabIndex = 10
            Me.radTextBox1.Text = "Task name"
            ' 
            ' radButton1
            ' 
            Me.radButton1.Location = New System.Drawing.Point(0, 279)
            Me.radButton1.Name = "radButton1"
            Me.radButton1.Size = New System.Drawing.Size(110, 24)
            Me.radButton1.TabIndex = 11
            Me.radButton1.Text = "Create"

            ' 
            ' Form1
            ' 
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0F, 13.0F)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.radButton1)
            Me.Controls.Add(Me.radTextBox1)
            Me.Controls.Add(Me.radTimePicker1)
            Me.Controls.Add(Me.radCheckedDropDownList2)
            Me.Controls.Add(Me.radLabel4)
            Me.Controls.Add(Me.radLabel3)
            Me.Controls.Add(Me.radCheckedDropDownList1)
            Me.Controls.Add(Me.radLabel2)
            Me.Controls.Add(Me.radLabel1)
            Me.Name = "Form1"
            Me.Size = New System.Drawing.Size(1414, 607)
            Me.Controls.SetChildIndex(Me.radLabel1, 0)
            Me.Controls.SetChildIndex(Me.radLabel2, 0)
            Me.Controls.SetChildIndex(Me.radCheckedDropDownList1, 0)
            Me.Controls.SetChildIndex(Me.radLabel3, 0)
            Me.Controls.SetChildIndex(Me.radLabel4, 0)
            Me.Controls.SetChildIndex(Me.radCheckedDropDownList2, 0)
            Me.Controls.SetChildIndex(Me.radTimePicker1, 0)
            Me.Controls.SetChildIndex(Me.radTextBox1, 0)
            Me.Controls.SetChildIndex(Me.settingsPanel, 0)
            Me.Controls.SetChildIndex(Me.themePanel, 0)
            Me.Controls.SetChildIndex(Me.radButton1, 0)
            CType((Me.settingsPanel), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.themePanel), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.radLabel1), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.radLabel2), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.radCheckedDropDownList1), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.radLabel3), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.radLabel4), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.radCheckedDropDownList2), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.radTimePicker1), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.radTextBox1), System.ComponentModel.ISupportInitialize).EndInit()
            CType((Me.radButton1), System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub



        Private radLabel1 As Telerik.WinControls.UI.RadLabel
        Private radLabel2 As Telerik.WinControls.UI.RadLabel
        Private radCheckedDropDownList1 As Telerik.WinControls.UI.RadCheckedDropDownList
        Private radLabel3 As Telerik.WinControls.UI.RadLabel
        Private radLabel4 As Telerik.WinControls.UI.RadLabel
        Private radCheckedDropDownList2 As Telerik.WinControls.UI.RadCheckedDropDownList
        Private radTimePicker1 As Telerik.WinControls.UI.RadTimePicker
        Private radTextBox1 As Telerik.WinControls.UI.RadTextBox
        Friend WithEvents radButton1 As Telerik.WinControls.UI.RadButton
    End Class
End Namespace