﻿namespace Telerik.Examples.WinControls.GanttView.Settings
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.radGanttView1 = new Telerik.WinControls.UI.RadGanttView();
            this.radRibbonBar1 = new Telerik.WinControls.UI.RadRibbonBar();
            this.ribbonTab1 = new Telerik.WinControls.UI.RibbonTab();
            this.radRibbonBarGroup1 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElementAddTask = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElementDeleteTask = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElementAddMilestone = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup2 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElementProgress25 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElementProgress50 = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElementProgress100 = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup3 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElementWeek = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElementMonth = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElementYear = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup6 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radRibbonBarButtonGroup1 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radLabelElement1 = new Telerik.WinControls.UI.RadLabelElement();
            this.radDateTimePickerElementStart = new Telerik.WinControls.UI.RadDateTimePickerElement();
            this.radRibbonBarButtonGroup2 = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.radLabelElement2 = new Telerik.WinControls.UI.RadLabelElement();
            this.radDateTimePickerElementEnd = new Telerik.WinControls.UI.RadDateTimePickerElement();
            this.radRibbonBarGroup4 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radButtonElementPrevious = new Telerik.WinControls.UI.RadButtonElement();
            this.radButtonElementNext = new Telerik.WinControls.UI.RadButtonElement();
            this.radRibbonBarGroup5 = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.radTrackBarElementZoom = new Telerik.WinControls.UI.RadTrackBarElement();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGanttView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // radGanttView1
            // 
            this.radGanttView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGanttView1.Location = new System.Drawing.Point(0, 115);
            this.radGanttView1.Name = "radGanttView1";
            this.radGanttView1.Ratio = 0.3417635F;
            this.radGanttView1.Size = new System.Drawing.Size(1873, 970);
            this.radGanttView1.SplitterWidth = 7;
            this.radGanttView1.TabIndex = 0;
            this.radGanttView1.Text = "radGanttView1";
            this.radGanttView1.ThemeName = "TelerikMetroBlue";
            // 
            // radRibbonBar1
            // 
            this.radRibbonBar1.CommandTabs.AddRange(new Telerik.WinControls.RadItem[] {
            this.ribbonTab1});
            this.radRibbonBar1.Location = new System.Drawing.Point(0, 0);
            this.radRibbonBar1.Name = "radRibbonBar1";
            this.radRibbonBar1.Size = new System.Drawing.Size(1873, 115);
            this.radRibbonBar1.StartButtonImage = ((System.Drawing.Image)(resources.GetObject("radRibbonBar1.StartButtonImage")));
            this.radRibbonBar1.TabIndex = 4;
            this.radRibbonBar1.Text = "RadGanttViewExample";
            this.radRibbonBar1.ThemeName = "TelerikMetroBlue";
            ((Telerik.WinControls.UI.RadRibbonBarElement)(this.radRibbonBar1.GetChildAt(0))).Text = "RadGanttViewExample";
            ((Telerik.WinControls.UI.RadQuickAccessToolBar)(this.radRibbonBar1.GetChildAt(0).GetChildAt(2))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            ((Telerik.WinControls.UI.RadRibbonBarCaption)(this.radRibbonBar1.GetChildAt(0).GetChildAt(3))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            ((Telerik.WinControls.UI.StripViewItemContainer)(this.radRibbonBar1.GetChildAt(0).GetChildAt(4).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            ((Telerik.WinControls.UI.RadPageViewContentAreaElement)(this.radRibbonBar1.GetChildAt(0).GetChildAt(4).GetChildAt(1))).Margin = new System.Windows.Forms.Padding(0);
            ((Telerik.WinControls.UI.RadApplicationMenuButtonElement)(this.radRibbonBar1.GetChildAt(0).GetChildAt(5))).Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            ((Telerik.WinControls.UI.RadApplicationMenuButtonElement)(this.radRibbonBar1.GetChildAt(0).GetChildAt(5))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radRibbonBar1.GetChildAt(0).GetChildAt(6))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.AccessibleDescription = "s";
            this.ribbonTab1.AccessibleName = "s";
            this.ribbonTab1.IsSelected = true;
            this.ribbonTab1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarGroup1,
            this.radRibbonBarGroup2,
            this.radRibbonBarGroup3,
            this.radRibbonBarGroup6,
            this.radRibbonBarGroup4,
            this.radRibbonBarGroup5});
            this.ribbonTab1.Name = "ribbonTab1";
            this.ribbonTab1.Row = -1;
            this.ribbonTab1.Text = "s";
            this.ribbonTab1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup1
            // 
            this.radRibbonBarGroup1.AccessibleDescription = "ITEMS";
            this.radRibbonBarGroup1.AccessibleName = "ITEMS";
            this.radRibbonBarGroup1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElementAddTask,
            this.radButtonElementDeleteTask,
            this.radButtonElementAddMilestone});
            this.radRibbonBarGroup1.Name = "radRibbonBarGroup1";
            this.radRibbonBarGroup1.Text = "ITEMS";
            this.radRibbonBarGroup1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElementAddTask
            // 
            this.radButtonElementAddTask.AccessibleDescription = "Add child to selected item";
            this.radButtonElementAddTask.AccessibleName = "Add child to selected item";
            this.radButtonElementAddTask.Image = global::Telerik.Examples.WinControls.Properties.Resources.GanttAdd;
            this.radButtonElementAddTask.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElementAddTask.MaxSize = new System.Drawing.Size(80, 0);
            this.radButtonElementAddTask.Name = "radButtonElementAddTask";
            this.radButtonElementAddTask.StretchHorizontally = true;
            this.radButtonElementAddTask.Text = "Add child to selected item";
            this.radButtonElementAddTask.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElementAddTask.TextWrap = true;
            this.radButtonElementAddTask.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElementDeleteTask
            // 
            this.radButtonElementDeleteTask.AccessibleDescription = "Delete selected task";
            this.radButtonElementDeleteTask.AccessibleName = "Delete selected task";
            this.radButtonElementDeleteTask.Image = global::Telerik.Examples.WinControls.Properties.Resources.GanttDelete;
            this.radButtonElementDeleteTask.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElementDeleteTask.MaxSize = new System.Drawing.Size(80, 0);
            this.radButtonElementDeleteTask.Name = "radButtonElementDeleteTask";
            this.radButtonElementDeleteTask.Text = "Delete selected task";
            this.radButtonElementDeleteTask.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElementDeleteTask.TextWrap = true;
            this.radButtonElementDeleteTask.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElementAddMilestone
            // 
            this.radButtonElementAddMilestone.AccessibleDescription = "Add milestone";
            this.radButtonElementAddMilestone.AccessibleName = "Add milestone";
            this.radButtonElementAddMilestone.Image = global::Telerik.Examples.WinControls.Properties.Resources.GanttAddMilestone;
            this.radButtonElementAddMilestone.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElementAddMilestone.MaxSize = new System.Drawing.Size(80, 0);
            this.radButtonElementAddMilestone.Name = "radButtonElementAddMilestone";
            this.radButtonElementAddMilestone.Text = "Add milestone";
            this.radButtonElementAddMilestone.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElementAddMilestone.TextWrap = true;
            this.radButtonElementAddMilestone.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup2
            // 
            this.radRibbonBarGroup2.AccessibleDescription = "TASK PROGRESS";
            this.radRibbonBarGroup2.AccessibleName = "TASK PROGRESS";
            this.radRibbonBarGroup2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElementProgress25,
            this.radButtonElementProgress50,
            this.radButtonElementProgress100});
            this.radRibbonBarGroup2.Name = "radRibbonBarGroup2";
            this.radRibbonBarGroup2.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarGroup2.Text = "TASK PROGRESS";
            this.radRibbonBarGroup2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElementProgress25
            // 
            this.radButtonElementProgress25.AccessibleDescription = "25%";
            this.radButtonElementProgress25.AccessibleName = "25%";
            this.radButtonElementProgress25.Image = global::Telerik.Examples.WinControls.Properties.Resources.Gantt25;
            this.radButtonElementProgress25.Name = "radButtonElementProgress25";
            this.radButtonElementProgress25.Text = "25%";
            this.radButtonElementProgress25.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButtonElementProgress25.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElementProgress50
            // 
            this.radButtonElementProgress50.AccessibleDescription = "50%";
            this.radButtonElementProgress50.AccessibleName = "50%";
            this.radButtonElementProgress50.Image = global::Telerik.Examples.WinControls.Properties.Resources.Gantt50;
            this.radButtonElementProgress50.Name = "radButtonElementProgress50";
            this.radButtonElementProgress50.Text = "50%";
            this.radButtonElementProgress50.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButtonElementProgress50.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElementProgress100
            // 
            this.radButtonElementProgress100.AccessibleDescription = "100%";
            this.radButtonElementProgress100.AccessibleName = "100%";
            this.radButtonElementProgress100.Image = global::Telerik.Examples.WinControls.Properties.Resources.Gantt100;
            this.radButtonElementProgress100.Name = "radButtonElementProgress100";
            this.radButtonElementProgress100.Text = "100%";
            this.radButtonElementProgress100.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButtonElementProgress100.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup3
            // 
            this.radRibbonBarGroup3.AccessibleDescription = "NAVIGATE";
            this.radRibbonBarGroup3.AccessibleName = "NAVIGATE";
            this.radRibbonBarGroup3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElementWeek,
            this.radButtonElementMonth,
            this.radButtonElementYear});
            this.radRibbonBarGroup3.Name = "radRibbonBarGroup3";
            this.radRibbonBarGroup3.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarGroup3.Text = "VIEW MODE";
            this.radRibbonBarGroup3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElementWeek
            // 
            this.radButtonElementWeek.AccessibleDescription = "Week";
            this.radButtonElementWeek.AccessibleName = "Week";
            this.radButtonElementWeek.Image = global::Telerik.Examples.WinControls.Properties.Resources.GanttWeek;
            this.radButtonElementWeek.Name = "radButtonElementWeek";
            this.radButtonElementWeek.Text = "Week";
            this.radButtonElementWeek.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButtonElementWeek.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButtonElementWeek.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElementMonth
            // 
            this.radButtonElementMonth.AccessibleDescription = "Month";
            this.radButtonElementMonth.AccessibleName = "Month";
            this.radButtonElementMonth.Image = global::Telerik.Examples.WinControls.Properties.Resources.GanttMonth;
            this.radButtonElementMonth.Name = "radButtonElementMonth";
            this.radButtonElementMonth.Text = "Month";
            this.radButtonElementMonth.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButtonElementMonth.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButtonElementMonth.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElementYear
            // 
            this.radButtonElementYear.AccessibleDescription = "Year";
            this.radButtonElementYear.AccessibleName = "Year";
            this.radButtonElementYear.Image = global::Telerik.Examples.WinControls.Properties.Resources.GanttYear;
            this.radButtonElementYear.Name = "radButtonElementYear";
            this.radButtonElementYear.Text = "Year";
            this.radButtonElementYear.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButtonElementYear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radButtonElementYear.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup6
            // 
            this.radRibbonBarGroup6.AccessibleDescription = "TIMERANGE";
            this.radRibbonBarGroup6.AccessibleName = "TIMERANGE";
            this.radRibbonBarGroup6.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radRibbonBarButtonGroup1,
            this.radRibbonBarButtonGroup2});
            this.radRibbonBarGroup6.MinSize = new System.Drawing.Size(165, 86);
            this.radRibbonBarGroup6.Name = "radRibbonBarGroup6";
            this.radRibbonBarGroup6.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.radRibbonBarGroup6.Text = "TIMELINE RANGE";
            this.radRibbonBarGroup6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarButtonGroup1
            // 
            this.radRibbonBarButtonGroup1.AccessibleDescription = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.AccessibleName = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radLabelElement1,
            this.radDateTimePickerElementStart});
            this.radRibbonBarButtonGroup1.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.radRibbonBarButtonGroup1.Name = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.Padding = new System.Windows.Forms.Padding(1);
            this.radRibbonBarButtonGroup1.ShowBackColor = false;
            this.radRibbonBarButtonGroup1.Text = "radRibbonBarButtonGroup1";
            this.radRibbonBarButtonGroup1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radLabelElement1
            // 
            this.radLabelElement1.AccessibleDescription = "Start";
            this.radLabelElement1.AccessibleName = "Start";
            this.radLabelElement1.Name = "radLabelElement1";
            this.radLabelElement1.Text = "Start";
            this.radLabelElement1.TextWrap = true;
            this.radLabelElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radDateTimePickerElementStart
            // 
            this.radDateTimePickerElementStart.AccessibleDescription = "radButtonElement9";
            this.radDateTimePickerElementStart.AccessibleName = "radButtonElement9";
            this.radDateTimePickerElementStart.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.radDateTimePickerElementStart.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.radDateTimePickerElementStart.MinDate = new System.DateTime(((long)(0)));
            this.radDateTimePickerElementStart.MinSize = new System.Drawing.Size(130, 0);
            this.radDateTimePickerElementStart.Name = "radDateTimePickerElementStart";
            this.radDateTimePickerElementStart.NullDate = new System.DateTime(((long)(0)));
            this.radDateTimePickerElementStart.StretchVertically = false;
            this.radDateTimePickerElementStart.Text = "radButtonElement9";
            this.radDateTimePickerElementStart.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarButtonGroup2
            // 
            this.radRibbonBarButtonGroup2.AccessibleDescription = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.AccessibleName = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radLabelElement2,
            this.radDateTimePickerElementEnd});
            this.radRibbonBarButtonGroup2.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.radRibbonBarButtonGroup2.Name = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.Padding = new System.Windows.Forms.Padding(1);
            this.radRibbonBarButtonGroup2.ShowBackColor = false;
            this.radRibbonBarButtonGroup2.Text = "radRibbonBarButtonGroup2";
            this.radRibbonBarButtonGroup2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radLabelElement2
            // 
            this.radLabelElement2.AccessibleDescription = "End";
            this.radLabelElement2.AccessibleName = "End";
            this.radLabelElement2.MinSize = new System.Drawing.Size(32, 0);
            this.radLabelElement2.Name = "radLabelElement2";
            this.radLabelElement2.Text = "End";
            this.radLabelElement2.TextWrap = true;
            this.radLabelElement2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radDateTimePickerElementEnd
            // 
            this.radDateTimePickerElementEnd.AccessibleDescription = "radButtonElement10";
            this.radDateTimePickerElementEnd.AccessibleName = "radButtonElement10";
            this.radDateTimePickerElementEnd.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.radDateTimePickerElementEnd.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.radDateTimePickerElementEnd.MinDate = new System.DateTime(((long)(0)));
            this.radDateTimePickerElementEnd.MinSize = new System.Drawing.Size(130, 0);
            this.radDateTimePickerElementEnd.Name = "radDateTimePickerElementEnd";
            this.radDateTimePickerElementEnd.NullDate = new System.DateTime(((long)(0)));
            this.radDateTimePickerElementEnd.StretchVertically = false;
            this.radDateTimePickerElementEnd.Text = "radButtonElement10";
            this.radDateTimePickerElementEnd.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup4
            // 
            this.radRibbonBarGroup4.AccessibleDescription = "ZOOM";
            this.radRibbonBarGroup4.AccessibleName = "ZOOM";
            this.radRibbonBarGroup4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElementPrevious,
            this.radButtonElementNext});
            this.radRibbonBarGroup4.Name = "radRibbonBarGroup4";
            this.radRibbonBarGroup4.Text = "NAVIGATE";
            this.radRibbonBarGroup4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElementPrevious
            // 
            this.radButtonElementPrevious.AccessibleDescription = "Previous";
            this.radButtonElementPrevious.AccessibleName = "Previous";
            this.radButtonElementPrevious.Image = global::Telerik.Examples.WinControls.Properties.Resources.GanttPrevious;
            this.radButtonElementPrevious.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElementPrevious.MinSize = new System.Drawing.Size(60, 0);
            this.radButtonElementPrevious.Name = "radButtonElementPrevious";
            this.radButtonElementPrevious.Text = "Previous";
            this.radButtonElementPrevious.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElementPrevious.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radButtonElementNext
            // 
            this.radButtonElementNext.AccessibleDescription = "Next";
            this.radButtonElementNext.AccessibleName = "Next";
            this.radButtonElementNext.Image = global::Telerik.Examples.WinControls.Properties.Resources.GanttNext;
            this.radButtonElementNext.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButtonElementNext.MinSize = new System.Drawing.Size(60, 0);
            this.radButtonElementNext.Name = "radButtonElementNext";
            this.radButtonElementNext.Text = "Next";
            this.radButtonElementNext.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radButtonElementNext.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radRibbonBarGroup5
            // 
            this.radRibbonBarGroup5.AccessibleDescription = "ZOOM";
            this.radRibbonBarGroup5.AccessibleName = "ZOOM";
            this.radRibbonBarGroup5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radTrackBarElementZoom});
            this.radRibbonBarGroup5.Name = "radRibbonBarGroup5";
            this.radRibbonBarGroup5.Text = "ZOOM";
            this.radRibbonBarGroup5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radTrackBarElementZoom
            // 
            this.radTrackBarElementZoom.AccessibleDescription = "radButtonElement9";
            this.radTrackBarElementZoom.AccessibleName = "radButtonElement9";
            this.radTrackBarElementZoom.FitInAvailableSize = true;
            this.radTrackBarElementZoom.Margin = new System.Windows.Forms.Padding(0, 30, 0, 0);
            this.radTrackBarElementZoom.Maximum = 60F;
            this.radTrackBarElementZoom.Minimum = 1F;
            this.radTrackBarElementZoom.Name = "radTrackBarElementZoom";
            this.radTrackBarElementZoom.Text = "radButtonElement9";
            this.radTrackBarElementZoom.Value = 30F;
            this.radTrackBarElementZoom.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radGanttView1);
            this.Controls.Add(this.radRibbonBar1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1883, 1095);
            this.Controls.SetChildIndex(this.radRibbonBar1, 0);
            this.Controls.SetChildIndex(this.radGanttView1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.themePanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGanttView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRibbonBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadGanttView radGanttView1;
        private Telerik.WinControls.UI.RadRibbonBar radRibbonBar1;
        private Telerik.WinControls.UI.RibbonTab ribbonTab1;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElementAddTask;
        private Telerik.WinControls.UI.RadButtonElement radButtonElementDeleteTask;
        private Telerik.WinControls.UI.RadButtonElement radButtonElementAddMilestone;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup2;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup3;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup4;
        private Telerik.WinControls.UI.RadButtonElement radButtonElementProgress25;
        private Telerik.WinControls.UI.RadButtonElement radButtonElementProgress50;
        private Telerik.WinControls.UI.RadButtonElement radButtonElementProgress100;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup5;
        private Telerik.WinControls.UI.RadButtonElement radButtonElementWeek;
        private Telerik.WinControls.UI.RadButtonElement radButtonElementMonth;
        private Telerik.WinControls.UI.RadButtonElement radButtonElementYear;
        private Telerik.WinControls.UI.RadButtonElement radButtonElementPrevious;
        private Telerik.WinControls.UI.RadButtonElement radButtonElementNext;
        private Telerik.WinControls.UI.RadRibbonBarGroup radRibbonBarGroup6;
        private Telerik.WinControls.UI.RadDateTimePickerElement radDateTimePickerElementStart;
        private Telerik.WinControls.UI.RadDateTimePickerElement radDateTimePickerElementEnd;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup1;
        private Telerik.WinControls.UI.RadLabelElement radLabelElement1;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup radRibbonBarButtonGroup2;
        private Telerik.WinControls.UI.RadLabelElement radLabelElement2;
        private Telerik.WinControls.UI.RadTrackBarElement radTrackBarElementZoom;
    }
}

