﻿Imports Telerik.WinControls.UI
Imports Telerik.WinControls
Imports Telerik.Examples.WinControls.Forms.AboutBox
Imports Telerik.QuickStart.WinControls

Namespace Telerik.Examples.WinControls.Forms.FormTitleBarStatusStrip
	''' <summary>
	''' example form
	''' </summary>
	Partial Public Class Form1
		Inherits ExamplesRadForm
		''' <summary>
		''' default constructor
		''' </summary>
		Public Sub New()
			InitializeComponent()

			Me.radGridView1.TableElement.RowHeight = 60
			Me.radGridView1.TableElement.Text = ""
		End Sub

		Protected Overrides Sub WireEvents()
			AddHandler timer1.Tick, AddressOf timer1_Tick
			AddHandler radMenuItem26.Click, AddressOf OnRadMenuItemChangeTheme_Click
			AddHandler radMenuItem22.Click, AddressOf OnRadMenuItemChangeTheme_Click
			AddHandler radMenuItem17.Click, AddressOf radMenuItem17_Click
			AddHandler radMenuItem12.Click, AddressOf OnRadMenuItemChangeTheme_Click
		End Sub

		Private Sub timer1_Tick(ByVal sender As Object, ByVal e As EventArgs)
			If Me.radProgressBarElement1.Value1 < Me.radProgressBarElement1.Maximum Then
				Me.radProgressBarElement1.Value1 += 1
			Else
				Me.radProgressBarElement1.Value1 = Me.radProgressBarElement1.Minimum
			End If
		End Sub

		Private Sub OnRadMenuItemChangeTheme_Click(ByVal sender As Object, ByVal e As EventArgs)
			Dim menuItem As RadMenuItem = CType(sender, RadMenuItem)

			For Each sibling As RadMenuItem In menuItem.HierarchyParent.Items
				sibling.IsChecked = False
			Next sibling

			menuItem.IsChecked = True

			Dim themeName As String = CStr((menuItem).Tag)
			ChangeThemeName(Me, themeName)
		End Sub

		Private Sub ChangeThemeName(ByVal control As Control, ByVal themeName As String)
			Dim radControl As IComponentTreeHandler = TryCast(control, IComponentTreeHandler)
			If radControl IsNot Nothing Then
				radControl.ThemeName = themeName
			End If

			For Each child As Control In control.Controls
				ChangeThemeName(child, themeName)
			Next child
		End Sub

		Protected Overrides Overloads Sub OnLoad(ByVal e As EventArgs)
			MyBase.OnLoad(e)

			Me.employeesTableAdapter.Fill(Me.northwindDataSet.Employees)
			ThemeResolutionService.ApplyThemeToControlTree(Me, Telerik.QuickStart.WinControls.Utils.ThemeName)
		End Sub

		Private Sub radMenuItem17_Click(ByVal sender As Object, ByVal e As EventArgs)
			Dim aboutBox As New RadAboutBox1()

			aboutBox.ThemeName = Me.ThemeName
			aboutBox.ShowDialog(Me)
		End Sub
	End Class
End Namespace
