﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Telerik.Examples.WinControls.Forms.AboutBox;
using Telerik.QuickStart.WinControls;

namespace Telerik.Examples.WinControls.Forms.FormTitleBarStatusStrip
{
    /// <summary>
    /// example form
    /// </summary>
    public partial class Form1 : ExamplesRadForm
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            this.radGridView1.TableElement.RowHeight = 60;
            this.radGridView1.TableElement.Text = "";
        }

        protected override void WireEvents()
        {
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            this.radMenuItem26.Click += new System.EventHandler(this.OnRadMenuItemChangeTheme_Click);
            this.radMenuItem22.Click += new System.EventHandler(this.OnRadMenuItemChangeTheme_Click);
            this.radMenuItem17.Click += new System.EventHandler(this.radMenuItem17_Click);
            this.radMenuItem12.Click += new System.EventHandler(this.OnRadMenuItemChangeTheme_Click);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.radProgressBarElement1.Value1 < this.radProgressBarElement1.Maximum)
            {
                this.radProgressBarElement1.Value1++;
            }
            else
            {
                this.radProgressBarElement1.Value1 = this.radProgressBarElement1.Minimum;
            }
        }

        private void OnRadMenuItemChangeTheme_Click(object sender, EventArgs e)
        {
            RadMenuItem menuItem = (RadMenuItem) sender;

            foreach (RadMenuItem sibling in menuItem.HierarchyParent.Items)
            {
                sibling.IsChecked = false;
            }

            menuItem.IsChecked = true;

            string themeName = (string)(menuItem).Tag;
            ChangeThemeName(this, themeName);
        }

        private void ChangeThemeName(Control control, string themeName)
        {
            IComponentTreeHandler radControl = control as IComponentTreeHandler;
            if (radControl != null)
            {
                radControl.ThemeName = themeName;
            }

            foreach(Control child in control.Controls)
            {
                ChangeThemeName(child, themeName);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.employeesTableAdapter.Fill(this.northwindDataSet.Employees);
            ThemeResolutionService.ApplyThemeToControlTree(this, Telerik.QuickStart.WinControls.Utils.ThemeName);
        }

        private void radMenuItem17_Click(object sender, EventArgs e)
        {
            RadAboutBox1 aboutBox = new RadAboutBox1();

            aboutBox.ThemeName = this.ThemeName;
            aboutBox.ShowDialog(this);
        }
    }
}
