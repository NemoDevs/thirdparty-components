﻿namespace Telerik.Examples.WinControls.TreeView.ManipulateData.OptionsTree
{
    partial class Form1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadTreeNode radTreeNode1 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode2 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode3 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode4 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode5 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode6 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode7 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode8 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode9 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode10 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode11 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode12 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode13 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode14 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode15 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode16 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode17 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode18 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode19 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode20 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode21 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode22 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode23 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode24 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode25 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode26 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode27 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode28 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode29 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode30 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode31 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode32 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode33 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode34 = new Telerik.WinControls.UI.RadTreeNode();
            Telerik.WinControls.UI.RadTreeNode radTreeNode35 = new Telerik.WinControls.UI.RadTreeNode();
            this.headerPanel = new TreeExampleHeaderPanel();
            this.radTreeView1 = new Telerik.WinControls.UI.RadTreeView();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.rrbNone = new Telerik.WinControls.UI.RadRadioButton();
            this.rrbShowRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.rrbShowCheckBox = new Telerik.WinControls.UI.RadRadioButton();
            this.rcbTriState = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rrbNone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rrbShowRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rrbShowCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbTriState)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.settingsPanel.BackColor = System.Drawing.Color.Transparent;
            this.settingsPanel.Controls.Add(this.radGroupBox1);
            this.settingsPanel.Controls.Add(this.rcbTriState);
            this.settingsPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.settingsPanel.Location = new System.Drawing.Point(990, 1);
            this.settingsPanel.Size = new System.Drawing.Size(340, 752);
            this.settingsPanel.Controls.SetChildIndex(this.rcbTriState, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox1, 0);
            // 
            // headerPanel
            // 
            this.headerPanel.Location = new System.Drawing.Point(0, 0);
            this.headerPanel.Name = "headerPanel";
            this.headerPanel.Size = new System.Drawing.Size(200, 100);
            this.headerPanel.TabIndex = 1;
            // 
            // radTreeView1
            // 
            this.radTreeView1.BackColor = System.Drawing.SystemColors.Control;
            this.radTreeView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.radTreeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radTreeView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTreeView1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radTreeView1.Location = new System.Drawing.Point(0, 0);
            this.radTreeView1.Name = "radTreeView1";
            radTreeNode1.Name = "Node1";
            radTreeNode2.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode2.Name = "Node7";
            radTreeNode2.Text = "Lexus IS";
            radTreeNode3.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode3.Name = "Node8";
            radTreeNode3.Text = "BMW Touring";
            radTreeNode4.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode4.Name = "Node9";
            radTreeNode4.Text = "Mercedes-Benz CL";
            radTreeNode5.Checked = true;
            radTreeNode5.CheckState = Telerik.WinControls.Enumerations.ToggleState.On;
            radTreeNode5.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode5.Name = "Node10";
            radTreeNode5.Text = "Aston Martin DBS";
            radTreeNode1.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode2,
            radTreeNode3,
            radTreeNode4,
            radTreeNode5});
            radTreeNode1.Text = "Model";
            radTreeNode6.Name = "Node2";
            radTreeNode7.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode7.Name = "Node11";
            radTreeNode7.Text = "2.0L Petrol";
            radTreeNode8.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode8.Name = "Node12";
            radTreeNode8.Text = "3.5L Diesel";
            radTreeNode9.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode9.Name = "Node13";
            radTreeNode9.Text = "6.0L V12 ";
            radTreeNode10.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode10.Name = "Node14";
            radTreeNode10.Text = "3.0 TDI";
            radTreeNode6.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode7,
            radTreeNode8,
            radTreeNode9,
            radTreeNode10});
            radTreeNode6.Text = "Engine";
            radTreeNode11.Name = "Node3";
            radTreeNode12.CheckType = Telerik.WinControls.UI.CheckType.CheckBox;
            radTreeNode12.Name = "Node15";
            radTreeNode12.Text = "Alpine White";
            radTreeNode13.CheckType = Telerik.WinControls.UI.CheckType.CheckBox;
            radTreeNode13.Name = "Node16";
            radTreeNode13.Text = "Graphite Black";
            radTreeNode14.Checked = true;
            radTreeNode14.CheckState = Telerik.WinControls.Enumerations.ToggleState.On;
            radTreeNode14.CheckType = Telerik.WinControls.UI.CheckType.CheckBox;
            radTreeNode14.Name = "Node17";
            radTreeNode14.Text = "Sparkling Bronze";
            radTreeNode15.CheckType = Telerik.WinControls.UI.CheckType.CheckBox;
            radTreeNode15.Name = "Node18";
            radTreeNode15.Text = "Pearl Silver Metallic";
            radTreeNode11.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode12,
            radTreeNode13,
            radTreeNode14,
            radTreeNode15});
            radTreeNode11.Text = "Exterior";
            radTreeNode16.Name = "Node4";
            radTreeNode17.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode17.Name = "Node19";
            radTreeNode18.CheckType = Telerik.WinControls.UI.CheckType.CheckBox;
            radTreeNode18.Name = "Node21";
            radTreeNode18.Text = "Peluche";
            radTreeNode19.CheckType = Telerik.WinControls.UI.CheckType.CheckBox;
            radTreeNode19.Name = "Node22";
            radTreeNode19.Text = "Cadiffe";
            radTreeNode20.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode20.Name = "Node23";
            radTreeNode20.Text = "Poliester";
            radTreeNode21.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode21.Name = "Node27";
            radTreeNode21.Text = "Wool";
            radTreeNode17.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode18,
            radTreeNode19,
            radTreeNode20,
            radTreeNode21});
            radTreeNode17.Text = "Textile";
            radTreeNode22.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode22.Name = "Node20";
            radTreeNode23.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode23.Name = "Node24";
            radTreeNode23.Text = "Oyster Nevada Leather";
            radTreeNode24.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode24.Name = "Node25";
            radTreeNode24.Text = "Black Exclusive Nappa Leather";
            radTreeNode25.CheckType = Telerik.WinControls.UI.CheckType.CheckBox;
            radTreeNode25.Name = "Node26";
            radTreeNode25.Text = "Alcantara Leather";
            radTreeNode22.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode23,
            radTreeNode24,
            radTreeNode25});
            radTreeNode22.Text = "Leather";
            radTreeNode16.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode17,
            radTreeNode22});
            radTreeNode16.Text = "Interior";
            radTreeNode26.Name = "Node5";
            radTreeNode27.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode27.Name = "Node28";
            radTreeNode27.Text = "Media Package";
            radTreeNode28.CheckType = Telerik.WinControls.UI.CheckType.RadioButton;
            radTreeNode28.Name = "Node29";
            radTreeNode28.Text = "Dynamic Package";
            radTreeNode26.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode27,
            radTreeNode28});
            radTreeNode26.Text = "Packages";
            radTreeNode29.Name = "Node6";
            radTreeNode30.CheckType = Telerik.WinControls.UI.CheckType.CheckBox;
            radTreeNode30.Name = "Node30";
            radTreeNode30.Text = "DVD System";
            radTreeNode31.CheckType = Telerik.WinControls.UI.CheckType.CheckBox;
            radTreeNode31.Name = "Node31";
            radTreeNode31.Text = "Head-up Display";
            radTreeNode32.CheckType = Telerik.WinControls.UI.CheckType.CheckBox;
            radTreeNode32.Name = "Node32";
            radTreeNode32.Text = "Voice Control";
            radTreeNode33.CheckType = Telerik.WinControls.UI.CheckType.CheckBox;
            radTreeNode33.Name = "Node33";
            radTreeNode33.Text = "BOSE Sound System";
            radTreeNode34.CheckType = Telerik.WinControls.UI.CheckType.CheckBox;
            radTreeNode34.Name = "Node34";
            radTreeNode34.Text = "Navigation System";
            radTreeNode35.CheckType = Telerik.WinControls.UI.CheckType.CheckBox;
            radTreeNode35.Name = "Node35";
            radTreeNode35.Text = "Universal Remote Control";
            radTreeNode29.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode30,
            radTreeNode31,
            radTreeNode32,
            radTreeNode33,
            radTreeNode34,
            radTreeNode35});
            radTreeNode29.Text = "Optional Equipment";
            this.radTreeView1.Nodes.AddRange(new Telerik.WinControls.UI.RadTreeNode[] {
            radTreeNode1,
            radTreeNode6,
            radTreeNode11,
            radTreeNode16,
            radTreeNode26,
            radTreeNode29});
            this.radTreeView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radTreeView1.Size = new System.Drawing.Size(324, 754);
            this.radTreeView1.SpacingBetweenNodes = -1;
            this.radTreeView1.TabIndex = 2;
            this.radTreeView1.Text = "radTreeView1";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox1.Controls.Add(this.rrbNone);
            this.radGroupBox1.Controls.Add(this.rrbShowRadioButton);
            this.radGroupBox1.Controls.Add(this.rrbShowCheckBox);
            this.radGroupBox1.HeaderText = "Node Check Type";
            this.radGroupBox1.Location = new System.Drawing.Point(80, 77);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupBox1.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox1.Size = new System.Drawing.Size(180, 100);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = "Node Check Type";
            // 
            // rrbNone
            // 
            this.rrbNone.Location = new System.Drawing.Point(22, 76);
            this.rrbNone.Name = "rrbNone";
            this.rrbNone.Size = new System.Drawing.Size(48, 18);
            this.rrbNone.TabIndex = 2;
            this.rrbNone.Text = "None";
      
            // 
            // rrbShowRadioButton
            // 
            this.rrbShowRadioButton.Location = new System.Drawing.Point(22, 51);
            this.rrbShowRadioButton.Name = "rrbShowRadioButton";
            this.rrbShowRadioButton.Size = new System.Drawing.Size(86, 18);
            this.rrbShowRadioButton.TabIndex = 1;
            this.rrbShowRadioButton.Text = "Radio Button";
 
            // 
            // rrbShowCheckBox
            // 
            this.rrbShowCheckBox.Location = new System.Drawing.Point(22, 26);
            this.rrbShowCheckBox.Name = "rrbShowCheckBox";
            this.rrbShowCheckBox.Size = new System.Drawing.Size(72, 18);
            this.rrbShowCheckBox.TabIndex = 0;
            this.rrbShowCheckBox.Text = "Check Box";
 
            // 
            // rcbTriState
            // 
            this.rcbTriState.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rcbTriState.Location = new System.Drawing.Point(80, 48);
            this.rcbTriState.Name = "rcbTriState";
            this.rcbTriState.Size = new System.Drawing.Size(125, 18);
            this.rcbTriState.TabIndex = 2;
            this.rcbTriState.Text = "Tri-state check boxes";
         
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radTreeView1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1331, 754);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            this.Controls.SetChildIndex(this.radTreeView1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.headerPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTreeView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rrbNone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rrbShowRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rrbShowCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbTriState)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadTreeView radTreeView1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadRadioButton rrbNone;
        private Telerik.WinControls.UI.RadRadioButton rrbShowRadioButton;
        private Telerik.WinControls.UI.RadRadioButton rrbShowCheckBox;
        private Telerik.WinControls.UI.RadCheckBox rcbTriState;
        private TreeExampleHeaderPanel headerPanel;
    }
}
