﻿Namespace Telerik.Examples.WinControls.PageView.ViewModes
	Partial Public Class Form1
		''' <summary> 
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary> 
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Component Designer generated code"

		''' <summary> 
		''' Required method for Designer support - do not modify 
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
            Me.radLabel1 = New Telerik.WinControls.UI.RadLabel()
            Me.viewModeCombo = New Telerik.WinControls.UI.RadDropDownList()
            Me.radPageView1 = New Telerik.WinControls.UI.RadPageView()
            CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.settingsPanel.SuspendLayout()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.viewModeCombo, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radPageView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            ' 
            ' settingsPanel
            ' 
            Me.settingsPanel.Controls.Add(Me.viewModeCombo)
            Me.settingsPanel.Controls.Add(Me.radLabel1)
            Me.settingsPanel.Location = New Point(1002, 1)
            Me.settingsPanel.Size = New Size(200, 536)
            Me.settingsPanel.Controls.SetChildIndex(Me.radLabel1, 0)
            Me.settingsPanel.Controls.SetChildIndex(Me.viewModeCombo, 0)
            ' 
            ' radLabel1
            ' 
            Me.radLabel1.Anchor = AnchorStyles.Top
            Me.radLabel1.Location = New Point(10, 37)
            Me.radLabel1.Name = "radLabel1"
            Me.radLabel1.Size = New Size(65, 16)
            Me.radLabel1.TabIndex = 1
            Me.radLabel1.Text = "View Mode:"
            ' 
            ' viewModeCombo
            ' 
            Me.viewModeCombo.Anchor = AnchorStyles.Top
            Me.viewModeCombo.Location = New Point(10, 59)
            Me.viewModeCombo.Name = "viewModeCombo"
            ' 
            ' 
            ' 
            Me.viewModeCombo.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.viewModeCombo.Size = New Size(180, 20)
            Me.viewModeCombo.TabIndex = 2
            Me.viewModeCombo.TabStop = False
            Me.viewModeCombo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            ' 
            ' radPageView1
            ' 
            Me.radPageView1.Location = New Point(0, 0)
            Me.radPageView1.Name = "radPageView1"
            Me.radPageView1.Size = New Size(519, 419)
            Me.radPageView1.TabIndex = 1
            Me.radPageView1.Text = "radPageView1"
            ' 
            ' Form1
            ' 
            Me.AutoScaleDimensions = New SizeF(6.0F, 13.0F)
            Me.AutoScaleMode = AutoScaleMode.Font
            Me.AutoScrollMinSize = New System.Drawing.Size(519, 419)
            Me.Controls.Add(Me.radPageView1)
            Me.Name = "Form1"
            Me.Size = New Size(1203, 538)
            Me.Controls.SetChildIndex(Me.radPageView1, 0)
            Me.Controls.SetChildIndex(Me.settingsPanel, 0)
            CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).EndInit()
            Me.settingsPanel.ResumeLayout(False)
            Me.settingsPanel.PerformLayout()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.viewModeCombo, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radPageView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private radLabel1 As Telerik.WinControls.UI.RadLabel
        Private viewModeCombo As Telerik.WinControls.UI.RadDropDownList
        WithEvents radPageView1 As Telerik.WinControls.UI.RadPageView
	End Class
End Namespace
