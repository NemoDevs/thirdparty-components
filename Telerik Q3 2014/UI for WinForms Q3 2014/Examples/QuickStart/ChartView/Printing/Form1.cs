﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Telerik.QuickStart.WinControls;
using Telerik.WinControls.UI;

namespace Telerik.Examples.WinControls.ChartView.Printing
{
    public partial class Form1 : ExamplesForm
    {
        private RadChartView radChartView1;
        private RadPrintDocument radPrintDocument1;

        public Form1()
        {
            InitializeComponent();

            this.radChartView1 = new RadChartView();
            this.radChartView1.Dock = DockStyle.Fill;

            this.radPrintDocument1 = new RadPrintDocument();
            this.radPrintDocument1.Landscape = true;
            this.radPrintDocument1.AssociatedObject = this.radChartView1;

            this.Controls.Add(this.radChartView1);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.LoadData();

            this.radChartView1.ShowLegend = true;
            this.radChartView1.ChartElement.LegendElement.LegendTitle = "Expenses";
            this.radChartView1.ShowTitle = true;
            this.radChartView1.Title = "Personal monthly expenses";
        }

        private void LoadData()
        {
            LineSeries lineSeries;
            LineSeriesDataModel model = new LineSeriesDataModel();

            for (int i = 0; i < 11; i++)
            {
                lineSeries = new LineSeries();
                lineSeries.CategoryMember = "Month";
                lineSeries.ValueMember = "Expense";
                lineSeries.LegendTitle = model.GetLegendText(i);
                lineSeries.DataSource = model.GetData(i);
                lineSeries.PointSize = new SizeF(3f, 3f);

                this.radChartView1.Series.Add(lineSeries);
            }
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            this.radChartView1.Print(true, this.radPrintDocument1);
        }

        private void buttonPrintPreview_Click(object sender, EventArgs e)
        {
            this.radChartView1.PrintPreview(this.radPrintDocument1);
        }

        private void buttonPrintSettings_Click(object sender, EventArgs e)
        {
            Form dialog = this.radChartView1.GetSettingsDialog(this.radPrintDocument1);

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                this.radChartView1.PrintPreview(this.radPrintDocument1);
            }
        }

        protected override void WireEvents()
        {
            this.buttonPrint.Click += this.buttonPrint_Click;
            this.buttonPrintPreview.Click += this.buttonPrintPreview_Click;
            this.buttonPrintSettings.Click += this.buttonPrintSettings_Click;
        }
    }
}
