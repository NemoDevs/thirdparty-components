﻿namespace Telerik.Examples.WinControls.ChartView.TitleAndLegend
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radCheckBoxFlipText = new Telerik.WinControls.UI.RadCheckBox();
            this.radTextBoxControlTitle = new Telerik.WinControls.UI.RadTextBoxControl();
            this.radLabelTitle = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownListTitlePosition = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabelTitleOrientation = new Telerik.WinControls.UI.RadLabel();
            this.radRadioButtonHorizontalTitle = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButtonVerticalTitle = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radButtonEditShape = new Telerik.WinControls.UI.RadButton();
            this.radDropDownListShapes = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabelMarkerShape = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownListLegendPosition = new Telerik.WinControls.UI.RadDropDownList();
            this.radSpinEditorLegendX = new Telerik.WinControls.UI.RadSpinEditor();
            this.radRadioButtonVerticalLegend = new Telerik.WinControls.UI.RadRadioButton();
            this.radSpinEditorLegendY = new Telerik.WinControls.UI.RadSpinEditor();
            this.radRadioButtonHorizontalLegend = new Telerik.WinControls.UI.RadRadioButton();
            this.radChartView1 = new Telerik.WinControls.UI.RadChartView();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxFlipText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControlTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListTitlePosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelTitleOrientation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonHorizontalTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonVerticalTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonEditShape)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListShapes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelMarkerShape)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListLegendPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorLegendX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonVerticalLegend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorLegendY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonHorizontalLegend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupBox2);
            this.settingsPanel.Controls.Add(this.radGroupBox1);
            this.settingsPanel.Location = new System.Drawing.Point(835, 45);
            this.settingsPanel.Size = new System.Drawing.Size(244, 682);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox2, 0);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox1.Controls.Add(this.radCheckBoxFlipText);
            this.radGroupBox1.Controls.Add(this.radTextBoxControlTitle);
            this.radGroupBox1.Controls.Add(this.radLabelTitle);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.radDropDownListTitlePosition);
            this.radGroupBox1.Controls.Add(this.radLabelTitleOrientation);
            this.radGroupBox1.Controls.Add(this.radRadioButtonHorizontalTitle);
            this.radGroupBox1.Controls.Add(this.radRadioButtonVerticalTitle);
            this.radGroupBox1.HeaderText = "Title";
            this.radGroupBox1.Location = new System.Drawing.Point(10, 3);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(224, 194);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = "Title";
            // 
            // radCheckBoxFlipText
            // 
            this.radCheckBoxFlipText.Location = new System.Drawing.Point(115, 144);
            this.radCheckBoxFlipText.Name = "radCheckBoxFlipText";
            this.radCheckBoxFlipText.Size = new System.Drawing.Size(60, 18);
            this.radCheckBoxFlipText.TabIndex = 13;
            this.radCheckBoxFlipText.Tag = "NotTouch";
            this.radCheckBoxFlipText.Text = "Flip text";
            // 
            // radTextBoxControlTitle
            // 
            this.radTextBoxControlTitle.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radTextBoxControlTitle.Location = new System.Drawing.Point(5, 44);
            this.radTextBoxControlTitle.Name = "radTextBoxControlTitle";
            this.radTextBoxControlTitle.NullText = "Chart title";
            this.radTextBoxControlTitle.Size = new System.Drawing.Size(214, 20);
            this.radTextBoxControlTitle.TabIndex = 12;
            // 
            // radLabelTitle
            // 
            this.radLabelTitle.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabelTitle.Location = new System.Drawing.Point(5, 20);
            this.radLabelTitle.Name = "radLabelTitle";
            this.radLabelTitle.Size = new System.Drawing.Size(26, 18);
            this.radLabelTitle.TabIndex = 8;
            this.radLabelTitle.Text = "Title";
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel1.Location = new System.Drawing.Point(5, 70);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(45, 18);
            this.radLabel1.TabIndex = 8;
            this.radLabel1.Text = "Position";
            // 
            // radDropDownListTitlePosition
            // 
            this.radDropDownListTitlePosition.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radDropDownListTitlePosition.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownListTitlePosition.Location = new System.Drawing.Point(5, 94);
            this.radDropDownListTitlePosition.Name = "radDropDownListTitlePosition";
            this.radDropDownListTitlePosition.Size = new System.Drawing.Size(214, 20);
            this.radDropDownListTitlePosition.TabIndex = 7;
            this.radDropDownListTitlePosition.Text = "Title position";
            // 
            // radLabelTitleOrientation
            // 
            this.radLabelTitleOrientation.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabelTitleOrientation.Location = new System.Drawing.Point(5, 120);
            this.radLabelTitleOrientation.Name = "radLabelTitleOrientation";
            this.radLabelTitleOrientation.Size = new System.Drawing.Size(84, 18);
            this.radLabelTitleOrientation.TabIndex = 9;
            this.radLabelTitleOrientation.Text = "Title orientation";
            // 
            // radRadioButtonHorizontalTitle
            // 
            this.radRadioButtonHorizontalTitle.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radRadioButtonHorizontalTitle.Location = new System.Drawing.Point(5, 144);
            this.radRadioButtonHorizontalTitle.Name = "radRadioButtonHorizontalTitle";
            this.radRadioButtonHorizontalTitle.Size = new System.Drawing.Size(72, 18);
            this.radRadioButtonHorizontalTitle.TabIndex = 10;
            this.radRadioButtonHorizontalTitle.TabStop = true;
            this.radRadioButtonHorizontalTitle.Text = "Horizontal";
            this.radRadioButtonHorizontalTitle.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radRadioButtonVerticalTitle
            // 
            this.radRadioButtonVerticalTitle.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radRadioButtonVerticalTitle.Location = new System.Drawing.Point(5, 168);
            this.radRadioButtonVerticalTitle.Name = "radRadioButtonVerticalTitle";
            this.radRadioButtonVerticalTitle.Size = new System.Drawing.Size(57, 18);
            this.radRadioButtonVerticalTitle.TabIndex = 11;
            this.radRadioButtonVerticalTitle.Text = "Vertical";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox2.Controls.Add(this.radButtonEditShape);
            this.radGroupBox2.Controls.Add(this.radDropDownListShapes);
            this.radGroupBox2.Controls.Add(this.radLabelMarkerShape);
            this.radGroupBox2.Controls.Add(this.radLabel4);
            this.radGroupBox2.Controls.Add(this.radLabel3);
            this.radGroupBox2.Controls.Add(this.radLabel5);
            this.radGroupBox2.Controls.Add(this.radLabel2);
            this.radGroupBox2.Controls.Add(this.radDropDownListLegendPosition);
            this.radGroupBox2.Controls.Add(this.radSpinEditorLegendX);
            this.radGroupBox2.Controls.Add(this.radRadioButtonVerticalLegend);
            this.radGroupBox2.Controls.Add(this.radSpinEditorLegendY);
            this.radGroupBox2.Controls.Add(this.radRadioButtonHorizontalLegend);
            this.radGroupBox2.HeaderText = "Legend";
            this.radGroupBox2.Location = new System.Drawing.Point(10, 203);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(224, 265);
            this.radGroupBox2.TabIndex = 8;
            this.radGroupBox2.Text = "Legend";
            // 
            // radButtonEditShape
            // 
            this.radButtonEditShape.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radButtonEditShape.Location = new System.Drawing.Point(5, 229);
            this.radButtonEditShape.Name = "radButtonEditShape";
            this.radButtonEditShape.Size = new System.Drawing.Size(214, 24);
            this.radButtonEditShape.TabIndex = 10;
            this.radButtonEditShape.Text = "Edit shape";
            // 
            // radDropDownListShapes
            // 
            this.radDropDownListShapes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radDropDownListShapes.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownListShapes.Location = new System.Drawing.Point(5, 203);
            this.radDropDownListShapes.Name = "radDropDownListShapes";
            this.radDropDownListShapes.Size = new System.Drawing.Size(214, 20);
            this.radDropDownListShapes.TabIndex = 9;
            // 
            // radLabelMarkerShape
            // 
            this.radLabelMarkerShape.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabelMarkerShape.Location = new System.Drawing.Point(5, 179);
            this.radLabelMarkerShape.Name = "radLabelMarkerShape";
            this.radLabelMarkerShape.Size = new System.Drawing.Size(78, 18);
            this.radLabelMarkerShape.TabIndex = 8;
            this.radLabelMarkerShape.Text = "Markers shape";
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel4.Location = new System.Drawing.Point(117, 73);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(11, 18);
            this.radLabel4.TabIndex = 7;
            this.radLabel4.Tag = "NotTouch";
            this.radLabel4.Text = "Y";
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel3.Location = new System.Drawing.Point(32, 73);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(12, 18);
            this.radLabel3.TabIndex = 7;
            this.radLabel3.Tag = "NotTouch";
            this.radLabel3.Text = "X";
            // 
            // radLabel5
            // 
            this.radLabel5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel5.Location = new System.Drawing.Point(5, 107);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(91, 18);
            this.radLabel5.TabIndex = 1;
            this.radLabel5.Text = "Items orientation";
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLabel2.Location = new System.Drawing.Point(5, 21);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(45, 18);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "Position";
            // 
            // radDropDownListLegendPosition
            // 
            this.radDropDownListLegendPosition.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radDropDownListLegendPosition.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownListLegendPosition.Location = new System.Drawing.Point(5, 45);
            this.radDropDownListLegendPosition.Name = "radDropDownListLegendPosition";
            this.radDropDownListLegendPosition.Size = new System.Drawing.Size(214, 20);
            this.radDropDownListLegendPosition.TabIndex = 1;
            this.radDropDownListLegendPosition.Text = "Legend position";
            // 
            // radSpinEditorLegendX
            // 
            this.radSpinEditorLegendX.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditorLegendX.Location = new System.Drawing.Point(51, 71);
            this.radSpinEditorLegendX.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.radSpinEditorLegendX.Name = "radSpinEditorLegendX";
            this.radSpinEditorLegendX.Size = new System.Drawing.Size(57, 20);
            this.radSpinEditorLegendX.TabIndex = 2;
            this.radSpinEditorLegendX.TabStop = false;
            this.radSpinEditorLegendX.Tag = "NotTouch";
            // 
            // radRadioButtonVerticalLegend
            // 
            this.radRadioButtonVerticalLegend.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radRadioButtonVerticalLegend.Location = new System.Drawing.Point(5, 155);
            this.radRadioButtonVerticalLegend.Name = "radRadioButtonVerticalLegend";
            this.radRadioButtonVerticalLegend.Size = new System.Drawing.Size(57, 18);
            this.radRadioButtonVerticalLegend.TabIndex = 6;
            this.radRadioButtonVerticalLegend.TabStop = true;
            this.radRadioButtonVerticalLegend.Text = "Vertical";
            this.radRadioButtonVerticalLegend.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radSpinEditorLegendY
            // 
            this.radSpinEditorLegendY.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radSpinEditorLegendY.Location = new System.Drawing.Point(135, 71);
            this.radSpinEditorLegendY.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.radSpinEditorLegendY.Name = "radSpinEditorLegendY";
            this.radSpinEditorLegendY.Size = new System.Drawing.Size(57, 20);
            this.radSpinEditorLegendY.TabIndex = 2;
            this.radSpinEditorLegendY.TabStop = false;
            this.radSpinEditorLegendY.Tag = "NotTouch";
            // 
            // radRadioButtonHorizontalLegend
            // 
            this.radRadioButtonHorizontalLegend.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radRadioButtonHorizontalLegend.Location = new System.Drawing.Point(5, 131);
            this.radRadioButtonHorizontalLegend.Name = "radRadioButtonHorizontalLegend";
            this.radRadioButtonHorizontalLegend.Size = new System.Drawing.Size(72, 18);
            this.radRadioButtonHorizontalLegend.TabIndex = 6;
            this.radRadioButtonHorizontalLegend.Text = "Horizontal";
            // 
            // radChartView1
            // 
            this.radChartView1.AreaDesign = cartesianArea1;
            this.radChartView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radChartView1.Location = new System.Drawing.Point(0, 0);
            this.radChartView1.MinimumSize = new System.Drawing.Size(460, 350);
            this.radChartView1.Name = "radChartView1";
            // 
            // 
            // 
            this.radChartView1.RootElement.MinSize = new System.Drawing.Size(460, 350);
            this.radChartView1.ShowGrid = false;
            this.radChartView1.Size = new System.Drawing.Size(1158, 695);
            this.radChartView1.TabIndex = 1;
            this.radChartView1.Text = "radChartView1";
            this.radChartView1.Title = "Chart title";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScrollMinSize = new System.Drawing.Size(460, 350);
            this.Controls.Add(this.radChartView1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1168, 705);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radChartView1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBoxFlipText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControlTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListTitlePosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelTitleOrientation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonHorizontalTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonVerticalTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonEditShape)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListShapes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabelMarkerShape)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownListLegendPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorLegendX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonVerticalLegend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditorLegendY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButtonHorizontalLegend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList radDropDownListLegendPosition;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorLegendX;
        private Telerik.WinControls.UI.RadRadioButton radRadioButtonVerticalLegend;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditorLegendY;
        private Telerik.WinControls.UI.RadRadioButton radRadioButtonHorizontalLegend;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDropDownList radDropDownListTitlePosition;
        private Telerik.WinControls.UI.RadLabel radLabelTitleOrientation;
        private Telerik.WinControls.UI.RadRadioButton radRadioButtonHorizontalTitle;
        private Telerik.WinControls.UI.RadRadioButton radRadioButtonVerticalTitle;
        private Telerik.WinControls.UI.RadChartView radChartView1;
        private Telerik.WinControls.UI.RadDropDownList radDropDownListShapes;
        private Telerik.WinControls.UI.RadLabel radLabelMarkerShape;
        private Telerik.WinControls.UI.RadCheckBox radCheckBoxFlipText;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControlTitle;
        private Telerik.WinControls.UI.RadLabel radLabelTitle;
        private Telerik.WinControls.UI.RadButton radButtonEditShape;
    }
}