﻿namespace Telerik.Examples.WinControls.MultiColumnComboBox
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            this.radMultiColumnComboBox1 = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radGroupSettings = new Telerik.WinControls.UI.RadGroupBox();
            this.radLblAutoComplete = new Telerik.WinControls.UI.RadLabel();
            this.radComboAutoCompl = new Telerik.WinControls.UI.RadDropDownList();
            this.radCheckRotate = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckRTL = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).BeginInit();
            this.radPanelDemoHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox1.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox1.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupSettings)).BeginInit();
            this.radGroupSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLblAutoComplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboAutoCompl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckRotate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckRTL)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanelDemoHolder
            // 
            this.radPanelDemoHolder.Controls.Add(this.radMultiColumnComboBox1);
            this.radPanelDemoHolder.ForeColor = System.Drawing.Color.Black;
            this.radPanelDemoHolder.MaximumSize = new System.Drawing.Size(362, 100);
            this.radPanelDemoHolder.MinimumSize = new System.Drawing.Size(362, 100);
            // 
            // 
            // 
            this.radPanelDemoHolder.RootElement.MaxSize = new System.Drawing.Size(362, 100);
            this.radPanelDemoHolder.RootElement.MinSize = new System.Drawing.Size(362, 100);
            this.radPanelDemoHolder.Size = new System.Drawing.Size(362, 100);
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupSettings);
            this.settingsPanel.Location = new System.Drawing.Point(779, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 784);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupSettings, 0);
            // 
            // radMultiColumnComboBox1
            // 
            // 
            // radMultiColumnComboBox1.NestedRadGridView
            // 
            this.radMultiColumnComboBox1.EditorControl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radMultiColumnComboBox1.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radMultiColumnComboBox1.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radMultiColumnComboBox1.EditorControl.Location = new System.Drawing.Point(4, 1);
            // 
            // 
            // 
            this.radMultiColumnComboBox1.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.radMultiColumnComboBox1.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.radMultiColumnComboBox1.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.radMultiColumnComboBox1.EditorControl.MasterTemplate.EnableGrouping = false;
            this.radMultiColumnComboBox1.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.radMultiColumnComboBox1.EditorControl.Name = "NestedRadGridView";
            this.radMultiColumnComboBox1.EditorControl.ReadOnly = true;
            this.radMultiColumnComboBox1.EditorControl.ShowGroupPanel = false;
            this.radMultiColumnComboBox1.EditorControl.Size = new System.Drawing.Size(282, 104);
            this.radMultiColumnComboBox1.EditorControl.TabIndex = 0;
            this.radMultiColumnComboBox1.Location = new System.Drawing.Point(0, 0);
            this.radMultiColumnComboBox1.Name = "radMultiColumnComboBox1";
            // 
            // 
            // 
            this.radMultiColumnComboBox1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radMultiColumnComboBox1.Size = new System.Drawing.Size(306, 21);
            this.radMultiColumnComboBox1.TabIndex = 0;
            this.radMultiColumnComboBox1.TabStop = false;
            // 
            // radGroupSettings
            // 
            this.radGroupSettings.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupSettings.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupSettings.Controls.Add(this.radLblAutoComplete);
            this.radGroupSettings.Controls.Add(this.radComboAutoCompl);
            this.radGroupSettings.Controls.Add(this.radCheckRotate);
            this.radGroupSettings.Controls.Add(this.radCheckRTL);
            this.radGroupSettings.FooterText = "";
            this.radGroupSettings.ForeColor = System.Drawing.Color.Black;
            this.radGroupSettings.HeaderText = "Settings";
            this.radGroupSettings.Location = new System.Drawing.Point(10, 6);
            this.radGroupSettings.Name = "radGroupSettings";
            this.radGroupSettings.Size = new System.Drawing.Size(180, 125);
            this.radGroupSettings.TabIndex = 0;
            this.radGroupSettings.Text = "Settings";
            // 
            // radLblAutoComplete
            // 
            this.radLblAutoComplete.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radLblAutoComplete.Location = new System.Drawing.Point(5, 21);
            this.radLblAutoComplete.Name = "radLblAutoComplete";
            this.radLblAutoComplete.Size = new System.Drawing.Size(113, 18);
            this.radLblAutoComplete.TabIndex = 2;
            this.radLblAutoComplete.Text = "AutoComplete Mode:";
            // 
            // radComboAutoCompl
            // 
            this.radComboAutoCompl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radComboAutoCompl.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radComboAutoCompl.FormatString = "{0}";
            radListDataItem1.Text = "None";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "Suggest";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "Append";
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "SuggestAppend";
            radListDataItem4.TextWrap = true;
            this.radComboAutoCompl.Items.Add(radListDataItem1);
            this.radComboAutoCompl.Items.Add(radListDataItem2);
            this.radComboAutoCompl.Items.Add(radListDataItem3);
            this.radComboAutoCompl.Items.Add(radListDataItem4);
            this.radComboAutoCompl.Location = new System.Drawing.Point(5, 44);
            this.radComboAutoCompl.Name = "radComboAutoCompl";
            // 
            // 
            // 
            this.radComboAutoCompl.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radComboAutoCompl.Size = new System.Drawing.Size(170, 20);
            this.radComboAutoCompl.TabIndex = 1;
            this.radComboAutoCompl.Text = "Select mode:";
            // 
            // radCheckRotate
            // 
            this.radCheckRotate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckRotate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.radCheckRotate.Location = new System.Drawing.Point(5, 78);
            this.radCheckRotate.Name = "radCheckRotate";
            this.radCheckRotate.Size = new System.Drawing.Size(138, 18);
            this.radCheckRotate.TabIndex = 0;
            this.radCheckRotate.Text = "Rotate On Double-Click";
            // 
            // radCheckRTL
            // 
            this.radCheckRTL.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radCheckRTL.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.radCheckRTL.Location = new System.Drawing.Point(5, 101);
            this.radCheckRTL.Name = "radCheckRTL";
            this.radCheckRTL.Size = new System.Drawing.Size(84, 18);
            this.radCheckRTL.TabIndex = 0;
            this.radCheckRTL.Text = "Right To Left";
            // 
            // Form1
            // 
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1495, 898);
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).EndInit();
            this.radPanelDemoHolder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox1.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox1.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMultiColumnComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupSettings)).EndInit();
            this.radGroupSettings.ResumeLayout(false);
            this.radGroupSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLblAutoComplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radComboAutoCompl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckRotate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckRTL)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

        private Telerik.WinControls.UI.RadMultiColumnComboBox radMultiColumnComboBox1;
        private Telerik.WinControls.UI.RadGroupBox radGroupSettings;
        private Telerik.WinControls.UI.RadCheckBox radCheckRotate;
        private Telerik.WinControls.UI.RadCheckBox radCheckRTL;
        private Telerik.WinControls.UI.RadDropDownList radComboAutoCompl;
        private Telerik.WinControls.UI.RadLabel radLblAutoComplete;
	}
}