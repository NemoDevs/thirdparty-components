Namespace Telerik.Examples.WinControls.MenuStrip.MenuOrientation
	Partial Public Class Form1
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.components = New System.ComponentModel.Container()
			Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(Form1))
			Dim themeSource1 As New Telerik.WinControls.ThemeSource()
			Me.imageList1 = New ImageList(Me.components)
			Me.radMenuItemSrc_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSrc_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSrc_3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_4 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_5 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_6 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_7 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_8 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_9 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_10 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_11 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_12 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radButton2 = New Telerik.WinControls.UI.RadButton()
			Me.radButton1 = New Telerik.WinControls.UI.RadButton()
			Me.radThemeManager1 = New Telerik.WinControls.RadThemeManager()
			Me.radPanelDemoHolder = New Telerik.WinControls.UI.RadPanel()
			Me.radMenu1 = New Telerik.WinControls.UI.RadMenu()
			Me.radMenuItem1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem5 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem1 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem7 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem2 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem8 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem9 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem3 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem10 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem11 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem17 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem18 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem19 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem12 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem4 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem13 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem20 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem21 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem22 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem5 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem14 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem15 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem6 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem16 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem23 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem7 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem24 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem25 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem26 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem8 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem27 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem28 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem29 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem30 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem31 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem9 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem32 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem10 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem33 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem34 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem35 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem36 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem37 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem38 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem39 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem11 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem40 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem41 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem42 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem43 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem4 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem44 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem45 = New Telerik.WinControls.UI.RadMenuItem()
			CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radButton2, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radButton1, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radPanelDemoHolder, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.radPanelDemoHolder.SuspendLayout()
			CType(Me.radMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.SuspendLayout()
			' 
			' settingsPanel
			' 
			Me.settingsPanel.Location = New Point(623, 1)
			Me.settingsPanel.Size = New Size(200, 705)
			Me.settingsPanel.ThemeName = "ControlDefault"
			' 
			' imageList1
			' 
			Me.imageList1.ImageStream = (CType(resources.GetObject("imageList1.ImageStream"), ImageListStreamer))
			Me.imageList1.TransparentColor = Color.Magenta
			Me.imageList1.Images.SetKeyName(0, "")
			Me.imageList1.Images.SetKeyName(1, "")
			Me.imageList1.Images.SetKeyName(2, "")
			Me.imageList1.Images.SetKeyName(3, "")
			Me.imageList1.Images.SetKeyName(4, "")
			Me.imageList1.Images.SetKeyName(5, "CloseSolution.bmp")
			' 
			' radMenuItemSrc_1
			' 
			Me.radMenuItemSrc_1.AccessibleDescription = "View History"
			Me.radMenuItemSrc_1.AccessibleName = "View History"
			Me.radMenuItemSrc_1.Name = "radMenuItemSrc_1"
			Me.radMenuItemSrc_1.Text = "View History"
			Me.radMenuItemSrc_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItemSrc_2
			' 
			Me.radMenuItemSrc_2.AccessibleDescription = "Refresh Status"
			Me.radMenuItemSrc_2.AccessibleName = "Refresh Status"
			Me.radMenuItemSrc_2.Name = "radMenuItemSrc_2"
			Me.radMenuItemSrc_2.Text = "Refresh Status"
			Me.radMenuItemSrc_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItemSrc_3
			' 
			Me.radMenuItemSrc_3.AccessibleDescription = "Compare..."
			Me.radMenuItemSrc_3.AccessibleName = "Compare..."
			Me.radMenuItemSrc_3.Name = "radMenuItemSrc_3"
			Me.radMenuItemSrc_3.Text = "Compare..."
			Me.radMenuItemSrc_3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_1
			' 
			Me.radMenuItem6_1.AccessibleDescription = "How Do I"
			Me.radMenuItem6_1.AccessibleName = "How Do I"
			Me.radMenuItem6_1.Name = "radMenuItem6_1"
			Me.radMenuItem6_1.Text = "How Do I"
			Me.radMenuItem6_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_2
			' 
			Me.radMenuItem6_2.AccessibleDescription = "Search"
			Me.radMenuItem6_2.AccessibleName = "Search"
			Me.radMenuItem6_2.Name = "radMenuItem6_2"
			Me.radMenuItem6_2.Text = "Search"
			Me.radMenuItem6_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_3
			' 
			Me.radMenuItem6_3.AccessibleDescription = "Contents"
			Me.radMenuItem6_3.AccessibleName = "Contents"
			Me.radMenuItem6_3.Name = "radMenuItem6_3"
			Me.radMenuItem6_3.Text = "Contents"
			Me.radMenuItem6_3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_4
			' 
			Me.radMenuItem6_4.AccessibleDescription = "Index"
			Me.radMenuItem6_4.AccessibleName = "Index"
			Me.radMenuItem6_4.Name = "radMenuItem6_4"
			Me.radMenuItem6_4.Text = "Index"
			Me.radMenuItem6_4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_5
			' 
			Me.radMenuItem6_5.AccessibleDescription = "Help Favorites"
			Me.radMenuItem6_5.AccessibleName = "Help Favorites"
			Me.radMenuItem6_5.Name = "radMenuItem6_5"
			Me.radMenuItem6_5.Text = "Help Favorites"
			Me.radMenuItem6_5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_6
			' 
			Me.radMenuItem6_6.AccessibleDescription = "Dynamic Help"
			Me.radMenuItem6_6.AccessibleName = "Dynamic Help"
			Me.radMenuItem6_6.Name = "radMenuItem6_6"
			Me.radMenuItem6_6.Text = "Dynamic Help"
			Me.radMenuItem6_6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_7
			' 
			Me.radMenuItem6_7.AccessibleDescription = "Index Results"
			Me.radMenuItem6_7.AccessibleName = "Index Results"
			Me.radMenuItem6_7.Name = "radMenuItem6_7"
			Me.radMenuItem6_7.Text = "Index Results"
			Me.radMenuItem6_7.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_8
			' 
			Me.radMenuItem6_8.AccessibleDescription = "Customer Feedback Options..."
			Me.radMenuItem6_8.AccessibleName = "Customer Feedback Options..."
			Me.radMenuItem6_8.Name = "radMenuItem6_8"
			Me.radMenuItem6_8.Text = "Customer Feedback Options..."
			Me.radMenuItem6_8.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_9
			' 
			Me.radMenuItem6_9.AccessibleDescription = "Register Product..."
			Me.radMenuItem6_9.AccessibleName = "Register Product..."
			Me.radMenuItem6_9.Name = "radMenuItem6_9"
			Me.radMenuItem6_9.Text = "Register Product..."
			Me.radMenuItem6_9.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_10
			' 
			Me.radMenuItem6_10.AccessibleDescription = "Check for Update"
			Me.radMenuItem6_10.AccessibleName = "Check for Update"
			Me.radMenuItem6_10.Name = "radMenuItem6_10"
			Me.radMenuItem6_10.Text = "Check for Update"
			Me.radMenuItem6_10.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_11
			' 
			Me.radMenuItem6_11.AccessibleDescription = "Technical Support"
			Me.radMenuItem6_11.AccessibleName = "Technical Support"
			Me.radMenuItem6_11.Name = "radMenuItem6_11"
			Me.radMenuItem6_11.Text = "Technical Support"
			Me.radMenuItem6_11.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_12
			' 
			Me.radMenuItem6_12.AccessibleDescription = "About Telerik WinControls Suite..."
			Me.radMenuItem6_12.AccessibleName = "About Telerik WinControls Suite..."
			Me.radMenuItem6_12.Name = "radMenuItem6_12"
			Me.radMenuItem6_12.Text = "About Telerik WinControls Suite..."
			Me.radMenuItem6_12.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radButton2
			' 
			Me.radButton2.Image = My.Resources.button2
			Me.radButton2.ImageAlignment = ContentAlignment.TopCenter
			Me.radButton2.Location = New Point(425, 155)
			Me.radButton2.Name = "radButton2"
			Me.radButton2.Size = New Size(133, 89)
			Me.radButton2.TabIndex = 4
			Me.radButton2.Text = "Switch Text Orientation"
			Me.radButton2.TextImageRelation = TextImageRelation.ImageAboveText

			' 
			' radButton1
			' 
			Me.radButton1.Image = My.Resources.button1
			Me.radButton1.ImageAlignment = ContentAlignment.TopCenter
			Me.radButton1.Location = New Point(173, 155)
			Me.radButton1.Name = "radButton1"
			Me.radButton1.Size = New Size(138, 89)
			Me.radButton1.TabIndex = 3
			Me.radButton1.Text = "Switch Menu Orientation"
			Me.radButton1.TextImageRelation = TextImageRelation.ImageAboveText

			' 
			' radThemeManager1
			' 
			themeSource1.StorageType = Telerik.WinControls.ThemeStorageType.Resource
			themeSource1.ThemeLocation = "Telerik.Examples.WinControls.Resources.MenuButtonTheme.xml"
			Me.radThemeManager1.LoadedThemes.AddRange(New Telerik.WinControls.ThemeSource() { themeSource1})
			' 
			' radPanelDemoHolder
			' 
			Me.radPanelDemoHolder.BackgroundImage = My.Resources.background
			Me.radPanelDemoHolder.BackgroundImageLayout = ImageLayout.Zoom
			Me.radPanelDemoHolder.Controls.Add(Me.radMenu1)
			Me.radPanelDemoHolder.Controls.Add(Me.radButton2)
			Me.radPanelDemoHolder.Controls.Add(Me.radButton1)
			Me.radPanelDemoHolder.ForeColor = Color.Black
			Me.radPanelDemoHolder.Location = New Point(0, 0)
			Me.radPanelDemoHolder.Name = "radPanelDemoHolder"
			Me.radPanelDemoHolder.Size = New Size(618, 354)
			Me.radPanelDemoHolder.TabIndex = 15
			CType(Me.radPanelDemoHolder.GetChildAt(0).GetChildAt(0), Telerik.WinControls.Primitives.FillPrimitive).Visibility = Telerik.WinControls.ElementVisibility.Hidden
			CType(Me.radPanelDemoHolder.GetChildAt(0).GetChildAt(1), Telerik.WinControls.Primitives.BorderPrimitive).Visibility = Telerik.WinControls.ElementVisibility.Hidden
			' 
			' radMenu1
			' 
			Me.radMenu1.Dock = DockStyle.None
			Me.radMenu1.ForeColor = Color.Black
			Me.radMenu1.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem1, Me.radMenuItem2, Me.radMenuItem3, Me.radMenuItem4})
			Me.radMenu1.Location = New Point(33, 30)
			Me.radMenu1.Name = "radMenu1"
			Me.radMenu1.Size = New Size(525, 20)
			Me.radMenu1.TabIndex = 5
			Me.radMenu1.Text = "radMenu1"
			' 
			' radMenuItem1
			' 
			Me.radMenuItem1.AccessibleDescription = "File"
			Me.radMenuItem1.AccessibleName = "File"
			Me.radMenuItem1.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem5, Me.radMenuItem6, Me.radMenuSeparatorItem1, Me.radMenuItem7, Me.radMenuSeparatorItem2, Me.radMenuItem8, Me.radMenuItem9, Me.radMenuSeparatorItem3, Me.radMenuItem10, Me.radMenuItem11, Me.radMenuItem12, Me.radMenuSeparatorItem4, Me.radMenuItem13, Me.radMenuSeparatorItem5, Me.radMenuItem14, Me.radMenuItem15, Me.radMenuSeparatorItem6, Me.radMenuItem16, Me.radMenuItem23, Me.radMenuSeparatorItem7, Me.radMenuItem24})
			Me.radMenuItem1.Name = "radMenuItem1"
			Me.radMenuItem1.Text = "File"
			Me.radMenuItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem5
			' 
			Me.radMenuItem5.AccessibleDescription = "New"
			Me.radMenuItem5.AccessibleName = "New"
			Me.radMenuItem5.Name = "radMenuItem5"
			Me.radMenuItem5.Text = "New"
			Me.radMenuItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6
			' 
			Me.radMenuItem6.AccessibleDescription = "Open"
			Me.radMenuItem6.AccessibleName = "Open"
			Me.radMenuItem6.Name = "radMenuItem6"
			Me.radMenuItem6.Text = "Open"
			Me.radMenuItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem1
			' 
			Me.radMenuSeparatorItem1.AccessibleDescription = "radMenuSeparatorItem1"
			Me.radMenuSeparatorItem1.AccessibleName = "radMenuSeparatorItem1"
			Me.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1"
			Me.radMenuSeparatorItem1.Text = "radMenuSeparatorItem1"
			Me.radMenuSeparatorItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem7
			' 
			Me.radMenuItem7.AccessibleDescription = "Add"
			Me.radMenuItem7.AccessibleName = "Add"
			Me.radMenuItem7.Name = "radMenuItem7"
			Me.radMenuItem7.Text = "Add"
			Me.radMenuItem7.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem2
			' 
			Me.radMenuSeparatorItem2.AccessibleDescription = "radMenuSeparatorItem2"
			Me.radMenuSeparatorItem2.AccessibleName = "radMenuSeparatorItem2"
			Me.radMenuSeparatorItem2.Name = "radMenuSeparatorItem2"
			Me.radMenuSeparatorItem2.Text = "radMenuSeparatorItem2"
			Me.radMenuSeparatorItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem8
			' 
			Me.radMenuItem8.AccessibleDescription = "Close"
			Me.radMenuItem8.AccessibleName = "Close"
			Me.radMenuItem8.Name = "radMenuItem8"
			Me.radMenuItem8.Text = "Close"
			Me.radMenuItem8.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem9
			' 
			Me.radMenuItem9.AccessibleDescription = "Close Solution"
			Me.radMenuItem9.AccessibleName = "Close Solution"
			Me.radMenuItem9.Name = "radMenuItem9"
			Me.radMenuItem9.Text = "Close Solution"
			Me.radMenuItem9.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem3
			' 
			Me.radMenuSeparatorItem3.AccessibleDescription = "radMenuSeparatorItem3"
			Me.radMenuSeparatorItem3.AccessibleName = "radMenuSeparatorItem3"
			Me.radMenuSeparatorItem3.Name = "radMenuSeparatorItem3"
			Me.radMenuSeparatorItem3.Text = "radMenuSeparatorItem3"
			Me.radMenuSeparatorItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem10
			' 
			Me.radMenuItem10.AccessibleDescription = "Advanced Save Options..."
			Me.radMenuItem10.AccessibleName = "Advanced Save Options..."
			Me.radMenuItem10.Name = "radMenuItem10"
			Me.radMenuItem10.Text = "Advanced Save Options..."
			Me.radMenuItem10.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem11
			' 
			Me.radMenuItem11.AccessibleDescription = "Save All"
			Me.radMenuItem11.AccessibleName = "Save All"
			Me.radMenuItem11.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem17, Me.radMenuItem18, Me.radMenuItem19})
			Me.radMenuItem11.Name = "radMenuItem11"
			Me.radMenuItem11.Text = "Save All"
			Me.radMenuItem11.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem17
			' 
			Me.radMenuItem17.AccessibleDescription = "radMenuItem17"
			Me.radMenuItem17.AccessibleName = "radMenuItem17"
			Me.radMenuItem17.Name = "radMenuItem17"
			Me.radMenuItem17.Text = "radMenuItem17"
			Me.radMenuItem17.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem18
			' 
			Me.radMenuItem18.AccessibleDescription = "radMenuItem18"
			Me.radMenuItem18.AccessibleName = "radMenuItem18"
			Me.radMenuItem18.Name = "radMenuItem18"
			Me.radMenuItem18.Text = "radMenuItem18"
			Me.radMenuItem18.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem19
			' 
			Me.radMenuItem19.AccessibleDescription = "radMenuItem19"
			Me.radMenuItem19.AccessibleName = "radMenuItem19"
			Me.radMenuItem19.Name = "radMenuItem19"
			Me.radMenuItem19.Text = "radMenuItem19"
			Me.radMenuItem19.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem12
			' 
			Me.radMenuItem12.AccessibleDescription = "Export Template..."
			Me.radMenuItem12.AccessibleName = "Export Template..."
			Me.radMenuItem12.Name = "radMenuItem12"
			Me.radMenuItem12.Text = "Export Template..."
			Me.radMenuItem12.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem4
			' 
			Me.radMenuSeparatorItem4.AccessibleDescription = "radMenuSeparatorItem4"
			Me.radMenuSeparatorItem4.AccessibleName = "radMenuSeparatorItem4"
			Me.radMenuSeparatorItem4.Name = "radMenuSeparatorItem4"
			Me.radMenuSeparatorItem4.Text = "radMenuSeparatorItem4"
			Me.radMenuSeparatorItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem13
			' 
			Me.radMenuItem13.AccessibleDescription = "Source Control"
			Me.radMenuItem13.AccessibleName = "Source Control"
			Me.radMenuItem13.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem20, Me.radMenuItem21, Me.radMenuItem22})
			Me.radMenuItem13.Name = "radMenuItem13"
			Me.radMenuItem13.Text = "Source Control"
			Me.radMenuItem13.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem20
			' 
			Me.radMenuItem20.AccessibleDescription = "View History"
			Me.radMenuItem20.AccessibleName = "View History"
			Me.radMenuItem20.Name = "radMenuItem20"
			Me.radMenuItem20.Text = "View History"
			Me.radMenuItem20.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem21
			' 
			Me.radMenuItem21.AccessibleDescription = "Refresh Status"
			Me.radMenuItem21.AccessibleName = "Refresh Status"
			Me.radMenuItem21.Name = "radMenuItem21"
			Me.radMenuItem21.Text = "Refresh Status"
			Me.radMenuItem21.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem22
			' 
			Me.radMenuItem22.AccessibleDescription = "Compare..."
			Me.radMenuItem22.AccessibleName = "Compare..."
			Me.radMenuItem22.Name = "radMenuItem22"
			Me.radMenuItem22.Text = "Compare..."
			Me.radMenuItem22.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem5
			' 
			Me.radMenuSeparatorItem5.AccessibleDescription = "radMenuSeparatorItem5"
			Me.radMenuSeparatorItem5.AccessibleName = "radMenuSeparatorItem5"
			Me.radMenuSeparatorItem5.Name = "radMenuSeparatorItem5"
			Me.radMenuSeparatorItem5.Text = "radMenuSeparatorItem5"
			Me.radMenuSeparatorItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem14
			' 
			Me.radMenuItem14.AccessibleDescription = "Page Setup..."
			Me.radMenuItem14.AccessibleName = "Page Setup..."
			Me.radMenuItem14.Name = "radMenuItem14"
			Me.radMenuItem14.Text = "Page Setup..."
			Me.radMenuItem14.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem15
			' 
			Me.radMenuItem15.AccessibleDescription = "Print..."
			Me.radMenuItem15.AccessibleName = "Print..."
			Me.radMenuItem15.Name = "radMenuItem15"
			Me.radMenuItem15.Text = "Print..."
			Me.radMenuItem15.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem6
			' 
			Me.radMenuSeparatorItem6.AccessibleDescription = "radMenuSeparatorItem6"
			Me.radMenuSeparatorItem6.AccessibleName = "radMenuSeparatorItem6"
			Me.radMenuSeparatorItem6.Name = "radMenuSeparatorItem6"
			Me.radMenuSeparatorItem6.Text = "radMenuSeparatorItem6"
			Me.radMenuSeparatorItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem16
			' 
			Me.radMenuItem16.AccessibleDescription = "Recent Files"
			Me.radMenuItem16.AccessibleName = "Recent Files"
			Me.radMenuItem16.Name = "radMenuItem16"
			Me.radMenuItem16.Text = "Recent Files"
			Me.radMenuItem16.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem23
			' 
			Me.radMenuItem23.AccessibleDescription = "Recent Projects"
			Me.radMenuItem23.AccessibleName = "Recent Projects"
			Me.radMenuItem23.Name = "radMenuItem23"
			Me.radMenuItem23.Text = "Recent Projects"
			Me.radMenuItem23.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem7
			' 
			Me.radMenuSeparatorItem7.AccessibleDescription = "radMenuSeparatorItem7"
			Me.radMenuSeparatorItem7.AccessibleName = "radMenuSeparatorItem7"
			Me.radMenuSeparatorItem7.Name = "radMenuSeparatorItem7"
			Me.radMenuSeparatorItem7.Text = "radMenuSeparatorItem7"
			Me.radMenuSeparatorItem7.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem24
			' 
			Me.radMenuItem24.AccessibleDescription = "Exit"
			Me.radMenuItem24.AccessibleName = "Exit"
			Me.radMenuItem24.Name = "radMenuItem24"
			Me.radMenuItem24.Text = "Exit"
			Me.radMenuItem24.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2
			' 
			Me.radMenuItem2.AccessibleDescription = "Edit"
			Me.radMenuItem2.AccessibleName = "Edit"
			Me.radMenuItem2.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem25, Me.radMenuItem26, Me.radMenuSeparatorItem8, Me.radMenuItem27, Me.radMenuItem28, Me.radMenuItem29, Me.radMenuItem30, Me.radMenuItem31, Me.radMenuSeparatorItem9, Me.radMenuItem32, Me.radMenuSeparatorItem10, Me.radMenuItem33, Me.radMenuItem34, Me.radMenuItem35, Me.radMenuItem36, Me.radMenuItem37, Me.radMenuItem38, Me.radMenuItem39, Me.radMenuSeparatorItem11, Me.radMenuItem40})
			Me.radMenuItem2.Name = "radMenuItem2"
			Me.radMenuItem2.Text = "Edit"
			Me.radMenuItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem25
			' 
			Me.radMenuItem25.AccessibleDescription = "Undo"
			Me.radMenuItem25.AccessibleName = "Undo"
			Me.radMenuItem25.Name = "radMenuItem25"
			Me.radMenuItem25.Text = "Undo"
			Me.radMenuItem25.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem26
			' 
			Me.radMenuItem26.AccessibleDescription = "Redo"
			Me.radMenuItem26.AccessibleName = "Redo"
			Me.radMenuItem26.Name = "radMenuItem26"
			Me.radMenuItem26.Text = "Redo"
			Me.radMenuItem26.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem8
			' 
			Me.radMenuSeparatorItem8.AccessibleDescription = "radMenuSeparatorItem8"
			Me.radMenuSeparatorItem8.AccessibleName = "radMenuSeparatorItem8"
			Me.radMenuSeparatorItem8.Name = "radMenuSeparatorItem8"
			Me.radMenuSeparatorItem8.Text = "radMenuSeparatorItem8"
			Me.radMenuSeparatorItem8.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem27
			' 
			Me.radMenuItem27.AccessibleDescription = "Cut"
			Me.radMenuItem27.AccessibleName = "Cut"
			Me.radMenuItem27.Name = "radMenuItem27"
			Me.radMenuItem27.Text = "Cut"
			Me.radMenuItem27.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem28
			' 
			Me.radMenuItem28.AccessibleDescription = "Copy"
			Me.radMenuItem28.AccessibleName = "Copy"
			Me.radMenuItem28.Name = "radMenuItem28"
			Me.radMenuItem28.Text = "Copy"
			Me.radMenuItem28.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem29
			' 
			Me.radMenuItem29.AccessibleDescription = "Paste"
			Me.radMenuItem29.AccessibleName = "Paste"
			Me.radMenuItem29.Name = "radMenuItem29"
			Me.radMenuItem29.Text = "Paste"
			Me.radMenuItem29.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem30
			' 
			Me.radMenuItem30.AccessibleDescription = "Cycle Clipboard Ring"
			Me.radMenuItem30.AccessibleName = "Cycle Clipboard Ring"
			Me.radMenuItem30.Name = "radMenuItem30"
			Me.radMenuItem30.Text = "Cycle Clipboard Ring"
			Me.radMenuItem30.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem31
			' 
			Me.radMenuItem31.AccessibleDescription = "Delete"
			Me.radMenuItem31.AccessibleName = "Delete"
			Me.radMenuItem31.Name = "radMenuItem31"
			Me.radMenuItem31.Text = "Delete"
			Me.radMenuItem31.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem9
			' 
			Me.radMenuSeparatorItem9.AccessibleDescription = "radMenuSeparatorItem9"
			Me.radMenuSeparatorItem9.AccessibleName = "radMenuSeparatorItem9"
			Me.radMenuSeparatorItem9.Name = "radMenuSeparatorItem9"
			Me.radMenuSeparatorItem9.Text = "radMenuSeparatorItem9"
			Me.radMenuSeparatorItem9.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem32
			' 
			Me.radMenuItem32.AccessibleDescription = "Select All"
			Me.radMenuItem32.AccessibleName = "Select All"
			Me.radMenuItem32.Name = "radMenuItem32"
			Me.radMenuItem32.Text = "Select All"
			Me.radMenuItem32.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem10
			' 
			Me.radMenuSeparatorItem10.AccessibleDescription = "radMenuSeparatorItem10"
			Me.radMenuSeparatorItem10.AccessibleName = "radMenuSeparatorItem10"
			Me.radMenuSeparatorItem10.Name = "radMenuSeparatorItem10"
			Me.radMenuSeparatorItem10.Text = "radMenuSeparatorItem10"
			Me.radMenuSeparatorItem10.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem33
			' 
			Me.radMenuItem33.AccessibleDescription = "Find and Replace"
			Me.radMenuItem33.AccessibleName = "Find and Replace"
			Me.radMenuItem33.Name = "radMenuItem33"
			Me.radMenuItem33.Text = "Find and Replace"
			Me.radMenuItem33.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem34
			' 
			Me.radMenuItem34.AccessibleDescription = "Go To..."
			Me.radMenuItem34.AccessibleName = "Go To..."
			Me.radMenuItem34.Name = "radMenuItem34"
			Me.radMenuItem34.Text = "Go To..."
			Me.radMenuItem34.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem35
			' 
			Me.radMenuItem35.AccessibleDescription = "Show Source File"
			Me.radMenuItem35.AccessibleName = "Show Source File"
			Me.radMenuItem35.Name = "radMenuItem35"
			Me.radMenuItem35.Text = "Show Source File"
			Me.radMenuItem35.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem36
			' 
			Me.radMenuItem36.AccessibleDescription = "Insert File as Text..."
			Me.radMenuItem36.AccessibleName = "Insert File as Text..."
			Me.radMenuItem36.Name = "radMenuItem36"
			Me.radMenuItem36.Text = "Insert File as Text..."
			Me.radMenuItem36.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem37
			' 
			Me.radMenuItem37.AccessibleDescription = "Advanced"
			Me.radMenuItem37.AccessibleName = "Advanced"
			Me.radMenuItem37.Name = "radMenuItem37"
			Me.radMenuItem37.Text = "Advanced"
			Me.radMenuItem37.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem38
			' 
			Me.radMenuItem38.AccessibleDescription = "Bookmakrs"
			Me.radMenuItem38.AccessibleName = "Bookmakrs"
			Me.radMenuItem38.Name = "radMenuItem38"
			Me.radMenuItem38.Text = "Bookmakrs"
			Me.radMenuItem38.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem39
			' 
			Me.radMenuItem39.AccessibleDescription = "Outlining"
			Me.radMenuItem39.AccessibleName = "Outlining"
			Me.radMenuItem39.Name = "radMenuItem39"
			Me.radMenuItem39.Text = "Outlining"
			Me.radMenuItem39.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem11
			' 
			Me.radMenuSeparatorItem11.AccessibleDescription = "radMenuSeparatorItem11"
			Me.radMenuSeparatorItem11.AccessibleName = "radMenuSeparatorItem11"
			Me.radMenuSeparatorItem11.Name = "radMenuSeparatorItem11"
			Me.radMenuSeparatorItem11.Text = "radMenuSeparatorItem11"
			Me.radMenuSeparatorItem11.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem40
			' 
			Me.radMenuItem40.AccessibleDescription = "InteliSence"
			Me.radMenuItem40.AccessibleName = "InteliSence"
			Me.radMenuItem40.Name = "radMenuItem40"
			Me.radMenuItem40.Text = "InteliSence"
			Me.radMenuItem40.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3
			' 
			Me.radMenuItem3.AccessibleDescription = "Build"
			Me.radMenuItem3.AccessibleName = "Build"
			Me.radMenuItem3.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem41, Me.radMenuItem42, Me.radMenuItem43})
			Me.radMenuItem3.Name = "radMenuItem3"
			Me.radMenuItem3.Text = "Build"
			Me.radMenuItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem41
			' 
			Me.radMenuItem41.AccessibleDescription = "Build Solution"
			Me.radMenuItem41.AccessibleName = "Build Solution"
			Me.radMenuItem41.Name = "radMenuItem41"
			Me.radMenuItem41.Text = "Build Solution"
			Me.radMenuItem41.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem42
			' 
			Me.radMenuItem42.AccessibleDescription = "Build Examples"
			Me.radMenuItem42.AccessibleName = "Build Examples"
			Me.radMenuItem42.Name = "radMenuItem42"
			Me.radMenuItem42.Text = "Build Examples"
			Me.radMenuItem42.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem43
			' 
			Me.radMenuItem43.AccessibleDescription = "Publish Examples"
			Me.radMenuItem43.AccessibleName = "Publish Examples"
			Me.radMenuItem43.Name = "radMenuItem43"
			Me.radMenuItem43.Text = "Publish Examples"
			Me.radMenuItem43.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem4
			' 
			Me.radMenuItem4.AccessibleDescription = "Data"
			Me.radMenuItem4.AccessibleName = "Data"
			Me.radMenuItem4.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem44, Me.radMenuItem45})
			Me.radMenuItem4.Name = "radMenuItem4"
			Me.radMenuItem4.Text = "Data"
			Me.radMenuItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem44
			' 
			Me.radMenuItem44.AccessibleDescription = "Show Data Sources"
			Me.radMenuItem44.AccessibleName = "Show Data Sources"
			Me.radMenuItem44.Name = "radMenuItem44"
			Me.radMenuItem44.Text = "Show Data Sources"
			Me.radMenuItem44.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem45
			' 
			Me.radMenuItem45.AccessibleDescription = "Add New Data Source..."
			Me.radMenuItem45.AccessibleName = "Add New Data Source..."
			Me.radMenuItem45.Name = "radMenuItem45"
			Me.radMenuItem45.Text = "Add New Data Source..."
			Me.radMenuItem45.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' Form1
			' 
			Me.Controls.Add(Me.radPanelDemoHolder)
			Me.MaximumSize = New Size(1280, 994)
			Me.Name = "Form1"
			Me.Padding = New Padding(2, 35, 2, 4)
			Me.Size = New Size(1170, 671)
			Me.Controls.SetChildIndex(Me.radPanelDemoHolder, 0)
			Me.Controls.SetChildIndex(Me.settingsPanel, 0)
			CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radButton2, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radButton1, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radPanelDemoHolder, System.ComponentModel.ISupportInitialize).EndInit()
			Me.radPanelDemoHolder.ResumeLayout(False)
			Me.radPanelDemoHolder.PerformLayout()
			CType(Me.radMenu1, System.ComponentModel.ISupportInitialize).EndInit()
			Me.ResumeLayout(False)

		End Sub

		#End Region

		Private radMenuItem6_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_4 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_5 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_6 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_7 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_8 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_9 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_10 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_11 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_12 As Telerik.WinControls.UI.RadMenuItem
		Private imageList1 As ImageList
		Private radMenuItemSrc_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSrc_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSrc_3 As Telerik.WinControls.UI.RadMenuItem
		Private radButton2 As Telerik.WinControls.UI.RadButton
		Private radButton1 As Telerik.WinControls.UI.RadButton
		Private radThemeManager1 As Telerik.WinControls.RadThemeManager
		Private radPanelDemoHolder As Telerik.WinControls.UI.RadPanel
		Private radMenu1 As Telerik.WinControls.UI.RadMenu
		Private radMenuItem1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem5 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuSeparatorItem1 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuItem7 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuSeparatorItem2 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuItem8 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem9 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem10 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuSeparatorItem3 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuItem11 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem17 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem18 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem19 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuSeparatorItem4 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuItem12 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem13 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuSeparatorItem5 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuItem14 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem15 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuSeparatorItem6 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuItem16 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem4 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem20 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem21 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem22 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem23 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuSeparatorItem7 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuItem24 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem25 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem26 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuSeparatorItem8 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuItem27 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem28 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem29 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem30 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem31 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuSeparatorItem9 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuItem32 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuSeparatorItem10 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuItem33 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem34 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem35 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem36 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem37 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem38 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem39 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuSeparatorItem11 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuItem40 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem41 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem42 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem43 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem44 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem45 As Telerik.WinControls.UI.RadMenuItem
	End Class
End Namespace