Namespace Telerik.Examples.WinControls.MenuStrip.MultilineTextSupport
	Partial Public Class Form1
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.components = New System.ComponentModel.Container()
			Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(Form1))
			Me.imageList1 = New ImageList(Me.components)
			Me.radMenu1 = New Telerik.WinControls.UI.RadMenu()
			Me.radMenuItem1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem1_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem1_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem1 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem1_3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem2 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem1_4 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem1_5 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem3 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem1_6 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem1_7 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem1_8 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem4 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem1_9 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSrc_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSrc_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSrc_3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem5 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem1_10 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem1_11 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem6 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem1_12 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem1_13 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem7 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem1_14 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem8 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem2_3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_4 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_5 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_6 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_7 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem9 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem2_8 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem10 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem2_9 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_10 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_11 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_12 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_13 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_14 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_15 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_16 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_4 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_5 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_6 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_7 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_8 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_9 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_10 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem4 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem4_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem4_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem4_3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem5 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem5_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem5_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_4 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_5 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_6 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_7 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_8 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_9 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_10 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_11 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_12 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radPanelDemoHolder = New Telerik.WinControls.UI.RadPanel()
			CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radPanelDemoHolder, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.radPanelDemoHolder.SuspendLayout()
			Me.SuspendLayout()
			' 
			' settingsPanel
			' 
			Me.settingsPanel.Location = New Point(1023, 1)
			Me.settingsPanel.Size = New Size(200, 735)
			' 
			' imageList1
			' 
			Me.imageList1.ImageStream = (CType(resources.GetObject("imageList1.ImageStream"), ImageListStreamer))
			Me.imageList1.TransparentColor = Color.Magenta
			Me.imageList1.Images.SetKeyName(0, "")
			Me.imageList1.Images.SetKeyName(1, "")
			Me.imageList1.Images.SetKeyName(2, "")
			Me.imageList1.Images.SetKeyName(3, "")
			Me.imageList1.Images.SetKeyName(4, "")
			Me.imageList1.Images.SetKeyName(5, "CloseSolution.bmp")
			' 
			' radMenu1
			' 
			Me.radMenu1.AllItemsEqualHeight = True
			Me.radMenu1.ImageList = Me.imageList1
			Me.radMenu1.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem1, Me.radMenuItem2, Me.radMenuItem3, Me.radMenuItem4, Me.radMenuItem5})
			Me.radMenu1.Location = New Point(0, 0)
			Me.radMenu1.Name = "radMenu1"
			' 
			' 
			' 
			Me.radMenu1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
			Me.radMenu1.Size = New Size(500, 37)
			Me.radMenu1.TabIndex = 0
			' 
			' radMenuItem1
			' 
			Me.radMenuItem1.AccessibleDescription = "Content" & vbCrLf & "Management"
			Me.radMenuItem1.AccessibleName = "Content" & vbCrLf & "Management"
			Me.radMenuItem1.ClickMode = Telerik.WinControls.ClickMode.Press
			Me.radMenuItem1.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem1_1, Me.radMenuItem1_2, Me.radMenuSeparatorItem1, Me.radMenuItem1_3, Me.radMenuSeparatorItem2, Me.radMenuItem1_4, Me.radMenuItem1_5, Me.radMenuSeparatorItem3, Me.radMenuItem1_6, Me.radMenuItem1_7, Me.radMenuItem1_8, Me.radMenuSeparatorItem4, Me.radMenuItem1_9, Me.radMenuSeparatorItem5, Me.radMenuItem1_10, Me.radMenuItem1_11, Me.radMenuSeparatorItem6, Me.radMenuItem1_12, Me.radMenuItem1_13, Me.radMenuSeparatorItem7, Me.radMenuItem1_14})
			Me.radMenuItem1.Name = "radMenuItem1"
			Me.radMenuItem1.Padding = New Padding(2)
			Me.radMenuItem1.ShowArrow = False
			Me.radMenuItem1.Text = "Content" & vbCrLf & "Management"
			Me.radMenuItem1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_1
			' 
			Me.radMenuItem1_1.AccessibleDescription = "New"
			Me.radMenuItem1_1.AccessibleName = "New"
			Me.radMenuItem1_1.MinSize = New Size(180, 25)
			Me.radMenuItem1_1.Name = "radMenuItem1_1"
			Me.radMenuItem1_1.Text = "New"
			Me.radMenuItem1_1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_2
			' 
			Me.radMenuItem1_2.AccessibleDescription = "Open"
			Me.radMenuItem1_2.AccessibleName = "Open"
			Me.radMenuItem1_2.Name = "radMenuItem1_2"
			Me.radMenuItem1_2.Text = "Open"
			Me.radMenuItem1_2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem1
			' 
			Me.radMenuSeparatorItem1.AccessibleDescription = "radMenuSeparatorItem1"
			Me.radMenuSeparatorItem1.AccessibleName = "radMenuSeparatorItem1"
			Me.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1"
			Me.radMenuSeparatorItem1.Text = "radMenuSeparatorItem1"
			Me.radMenuSeparatorItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_3
			' 
			Me.radMenuItem1_3.AccessibleDescription = "Add"
			Me.radMenuItem1_3.AccessibleName = "Add"
			Me.radMenuItem1_3.Name = "radMenuItem1_3"
			Me.radMenuItem1_3.Text = "Add"
			Me.radMenuItem1_3.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem2
			' 
			Me.radMenuSeparatorItem2.AccessibleDescription = "radMenuSeparatorItem2"
			Me.radMenuSeparatorItem2.AccessibleName = "radMenuSeparatorItem2"
			Me.radMenuSeparatorItem2.Name = "radMenuSeparatorItem2"
			Me.radMenuSeparatorItem2.Text = "radMenuSeparatorItem2"
			Me.radMenuSeparatorItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_4
			' 
			Me.radMenuItem1_4.AccessibleDescription = "Close"
			Me.radMenuItem1_4.AccessibleName = "Close"
			Me.radMenuItem1_4.Name = "radMenuItem1_4"
			Me.radMenuItem1_4.Text = "Close"
			Me.radMenuItem1_4.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_5
			' 
			Me.radMenuItem1_5.AccessibleDescription = "Close Solution"
			Me.radMenuItem1_5.AccessibleName = "Close Solution"
			Me.radMenuItem1_5.Name = "radMenuItem1_5"
			Me.radMenuItem1_5.Text = "Close Solution"
			Me.radMenuItem1_5.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem3
			' 
			Me.radMenuSeparatorItem3.AccessibleDescription = "radMenuSeparatorItem3"
			Me.radMenuSeparatorItem3.AccessibleName = "radMenuSeparatorItem3"
			Me.radMenuSeparatorItem3.Name = "radMenuSeparatorItem3"
			Me.radMenuSeparatorItem3.Text = "radMenuSeparatorItem3"
			Me.radMenuSeparatorItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_6
			' 
			Me.radMenuItem1_6.AccessibleDescription = "Advanced Save Options..."
			Me.radMenuItem1_6.AccessibleName = "Advanced Save Options..."
			Me.radMenuItem1_6.Name = "radMenuItem1_6"
			Me.radMenuItem1_6.Text = "Advanced Save Options..."
			Me.radMenuItem1_6.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_7
			' 
			Me.radMenuItem1_7.AccessibleDescription = "Save All"
			Me.radMenuItem1_7.AccessibleName = "Save All"
			Me.radMenuItem1_7.Name = "radMenuItem1_7"
			Me.radMenuItem1_7.Text = "Save All"
			Me.radMenuItem1_7.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_7.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_8
			' 
			Me.radMenuItem1_8.AccessibleDescription = "Export Template..."
			Me.radMenuItem1_8.AccessibleName = "Export Template..."
			Me.radMenuItem1_8.Name = "radMenuItem1_8"
			Me.radMenuItem1_8.Text = "Export Template..."
			Me.radMenuItem1_8.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_8.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem4
			' 
			Me.radMenuSeparatorItem4.AccessibleDescription = "radMenuSeparatorItem4"
			Me.radMenuSeparatorItem4.AccessibleName = "radMenuSeparatorItem4"
			Me.radMenuSeparatorItem4.Name = "radMenuSeparatorItem4"
			Me.radMenuSeparatorItem4.Text = "radMenuSeparatorItem4"
			Me.radMenuSeparatorItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_9
			' 
			Me.radMenuItem1_9.AccessibleDescription = "Source Control"
			Me.radMenuItem1_9.AccessibleName = "Source Control"
			Me.radMenuItem1_9.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItemSrc_1, Me.radMenuItemSrc_2, Me.radMenuItemSrc_3})
			Me.radMenuItem1_9.Name = "radMenuItem1_9"
			Me.radMenuItem1_9.Text = "Source Control"
			Me.radMenuItem1_9.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_9.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItemSrc_1
			' 
			Me.radMenuItemSrc_1.AccessibleDescription = "View History"
			Me.radMenuItemSrc_1.AccessibleName = "View History"
			Me.radMenuItemSrc_1.Name = "radMenuItemSrc_1"
			Me.radMenuItemSrc_1.Text = "View History"
			Me.radMenuItemSrc_1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItemSrc_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItemSrc_2
			' 
			Me.radMenuItemSrc_2.AccessibleDescription = "Refresh Status"
			Me.radMenuItemSrc_2.AccessibleName = "Refresh Status"
			Me.radMenuItemSrc_2.Name = "radMenuItemSrc_2"
			Me.radMenuItemSrc_2.Text = "Refresh Status"
			Me.radMenuItemSrc_2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItemSrc_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItemSrc_3
			' 
			Me.radMenuItemSrc_3.AccessibleDescription = "Compare..."
			Me.radMenuItemSrc_3.AccessibleName = "Compare..."
			Me.radMenuItemSrc_3.Name = "radMenuItemSrc_3"
			Me.radMenuItemSrc_3.Text = "Compare..."
			Me.radMenuItemSrc_3.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItemSrc_3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem5
			' 
			Me.radMenuSeparatorItem5.AccessibleDescription = "radMenuSeparatorItem5"
			Me.radMenuSeparatorItem5.AccessibleName = "radMenuSeparatorItem5"
			Me.radMenuSeparatorItem5.Name = "radMenuSeparatorItem5"
			Me.radMenuSeparatorItem5.Text = "radMenuSeparatorItem5"
			Me.radMenuSeparatorItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_10
			' 
			Me.radMenuItem1_10.AccessibleDescription = "Page Setup..."
			Me.radMenuItem1_10.AccessibleName = "Page Setup..."
			Me.radMenuItem1_10.Name = "radMenuItem1_10"
			Me.radMenuItem1_10.Text = "Page Setup..."
			Me.radMenuItem1_10.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_10.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_11
			' 
			Me.radMenuItem1_11.AccessibleDescription = "Print..."
			Me.radMenuItem1_11.AccessibleName = "Print..."
			Me.radMenuItem1_11.Name = "radMenuItem1_11"
			Me.radMenuItem1_11.Text = "Print..."
			Me.radMenuItem1_11.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_11.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem6
			' 
			Me.radMenuSeparatorItem6.AccessibleDescription = "radMenuSeparatorItem6"
			Me.radMenuSeparatorItem6.AccessibleName = "radMenuSeparatorItem6"
			Me.radMenuSeparatorItem6.Name = "radMenuSeparatorItem6"
			Me.radMenuSeparatorItem6.Text = "radMenuSeparatorItem6"
			Me.radMenuSeparatorItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_12
			' 
			Me.radMenuItem1_12.AccessibleDescription = "Recent Files"
			Me.radMenuItem1_12.AccessibleName = "Recent Files"
			Me.radMenuItem1_12.Name = "radMenuItem1_12"
			Me.radMenuItem1_12.Text = "Recent Files"
			Me.radMenuItem1_12.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_12.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_13
			' 
			Me.radMenuItem1_13.AccessibleDescription = "Recent Projects"
			Me.radMenuItem1_13.AccessibleName = "Recent Projects"
			Me.radMenuItem1_13.Name = "radMenuItem1_13"
			Me.radMenuItem1_13.Text = "Recent Projects"
			Me.radMenuItem1_13.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_13.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem7
			' 
			Me.radMenuSeparatorItem7.AccessibleDescription = "radMenuSeparatorItem7"
			Me.radMenuSeparatorItem7.AccessibleName = "radMenuSeparatorItem7"
			Me.radMenuSeparatorItem7.Name = "radMenuSeparatorItem7"
			Me.radMenuSeparatorItem7.Text = "radMenuSeparatorItem7"
			Me.radMenuSeparatorItem7.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_14
			' 
			Me.radMenuItem1_14.AccessibleDescription = "Exit"
			Me.radMenuItem1_14.AccessibleName = "Exit"
			Me.radMenuItem1_14.Name = "radMenuItem1_14"
			Me.radMenuItem1_14.Text = "Exit"
			Me.radMenuItem1_14.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_14.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2
			' 
			Me.radMenuItem2.AccessibleDescription = "Customer" & vbCrLf & "Relations"
			Me.radMenuItem2.AccessibleName = "Customer" & vbCrLf & "Relations"
			Me.radMenuItem2.ClickMode = Telerik.WinControls.ClickMode.Press
			Me.radMenuItem2.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem2_1, Me.radMenuItem2_2, Me.radMenuSeparatorItem8, Me.radMenuItem2_3, Me.radMenuItem2_4, Me.radMenuItem2_5, Me.radMenuItem2_6, Me.radMenuItem2_7, Me.radMenuSeparatorItem9, Me.radMenuItem2_8, Me.radMenuSeparatorItem10, Me.radMenuItem2_9, Me.radMenuItem2_10, Me.radMenuItem2_11, Me.radMenuItem2_12, Me.radMenuItem2_13, Me.radMenuItem2_14, Me.radMenuItem2_15, Me.radMenuItem2_16})
			Me.radMenuItem2.Name = "radMenuItem2"
			Me.radMenuItem2.Padding = New Padding(2)
			Me.radMenuItem2.ShowArrow = False
			Me.radMenuItem2.Text = "Customer" & vbCrLf & "Relations"
			Me.radMenuItem2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_1
			' 
			Me.radMenuItem2_1.AccessibleDescription = "Undo"
			Me.radMenuItem2_1.AccessibleName = "Undo"
			Me.radMenuItem2_1.Name = "radMenuItem2_1"
			Me.radMenuItem2_1.Text = "Undo"
			Me.radMenuItem2_1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_2
			' 
			Me.radMenuItem2_2.AccessibleDescription = "Redo"
			Me.radMenuItem2_2.AccessibleName = "Redo"
			Me.radMenuItem2_2.Name = "radMenuItem2_2"
			Me.radMenuItem2_2.Text = "Redo"
			Me.radMenuItem2_2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem8
			' 
			Me.radMenuSeparatorItem8.AccessibleDescription = "radMenuSeparatorItem8"
			Me.radMenuSeparatorItem8.AccessibleName = "radMenuSeparatorItem8"
			Me.radMenuSeparatorItem8.Name = "radMenuSeparatorItem8"
			Me.radMenuSeparatorItem8.Text = "radMenuSeparatorItem8"
			Me.radMenuSeparatorItem8.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_3
			' 
			Me.radMenuItem2_3.AccessibleDescription = "Cut"
			Me.radMenuItem2_3.AccessibleName = "Cut"
			Me.radMenuItem2_3.Name = "radMenuItem2_3"
			Me.radMenuItem2_3.Text = "Cut"
			Me.radMenuItem2_3.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_4
			' 
			Me.radMenuItem2_4.AccessibleDescription = "Copy"
			Me.radMenuItem2_4.AccessibleName = "Copy"
			Me.radMenuItem2_4.Name = "radMenuItem2_4"
			Me.radMenuItem2_4.Text = "Copy"
			Me.radMenuItem2_4.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_5
			' 
			Me.radMenuItem2_5.AccessibleDescription = "Paste"
			Me.radMenuItem2_5.AccessibleName = "Paste"
			Me.radMenuItem2_5.Name = "radMenuItem2_5"
			Me.radMenuItem2_5.Text = "Paste"
			Me.radMenuItem2_5.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_6
			' 
			Me.radMenuItem2_6.AccessibleDescription = "Cycle Clipboard Ring"
			Me.radMenuItem2_6.AccessibleName = "Cycle Clipboard Ring"
			Me.radMenuItem2_6.Name = "radMenuItem2_6"
			Me.radMenuItem2_6.Text = "Cycle Clipboard Ring"
			Me.radMenuItem2_6.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_7
			' 
			Me.radMenuItem2_7.AccessibleDescription = "Delete"
			Me.radMenuItem2_7.AccessibleName = "Delete"
			Me.radMenuItem2_7.Name = "radMenuItem2_7"
			Me.radMenuItem2_7.Text = "Delete"
			Me.radMenuItem2_7.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_7.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem9
			' 
			Me.radMenuSeparatorItem9.AccessibleDescription = "radMenuSeparatorItem9"
			Me.radMenuSeparatorItem9.AccessibleName = "radMenuSeparatorItem9"
			Me.radMenuSeparatorItem9.Name = "radMenuSeparatorItem9"
			Me.radMenuSeparatorItem9.Text = "radMenuSeparatorItem9"
			Me.radMenuSeparatorItem9.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_8
			' 
			Me.radMenuItem2_8.AccessibleDescription = "Select All"
			Me.radMenuItem2_8.AccessibleName = "Select All"
			Me.radMenuItem2_8.Name = "radMenuItem2_8"
			Me.radMenuItem2_8.Text = "Select All"
			Me.radMenuItem2_8.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_8.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem10
			' 
			Me.radMenuSeparatorItem10.AccessibleDescription = "radMenuSeparatorItem10"
			Me.radMenuSeparatorItem10.AccessibleName = "radMenuSeparatorItem10"
			Me.radMenuSeparatorItem10.Name = "radMenuSeparatorItem10"
			Me.radMenuSeparatorItem10.Text = "radMenuSeparatorItem10"
			Me.radMenuSeparatorItem10.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_9
			' 
			Me.radMenuItem2_9.AccessibleDescription = "Find and Replace"
			Me.radMenuItem2_9.AccessibleName = "Find and Replace"
			Me.radMenuItem2_9.Name = "radMenuItem2_9"
			Me.radMenuItem2_9.Text = "Find and Replace"
			Me.radMenuItem2_9.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_9.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_10
			' 
			Me.radMenuItem2_10.AccessibleDescription = "Go To..."
			Me.radMenuItem2_10.AccessibleName = "Go To..."
			Me.radMenuItem2_10.Name = "radMenuItem2_10"
			Me.radMenuItem2_10.Text = "Go To..."
			Me.radMenuItem2_10.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_10.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_11
			' 
			Me.radMenuItem2_11.AccessibleDescription = "Show Source File"
			Me.radMenuItem2_11.AccessibleName = "Show Source File"
			Me.radMenuItem2_11.Name = "radMenuItem2_11"
			Me.radMenuItem2_11.Text = "Show Source File"
			Me.radMenuItem2_11.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_11.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_12
			' 
			Me.radMenuItem2_12.AccessibleDescription = "Insert File as Text..."
			Me.radMenuItem2_12.AccessibleName = "Insert File as Text..."
			Me.radMenuItem2_12.Name = "radMenuItem2_12"
			Me.radMenuItem2_12.Text = "Insert File as Text..."
			Me.radMenuItem2_12.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_12.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_13
			' 
			Me.radMenuItem2_13.AccessibleDescription = "Advanced"
			Me.radMenuItem2_13.AccessibleName = "Advanced"
			Me.radMenuItem2_13.Name = "radMenuItem2_13"
			Me.radMenuItem2_13.Text = "Advanced"
			Me.radMenuItem2_13.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_13.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_14
			' 
			Me.radMenuItem2_14.AccessibleDescription = "Bookmarks"
			Me.radMenuItem2_14.AccessibleName = "Bookmarks"
			Me.radMenuItem2_14.Name = "radMenuItem2_14"
			Me.radMenuItem2_14.Text = "Bookmarks"
			Me.radMenuItem2_14.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_14.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_15
			' 
			Me.radMenuItem2_15.AccessibleDescription = "Outlining"
			Me.radMenuItem2_15.AccessibleName = "Outlining"
			Me.radMenuItem2_15.Name = "radMenuItem2_15"
			Me.radMenuItem2_15.Text = "Outlining"
			Me.radMenuItem2_15.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_15.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_16
			' 
			Me.radMenuItem2_16.AccessibleDescription = "IntelliSense"
			Me.radMenuItem2_16.AccessibleName = "IntelliSense"
			Me.radMenuItem2_16.Name = "radMenuItem2_16"
			Me.radMenuItem2_16.Text = "IntelliSense"
			Me.radMenuItem2_16.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_16.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3
			' 
			Me.radMenuItem3.AccessibleDescription = "Supply Chain" & vbCrLf & "Management"
			Me.radMenuItem3.AccessibleName = "Supply Chain" & vbCrLf & "Management"
			Me.radMenuItem3.ClickMode = Telerik.WinControls.ClickMode.Press
			Me.radMenuItem3.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem3_1, Me.radMenuItem3_2, Me.radMenuItem3_3, Me.radMenuItem3_4, Me.radMenuItem3_5, Me.radMenuItem3_6, Me.radMenuItem3_7, Me.radMenuItem3_8, Me.radMenuItem3_9, Me.radMenuItem3_10})
			Me.radMenuItem3.Name = "radMenuItem3"
			Me.radMenuItem3.Padding = New Padding(2)
			Me.radMenuItem3.ShowArrow = False
			Me.radMenuItem3.Text = "Supply Chain" & vbCrLf & "Management"
			Me.radMenuItem3.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_1
			' 
			Me.radMenuItem3_1.AccessibleDescription = "Code"
			Me.radMenuItem3_1.AccessibleName = "Code"
			Me.radMenuItem3_1.Name = "radMenuItem3_1"
			Me.radMenuItem3_1.Text = "Code"
			Me.radMenuItem3_1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_2
			' 
			Me.radMenuItem3_2.AccessibleDescription = "Open"
			Me.radMenuItem3_2.AccessibleName = "Open"
			Me.radMenuItem3_2.Name = "radMenuItem3_2"
			Me.radMenuItem3_2.Text = "Open"
			Me.radMenuItem3_2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_3
			' 
			Me.radMenuItem3_3.AccessibleDescription = "Open With..."
			Me.radMenuItem3_3.AccessibleName = "Open With..."
			Me.radMenuItem3_3.Name = "radMenuItem3_3"
			Me.radMenuItem3_3.Text = "Open With..."
			Me.radMenuItem3_3.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_4
			' 
			Me.radMenuItem3_4.AccessibleDescription = "Server Explorer"
			Me.radMenuItem3_4.AccessibleName = "Server Explorer"
			Me.radMenuItem3_4.Name = "radMenuItem3_4"
			Me.radMenuItem3_4.Text = "Server Explorer"
			Me.radMenuItem3_4.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_5
			' 
			Me.radMenuItem3_5.AccessibleDescription = "Solution Explorer"
			Me.radMenuItem3_5.AccessibleName = "Solution Explorer"
			Me.radMenuItem3_5.Name = "radMenuItem3_5"
			Me.radMenuItem3_5.Text = "Solution Explorer"
			Me.radMenuItem3_5.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_6
			' 
			Me.radMenuItem3_6.AccessibleDescription = "Bookmark Window"
			Me.radMenuItem3_6.AccessibleName = "Bookmark Window"
			Me.radMenuItem3_6.Name = "radMenuItem3_6"
			Me.radMenuItem3_6.Text = "Bookmark Window"
			Me.radMenuItem3_6.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_7
			' 
			Me.radMenuItem3_7.AccessibleDescription = "Class View"
			Me.radMenuItem3_7.AccessibleName = "Class View"
			Me.radMenuItem3_7.Name = "radMenuItem3_7"
			Me.radMenuItem3_7.Text = "Class View"
			Me.radMenuItem3_7.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_7.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_8
			' 
			Me.radMenuItem3_8.AccessibleDescription = "Code Definition Window"
			Me.radMenuItem3_8.AccessibleName = "Code Definition Window"
			Me.radMenuItem3_8.Name = "radMenuItem3_8"
			Me.radMenuItem3_8.Text = "Code Definition Window"
			Me.radMenuItem3_8.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_8.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_9
			' 
			Me.radMenuItem3_9.AccessibleDescription = "Object Browser"
			Me.radMenuItem3_9.AccessibleName = "Object Browser"
			Me.radMenuItem3_9.Name = "radMenuItem3_9"
			Me.radMenuItem3_9.Text = "Object Browser"
			Me.radMenuItem3_9.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_9.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_10
			' 
			Me.radMenuItem3_10.AccessibleDescription = "Error List"
			Me.radMenuItem3_10.AccessibleName = "Error List"
			Me.radMenuItem3_10.Name = "radMenuItem3_10"
			Me.radMenuItem3_10.Text = "Error List"
			Me.radMenuItem3_10.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_10.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem4
			' 
			Me.radMenuItem4.AccessibleDescription = "Enterprise Resource" & vbCrLf & "Planning"
			Me.radMenuItem4.AccessibleName = "Enterprise Resource" & vbCrLf & "Planning"
			Me.radMenuItem4.ClickMode = Telerik.WinControls.ClickMode.Press
			Me.radMenuItem4.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem4_1, Me.radMenuItem4_2, Me.radMenuItem4_3})
			Me.radMenuItem4.Name = "radMenuItem4"
			Me.radMenuItem4.Padding = New Padding(2)
			Me.radMenuItem4.ShowArrow = False
			Me.radMenuItem4.Text = "Enterprise Resource" & vbCrLf & "Planning"
			Me.radMenuItem4.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem4_1
			' 
			Me.radMenuItem4_1.AccessibleDescription = "Build Solution"
			Me.radMenuItem4_1.AccessibleName = "Build Solution"
			Me.radMenuItem4_1.Name = "radMenuItem4_1"
			Me.radMenuItem4_1.Text = "Build Solution"
			Me.radMenuItem4_1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem4_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem4_2
			' 
			Me.radMenuItem4_2.AccessibleDescription = "Build Examples"
			Me.radMenuItem4_2.AccessibleName = "Build Examples"
			Me.radMenuItem4_2.Name = "radMenuItem4_2"
			Me.radMenuItem4_2.Text = "Build Examples"
			Me.radMenuItem4_2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem4_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem4_3
			' 
			Me.radMenuItem4_3.AccessibleDescription = "Publish Examples"
			Me.radMenuItem4_3.AccessibleName = "Publish Examples"
			Me.radMenuItem4_3.Name = "radMenuItem4_3"
			Me.radMenuItem4_3.Text = "Publish Examples"
			Me.radMenuItem4_3.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem4_3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem5
			' 
			Me.radMenuItem5.AccessibleDescription = "Data"
			Me.radMenuItem5.AccessibleName = "Data"
			Me.radMenuItem5.ClickMode = Telerik.WinControls.ClickMode.Press
			Me.radMenuItem5.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem5_1, Me.radMenuItem5_2})
			Me.radMenuItem5.Name = "radMenuItem5"
			Me.radMenuItem5.Padding = New Padding(2)
			Me.radMenuItem5.ShowArrow = False
			Me.radMenuItem5.Text = "Data"
			Me.radMenuItem5.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem5_1
			' 
			Me.radMenuItem5_1.AccessibleDescription = "Show Data Sources"
			Me.radMenuItem5_1.AccessibleName = "Show Data Sources"
			Me.radMenuItem5_1.Name = "radMenuItem5_1"
			Me.radMenuItem5_1.Text = "Show Data Sources"
			Me.radMenuItem5_1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem5_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem5_2
			' 
			Me.radMenuItem5_2.AccessibleDescription = "Add New Data Source..."
			Me.radMenuItem5_2.AccessibleName = "Add New Data Source..."
			Me.radMenuItem5_2.Name = "radMenuItem5_2"
			Me.radMenuItem5_2.Text = "Add New Data Source..."
			Me.radMenuItem5_2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem5_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_1
			' 
			Me.radMenuItem6_1.AccessibleDescription = "How Do I"
			Me.radMenuItem6_1.AccessibleName = "How Do I"
			Me.radMenuItem6_1.Name = "radMenuItem6_1"
			Me.radMenuItem6_1.Text = "How Do I"
			Me.radMenuItem6_1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_2
			' 
			Me.radMenuItem6_2.AccessibleDescription = "Search"
			Me.radMenuItem6_2.AccessibleName = "Search"
			Me.radMenuItem6_2.Name = "radMenuItem6_2"
			Me.radMenuItem6_2.Text = "Search"
			Me.radMenuItem6_2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_3
			' 
			Me.radMenuItem6_3.AccessibleDescription = "Contents"
			Me.radMenuItem6_3.AccessibleName = "Contents"
			Me.radMenuItem6_3.Name = "radMenuItem6_3"
			Me.radMenuItem6_3.Text = "Contents"
			Me.radMenuItem6_3.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_4
			' 
			Me.radMenuItem6_4.AccessibleDescription = "Index"
			Me.radMenuItem6_4.AccessibleName = "Index"
			Me.radMenuItem6_4.Name = "radMenuItem6_4"
			Me.radMenuItem6_4.Text = "Index"
			Me.radMenuItem6_4.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_5
			' 
			Me.radMenuItem6_5.AccessibleDescription = "Help Favorites"
			Me.radMenuItem6_5.AccessibleName = "Help Favorites"
			Me.radMenuItem6_5.Name = "radMenuItem6_5"
			Me.radMenuItem6_5.Text = "Help Favorites"
			Me.radMenuItem6_5.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_6
			' 
			Me.radMenuItem6_6.AccessibleDescription = "Dynamic Help"
			Me.radMenuItem6_6.AccessibleName = "Dynamic Help"
			Me.radMenuItem6_6.Name = "radMenuItem6_6"
			Me.radMenuItem6_6.Text = "Dynamic Help"
			Me.radMenuItem6_6.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_7
			' 
			Me.radMenuItem6_7.AccessibleDescription = "Index Results"
			Me.radMenuItem6_7.AccessibleName = "Index Results"
			Me.radMenuItem6_7.Name = "radMenuItem6_7"
			Me.radMenuItem6_7.Text = "Index Results"
			Me.radMenuItem6_7.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_7.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_8
			' 
			Me.radMenuItem6_8.AccessibleDescription = "Customer Feedback Options..."
			Me.radMenuItem6_8.AccessibleName = "Customer Feedback Options..."
			Me.radMenuItem6_8.Name = "radMenuItem6_8"
			Me.radMenuItem6_8.Text = "Customer Feedback Options..."
			Me.radMenuItem6_8.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_8.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_9
			' 
			Me.radMenuItem6_9.AccessibleDescription = "Register Product..."
			Me.radMenuItem6_9.AccessibleName = "Register Product..."
			Me.radMenuItem6_9.Name = "radMenuItem6_9"
			Me.radMenuItem6_9.Text = "Register Product..."
			Me.radMenuItem6_9.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_9.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_10
			' 
			Me.radMenuItem6_10.AccessibleDescription = "Check for Update"
			Me.radMenuItem6_10.AccessibleName = "Check for Update"
			Me.radMenuItem6_10.Name = "radMenuItem6_10"
			Me.radMenuItem6_10.Text = "Check for Update"
			Me.radMenuItem6_10.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_10.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_11
			' 
			Me.radMenuItem6_11.AccessibleDescription = "Technical Support"
			Me.radMenuItem6_11.AccessibleName = "Technical Support"
			Me.radMenuItem6_11.Name = "radMenuItem6_11"
			Me.radMenuItem6_11.Text = "Technical Support"
			Me.radMenuItem6_11.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_11.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_12
			' 
			Me.radMenuItem6_12.AccessibleDescription = "About Telerik WinControls Suite..."
			Me.radMenuItem6_12.AccessibleName = "About Telerik WinControls Suite..."
			Me.radMenuItem6_12.Name = "radMenuItem6_12"
			Me.radMenuItem6_12.Text = "About Telerik WinControls Suite..."
			Me.radMenuItem6_12.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_12.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radPanelDemoHolder
			' 
			Me.radPanelDemoHolder.Controls.Add(Me.radMenu1)
			Me.radPanelDemoHolder.ForeColor = Color.Black
			Me.radPanelDemoHolder.Location = New Point(0, 0)
			Me.radPanelDemoHolder.Name = "radPanelDemoHolder"
			' 
			' 
			' 
			Me.radPanelDemoHolder.RootElement.Padding = New Padding(1, 1, 0, 0)
			Me.radPanelDemoHolder.Size = New Size(500, 300)
			Me.radPanelDemoHolder.TabIndex = 2
			' 
			' Form1
			' 
			Me.Controls.Add(Me.radPanelDemoHolder)
			Me.MaximumSize = New Size(1280, 994)
			Me.Name = "Form1"
			Me.Padding = New Padding(2, 35, 2, 4)
			Me.Size = New Size(1170, 671)
			Me.Controls.SetChildIndex(Me.radPanelDemoHolder, 0)
			Me.Controls.SetChildIndex(Me.settingsPanel, 0)
			CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radMenu1, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radPanelDemoHolder, System.ComponentModel.ISupportInitialize).EndInit()
			Me.radPanelDemoHolder.ResumeLayout(False)
			Me.radPanelDemoHolder.PerformLayout()
			Me.ResumeLayout(False)

		End Sub

		#End Region


		Private radMenu1 As Telerik.WinControls.UI.RadMenu
		Private radMenuItem1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem4 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem5 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_4 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_5 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_6 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_7 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_8 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_9 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_10 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_11 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_12 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_13 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_14 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_4 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_5 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_6 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_7 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_8 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_9 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_10 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_11 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_12 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_13 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_14 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_15 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_16 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_4 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_5 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_6 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_7 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_8 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_9 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_10 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem4_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem4_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem4_3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem5_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem5_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_4 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_5 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_6 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_7 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_8 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_9 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_10 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_11 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_12 As Telerik.WinControls.UI.RadMenuItem
		Private imageList1 As ImageList
		Private radMenuItemSrc_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSrc_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSrc_3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuSeparatorItem1 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem2 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem3 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem4 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem5 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem6 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem8 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem9 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem10 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem7 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radPanelDemoHolder As Telerik.WinControls.UI.RadPanel
	End Class
End Namespace