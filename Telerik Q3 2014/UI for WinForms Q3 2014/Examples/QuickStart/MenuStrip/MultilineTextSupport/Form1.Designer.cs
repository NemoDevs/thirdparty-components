namespace Telerik.Examples.WinControls.MenuStrip.MultilineTextSupport
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.radMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem1_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem1_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem1_3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem2 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem1_4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem1_5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem3 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem1_6 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem1_7 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem1_8 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem4 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem1_9 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItemSrc_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItemSrc_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItemSrc_3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem5 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem1_10 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem1_11 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem6 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem1_12 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem1_13 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem7 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem1_14 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem8 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem2_3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_6 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_7 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem9 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem2_8 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem10 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem2_9 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_10 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_11 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_12 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_13 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_14 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_15 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2_16 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_6 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_7 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_8 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_9 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3_10 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem4_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem4_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem4_3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem5_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem5_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_6 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_7 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_8 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_9 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_10 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_11 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem6_12 = new Telerik.WinControls.UI.RadMenuItem();
            this.radPanelDemoHolder = new Telerik.WinControls.UI.RadPanel();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).BeginInit();
            this.radPanelDemoHolder.SuspendLayout();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(1023, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 735);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            this.imageList1.Images.SetKeyName(4, "");
            this.imageList1.Images.SetKeyName(5, "CloseSolution.bmp");
            // 
            // radMenu1
            // 
            this.radMenu1.AllItemsEqualHeight = true;
            this.radMenu1.ImageList = this.imageList1;
            this.radMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1,
            this.radMenuItem2,
            this.radMenuItem3,
            this.radMenuItem4,
            this.radMenuItem5});
            this.radMenu1.Location = new System.Drawing.Point(0, 0);
            this.radMenu1.Name = "radMenu1";
            // 
            // 
            // 
            this.radMenu1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.radMenu1.Size = new System.Drawing.Size(500, 37);
            this.radMenu1.TabIndex = 0;
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.AccessibleDescription = "Content\r\nManagement";
            this.radMenuItem1.AccessibleName = "Content\r\nManagement";
            this.radMenuItem1.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radMenuItem1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1_1,
            this.radMenuItem1_2,
            this.radMenuSeparatorItem1,
            this.radMenuItem1_3,
            this.radMenuSeparatorItem2,
            this.radMenuItem1_4,
            this.radMenuItem1_5,
            this.radMenuSeparatorItem3,
            this.radMenuItem1_6,
            this.radMenuItem1_7,
            this.radMenuItem1_8,
            this.radMenuSeparatorItem4,
            this.radMenuItem1_9,
            this.radMenuSeparatorItem5,
            this.radMenuItem1_10,
            this.radMenuItem1_11,
            this.radMenuSeparatorItem6,
            this.radMenuItem1_12,
            this.radMenuItem1_13,
            this.radMenuSeparatorItem7,
            this.radMenuItem1_14});
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.Padding = new System.Windows.Forms.Padding(2);
            this.radMenuItem1.ShowArrow = false;
            this.radMenuItem1.Text = "Content\r\nManagement";
            this.radMenuItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_1
            // 
            this.radMenuItem1_1.AccessibleDescription = "New";
            this.radMenuItem1_1.AccessibleName = "New";
            this.radMenuItem1_1.MinSize = new System.Drawing.Size(180, 25);
            this.radMenuItem1_1.Name = "radMenuItem1_1";
            this.radMenuItem1_1.Text = "New";
            this.radMenuItem1_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_2
            // 
            this.radMenuItem1_2.AccessibleDescription = "Open";
            this.radMenuItem1_2.AccessibleName = "Open";
            this.radMenuItem1_2.Name = "radMenuItem1_2";
            this.radMenuItem1_2.Text = "Open";
            this.radMenuItem1_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem1
            // 
            this.radMenuSeparatorItem1.AccessibleDescription = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.AccessibleName = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Text = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_3
            // 
            this.radMenuItem1_3.AccessibleDescription = "Add";
            this.radMenuItem1_3.AccessibleName = "Add";
            this.radMenuItem1_3.Name = "radMenuItem1_3";
            this.radMenuItem1_3.Text = "Add";
            this.radMenuItem1_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem2
            // 
            this.radMenuSeparatorItem2.AccessibleDescription = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.AccessibleName = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Name = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Text = "radMenuSeparatorItem2";
            this.radMenuSeparatorItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_4
            // 
            this.radMenuItem1_4.AccessibleDescription = "Close";
            this.radMenuItem1_4.AccessibleName = "Close";
            this.radMenuItem1_4.Name = "radMenuItem1_4";
            this.radMenuItem1_4.Text = "Close";
            this.radMenuItem1_4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_5
            // 
            this.radMenuItem1_5.AccessibleDescription = "Close Solution";
            this.radMenuItem1_5.AccessibleName = "Close Solution";
            this.radMenuItem1_5.Name = "radMenuItem1_5";
            this.radMenuItem1_5.Text = "Close Solution";
            this.radMenuItem1_5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem3
            // 
            this.radMenuSeparatorItem3.AccessibleDescription = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.AccessibleName = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Name = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Text = "radMenuSeparatorItem3";
            this.radMenuSeparatorItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_6
            // 
            this.radMenuItem1_6.AccessibleDescription = "Advanced Save Options...";
            this.radMenuItem1_6.AccessibleName = "Advanced Save Options...";
            this.radMenuItem1_6.Name = "radMenuItem1_6";
            this.radMenuItem1_6.Text = "Advanced Save Options...";
            this.radMenuItem1_6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_7
            // 
            this.radMenuItem1_7.AccessibleDescription = "Save All";
            this.radMenuItem1_7.AccessibleName = "Save All";
            this.radMenuItem1_7.Name = "radMenuItem1_7";
            this.radMenuItem1_7.Text = "Save All";
            this.radMenuItem1_7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_8
            // 
            this.radMenuItem1_8.AccessibleDescription = "Export Template...";
            this.radMenuItem1_8.AccessibleName = "Export Template...";
            this.radMenuItem1_8.Name = "radMenuItem1_8";
            this.radMenuItem1_8.Text = "Export Template...";
            this.radMenuItem1_8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem4
            // 
            this.radMenuSeparatorItem4.AccessibleDescription = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.AccessibleName = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Name = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Text = "radMenuSeparatorItem4";
            this.radMenuSeparatorItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_9
            // 
            this.radMenuItem1_9.AccessibleDescription = "Source Control";
            this.radMenuItem1_9.AccessibleName = "Source Control";
            this.radMenuItem1_9.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItemSrc_1,
            this.radMenuItemSrc_2,
            this.radMenuItemSrc_3});
            this.radMenuItem1_9.Name = "radMenuItem1_9";
            this.radMenuItem1_9.Text = "Source Control";
            this.radMenuItem1_9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItemSrc_1
            // 
            this.radMenuItemSrc_1.AccessibleDescription = "View History";
            this.radMenuItemSrc_1.AccessibleName = "View History";
            this.radMenuItemSrc_1.Name = "radMenuItemSrc_1";
            this.radMenuItemSrc_1.Text = "View History";
            this.radMenuItemSrc_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItemSrc_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItemSrc_2
            // 
            this.radMenuItemSrc_2.AccessibleDescription = "Refresh Status";
            this.radMenuItemSrc_2.AccessibleName = "Refresh Status";
            this.radMenuItemSrc_2.Name = "radMenuItemSrc_2";
            this.radMenuItemSrc_2.Text = "Refresh Status";
            this.radMenuItemSrc_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItemSrc_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItemSrc_3
            // 
            this.radMenuItemSrc_3.AccessibleDescription = "Compare...";
            this.radMenuItemSrc_3.AccessibleName = "Compare...";
            this.radMenuItemSrc_3.Name = "radMenuItemSrc_3";
            this.radMenuItemSrc_3.Text = "Compare...";
            this.radMenuItemSrc_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItemSrc_3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem5
            // 
            this.radMenuSeparatorItem5.AccessibleDescription = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.AccessibleName = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.Name = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.Text = "radMenuSeparatorItem5";
            this.radMenuSeparatorItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_10
            // 
            this.radMenuItem1_10.AccessibleDescription = "Page Setup...";
            this.radMenuItem1_10.AccessibleName = "Page Setup...";
            this.radMenuItem1_10.Name = "radMenuItem1_10";
            this.radMenuItem1_10.Text = "Page Setup...";
            this.radMenuItem1_10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_11
            // 
            this.radMenuItem1_11.AccessibleDescription = "Print...";
            this.radMenuItem1_11.AccessibleName = "Print...";
            this.radMenuItem1_11.Name = "radMenuItem1_11";
            this.radMenuItem1_11.Text = "Print...";
            this.radMenuItem1_11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_11.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem6
            // 
            this.radMenuSeparatorItem6.AccessibleDescription = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.AccessibleName = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.Name = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.Text = "radMenuSeparatorItem6";
            this.radMenuSeparatorItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_12
            // 
            this.radMenuItem1_12.AccessibleDescription = "Recent Files";
            this.radMenuItem1_12.AccessibleName = "Recent Files";
            this.radMenuItem1_12.Name = "radMenuItem1_12";
            this.radMenuItem1_12.Text = "Recent Files";
            this.radMenuItem1_12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_12.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_13
            // 
            this.radMenuItem1_13.AccessibleDescription = "Recent Projects";
            this.radMenuItem1_13.AccessibleName = "Recent Projects";
            this.radMenuItem1_13.Name = "radMenuItem1_13";
            this.radMenuItem1_13.Text = "Recent Projects";
            this.radMenuItem1_13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_13.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem7
            // 
            this.radMenuSeparatorItem7.AccessibleDescription = "radMenuSeparatorItem7";
            this.radMenuSeparatorItem7.AccessibleName = "radMenuSeparatorItem7";
            this.radMenuSeparatorItem7.Name = "radMenuSeparatorItem7";
            this.radMenuSeparatorItem7.Text = "radMenuSeparatorItem7";
            this.radMenuSeparatorItem7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem1_14
            // 
            this.radMenuItem1_14.AccessibleDescription = "Exit";
            this.radMenuItem1_14.AccessibleName = "Exit";
            this.radMenuItem1_14.Name = "radMenuItem1_14";
            this.radMenuItem1_14.Text = "Exit";
            this.radMenuItem1_14.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem1_14.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.AccessibleDescription = "Customer\r\nRelations";
            this.radMenuItem2.AccessibleName = "Customer\r\nRelations";
            this.radMenuItem2.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radMenuItem2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem2_1,
            this.radMenuItem2_2,
            this.radMenuSeparatorItem8,
            this.radMenuItem2_3,
            this.radMenuItem2_4,
            this.radMenuItem2_5,
            this.radMenuItem2_6,
            this.radMenuItem2_7,
            this.radMenuSeparatorItem9,
            this.radMenuItem2_8,
            this.radMenuSeparatorItem10,
            this.radMenuItem2_9,
            this.radMenuItem2_10,
            this.radMenuItem2_11,
            this.radMenuItem2_12,
            this.radMenuItem2_13,
            this.radMenuItem2_14,
            this.radMenuItem2_15,
            this.radMenuItem2_16});
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.Padding = new System.Windows.Forms.Padding(2);
            this.radMenuItem2.ShowArrow = false;
            this.radMenuItem2.Text = "Customer\r\nRelations";
            this.radMenuItem2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_1
            // 
            this.radMenuItem2_1.AccessibleDescription = "Undo";
            this.radMenuItem2_1.AccessibleName = "Undo";
            this.radMenuItem2_1.Name = "radMenuItem2_1";
            this.radMenuItem2_1.Text = "Undo";
            this.radMenuItem2_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_2
            // 
            this.radMenuItem2_2.AccessibleDescription = "Redo";
            this.radMenuItem2_2.AccessibleName = "Redo";
            this.radMenuItem2_2.Name = "radMenuItem2_2";
            this.radMenuItem2_2.Text = "Redo";
            this.radMenuItem2_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem8
            // 
            this.radMenuSeparatorItem8.AccessibleDescription = "radMenuSeparatorItem8";
            this.radMenuSeparatorItem8.AccessibleName = "radMenuSeparatorItem8";
            this.radMenuSeparatorItem8.Name = "radMenuSeparatorItem8";
            this.radMenuSeparatorItem8.Text = "radMenuSeparatorItem8";
            this.radMenuSeparatorItem8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_3
            // 
            this.radMenuItem2_3.AccessibleDescription = "Cut";
            this.radMenuItem2_3.AccessibleName = "Cut";
            this.radMenuItem2_3.Name = "radMenuItem2_3";
            this.radMenuItem2_3.Text = "Cut";
            this.radMenuItem2_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_4
            // 
            this.radMenuItem2_4.AccessibleDescription = "Copy";
            this.radMenuItem2_4.AccessibleName = "Copy";
            this.radMenuItem2_4.Name = "radMenuItem2_4";
            this.radMenuItem2_4.Text = "Copy";
            this.radMenuItem2_4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_5
            // 
            this.radMenuItem2_5.AccessibleDescription = "Paste";
            this.radMenuItem2_5.AccessibleName = "Paste";
            this.radMenuItem2_5.Name = "radMenuItem2_5";
            this.radMenuItem2_5.Text = "Paste";
            this.radMenuItem2_5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_6
            // 
            this.radMenuItem2_6.AccessibleDescription = "Cycle Clipboard Ring";
            this.radMenuItem2_6.AccessibleName = "Cycle Clipboard Ring";
            this.radMenuItem2_6.Name = "radMenuItem2_6";
            this.radMenuItem2_6.Text = "Cycle Clipboard Ring";
            this.radMenuItem2_6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_7
            // 
            this.radMenuItem2_7.AccessibleDescription = "Delete";
            this.radMenuItem2_7.AccessibleName = "Delete";
            this.radMenuItem2_7.Name = "radMenuItem2_7";
            this.radMenuItem2_7.Text = "Delete";
            this.radMenuItem2_7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem9
            // 
            this.radMenuSeparatorItem9.AccessibleDescription = "radMenuSeparatorItem9";
            this.radMenuSeparatorItem9.AccessibleName = "radMenuSeparatorItem9";
            this.radMenuSeparatorItem9.Name = "radMenuSeparatorItem9";
            this.radMenuSeparatorItem9.Text = "radMenuSeparatorItem9";
            this.radMenuSeparatorItem9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_8
            // 
            this.radMenuItem2_8.AccessibleDescription = "Select All";
            this.radMenuItem2_8.AccessibleName = "Select All";
            this.radMenuItem2_8.Name = "radMenuItem2_8";
            this.radMenuItem2_8.Text = "Select All";
            this.radMenuItem2_8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuSeparatorItem10
            // 
            this.radMenuSeparatorItem10.AccessibleDescription = "radMenuSeparatorItem10";
            this.radMenuSeparatorItem10.AccessibleName = "radMenuSeparatorItem10";
            this.radMenuSeparatorItem10.Name = "radMenuSeparatorItem10";
            this.radMenuSeparatorItem10.Text = "radMenuSeparatorItem10";
            this.radMenuSeparatorItem10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_9
            // 
            this.radMenuItem2_9.AccessibleDescription = "Find and Replace";
            this.radMenuItem2_9.AccessibleName = "Find and Replace";
            this.radMenuItem2_9.Name = "radMenuItem2_9";
            this.radMenuItem2_9.Text = "Find and Replace";
            this.radMenuItem2_9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_10
            // 
            this.radMenuItem2_10.AccessibleDescription = "Go To...";
            this.radMenuItem2_10.AccessibleName = "Go To...";
            this.radMenuItem2_10.Name = "radMenuItem2_10";
            this.radMenuItem2_10.Text = "Go To...";
            this.radMenuItem2_10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_11
            // 
            this.radMenuItem2_11.AccessibleDescription = "Show Source File";
            this.radMenuItem2_11.AccessibleName = "Show Source File";
            this.radMenuItem2_11.Name = "radMenuItem2_11";
            this.radMenuItem2_11.Text = "Show Source File";
            this.radMenuItem2_11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_11.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_12
            // 
            this.radMenuItem2_12.AccessibleDescription = "Insert File as Text...";
            this.radMenuItem2_12.AccessibleName = "Insert File as Text...";
            this.radMenuItem2_12.Name = "radMenuItem2_12";
            this.radMenuItem2_12.Text = "Insert File as Text...";
            this.radMenuItem2_12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_12.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_13
            // 
            this.radMenuItem2_13.AccessibleDescription = "Advanced";
            this.radMenuItem2_13.AccessibleName = "Advanced";
            this.radMenuItem2_13.Name = "radMenuItem2_13";
            this.radMenuItem2_13.Text = "Advanced";
            this.radMenuItem2_13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_13.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_14
            // 
            this.radMenuItem2_14.AccessibleDescription = "Bookmarks";
            this.radMenuItem2_14.AccessibleName = "Bookmarks";
            this.radMenuItem2_14.Name = "radMenuItem2_14";
            this.radMenuItem2_14.Text = "Bookmarks";
            this.radMenuItem2_14.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_14.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_15
            // 
            this.radMenuItem2_15.AccessibleDescription = "Outlining";
            this.radMenuItem2_15.AccessibleName = "Outlining";
            this.radMenuItem2_15.Name = "radMenuItem2_15";
            this.radMenuItem2_15.Text = "Outlining";
            this.radMenuItem2_15.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_15.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2_16
            // 
            this.radMenuItem2_16.AccessibleDescription = "IntelliSense";
            this.radMenuItem2_16.AccessibleName = "IntelliSense";
            this.radMenuItem2_16.Name = "radMenuItem2_16";
            this.radMenuItem2_16.Text = "IntelliSense";
            this.radMenuItem2_16.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem2_16.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3
            // 
            this.radMenuItem3.AccessibleDescription = "Supply Chain\r\nManagement";
            this.radMenuItem3.AccessibleName = "Supply Chain\r\nManagement";
            this.radMenuItem3.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radMenuItem3.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem3_1,
            this.radMenuItem3_2,
            this.radMenuItem3_3,
            this.radMenuItem3_4,
            this.radMenuItem3_5,
            this.radMenuItem3_6,
            this.radMenuItem3_7,
            this.radMenuItem3_8,
            this.radMenuItem3_9,
            this.radMenuItem3_10});
            this.radMenuItem3.Name = "radMenuItem3";
            this.radMenuItem3.Padding = new System.Windows.Forms.Padding(2);
            this.radMenuItem3.ShowArrow = false;
            this.radMenuItem3.Text = "Supply Chain\r\nManagement";
            this.radMenuItem3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_1
            // 
            this.radMenuItem3_1.AccessibleDescription = "Code";
            this.radMenuItem3_1.AccessibleName = "Code";
            this.radMenuItem3_1.Name = "radMenuItem3_1";
            this.radMenuItem3_1.Text = "Code";
            this.radMenuItem3_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_2
            // 
            this.radMenuItem3_2.AccessibleDescription = "Open";
            this.radMenuItem3_2.AccessibleName = "Open";
            this.radMenuItem3_2.Name = "radMenuItem3_2";
            this.radMenuItem3_2.Text = "Open";
            this.radMenuItem3_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_3
            // 
            this.radMenuItem3_3.AccessibleDescription = "Open With...";
            this.radMenuItem3_3.AccessibleName = "Open With...";
            this.radMenuItem3_3.Name = "radMenuItem3_3";
            this.radMenuItem3_3.Text = "Open With...";
            this.radMenuItem3_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_4
            // 
            this.radMenuItem3_4.AccessibleDescription = "Server Explorer";
            this.radMenuItem3_4.AccessibleName = "Server Explorer";
            this.radMenuItem3_4.Name = "radMenuItem3_4";
            this.radMenuItem3_4.Text = "Server Explorer";
            this.radMenuItem3_4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_5
            // 
            this.radMenuItem3_5.AccessibleDescription = "Solution Explorer";
            this.radMenuItem3_5.AccessibleName = "Solution Explorer";
            this.radMenuItem3_5.Name = "radMenuItem3_5";
            this.radMenuItem3_5.Text = "Solution Explorer";
            this.radMenuItem3_5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_6
            // 
            this.radMenuItem3_6.AccessibleDescription = "Bookmark Window";
            this.radMenuItem3_6.AccessibleName = "Bookmark Window";
            this.radMenuItem3_6.Name = "radMenuItem3_6";
            this.radMenuItem3_6.Text = "Bookmark Window";
            this.radMenuItem3_6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_7
            // 
            this.radMenuItem3_7.AccessibleDescription = "Class View";
            this.radMenuItem3_7.AccessibleName = "Class View";
            this.radMenuItem3_7.Name = "radMenuItem3_7";
            this.radMenuItem3_7.Text = "Class View";
            this.radMenuItem3_7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_8
            // 
            this.radMenuItem3_8.AccessibleDescription = "Code Definition Window";
            this.radMenuItem3_8.AccessibleName = "Code Definition Window";
            this.radMenuItem3_8.Name = "radMenuItem3_8";
            this.radMenuItem3_8.Text = "Code Definition Window";
            this.radMenuItem3_8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_9
            // 
            this.radMenuItem3_9.AccessibleDescription = "Object Browser";
            this.radMenuItem3_9.AccessibleName = "Object Browser";
            this.radMenuItem3_9.Name = "radMenuItem3_9";
            this.radMenuItem3_9.Text = "Object Browser";
            this.radMenuItem3_9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem3_10
            // 
            this.radMenuItem3_10.AccessibleDescription = "Error List";
            this.radMenuItem3_10.AccessibleName = "Error List";
            this.radMenuItem3_10.Name = "radMenuItem3_10";
            this.radMenuItem3_10.Text = "Error List";
            this.radMenuItem3_10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem3_10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem4
            // 
            this.radMenuItem4.AccessibleDescription = "Enterprise Resource\r\nPlanning";
            this.radMenuItem4.AccessibleName = "Enterprise Resource\r\nPlanning";
            this.radMenuItem4.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radMenuItem4.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem4_1,
            this.radMenuItem4_2,
            this.radMenuItem4_3});
            this.radMenuItem4.Name = "radMenuItem4";
            this.radMenuItem4.Padding = new System.Windows.Forms.Padding(2);
            this.radMenuItem4.ShowArrow = false;
            this.radMenuItem4.Text = "Enterprise Resource\r\nPlanning";
            this.radMenuItem4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem4_1
            // 
            this.radMenuItem4_1.AccessibleDescription = "Build Solution";
            this.radMenuItem4_1.AccessibleName = "Build Solution";
            this.radMenuItem4_1.Name = "radMenuItem4_1";
            this.radMenuItem4_1.Text = "Build Solution";
            this.radMenuItem4_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem4_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem4_2
            // 
            this.radMenuItem4_2.AccessibleDescription = "Build Examples";
            this.radMenuItem4_2.AccessibleName = "Build Examples";
            this.radMenuItem4_2.Name = "radMenuItem4_2";
            this.radMenuItem4_2.Text = "Build Examples";
            this.radMenuItem4_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem4_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem4_3
            // 
            this.radMenuItem4_3.AccessibleDescription = "Publish Examples";
            this.radMenuItem4_3.AccessibleName = "Publish Examples";
            this.radMenuItem4_3.Name = "radMenuItem4_3";
            this.radMenuItem4_3.Text = "Publish Examples";
            this.radMenuItem4_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem4_3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem5
            // 
            this.radMenuItem5.AccessibleDescription = "Data";
            this.radMenuItem5.AccessibleName = "Data";
            this.radMenuItem5.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radMenuItem5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem5_1,
            this.radMenuItem5_2});
            this.radMenuItem5.Name = "radMenuItem5";
            this.radMenuItem5.Padding = new System.Windows.Forms.Padding(2);
            this.radMenuItem5.ShowArrow = false;
            this.radMenuItem5.Text = "Data";
            this.radMenuItem5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem5_1
            // 
            this.radMenuItem5_1.AccessibleDescription = "Show Data Sources";
            this.radMenuItem5_1.AccessibleName = "Show Data Sources";
            this.radMenuItem5_1.Name = "radMenuItem5_1";
            this.radMenuItem5_1.Text = "Show Data Sources";
            this.radMenuItem5_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem5_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem5_2
            // 
            this.radMenuItem5_2.AccessibleDescription = "Add New Data Source...";
            this.radMenuItem5_2.AccessibleName = "Add New Data Source...";
            this.radMenuItem5_2.Name = "radMenuItem5_2";
            this.radMenuItem5_2.Text = "Add New Data Source...";
            this.radMenuItem5_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem5_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_1
            // 
            this.radMenuItem6_1.AccessibleDescription = "How Do I";
            this.radMenuItem6_1.AccessibleName = "How Do I";
            this.radMenuItem6_1.Name = "radMenuItem6_1";
            this.radMenuItem6_1.Text = "How Do I";
            this.radMenuItem6_1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_2
            // 
            this.radMenuItem6_2.AccessibleDescription = "Search";
            this.radMenuItem6_2.AccessibleName = "Search";
            this.radMenuItem6_2.Name = "radMenuItem6_2";
            this.radMenuItem6_2.Text = "Search";
            this.radMenuItem6_2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_3
            // 
            this.radMenuItem6_3.AccessibleDescription = "Contents";
            this.radMenuItem6_3.AccessibleName = "Contents";
            this.radMenuItem6_3.Name = "radMenuItem6_3";
            this.radMenuItem6_3.Text = "Contents";
            this.radMenuItem6_3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_4
            // 
            this.radMenuItem6_4.AccessibleDescription = "Index";
            this.radMenuItem6_4.AccessibleName = "Index";
            this.radMenuItem6_4.Name = "radMenuItem6_4";
            this.radMenuItem6_4.Text = "Index";
            this.radMenuItem6_4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_5
            // 
            this.radMenuItem6_5.AccessibleDescription = "Help Favorites";
            this.radMenuItem6_5.AccessibleName = "Help Favorites";
            this.radMenuItem6_5.Name = "radMenuItem6_5";
            this.radMenuItem6_5.Text = "Help Favorites";
            this.radMenuItem6_5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_5.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_6
            // 
            this.radMenuItem6_6.AccessibleDescription = "Dynamic Help";
            this.radMenuItem6_6.AccessibleName = "Dynamic Help";
            this.radMenuItem6_6.Name = "radMenuItem6_6";
            this.radMenuItem6_6.Text = "Dynamic Help";
            this.radMenuItem6_6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_6.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_7
            // 
            this.radMenuItem6_7.AccessibleDescription = "Index Results";
            this.radMenuItem6_7.AccessibleName = "Index Results";
            this.radMenuItem6_7.Name = "radMenuItem6_7";
            this.radMenuItem6_7.Text = "Index Results";
            this.radMenuItem6_7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_7.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_8
            // 
            this.radMenuItem6_8.AccessibleDescription = "Customer Feedback Options...";
            this.radMenuItem6_8.AccessibleName = "Customer Feedback Options...";
            this.radMenuItem6_8.Name = "radMenuItem6_8";
            this.radMenuItem6_8.Text = "Customer Feedback Options...";
            this.radMenuItem6_8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_8.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_9
            // 
            this.radMenuItem6_9.AccessibleDescription = "Register Product...";
            this.radMenuItem6_9.AccessibleName = "Register Product...";
            this.radMenuItem6_9.Name = "radMenuItem6_9";
            this.radMenuItem6_9.Text = "Register Product...";
            this.radMenuItem6_9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_9.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_10
            // 
            this.radMenuItem6_10.AccessibleDescription = "Check for Update";
            this.radMenuItem6_10.AccessibleName = "Check for Update";
            this.radMenuItem6_10.Name = "radMenuItem6_10";
            this.radMenuItem6_10.Text = "Check for Update";
            this.radMenuItem6_10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_10.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_11
            // 
            this.radMenuItem6_11.AccessibleDescription = "Technical Support";
            this.radMenuItem6_11.AccessibleName = "Technical Support";
            this.radMenuItem6_11.Name = "radMenuItem6_11";
            this.radMenuItem6_11.Text = "Technical Support";
            this.radMenuItem6_11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_11.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem6_12
            // 
            this.radMenuItem6_12.AccessibleDescription = "About Telerik WinControls Suite...";
            this.radMenuItem6_12.AccessibleName = "About Telerik WinControls Suite...";
            this.radMenuItem6_12.Name = "radMenuItem6_12";
            this.radMenuItem6_12.Text = "About Telerik WinControls Suite...";
            this.radMenuItem6_12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.radMenuItem6_12.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radPanelDemoHolder
            // 
            this.radPanelDemoHolder.Controls.Add(this.radMenu1);
            this.radPanelDemoHolder.ForeColor = System.Drawing.Color.Black;
            this.radPanelDemoHolder.Location = new System.Drawing.Point(0, 0);
            this.radPanelDemoHolder.Name = "radPanelDemoHolder";
            // 
            // 
            // 
            this.radPanelDemoHolder.RootElement.Padding = new System.Windows.Forms.Padding(1, 1, 0, 0);
            this.radPanelDemoHolder.Size = new System.Drawing.Size(500, 300);
            this.radPanelDemoHolder.TabIndex = 2;
            // 
            // Form1
            // 
            this.Controls.Add(this.radPanelDemoHolder);
            this.MaximumSize = new System.Drawing.Size(1280, 994);
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(2, 35, 2, 4);
            this.Size = new System.Drawing.Size(1170, 671);
            this.Controls.SetChildIndex(this.radPanelDemoHolder, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelDemoHolder)).EndInit();
            this.radPanelDemoHolder.ResumeLayout(false);
            this.radPanelDemoHolder.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion


        private Telerik.WinControls.UI.RadMenu radMenu1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem4;
		private Telerik.WinControls.UI.RadMenuItem radMenuItem5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_6;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_7;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_8;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_9;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_10;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_11;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_12;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_13;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1_14;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_6;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_7;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_8;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_9;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_10;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_11;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_12;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_13;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_14;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_15;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2_16;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_6;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_7;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_8;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_9;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3_10;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem4_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem4_2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem4_3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem5_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem5_2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_6;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_7;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_8;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_9;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_10;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_11;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6_12;
        private System.Windows.Forms.ImageList imageList1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItemSrc_1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItemSrc_2;
		private Telerik.WinControls.UI.RadMenuItem radMenuItemSrc_3;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem1;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem2;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem3;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem4;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem5;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem6;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem8;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem9;
		private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem10;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem7;
        private Telerik.WinControls.UI.RadPanel radPanelDemoHolder;
    }
}