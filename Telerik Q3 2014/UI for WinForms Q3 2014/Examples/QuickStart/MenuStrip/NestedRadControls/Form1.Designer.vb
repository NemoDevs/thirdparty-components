Namespace Telerik.Examples.WinControls.MenuStrip.NestedRadControls
	Partial Public Class Form1
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.components = New System.ComponentModel.Container()
			Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(Form1))
			Dim radListDataItem19 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem20 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem21 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem22 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem23 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem24 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem25 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem1 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem2 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem3 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem4 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem5 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem6 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem7 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem8 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem9 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem10 As New Telerik.WinControls.UI.RadListDataItem()
			Dim radListDataItem11 As New Telerik.WinControls.UI.RadListDataItem()
			Me.imageList1 = New ImageList(Me.components)
			Me.radMenuDemo = New Telerik.WinControls.UI.RadMenu()
			Me.radMenuItem1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem1_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem1_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem1 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem1_3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem2 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem1_4 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem1_5 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem3 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem1_6 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem1_7 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem4 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem1_9 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSrc_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSrc_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItemSrc_3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem6 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuComboItem1 = New Telerik.WinControls.UI.RadMenuComboItem()
			Me.radMenuComboItem2 = New Telerik.WinControls.UI.RadMenuComboItem()
			Me.radMenuItem2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuComboItem3 = New Telerik.WinControls.UI.RadMenuComboItem()
			Me.radMenuComboItem4 = New Telerik.WinControls.UI.RadMenuComboItem()
			Me.radMenuSeparatorItem8 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem2_3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_4 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_5 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_6 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem2_7 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuSeparatorItem9 = New Telerik.WinControls.UI.RadMenuSeparatorItem()
			Me.radMenuItem2_8 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_4 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_5 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_6 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_7 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_8 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_9 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_10 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_11 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_12 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_13 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_14 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_15 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_16 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_17 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_18 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_19 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_20 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem3_21 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem4_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem4_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem4_3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem5_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem5_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_1 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_2 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_3 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_4 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_5 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_6 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_7 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_8 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_9 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_10 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_11 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radMenuItem6_12 = New Telerik.WinControls.UI.RadMenuItem()
			Me.radPanel1 = New Telerik.WinControls.UI.RadPanel()
			CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radMenuDemo, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radMenuComboItem1.ComboBoxElement, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radMenuComboItem2.ComboBoxElement, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radMenuComboItem3.ComboBoxElement, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radMenuComboItem4.ComboBoxElement, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.radPanel1.SuspendLayout()
			Me.SuspendLayout()
			' 
			' settingsPanel
			' 
			Me.settingsPanel.Location = New Point(1023, 1)
			Me.settingsPanel.Size = New Size(200, 735)

			' 
			' imageList1
			' 
			Me.imageList1.ImageStream = (CType(resources.GetObject("imageList1.ImageStream"), ImageListStreamer))
			Me.imageList1.TransparentColor = Color.Magenta
			Me.imageList1.Images.SetKeyName(0, "")
			Me.imageList1.Images.SetKeyName(1, "")
			Me.imageList1.Images.SetKeyName(2, "")
			Me.imageList1.Images.SetKeyName(3, "")
			Me.imageList1.Images.SetKeyName(4, "")
			Me.imageList1.Images.SetKeyName(5, "CloseSolution.bmp")
			' 
			' radMenuDemo
			' 
			Me.radMenuDemo.AllowMerge = False
			Me.radMenuDemo.BackColor = Color.Transparent
			Me.radMenuDemo.ImageList = Me.imageList1
			Me.radMenuDemo.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem1, Me.radMenuItem2})
			Me.radMenuDemo.Location = New Point(0, 0)
			Me.radMenuDemo.Name = "radMenuDemo"
			' 
			' 
			' 
			Me.radMenuDemo.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
			Me.radMenuDemo.Size = New Size(500, 20)
			Me.radMenuDemo.TabIndex = 0
			' 
			' radMenuItem1
			' 
			Me.radMenuItem1.AccessibleDescription = "File"
			Me.radMenuItem1.AccessibleName = "File"
			Me.radMenuItem1.ClickMode = Telerik.WinControls.ClickMode.Press
			Me.radMenuItem1.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItem1_1, Me.radMenuItem1_2, Me.radMenuSeparatorItem1, Me.radMenuItem1_3, Me.radMenuSeparatorItem2, Me.radMenuItem1_4, Me.radMenuItem1_5, Me.radMenuSeparatorItem3, Me.radMenuItem1_6, Me.radMenuItem1_7, Me.radMenuSeparatorItem4, Me.radMenuItem1_9, Me.radMenuSeparatorItem6, Me.radMenuComboItem1, Me.radMenuComboItem2})
			Me.radMenuItem1.Name = "radMenuItem1"
			Me.radMenuItem1.PopupDirection = Telerik.WinControls.UI.RadDirection.Down
			Me.radMenuItem1.ShowArrow = False
			Me.radMenuItem1.Text = "File"
			Me.radMenuItem1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_1
			' 
			Me.radMenuItem1_1.AccessibleDescription = "New"
			Me.radMenuItem1_1.AccessibleName = "New"
			Me.radMenuItem1_1.Name = "radMenuItem1_1"
			Me.radMenuItem1_1.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem1_1.Text = "New"
			Me.radMenuItem1_1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_2
			' 
			Me.radMenuItem1_2.AccessibleDescription = "Open"
			Me.radMenuItem1_2.AccessibleName = "Open"
			Me.radMenuItem1_2.Name = "radMenuItem1_2"
			Me.radMenuItem1_2.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem1_2.Text = "Open"
			Me.radMenuItem1_2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem1
			' 
			Me.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1"
			Me.radMenuSeparatorItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_3
			' 
			Me.radMenuItem1_3.AccessibleDescription = "Add"
			Me.radMenuItem1_3.AccessibleName = "Add"
			Me.radMenuItem1_3.Name = "radMenuItem1_3"
			Me.radMenuItem1_3.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem1_3.Text = "Add"
			Me.radMenuItem1_3.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem2
			' 
			Me.radMenuSeparatorItem2.Name = "radMenuSeparatorItem2"
			Me.radMenuSeparatorItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_4
			' 
			Me.radMenuItem1_4.AccessibleDescription = "Close"
			Me.radMenuItem1_4.AccessibleName = "Close"
			Me.radMenuItem1_4.Image = (CType(resources.GetObject("radMenuItem1_4.Image"), Image))
			Me.radMenuItem1_4.ImageIndex = 0
			Me.radMenuItem1_4.Name = "radMenuItem1_4"
			Me.radMenuItem1_4.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem1_4.Text = "Close"
			Me.radMenuItem1_4.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_5
			' 
			Me.radMenuItem1_5.AccessibleDescription = "Close Solution"
			Me.radMenuItem1_5.AccessibleName = "Close Solution"
			Me.radMenuItem1_5.Image = (CType(resources.GetObject("radMenuItem1_5.Image"), Image))
			Me.radMenuItem1_5.ImageIndex = 5
			Me.radMenuItem1_5.Name = "radMenuItem1_5"
			Me.radMenuItem1_5.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem1_5.Text = "Close Solution"
			Me.radMenuItem1_5.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem3
			' 
			Me.radMenuSeparatorItem3.Name = "radMenuSeparatorItem3"
			Me.radMenuSeparatorItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_6
			' 
			Me.radMenuItem1_6.AccessibleDescription = "Advanced Save Options..."
			Me.radMenuItem1_6.AccessibleName = "Advanced Save Options..."
			Me.radMenuItem1_6.Name = "radMenuItem1_6"
			Me.radMenuItem1_6.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem1_6.Text = "Advanced Save Options..."
			Me.radMenuItem1_6.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_7
			' 
			Me.radMenuItem1_7.AccessibleDescription = "Save All"
			Me.radMenuItem1_7.AccessibleName = "Save All"
			Me.radMenuItem1_7.Image = (CType(resources.GetObject("radMenuItem1_7.Image"), Image))
			Me.radMenuItem1_7.ImageIndex = 0
			Me.radMenuItem1_7.Name = "radMenuItem1_7"
			Me.radMenuItem1_7.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem1_7.Text = "Save All"
			Me.radMenuItem1_7.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_7.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem4
			' 
			Me.radMenuSeparatorItem4.Name = "radMenuSeparatorItem4"
			Me.radMenuSeparatorItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem1_9
			' 
			Me.radMenuItem1_9.AccessibleDescription = "Source Control"
			Me.radMenuItem1_9.AccessibleName = "Source Control"
			Me.radMenuItem1_9.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuItemSrc_1, Me.radMenuItemSrc_2, Me.radMenuItemSrc_3})
			Me.radMenuItem1_9.Name = "radMenuItem1_9"
			Me.radMenuItem1_9.PopupDirection = Telerik.WinControls.UI.RadDirection.Right
			Me.radMenuItem1_9.Text = "Source Control"
			Me.radMenuItem1_9.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem1_9.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItemSrc_1
			' 
			Me.radMenuItemSrc_1.AccessibleDescription = "View History"
			Me.radMenuItemSrc_1.AccessibleName = "View History"
			Me.radMenuItemSrc_1.Name = "radMenuItemSrc_1"
			Me.radMenuItemSrc_1.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItemSrc_1.Text = "View History"
			Me.radMenuItemSrc_1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItemSrc_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItemSrc_2
			' 
			Me.radMenuItemSrc_2.AccessibleDescription = "Refresh Status"
			Me.radMenuItemSrc_2.AccessibleName = "Refresh Status"
			Me.radMenuItemSrc_2.Name = "radMenuItemSrc_2"
			Me.radMenuItemSrc_2.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItemSrc_2.Text = "Refresh Status"
			Me.radMenuItemSrc_2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItemSrc_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItemSrc_3
			' 
			Me.radMenuItemSrc_3.AccessibleDescription = "Compare..."
			Me.radMenuItemSrc_3.AccessibleName = "Compare..."
			Me.radMenuItemSrc_3.Name = "radMenuItemSrc_3"
			Me.radMenuItemSrc_3.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItemSrc_3.Text = "Compare..."
			Me.radMenuItemSrc_3.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItemSrc_3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem6
			' 
			Me.radMenuSeparatorItem6.Name = "radMenuSeparatorItem6"
			Me.radMenuSeparatorItem6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuComboItem1
			' 
			' 
			' 
			' 
			Me.radMenuComboItem1.ComboBoxElement.ArrowButtonMinWidth = 16
			Me.radMenuComboItem1.ComboBoxElement.AutoCompleteAppend = Nothing
			Me.radMenuComboItem1.ComboBoxElement.AutoCompleteDataSource = Nothing
			Me.radMenuComboItem1.ComboBoxElement.AutoCompleteDisplayMember = Nothing
			Me.radMenuComboItem1.ComboBoxElement.AutoCompleteSuggest = Nothing
			Me.radMenuComboItem1.ComboBoxElement.AutoCompleteValueMember = Nothing
			Me.radMenuComboItem1.ComboBoxElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize
			Me.radMenuComboItem1.ComboBoxElement.DataMember = ""
			Me.radMenuComboItem1.ComboBoxElement.DataSource = Nothing
			Me.radMenuComboItem1.ComboBoxElement.DefaultItemsCountInDropDown = 6
			Me.radMenuComboItem1.ComboBoxElement.DefaultValue = Nothing
			Me.radMenuComboItem1.ComboBoxElement.DisplayMember = ""
			Me.radMenuComboItem1.ComboBoxElement.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad
			Me.radMenuComboItem1.ComboBoxElement.DropDownAnimationEnabled = True
			Me.radMenuComboItem1.ComboBoxElement.DropDownMinSize = New Size(250, 0)
			Me.radMenuComboItem1.ComboBoxElement.DropDownSizingMode = (CType((Telerik.WinControls.UI.SizingMode.RightBottom Or Telerik.WinControls.UI.SizingMode.UpDown), Telerik.WinControls.UI.SizingMode))
			Me.radMenuComboItem1.ComboBoxElement.DropDownWidth = 250
			Me.radMenuComboItem1.ComboBoxElement.EditableElementText = ""
			Me.radMenuComboItem1.ComboBoxElement.EditorElement = Me.radMenuComboItem1.ComboBoxElement
			Me.radMenuComboItem1.ComboBoxElement.EditorManager = Nothing
			Me.radMenuComboItem1.ComboBoxElement.Filter = Nothing
			Me.radMenuComboItem1.ComboBoxElement.FilterExpression = ""
			Me.radMenuComboItem1.ComboBoxElement.Focusable = True
			Me.radMenuComboItem1.ComboBoxElement.FormatString = ""
			Me.radMenuComboItem1.ComboBoxElement.FormattingEnabled = True
			Me.radMenuComboItem1.ComboBoxElement.ItemHeight = 18
			radListDataItem19.Text = "1 c:\Projects\Form1.cs"
			radListDataItem19.TextWrap = True
			radListDataItem20.Text = "2 c:\Projects\RadMenu.cs"
			radListDataItem20.TextWrap = True
			radListDataItem21.Text = "3 c:\Projects\RadComboBox.cs"
			radListDataItem21.TextWrap = True
			radListDataItem22.Text = "4 c:\Projects\Program.cs"
			radListDataItem22.TextWrap = True
			radListDataItem23.Text = "5 c:\Projects\MyTheme.xml"
			radListDataItem23.TextWrap = True
			radListDataItem24.Text = "6 c:\Projects\Form2.cs"
			radListDataItem24.TextWrap = True
			radListDataItem25.Text = "7 C:\Projects\Items\RadComboBoxItem.cs"
			radListDataItem25.TextWrap = True
			Me.radMenuComboItem1.ComboBoxElement.Items.Add(radListDataItem19)
			Me.radMenuComboItem1.ComboBoxElement.Items.Add(radListDataItem20)
			Me.radMenuComboItem1.ComboBoxElement.Items.Add(radListDataItem21)
			Me.radMenuComboItem1.ComboBoxElement.Items.Add(radListDataItem22)
			Me.radMenuComboItem1.ComboBoxElement.Items.Add(radListDataItem23)
			Me.radMenuComboItem1.ComboBoxElement.Items.Add(radListDataItem24)
			Me.radMenuComboItem1.ComboBoxElement.Items.Add(radListDataItem25)
			Me.radMenuComboItem1.ComboBoxElement.MaxDropDownItems = 0
			Me.radMenuComboItem1.ComboBoxElement.MaxLength = 32767
			Me.radMenuComboItem1.ComboBoxElement.MaxValue = Nothing
			Me.radMenuComboItem1.ComboBoxElement.MinSize = New Size(100, 20)
			Me.radMenuComboItem1.ComboBoxElement.MinValue = Nothing
			Me.radMenuComboItem1.ComboBoxElement.NullText = "Select recent file..."
			Me.radMenuComboItem1.ComboBoxElement.NullValue = Nothing
			Me.radMenuComboItem1.ComboBoxElement.OwnerOffset = 0
			Me.radMenuComboItem1.ComboBoxElement.ShowImageInEditorArea = True
			Me.radMenuComboItem1.ComboBoxElement.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None
			Me.radMenuComboItem1.ComboBoxElement.Value = Nothing
			Me.radMenuComboItem1.ComboBoxElement.ValueMember = ""
			Me.radMenuComboItem1.Name = "radMenuComboItem1"
			Me.radMenuComboItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			CType(Me.radMenuComboItem1.GetChildAt(3), Telerik.WinControls.UI.RadDropDownListElement).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize
			CType(Me.radMenuComboItem1.GetChildAt(3), Telerik.WinControls.UI.RadDropDownListElement).MinSize = New Size(100, 20)
			' 
			' radMenuComboItem2
			' 
			Me.radMenuComboItem2.AccessibleDescription = "New item"
			Me.radMenuComboItem2.AccessibleName = "New item"
			' 
			' 
			' 
			Me.radMenuComboItem2.ComboBoxElement.ArrowButtonMinWidth = 16
			Me.radMenuComboItem2.ComboBoxElement.AutoCompleteAppend = Nothing
			Me.radMenuComboItem2.ComboBoxElement.AutoCompleteDataSource = Nothing
			Me.radMenuComboItem2.ComboBoxElement.AutoCompleteDisplayMember = Nothing
			Me.radMenuComboItem2.ComboBoxElement.AutoCompleteSuggest = Nothing
			Me.radMenuComboItem2.ComboBoxElement.AutoCompleteValueMember = Nothing
			Me.radMenuComboItem2.ComboBoxElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize
			Me.radMenuComboItem2.ComboBoxElement.DataMember = ""
			Me.radMenuComboItem2.ComboBoxElement.DataSource = Nothing
			Me.radMenuComboItem2.ComboBoxElement.DefaultItemsCountInDropDown = 6
			Me.radMenuComboItem2.ComboBoxElement.DefaultValue = Nothing
			Me.radMenuComboItem2.ComboBoxElement.DisplayMember = ""
			Me.radMenuComboItem2.ComboBoxElement.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad
			Me.radMenuComboItem2.ComboBoxElement.DropDownAnimationEnabled = True
			Me.radMenuComboItem2.ComboBoxElement.DropDownMinSize = New Size(250, 0)
			Me.radMenuComboItem2.ComboBoxElement.DropDownSizingMode = (CType((Telerik.WinControls.UI.SizingMode.RightBottom Or Telerik.WinControls.UI.SizingMode.UpDown), Telerik.WinControls.UI.SizingMode))
			Me.radMenuComboItem2.ComboBoxElement.DropDownWidth = 250
			Me.radMenuComboItem2.ComboBoxElement.EditableElementText = ""
			Me.radMenuComboItem2.ComboBoxElement.EditorElement = Me.radMenuComboItem2.ComboBoxElement
			Me.radMenuComboItem2.ComboBoxElement.EditorManager = Nothing
			Me.radMenuComboItem2.ComboBoxElement.Filter = Nothing
			Me.radMenuComboItem2.ComboBoxElement.FilterExpression = ""
			Me.radMenuComboItem2.ComboBoxElement.Focusable = True
			Me.radMenuComboItem2.ComboBoxElement.FormatString = ""
			Me.radMenuComboItem2.ComboBoxElement.FormattingEnabled = True
			Me.radMenuComboItem2.ComboBoxElement.ItemHeight = 18
			radListDataItem1.Text = "1 C:\DemoApplication\DemoApplication.sln"
			radListDataItem1.TextWrap = True
			radListDataItem2.Text = "2 C:\TestSolution1\TestSolution1.sln"
			radListDataItem2.TextWrap = True
			radListDataItem3.Text = "3 C:\WindowsApplication1.csproj"
			radListDataItem3.TextWrap = True
			Me.radMenuComboItem2.ComboBoxElement.Items.Add(radListDataItem1)
			Me.radMenuComboItem2.ComboBoxElement.Items.Add(radListDataItem2)
			Me.radMenuComboItem2.ComboBoxElement.Items.Add(radListDataItem3)
			Me.radMenuComboItem2.ComboBoxElement.MaxDropDownItems = 0
			Me.radMenuComboItem2.ComboBoxElement.MaxLength = 32767
			Me.radMenuComboItem2.ComboBoxElement.MaxValue = Nothing
			Me.radMenuComboItem2.ComboBoxElement.MinSize = New Size(100, 20)
			Me.radMenuComboItem2.ComboBoxElement.MinValue = Nothing
			Me.radMenuComboItem2.ComboBoxElement.NullText = "Select recent project..."
			Me.radMenuComboItem2.ComboBoxElement.NullValue = Nothing
			Me.radMenuComboItem2.ComboBoxElement.OwnerOffset = 0
			Me.radMenuComboItem2.ComboBoxElement.ShowImageInEditorArea = True
			Me.radMenuComboItem2.ComboBoxElement.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None
			Me.radMenuComboItem2.ComboBoxElement.Value = Nothing
			Me.radMenuComboItem2.ComboBoxElement.ValueMember = ""
			Me.radMenuComboItem2.Name = "radMenuComboItem2"
			Me.radMenuComboItem2.Text = "New item"
			Me.radMenuComboItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			CType(Me.radMenuComboItem2.GetChildAt(3), Telerik.WinControls.UI.RadDropDownListElement).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize
			CType(Me.radMenuComboItem2.GetChildAt(3), Telerik.WinControls.UI.RadDropDownListElement).MinSize = New Size(100, 20)
			' 
			' radMenuItem2
			' 
			Me.radMenuItem2.AccessibleDescription = "Edit"
			Me.radMenuItem2.AccessibleName = "Edit"
			Me.radMenuItem2.ClickMode = Telerik.WinControls.ClickMode.Press
			Me.radMenuItem2.Items.AddRange(New Telerik.WinControls.RadItem() { Me.radMenuComboItem3, Me.radMenuComboItem4, Me.radMenuSeparatorItem8, Me.radMenuItem2_3, Me.radMenuItem2_4, Me.radMenuItem2_5, Me.radMenuItem2_6, Me.radMenuItem2_7, Me.radMenuSeparatorItem9, Me.radMenuItem2_8})
			Me.radMenuItem2.Name = "radMenuItem2"
			Me.radMenuItem2.PopupDirection = Telerik.WinControls.UI.RadDirection.Down
			Me.radMenuItem2.ShowArrow = False
			Me.radMenuItem2.Text = "Edit"
			Me.radMenuItem2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuComboItem3
			' 
			Me.radMenuComboItem3.AccessibleDescription = "Undo"
			Me.radMenuComboItem3.AccessibleName = "Undo"
			' 
			' 
			' 
			Me.radMenuComboItem3.ComboBoxElement.ArrowButtonMinWidth = 16
			Me.radMenuComboItem3.ComboBoxElement.AutoCompleteAppend = Nothing
			Me.radMenuComboItem3.ComboBoxElement.AutoCompleteDataSource = Nothing
			Me.radMenuComboItem3.ComboBoxElement.AutoCompleteDisplayMember = Nothing
			Me.radMenuComboItem3.ComboBoxElement.AutoCompleteSuggest = Nothing
			Me.radMenuComboItem3.ComboBoxElement.AutoCompleteValueMember = Nothing
			Me.radMenuComboItem3.ComboBoxElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize
			Me.radMenuComboItem3.ComboBoxElement.DataMember = ""
			Me.radMenuComboItem3.ComboBoxElement.DataSource = Nothing
			Me.radMenuComboItem3.ComboBoxElement.DefaultItemsCountInDropDown = 6
			Me.radMenuComboItem3.ComboBoxElement.DefaultValue = Nothing
			Me.radMenuComboItem3.ComboBoxElement.DisplayMember = ""
			Me.radMenuComboItem3.ComboBoxElement.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad
			Me.radMenuComboItem3.ComboBoxElement.DropDownAnimationEnabled = True
			Me.radMenuComboItem3.ComboBoxElement.DropDownSizingMode = (CType((Telerik.WinControls.UI.SizingMode.RightBottom Or Telerik.WinControls.UI.SizingMode.UpDown), Telerik.WinControls.UI.SizingMode))
			Me.radMenuComboItem3.ComboBoxElement.EditableElementText = ""
			Me.radMenuComboItem3.ComboBoxElement.EditorElement = Me.radMenuComboItem3.ComboBoxElement
			Me.radMenuComboItem3.ComboBoxElement.EditorManager = Nothing
			Me.radMenuComboItem3.ComboBoxElement.Filter = Nothing
			Me.radMenuComboItem3.ComboBoxElement.FilterExpression = ""
			Me.radMenuComboItem3.ComboBoxElement.Focusable = True
			Me.radMenuComboItem3.ComboBoxElement.FormatString = ""
			Me.radMenuComboItem3.ComboBoxElement.FormattingEnabled = True
			Me.radMenuComboItem3.ComboBoxElement.ItemHeight = 18
			radListDataItem4.Text = "Copy text"
			radListDataItem4.TextWrap = True
			radListDataItem5.Text = " Type ""This is my.."""
			radListDataItem5.TextWrap = True
			radListDataItem6.Text = "Font color"
			radListDataItem6.TextWrap = True
			radListDataItem7.Text = " Font size"
			radListDataItem7.TextWrap = True
			Me.radMenuComboItem3.ComboBoxElement.Items.Add(radListDataItem4)
			Me.radMenuComboItem3.ComboBoxElement.Items.Add(radListDataItem5)
			Me.radMenuComboItem3.ComboBoxElement.Items.Add(radListDataItem6)
			Me.radMenuComboItem3.ComboBoxElement.Items.Add(radListDataItem7)
			Me.radMenuComboItem3.ComboBoxElement.MaxDropDownItems = 0
			Me.radMenuComboItem3.ComboBoxElement.MaxLength = 32767
			Me.radMenuComboItem3.ComboBoxElement.MaxValue = Nothing
			Me.radMenuComboItem3.ComboBoxElement.MinSize = New Size(100, 20)
			Me.radMenuComboItem3.ComboBoxElement.MinValue = Nothing
			Me.radMenuComboItem3.ComboBoxElement.NullValue = Nothing
			Me.radMenuComboItem3.ComboBoxElement.OwnerOffset = 0
			Me.radMenuComboItem3.ComboBoxElement.ShowImageInEditorArea = True
			Me.radMenuComboItem3.ComboBoxElement.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None
			Me.radMenuComboItem3.ComboBoxElement.Value = Nothing
			Me.radMenuComboItem3.ComboBoxElement.ValueMember = ""
			Me.radMenuComboItem3.Name = "radMenuComboItem3"
			Me.radMenuComboItem3.Text = "Undo"
			Me.radMenuComboItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			CType(Me.radMenuComboItem3.GetChildAt(3), Telerik.WinControls.UI.RadDropDownListElement).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize
			CType(Me.radMenuComboItem3.GetChildAt(3), Telerik.WinControls.UI.RadDropDownListElement).MinSize = New Size(100, 20)
			' 
			' radMenuComboItem4
			' 
			Me.radMenuComboItem4.AccessibleDescription = "Redo"
			Me.radMenuComboItem4.AccessibleName = "Redo"
			' 
			' 
			' 
			Me.radMenuComboItem4.ComboBoxElement.ArrowButtonMinWidth = 16
			Me.radMenuComboItem4.ComboBoxElement.AutoCompleteAppend = Nothing
			Me.radMenuComboItem4.ComboBoxElement.AutoCompleteDataSource = Nothing
			Me.radMenuComboItem4.ComboBoxElement.AutoCompleteDisplayMember = Nothing
			Me.radMenuComboItem4.ComboBoxElement.AutoCompleteSuggest = Nothing
			Me.radMenuComboItem4.ComboBoxElement.AutoCompleteValueMember = Nothing
			Me.radMenuComboItem4.ComboBoxElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize
			Me.radMenuComboItem4.ComboBoxElement.DataMember = ""
			Me.radMenuComboItem4.ComboBoxElement.DataSource = Nothing
			Me.radMenuComboItem4.ComboBoxElement.DefaultItemsCountInDropDown = 6
			Me.radMenuComboItem4.ComboBoxElement.DefaultValue = Nothing
			Me.radMenuComboItem4.ComboBoxElement.DisplayMember = ""
			Me.radMenuComboItem4.ComboBoxElement.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad
			Me.radMenuComboItem4.ComboBoxElement.DropDownAnimationEnabled = True
			Me.radMenuComboItem4.ComboBoxElement.DropDownSizingMode = (CType((Telerik.WinControls.UI.SizingMode.RightBottom Or Telerik.WinControls.UI.SizingMode.UpDown), Telerik.WinControls.UI.SizingMode))
			Me.radMenuComboItem4.ComboBoxElement.EditableElementText = ""
			Me.radMenuComboItem4.ComboBoxElement.EditorElement = Me.radMenuComboItem4.ComboBoxElement
			Me.radMenuComboItem4.ComboBoxElement.EditorManager = Nothing
			Me.radMenuComboItem4.ComboBoxElement.Filter = Nothing
			Me.radMenuComboItem4.ComboBoxElement.FilterExpression = ""
			Me.radMenuComboItem4.ComboBoxElement.Focusable = True
			Me.radMenuComboItem4.ComboBoxElement.FormatString = ""
			Me.radMenuComboItem4.ComboBoxElement.FormattingEnabled = True
			Me.radMenuComboItem4.ComboBoxElement.ItemHeight = 18
			radListDataItem8.Text = "Format paragraph"
			radListDataItem8.TextWrap = True
			radListDataItem9.Text = "Ordered list"
			radListDataItem9.TextWrap = True
			radListDataItem10.Text = "Page Break"
			radListDataItem10.TextWrap = True
			radListDataItem11.Text = "Rotate"
			radListDataItem11.TextWrap = True
			Me.radMenuComboItem4.ComboBoxElement.Items.Add(radListDataItem8)
			Me.radMenuComboItem4.ComboBoxElement.Items.Add(radListDataItem9)
			Me.radMenuComboItem4.ComboBoxElement.Items.Add(radListDataItem10)
			Me.radMenuComboItem4.ComboBoxElement.Items.Add(radListDataItem11)
			Me.radMenuComboItem4.ComboBoxElement.MaxDropDownItems = 0
			Me.radMenuComboItem4.ComboBoxElement.MaxLength = 32767
			Me.radMenuComboItem4.ComboBoxElement.MaxValue = Nothing
			Me.radMenuComboItem4.ComboBoxElement.MinSize = New Size(100, 20)
			Me.radMenuComboItem4.ComboBoxElement.MinValue = Nothing
			Me.radMenuComboItem4.ComboBoxElement.NullValue = Nothing
			Me.radMenuComboItem4.ComboBoxElement.OwnerOffset = 0
			Me.radMenuComboItem4.ComboBoxElement.ShowImageInEditorArea = True
			Me.radMenuComboItem4.ComboBoxElement.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None
			Me.radMenuComboItem4.ComboBoxElement.Value = Nothing
			Me.radMenuComboItem4.ComboBoxElement.ValueMember = ""
			Me.radMenuComboItem4.Name = "radMenuComboItem4"
			Me.radMenuComboItem4.Text = "Redo"
			Me.radMenuComboItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			CType(Me.radMenuComboItem4.GetChildAt(3), Telerik.WinControls.UI.RadDropDownListElement).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.FitToAvailableSize
			CType(Me.radMenuComboItem4.GetChildAt(3), Telerik.WinControls.UI.RadDropDownListElement).MinSize = New Size(100, 20)
			' 
			' radMenuSeparatorItem8
			' 
			Me.radMenuSeparatorItem8.Name = "radMenuSeparatorItem8"
			Me.radMenuSeparatorItem8.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_3
			' 
			Me.radMenuItem2_3.AccessibleDescription = "Cut"
			Me.radMenuItem2_3.AccessibleName = "Cut"
			Me.radMenuItem2_3.Name = "radMenuItem2_3"
			Me.radMenuItem2_3.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem2_3.Text = "Cut"
			Me.radMenuItem2_3.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_4
			' 
			Me.radMenuItem2_4.AccessibleDescription = "Copy"
			Me.radMenuItem2_4.AccessibleName = "Copy"
			Me.radMenuItem2_4.Name = "radMenuItem2_4"
			Me.radMenuItem2_4.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem2_4.Text = "Copy"
			Me.radMenuItem2_4.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_5
			' 
			Me.radMenuItem2_5.AccessibleDescription = "Paste"
			Me.radMenuItem2_5.AccessibleName = "Paste"
			Me.radMenuItem2_5.Name = "radMenuItem2_5"
			Me.radMenuItem2_5.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem2_5.Text = "Paste"
			Me.radMenuItem2_5.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_6
			' 
			Me.radMenuItem2_6.AccessibleDescription = "Cycle Clipboard Ring"
			Me.radMenuItem2_6.AccessibleName = "Cycle Clipboard Ring"
			Me.radMenuItem2_6.Name = "radMenuItem2_6"
			Me.radMenuItem2_6.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem2_6.Text = "Cycle Clipboard Ring"
			Me.radMenuItem2_6.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_7
			' 
			Me.radMenuItem2_7.AccessibleDescription = "Delete"
			Me.radMenuItem2_7.AccessibleName = "Delete"
			Me.radMenuItem2_7.Name = "radMenuItem2_7"
			Me.radMenuItem2_7.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem2_7.Text = "Delete"
			Me.radMenuItem2_7.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_7.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuSeparatorItem9
			' 
			Me.radMenuSeparatorItem9.Name = "radMenuSeparatorItem9"
			Me.radMenuSeparatorItem9.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem2_8
			' 
			Me.radMenuItem2_8.AccessibleDescription = "Select All"
			Me.radMenuItem2_8.AccessibleName = "Select All"
			Me.radMenuItem2_8.Name = "radMenuItem2_8"
			Me.radMenuItem2_8.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem2_8.Text = "Select All"
			Me.radMenuItem2_8.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem2_8.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_1
			' 
			Me.radMenuItem3_1.AccessibleDescription = "Code"
			Me.radMenuItem3_1.AccessibleName = "Code"
			Me.radMenuItem3_1.Name = "radMenuItem3_1"
			Me.radMenuItem3_1.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_1.Text = "Code"
			Me.radMenuItem3_1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_2
			' 
			Me.radMenuItem3_2.AccessibleDescription = "Open"
			Me.radMenuItem3_2.AccessibleName = "Open"
			Me.radMenuItem3_2.Name = "radMenuItem3_2"
			Me.radMenuItem3_2.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_2.Text = "Open"
			Me.radMenuItem3_2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_3
			' 
			Me.radMenuItem3_3.AccessibleDescription = "Open With..."
			Me.radMenuItem3_3.AccessibleName = "Open With..."
			Me.radMenuItem3_3.Name = "radMenuItem3_3"
			Me.radMenuItem3_3.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_3.Text = "Open With..."
			Me.radMenuItem3_3.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_4
			' 
			Me.radMenuItem3_4.AccessibleDescription = "Server Explorer"
			Me.radMenuItem3_4.AccessibleName = "Server Explorer"
			Me.radMenuItem3_4.Name = "radMenuItem3_4"
			Me.radMenuItem3_4.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_4.Text = "Server Explorer"
			Me.radMenuItem3_4.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_5
			' 
			Me.radMenuItem3_5.AccessibleDescription = "Solution Explorer"
			Me.radMenuItem3_5.AccessibleName = "Solution Explorer"
			Me.radMenuItem3_5.Name = "radMenuItem3_5"
			Me.radMenuItem3_5.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_5.Text = "Solution Explorer"
			Me.radMenuItem3_5.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_6
			' 
			Me.radMenuItem3_6.AccessibleDescription = "Bookmark Window"
			Me.radMenuItem3_6.AccessibleName = "Bookmark Window"
			Me.radMenuItem3_6.Name = "radMenuItem3_6"
			Me.radMenuItem3_6.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_6.Text = "Bookmark Window"
			Me.radMenuItem3_6.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_7
			' 
			Me.radMenuItem3_7.AccessibleDescription = "Class View"
			Me.radMenuItem3_7.AccessibleName = "Class View"
			Me.radMenuItem3_7.Name = "radMenuItem3_7"
			Me.radMenuItem3_7.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_7.Text = "Class View"
			Me.radMenuItem3_7.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_7.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_8
			' 
			Me.radMenuItem3_8.AccessibleDescription = "Code Definition Window"
			Me.radMenuItem3_8.AccessibleName = "Code Definition Window"
			Me.radMenuItem3_8.Name = "radMenuItem3_8"
			Me.radMenuItem3_8.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_8.Text = "Code Definition Window"
			Me.radMenuItem3_8.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_8.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_9
			' 
			Me.radMenuItem3_9.AccessibleDescription = "Object Browser"
			Me.radMenuItem3_9.AccessibleName = "Object Browser"
			Me.radMenuItem3_9.Name = "radMenuItem3_9"
			Me.radMenuItem3_9.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_9.Text = "Object Browser"
			Me.radMenuItem3_9.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_9.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_10
			' 
			Me.radMenuItem3_10.AccessibleDescription = "Error List"
			Me.radMenuItem3_10.AccessibleName = "Error List"
			Me.radMenuItem3_10.Name = "radMenuItem3_10"
			Me.radMenuItem3_10.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_10.Text = "Error List"
			Me.radMenuItem3_10.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_10.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_11
			' 
			Me.radMenuItem3_11.AccessibleDescription = "Output"
			Me.radMenuItem3_11.AccessibleName = "Output"
			Me.radMenuItem3_11.Name = "radMenuItem3_11"
			Me.radMenuItem3_11.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_11.Text = "Output"
			Me.radMenuItem3_11.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_11.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_12
			' 
			Me.radMenuItem3_12.AccessibleDescription = "Properties Window"
			Me.radMenuItem3_12.AccessibleName = "Properties Window"
			Me.radMenuItem3_12.Name = "radMenuItem3_12"
			Me.radMenuItem3_12.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_12.Text = "Properties Window"
			Me.radMenuItem3_12.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_12.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_13
			' 
			Me.radMenuItem3_13.AccessibleDescription = "Task List"
			Me.radMenuItem3_13.AccessibleName = "Task List"
			Me.radMenuItem3_13.Name = "radMenuItem3_13"
			Me.radMenuItem3_13.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_13.Text = "Task List"
			Me.radMenuItem3_13.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_13.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_14
			' 
			Me.radMenuItem3_14.AccessibleDescription = "Toolbox"
			Me.radMenuItem3_14.AccessibleName = "Toolbox"
			Me.radMenuItem3_14.Name = "radMenuItem3_14"
			Me.radMenuItem3_14.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_14.Text = "Toolbox"
			Me.radMenuItem3_14.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_14.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_15
			' 
			Me.radMenuItem3_15.AccessibleDescription = "Find Results"
			Me.radMenuItem3_15.AccessibleName = "Find Results"
			Me.radMenuItem3_15.Name = "radMenuItem3_15"
			Me.radMenuItem3_15.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_15.Text = "Find Results"
			Me.radMenuItem3_15.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_15.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_16
			' 
			Me.radMenuItem3_16.AccessibleDescription = "Other Windows"
			Me.radMenuItem3_16.AccessibleName = "Other Windows"
			Me.radMenuItem3_16.Name = "radMenuItem3_16"
			Me.radMenuItem3_16.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_16.Text = "Other Windows"
			Me.radMenuItem3_16.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_16.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_17
			' 
			Me.radMenuItem3_17.AccessibleDescription = "Toolbars"
			Me.radMenuItem3_17.AccessibleName = "Toolbars"
			Me.radMenuItem3_17.Name = "radMenuItem3_17"
			Me.radMenuItem3_17.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_17.Text = "Toolbars"
			Me.radMenuItem3_17.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_17.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_18
			' 
			Me.radMenuItem3_18.AccessibleDescription = "Full Screen"
			Me.radMenuItem3_18.AccessibleName = "Full Screen"
			Me.radMenuItem3_18.Name = "radMenuItem3_18"
			Me.radMenuItem3_18.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_18.Text = "Full Screen"
			Me.radMenuItem3_18.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_18.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_19
			' 
			Me.radMenuItem3_19.AccessibleDescription = "Pending Checkins"
			Me.radMenuItem3_19.AccessibleName = "Pending Checkins"
			Me.radMenuItem3_19.Name = "radMenuItem3_19"
			Me.radMenuItem3_19.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_19.Text = "Pending Checkins"
			Me.radMenuItem3_19.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_19.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_20
			' 
			Me.radMenuItem3_20.AccessibleDescription = "Navigate Backward"
			Me.radMenuItem3_20.AccessibleName = "Navigate Backward"
			Me.radMenuItem3_20.Name = "radMenuItem3_20"
			Me.radMenuItem3_20.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_20.Text = "Navigate Backward"
			Me.radMenuItem3_20.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_20.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem3_21
			' 
			Me.radMenuItem3_21.AccessibleDescription = "Navigate Forward"
			Me.radMenuItem3_21.AccessibleName = "Navigate Forward"
			Me.radMenuItem3_21.Name = "radMenuItem3_21"
			Me.radMenuItem3_21.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem3_21.Text = "Navigate Forward"
			Me.radMenuItem3_21.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem3_21.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem4_1
			' 
			Me.radMenuItem4_1.AccessibleDescription = "Build Solution"
			Me.radMenuItem4_1.AccessibleName = "Build Solution"
			Me.radMenuItem4_1.Name = "radMenuItem4_1"
			Me.radMenuItem4_1.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem4_1.Text = "Build Solution"
			Me.radMenuItem4_1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem4_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem4_2
			' 
			Me.radMenuItem4_2.AccessibleDescription = "Build Examples"
			Me.radMenuItem4_2.AccessibleName = "Build Examples"
			Me.radMenuItem4_2.Name = "radMenuItem4_2"
			Me.radMenuItem4_2.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem4_2.Text = "Build Examples"
			Me.radMenuItem4_2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem4_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem4_3
			' 
			Me.radMenuItem4_3.AccessibleDescription = "Publish Examples"
			Me.radMenuItem4_3.AccessibleName = "Publish Examples"
			Me.radMenuItem4_3.Name = "radMenuItem4_3"
			Me.radMenuItem4_3.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem4_3.Text = "Publish Examples"
			Me.radMenuItem4_3.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem4_3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem5_1
			' 
			Me.radMenuItem5_1.AccessibleDescription = "Show Data Sources"
			Me.radMenuItem5_1.AccessibleName = "Show Data Sources"
			Me.radMenuItem5_1.Name = "radMenuItem5_1"
			Me.radMenuItem5_1.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem5_1.Text = "Show Data Sources"
			Me.radMenuItem5_1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem5_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem5_2
			' 
			Me.radMenuItem5_2.AccessibleDescription = "Add New Data Source..."
			Me.radMenuItem5_2.AccessibleName = "Add New Data Source..."
			Me.radMenuItem5_2.Name = "radMenuItem5_2"
			Me.radMenuItem5_2.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem5_2.Text = "Add New Data Source..."
			Me.radMenuItem5_2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem5_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_1
			' 
			Me.radMenuItem6_1.AccessibleDescription = "How Do I"
			Me.radMenuItem6_1.AccessibleName = "How Do I"
			Me.radMenuItem6_1.Name = "radMenuItem6_1"
			Me.radMenuItem6_1.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem6_1.Text = "How Do I"
			Me.radMenuItem6_1.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_1.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_2
			' 
			Me.radMenuItem6_2.AccessibleDescription = "Search"
			Me.radMenuItem6_2.AccessibleName = "Search"
			Me.radMenuItem6_2.Name = "radMenuItem6_2"
			Me.radMenuItem6_2.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem6_2.Text = "Search"
			Me.radMenuItem6_2.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_2.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_3
			' 
			Me.radMenuItem6_3.AccessibleDescription = "Contents"
			Me.radMenuItem6_3.AccessibleName = "Contents"
			Me.radMenuItem6_3.Name = "radMenuItem6_3"
			Me.radMenuItem6_3.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem6_3.Text = "Contents"
			Me.radMenuItem6_3.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_3.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_4
			' 
			Me.radMenuItem6_4.AccessibleDescription = "Index"
			Me.radMenuItem6_4.AccessibleName = "Index"
			Me.radMenuItem6_4.Name = "radMenuItem6_4"
			Me.radMenuItem6_4.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem6_4.Text = "Index"
			Me.radMenuItem6_4.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_4.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_5
			' 
			Me.radMenuItem6_5.AccessibleDescription = "Help Favorites"
			Me.radMenuItem6_5.AccessibleName = "Help Favorites"
			Me.radMenuItem6_5.Name = "radMenuItem6_5"
			Me.radMenuItem6_5.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem6_5.Text = "Help Favorites"
			Me.radMenuItem6_5.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_5.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_6
			' 
			Me.radMenuItem6_6.AccessibleDescription = "Dynamic Help"
			Me.radMenuItem6_6.AccessibleName = "Dynamic Help"
			Me.radMenuItem6_6.Name = "radMenuItem6_6"
			Me.radMenuItem6_6.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem6_6.Text = "Dynamic Help"
			Me.radMenuItem6_6.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_6.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_7
			' 
			Me.radMenuItem6_7.AccessibleDescription = "Index Results"
			Me.radMenuItem6_7.AccessibleName = "Index Results"
			Me.radMenuItem6_7.Name = "radMenuItem6_7"
			Me.radMenuItem6_7.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem6_7.Text = "Index Results"
			Me.radMenuItem6_7.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_7.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_8
			' 
			Me.radMenuItem6_8.AccessibleDescription = "Customer Feedback Options..."
			Me.radMenuItem6_8.AccessibleName = "Customer Feedback Options..."
			Me.radMenuItem6_8.Name = "radMenuItem6_8"
			Me.radMenuItem6_8.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem6_8.Text = "Customer Feedback Options..."
			Me.radMenuItem6_8.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_8.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_9
			' 
			Me.radMenuItem6_9.AccessibleDescription = "Register Product..."
			Me.radMenuItem6_9.AccessibleName = "Register Product..."
			Me.radMenuItem6_9.Name = "radMenuItem6_9"
			Me.radMenuItem6_9.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem6_9.Text = "Register Product..."
			Me.radMenuItem6_9.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_9.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_10
			' 
			Me.radMenuItem6_10.AccessibleDescription = "Check for Update"
			Me.radMenuItem6_10.AccessibleName = "Check for Update"
			Me.radMenuItem6_10.Name = "radMenuItem6_10"
			Me.radMenuItem6_10.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem6_10.Text = "Check for Update"
			Me.radMenuItem6_10.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_10.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_11
			' 
			Me.radMenuItem6_11.AccessibleDescription = "Technical Support"
			Me.radMenuItem6_11.AccessibleName = "Technical Support"
			Me.radMenuItem6_11.Name = "radMenuItem6_11"
			Me.radMenuItem6_11.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem6_11.Text = "Technical Support"
			Me.radMenuItem6_11.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_11.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radMenuItem6_12
			' 
			Me.radMenuItem6_12.AccessibleDescription = "About Telerik WinControls Suite..."
			Me.radMenuItem6_12.AccessibleName = "About Telerik WinControls Suite..."
			Me.radMenuItem6_12.Name = "radMenuItem6_12"
			Me.radMenuItem6_12.PopupDirection = Telerik.WinControls.UI.RadDirection.Left
			Me.radMenuItem6_12.Text = "About Telerik WinControls Suite..."
			Me.radMenuItem6_12.TextImageRelation = TextImageRelation.ImageBeforeText
			Me.radMenuItem6_12.Visibility = Telerik.WinControls.ElementVisibility.Visible
			' 
			' radPanel1
			' 
			Me.radPanel1.Controls.Add(Me.radMenuDemo)
			Me.radPanel1.Location = New Point(0, 0)
			Me.radPanel1.Name = "radPanel1"
			' 
			' 
			' 
			Me.radPanel1.RootElement.Padding = New Padding(1, 1, 0, 0)
			Me.radPanel1.Size = New Size(500, 350)
			Me.radPanel1.TabIndex = 2
			' 
			' Form1
			' 
			Me.Controls.Add(Me.radPanel1)
			Me.Name = "Form1"
			Me.Size = New Size(1170, 671)
			Me.Controls.SetChildIndex(Me.radPanel1, 0)
			Me.Controls.SetChildIndex(Me.settingsPanel, 0)
			CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radMenuDemo, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radMenuComboItem1.ComboBoxElement, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radMenuComboItem2.ComboBoxElement, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radMenuComboItem3.ComboBoxElement, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radMenuComboItem4.ComboBoxElement, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radPanel1, System.ComponentModel.ISupportInitialize).EndInit()
			Me.radPanel1.ResumeLayout(False)
			Me.radPanel1.PerformLayout()
			Me.ResumeLayout(False)

		End Sub

		#End Region


		Private radMenuDemo As Telerik.WinControls.UI.RadMenu
		Private radMenuItem1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_4 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_5 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_6 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_7 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem1_9 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_4 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_5 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_6 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_7 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem2_8 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_4 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_5 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_6 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_7 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_8 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_9 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_10 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_11 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_12 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_13 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_14 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_15 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_16 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_17 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_18 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_19 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_20 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem3_21 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem4_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem4_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem4_3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem5_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem5_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_4 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_5 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_6 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_7 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_8 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_9 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_10 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_11 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItem6_12 As Telerik.WinControls.UI.RadMenuItem
		Private imageList1 As ImageList
		Private radMenuItemSrc_1 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSrc_2 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuItemSrc_3 As Telerik.WinControls.UI.RadMenuItem
		Private radMenuComboItem1 As Telerik.WinControls.UI.RadMenuComboItem
		Private radMenuSeparatorItem1 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem2 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem3 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem4 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem6 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem8 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuSeparatorItem9 As Telerik.WinControls.UI.RadMenuSeparatorItem
		Private radMenuComboItem2 As Telerik.WinControls.UI.RadMenuComboItem
		Private radMenuComboItem3 As Telerik.WinControls.UI.RadMenuComboItem
		Private radMenuComboItem4 As Telerik.WinControls.UI.RadMenuComboItem
		Private radPanel1 As Telerik.WinControls.UI.RadPanel
	End Class
End Namespace