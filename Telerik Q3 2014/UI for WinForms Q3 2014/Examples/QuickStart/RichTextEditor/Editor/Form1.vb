﻿Imports System.IO
Imports Telerik.QuickStart.WinControls
Imports System.Management

Namespace Telerik.Examples.WinControls.RichTextEditor.Editor
    Partial Public Class Form1
        Inherits ExternalProcessForm
        Private ReadOnly ExternalExampleName As String = "FirstLook"

        Protected Overrides Function GetExecutablePath() As String
            Return "\..\..\RichTextEditor\bin\RichTextEditor.exe"
        End Function

        Protected Overrides Function GetExternalProcessArguments(ByVal excutablePath As String) As String
            If String.IsNullOrEmpty(Me.ThemeName) Then
                Return String.Format("{0} {1}", ExternalExampleName, "TelerikMetro")
            Else
                Return String.Format("{0} {1}", ExternalExampleName, Me.ThemeName)
            End If
        End Function

        Protected Overrides ReadOnly Property CanOpenMultipleInstances() As Boolean
            Get
                Return True
            End Get
        End Property
    End Class
End Namespace
