﻿Namespace Telerik.Examples.WinControls.TrackStatusControls.Rating
    Partial Class Form1
        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Windows Form Designer generated code"

        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Me.radRating1 = New Telerik.WinControls.UI.RadRating()
            Me.ratingStarVisualElement1 = New Telerik.WinControls.UI.RatingStarVisualElement()
            Me.ratingStarVisualElement2 = New Telerik.WinControls.UI.RatingStarVisualElement()
            Me.ratingStarVisualElement3 = New Telerik.WinControls.UI.RatingStarVisualElement()
            Me.ratingStarVisualElement4 = New Telerik.WinControls.UI.RatingStarVisualElement()
            Me.ratingStarVisualElement5 = New Telerik.WinControls.UI.RatingStarVisualElement()
            Me.radRating2 = New Telerik.WinControls.UI.RadRating()
            Me.ratingDiamondVisualElement1 = New Telerik.WinControls.UI.RatingDiamondVisualElement()
            Me.ratingDiamondVisualElement2 = New Telerik.WinControls.UI.RatingDiamondVisualElement()
            Me.ratingDiamondVisualElement3 = New Telerik.WinControls.UI.RatingDiamondVisualElement()
            Me.ratingDiamondVisualElement4 = New Telerik.WinControls.UI.RatingDiamondVisualElement()
            Me.ratingDiamondVisualElement5 = New Telerik.WinControls.UI.RatingDiamondVisualElement()
            Me.radRating3 = New Telerik.WinControls.UI.RadRating()
            Me.ratingHeartVisualElement1 = New Telerik.WinControls.UI.RatingHeartVisualElement()
            Me.ratingHeartVisualElement2 = New Telerik.WinControls.UI.RatingHeartVisualElement()
            Me.ratingHeartVisualElement3 = New Telerik.WinControls.UI.RatingHeartVisualElement()
            Me.ratingHeartVisualElement4 = New Telerik.WinControls.UI.RatingHeartVisualElement()
            Me.ratingHeartVisualElement5 = New Telerik.WinControls.UI.RatingHeartVisualElement()
            Me.radGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
            Me.radGroupBox3 = New Telerik.WinControls.UI.RadGroupBox()
            Me.radRadioButton3 = New Telerik.WinControls.UI.RadRadioButton()
            Me.radRadioButton4 = New Telerik.WinControls.UI.RadRadioButton()
            Me.radSpinEditor2 = New Telerik.WinControls.UI.RadSpinEditor()
            Me.radSpinEditor1 = New Telerik.WinControls.UI.RadSpinEditor()
            Me.radCheckBox2 = New Telerik.WinControls.UI.RadCheckBox()
            Me.radCheckBox1 = New Telerik.WinControls.UI.RadCheckBox()
            Me.radGroupBox2 = New Telerik.WinControls.UI.RadGroupBox()
            Me.radRadioButton1 = New Telerik.WinControls.UI.RadRadioButton()
            Me.radRadioButton2 = New Telerik.WinControls.UI.RadRadioButton()
            Me.radLabel8 = New Telerik.WinControls.UI.RadLabel()
            Me.radLabel7 = New Telerik.WinControls.UI.RadLabel()
            Me.radDropDownList1 = New Telerik.WinControls.UI.RadDropDownList()
            Me.radLabel5 = New Telerik.WinControls.UI.RadLabel()
            Me.radRating4 = New Telerik.WinControls.UI.RadRating()
            Me.ratingStarVisualElement6 = New Telerik.WinControls.UI.RatingStarVisualElement()
            Me.ratingStarVisualElement7 = New Telerik.WinControls.UI.RatingStarVisualElement()
            Me.ratingStarVisualElement8 = New Telerik.WinControls.UI.RatingStarVisualElement()
            Me.ratingHeartVisualElement6 = New Telerik.WinControls.UI.RatingHeartVisualElement()
            Me.ratingHeartVisualElement7 = New Telerik.WinControls.UI.RatingHeartVisualElement()
            DirectCast(Me.settingsPanel, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.settingsPanel.SuspendLayout()
            DirectCast(Me.themePanel, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radRating1, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radRating2, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radRating3, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radGroupBox1.SuspendLayout()
            DirectCast(Me.radGroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radGroupBox3.SuspendLayout()
            DirectCast(Me.radRadioButton3, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radRadioButton4, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radSpinEditor2, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radSpinEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radCheckBox2, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radCheckBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radGroupBox2.SuspendLayout()
            DirectCast(Me.radRadioButton1, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radRadioButton2, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radLabel8, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radLabel7, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radDropDownList1, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radLabel5, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radRating4, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            ' 
            ' settingsPanel
            ' 
            Me.settingsPanel.Controls.Add(Me.radGroupBox1)
            Me.settingsPanel.Location = New System.Drawing.Point(851, 3)
            Me.settingsPanel.Size = New System.Drawing.Size(355, 354)
            Me.settingsPanel.Controls.SetChildIndex(Me.radGroupBox1, 0)
            ' 
            ' themePanel
            ' 
            Me.themePanel.Location = New System.Drawing.Point(851, 387)
            ' 
            ' radRating1
            ' 
            Me.radRating1.Caption = "The Godfather 1972"
            Me.radRating1.Items.AddRange(New Telerik.WinControls.RadItem() {Me.ratingStarVisualElement1, Me.ratingStarVisualElement2, Me.ratingStarVisualElement3, Me.ratingStarVisualElement4, Me.ratingStarVisualElement5})
            Me.radRating1.Location = New System.Drawing.Point(50, 50)
            Me.radRating1.Name = "radRating1"
            Me.radRating1.Size = New System.Drawing.Size(140, 51)
            Me.radRating1.TabIndex = 2
            Me.radRating1.Text = "radRating1"
            ' 
            ' ratingStarVisualElement1
            ' 
            Me.ratingStarVisualElement1.AccessibleDescription = "ratingStarVisualElement1"
            Me.ratingStarVisualElement1.AccessibleName = "ratingStarVisualElement1"
            Me.ratingStarVisualElement1.Name = "ratingStarVisualElement1"
            Me.ratingStarVisualElement1.Text = "ratingStarVisualElement1"
            Me.ratingStarVisualElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingStarVisualElement2
            ' 
            Me.ratingStarVisualElement2.AccessibleDescription = "ratingStarVisualElement2"
            Me.ratingStarVisualElement2.AccessibleName = "ratingStarVisualElement2"
            Me.ratingStarVisualElement2.Name = "ratingStarVisualElement2"
            Me.ratingStarVisualElement2.Text = "ratingStarVisualElement2"
            Me.ratingStarVisualElement2.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingStarVisualElement3
            ' 
            Me.ratingStarVisualElement3.AccessibleDescription = "ratingStarVisualElement3"
            Me.ratingStarVisualElement3.AccessibleName = "ratingStarVisualElement3"
            Me.ratingStarVisualElement3.Name = "ratingStarVisualElement3"
            Me.ratingStarVisualElement3.Text = "ratingStarVisualElement3"
            Me.ratingStarVisualElement3.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingStarVisualElement4
            ' 
            Me.ratingStarVisualElement4.AccessibleDescription = "ratingStarVisualElement4"
            Me.ratingStarVisualElement4.AccessibleName = "ratingStarVisualElement4"
            Me.ratingStarVisualElement4.Name = "ratingStarVisualElement4"
            Me.ratingStarVisualElement4.Text = "ratingStarVisualElement4"
            Me.ratingStarVisualElement4.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingStarVisualElement5
            ' 
            Me.ratingStarVisualElement5.AccessibleDescription = "ratingStarVisualElement5"
            Me.ratingStarVisualElement5.AccessibleName = "ratingStarVisualElement5"
            Me.ratingStarVisualElement5.Name = "ratingStarVisualElement5"
            Me.ratingStarVisualElement5.Text = "ratingStarVisualElement5"
            Me.ratingStarVisualElement5.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' radRating2
            ' 
            Me.radRating2.Caption = "Forrest Gump 1994"
            Me.radRating2.Items.AddRange(New Telerik.WinControls.RadItem() {Me.ratingDiamondVisualElement1, Me.ratingDiamondVisualElement2, Me.ratingDiamondVisualElement3, Me.ratingDiamondVisualElement4, Me.ratingDiamondVisualElement5})
            Me.radRating2.Location = New System.Drawing.Point(50, 120)
            Me.radRating2.Name = "radRating2"
            Me.radRating2.Size = New System.Drawing.Size(140, 51)
            Me.radRating2.TabIndex = 3
            Me.radRating2.Text = "radRating2"
            ' 
            ' ratingDiamondVisualElement1
            ' 
            Me.ratingDiamondVisualElement1.AccessibleDescription = "ratingDiamondVisualElement1"
            Me.ratingDiamondVisualElement1.AccessibleName = "ratingDiamondVisualElement1"
            Me.ratingDiamondVisualElement1.Name = "ratingDiamondVisualElement1"
            Me.ratingDiamondVisualElement1.Text = "ratingDiamondVisualElement1"
            Me.ratingDiamondVisualElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingDiamondVisualElement2
            ' 
            Me.ratingDiamondVisualElement2.AccessibleDescription = "ratingDiamondVisualElement2"
            Me.ratingDiamondVisualElement2.AccessibleName = "ratingDiamondVisualElement2"
            Me.ratingDiamondVisualElement2.Name = "ratingDiamondVisualElement2"
            Me.ratingDiamondVisualElement2.Text = "ratingDiamondVisualElement2"
            Me.ratingDiamondVisualElement2.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingDiamondVisualElement3
            ' 
            Me.ratingDiamondVisualElement3.AccessibleDescription = "ratingDiamondVisualElement3"
            Me.ratingDiamondVisualElement3.AccessibleName = "ratingDiamondVisualElement3"
            Me.ratingDiamondVisualElement3.Name = "ratingDiamondVisualElement3"
            Me.ratingDiamondVisualElement3.Text = "ratingDiamondVisualElement3"
            Me.ratingDiamondVisualElement3.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingDiamondVisualElement4
            ' 
            Me.ratingDiamondVisualElement4.AccessibleDescription = "ratingDiamondVisualElement4"
            Me.ratingDiamondVisualElement4.AccessibleName = "ratingDiamondVisualElement4"
            Me.ratingDiamondVisualElement4.Name = "ratingDiamondVisualElement4"
            Me.ratingDiamondVisualElement4.Text = "ratingDiamondVisualElement4"
            Me.ratingDiamondVisualElement4.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingDiamondVisualElement5
            ' 
            Me.ratingDiamondVisualElement5.AccessibleDescription = "ratingDiamondVisualElement5"
            Me.ratingDiamondVisualElement5.AccessibleName = "ratingDiamondVisualElement5"
            Me.ratingDiamondVisualElement5.Name = "ratingDiamondVisualElement5"
            Me.ratingDiamondVisualElement5.Text = "ratingDiamondVisualElement5"
            Me.ratingDiamondVisualElement5.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' radRating3
            ' 
            Me.radRating3.Caption = "The Sixth Sense 1999"
            Me.radRating3.Items.AddRange(New Telerik.WinControls.RadItem() {Me.ratingHeartVisualElement1, Me.ratingHeartVisualElement2, Me.ratingHeartVisualElement3, Me.ratingHeartVisualElement4, Me.ratingHeartVisualElement5})
            Me.radRating3.Location = New System.Drawing.Point(250, 50)
            Me.radRating3.Name = "radRating3"
            Me.radRating3.Size = New System.Drawing.Size(140, 51)
            Me.radRating3.TabIndex = 4
            Me.radRating3.Text = "radRating3"
            ' 
            ' ratingHeartVisualElement1
            ' 
            Me.ratingHeartVisualElement1.AccessibleDescription = "ratingHeartVisualElement1"
            Me.ratingHeartVisualElement1.AccessibleName = "ratingHeartVisualElement1"
            Me.ratingHeartVisualElement1.Name = "ratingHeartVisualElement1"
            Me.ratingHeartVisualElement1.Text = "ratingHeartVisualElement1"
            Me.ratingHeartVisualElement1.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingHeartVisualElement2
            ' 
            Me.ratingHeartVisualElement2.AccessibleDescription = "ratingHeartVisualElement2"
            Me.ratingHeartVisualElement2.AccessibleName = "ratingHeartVisualElement2"
            Me.ratingHeartVisualElement2.Name = "ratingHeartVisualElement2"
            Me.ratingHeartVisualElement2.Text = "ratingHeartVisualElement2"
            Me.ratingHeartVisualElement2.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingHeartVisualElement3
            ' 
            Me.ratingHeartVisualElement3.AccessibleDescription = "ratingHeartVisualElement3"
            Me.ratingHeartVisualElement3.AccessibleName = "ratingHeartVisualElement3"
            Me.ratingHeartVisualElement3.Name = "ratingHeartVisualElement3"
            Me.ratingHeartVisualElement3.Text = "ratingHeartVisualElement3"
            Me.ratingHeartVisualElement3.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingHeartVisualElement4
            ' 
            Me.ratingHeartVisualElement4.AccessibleDescription = "ratingHeartVisualElement4"
            Me.ratingHeartVisualElement4.AccessibleName = "ratingHeartVisualElement4"
            Me.ratingHeartVisualElement4.Name = "ratingHeartVisualElement4"
            Me.ratingHeartVisualElement4.Text = "ratingHeartVisualElement4"
            Me.ratingHeartVisualElement4.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingHeartVisualElement5
            ' 
            Me.ratingHeartVisualElement5.AccessibleDescription = "ratingHeartVisualElement5"
            Me.ratingHeartVisualElement5.AccessibleName = "ratingHeartVisualElement5"
            Me.ratingHeartVisualElement5.Name = "ratingHeartVisualElement5"
            Me.ratingHeartVisualElement5.Text = "ratingHeartVisualElement5"
            Me.ratingHeartVisualElement5.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' radGroupBox1
            ' 
            Me.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
            Me.radGroupBox1.Controls.Add(Me.radGroupBox3)
            Me.radGroupBox1.Controls.Add(Me.radSpinEditor2)
            Me.radGroupBox1.Controls.Add(Me.radSpinEditor1)
            Me.radGroupBox1.Controls.Add(Me.radCheckBox2)
            Me.radGroupBox1.Controls.Add(Me.radCheckBox1)
            Me.radGroupBox1.Controls.Add(Me.radGroupBox2)
            Me.radGroupBox1.Controls.Add(Me.radLabel8)
            Me.radGroupBox1.Controls.Add(Me.radLabel7)
            Me.radGroupBox1.Controls.Add(Me.radDropDownList1)
            Me.radGroupBox1.Controls.Add(Me.radLabel5)
            Me.radGroupBox1.HeaderText = "Settings"
            Me.radGroupBox1.Location = New System.Drawing.Point(4, 47)
            Me.radGroupBox1.Name = "radGroupBox1"
            ' 
            ' 
            ' 
            Me.radGroupBox1.RootElement.Padding = New System.Windows.Forms.Padding(2, 18, 2, 2)
            Me.radGroupBox1.Size = New System.Drawing.Size(187, 304)
            Me.radGroupBox1.TabIndex = 2
            Me.radGroupBox1.Text = "Settings"
            ' 
            ' radGroupBox3
            ' 
            Me.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
            Me.radGroupBox3.Controls.Add(Me.radRadioButton3)
            Me.radGroupBox3.Controls.Add(Me.radRadioButton4)
            Me.radGroupBox3.HeaderText = "Orientation"
            Me.radGroupBox3.Location = New System.Drawing.Point(5, 191)
            Me.radGroupBox3.Name = "radGroupBox3"
            ' 
            ' 
            ' 
            Me.radGroupBox3.RootElement.Padding = New System.Windows.Forms.Padding(2, 18, 2, 2)
            Me.radGroupBox3.Size = New System.Drawing.Size(170, 53)
            Me.radGroupBox3.TabIndex = 21
            Me.radGroupBox3.Text = "Orientation"
            ' 
            ' radRadioButton3
            ' 
            Me.radRadioButton3.Location = New System.Drawing.Point(5, 21)
            Me.radRadioButton3.Name = "radRadioButton3"
            Me.radRadioButton3.Size = New System.Drawing.Size(72, 18)
            Me.radRadioButton3.TabIndex = 0
            Me.radRadioButton3.TabStop = True
            Me.radRadioButton3.Text = "Horizontal"
            Me.radRadioButton3.ToggleState = Telerik.WinControls.Enumerations.ToggleState.[On]
            ' 
            ' radRadioButton4
            ' 
            Me.radRadioButton4.Location = New System.Drawing.Point(89, 21)
            Me.radRadioButton4.Name = "radRadioButton4"
            Me.radRadioButton4.Size = New System.Drawing.Size(57, 18)
            Me.radRadioButton4.TabIndex = 1
            Me.radRadioButton4.Text = "Vertical"
            ' 
            ' radSpinEditor2
            ' 
            Me.radSpinEditor2.Location = New System.Drawing.Point(88, 95)
            Me.radSpinEditor2.Name = "radSpinEditor2"
            Me.radSpinEditor2.Size = New System.Drawing.Size(87, 20)
            Me.radSpinEditor2.TabIndex = 20
            Me.radSpinEditor2.TabStop = False
            Me.radSpinEditor2.Tag = "Right"
            ' 
            ' radSpinEditor1
            ' 
            Me.radSpinEditor1.Location = New System.Drawing.Point(88, 70)
            Me.radSpinEditor1.Name = "radSpinEditor1"
            Me.radSpinEditor1.Size = New System.Drawing.Size(87, 20)
            Me.radSpinEditor1.TabIndex = 19
            Me.radSpinEditor1.TabStop = False
            Me.radSpinEditor1.Tag = "Right"
            ' 
            ' radCheckBox2
            ' 
            Me.radCheckBox2.Location = New System.Drawing.Point(10, 277)
            Me.radCheckBox2.Name = "radCheckBox2"
            Me.radCheckBox2.Size = New System.Drawing.Size(68, 18)
            Me.radCheckBox2.TabIndex = 16
            Me.radCheckBox2.Text = "ReadOnly"
            ' 
            ' radCheckBox1
            ' 
            Me.radCheckBox1.Location = New System.Drawing.Point(10, 253)
            Me.radCheckBox1.Name = "radCheckBox1"
            Me.radCheckBox1.Size = New System.Drawing.Size(78, 18)
            Me.radCheckBox1.TabIndex = 15
            Me.radCheckBox1.Text = "RightToLeft"
            ' 
            ' radGroupBox2
            ' 
            Me.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
            Me.radGroupBox2.Controls.Add(Me.radRadioButton1)
            Me.radGroupBox2.Controls.Add(Me.radRadioButton2)
            Me.radGroupBox2.HeaderText = "Direction"
            Me.radGroupBox2.Location = New System.Drawing.Point(5, 132)
            Me.radGroupBox2.Name = "radGroupBox2"
            ' 
            ' 
            ' 
            Me.radGroupBox2.RootElement.Padding = New System.Windows.Forms.Padding(2, 18, 2, 2)
            Me.radGroupBox2.Size = New System.Drawing.Size(170, 53)
            Me.radGroupBox2.TabIndex = 14
            Me.radGroupBox2.Text = "Direction"
            ' 
            ' radRadioButton1
            ' 
            Me.radRadioButton1.Location = New System.Drawing.Point(5, 21)
            Me.radRadioButton1.Name = "radRadioButton1"
            Me.radRadioButton1.Size = New System.Drawing.Size(65, 18)
            Me.radRadioButton1.TabIndex = 0
            Me.radRadioButton1.TabStop = True
            Me.radRadioButton1.Text = "Standard"
            Me.radRadioButton1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.[On]
            ' 
            ' radRadioButton2
            ' 
            Me.radRadioButton2.Location = New System.Drawing.Point(89, 21)
            Me.radRadioButton2.Name = "radRadioButton2"
            Me.radRadioButton2.Size = New System.Drawing.Size(66, 18)
            Me.radRadioButton2.TabIndex = 1
            Me.radRadioButton2.Text = "Reversed"
            ' 
            ' radLabel8
            ' 
            Me.radLabel8.Location = New System.Drawing.Point(4, 97)
            Me.radLabel8.Name = "radLabel8"
            Me.radLabel8.Size = New System.Drawing.Size(55, 18)
            Me.radLabel8.TabIndex = 8
            Me.radLabel8.Text = "MaxValue"
            ' 
            ' radLabel7
            ' 
            Me.radLabel7.Location = New System.Drawing.Point(5, 72)
            Me.radLabel7.Name = "radLabel7"
            Me.radLabel7.Size = New System.Drawing.Size(53, 18)
            Me.radLabel7.TabIndex = 6
            Me.radLabel7.Text = "MinValue"
            ' 
            ' radDropDownList1
            ' 
            Me.radDropDownList1.Location = New System.Drawing.Point(88, 44)
            Me.radDropDownList1.Name = "radDropDownList1"
            Me.radDropDownList1.Size = New System.Drawing.Size(87, 20)
            Me.radDropDownList1.TabIndex = 3
            Me.radDropDownList1.Tag = "Right"
            Me.radDropDownList1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            ' 
            ' radLabel5
            ' 
            Me.radLabel5.Location = New System.Drawing.Point(5, 44)
            Me.radLabel5.Name = "radLabel5"
            Me.radLabel5.Size = New System.Drawing.Size(81, 18)
            Me.radLabel5.TabIndex = 2
            Me.radLabel5.Text = "SelectionMode"
            ' 
            ' radRating4
            ' 
            Me.radRating4.Caption = "Office Space 1999"
            Me.radRating4.Items.AddRange(New Telerik.WinControls.RadItem() {Me.ratingStarVisualElement6, Me.ratingStarVisualElement7, Me.ratingStarVisualElement8, Me.ratingHeartVisualElement6, Me.ratingHeartVisualElement7})
            Me.radRating4.Location = New System.Drawing.Point(250, 120)
            Me.radRating4.Name = "radRating4"
            Me.radRating4.Size = New System.Drawing.Size(140, 51)
            Me.radRating4.TabIndex = 5
            Me.radRating4.Text = "radRating4"
            ' 
            ' ratingStarVisualElement6
            ' 
            Me.ratingStarVisualElement6.AccessibleDescription = "ratingStarVisualElement6"
            Me.ratingStarVisualElement6.AccessibleName = "ratingStarVisualElement6"
            Me.ratingStarVisualElement6.Name = "ratingStarVisualElement6"
            Me.ratingStarVisualElement6.Text = "ratingStarVisualElement6"
            Me.ratingStarVisualElement6.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingStarVisualElement7
            ' 
            Me.ratingStarVisualElement7.AccessibleDescription = "ratingStarVisualElement7"
            Me.ratingStarVisualElement7.AccessibleName = "ratingStarVisualElement7"
            Me.ratingStarVisualElement7.Name = "ratingStarVisualElement7"
            Me.ratingStarVisualElement7.Text = "ratingStarVisualElement7"
            Me.ratingStarVisualElement7.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingStarVisualElement8
            ' 
            Me.ratingStarVisualElement8.AccessibleDescription = "ratingStarVisualElement8"
            Me.ratingStarVisualElement8.AccessibleName = "ratingStarVisualElement8"
            Me.ratingStarVisualElement8.Name = "ratingStarVisualElement8"
            Me.ratingStarVisualElement8.Text = "ratingStarVisualElement8"
            Me.ratingStarVisualElement8.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingHeartVisualElement6
            ' 
            Me.ratingHeartVisualElement6.AccessibleDescription = "ratingHeartVisualElement6"
            Me.ratingHeartVisualElement6.AccessibleName = "ratingHeartVisualElement6"
            Me.ratingHeartVisualElement6.Name = "ratingHeartVisualElement6"
            Me.ratingHeartVisualElement6.Text = "ratingHeartVisualElement6"
            Me.ratingHeartVisualElement6.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' ratingHeartVisualElement7
            ' 
            Me.ratingHeartVisualElement7.AccessibleDescription = "ratingHeartVisualElement7"
            Me.ratingHeartVisualElement7.AccessibleName = "ratingHeartVisualElement7"
            Me.ratingHeartVisualElement7.Name = "ratingHeartVisualElement7"
            Me.ratingHeartVisualElement7.Text = "ratingHeartVisualElement7"
            Me.ratingHeartVisualElement7.Visibility = Telerik.WinControls.ElementVisibility.Visible
            ' 
            ' Form1
            ' 
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0F, 13.0F)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.radRating4)
            Me.Controls.Add(Me.radRating3)
            Me.Controls.Add(Me.radRating2)
            Me.Controls.Add(Me.radRating1)
            Me.Name = "Form1"
            Me.Size = New System.Drawing.Size(1136, 633)
            Me.Controls.SetChildIndex(Me.settingsPanel, 0)
            Me.Controls.SetChildIndex(Me.themePanel, 0)
            Me.Controls.SetChildIndex(Me.radRating1, 0)
            Me.Controls.SetChildIndex(Me.radRating2, 0)
            Me.Controls.SetChildIndex(Me.radRating3, 0)
            Me.Controls.SetChildIndex(Me.radRating4, 0)
            DirectCast(Me.settingsPanel, System.ComponentModel.ISupportInitialize).EndInit()
            Me.settingsPanel.ResumeLayout(False)
            Me.settingsPanel.PerformLayout()
            DirectCast(Me.themePanel, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radRating1, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radRating2, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radRating3, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radGroupBox1.ResumeLayout(False)
            Me.radGroupBox1.PerformLayout()
            DirectCast(Me.radGroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radGroupBox3.ResumeLayout(False)
            Me.radGroupBox3.PerformLayout()
            DirectCast(Me.radRadioButton3, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radRadioButton4, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radSpinEditor2, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radSpinEditor1, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radCheckBox2, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radCheckBox1, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radGroupBox2.ResumeLayout(False)
            Me.radGroupBox2.PerformLayout()
            DirectCast(Me.radRadioButton1, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radRadioButton2, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radLabel8, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radLabel7, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radDropDownList1, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radLabel5, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radRating4, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private radRating1 As Telerik.WinControls.UI.RadRating
        Private ratingStarVisualElement1 As Telerik.WinControls.UI.RatingStarVisualElement
        Private ratingStarVisualElement2 As Telerik.WinControls.UI.RatingStarVisualElement
        Private ratingStarVisualElement3 As Telerik.WinControls.UI.RatingStarVisualElement
        Private ratingStarVisualElement4 As Telerik.WinControls.UI.RatingStarVisualElement
        Private ratingStarVisualElement5 As Telerik.WinControls.UI.RatingStarVisualElement
        Private radRating2 As Telerik.WinControls.UI.RadRating
        Private ratingDiamondVisualElement1 As Telerik.WinControls.UI.RatingDiamondVisualElement
        Private ratingDiamondVisualElement2 As Telerik.WinControls.UI.RatingDiamondVisualElement
        Private ratingDiamondVisualElement3 As Telerik.WinControls.UI.RatingDiamondVisualElement
        Private ratingDiamondVisualElement4 As Telerik.WinControls.UI.RatingDiamondVisualElement
        Private ratingDiamondVisualElement5 As Telerik.WinControls.UI.RatingDiamondVisualElement
        Private radRating3 As Telerik.WinControls.UI.RadRating
        Private ratingHeartVisualElement1 As Telerik.WinControls.UI.RatingHeartVisualElement
        Private ratingHeartVisualElement2 As Telerik.WinControls.UI.RatingHeartVisualElement
        Private ratingHeartVisualElement3 As Telerik.WinControls.UI.RatingHeartVisualElement
        Private ratingHeartVisualElement4 As Telerik.WinControls.UI.RatingHeartVisualElement
        Private ratingHeartVisualElement5 As Telerik.WinControls.UI.RatingHeartVisualElement
        Private radGroupBox1 As Telerik.WinControls.UI.RadGroupBox
        Private radSpinEditor2 As Telerik.WinControls.UI.RadSpinEditor
        Private radSpinEditor1 As Telerik.WinControls.UI.RadSpinEditor
        Private radCheckBox2 As Telerik.WinControls.UI.RadCheckBox
        Private radCheckBox1 As Telerik.WinControls.UI.RadCheckBox
        Private radGroupBox2 As Telerik.WinControls.UI.RadGroupBox
        Private radRadioButton1 As Telerik.WinControls.UI.RadRadioButton
        Private radRadioButton2 As Telerik.WinControls.UI.RadRadioButton
        Private radLabel8 As Telerik.WinControls.UI.RadLabel
        Private radLabel7 As Telerik.WinControls.UI.RadLabel
        Private radDropDownList1 As Telerik.WinControls.UI.RadDropDownList
        Private radLabel5 As Telerik.WinControls.UI.RadLabel
        Private radRating4 As Telerik.WinControls.UI.RadRating
        Private ratingStarVisualElement6 As Telerik.WinControls.UI.RatingStarVisualElement
        Private ratingStarVisualElement7 As Telerik.WinControls.UI.RatingStarVisualElement
        Private ratingStarVisualElement8 As Telerik.WinControls.UI.RatingStarVisualElement
        Private ratingHeartVisualElement6 As Telerik.WinControls.UI.RatingHeartVisualElement
        Private ratingHeartVisualElement7 As Telerik.WinControls.UI.RatingHeartVisualElement
        Private radGroupBox3 As Telerik.WinControls.UI.RadGroupBox
        Private radRadioButton3 As Telerik.WinControls.UI.RadRadioButton
        Private radRadioButton4 As Telerik.WinControls.UI.RadRadioButton


    End Class
End Namespace
