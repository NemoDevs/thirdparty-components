namespace Telerik.Examples.WinControls.TrackStatusControls.ProgressBar
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.radTrackBar1 = new Telerik.WinControls.UI.RadTrackBar();
            this.radProgressBar5 = new Telerik.WinControls.UI.RadProgressBar();
            this.radProgressBar2 = new Telerik.WinControls.UI.RadProgressBar();
            this.radProgressBar4 = new Telerik.WinControls.UI.RadProgressBar();
            this.radProgressBar1 = new Telerik.WinControls.UI.RadProgressBar();
            this.radProgressBar6 = new Telerik.WinControls.UI.RadProgressBar();
            this.radGroupHorizontal = new Telerik.WinControls.UI.RadGroupBox();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radGroupImage = new Telerik.WinControls.UI.RadGroupBox();
            this.radGroupSlider = new Telerik.WinControls.UI.RadGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupHorizontal)).BeginInit();
            this.radGroupHorizontal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupImage)).BeginInit();
            this.radGroupImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupSlider)).BeginInit();
            this.radGroupSlider.SuspendLayout();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Location = new System.Drawing.Point(821, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 705);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 50;
            // 
            // radTrackBar1
            // 
            this.radTrackBar1.Location = new System.Drawing.Point(15, 27);
            this.radTrackBar1.Maximum = 10F;
            this.radTrackBar1.Name = "radTrackBar1";
            this.radTrackBar1.Size = new System.Drawing.Size(436, 37);
            this.radTrackBar1.TabIndex = 18;
            this.radTrackBar1.Text = "radTrackBar1";
            this.radTrackBar1.Value = 10F;
            // 
            // radProgressBar5
            // 
            this.radProgressBar5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radProgressBar5.ForeColor = System.Drawing.Color.Black;
            this.radProgressBar5.Image = ((System.Drawing.Image)(resources.GetObject("radProgressBar5.Image")));         
            this.radProgressBar5.Location = new System.Drawing.Point(16, 22);
            this.radProgressBar5.Name = "radProgressBar5";
            this.radProgressBar5.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.radProgressBar5.SeparatorWidth = 4;
            this.radProgressBar5.Size = new System.Drawing.Size(316, 64);
            this.radProgressBar5.StepWidth = 13;
            this.radProgressBar5.TabIndex = 14;
            this.radProgressBar5.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radProgressBar5.Value1 = 100;
            // 
            // radProgressBar2
            // 
            this.radProgressBar2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radProgressBar2.ForeColor = System.Drawing.Color.Black;
            this.radProgressBar2.Location = new System.Drawing.Point(16, 69);
            this.radProgressBar2.Name = "radProgressBar2";
            this.radProgressBar2.ProgressOrientation = Telerik.WinControls.ProgressOrientation.Right;
            this.radProgressBar2.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.radProgressBar2.SeparatorWidth = 0;
            this.radProgressBar2.ShowProgressIndicators = true;
            this.radProgressBar2.Size = new System.Drawing.Size(316, 28);
            this.radProgressBar2.StepWidth = 13;
            this.radProgressBar2.TabIndex = 13;
            this.radProgressBar2.Text = "60 %";
            this.radProgressBar2.Value1 = 60;
            // 
            // radProgressBar4
            // 
            this.radProgressBar4.ForeColor = System.Drawing.Color.Black;
            this.radProgressBar4.Location = new System.Drawing.Point(68, 28);
            this.radProgressBar4.Name = "radProgressBar4";
            this.radProgressBar4.ProgressOrientation = Telerik.WinControls.ProgressOrientation.Top;
            this.radProgressBar4.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.radProgressBar4.SeparatorWidth = 4;
            this.radProgressBar4.ShowProgressIndicators = true;
            this.radProgressBar4.Size = new System.Drawing.Size(31, 202);
            this.radProgressBar4.StepWidth = 13;
            this.radProgressBar4.TabIndex = 12;
            this.radProgressBar4.Text = "60 %";
            this.radProgressBar4.Value1 = 60;
            // 
            // radProgressBar1
            // 
            this.radProgressBar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radProgressBar1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radProgressBar1.ImageScalingSize = new System.Drawing.Size(0, 0);
            this.radProgressBar1.Location = new System.Drawing.Point(16, 26);
            this.radProgressBar1.Name = "radProgressBar1";
            this.radProgressBar1.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.radProgressBar1.SeparatorWidth = 0;
            this.radProgressBar1.ShowProgressIndicators = true;
            this.radProgressBar1.Size = new System.Drawing.Size(316, 28);
            this.radProgressBar1.StepWidth = 13;
            this.radProgressBar1.SweepAngle = 120;
            this.radProgressBar1.TabIndex = 6;
            this.radProgressBar1.Text = "60 %";
            this.radProgressBar1.Value1 = 60;
            // 
            // radProgressBar6
            // 
            this.radProgressBar6.ForeColor = System.Drawing.Color.Black;
            this.radProgressBar6.Location = new System.Drawing.Point(16, 28);
            this.radProgressBar6.Name = "radProgressBar6";
            this.radProgressBar6.ProgressOrientation = Telerik.WinControls.ProgressOrientation.Bottom;
            this.radProgressBar6.SeparatorColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.radProgressBar6.SeparatorWidth = 0;
            this.radProgressBar6.ShowProgressIndicators = true;
            this.radProgressBar6.Size = new System.Drawing.Size(31, 202);
            this.radProgressBar6.StepWidth = 13;
            this.radProgressBar6.TabIndex = 25;
            this.radProgressBar6.Text = "60 %";
            this.radProgressBar6.Value1 = 60;
            // 
            // radGroupHorizontal
            // 
            this.radGroupHorizontal.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupHorizontal.Controls.Add(this.radProgressBar1);
            this.radGroupHorizontal.Controls.Add(this.radProgressBar2);
            this.radGroupHorizontal.FooterText = "";
            this.radGroupHorizontal.ForeColor = System.Drawing.Color.Black;
            this.radGroupHorizontal.HeaderText = "Horizontal";
            this.radGroupHorizontal.Location = new System.Drawing.Point(0, 0);
            this.radGroupHorizontal.Name = "radGroupHorizontal";
            this.radGroupHorizontal.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupHorizontal.Size = new System.Drawing.Size(349, 122);
            this.radGroupHorizontal.TabIndex = 30;
            this.radGroupHorizontal.Text = "Horizontal";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.radProgressBar6);
            this.radGroupBox2.Controls.Add(this.radProgressBar4);
            this.radGroupBox2.FooterText = "";
            this.radGroupBox2.ForeColor = System.Drawing.Color.Black;
            this.radGroupBox2.HeaderText = "Vertical";
            this.radGroupBox2.Location = new System.Drawing.Point(355, 0);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupBox2.Size = new System.Drawing.Size(112, 245);
            this.radGroupBox2.TabIndex = 31;
            this.radGroupBox2.Text = "Vertical";
            // 
            // radGroupImage
            // 
            this.radGroupImage.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupImage.Controls.Add(this.radProgressBar5);
            this.radGroupImage.FooterText = "";
            this.radGroupImage.ForeColor = System.Drawing.Color.Black;
            this.radGroupImage.HeaderText = "Image";
            this.radGroupImage.Location = new System.Drawing.Point(0, 145);
            this.radGroupImage.Name = "radGroupImage";
            this.radGroupImage.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupImage.Size = new System.Drawing.Size(349, 100);
            this.radGroupImage.TabIndex = 32;
            this.radGroupImage.Text = "Image";
            // 
            // radGroupSlider
            // 
            this.radGroupSlider.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupSlider.Controls.Add(this.radTrackBar1);
            this.radGroupSlider.FooterText = "";
            this.radGroupSlider.ForeColor = System.Drawing.Color.Black;
            this.radGroupSlider.HeaderText = "Slide me!";
            this.radGroupSlider.Location = new System.Drawing.Point(0, 262);
            this.radGroupSlider.Name = "radGroupSlider";
            this.radGroupSlider.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupSlider.Size = new System.Drawing.Size(467, 75);
            this.radGroupSlider.TabIndex = 33;
            this.radGroupSlider.Text = "Slide me!";
            // 
            // Form1
            // 
            this.Controls.Add(this.radGroupBox2);
            this.Controls.Add(this.radGroupSlider);
            this.Controls.Add(this.radGroupImage);
            this.Controls.Add(this.radGroupHorizontal);
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(2, 35, 2, 4);
            this.Size = new System.Drawing.Size(1077, 655);
            this.Controls.SetChildIndex(this.themePanel, 0);
            this.Controls.SetChildIndex(this.radGroupHorizontal, 0);
            this.Controls.SetChildIndex(this.radGroupImage, 0);
            this.Controls.SetChildIndex(this.radGroupSlider, 0);
            this.Controls.SetChildIndex(this.radGroupBox2, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.themePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBar6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupHorizontal)).EndInit();
            this.radGroupHorizontal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupImage)).EndInit();
            this.radGroupImage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupSlider)).EndInit();
            this.radGroupSlider.ResumeLayout(false);
            this.radGroupSlider.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

		private System.Windows.Forms.Timer timer1;
		private Telerik.WinControls.UI.RadProgressBar radProgressBar1;
        private Telerik.WinControls.UI.RadProgressBar radProgressBar4;
        private Telerik.WinControls.UI.RadProgressBar radProgressBar2;
        private Telerik.WinControls.UI.RadProgressBar radProgressBar5;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar1;
        private Telerik.WinControls.UI.RadProgressBar radProgressBar6;
        private Telerik.WinControls.UI.RadGroupBox radGroupHorizontal;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGroupBox radGroupImage;
        private Telerik.WinControls.UI.RadGroupBox radGroupSlider;
    

    }
}
