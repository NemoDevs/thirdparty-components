namespace Telerik.Examples.WinControls.TrackStatusControls.TrackBar
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radDropDownList3 = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radCheckBox3 = new Telerik.WinControls.UI.RadCheckBox();
            this.radSpinEditor4 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radSpinEditor3 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radSpinEditor2 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radSpinEditor1 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radCheckBox2 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioButton1 = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioButton2 = new Telerik.WinControls.UI.RadRadioButton();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList2 = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList1 = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radTrackBar1 = new Telerik.WinControls.UI.RadTrackBar();
            this.radTrackBar2 = new Telerik.WinControls.UI.RadTrackBar();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radTrackBar3 = new Telerik.WinControls.UI.RadTrackBar();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radTrackBar4 = new Telerik.WinControls.UI.RadTrackBar();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupBox1);
            this.settingsPanel.Location = new System.Drawing.Point(700, 1);
            this.settingsPanel.Size = new System.Drawing.Size(348, 773);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupBox1, 0);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radDropDownList3);
            this.radGroupBox1.Controls.Add(this.radLabel11);
            this.radGroupBox1.Controls.Add(this.radCheckBox3);
            this.radGroupBox1.Controls.Add(this.radSpinEditor4);
            this.radGroupBox1.Controls.Add(this.radSpinEditor3);
            this.radGroupBox1.Controls.Add(this.radSpinEditor2);
            this.radGroupBox1.Controls.Add(this.radSpinEditor1);
            this.radGroupBox1.Controls.Add(this.radCheckBox2);
            this.radGroupBox1.Controls.Add(this.radCheckBox1);
            this.radGroupBox1.Controls.Add(this.radGroupBox2);
            this.radGroupBox1.Controls.Add(this.radLabel10);
            this.radGroupBox1.Controls.Add(this.radLabel9);
            this.radGroupBox1.Controls.Add(this.radLabel8);
            this.radGroupBox1.Controls.Add(this.radLabel7);
            this.radGroupBox1.Controls.Add(this.radDropDownList2);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.radDropDownList1);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.HeaderText = "Settings";
            this.radGroupBox1.Location = new System.Drawing.Point(3, 42);
            this.radGroupBox1.Name = "radGroupBox1";
            // 
            // 
            // 
            this.radGroupBox1.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.radGroupBox1.Size = new System.Drawing.Size(187, 429);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = "Settings";
            // 
            // radDropDownList3
            // 
            this.radDropDownList3.Location = new System.Drawing.Point(68, 106);
            this.radDropDownList3.Name = "radDropDownList3";
            this.radDropDownList3.Size = new System.Drawing.Size(107, 20);
            this.radDropDownList3.TabIndex = 24;
            this.radDropDownList3.Tag = "Right";
            this.radDropDownList3.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(5, 106);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(60, 18);
            this.radLabel11.TabIndex = 23;
            this.radLabel11.Text = "SnapMode";
            // 
            // radCheckBox3
            // 
            this.radCheckBox3.Location = new System.Drawing.Point(5, 399);
            this.radCheckBox3.Name = "radCheckBox3";
            this.radCheckBox3.Size = new System.Drawing.Size(86, 18);
            this.radCheckBox3.TabIndex = 17;
            this.radCheckBox3.Text = "ShowButtons";
            // 
            // radSpinEditor4
            // 
            this.radSpinEditor4.Location = new System.Drawing.Point(115, 247);
            this.radSpinEditor4.Name = "radSpinEditor4";
            this.radSpinEditor4.Size = new System.Drawing.Size(58, 20);
            this.radSpinEditor4.TabIndex = 22;
            this.radSpinEditor4.TabStop = false;
            this.radSpinEditor4.Tag = "Right";
            // 
            // radSpinEditor3
            // 
            this.radSpinEditor3.Location = new System.Drawing.Point(115, 208);
            this.radSpinEditor3.Name = "radSpinEditor3";
            this.radSpinEditor3.Size = new System.Drawing.Size(58, 20);
            this.radSpinEditor3.TabIndex = 21;
            this.radSpinEditor3.TabStop = false;
            this.radSpinEditor3.Tag = "Right";
            // 
            // radSpinEditor2
            // 
            this.radSpinEditor2.Location = new System.Drawing.Point(115, 174);
            this.radSpinEditor2.Name = "radSpinEditor2";
            this.radSpinEditor2.Size = new System.Drawing.Size(58, 20);
            this.radSpinEditor2.TabIndex = 20;
            this.radSpinEditor2.TabStop = false;
            this.radSpinEditor2.Tag = "Right";
            // 
            // radSpinEditor1
            // 
            this.radSpinEditor1.Location = new System.Drawing.Point(115, 138);
            this.radSpinEditor1.Name = "radSpinEditor1";
            this.radSpinEditor1.Size = new System.Drawing.Size(58, 20);
            this.radSpinEditor1.TabIndex = 19;
            this.radSpinEditor1.TabStop = false;
            this.radSpinEditor1.Tag = "Right";
            // 
            // radCheckBox2
            // 
            this.radCheckBox2.Location = new System.Drawing.Point(5, 375);
            this.radCheckBox2.Name = "radCheckBox2";
            this.radCheckBox2.Size = new System.Drawing.Size(94, 18);
            this.radCheckBox2.TabIndex = 16;
            this.radCheckBox2.Text = "ShowSlideArea";
            // 
            // radCheckBox1
            // 
            this.radCheckBox1.Location = new System.Drawing.Point(5, 351);
            this.radCheckBox1.Name = "radCheckBox1";
            this.radCheckBox1.Size = new System.Drawing.Size(78, 18);
            this.radCheckBox1.TabIndex = 15;
            this.radCheckBox1.Text = "RightToLeft";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.radRadioButton1);
            this.radGroupBox2.Controls.Add(this.radRadioButton2);
            this.radGroupBox2.HeaderText = "Orientation";
            this.radGroupBox2.Location = new System.Drawing.Point(5, 288);
            this.radGroupBox2.Name = "radGroupBox2";
            // 
            // 
            // 
            this.radGroupBox2.RootElement.Padding = new System.Windows.Forms.Padding(2, 18, 2, 2);
            this.radGroupBox2.Size = new System.Drawing.Size(170, 53);
            this.radGroupBox2.TabIndex = 14;
            this.radGroupBox2.Text = "Orientation";
            // 
            // radRadioButton1
            // 
            this.radRadioButton1.Location = new System.Drawing.Point(5, 21);
            this.radRadioButton1.Name = "radRadioButton1";
            this.radRadioButton1.Size = new System.Drawing.Size(72, 18);
            this.radRadioButton1.TabIndex = 0;
            this.radRadioButton1.Text = "Horizontal";
            // 
            // radRadioButton2
            // 
            this.radRadioButton2.Location = new System.Drawing.Point(83, 21);
            this.radRadioButton2.Name = "radRadioButton2";
            this.radRadioButton2.Size = new System.Drawing.Size(57, 18);
            this.radRadioButton2.TabIndex = 1;
            this.radRadioButton2.Text = "Vertical";
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(5, 249);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(78, 18);
            this.radLabel10.TabIndex = 12;
            this.radLabel10.Text = "Thumb Height";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(5, 212);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(75, 18);
            this.radLabel9.TabIndex = 10;
            this.radLabel9.Text = "Thumb Width";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(5, 176);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(104, 18);
            this.radLabel8.TabIndex = 8;
            this.radLabel8.Text = "SmallTickFrequency";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(5, 138);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(105, 18);
            this.radLabel7.TabIndex = 6;
            this.radLabel7.Text = "LargeTickFrequency";
            // 
            // radDropDownList2
            // 
            this.radDropDownList2.Location = new System.Drawing.Point(68, 76);
            this.radDropDownList2.Name = "radDropDownList2";
            this.radDropDownList2.Size = new System.Drawing.Size(107, 20);
            this.radDropDownList2.TabIndex = 5;
            this.radDropDownList2.Tag = "Right";
            this.radDropDownList2.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(5, 78);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(57, 18);
            this.radLabel6.TabIndex = 4;
            this.radLabel6.Text = "LabelStyle";
            // 
            // radDropDownList1
            // 
            this.radDropDownList1.Location = new System.Drawing.Point(68, 44);
            this.radDropDownList1.Name = "radDropDownList1";
            this.radDropDownList1.Size = new System.Drawing.Size(107, 20);
            this.radDropDownList1.TabIndex = 3;
            this.radDropDownList1.Tag = "Right";
            this.radDropDownList1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(5, 44);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(50, 18);
            this.radLabel5.TabIndex = 2;
            this.radLabel5.Text = "TickStyle";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(1, 2);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(104, 18);
            this.radLabel1.TabIndex = 5;
            this.radLabel1.Text = "SingleThumb Mode";
            // 
            // radTrackBar1
            // 
            this.radTrackBar1.LabelStyle = Telerik.WinControls.UI.TrackBarLabelStyle.Both;
            this.radTrackBar1.Location = new System.Drawing.Point(1, 19);
            this.radTrackBar1.Name = "radTrackBar1";
            this.radTrackBar1.Size = new System.Drawing.Size(500, 73);
            this.radTrackBar1.TabIndex = 9;
            this.radTrackBar1.Text = "radTrackBar1";
            // 
            // radTrackBar2
            // 
            this.radTrackBar2.LabelStyle = Telerik.WinControls.UI.TrackBarLabelStyle.Both;
            this.radTrackBar2.Location = new System.Drawing.Point(1, 119);
            this.radTrackBar2.Name = "radTrackBar2";
            this.radTrackBar2.Size = new System.Drawing.Size(500, 73);
            this.radTrackBar2.TabIndex = 11;
            this.radTrackBar2.Text = "radTrackBar2";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(1, 102);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(70, 18);
            this.radLabel2.TabIndex = 10;
            this.radLabel2.Text = "Range Mode (with a single range)";
            // 
            // radTrackBar3
            // 
            this.radTrackBar3.LabelStyle = Telerik.WinControls.UI.TrackBarLabelStyle.Both;
            this.radTrackBar3.Location = new System.Drawing.Point(1, 219);
            this.radTrackBar3.Name = "radTrackBar3";
            this.radTrackBar3.Size = new System.Drawing.Size(500, 73);
            this.radTrackBar3.TabIndex = 13;
            this.radTrackBar3.Text = "radTrackBar3";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(1, 202);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(115, 18);
            this.radLabel3.TabIndex = 12;
            this.radLabel3.Text = "Range Mode (with multiple ranges)";
            // 
            // radTrackBar4
            // 
            this.radTrackBar4.LabelStyle = Telerik.WinControls.UI.TrackBarLabelStyle.Both;
            this.radTrackBar4.Location = new System.Drawing.Point(1, 319);
            this.radTrackBar4.Name = "radTrackBar4";
            this.radTrackBar4.Size = new System.Drawing.Size(500, 73);
            this.radTrackBar4.TabIndex = 15;
            this.radTrackBar4.Text = "radTrackBar4";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(1, 302);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(157, 18);
            this.radLabel4.TabIndex = 14;
            this.radLabel4.Text = "StartFromTheBeginning Mode";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radTrackBar4);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.radTrackBar3);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radTrackBar2);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.radTrackBar1);
            this.Controls.Add(this.radLabel1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1063, 599);
            this.Controls.SetChildIndex(this.radLabel1, 0);
            this.Controls.SetChildIndex(this.radTrackBar1, 0);
            this.Controls.SetChildIndex(this.radLabel2, 0);
            this.Controls.SetChildIndex(this.radTrackBar2, 0);
            this.Controls.SetChildIndex(this.radLabel3, 0);
            this.Controls.SetChildIndex(this.radTrackBar3, 0);
            this.Controls.SetChildIndex(this.radLabel4, 0);
            this.Controls.SetChildIndex(this.radTrackBar4, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTrackBar4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar1;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar2;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar3;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadTrackBar radTrackBar4;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox3;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox2;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton1;
        private Telerik.WinControls.UI.RadRadioButton radRadioButton2;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList2;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList1;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor4;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor3;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor2;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor1;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList3;
        private Telerik.WinControls.UI.RadLabel radLabel11;

    }
}