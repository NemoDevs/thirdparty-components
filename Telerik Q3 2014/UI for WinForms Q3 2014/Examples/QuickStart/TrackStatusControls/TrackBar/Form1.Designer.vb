Namespace Telerik.Examples.WinControls.TrackStatusControls.TrackBar
	Partial Public Class Form1
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.radGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
			Me.radDropDownList3 = New Telerik.WinControls.UI.RadDropDownList()
			Me.radLabel11 = New Telerik.WinControls.UI.RadLabel()
			Me.radCheckBox3 = New Telerik.WinControls.UI.RadCheckBox()
			Me.radSpinEditor4 = New Telerik.WinControls.UI.RadSpinEditor()
			Me.radSpinEditor3 = New Telerik.WinControls.UI.RadSpinEditor()
			Me.radSpinEditor2 = New Telerik.WinControls.UI.RadSpinEditor()
			Me.radSpinEditor1 = New Telerik.WinControls.UI.RadSpinEditor()
			Me.radCheckBox2 = New Telerik.WinControls.UI.RadCheckBox()
			Me.radCheckBox1 = New Telerik.WinControls.UI.RadCheckBox()
			Me.radGroupBox2 = New Telerik.WinControls.UI.RadGroupBox()
			Me.radRadioButton1 = New Telerik.WinControls.UI.RadRadioButton()
			Me.radRadioButton2 = New Telerik.WinControls.UI.RadRadioButton()
			Me.radLabel10 = New Telerik.WinControls.UI.RadLabel()
			Me.radLabel9 = New Telerik.WinControls.UI.RadLabel()
			Me.radLabel8 = New Telerik.WinControls.UI.RadLabel()
			Me.radLabel7 = New Telerik.WinControls.UI.RadLabel()
			Me.radDropDownList2 = New Telerik.WinControls.UI.RadDropDownList()
			Me.radLabel6 = New Telerik.WinControls.UI.RadLabel()
			Me.radDropDownList1 = New Telerik.WinControls.UI.RadDropDownList()
			Me.radLabel5 = New Telerik.WinControls.UI.RadLabel()
			Me.radLabel1 = New Telerik.WinControls.UI.RadLabel()
			Me.radTrackBar1 = New Telerik.WinControls.UI.RadTrackBar()
			Me.radTrackBar2 = New Telerik.WinControls.UI.RadTrackBar()
			Me.radLabel2 = New Telerik.WinControls.UI.RadLabel()
			Me.radTrackBar3 = New Telerik.WinControls.UI.RadTrackBar()
			Me.radLabel3 = New Telerik.WinControls.UI.RadLabel()
			Me.radTrackBar4 = New Telerik.WinControls.UI.RadTrackBar()
			Me.radLabel4 = New Telerik.WinControls.UI.RadLabel()
			CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.settingsPanel.SuspendLayout()
			CType(Me.radGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.radGroupBox1.SuspendLayout()
			CType(Me.radDropDownList3, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel11, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radCheckBox3, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radSpinEditor4, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radSpinEditor3, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radSpinEditor2, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radSpinEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radCheckBox2, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radCheckBox1, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.radGroupBox2.SuspendLayout()
			CType(Me.radRadioButton1, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radRadioButton2, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel10, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel9, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel8, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel7, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radDropDownList2, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel6, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radDropDownList1, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel5, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radTrackBar1, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radTrackBar2, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radTrackBar3, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radTrackBar4, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radLabel4, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.SuspendLayout()
			' 
			' settingsPanel
			' 
			Me.settingsPanel.Controls.Add(Me.radGroupBox1)
			Me.settingsPanel.Location = New Point(700, 1)
			Me.settingsPanel.Size = New Size(348, 773)
			Me.settingsPanel.Controls.SetChildIndex(Me.radGroupBox1, 0)
			' 
			' radGroupBox1
			' 
			Me.radGroupBox1.AccessibleRole = AccessibleRole.Grouping
			Me.radGroupBox1.Controls.Add(Me.radDropDownList3)
			Me.radGroupBox1.Controls.Add(Me.radLabel11)
			Me.radGroupBox1.Controls.Add(Me.radCheckBox3)
			Me.radGroupBox1.Controls.Add(Me.radSpinEditor4)
			Me.radGroupBox1.Controls.Add(Me.radSpinEditor3)
			Me.radGroupBox1.Controls.Add(Me.radSpinEditor2)
			Me.radGroupBox1.Controls.Add(Me.radSpinEditor1)
			Me.radGroupBox1.Controls.Add(Me.radCheckBox2)
			Me.radGroupBox1.Controls.Add(Me.radCheckBox1)
			Me.radGroupBox1.Controls.Add(Me.radGroupBox2)
			Me.radGroupBox1.Controls.Add(Me.radLabel10)
			Me.radGroupBox1.Controls.Add(Me.radLabel9)
			Me.radGroupBox1.Controls.Add(Me.radLabel8)
			Me.radGroupBox1.Controls.Add(Me.radLabel7)
			Me.radGroupBox1.Controls.Add(Me.radDropDownList2)
			Me.radGroupBox1.Controls.Add(Me.radLabel6)
			Me.radGroupBox1.Controls.Add(Me.radDropDownList1)
			Me.radGroupBox1.Controls.Add(Me.radLabel5)
			Me.radGroupBox1.HeaderText = "Settings"
			Me.radGroupBox1.Location = New Point(3, 42)
			Me.radGroupBox1.Name = "radGroupBox1"
			' 
			' 
			' 
			Me.radGroupBox1.RootElement.Padding = New Padding(2, 18, 2, 2)
			Me.radGroupBox1.Size = New Size(187, 429)
			Me.radGroupBox1.TabIndex = 1
			Me.radGroupBox1.Text = "Settings"
			' 
			' radDropDownList3
			' 
			Me.radDropDownList3.Location = New Point(68, 106)
			Me.radDropDownList3.Name = "radDropDownList3"
			Me.radDropDownList3.Size = New Size(107, 20)
			Me.radDropDownList3.TabIndex = 24
            Me.radDropDownList3.Tag = "Right"
            Me.radDropDownList3.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
			' 
			' radLabel11
			' 
			Me.radLabel11.Location = New Point(5, 106)
			Me.radLabel11.Name = "radLabel11"
			Me.radLabel11.Size = New Size(60, 18)
			Me.radLabel11.TabIndex = 23
			Me.radLabel11.Text = "SnapMode"
			' 
			' radCheckBox3
			' 
			Me.radCheckBox3.Location = New Point(5, 399)
			Me.radCheckBox3.Name = "radCheckBox3"
			Me.radCheckBox3.Size = New Size(86, 18)
			Me.radCheckBox3.TabIndex = 17
			Me.radCheckBox3.Text = "ShowButtons"
			' 
			' radSpinEditor4
			' 
			Me.radSpinEditor4.Location = New Point(115, 247)
			Me.radSpinEditor4.Name = "radSpinEditor4"
			Me.radSpinEditor4.Size = New Size(58, 20)
			Me.radSpinEditor4.TabIndex = 22
			Me.radSpinEditor4.TabStop = False
			Me.radSpinEditor4.Tag = "Right"
			' 
			' radSpinEditor3
			' 
			Me.radSpinEditor3.Location = New Point(115, 208)
			Me.radSpinEditor3.Name = "radSpinEditor3"
			Me.radSpinEditor3.Size = New Size(58, 20)
			Me.radSpinEditor3.TabIndex = 21
			Me.radSpinEditor3.TabStop = False
			Me.radSpinEditor3.Tag = "Right"
			' 
			' radSpinEditor2
			' 
			Me.radSpinEditor2.Location = New Point(115, 174)
			Me.radSpinEditor2.Name = "radSpinEditor2"
			Me.radSpinEditor2.Size = New Size(58, 20)
			Me.radSpinEditor2.TabIndex = 20
			Me.radSpinEditor2.TabStop = False
			Me.radSpinEditor2.Tag = "Right"
			' 
			' radSpinEditor1
			' 
			Me.radSpinEditor1.Location = New Point(115, 138)
			Me.radSpinEditor1.Name = "radSpinEditor1"
			Me.radSpinEditor1.Size = New Size(58, 20)
			Me.radSpinEditor1.TabIndex = 19
			Me.radSpinEditor1.TabStop = False
			Me.radSpinEditor1.Tag = "Right"
			' 
			' radCheckBox2
			' 
			Me.radCheckBox2.Location = New Point(5, 375)
			Me.radCheckBox2.Name = "radCheckBox2"
			Me.radCheckBox2.Size = New Size(94, 18)
			Me.radCheckBox2.TabIndex = 16
			Me.radCheckBox2.Text = "ShowSlideArea"
			' 
			' radCheckBox1
			' 
			Me.radCheckBox1.Location = New Point(5, 351)
			Me.radCheckBox1.Name = "radCheckBox1"
			Me.radCheckBox1.Size = New Size(78, 18)
			Me.radCheckBox1.TabIndex = 15
			Me.radCheckBox1.Text = "RightToLeft"
			' 
			' radGroupBox2
			' 
			Me.radGroupBox2.AccessibleRole = AccessibleRole.Grouping
			Me.radGroupBox2.Controls.Add(Me.radRadioButton1)
			Me.radGroupBox2.Controls.Add(Me.radRadioButton2)
			Me.radGroupBox2.HeaderText = "Orientation"
			Me.radGroupBox2.Location = New Point(5, 288)
			Me.radGroupBox2.Name = "radGroupBox2"
			' 
			' 
			' 
			Me.radGroupBox2.RootElement.Padding = New Padding(2, 18, 2, 2)
			Me.radGroupBox2.Size = New Size(170, 53)
			Me.radGroupBox2.TabIndex = 14
			Me.radGroupBox2.Text = "Orientation"
			' 
			' radRadioButton1
			' 
			Me.radRadioButton1.Location = New Point(5, 21)
			Me.radRadioButton1.Name = "radRadioButton1"
			Me.radRadioButton1.Size = New Size(72, 18)
			Me.radRadioButton1.TabIndex = 0
			Me.radRadioButton1.Text = "Horizontal"
			' 
			' radRadioButton2
			' 
			Me.radRadioButton2.Location = New Point(83, 21)
			Me.radRadioButton2.Name = "radRadioButton2"
			Me.radRadioButton2.Size = New Size(57, 18)
			Me.radRadioButton2.TabIndex = 1
			Me.radRadioButton2.Text = "Vertical"
			' 
			' radLabel10
			' 
			Me.radLabel10.Location = New Point(5, 249)
			Me.radLabel10.Name = "radLabel10"
			Me.radLabel10.Size = New Size(78, 18)
			Me.radLabel10.TabIndex = 12
			Me.radLabel10.Text = "Thumb Height"
			' 
			' radLabel9
			' 
			Me.radLabel9.Location = New Point(5, 212)
			Me.radLabel9.Name = "radLabel9"
			Me.radLabel9.Size = New Size(75, 18)
			Me.radLabel9.TabIndex = 10
			Me.radLabel9.Text = "Thumb Width"
			' 
			' radLabel8
			' 
			Me.radLabel8.Location = New Point(5, 176)
			Me.radLabel8.Name = "radLabel8"
			Me.radLabel8.Size = New Size(104, 18)
			Me.radLabel8.TabIndex = 8
			Me.radLabel8.Text = "SmallTickFrequency"
			' 
			' radLabel7
			' 
			Me.radLabel7.Location = New Point(5, 138)
			Me.radLabel7.Name = "radLabel7"
			Me.radLabel7.Size = New Size(105, 18)
			Me.radLabel7.TabIndex = 6
			Me.radLabel7.Text = "LargeTickFrequency"
			' 
			' radDropDownList2
			' 
			Me.radDropDownList2.Location = New Point(68, 76)
			Me.radDropDownList2.Name = "radDropDownList2"
			Me.radDropDownList2.Size = New Size(107, 20)
			Me.radDropDownList2.TabIndex = 5
            Me.radDropDownList2.Tag = "Right"
            Me.radDropDownList2.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
			' 
			' radLabel6
			' 
			Me.radLabel6.Location = New Point(5, 78)
			Me.radLabel6.Name = "radLabel6"
			Me.radLabel6.Size = New Size(57, 18)
			Me.radLabel6.TabIndex = 4
			Me.radLabel6.Text = "LabelStyle"
			' 
			' radDropDownList1
			' 
			Me.radDropDownList1.Location = New Point(68, 44)
			Me.radDropDownList1.Name = "radDropDownList1"
			Me.radDropDownList1.Size = New Size(107, 20)
			Me.radDropDownList1.TabIndex = 3
            Me.radDropDownList1.Tag = "Right"
            Me.radDropDownList1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
			' 
			' radLabel5
			' 
			Me.radLabel5.Location = New Point(5, 44)
			Me.radLabel5.Name = "radLabel5"
			Me.radLabel5.Size = New Size(50, 18)
			Me.radLabel5.TabIndex = 2
			Me.radLabel5.Text = "TickStyle"
			' 
			' radLabel1
			' 
			Me.radLabel1.Location = New Point(1, 2)
			Me.radLabel1.Name = "radLabel1"
			Me.radLabel1.Size = New Size(104, 18)
			Me.radLabel1.TabIndex = 5
			Me.radLabel1.Text = "SingleThumb Mode"
			' 
			' radTrackBar1
			' 
			Me.radTrackBar1.LabelStyle = Telerik.WinControls.UI.TrackBarLabelStyle.Both
			Me.radTrackBar1.Location = New Point(1, 19)
			Me.radTrackBar1.Name = "radTrackBar1"
			Me.radTrackBar1.Size = New Size(500, 73)
			Me.radTrackBar1.TabIndex = 9
			Me.radTrackBar1.Text = "radTrackBar1"
			' 
			' radTrackBar2
			' 
			Me.radTrackBar2.LabelStyle = Telerik.WinControls.UI.TrackBarLabelStyle.Both
			Me.radTrackBar2.Location = New Point(1, 119)
			Me.radTrackBar2.Name = "radTrackBar2"
			Me.radTrackBar2.Size = New Size(500, 73)
			Me.radTrackBar2.TabIndex = 11
			Me.radTrackBar2.Text = "radTrackBar2"
			' 
			' radLabel2
			' 
			Me.radLabel2.Location = New Point(1, 102)
			Me.radLabel2.Name = "radLabel2"
			Me.radLabel2.Size = New Size(70, 18)
			Me.radLabel2.TabIndex = 10
            Me.radLabel2.Text = "Range Mode (with a single range)"
			' 
			' radTrackBar3
			' 
			Me.radTrackBar3.LabelStyle = Telerik.WinControls.UI.TrackBarLabelStyle.Both
			Me.radTrackBar3.Location = New Point(1, 219)
			Me.radTrackBar3.Name = "radTrackBar3"
			Me.radTrackBar3.Size = New Size(500, 73)
			Me.radTrackBar3.TabIndex = 13
			Me.radTrackBar3.Text = "radTrackBar3"
			' 
			' radLabel3
			' 
			Me.radLabel3.Location = New Point(1, 202)
			Me.radLabel3.Name = "radLabel3"
			Me.radLabel3.Size = New Size(115, 18)
			Me.radLabel3.TabIndex = 12
            Me.radLabel3.Text = "Range Mode (with multiple ranges)"
			' 
			' radTrackBar4
			' 
			Me.radTrackBar4.LabelStyle = Telerik.WinControls.UI.TrackBarLabelStyle.Both
			Me.radTrackBar4.Location = New Point(1, 319)
			Me.radTrackBar4.Name = "radTrackBar4"
			Me.radTrackBar4.Size = New Size(500, 73)
			Me.radTrackBar4.TabIndex = 15
			Me.radTrackBar4.Text = "radTrackBar4"
			' 
			' radLabel4
			' 
			Me.radLabel4.Location = New Point(1, 302)
			Me.radLabel4.Name = "radLabel4"
			Me.radLabel4.Size = New Size(157, 18)
			Me.radLabel4.TabIndex = 14
			Me.radLabel4.Text = "StartFromTheBeginning Mode"
			' 
			' Form1
			' 
			Me.AutoScaleDimensions = New SizeF(6F, 13F)
			Me.AutoScaleMode = AutoScaleMode.Font
			Me.Controls.Add(Me.radTrackBar4)
			Me.Controls.Add(Me.radLabel4)
			Me.Controls.Add(Me.radTrackBar3)
			Me.Controls.Add(Me.radLabel3)
			Me.Controls.Add(Me.radTrackBar2)
			Me.Controls.Add(Me.radLabel2)
			Me.Controls.Add(Me.radTrackBar1)
			Me.Controls.Add(Me.radLabel1)
			Me.Name = "Form1"
			Me.Size = New Size(1063, 599)
			Me.Controls.SetChildIndex(Me.radLabel1, 0)
			Me.Controls.SetChildIndex(Me.radTrackBar1, 0)
			Me.Controls.SetChildIndex(Me.radLabel2, 0)
			Me.Controls.SetChildIndex(Me.radTrackBar2, 0)
			Me.Controls.SetChildIndex(Me.radLabel3, 0)
			Me.Controls.SetChildIndex(Me.radTrackBar3, 0)
			Me.Controls.SetChildIndex(Me.radLabel4, 0)
			Me.Controls.SetChildIndex(Me.radTrackBar4, 0)
			Me.Controls.SetChildIndex(Me.settingsPanel, 0)
			CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).EndInit()
			Me.settingsPanel.ResumeLayout(False)
			Me.settingsPanel.PerformLayout()
			CType(Me.radGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
			Me.radGroupBox1.ResumeLayout(False)
			Me.radGroupBox1.PerformLayout()
			CType(Me.radDropDownList3, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel11, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radCheckBox3, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radSpinEditor4, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radSpinEditor3, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radSpinEditor2, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radSpinEditor1, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radCheckBox2, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radCheckBox1, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
			Me.radGroupBox2.ResumeLayout(False)
			Me.radGroupBox2.PerformLayout()
			CType(Me.radRadioButton1, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radRadioButton2, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel10, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel9, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel8, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel7, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radDropDownList2, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel6, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radDropDownList1, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel5, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radTrackBar1, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radTrackBar2, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radTrackBar3, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel3, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radTrackBar4, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radLabel4, System.ComponentModel.ISupportInitialize).EndInit()
			Me.ResumeLayout(False)
			Me.PerformLayout()

		End Sub

		#End Region

		Private radGroupBox1 As Telerik.WinControls.UI.RadGroupBox
		Private radLabel1 As Telerik.WinControls.UI.RadLabel
		Private radTrackBar1 As Telerik.WinControls.UI.RadTrackBar
		Private radTrackBar2 As Telerik.WinControls.UI.RadTrackBar
		Private radLabel2 As Telerik.WinControls.UI.RadLabel
		Private radTrackBar3 As Telerik.WinControls.UI.RadTrackBar
		Private radLabel3 As Telerik.WinControls.UI.RadLabel
		Private radTrackBar4 As Telerik.WinControls.UI.RadTrackBar
		Private radLabel4 As Telerik.WinControls.UI.RadLabel
		Private radCheckBox3 As Telerik.WinControls.UI.RadCheckBox
		Private radCheckBox2 As Telerik.WinControls.UI.RadCheckBox
		Private radCheckBox1 As Telerik.WinControls.UI.RadCheckBox
		Private radGroupBox2 As Telerik.WinControls.UI.RadGroupBox
		Private radRadioButton1 As Telerik.WinControls.UI.RadRadioButton
		Private radRadioButton2 As Telerik.WinControls.UI.RadRadioButton
		Private radLabel10 As Telerik.WinControls.UI.RadLabel
		Private radLabel9 As Telerik.WinControls.UI.RadLabel
		Private radLabel8 As Telerik.WinControls.UI.RadLabel
		Private radLabel7 As Telerik.WinControls.UI.RadLabel
		Private radDropDownList2 As Telerik.WinControls.UI.RadDropDownList
		Private radLabel6 As Telerik.WinControls.UI.RadLabel
		Private radDropDownList1 As Telerik.WinControls.UI.RadDropDownList
		Private radLabel5 As Telerik.WinControls.UI.RadLabel
		Private radSpinEditor4 As Telerik.WinControls.UI.RadSpinEditor
		Private radSpinEditor3 As Telerik.WinControls.UI.RadSpinEditor
		Private radSpinEditor2 As Telerik.WinControls.UI.RadSpinEditor
		Private radSpinEditor1 As Telerik.WinControls.UI.RadSpinEditor
		Private radDropDownList3 As Telerik.WinControls.UI.RadDropDownList
		Private radLabel11 As Telerik.WinControls.UI.RadLabel

	End Class
End Namespace