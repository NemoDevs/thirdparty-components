namespace Telerik.Examples.WinControls.Calendar.Selection
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radCalendar1 = new Telerik.WinControls.UI.RadCalendar();
            this.radGroupAllowSelect = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioAllowSelectEnable = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioAllowSelectDisable = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupMultiSelect = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioMultiSelectEnable = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioMultiSelectDisable = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupShowRowHeader = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioShowRowHeaderEnable = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioShowRowHeaderDisable = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupAllowViewSelector = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioEnableAllowViewSelectorEnable = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioEnableAllowViewSelectorDisable = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupAllowRowHeader = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioAllowRowHeaderEnable = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioAllowRowHeaderDisable = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupAllowColumnHeader = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioAllowColumnHeaderEnable = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioAllowColumnHeaderDisable = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupColumnHeaders = new Telerik.WinControls.UI.RadGroupBox();
            this.radRadioColumnHeaderEnable = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioColumnHeaderDisable = new Telerik.WinControls.UI.RadRadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCalendar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupAllowSelect)).BeginInit();
            this.radGroupAllowSelect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioAllowSelectEnable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioAllowSelectDisable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupMultiSelect)).BeginInit();
            this.radGroupMultiSelect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioMultiSelectEnable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioMultiSelectDisable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupShowRowHeader)).BeginInit();
            this.radGroupShowRowHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioShowRowHeaderEnable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioShowRowHeaderDisable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupAllowViewSelector)).BeginInit();
            this.radGroupAllowViewSelector.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioEnableAllowViewSelectorEnable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioEnableAllowViewSelectorDisable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupAllowRowHeader)).BeginInit();
            this.radGroupAllowRowHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioAllowRowHeaderEnable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioAllowRowHeaderDisable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupAllowColumnHeader)).BeginInit();
            this.radGroupAllowColumnHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioAllowColumnHeaderEnable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioAllowColumnHeaderDisable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupColumnHeaders)).BeginInit();
            this.radGroupColumnHeaders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioColumnHeaderEnable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioColumnHeaderDisable)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.radGroupAllowSelect);
            this.settingsPanel.Controls.Add(this.radGroupMultiSelect);
            this.settingsPanel.Controls.Add(this.radGroupAllowColumnHeader);
            this.settingsPanel.Controls.Add(this.radGroupColumnHeaders);
            this.settingsPanel.Controls.Add(this.radGroupAllowRowHeader);
            this.settingsPanel.Controls.Add(this.radGroupAllowViewSelector);
            this.settingsPanel.Controls.Add(this.radGroupShowRowHeader);
            this.settingsPanel.Controls.Add(this.radButton1);
            this.settingsPanel.Location = new System.Drawing.Point(692, 1);
            this.settingsPanel.Size = new System.Drawing.Size(200, 806);
            this.settingsPanel.Controls.SetChildIndex(this.radButton1, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupShowRowHeader, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupAllowViewSelector, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupAllowRowHeader, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupColumnHeaders, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupAllowColumnHeader, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupMultiSelect, 0);
            this.settingsPanel.Controls.SetChildIndex(this.radGroupAllowSelect, 0);
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(10, 481);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(180, 23);
            this.radButton1.TabIndex = 8;
            this.radButton1.Text = "Clear Selections";

            // 
            // radCalendar1
            // 
            this.radCalendar1.FocusedDate = new System.DateTime(2009, 3, 5, 0, 0, 0, 0);
            this.radCalendar1.ForeColor = System.Drawing.Color.Black;
            this.radCalendar1.Location = new System.Drawing.Point(0, 0);
            this.radCalendar1.Name = "radCalendar1";
            this.radCalendar1.ShowViewSelector = true;
            this.radCalendar1.Size = new System.Drawing.Size(288, 229);
            this.radCalendar1.TabIndex = 0;
            this.radCalendar1.Text = "radCalendar1";
            this.radCalendar1.ZoomFactor = 1.2F;
            // 
            // radGroupAllowSelect
            // 
            this.radGroupAllowSelect.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupAllowSelect.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupAllowSelect.Controls.Add(this.radRadioAllowSelectEnable);
            this.radGroupAllowSelect.Controls.Add(this.radRadioAllowSelectDisable);
            this.radGroupAllowSelect.ForeColor = System.Drawing.Color.Black;
            this.radGroupAllowSelect.HeaderText = "Allow Select";
            this.radGroupAllowSelect.Location = new System.Drawing.Point(10, 6);
            this.radGroupAllowSelect.Name = "radGroupAllowSelect";
            this.radGroupAllowSelect.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupAllowSelect.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupAllowSelect.Size = new System.Drawing.Size(180, 66);
            this.radGroupAllowSelect.TabIndex = 6;
            this.radGroupAllowSelect.Text = "Allow Select";
            // 
            // radRadioAllowSelectEnable
            // 
            this.radRadioAllowSelectEnable.ForeColor = System.Drawing.Color.Black;
            this.radRadioAllowSelectEnable.Location = new System.Drawing.Point(17, 24);
            this.radRadioAllowSelectEnable.Name = "radRadioAllowSelectEnable";
            this.radRadioAllowSelectEnable.Size = new System.Drawing.Size(53, 18);
            this.radRadioAllowSelectEnable.TabIndex = 7;
            this.radRadioAllowSelectEnable.Text = "Enable";
           
            // 
            // radRadioAllowSelectDisable
            // 
            this.radRadioAllowSelectDisable.ForeColor = System.Drawing.Color.Black;
            this.radRadioAllowSelectDisable.Location = new System.Drawing.Point(17, 41);
            this.radRadioAllowSelectDisable.Name = "radRadioAllowSelectDisable";
            this.radRadioAllowSelectDisable.Size = new System.Drawing.Size(57, 18);
            this.radRadioAllowSelectDisable.TabIndex = 7;
            this.radRadioAllowSelectDisable.Text = "Disable";
        
            // 
            // radGroupMultiSelect
            // 
            this.radGroupMultiSelect.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupMultiSelect.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupMultiSelect.Controls.Add(this.radRadioMultiSelectEnable);
            this.radGroupMultiSelect.Controls.Add(this.radRadioMultiSelectDisable);
            this.radGroupMultiSelect.ForeColor = System.Drawing.Color.Black;
            this.radGroupMultiSelect.HeaderText = "Allow Multi Select";
            this.radGroupMultiSelect.Location = new System.Drawing.Point(10, 77);
            this.radGroupMultiSelect.Name = "radGroupMultiSelect";
            this.radGroupMultiSelect.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupMultiSelect.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupMultiSelect.Size = new System.Drawing.Size(180, 63);
            this.radGroupMultiSelect.TabIndex = 6;
            this.radGroupMultiSelect.Text = "Allow Multi Select";
            // 
            // radRadioMultiSelectEnable
            // 
            this.radRadioMultiSelectEnable.ForeColor = System.Drawing.Color.Black;
            this.radRadioMultiSelectEnable.Location = new System.Drawing.Point(20, 24);
            this.radRadioMultiSelectEnable.Name = "radRadioMultiSelectEnable";
            this.radRadioMultiSelectEnable.Size = new System.Drawing.Size(53, 18);
            this.radRadioMultiSelectEnable.TabIndex = 7;
            this.radRadioMultiSelectEnable.Text = "Enable";
          
            // 
            // radRadioMultiSelectDisable
            // 
            this.radRadioMultiSelectDisable.ForeColor = System.Drawing.Color.Black;
            this.radRadioMultiSelectDisable.Location = new System.Drawing.Point(20, 41);
            this.radRadioMultiSelectDisable.Name = "radRadioMultiSelectDisable";
            this.radRadioMultiSelectDisable.Size = new System.Drawing.Size(57, 18);
            this.radRadioMultiSelectDisable.TabIndex = 7;
            this.radRadioMultiSelectDisable.Text = "Disable";
         
            // 
            // radGroupShowRowHeader
            // 
            this.radGroupShowRowHeader.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupShowRowHeader.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupShowRowHeader.Controls.Add(this.radRadioShowRowHeaderEnable);
            this.radGroupShowRowHeader.Controls.Add(this.radRadioShowRowHeaderDisable);
            this.radGroupShowRowHeader.ForeColor = System.Drawing.Color.Black;
            this.radGroupShowRowHeader.HeaderText = "Show Row Selectors";
            this.radGroupShowRowHeader.Location = new System.Drawing.Point(10, 212);
            this.radGroupShowRowHeader.Name = "radGroupShowRowHeader";
            this.radGroupShowRowHeader.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupShowRowHeader.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupShowRowHeader.Size = new System.Drawing.Size(180, 60);
            this.radGroupShowRowHeader.TabIndex = 6;
            this.radGroupShowRowHeader.Text = "Show Row Selectors";
            // 
            // radRadioShowRowHeaderEnable
            // 
            this.radRadioShowRowHeaderEnable.ForeColor = System.Drawing.Color.Black;
            this.radRadioShowRowHeaderEnable.Location = new System.Drawing.Point(22, 23);
            this.radRadioShowRowHeaderEnable.Name = "radRadioShowRowHeaderEnable";
            this.radRadioShowRowHeaderEnable.Size = new System.Drawing.Size(53, 18);
            this.radRadioShowRowHeaderEnable.TabIndex = 7;
            this.radRadioShowRowHeaderEnable.Text = "Enable";
          
            // 
            // radRadioShowRowHeaderDisable
            // 
            this.radRadioShowRowHeaderDisable.ForeColor = System.Drawing.Color.Black;
            this.radRadioShowRowHeaderDisable.Location = new System.Drawing.Point(22, 40);
            this.radRadioShowRowHeaderDisable.Name = "radRadioShowRowHeaderDisable";
            this.radRadioShowRowHeaderDisable.Size = new System.Drawing.Size(57, 18);
            this.radRadioShowRowHeaderDisable.TabIndex = 7;
            this.radRadioShowRowHeaderDisable.Text = "Disable";
           
            // 
            // radGroupAllowViewSelector
            // 
            this.radGroupAllowViewSelector.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupAllowViewSelector.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupAllowViewSelector.Controls.Add(this.radRadioEnableAllowViewSelectorEnable);
            this.radGroupAllowViewSelector.Controls.Add(this.radRadioEnableAllowViewSelectorDisable);
            this.radGroupAllowViewSelector.ForeColor = System.Drawing.Color.Black;
            this.radGroupAllowViewSelector.HeaderText = "Allow View Selector";
            this.radGroupAllowViewSelector.Location = new System.Drawing.Point(10, 145);
            this.radGroupAllowViewSelector.Name = "radGroupAllowViewSelector";
            this.radGroupAllowViewSelector.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupAllowViewSelector.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupAllowViewSelector.Size = new System.Drawing.Size(180, 62);
            this.radGroupAllowViewSelector.TabIndex = 6;
            this.radGroupAllowViewSelector.Text = "Allow View Selector";
            // 
            // radRadioEnableAllowViewSelectorEnable
            // 
            this.radRadioEnableAllowViewSelectorEnable.ForeColor = System.Drawing.Color.Black;
            this.radRadioEnableAllowViewSelectorEnable.Location = new System.Drawing.Point(17, 23);
            this.radRadioEnableAllowViewSelectorEnable.Name = "radRadioEnableAllowViewSelectorEnable";
            this.radRadioEnableAllowViewSelectorEnable.Size = new System.Drawing.Size(53, 18);
            this.radRadioEnableAllowViewSelectorEnable.TabIndex = 7;
            this.radRadioEnableAllowViewSelectorEnable.Text = "Enable";          
            // 
            // radRadioEnableAllowViewSelectorDisable
            // 
            this.radRadioEnableAllowViewSelectorDisable.ForeColor = System.Drawing.Color.Black;
            this.radRadioEnableAllowViewSelectorDisable.Location = new System.Drawing.Point(17, 40);
            this.radRadioEnableAllowViewSelectorDisable.Name = "radRadioEnableAllowViewSelectorDisable";
            this.radRadioEnableAllowViewSelectorDisable.Size = new System.Drawing.Size(57, 18);
            this.radRadioEnableAllowViewSelectorDisable.TabIndex = 7;
            this.radRadioEnableAllowViewSelectorDisable.Text = "Disable";          
            // 
            // radGroupAllowRowHeader
            // 
            this.radGroupAllowRowHeader.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupAllowRowHeader.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupAllowRowHeader.Controls.Add(this.radRadioAllowRowHeaderEnable);
            this.radGroupAllowRowHeader.Controls.Add(this.radRadioAllowRowHeaderDisable);
            this.radGroupAllowRowHeader.ForeColor = System.Drawing.Color.Black;
            this.radGroupAllowRowHeader.HeaderText = "Allow Row Selectors";
            this.radGroupAllowRowHeader.Location = new System.Drawing.Point(10, 343);
            this.radGroupAllowRowHeader.Name = "radGroupAllowRowHeader";
            this.radGroupAllowRowHeader.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupAllowRowHeader.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupAllowRowHeader.Size = new System.Drawing.Size(180, 61);
            this.radGroupAllowRowHeader.TabIndex = 6;
            this.radGroupAllowRowHeader.Text = "Allow Row Selectors";
            // 
            // radRadioAllowRowHeaderEnable
            // 
            this.radRadioAllowRowHeaderEnable.ForeColor = System.Drawing.Color.Black;
            this.radRadioAllowRowHeaderEnable.Location = new System.Drawing.Point(20, 23);
            this.radRadioAllowRowHeaderEnable.Name = "radRadioAllowRowHeaderEnable";
            this.radRadioAllowRowHeaderEnable.Size = new System.Drawing.Size(53, 18);
            this.radRadioAllowRowHeaderEnable.TabIndex = 7;
            this.radRadioAllowRowHeaderEnable.Text = "Enable";
           
            // 
            // radRadioAllowRowHeaderDisable
            // 
            this.radRadioAllowRowHeaderDisable.ForeColor = System.Drawing.Color.Black;
            this.radRadioAllowRowHeaderDisable.Location = new System.Drawing.Point(20, 40);
            this.radRadioAllowRowHeaderDisable.Name = "radRadioAllowRowHeaderDisable";
            this.radRadioAllowRowHeaderDisable.Size = new System.Drawing.Size(57, 18);
            this.radRadioAllowRowHeaderDisable.TabIndex = 7;
            this.radRadioAllowRowHeaderDisable.Text = "Disable";
          
            // 
            // radGroupAllowColumnHeader
            // 
            this.radGroupAllowColumnHeader.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupAllowColumnHeader.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupAllowColumnHeader.Controls.Add(this.radRadioAllowColumnHeaderEnable);
            this.radGroupAllowColumnHeader.Controls.Add(this.radRadioAllowColumnHeaderDisable);
            this.radGroupAllowColumnHeader.ForeColor = System.Drawing.Color.Black;
            this.radGroupAllowColumnHeader.HeaderText = "Allow Column Selector";
            this.radGroupAllowColumnHeader.Location = new System.Drawing.Point(10, 409);
            this.radGroupAllowColumnHeader.Name = "radGroupAllowColumnHeader";
            this.radGroupAllowColumnHeader.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupAllowColumnHeader.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupAllowColumnHeader.Size = new System.Drawing.Size(180, 60);
            this.radGroupAllowColumnHeader.TabIndex = 6;
            this.radGroupAllowColumnHeader.Text = "Allow Column Selector";
            // 
            // radRadioAllowColumnHeaderEnable
            // 
            this.radRadioAllowColumnHeaderEnable.ForeColor = System.Drawing.Color.Black;
            this.radRadioAllowColumnHeaderEnable.Location = new System.Drawing.Point(20, 21);
            this.radRadioAllowColumnHeaderEnable.Name = "radRadioAllowColumnHeaderEnable";
            this.radRadioAllowColumnHeaderEnable.Size = new System.Drawing.Size(53, 18);
            this.radRadioAllowColumnHeaderEnable.TabIndex = 7;
            this.radRadioAllowColumnHeaderEnable.Text = "Enable";
          
            // 
            // radRadioAllowColumnHeaderDisable
            // 
            this.radRadioAllowColumnHeaderDisable.ForeColor = System.Drawing.Color.Black;
            this.radRadioAllowColumnHeaderDisable.Location = new System.Drawing.Point(20, 38);
            this.radRadioAllowColumnHeaderDisable.Name = "radRadioAllowColumnHeaderDisable";
            this.radRadioAllowColumnHeaderDisable.Size = new System.Drawing.Size(57, 18);
            this.radRadioAllowColumnHeaderDisable.TabIndex = 7;
            this.radRadioAllowColumnHeaderDisable.Text = "Disable";
         
            // 
            // radGroupColumnHeaders
            // 
            this.radGroupColumnHeaders.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupColumnHeaders.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupColumnHeaders.Controls.Add(this.radRadioColumnHeaderEnable);
            this.radGroupColumnHeaders.Controls.Add(this.radRadioColumnHeaderDisable);
            this.radGroupColumnHeaders.ForeColor = System.Drawing.Color.Black;
            this.radGroupColumnHeaders.HeaderText = "Show Column Selectors";
            this.radGroupColumnHeaders.Location = new System.Drawing.Point(10, 277);
            this.radGroupColumnHeaders.Name = "radGroupColumnHeaders";
            this.radGroupColumnHeaders.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            // 
            // 
            // 
            this.radGroupColumnHeaders.RootElement.Padding = new System.Windows.Forms.Padding(10, 20, 10, 10);
            this.radGroupColumnHeaders.Size = new System.Drawing.Size(180, 61);
            this.radGroupColumnHeaders.TabIndex = 6;
            this.radGroupColumnHeaders.Text = "Show Column Selectors";
            // 
            // radRadioColumnHeaderEnable
            // 
            this.radRadioColumnHeaderEnable.ForeColor = System.Drawing.Color.Black;
            this.radRadioColumnHeaderEnable.Location = new System.Drawing.Point(22, 23);
            this.radRadioColumnHeaderEnable.Name = "radRadioColumnHeaderEnable";
            this.radRadioColumnHeaderEnable.Size = new System.Drawing.Size(53, 18);
            this.radRadioColumnHeaderEnable.TabIndex = 7;
            this.radRadioColumnHeaderEnable.Text = "Enable";
        
            // 
            // radRadioColumnHeaderDisable
            // 
            this.radRadioColumnHeaderDisable.ForeColor = System.Drawing.Color.Black;
            this.radRadioColumnHeaderDisable.Location = new System.Drawing.Point(22, 40);
            this.radRadioColumnHeaderDisable.Name = "radRadioColumnHeaderDisable";
            this.radRadioColumnHeaderDisable.Size = new System.Drawing.Size(57, 18);
            this.radRadioColumnHeaderDisable.TabIndex = 7;
            this.radRadioColumnHeaderDisable.Text = "Disable";
          
            // 
            // Form1
            // 
            this.Controls.Add(this.radCalendar1);
            this.Name = "Form1";
            this.Size = new System.Drawing.Size(1170, 754);
            this.Controls.SetChildIndex(this.radCalendar1, 0);
            this.Controls.SetChildIndex(this.settingsPanel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCalendar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupAllowSelect)).EndInit();
            this.radGroupAllowSelect.ResumeLayout(false);
            this.radGroupAllowSelect.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioAllowSelectEnable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioAllowSelectDisable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupMultiSelect)).EndInit();
            this.radGroupMultiSelect.ResumeLayout(false);
            this.radGroupMultiSelect.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioMultiSelectEnable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioMultiSelectDisable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupShowRowHeader)).EndInit();
            this.radGroupShowRowHeader.ResumeLayout(false);
            this.radGroupShowRowHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioShowRowHeaderEnable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioShowRowHeaderDisable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupAllowViewSelector)).EndInit();
            this.radGroupAllowViewSelector.ResumeLayout(false);
            this.radGroupAllowViewSelector.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioEnableAllowViewSelectorEnable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioEnableAllowViewSelectorDisable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupAllowRowHeader)).EndInit();
            this.radGroupAllowRowHeader.ResumeLayout(false);
            this.radGroupAllowRowHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioAllowRowHeaderEnable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioAllowRowHeaderDisable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupAllowColumnHeader)).EndInit();
            this.radGroupAllowColumnHeader.ResumeLayout(false);
            this.radGroupAllowColumnHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioAllowColumnHeaderEnable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioAllowColumnHeaderDisable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupColumnHeaders)).EndInit();
            this.radGroupColumnHeaders.ResumeLayout(false);
            this.radGroupColumnHeaders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioColumnHeaderEnable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioColumnHeaderDisable)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

        private Telerik.WinControls.UI.RadCalendar radCalendar1;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadGroupBox radGroupAllowSelect;
        private Telerik.WinControls.UI.RadGroupBox radGroupMultiSelect;
        private Telerik.WinControls.UI.RadGroupBox radGroupShowRowHeader;
        private Telerik.WinControls.UI.RadGroupBox radGroupAllowViewSelector;
        private Telerik.WinControls.UI.RadGroupBox radGroupAllowRowHeader;
        private Telerik.WinControls.UI.RadGroupBox radGroupAllowColumnHeader;
        private Telerik.WinControls.UI.RadGroupBox radGroupColumnHeaders;
        private Telerik.WinControls.UI.RadRadioButton radRadioAllowSelectEnable;
        private Telerik.WinControls.UI.RadRadioButton radRadioAllowSelectDisable;
        private Telerik.WinControls.UI.RadRadioButton radRadioMultiSelectDisable;
        private Telerik.WinControls.UI.RadRadioButton radRadioMultiSelectEnable;
        private Telerik.WinControls.UI.RadRadioButton radRadioEnableAllowViewSelectorDisable;
        private Telerik.WinControls.UI.RadRadioButton radRadioEnableAllowViewSelectorEnable;
        private Telerik.WinControls.UI.RadRadioButton radRadioShowRowHeaderEnable;
        private Telerik.WinControls.UI.RadRadioButton radRadioShowRowHeaderDisable;
        private Telerik.WinControls.UI.RadRadioButton radRadioColumnHeaderDisable;
        private Telerik.WinControls.UI.RadRadioButton radRadioColumnHeaderEnable;
        private Telerik.WinControls.UI.RadRadioButton radRadioAllowRowHeaderEnable;
        private Telerik.WinControls.UI.RadRadioButton radRadioAllowRowHeaderDisable;
        private Telerik.WinControls.UI.RadRadioButton radRadioAllowColumnHeaderEnable;
        private Telerik.WinControls.UI.RadRadioButton radRadioAllowColumnHeaderDisable;
	}
}