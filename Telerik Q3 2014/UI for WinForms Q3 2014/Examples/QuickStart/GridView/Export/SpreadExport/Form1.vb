﻿Imports System
Imports System.Linq
Imports Telerik.QuickStart.WinControls

Namespace Telerik.Examples.WinControls.GridView.Export.SpreadExport
    Partial Public Class Form1
        Inherits ExternalProcessForm
        Public Sub New(themeName As String)
            Me.ThemeName = themeName
        End Sub

        Protected Overrides Function GetExecutablePath() As String
            Return "\..\..\SpreadExport\bin\SpreadExport.exe"
        End Function
    End Class
End Namespace