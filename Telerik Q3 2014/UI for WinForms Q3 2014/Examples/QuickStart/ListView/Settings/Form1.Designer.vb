﻿Namespace Telerik.Examples.WinControls.ListView.Settings
	Partial Public Class Form1
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
            Dim ListViewDetailColumn1 As Telerik.WinControls.UI.ListViewDetailColumn = New Telerik.WinControls.UI.ListViewDetailColumn("Column 0", "File Name")
            Dim ListViewDetailColumn2 As Telerik.WinControls.UI.ListViewDetailColumn = New Telerik.WinControls.UI.ListViewDetailColumn("Column 1", "Total Size")
            Dim ListViewDetailColumn3 As Telerik.WinControls.UI.ListViewDetailColumn = New Telerik.WinControls.UI.ListViewDetailColumn("Column 2", "Type")
            Dim ListViewDetailColumn4 As Telerik.WinControls.UI.ListViewDetailColumn = New Telerik.WinControls.UI.ListViewDetailColumn("Column 3", "Free Space")
            Dim ListViewDataItemGroup1 As Telerik.WinControls.UI.ListViewDataItemGroup = New Telerik.WinControls.UI.ListViewDataItemGroup("Files Stored on This Computer")
            Dim ListViewDataItemGroup2 As Telerik.WinControls.UI.ListViewDataItemGroup = New Telerik.WinControls.UI.ListViewDataItemGroup("Hard Disk Drives")
            Dim ListViewDataItemGroup3 As Telerik.WinControls.UI.ListViewDataItemGroup = New Telerik.WinControls.UI.ListViewDataItemGroup("Devices with Removable Storage")
            Dim ListViewDataItem1 As Telerik.WinControls.UI.ListViewDataItem = New Telerik.WinControls.UI.ListViewDataItem("Shared Documents", New String() {"Shared Documents", "", "File Folder"})
            Dim ListViewDataItem2 As Telerik.WinControls.UI.ListViewDataItem = New Telerik.WinControls.UI.ListViewDataItem("Administrator's Documents", New String() {"Administrator's Documents", "", "File Folder"})
            Dim ListViewDataItem3 As Telerik.WinControls.UI.ListViewDataItem = New Telerik.WinControls.UI.ListViewDataItem("3.5 Floppy (A:)", New String() {"3.5 Floppy (A:)", "1.44 MB", "Floppy Drive", "203 KB"})
            Dim ListViewDataItem4 As Telerik.WinControls.UI.ListViewDataItem = New Telerik.WinControls.UI.ListViewDataItem("Local Disk (C:)", New String() {"Local Disk (C:)", "160.4 GB", "Local Disk", "31.02 GB"})
            Dim ListViewDataItem5 As Telerik.WinControls.UI.ListViewDataItem = New Telerik.WinControls.UI.ListViewDataItem("Local Disk (D:)", New String() {"Local Disk (D:)", "136.2 GB", "Local Disk", "57.52 GB"})
            Dim ListViewDataItem6 As Telerik.WinControls.UI.ListViewDataItem = New Telerik.WinControls.UI.ListViewDataItem("Local Disk (E:)", New String() {"Local Disk (E:)", "40.00 GB", "Local Disk", "13.37 GB"})
            Dim ListViewDataItem7 As Telerik.WinControls.UI.ListViewDataItem = New Telerik.WinControls.UI.ListViewDataItem("Local Disk (F:)", New String() {"Local Disk (F:)", "0.99 TB", "Local Disk", "357.37 GB"})
            Dim ListViewDataItem8 As Telerik.WinControls.UI.ListViewDataItem = New Telerik.WinControls.UI.ListViewDataItem("DVD/CD-RW Drive (G:)", New String() {"DVD/CD-RW (G:)", "4.7 GB", "DVD Drive", "0 KB"})
            Dim ListViewDataItem9 As Telerik.WinControls.UI.ListViewDataItem = New Telerik.WinControls.UI.ListViewDataItem("CD-RW Drive (H:)", New String() {"CD-RW (G:)", "", "CD Drive"})
            Dim RadListDataItem4 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem5 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem1 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem2 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Dim RadListDataItem3 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
            Me.radListView1 = New Telerik.WinControls.UI.RadListView()
            Me.radCheckBoxShowCheckboxes = New Telerik.WinControls.UI.RadCheckBox()
            Me.radCheckBoxGroups = New Telerik.WinControls.UI.RadCheckBox()
            Me.radGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
            Me.radCheckBoxKeyboardNavigation = New Telerik.WinControls.UI.RadCheckBox()
            Me.radCheckBoxFullRowSelect = New Telerik.WinControls.UI.RadCheckBox()
            Me.radLabel2 = New Telerik.WinControls.UI.RadLabel()
            Me.radLabel3 = New Telerik.WinControls.UI.RadLabel()
            Me.radLabel4 = New Telerik.WinControls.UI.RadLabel()
            Me.radLabel5 = New Telerik.WinControls.UI.RadLabel()
            Me.radCheckBox4 = New Telerik.WinControls.UI.RadCheckBox()
            Me.radCheckBoxArbWidth = New Telerik.WinControls.UI.RadCheckBox()
            Me.radCheckBoxArbHeight = New Telerik.WinControls.UI.RadCheckBox()
            Me.radGroupBox2 = New Telerik.WinControls.UI.RadGroupBox()
            Me.radLabel8 = New Telerik.WinControls.UI.RadLabel()
            Me.radSpinEditorGroupHeight = New Telerik.WinControls.UI.RadSpinEditor()
            Me.radSpinEditorItemHeight = New Telerik.WinControls.UI.RadSpinEditor()
            Me.radSpinEditorItemWidth = New Telerik.WinControls.UI.RadSpinEditor()
            Me.radSpinEditorGroupIndent = New Telerik.WinControls.UI.RadSpinEditor()
            Me.radSpinEditorItemSpacing = New Telerik.WinControls.UI.RadSpinEditor()
            Me.radGroupBoxDetailView = New Telerik.WinControls.UI.RadGroupBox()
            Me.radButtonBestFitColumns = New Telerik.WinControls.UI.RadButton()
            Me.radSpinEditorHeaderHeight = New Telerik.WinControls.UI.RadSpinEditor()
            Me.radCheckBoxColHeaders = New Telerik.WinControls.UI.RadCheckBox()
            Me.radCheckBoxColSort = New Telerik.WinControls.UI.RadCheckBox()
            Me.radCheckBoxColReorder = New Telerik.WinControls.UI.RadCheckBox()
            Me.radCheckBoxColResize = New Telerik.WinControls.UI.RadCheckBox()
            Me.radLabel6 = New Telerik.WinControls.UI.RadLabel()
            Me.radGroupBoxIconView = New Telerik.WinControls.UI.RadGroupBox()
            Me.radDropDownList2 = New Telerik.WinControls.UI.RadDropDownList()
            Me.radLabel7 = New Telerik.WinControls.UI.RadLabel()
            Me.radGroupBox5 = New Telerik.WinControls.UI.RadGroupBox()
            Me.radDropDownListViewType = New Telerik.WinControls.UI.RadDropDownList()
            Me.radLabel1 = New Telerik.WinControls.UI.RadLabel()
            Me.radPanel1 = New Telerik.Examples.WinControls.TreeView.TreeExampleHeaderPanel()
            Me.radPanel2 = New Telerik.WinControls.UI.RadPanel()
            CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.settingsPanel.SuspendLayout()
            CType(Me.themePanel, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radListView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radCheckBoxShowCheckboxes, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radCheckBoxGroups, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radGroupBox1.SuspendLayout()
            CType(Me.radCheckBoxKeyboardNavigation, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radCheckBoxFullRowSelect, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel5, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radLabel5.SuspendLayout()
            CType(Me.radCheckBox4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radCheckBoxArbWidth, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radCheckBoxArbHeight, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radGroupBox2.SuspendLayout()
            CType(Me.radLabel8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radSpinEditorGroupHeight, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radSpinEditorItemHeight, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radSpinEditorItemWidth, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radSpinEditorGroupIndent, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radSpinEditorItemSpacing, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radGroupBoxDetailView, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radGroupBoxDetailView.SuspendLayout()
            CType(Me.radButtonBestFitColumns, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radSpinEditorHeaderHeight, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radCheckBoxColHeaders, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radCheckBoxColSort, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radCheckBoxColReorder, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radCheckBoxColResize, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radGroupBoxIconView, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radGroupBoxIconView.SuspendLayout()
            CType(Me.radDropDownList2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radGroupBox5, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radGroupBox5.SuspendLayout()
            CType(Me.radDropDownListViewType, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radPanel2.SuspendLayout()
            Me.SuspendLayout()
            '
            'settingsPanel
            '
            Me.settingsPanel.Controls.Add(Me.radGroupBox5)
            Me.settingsPanel.Controls.Add(Me.radGroupBox2)
            Me.settingsPanel.Controls.Add(Me.radGroupBox1)
            Me.settingsPanel.Controls.Add(Me.radGroupBoxIconView)
            Me.settingsPanel.Controls.Add(Me.radGroupBoxDetailView)
            Me.settingsPanel.Location = New System.Drawing.Point(1032, 1)
            Me.settingsPanel.Size = New System.Drawing.Size(200, 830)
            Me.settingsPanel.Controls.SetChildIndex(Me.radGroupBoxDetailView, 0)
            Me.settingsPanel.Controls.SetChildIndex(Me.radGroupBoxIconView, 0)
            Me.settingsPanel.Controls.SetChildIndex(Me.radGroupBox1, 0)
            Me.settingsPanel.Controls.SetChildIndex(Me.radGroupBox2, 0)
            Me.settingsPanel.Controls.SetChildIndex(Me.radGroupBox5, 0)
            '
            'radListView1
            '
            ListViewDetailColumn1.HeaderText = "File Name"
            ListViewDetailColumn2.HeaderText = "Total Size"
            ListViewDetailColumn3.HeaderText = "Type"
            ListViewDetailColumn4.HeaderText = "Free Space"
            Me.radListView1.Columns.AddRange(New Telerik.WinControls.UI.ListViewDetailColumn() {ListViewDetailColumn1, ListViewDetailColumn2, ListViewDetailColumn3, ListViewDetailColumn4})
            Me.radListView1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.radListView1.EnableCustomGrouping = True
            Me.radListView1.EnableSorting = True
            ListViewDataItemGroup1.Text = "Files Stored on This Computer"
            ListViewDataItemGroup2.Text = "Hard Disk Drives"
            ListViewDataItemGroup3.Text = "Devices with Removable Storage"
            Me.radListView1.Groups.AddRange(New Telerik.WinControls.UI.ListViewDataItemGroup() {ListViewDataItemGroup1, ListViewDataItemGroup2, ListViewDataItemGroup3})
            ListViewDataItem1.Group = ListViewDataItemGroup1
            ListViewDataItem1.Image = Global.My.Resources.folder_xp
            ListViewDataItem1.Text = "Shared Documents"
            ListViewDataItem2.Group = ListViewDataItemGroup1
            ListViewDataItem2.Image = Global.My.Resources.folder_xp
            ListViewDataItem2.Text = "Administrator's Documents"
            ListViewDataItem3.Group = ListViewDataItemGroup3
            ListViewDataItem3.Image = Global.My.Resources.floppy_drive
            ListViewDataItem3.Text = "3.5 Floppy (A:)"
            ListViewDataItem4.Group = ListViewDataItemGroup2
            ListViewDataItem4.Image = Global.My.Resources.hard_drive
            ListViewDataItem4.Text = "Local Disk (C:)"
            ListViewDataItem5.Group = ListViewDataItemGroup2
            ListViewDataItem5.Image = Global.My.Resources.hard_drive
            ListViewDataItem5.Text = "Local Disk (D:)"
            ListViewDataItem6.Group = ListViewDataItemGroup2
            ListViewDataItem6.Image = Global.My.Resources.hard_drive
            ListViewDataItem6.Text = "Local Disk (E:)"
            ListViewDataItem7.Group = ListViewDataItemGroup2
            ListViewDataItem7.Image = Global.My.Resources.hard_drive
            ListViewDataItem7.Text = "Local Disk (F:)"
            ListViewDataItem8.Group = ListViewDataItemGroup3
            ListViewDataItem8.Image = Global.My.Resources.cd_drive
            ListViewDataItem8.Text = "DVD/CD-RW Drive (G:)"
            ListViewDataItem9.Group = ListViewDataItemGroup3
            ListViewDataItem9.Image = Global.My.Resources.cd_drive
            ListViewDataItem9.Text = "CD-RW Drive (H:)"
            Me.radListView1.Items.AddRange(New Telerik.WinControls.UI.ListViewDataItem() {ListViewDataItem1, ListViewDataItem2, ListViewDataItem3, ListViewDataItem4, ListViewDataItem5, ListViewDataItem6, ListViewDataItem7, ListViewDataItem8, ListViewDataItem9})
            Me.radListView1.ItemSize = New System.Drawing.Size(200, 32)
            Me.radListView1.Location = New System.Drawing.Point(0, 30)
            Me.radListView1.Name = "radListView1"
            Me.radListView1.Size = New System.Drawing.Size(561, 342)
            Me.radListView1.TabIndex = 1
            Me.radListView1.Text = "radListView1"
            '
            'radCheckBoxShowCheckboxes
            '
            Me.radCheckBoxShowCheckboxes.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radCheckBoxShowCheckboxes.Location = New System.Drawing.Point(5, 19)
            Me.radCheckBoxShowCheckboxes.Name = "radCheckBoxShowCheckboxes"
            Me.radCheckBoxShowCheckboxes.Size = New System.Drawing.Size(113, 18)
            Me.radCheckBoxShowCheckboxes.TabIndex = 2
            Me.radCheckBoxShowCheckboxes.Text = "Show Check Boxes"
            '
            'radCheckBoxGroups
            '
            Me.radCheckBoxGroups.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radCheckBoxGroups.Location = New System.Drawing.Point(5, 67)
            Me.radCheckBoxGroups.Name = "radCheckBoxGroups"
            Me.radCheckBoxGroups.Size = New System.Drawing.Size(87, 18)
            Me.radCheckBoxGroups.TabIndex = 4
            Me.radCheckBoxGroups.Text = "Show Groups"
            '
            'radGroupBox1
            '
            Me.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
            Me.radGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radGroupBox1.Controls.Add(Me.radCheckBoxKeyboardNavigation)
            Me.radGroupBox1.Controls.Add(Me.radCheckBoxShowCheckboxes)
            Me.radGroupBox1.Controls.Add(Me.radCheckBoxGroups)
            Me.radGroupBox1.Controls.Add(Me.radCheckBoxFullRowSelect)
            Me.radGroupBox1.HeaderText = "General Settings"
            Me.radGroupBox1.Location = New System.Drawing.Point(10, 91)
            Me.radGroupBox1.Name = "radGroupBox1"
            Me.radGroupBox1.Size = New System.Drawing.Size(180, 114)
            Me.radGroupBox1.TabIndex = 7
            Me.radGroupBox1.Text = "General Settings"
            '
            'radCheckBoxKeyboardNavigation
            '
            Me.radCheckBoxKeyboardNavigation.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radCheckBoxKeyboardNavigation.CheckState = System.Windows.Forms.CheckState.Checked
            Me.radCheckBoxKeyboardNavigation.Location = New System.Drawing.Point(5, 91)
            Me.radCheckBoxKeyboardNavigation.Name = "radCheckBoxKeyboardNavigation"
            Me.radCheckBoxKeyboardNavigation.Size = New System.Drawing.Size(162, 18)
            Me.radCheckBoxKeyboardNavigation.TabIndex = 5
            Me.radCheckBoxKeyboardNavigation.Text = "Enable Keyboard Navigation"
            Me.radCheckBoxKeyboardNavigation.ToggleState = Telerik.WinControls.Enumerations.ToggleState.[On]
            '
            'radCheckBoxFullRowSelect
            '
            Me.radCheckBoxFullRowSelect.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radCheckBoxFullRowSelect.Location = New System.Drawing.Point(5, 43)
            Me.radCheckBoxFullRowSelect.Name = "radCheckBoxFullRowSelect"
            Me.radCheckBoxFullRowSelect.Size = New System.Drawing.Size(125, 18)
            Me.radCheckBoxFullRowSelect.TabIndex = 14
            Me.radCheckBoxFullRowSelect.Text = "Enable FullRowSelect"
            '
            'radLabel2
            '
            Me.radLabel2.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLabel2.Location = New System.Drawing.Point(5, 21)
            Me.radLabel2.Name = "radLabel2"
            Me.radLabel2.Size = New System.Drawing.Size(61, 18)
            Me.radLabel2.TabIndex = 8
            Me.radLabel2.Text = "Item Width"
            '
            'radLabel3
            '
            Me.radLabel3.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLabel3.Location = New System.Drawing.Point(5, 71)
            Me.radLabel3.Name = "radLabel3"
            Me.radLabel3.Size = New System.Drawing.Size(70, 18)
            Me.radLabel3.TabIndex = 9
            Me.radLabel3.Text = "Item Spacing"
            '
            'radLabel4
            '
            Me.radLabel4.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLabel4.Location = New System.Drawing.Point(5, 99)
            Me.radLabel4.Name = "radLabel4"
            Me.radLabel4.Size = New System.Drawing.Size(73, 18)
            Me.radLabel4.TabIndex = 10
            Me.radLabel4.Text = "Group Height"
            '
            'radLabel5
            '
            Me.radLabel5.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLabel5.Controls.Add(Me.radCheckBox4)
            Me.radLabel5.Location = New System.Drawing.Point(5, 126)
            Me.radLabel5.Name = "radLabel5"
            Me.radLabel5.Size = New System.Drawing.Size(72, 18)
            Me.radLabel5.TabIndex = 11
            Me.radLabel5.Text = "Group Indent"
            '
            'radCheckBox4
            '
            Me.radCheckBox4.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radCheckBox4.Location = New System.Drawing.Point(0, 22)
            Me.radCheckBox4.Name = "radCheckBox4"
            Me.radCheckBox4.Size = New System.Drawing.Size(154, 18)
            Me.radCheckBox4.TabIndex = 12
            Me.radCheckBox4.Text = "Allow Arbitrary Item Width"
            '
            'radCheckBoxArbWidth
            '
            Me.radCheckBoxArbWidth.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radCheckBoxArbWidth.Location = New System.Drawing.Point(5, 149)
            Me.radCheckBoxArbWidth.Name = "radCheckBoxArbWidth"
            Me.radCheckBoxArbWidth.Size = New System.Drawing.Size(154, 18)
            Me.radCheckBoxArbWidth.TabIndex = 12
            Me.radCheckBoxArbWidth.Text = "Allow Arbitrary Item Width"
            '
            'radCheckBoxArbHeight
            '
            Me.radCheckBoxArbHeight.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radCheckBoxArbHeight.Location = New System.Drawing.Point(5, 172)
            Me.radCheckBoxArbHeight.Name = "radCheckBoxArbHeight"
            Me.radCheckBoxArbHeight.Size = New System.Drawing.Size(157, 18)
            Me.radCheckBoxArbHeight.TabIndex = 13
            Me.radCheckBoxArbHeight.Text = "Allow Arbitrary Item Height"
            '
            'radGroupBox2
            '
            Me.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
            Me.radGroupBox2.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radGroupBox2.Controls.Add(Me.radLabel8)
            Me.radGroupBox2.Controls.Add(Me.radSpinEditorGroupHeight)
            Me.radGroupBox2.Controls.Add(Me.radSpinEditorItemHeight)
            Me.radGroupBox2.Controls.Add(Me.radSpinEditorItemWidth)
            Me.radGroupBox2.Controls.Add(Me.radSpinEditorGroupIndent)
            Me.radGroupBox2.Controls.Add(Me.radSpinEditorItemSpacing)
            Me.radGroupBox2.Controls.Add(Me.radLabel2)
            Me.radGroupBox2.Controls.Add(Me.radLabel3)
            Me.radGroupBox2.Controls.Add(Me.radCheckBoxArbHeight)
            Me.radGroupBox2.Controls.Add(Me.radLabel4)
            Me.radGroupBox2.Controls.Add(Me.radCheckBoxArbWidth)
            Me.radGroupBox2.Controls.Add(Me.radLabel5)
            Me.radGroupBox2.HeaderText = "Sizing Settings"
            Me.radGroupBox2.Location = New System.Drawing.Point(10, 211)
            Me.radGroupBox2.Name = "radGroupBox2"
            Me.radGroupBox2.Size = New System.Drawing.Size(180, 194)
            Me.radGroupBox2.TabIndex = 15
            Me.radGroupBox2.Text = "Sizing Settings"
            '
            'radLabel8
            '
            Me.radLabel8.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLabel8.Location = New System.Drawing.Point(5, 47)
            Me.radLabel8.Name = "radLabel8"
            Me.radLabel8.Size = New System.Drawing.Size(64, 18)
            Me.radLabel8.TabIndex = 24
            Me.radLabel8.Text = "Item Height"
            '
            'radSpinEditorGroupHeight
            '
            Me.radSpinEditorGroupHeight.Location = New System.Drawing.Point(90, 97)
            Me.radSpinEditorGroupHeight.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
            Me.radSpinEditorGroupHeight.Name = "radSpinEditorGroupHeight"
            '
            '
            '
            Me.radSpinEditorGroupHeight.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.radSpinEditorGroupHeight.Size = New System.Drawing.Size(85, 20)
            Me.radSpinEditorGroupHeight.TabIndex = 23
            Me.radSpinEditorGroupHeight.TabStop = False
            Me.radSpinEditorGroupHeight.Tag = "Right"
            '
            'radSpinEditorItemHeight
            '
            Me.radSpinEditorItemHeight.Location = New System.Drawing.Point(90, 45)
            Me.radSpinEditorItemHeight.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
            Me.radSpinEditorItemHeight.Name = "radSpinEditorItemHeight"
            '
            '
            '
            Me.radSpinEditorItemHeight.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.radSpinEditorItemHeight.Size = New System.Drawing.Size(85, 20)
            Me.radSpinEditorItemHeight.TabIndex = 21
            Me.radSpinEditorItemHeight.TabStop = False
            Me.radSpinEditorItemHeight.Tag = "Right"
            '
            'radSpinEditorItemWidth
            '
            Me.radSpinEditorItemWidth.Location = New System.Drawing.Point(90, 19)
            Me.radSpinEditorItemWidth.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
            Me.radSpinEditorItemWidth.Name = "radSpinEditorItemWidth"
            '
            '
            '
            Me.radSpinEditorItemWidth.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.radSpinEditorItemWidth.Size = New System.Drawing.Size(85, 20)
            Me.radSpinEditorItemWidth.TabIndex = 20
            Me.radSpinEditorItemWidth.TabStop = False
            Me.radSpinEditorItemWidth.Tag = "Right"
            '
            'radSpinEditorGroupIndent
            '
            Me.radSpinEditorGroupIndent.Location = New System.Drawing.Point(90, 124)
            Me.radSpinEditorGroupIndent.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
            Me.radSpinEditorGroupIndent.Minimum = New Decimal(New Integer() {1000, 0, 0, -2147483648})
            Me.radSpinEditorGroupIndent.Name = "radSpinEditorGroupIndent"
            '
            '
            '
            Me.radSpinEditorGroupIndent.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.radSpinEditorGroupIndent.Size = New System.Drawing.Size(85, 20)
            Me.radSpinEditorGroupIndent.TabIndex = 19
            Me.radSpinEditorGroupIndent.TabStop = False
            Me.radSpinEditorGroupIndent.Tag = "Right"
            '
            'radSpinEditorItemSpacing
            '
            Me.radSpinEditorItemSpacing.Location = New System.Drawing.Point(90, 71)
            Me.radSpinEditorItemSpacing.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
            Me.radSpinEditorItemSpacing.Minimum = New Decimal(New Integer() {1000, 0, 0, -2147483648})
            Me.radSpinEditorItemSpacing.Name = "radSpinEditorItemSpacing"
            '
            '
            '
            Me.radSpinEditorItemSpacing.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.radSpinEditorItemSpacing.Size = New System.Drawing.Size(85, 20)
            Me.radSpinEditorItemSpacing.TabIndex = 18
            Me.radSpinEditorItemSpacing.TabStop = False
            Me.radSpinEditorItemSpacing.Tag = "Right"
            '
            'radGroupBoxDetailView
            '
            Me.radGroupBoxDetailView.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
            Me.radGroupBoxDetailView.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radGroupBoxDetailView.Controls.Add(Me.radButtonBestFitColumns)
            Me.radGroupBoxDetailView.Controls.Add(Me.radSpinEditorHeaderHeight)
            Me.radGroupBoxDetailView.Controls.Add(Me.radCheckBoxColHeaders)
            Me.radGroupBoxDetailView.Controls.Add(Me.radCheckBoxColSort)
            Me.radGroupBoxDetailView.Controls.Add(Me.radCheckBoxColReorder)
            Me.radGroupBoxDetailView.Controls.Add(Me.radCheckBoxColResize)
            Me.radGroupBoxDetailView.Controls.Add(Me.radLabel6)
            Me.radGroupBoxDetailView.HeaderText = "Detail View Settings"
            Me.radGroupBoxDetailView.Location = New System.Drawing.Point(10, 411)
            Me.radGroupBoxDetailView.Name = "radGroupBoxDetailView"
            Me.radGroupBoxDetailView.Size = New System.Drawing.Size(180, 175)
            Me.radGroupBoxDetailView.TabIndex = 16
            Me.radGroupBoxDetailView.Text = "Detail View Settings"
            '
            'radButtonBestFitColumns
            '
            Me.radButtonBestFitColumns.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radButtonBestFitColumns.Location = New System.Drawing.Point(5, 142)
            Me.radButtonBestFitColumns.Name = "radButtonBestFitColumns"
            Me.radButtonBestFitColumns.Size = New System.Drawing.Size(170, 24)
            Me.radButtonBestFitColumns.TabIndex = 20
            Me.radButtonBestFitColumns.Text = "Best Fit Columns"
            '
            'radSpinEditorHeaderHeight
            '
            Me.radSpinEditorHeaderHeight.Location = New System.Drawing.Point(90, 21)
            Me.radSpinEditorHeaderHeight.Maximum = New Decimal(New Integer() {300, 0, 0, 0})
            Me.radSpinEditorHeaderHeight.Name = "radSpinEditorHeaderHeight"
            '
            '
            '
            Me.radSpinEditorHeaderHeight.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.radSpinEditorHeaderHeight.Size = New System.Drawing.Size(85, 20)
            Me.radSpinEditorHeaderHeight.TabIndex = 19
            Me.radSpinEditorHeaderHeight.TabStop = False
            Me.radSpinEditorHeaderHeight.Tag = "Right"
            Me.radSpinEditorHeaderHeight.Value = New Decimal(New Integer() {35, 0, 0, 0})
            '
            'radCheckBoxColHeaders
            '
            Me.radCheckBoxColHeaders.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radCheckBoxColHeaders.CheckState = System.Windows.Forms.CheckState.Checked
            Me.radCheckBoxColHeaders.Location = New System.Drawing.Point(5, 45)
            Me.radCheckBoxColHeaders.Name = "radCheckBoxColHeaders"
            Me.radCheckBoxColHeaders.Size = New System.Drawing.Size(134, 18)
            Me.radCheckBoxColHeaders.TabIndex = 16
            Me.radCheckBoxColHeaders.Text = "Show Column Headers"
            Me.radCheckBoxColHeaders.ToggleState = Telerik.WinControls.Enumerations.ToggleState.[On]
            '
            'radCheckBoxColSort
            '
            Me.radCheckBoxColSort.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radCheckBoxColSort.Location = New System.Drawing.Point(5, 117)
            Me.radCheckBoxColSort.Name = "radCheckBoxColSort"
            Me.radCheckBoxColSort.Size = New System.Drawing.Size(119, 18)
            Me.radCheckBoxColSort.TabIndex = 15
            Me.radCheckBoxColSort.Text = "Enable Column Sort"
            '
            'radCheckBoxColReorder
            '
            Me.radCheckBoxColReorder.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radCheckBoxColReorder.CheckState = System.Windows.Forms.CheckState.Checked
            Me.radCheckBoxColReorder.Location = New System.Drawing.Point(5, 93)
            Me.radCheckBoxColReorder.Name = "radCheckBoxColReorder"
            Me.radCheckBoxColReorder.Size = New System.Drawing.Size(138, 18)
            Me.radCheckBoxColReorder.TabIndex = 14
            Me.radCheckBoxColReorder.Text = "Enable Column Reorder"
            Me.radCheckBoxColReorder.ToggleState = Telerik.WinControls.Enumerations.ToggleState.[On]
            '
            'radCheckBoxColResize
            '
            Me.radCheckBoxColResize.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radCheckBoxColResize.CheckState = System.Windows.Forms.CheckState.Checked
            Me.radCheckBoxColResize.Location = New System.Drawing.Point(5, 69)
            Me.radCheckBoxColResize.Name = "radCheckBoxColResize"
            Me.radCheckBoxColResize.Size = New System.Drawing.Size(130, 18)
            Me.radCheckBoxColResize.TabIndex = 13
            Me.radCheckBoxColResize.Text = "Enable Column Resize"
            Me.radCheckBoxColResize.ToggleState = Telerik.WinControls.Enumerations.ToggleState.[On]
            '
            'radLabel6
            '
            Me.radLabel6.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLabel6.Location = New System.Drawing.Point(5, 21)
            Me.radLabel6.Name = "radLabel6"
            Me.radLabel6.Size = New System.Drawing.Size(78, 18)
            Me.radLabel6.TabIndex = 9
            Me.radLabel6.Text = "Header Height"
            '
            'radGroupBoxIconView
            '
            Me.radGroupBoxIconView.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
            Me.radGroupBoxIconView.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radGroupBoxIconView.Controls.Add(Me.radDropDownList2)
            Me.radGroupBoxIconView.Controls.Add(Me.radLabel7)
            Me.radGroupBoxIconView.HeaderText = "Icon View Settings"
            Me.radGroupBoxIconView.Location = New System.Drawing.Point(10, 583)
            Me.radGroupBoxIconView.Name = "radGroupBoxIconView"
            Me.radGroupBoxIconView.Size = New System.Drawing.Size(180, 48)
            Me.radGroupBoxIconView.TabIndex = 17
            Me.radGroupBoxIconView.Text = "Icon View Settings"
            '
            'radDropDownList2
            '
            Me.radDropDownList2.AllowShowFocusCues = False
            Me.radDropDownList2.AutoCompleteDisplayMember = Nothing
            Me.radDropDownList2.AutoCompleteValueMember = Nothing
            Me.radDropDownList2.DescriptionTextMember = Nothing
            Me.radDropDownList2.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            RadListDataItem4.Text = "Vertical"
            RadListDataItem5.Text = "Horizontal"
            Me.radDropDownList2.Items.Add(RadListDataItem4)
            Me.radDropDownList2.Items.Add(RadListDataItem5)
            Me.radDropDownList2.Location = New System.Drawing.Point(69, 21)
            Me.radDropDownList2.Name = "radDropDownList2"
            Me.radDropDownList2.Size = New System.Drawing.Size(106, 20)
            Me.radDropDownList2.TabIndex = 11
            Me.radDropDownList2.Tag = "Right"
            Me.radDropDownList2.Text = "Vertical"
            '
            'radLabel7
            '
            Me.radLabel7.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLabel7.Location = New System.Drawing.Point(5, 22)
            Me.radLabel7.Name = "radLabel7"
            Me.radLabel7.Size = New System.Drawing.Size(62, 18)
            Me.radLabel7.TabIndex = 10
            Me.radLabel7.Text = "Orientation"
            '
            'radGroupBox5
            '
            Me.radGroupBox5.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
            Me.radGroupBox5.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radGroupBox5.Controls.Add(Me.radDropDownListViewType)
            Me.radGroupBox5.Controls.Add(Me.radLabel1)
            Me.radGroupBox5.HeaderText = "View Type"
            Me.radGroupBox5.Location = New System.Drawing.Point(10, 35)
            Me.radGroupBox5.Name = "radGroupBox5"
            Me.radGroupBox5.Size = New System.Drawing.Size(180, 47)
            Me.radGroupBox5.TabIndex = 18
            Me.radGroupBox5.Text = "View Type"
            '
            'radDropDownListViewType
            '
            Me.radDropDownListViewType.AllowShowFocusCues = False
            Me.radDropDownListViewType.AutoCompleteDisplayMember = Nothing
            Me.radDropDownListViewType.AutoCompleteValueMember = Nothing
            Me.radDropDownListViewType.DescriptionTextMember = Nothing
            Me.radDropDownListViewType.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            RadListDataItem1.Text = "List View"
            RadListDataItem2.Text = "Icon View"
            RadListDataItem3.Text = "Detail View"
            Me.radDropDownListViewType.Items.Add(RadListDataItem1)
            Me.radDropDownListViewType.Items.Add(RadListDataItem2)
            Me.radDropDownListViewType.Items.Add(RadListDataItem3)
            Me.radDropDownListViewType.Location = New System.Drawing.Point(69, 19)
            Me.radDropDownListViewType.Name = "radDropDownListViewType"
            Me.radDropDownListViewType.Size = New System.Drawing.Size(106, 20)
            Me.radDropDownListViewType.TabIndex = 3
            Me.radDropDownListViewType.Tag = "Right"
            Me.radDropDownListViewType.Text = "List View"
            '
            'radLabel1
            '
            Me.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLabel1.Location = New System.Drawing.Point(5, 21)
            Me.radLabel1.Name = "radLabel1"
            Me.radLabel1.Size = New System.Drawing.Size(32, 18)
            Me.radLabel1.TabIndex = 2
            Me.radLabel1.Text = "View:"
            '
            'radPanel1
            '
            Me.radPanel1.Dock = System.Windows.Forms.DockStyle.Top
            Me.radPanel1.Location = New System.Drawing.Point(0, 0)
            Me.radPanel1.Name = "radPanel1"
            Me.radPanel1.Size = New System.Drawing.Size(561, 30)
            Me.radPanel1.TabIndex = 2
            Me.radPanel1.Text = "My Computer"
            Me.radPanel1.PanelElement.PanelText.ForeColor = Color.Gray
            '
            'radPanel2
            '
            Me.radPanel2.Controls.Add(Me.radListView1)
            Me.radPanel2.Controls.Add(Me.radPanel1)
            Me.radPanel2.Location = New System.Drawing.Point(0, 0)
            Me.radPanel2.Name = "radPanel2"
            Me.radPanel2.Size = New System.Drawing.Size(561, 372)
            Me.radPanel2.TabIndex = 3
            Me.radPanel2.Text = "radPanel2"
            '
            'Form1
            '
            Me.Controls.Add(Me.radPanel2)
            Me.Name = "Form1"
            Me.Size = New System.Drawing.Size(1381, 1000)
            Me.Controls.SetChildIndex(Me.themePanel, 0)
            Me.Controls.SetChildIndex(Me.settingsPanel, 0)
            Me.Controls.SetChildIndex(Me.radPanel2, 0)
            CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).EndInit()
            Me.settingsPanel.ResumeLayout(False)
            Me.settingsPanel.PerformLayout()
            CType(Me.themePanel, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radListView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radCheckBoxShowCheckboxes, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radCheckBoxGroups, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radGroupBox1.ResumeLayout(False)
            Me.radGroupBox1.PerformLayout()
            CType(Me.radCheckBoxKeyboardNavigation, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radCheckBoxFullRowSelect, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel5, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radLabel5.ResumeLayout(False)
            Me.radLabel5.PerformLayout()
            CType(Me.radCheckBox4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radCheckBoxArbWidth, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radCheckBoxArbHeight, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radGroupBox2.ResumeLayout(False)
            Me.radGroupBox2.PerformLayout()
            CType(Me.radLabel8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radSpinEditorGroupHeight, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radSpinEditorItemHeight, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radSpinEditorItemWidth, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radSpinEditorGroupIndent, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radSpinEditorItemSpacing, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radGroupBoxDetailView, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radGroupBoxDetailView.ResumeLayout(False)
            Me.radGroupBoxDetailView.PerformLayout()
            CType(Me.radButtonBestFitColumns, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radSpinEditorHeaderHeight, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radCheckBoxColHeaders, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radCheckBoxColSort, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radCheckBoxColReorder, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radCheckBoxColResize, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radGroupBoxIconView, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radGroupBoxIconView.ResumeLayout(False)
            Me.radGroupBoxIconView.PerformLayout()
            CType(Me.radDropDownList2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radGroupBox5, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radGroupBox5.ResumeLayout(False)
            Me.radGroupBox5.PerformLayout()
            CType(Me.radDropDownListViewType, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radPanel1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radPanel2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radPanel2.ResumeLayout(False)
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private radListView1 As Telerik.WinControls.UI.RadListView
        Private radCheckBoxShowCheckboxes As Telerik.WinControls.UI.RadCheckBox
        Private radCheckBoxGroups As Telerik.WinControls.UI.RadCheckBox
        Private radGroupBox1 As Telerik.WinControls.UI.RadGroupBox
        Private radLabel2 As Telerik.WinControls.UI.RadLabel
        Private radLabel3 As Telerik.WinControls.UI.RadLabel
        Private radLabel4 As Telerik.WinControls.UI.RadLabel
        Private radLabel5 As Telerik.WinControls.UI.RadLabel
        Private radCheckBox4 As Telerik.WinControls.UI.RadCheckBox
        Private radCheckBoxArbWidth As Telerik.WinControls.UI.RadCheckBox
        Private radCheckBoxArbHeight As Telerik.WinControls.UI.RadCheckBox
        Private radCheckBoxFullRowSelect As Telerik.WinControls.UI.RadCheckBox
        Private radGroupBox2 As Telerik.WinControls.UI.RadGroupBox
        Private radGroupBoxDetailView As Telerik.WinControls.UI.RadGroupBox
        Private radLabel6 As Telerik.WinControls.UI.RadLabel
        Private radCheckBoxColResize As Telerik.WinControls.UI.RadCheckBox
        Private radCheckBoxColReorder As Telerik.WinControls.UI.RadCheckBox
        Private radCheckBoxColSort As Telerik.WinControls.UI.RadCheckBox
        Private radCheckBoxColHeaders As Telerik.WinControls.UI.RadCheckBox
        Private radGroupBoxIconView As Telerik.WinControls.UI.RadGroupBox
        Private radLabel7 As Telerik.WinControls.UI.RadLabel
        Private radGroupBox5 As Telerik.WinControls.UI.RadGroupBox
        Private radLabel1 As Telerik.WinControls.UI.RadLabel
        Private radDropDownListViewType As Telerik.WinControls.UI.RadDropDownList
        Private radSpinEditorItemSpacing As Telerik.WinControls.UI.RadSpinEditor
        Private radSpinEditorGroupIndent As Telerik.WinControls.UI.RadSpinEditor
        Private radDropDownList2 As Telerik.WinControls.UI.RadDropDownList
        Private radSpinEditorHeaderHeight As Telerik.WinControls.UI.RadSpinEditor
        Private radSpinEditorItemWidth As Telerik.WinControls.UI.RadSpinEditor
        Private radSpinEditorItemHeight As Telerik.WinControls.UI.RadSpinEditor
        Private radSpinEditorGroupHeight As Telerik.WinControls.UI.RadSpinEditor
        Private radLabel8 As Telerik.WinControls.UI.RadLabel
        Private radPanel1 As Telerik.Examples.WinControls.TreeView.TreeExampleHeaderPanel
        Private radPanel2 As Telerik.WinControls.UI.RadPanel
        Private radButtonBestFitColumns As Telerik.WinControls.UI.RadButton
        Private WithEvents radCheckBoxKeyboardNavigation As Telerik.WinControls.UI.RadCheckBox
    End Class
End Namespace