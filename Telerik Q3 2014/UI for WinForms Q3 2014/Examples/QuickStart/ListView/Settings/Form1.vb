﻿Imports System.ComponentModel
Imports System.Text
Imports Telerik.QuickStart.WinControls
Imports Telerik.WinControls.UI

Namespace Telerik.Examples.WinControls.ListView.Settings
    Partial Public Class Form1
        Inherits ListViewExamplesControl
        Public Sub New()
            InitializeComponent()

            Me.radGroupBoxDetailView.Visible = False
            Me.radGroupBoxIconView.Visible = False
            Me.radGroupBoxIconView.Location = Me.radGroupBoxDetailView.Location

            SyncSettings()
        End Sub

        Protected Overrides Sub WireEvents()
            AddHandler radCheckBoxShowCheckboxes.ToggleStateChanged, AddressOf radCheckBoxShowCheckboxes_ToggleStateChanged
            AddHandler radCheckBoxGroups.ToggleStateChanged, AddressOf radCheckBoxGroups_ToggleStateChanged
            AddHandler radCheckBoxFullRowSelect.ToggleStateChanged, AddressOf radCheckBoxFullRowSelect_ToggleStateChanged
            AddHandler radCheckBoxArbWidth.ToggleStateChanged, AddressOf radCheckBoxArbWidth_ToggleStateChanged
            AddHandler radCheckBoxArbHeight.ToggleStateChanged, AddressOf radCheckBoxArbHeight_ToggleStateChanged
            AddHandler radSpinEditorGroupHeight.ValueChanged, AddressOf radSpinEditorGroupHeight_ValueChanged
            AddHandler radSpinEditorItemHeight.ValueChanged, AddressOf radSpinEditorItemHeight_ValueChanged
            AddHandler radSpinEditorItemWidth.ValueChanged, AddressOf radSpinEditorItemWidth_ValueChanged
            AddHandler radSpinEditorGroupIndent.ValueChanged, AddressOf radSpinEditorGroupIndent_ValueChanged
            AddHandler radSpinEditorItemSpacing.ValueChanged, AddressOf radSpinEditorItemSpacing_ValueChanged
            AddHandler radSpinEditorHeaderHeight.ValueChanged, AddressOf radSpinEditorHeaderHeight_ValueChanged
            AddHandler radCheckBoxColHeaders.ToggleStateChanged, AddressOf radCheckBoxColHeaders_ToggleStateChanged
            AddHandler radCheckBoxColSort.ToggleStateChanged, AddressOf radCheckBoxColSort_ToggleStateChanged
            AddHandler radCheckBoxColReorder.ToggleStateChanged, AddressOf radCheckBoxColReorder_ToggleStateChanged
            AddHandler radCheckBoxColResize.ToggleStateChanged, AddressOf radCheckBoxColResize_ToggleStateChanged
            AddHandler radDropDownList2.SelectedIndexChanged, AddressOf radDropDownList2_SelectedIndexChanged
            AddHandler radDropDownListViewType.SelectedIndexChanged, AddressOf radDropDownListViewType_SelectedIndexChanged
            AddHandler radListView1.ViewTypeChanged, AddressOf radListView1_ViewTypeChanged
            AddHandler radButtonBestFitColumns.Click, AddressOf radButtonBestFitColumns_Click
        End Sub

        Private Sub radDropDownListViewType_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.WinControls.UI.Data.PositionChangedEventArgs)
            Select Case Me.radDropDownListViewType.Text
                Case "List View"
                    Me.radListView1.ViewType = Telerik.WinControls.UI.ListViewType.ListView
                Case "Icon View"
                    Me.radListView1.ViewType = Telerik.WinControls.UI.ListViewType.IconsView
                Case "Detail View"
                    Me.radListView1.ViewType = Telerik.WinControls.UI.ListViewType.DetailsView
            End Select
        End Sub

        Private Sub radCheckBoxShowCheckboxes_ToggleStateChanged(ByVal sender As Object, ByVal args As Telerik.WinControls.UI.StateChangedEventArgs)
            Me.radListView1.ShowCheckBoxes = Me.radCheckBoxShowCheckboxes.Checked
        End Sub

        Private Sub radCheckBoxGroups_ToggleStateChanged(ByVal sender As Object, ByVal args As Telerik.WinControls.UI.StateChangedEventArgs)
            Me.radListView1.ShowGroups = Me.radCheckBoxGroups.Checked
        End Sub

        Private Sub radCheckBoxKeyboardNavigation_ToggleStateChanged(sender As Object, args As StateChangedEventArgs) Handles radCheckBoxKeyboardNavigation.ToggleStateChanged
            Me.radListView1.KeyboardSearchEnabled = Me.radCheckBoxKeyboardNavigation.Checked
        End Sub

        Private Sub radCheckBoxArbWidth_ToggleStateChanged(ByVal sender As Object, ByVal args As Telerik.WinControls.UI.StateChangedEventArgs)
            Me.radListView1.AllowArbitraryItemWidth = Me.radCheckBoxArbWidth.Checked
        End Sub

        Private Sub radCheckBoxArbHeight_ToggleStateChanged(ByVal sender As Object, ByVal args As Telerik.WinControls.UI.StateChangedEventArgs)
            Me.radListView1.AllowArbitraryItemHeight = Me.radCheckBoxArbHeight.Checked
        End Sub

        Private Sub radCheckBoxFullRowSelect_ToggleStateChanged(ByVal sender As Object, ByVal args As Telerik.WinControls.UI.StateChangedEventArgs)
            Me.radListView1.FullRowSelect = Me.radCheckBoxFullRowSelect.Checked
        End Sub

        Private Sub radCheckBoxColHeaders_ToggleStateChanged(ByVal sender As Object, ByVal args As Telerik.WinControls.UI.StateChangedEventArgs)
            Me.radListView1.ShowColumnHeaders = Me.radCheckBoxColHeaders.Checked
        End Sub

        Private Sub radCheckBoxColResize_ToggleStateChanged(ByVal sender As Object, ByVal args As Telerik.WinControls.UI.StateChangedEventArgs)
            Me.radListView1.AllowColumnResize = Me.radCheckBoxColResize.Checked
        End Sub

        Private Sub radCheckBoxColReorder_ToggleStateChanged(ByVal sender As Object, ByVal args As Telerik.WinControls.UI.StateChangedEventArgs)
            Me.radListView1.AllowColumnReorder = Me.radCheckBoxColReorder.Checked
        End Sub

        Private Sub radCheckBoxColSort_ToggleStateChanged(ByVal sender As Object, ByVal args As Telerik.WinControls.UI.StateChangedEventArgs)
            Me.radListView1.EnableColumnSort = Me.radCheckBoxColSort.Checked
        End Sub

        Private Sub radSpinEditorHeaderHeight_ValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            Me.radListView1.HeaderHeight = CSng(Me.radSpinEditorHeaderHeight.Value)
        End Sub

        Private Sub radSpinEditorGroupIndent_ValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            Me.radListView1.GroupIndent = CInt(Fix(Me.radSpinEditorGroupIndent.Value))
        End Sub

        Private Sub radSpinEditorItemSpacing_ValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            Me.radListView1.ItemSpacing = CInt(Fix(Me.radSpinEditorItemSpacing.Value))
        End Sub

        Private Sub radSpinEditorItemWidth_ValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            Me.radListView1.ItemSize = New Size(CInt(Fix(Me.radSpinEditorItemWidth.Value)), Me.radListView1.ItemSize.Height)
        End Sub

        Private Sub radSpinEditorItemHeight_ValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            Me.radListView1.ItemSize = New Size(Me.radListView1.ItemSize.Width, CInt(Fix(Me.radSpinEditorItemHeight.Value)))
        End Sub

        Private Sub radSpinEditorGroupHeight_ValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            Me.radListView1.GroupItemSize = New Size(Me.radListView1.GroupItemSize.Width, CInt(Fix(Me.radSpinEditorGroupHeight.Value)))
        End Sub

        Private Sub radDropDownList2_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.WinControls.UI.Data.PositionChangedEventArgs)
            If Me.radListView1.ViewType = Telerik.WinControls.UI.ListViewType.IconsView Then
                Me.radListView1.ListViewElement.ViewElement.Orientation = If((Me.radDropDownList2.Text = "Vertical"), Orientation.Vertical, Orientation.Horizontal)
            End If
        End Sub

        Private Sub radListView1_ViewTypeChanged(ByVal sender As Object, ByVal e As EventArgs)
            Me.radGroupBoxDetailView.Visible = False
            Me.radGroupBoxIconView.Visible = False

            If Me.radListView1.ViewType = Telerik.WinControls.UI.ListViewType.DetailsView Then
                Me.radGroupBoxDetailView.Visible = True
                Me.radListView1.ItemSize = New Size(200, 32)
            End If
            If Me.radListView1.ViewType = Telerik.WinControls.UI.ListViewType.IconsView Then
                Me.radGroupBoxIconView.Visible = True
                Me.radListView1.ItemSize = New Size(155, 46)
            End If
            If Me.radListView1.ViewType = Telerik.WinControls.UI.ListViewType.ListView Then
                Me.radListView1.ItemSize = New Size(200, 32)
            End If

            SyncSettings()
        End Sub

        Private Sub SyncSettings()
            Me.radCheckBoxArbWidth.Checked = Me.radListView1.AllowArbitraryItemWidth
            Me.radCheckBoxArbHeight.Checked = Me.radListView1.AllowArbitraryItemHeight
            Me.radCheckBoxFullRowSelect.Checked = Me.radListView1.FullRowSelect
            Me.radCheckBoxKeyboardNavigation.Checked = Me.radListView1.KeyboardSearchEnabled

            Me.radSpinEditorItemSpacing.Value = Me.radListView1.ItemSpacing
            Me.radSpinEditorGroupIndent.Value = Me.radListView1.GroupIndent

            Me.radSpinEditorItemWidth.Value = Me.radListView1.ItemSize.Width
            Me.radSpinEditorItemHeight.Value = Me.radListView1.ItemSize.Height

            Me.radSpinEditorGroupHeight.Value = Me.radListView1.GroupItemSize.Height
        End Sub

        Public Overrides ReadOnly Property ContentControl() As Control
            Get
                Return Me.radPanel2
            End Get
        End Property

        Private Sub radButtonBestFitColumns_Click(sender As Object, e As EventArgs)
            Dim detailView As DetailListViewElement
            detailView = TryCast(Me.radListView1.ListViewElement.ViewElement, DetailListViewElement)

            If detailView IsNot Nothing Then
                detailView.BestFitColumns(ListViewBestFitColumnMode.AllCells)
            End If
        End Sub
      
    End Class
End Namespace
