Namespace Telerik.Examples.WinControls.Editors.MaskedEditBox
	Partial Public Class Form1
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		
        Private Sub InitializeComponent()
            Me.RadMaskedEditBox5 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.RadMaskedEditBox4 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.RadMaskedEditBox3 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.radLblUniversal = New Telerik.WinControls.UI.RadLabel()
            Me.RadMaskedEditBox2 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.radLblFullDateTime = New Telerik.WinControls.UI.RadLabel()
            Me.RadMaskedEditBox1 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.radLblFullTime = New Telerik.WinControls.UI.RadLabel()
            Me.radLblShortTime = New Telerik.WinControls.UI.RadLabel()
            Me.radLblSortable = New Telerik.WinControls.UI.RadLabel()
            Me.radLabel1 = New Telerik.WinControls.UI.RadLabel()
            Me.RadMaskedEditBox6 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.RadMaskedEditBox7 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.radLabel2 = New Telerik.WinControls.UI.RadLabel()
            Me.RadMaskedEditBox8 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.radLabel3 = New Telerik.WinControls.UI.RadLabel()
            Me.RadMaskedEditBox9 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.radLabel4 = New Telerik.WinControls.UI.RadLabel()
            Me.RadMaskedEditBox10 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.radLabel5 = New Telerik.WinControls.UI.RadLabel()
            Me.RadMaskedEditBox11 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.radLblNumber = New Telerik.WinControls.UI.RadLabel()
            Me.radLblShortDate = New Telerik.WinControls.UI.RadLabel()
            Me.radLblPhoneNumber = New Telerik.WinControls.UI.RadLabel()
            Me.RadMaskedEditBox12 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.radLabel6 = New Telerik.WinControls.UI.RadLabel()
            Me.radLabel7 = New Telerik.WinControls.UI.RadLabel()
            Me.radLabel8 = New Telerik.WinControls.UI.RadLabel()
            Me.RadMaskedEditBox13 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.RadMaskedEditBox14 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.RadMaskedEditBox15 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.RadMaskedEditBox16 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.radLblCulture = New Telerik.WinControls.UI.RadLabel()
            Me.radComboCultures = New Telerik.WinControls.UI.RadDropDownList()
            Me.radMaskedEditBox17 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.radLabel9 = New Telerik.WinControls.UI.RadLabel()
            Me.radLabel10 = New Telerik.WinControls.UI.RadLabel()
            Me.radMaskedEditBox18 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.radLabel11 = New Telerik.WinControls.UI.RadLabel()
            Me.radMaskedEditBox19 = New Telerik.WinControls.UI.RadMaskedEditBox()
            Me.radLabel12 = New Telerik.WinControls.UI.RadLabel()
            Me.radTextBox1 = New Telerik.WinControls.UI.RadTextBox()
            CType(Me.radPanelDemoHolder, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radPanelDemoHolder.SuspendLayout()
            CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.settingsPanel.SuspendLayout()
            CType(Me.themePanel, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblUniversal, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblFullDateTime, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblFullTime, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblShortTime, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblSortable, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblNumber, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblShortDate, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblPhoneNumber, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox12, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox13, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox14, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox15, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RadMaskedEditBox16, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLblCulture, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radComboCultures, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radMaskedEditBox17, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radMaskedEditBox18, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radMaskedEditBox19, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radLabel12, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.radTextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            ' 
            ' radPanelDemoHolder
            ' 
            Me.radPanelDemoHolder.Controls.Add(Me.radMaskedEditBox18)
            Me.radPanelDemoHolder.Controls.Add(Me.radMaskedEditBox19)
            Me.radPanelDemoHolder.Controls.Add(Me.radMaskedEditBox17)
            Me.radPanelDemoHolder.Controls.Add(Me.radLblUniversal)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox5)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox14)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox4)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox11)
            Me.radPanelDemoHolder.Controls.Add(Me.radLblSortable)
            Me.radPanelDemoHolder.Controls.Add(Me.radLabel10)
            Me.radPanelDemoHolder.Controls.Add(Me.radLabel11)
            Me.radPanelDemoHolder.Controls.Add(Me.radLabel9)
            Me.radPanelDemoHolder.Controls.Add(Me.radLblNumber)
            Me.radPanelDemoHolder.Controls.Add(Me.radLblShortTime)
            Me.radPanelDemoHolder.Controls.Add(Me.radLblShortDate)
            Me.radPanelDemoHolder.Controls.Add(Me.radLblFullTime)
            Me.radPanelDemoHolder.Controls.Add(Me.radLblPhoneNumber)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox1)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox12)
            Me.radPanelDemoHolder.Controls.Add(Me.radLblFullDateTime)
            Me.radPanelDemoHolder.Controls.Add(Me.radLabel6)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox2)
            Me.radPanelDemoHolder.Controls.Add(Me.radLabel7)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox3)
            Me.radPanelDemoHolder.Controls.Add(Me.radLabel8)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox13)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox15)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox16)
            Me.radPanelDemoHolder.Controls.Add(Me.radLabel5)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox10)
            Me.radPanelDemoHolder.Controls.Add(Me.radLabel4)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox9)
            Me.radPanelDemoHolder.Controls.Add(Me.radLabel3)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox8)
            Me.radPanelDemoHolder.Controls.Add(Me.radLabel2)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox7)
            Me.radPanelDemoHolder.Controls.Add(Me.RadMaskedEditBox6)
            Me.radPanelDemoHolder.Controls.Add(Me.radLabel1)
            Me.radPanelDemoHolder.ForeColor = System.Drawing.Color.Black
            Me.radPanelDemoHolder.Size = New System.Drawing.Size(661, 403)
            Me.radPanelDemoHolder.ThemeName = "ControlDefault"
            ' 
            ' settingsPanel
            ' 
            Me.settingsPanel.Controls.Add(Me.radTextBox1)
            Me.settingsPanel.Controls.Add(Me.radLblCulture)
            Me.settingsPanel.Controls.Add(Me.radLabel12)
            Me.settingsPanel.Controls.Add(Me.radComboCultures)
            Me.settingsPanel.Location = New System.Drawing.Point(945, 1)
            Me.settingsPanel.Size = New System.Drawing.Size(200, 527)
            Me.settingsPanel.Controls.SetChildIndex(Me.radComboCultures, 0)
            Me.settingsPanel.Controls.SetChildIndex(Me.radLabel12, 0)
            Me.settingsPanel.Controls.SetChildIndex(Me.radLblCulture, 0)
            Me.settingsPanel.Controls.SetChildIndex(Me.radTextBox1, 0)
            ' 
            ' RadMaskedEditBox5
            ' 
            Me.RadMaskedEditBox5.Location = New System.Drawing.Point(387, 322)
            Me.RadMaskedEditBox5.Mask = "g"
            Me.RadMaskedEditBox5.MaskType = Telerik.WinControls.UI.MaskType.DateTime
            Me.RadMaskedEditBox5.Name = "RadMaskedEditBox5"
            ' 
            ' 
            ' 
            Me.RadMaskedEditBox5.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.RadMaskedEditBox5.Size = New System.Drawing.Size(238, 20)
            Me.RadMaskedEditBox5.TabIndex = 15
            Me.RadMaskedEditBox5.TabStop = False
            Me.RadMaskedEditBox5.Text = "11/1/2007 12:00 AM"
            ' 
            ' RadMaskedEditBox4
            ' 
            Me.RadMaskedEditBox4.Location = New System.Drawing.Point(387, 203)
            Me.RadMaskedEditBox4.Mask = "T"
            Me.RadMaskedEditBox4.MaskType = Telerik.WinControls.UI.MaskType.DateTime
            Me.RadMaskedEditBox4.Name = "RadMaskedEditBox4"
            ' 
            ' 
            ' 
            Me.RadMaskedEditBox4.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.RadMaskedEditBox4.Size = New System.Drawing.Size(238, 20)
            Me.RadMaskedEditBox4.TabIndex = 14
            Me.RadMaskedEditBox4.TabStop = False
            Me.RadMaskedEditBox4.Text = "12:00:00 AM"
            ' 
            ' RadMaskedEditBox3
            ' 
            Me.RadMaskedEditBox3.Location = New System.Drawing.Point(387, 140)
            Me.RadMaskedEditBox3.Mask = "F"
            Me.RadMaskedEditBox3.MaskType = Telerik.WinControls.UI.MaskType.DateTime
            Me.RadMaskedEditBox3.Name = "RadMaskedEditBox3"
            ' 
            ' 
            ' 
            Me.RadMaskedEditBox3.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.RadMaskedEditBox3.Size = New System.Drawing.Size(238, 20)
            Me.RadMaskedEditBox3.TabIndex = 13
            Me.RadMaskedEditBox3.TabStop = False
            Me.RadMaskedEditBox3.Text = "Thursday, November 01, 2007 12:00:00 AM"
            ' 
            ' radLblUniversal
            ' 
            Me.radLblUniversal.Location = New System.Drawing.Point(387, 66)
            Me.radLblUniversal.Name = "radLblUniversal"
            Me.radLblUniversal.Size = New System.Drawing.Size(108, 18)
            Me.radLblUniversal.TabIndex = 20
            Me.radLblUniversal.Text = "Universal Date Time:"
            ' 
            ' RadMaskedEditBox2
            ' 
            Me.RadMaskedEditBox2.Location = New System.Drawing.Point(387, 263)
            Me.RadMaskedEditBox2.Mask = "t"
            Me.RadMaskedEditBox2.MaskType = Telerik.WinControls.UI.MaskType.DateTime
            Me.RadMaskedEditBox2.Name = "RadMaskedEditBox2"
            ' 
            ' 
            ' 
            Me.RadMaskedEditBox2.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.RadMaskedEditBox2.Size = New System.Drawing.Size(238, 20)
            Me.RadMaskedEditBox2.TabIndex = 12
            Me.RadMaskedEditBox2.TabStop = False
            Me.RadMaskedEditBox2.Text = "12:00 AM"
            ' 
            ' radLblFullDateTime
            ' 
            Me.radLblFullDateTime.Location = New System.Drawing.Point(387, 120)
            Me.radLblFullDateTime.Name = "radLblFullDateTime"
            Me.radLblFullDateTime.Size = New System.Drawing.Size(80, 18)
            Me.radLblFullDateTime.TabIndex = 19
            Me.radLblFullDateTime.Text = "Full Date Time:"
            ' 
            ' RadMaskedEditBox1
            ' 
            Me.RadMaskedEditBox1.Location = New System.Drawing.Point(387, 86)
            Me.RadMaskedEditBox1.Mask = "u"
            Me.RadMaskedEditBox1.MaskType = Telerik.WinControls.UI.MaskType.DateTime
            Me.RadMaskedEditBox1.Name = "RadMaskedEditBox1"
            ' 
            ' 
            ' 
            Me.RadMaskedEditBox1.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.RadMaskedEditBox1.Size = New System.Drawing.Size(238, 20)
            Me.RadMaskedEditBox1.TabIndex = 11
            Me.RadMaskedEditBox1.TabStop = False
            Me.RadMaskedEditBox1.Text = "2007-11-01 02:00:00Z"
            ' 
            ' radLblFullTime
            ' 
            Me.radLblFullTime.Location = New System.Drawing.Point(387, 183)
            Me.radLblFullTime.Name = "radLblFullTime"
            Me.radLblFullTime.Size = New System.Drawing.Size(54, 18)
            Me.radLblFullTime.TabIndex = 18
            Me.radLblFullTime.Text = "Full Time:"
            ' 
            ' radLblShortTime
            ' 
            Me.radLblShortTime.Location = New System.Drawing.Point(387, 243)
            Me.radLblShortTime.Name = "radLblShortTime"
            Me.radLblShortTime.Size = New System.Drawing.Size(63, 18)
            Me.radLblShortTime.TabIndex = 16
            Me.radLblShortTime.Text = "Short Time:"
            ' 
            ' radLblSortable
            ' 
            Me.radLblSortable.Location = New System.Drawing.Point(387, 302)
            Me.radLblSortable.Name = "radLblSortable"
            Me.radLblSortable.Size = New System.Drawing.Size(143, 18)
            Me.radLblSortable.TabIndex = 17
            Me.radLblSortable.Text = "Sortable Date Time Pattern:"
            ' 
            ' radLabel1
            ' 
            Me.radLabel1.Location = New System.Drawing.Point(198, 302)
            Me.radLabel1.Name = "radLabel1"
            Me.radLabel1.Size = New System.Drawing.Size(50, 18)
            Me.radLabel1.TabIndex = 10
            Me.radLabel1.Text = "Number:"
            ' 
            ' RadMaskedEditBox6
            ' 
            Me.RadMaskedEditBox6.Location = New System.Drawing.Point(198, 203)
            Me.RadMaskedEditBox6.Mask = "d6"
            Me.RadMaskedEditBox6.MaskType = Telerik.WinControls.UI.MaskType.Numeric
            Me.RadMaskedEditBox6.Name = "RadMaskedEditBox6"
            Me.RadMaskedEditBox6.Size = New System.Drawing.Size(141, 20)
            Me.RadMaskedEditBox6.TabIndex = 4
            Me.RadMaskedEditBox6.TabStop = False
            Me.RadMaskedEditBox6.Text = "000000"
            ' 
            ' RadMaskedEditBox7
            ' 
            Me.RadMaskedEditBox7.Location = New System.Drawing.Point(198, 322)
            Me.RadMaskedEditBox7.Mask = "n3"
            Me.RadMaskedEditBox7.MaskType = Telerik.WinControls.UI.MaskType.Numeric
            Me.RadMaskedEditBox7.Name = "RadMaskedEditBox7"
            Me.RadMaskedEditBox7.Size = New System.Drawing.Size(141, 20)
            Me.RadMaskedEditBox7.TabIndex = 3
            Me.RadMaskedEditBox7.TabStop = False
            Me.RadMaskedEditBox7.Text = "0,000"
            ' 
            ' radLabel2
            ' 
            Me.radLabel2.Location = New System.Drawing.Point(198, 243)
            Me.radLabel2.Name = "radLabel2"
            Me.radLabel2.Size = New System.Drawing.Size(66, 18)
            Me.radLabel2.TabIndex = 10
            Me.radLabel2.Text = "Fixed-point:"
            ' 
            ' RadMaskedEditBox8
            ' 
            Me.RadMaskedEditBox8.Location = New System.Drawing.Point(198, 140)
            Me.RadMaskedEditBox8.Mask = "p"
            Me.RadMaskedEditBox8.MaskType = Telerik.WinControls.UI.MaskType.Numeric
            Me.RadMaskedEditBox8.Name = "RadMaskedEditBox8"
            Me.RadMaskedEditBox8.Size = New System.Drawing.Size(141, 20)
            Me.RadMaskedEditBox8.TabIndex = 2
            Me.RadMaskedEditBox8.TabStop = False
            Me.RadMaskedEditBox8.Text = "0,00 %"
            ' 
            ' radLabel3
            ' 
            Me.radLabel3.Location = New System.Drawing.Point(198, 183)
            Me.radLabel3.Name = "radLabel3"
            Me.radLabel3.Size = New System.Drawing.Size(47, 18)
            Me.radLabel3.TabIndex = 10
            Me.radLabel3.Text = "6 Digits:"
            ' 
            ' RadMaskedEditBox9
            ' 
            Me.RadMaskedEditBox9.Location = New System.Drawing.Point(198, 263)
            Me.RadMaskedEditBox9.Mask = "f"
            Me.RadMaskedEditBox9.MaskType = Telerik.WinControls.UI.MaskType.Numeric
            Me.RadMaskedEditBox9.Name = "RadMaskedEditBox9"
            Me.RadMaskedEditBox9.Size = New System.Drawing.Size(141, 20)
            Me.RadMaskedEditBox9.TabIndex = 1
            Me.RadMaskedEditBox9.TabStop = False
            Me.RadMaskedEditBox9.Text = "0,00"
            ' 
            ' radLabel4
            ' 
            Me.radLabel4.Location = New System.Drawing.Point(197, 120)
            Me.radLabel4.Name = "radLabel4"
            Me.radLabel4.Size = New System.Drawing.Size(46, 18)
            Me.radLabel4.TabIndex = 10
            Me.radLabel4.Text = "Percent:"
            ' 
            ' RadMaskedEditBox10
            ' 
            Me.RadMaskedEditBox10.Location = New System.Drawing.Point(198, 86)
            Me.RadMaskedEditBox10.Mask = "c"
            Me.RadMaskedEditBox10.MaskType = Telerik.WinControls.UI.MaskType.Numeric
            Me.RadMaskedEditBox10.Name = "RadMaskedEditBox10"
            Me.RadMaskedEditBox10.Size = New System.Drawing.Size(141, 20)
            Me.RadMaskedEditBox10.TabIndex = 0
            Me.RadMaskedEditBox10.TabStop = False
            Me.RadMaskedEditBox10.Text = "$0,00"
            ' 
            ' radLabel5
            ' 
            Me.radLabel5.Location = New System.Drawing.Point(197, 66)
            Me.radLabel5.Name = "radLabel5"
            Me.radLabel5.Size = New System.Drawing.Size(53, 18)
            Me.radLabel5.TabIndex = 10
            Me.radLabel5.Text = "Currency:"
            ' 
            ' RadMaskedEditBox11
            ' 
            Me.RadMaskedEditBox11.Location = New System.Drawing.Point(198, 26)
            Me.RadMaskedEditBox11.Mask = "00000-9999"
            Me.RadMaskedEditBox11.MaskType = Telerik.WinControls.UI.MaskType.Standard
            Me.RadMaskedEditBox11.Name = "RadMaskedEditBox11"
            ' 
            ' 
            ' 
            Me.RadMaskedEditBox11.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.RadMaskedEditBox11.Size = New System.Drawing.Size(141, 20)
            Me.RadMaskedEditBox11.TabIndex = 29
            Me.RadMaskedEditBox11.TabStop = False
            Me.RadMaskedEditBox11.Text = "_____-____"
            ' 
            ' radLblNumber
            ' 
            Me.radLblNumber.Location = New System.Drawing.Point(5, 66)
            Me.radLblNumber.Name = "radLblNumber"
            Me.radLblNumber.Size = New System.Drawing.Size(96, 18)
            Me.radLblNumber.TabIndex = 31
            Me.radLblNumber.Text = "Number (5 digits):"
            ' 
            ' radLblShortDate
            ' 
            Me.radLblShortDate.Location = New System.Drawing.Point(5, 183)
            Me.radLblShortDate.Name = "radLblShortDate"
            Me.radLblShortDate.Size = New System.Drawing.Size(62, 18)
            Me.radLblShortDate.TabIndex = 30
            Me.radLblShortDate.Text = "Short Date:"
            ' 
            ' radLblPhoneNumber
            ' 
            Me.radLblPhoneNumber.Location = New System.Drawing.Point(5, 120)
            Me.radLblPhoneNumber.Name = "radLblPhoneNumber"
            Me.radLblPhoneNumber.Size = New System.Drawing.Size(85, 18)
            Me.radLblPhoneNumber.TabIndex = 35
            Me.radLblPhoneNumber.Text = "Phone Number:"
            ' 
            ' RadMaskedEditBox12
            ' 
            Me.RadMaskedEditBox12.Location = New System.Drawing.Point(5, 322)
            Me.RadMaskedEditBox12.Mask = "90:00"
            Me.RadMaskedEditBox12.MaskType = Telerik.WinControls.UI.MaskType.Standard
            Me.RadMaskedEditBox12.Name = "RadMaskedEditBox12"
            ' 
            ' 
            ' 
            Me.RadMaskedEditBox12.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.RadMaskedEditBox12.Size = New System.Drawing.Size(141, 20)
            Me.RadMaskedEditBox12.TabIndex = 28
            Me.RadMaskedEditBox12.TabStop = False
            Me.RadMaskedEditBox12.Text = "__:__"
            ' 
            ' radLabel6
            ' 
            Me.radLabel6.Location = New System.Drawing.Point(5, 243)
            Me.radLabel6.Name = "radLabel6"
            Me.radLabel6.Size = New System.Drawing.Size(125, 18)
            Me.radLabel6.TabIndex = 32
            Me.radLabel6.Text = "Social Security Number:"
            ' 
            ' radLabel7
            ' 
            Me.radLabel7.Location = New System.Drawing.Point(5, 302)
            Me.radLabel7.Name = "radLabel7"
            Me.radLabel7.Size = New System.Drawing.Size(33, 18)
            Me.radLabel7.TabIndex = 33
            Me.radLabel7.Text = "Time:"
            ' 
            ' radLabel8
            ' 
            Me.radLabel8.Location = New System.Drawing.Point(198, 6)
            Me.radLabel8.Name = "radLabel8"
            Me.radLabel8.Size = New System.Drawing.Size(54, 18)
            Me.radLabel8.TabIndex = 34
            Me.radLabel8.Text = "Zip Code:"
            ' 
            ' RadMaskedEditBox13
            ' 
            Me.RadMaskedEditBox13.Location = New System.Drawing.Point(5, 263)
            Me.RadMaskedEditBox13.Mask = "000-00-0000"
            Me.RadMaskedEditBox13.MaskType = Telerik.WinControls.UI.MaskType.Standard
            Me.RadMaskedEditBox13.Name = "RadMaskedEditBox13"
            ' 
            ' 
            ' 
            Me.RadMaskedEditBox13.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.RadMaskedEditBox13.Size = New System.Drawing.Size(141, 20)
            Me.RadMaskedEditBox13.TabIndex = 27
            Me.RadMaskedEditBox13.TabStop = False
            Me.RadMaskedEditBox13.Text = "___-__-____"
            ' 
            ' RadMaskedEditBox14
            ' 
            Me.RadMaskedEditBox14.Location = New System.Drawing.Point(5, 203)
            Me.RadMaskedEditBox14.Mask = "00/00/0000"
            Me.RadMaskedEditBox14.MaskType = Telerik.WinControls.UI.MaskType.Standard
            Me.RadMaskedEditBox14.Name = "RadMaskedEditBox14"
            ' 
            ' 
            ' 
            Me.RadMaskedEditBox14.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.RadMaskedEditBox14.Size = New System.Drawing.Size(141, 20)
            Me.RadMaskedEditBox14.TabIndex = 26
            Me.RadMaskedEditBox14.TabStop = False
            Me.RadMaskedEditBox14.Text = "__/__/____"
            ' 
            ' RadMaskedEditBox15
            ' 
            Me.RadMaskedEditBox15.Location = New System.Drawing.Point(5, 140)
            Me.RadMaskedEditBox15.Mask = "(999) 000-0000"
            Me.RadMaskedEditBox15.MaskType = Telerik.WinControls.UI.MaskType.Standard
            Me.RadMaskedEditBox15.Name = "RadMaskedEditBox15"
            ' 
            ' 
            ' 
            Me.RadMaskedEditBox15.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.RadMaskedEditBox15.Size = New System.Drawing.Size(141, 20)
            Me.RadMaskedEditBox15.TabIndex = 25
            Me.RadMaskedEditBox15.TabStop = False
            Me.RadMaskedEditBox15.Text = "(___) ___-____"
            ' 
            ' RadMaskedEditBox16
            ' 
            Me.RadMaskedEditBox16.Location = New System.Drawing.Point(5, 86)
            Me.RadMaskedEditBox16.Mask = "00000"
            Me.RadMaskedEditBox16.MaskType = Telerik.WinControls.UI.MaskType.Standard
            Me.RadMaskedEditBox16.Name = "RadMaskedEditBox16"
            ' 
            ' 
            ' 
            Me.RadMaskedEditBox16.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.RadMaskedEditBox16.Size = New System.Drawing.Size(141, 20)
            Me.RadMaskedEditBox16.TabIndex = 24
            Me.RadMaskedEditBox16.TabStop = False
            Me.RadMaskedEditBox16.Text = "_____"
            ' 
            ' radLblCulture
            ' 
            Me.radLblCulture.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLblCulture.BackColor = System.Drawing.Color.Transparent
            Me.radLblCulture.Location = New System.Drawing.Point(10, 56)
            Me.radLblCulture.Name = "radLblCulture"
            Me.radLblCulture.Size = New System.Drawing.Size(83, 18)
            Me.radLblCulture.TabIndex = 37
            Me.radLblCulture.Text = "Choose culture:"
            ' 
            ' radComboCultures
            ' 
            Me.radComboCultures.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radComboCultures.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
            Me.radComboCultures.BackColor = System.Drawing.Color.Transparent
            Me.radComboCultures.DropDownSizingMode = CType((Telerik.WinControls.UI.SizingMode.RightBottom Or Telerik.WinControls.UI.SizingMode.UpDown), Telerik.WinControls.UI.SizingMode)
            Me.radComboCultures.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
            Me.radComboCultures.Location = New System.Drawing.Point(10, 76)
            Me.radComboCultures.MaxDropDownItems = 6
            Me.radComboCultures.Name = "radComboCultures"
            ' 
            ' 
            ' 
            Me.radComboCultures.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
            Me.radComboCultures.Size = New System.Drawing.Size(180, 20)
            Me.radComboCultures.TabIndex = 36
            ' 
            ' radMaskedEditBox17
            ' 
            Me.radMaskedEditBox17.Location = New System.Drawing.Point(387, 26)
            Me.radMaskedEditBox17.MaskType = Telerik.WinControls.UI.MaskType.EMail
            Me.radMaskedEditBox17.Name = "radMaskedEditBox17"
            Me.radMaskedEditBox17.Size = New System.Drawing.Size(238, 20)
            Me.radMaskedEditBox17.TabIndex = 38
            Me.radMaskedEditBox17.TabStop = False
            ' 
            ' radLabel9
            ' 
            Me.radLabel9.Location = New System.Drawing.Point(387, 6)
            Me.radLabel9.Name = "radLabel9"
            Me.radLabel9.Size = New System.Drawing.Size(36, 18)
            Me.radLabel9.TabIndex = 31
            Me.radLabel9.Text = "EMail:"
            ' 
            ' radLabel10
            ' 
            Me.radLabel10.Location = New System.Drawing.Point(5, 6)
            Me.radLabel10.Name = "radLabel10"
            Me.radLabel10.Size = New System.Drawing.Size(18, 18)
            Me.radLabel10.TabIndex = 31
            Me.radLabel10.Text = "IP:"
            ' 
            ' radMaskedEditBox18
            ' 
            Me.radMaskedEditBox18.Location = New System.Drawing.Point(5, 26)
            Me.radMaskedEditBox18.Mask = "<>"
            Me.radMaskedEditBox18.MaskType = Telerik.WinControls.UI.MaskType.IP
            Me.radMaskedEditBox18.Name = "radMaskedEditBox18"
            Me.radMaskedEditBox18.Size = New System.Drawing.Size(141, 20)
            Me.radMaskedEditBox18.TabIndex = 38
            Me.radMaskedEditBox18.TabStop = False
            Me.radMaskedEditBox18.Text = "   ,   ,   ,   "
            ' 
            ' radLabel11
            ' 
            Me.radLabel11.Location = New System.Drawing.Point(5, 362)
            Me.radLabel11.Name = "radLabel11"
            Me.radLabel11.Size = New System.Drawing.Size(39, 18)
            Me.radLabel11.TabIndex = 31
            Me.radLabel11.Text = "Regex:"
            ' 
            ' radMaskedEditBox19
            ' 
            Me.radMaskedEditBox19.Location = New System.Drawing.Point(5, 382)
            Me.radMaskedEditBox19.Mask = "[A-z]"
            Me.radMaskedEditBox19.MaskType = Telerik.WinControls.UI.MaskType.Regex
            Me.radMaskedEditBox19.Name = "radMaskedEditBox19"
            Me.radMaskedEditBox19.Size = New System.Drawing.Size(238, 20)
            Me.radMaskedEditBox19.TabIndex = 38
            Me.radMaskedEditBox19.TabStop = False
            ' 
            ' radLabel12
            ' 
            Me.radLabel12.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radLabel12.BackColor = System.Drawing.Color.Transparent
            Me.radLabel12.Location = New System.Drawing.Point(10, 121)
            Me.radLabel12.Name = "radLabel12"
            Me.radLabel12.Size = New System.Drawing.Size(38, 18)
            Me.radLabel12.TabIndex = 37
            Me.radLabel12.Text = "Regex:"
            ' 
            ' radTextBox1
            ' 
            Me.radTextBox1.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radTextBox1.Location = New System.Drawing.Point(10, 141)
            Me.radTextBox1.Name = "radTextBox1"
            Me.radTextBox1.Size = New System.Drawing.Size(180, 20)
            Me.radTextBox1.TabIndex = 38
            Me.radTextBox1.TabStop = False
            Me.radTextBox1.Text = "[A-z]"
            ' 
            ' Form1
            ' 
            Me.Name = "Form1"
            Me.Size = New System.Drawing.Size(1273, 625)
            CType(Me.radPanelDemoHolder, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radPanelDemoHolder.ResumeLayout(False)
            Me.radPanelDemoHolder.PerformLayout()
            CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).EndInit()
            Me.settingsPanel.ResumeLayout(False)
            Me.settingsPanel.PerformLayout()
            CType(Me.themePanel, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblUniversal, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblFullDateTime, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblFullTime, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblShortTime, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblSortable, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblNumber, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblShortDate, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblPhoneNumber, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox12, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox13, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox14, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox15, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RadMaskedEditBox16, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLblCulture, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radComboCultures, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radMaskedEditBox17, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radMaskedEditBox18, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radMaskedEditBox19, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radLabel12, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.radTextBox1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

		#End Region

		Private RadMaskedEditBox5 As Telerik.WinControls.UI.RadMaskedEditBox
		Private RadMaskedEditBox4 As Telerik.WinControls.UI.RadMaskedEditBox
		Private RadMaskedEditBox3 As Telerik.WinControls.UI.RadMaskedEditBox
		Private radLblUniversal As Telerik.WinControls.UI.RadLabel
		Private RadMaskedEditBox2 As Telerik.WinControls.UI.RadMaskedEditBox
		Private radLblFullDateTime As Telerik.WinControls.UI.RadLabel
		Private RadMaskedEditBox1 As Telerik.WinControls.UI.RadMaskedEditBox
		Private radLblFullTime As Telerik.WinControls.UI.RadLabel
		Private radLblShortTime As Telerik.WinControls.UI.RadLabel
		Private radLabel5 As Telerik.WinControls.UI.RadLabel
		Private radLblSortable As Telerik.WinControls.UI.RadLabel
		Private RadMaskedEditBox10 As Telerik.WinControls.UI.RadMaskedEditBox
		Private radLabel4 As Telerik.WinControls.UI.RadLabel
		Private RadMaskedEditBox9 As Telerik.WinControls.UI.RadMaskedEditBox
		Private radLabel3 As Telerik.WinControls.UI.RadLabel
		Private RadMaskedEditBox8 As Telerik.WinControls.UI.RadMaskedEditBox
		Private radLabel2 As Telerik.WinControls.UI.RadLabel
		Private RadMaskedEditBox7 As Telerik.WinControls.UI.RadMaskedEditBox
		Private RadMaskedEditBox6 As Telerik.WinControls.UI.RadMaskedEditBox
		Private radLabel1 As Telerik.WinControls.UI.RadLabel
		Private RadMaskedEditBox11 As Telerik.WinControls.UI.RadMaskedEditBox
		Private radLblNumber As Telerik.WinControls.UI.RadLabel
		Private radLblShortDate As Telerik.WinControls.UI.RadLabel
		Private radLblPhoneNumber As Telerik.WinControls.UI.RadLabel
		Private RadMaskedEditBox12 As Telerik.WinControls.UI.RadMaskedEditBox
		Private radLabel6 As Telerik.WinControls.UI.RadLabel
		Private radLabel7 As Telerik.WinControls.UI.RadLabel
		Private radLabel8 As Telerik.WinControls.UI.RadLabel
		Private RadMaskedEditBox13 As Telerik.WinControls.UI.RadMaskedEditBox
		Private RadMaskedEditBox14 As Telerik.WinControls.UI.RadMaskedEditBox
		Private RadMaskedEditBox15 As Telerik.WinControls.UI.RadMaskedEditBox
		Private RadMaskedEditBox16 As Telerik.WinControls.UI.RadMaskedEditBox
		Private radLblCulture As Telerik.WinControls.UI.RadLabel
		Private radComboCultures As Telerik.WinControls.UI.RadDropDownList
		Private radMaskedEditBox18 As Telerik.WinControls.UI.RadMaskedEditBox
		Private radMaskedEditBox19 As Telerik.WinControls.UI.RadMaskedEditBox
		Private radMaskedEditBox17 As Telerik.WinControls.UI.RadMaskedEditBox
		Private radLabel10 As Telerik.WinControls.UI.RadLabel
		Private radLabel11 As Telerik.WinControls.UI.RadLabel
		Private radLabel9 As Telerik.WinControls.UI.RadLabel
		Private radLabel12 As Telerik.WinControls.UI.RadLabel
		Private radTextBox1 As Telerik.WinControls.UI.RadTextBox

	End Class
End Namespace