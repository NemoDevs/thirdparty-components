Namespace Telerik.Examples.WinControls.Editors.CalculatorDropDown
    Partial Public Class Form1
        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Windows Form Designer generated code"

        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Me.radCalculatorDropDown1 = New Telerik.WinControls.UI.RadCalculatorDropDown()
            CType(Me.radPanelDemoHolder, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radPanelDemoHolder.SuspendLayout()
            CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.settingsPanel.SuspendLayout()
            CType(Me.radCalculatorDropDown1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            ' 
            ' radPanelDemoHolder
            ' 
            Me.radPanelDemoHolder.Controls.Add(Me.radCalculatorDropDown1)
            Me.radPanelDemoHolder.ForeColor = Color.Black
            Me.radPanelDemoHolder.Size = New Size(405, 139)
            ' 
            ' settingsPanel
            ' 
            Me.settingsPanel.Location = New Point(1023, 1)
            Me.settingsPanel.Size = New Size(200, 735)
            ' 
            ' RadCalculatorDropDown
            ' 
            Me.radCalculatorDropDown1.Anchor = AnchorStyles.None
            Me.radCalculatorDropDown1.Location = New Point(0, 0)
            Me.radCalculatorDropDown1.Name = "radCalculatorDropDown1"
            Me.radCalculatorDropDown1.Size = New Size(96, 18)
            Me.radCalculatorDropDown1.TabIndex = 1
            ' 
            ' Form1
            ' 
            Me.Name = "Form1"
            Me.Size = New Size(1144, 489)
            CType(Me.radPanelDemoHolder, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radPanelDemoHolder.ResumeLayout(False)
            Me.radPanelDemoHolder.PerformLayout()
            CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).EndInit()
            Me.settingsPanel.ResumeLayout(False)
            Me.settingsPanel.PerformLayout()
            CType(Me.radCalculatorDropDown1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private radCalculatorDropDown1 As Telerik.WinControls.UI.RadCalculatorDropDown

    End Class
End Namespace