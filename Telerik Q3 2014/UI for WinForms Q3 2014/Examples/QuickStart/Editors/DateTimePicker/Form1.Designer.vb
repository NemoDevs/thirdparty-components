
Namespace Telerik.Examples.WinControls.Editors.DateTimePicker
    Partial Class Form1
        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Windows Form Designer generated code"

        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Me.radDateTimePicker1 = New Telerik.WinControls.UI.RadDateTimePicker()
            Me.radLblLongDateFormat = New Telerik.WinControls.UI.RadLabel()
            Me.radLblShortDateFormat = New Telerik.WinControls.UI.RadLabel()
            Me.radDateTimePicker2 = New Telerik.WinControls.UI.RadDateTimePicker()
            Me.radLblTimeFormat = New Telerik.WinControls.UI.RadLabel()
            Me.radDateTimePicker3 = New Telerik.WinControls.UI.RadDateTimePicker()
            Me.radBtnClearAll = New Telerik.WinControls.UI.RadButton()
            Me.radGroupExampleSettings = New Telerik.WinControls.UI.RadGroupBox()
            Me.radRadio24Hours = New Telerik.WinControls.UI.RadRadioButton()
            Me.radRadio12Hours = New Telerik.WinControls.UI.RadRadioButton()
            Me.radDateTimePicker4 = New Telerik.WinControls.UI.RadDateTimePicker()
            Me.radLabel1 = New Telerik.WinControls.UI.RadLabel()
            DirectCast(Me.radPanelDemoHolder, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radPanelDemoHolder.SuspendLayout()
            DirectCast(Me.settingsPanel, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.settingsPanel.SuspendLayout()
            DirectCast(Me.themePanel, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radDateTimePicker1, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radLblLongDateFormat, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radLblShortDateFormat, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radDateTimePicker2, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radLblTimeFormat, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radDateTimePicker3, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radBtnClearAll, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radGroupExampleSettings, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.radGroupExampleSettings.SuspendLayout()
            DirectCast(Me.radRadio24Hours, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radRadio12Hours, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radDateTimePicker4, System.ComponentModel.ISupportInitialize).BeginInit()
            DirectCast(Me.radLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            ' 
            ' radPanelDemoHolder
            ' 
            Me.radPanelDemoHolder.Controls.Add(Me.radDateTimePicker4)
            Me.radPanelDemoHolder.Controls.Add(Me.radLabel1)
            Me.radPanelDemoHolder.Controls.Add(Me.radLblLongDateFormat)
            Me.radPanelDemoHolder.Controls.Add(Me.radDateTimePicker2)
            Me.radPanelDemoHolder.Controls.Add(Me.radDateTimePicker1)
            Me.radPanelDemoHolder.Controls.Add(Me.radLblShortDateFormat)
            Me.radPanelDemoHolder.Controls.Add(Me.radDateTimePicker3)
            Me.radPanelDemoHolder.Controls.Add(Me.radLblTimeFormat)
            Me.radPanelDemoHolder.ForeColor = System.Drawing.Color.Black
            Me.radPanelDemoHolder.Size = New System.Drawing.Size(405, 139)
            ' 
            ' settingsPanel
            ' 
            Me.settingsPanel.Controls.Add(Me.radGroupExampleSettings)
            Me.settingsPanel.Location = New System.Drawing.Point(1023, 1)
            Me.settingsPanel.Size = New System.Drawing.Size(200, 735)
            Me.settingsPanel.Controls.SetChildIndex(Me.radGroupExampleSettings, 0)
            ' 
            ' radDateTimePicker1
            ' 
            Me.radDateTimePicker1.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.radDateTimePicker1.Checked = True
            Me.radDateTimePicker1.Location = New System.Drawing.Point(114, 0)
            Me.radDateTimePicker1.MinDate = New System.DateTime(1900, 1, 1, 0, 0, 0, _
             0)
            Me.radDateTimePicker1.Name = "radDateTimePicker1"
            Me.radDateTimePicker1.NullDate = New System.DateTime(1900, 1, 1, 0, 0, 0, _
             0)
            Me.radDateTimePicker1.Size = New System.Drawing.Size(191, 20)
            Me.radDateTimePicker1.TabIndex = 0
            Me.radDateTimePicker1.TabStop = False
            Me.radDateTimePicker1.Text = "Thursday, August 23, 2007"
            Me.radDateTimePicker1.Value = New System.DateTime(2007, 8, 23, 15, 29, 8, _
             309)
            ' 
            ' radLblLongDateFormat
            ' 
            Me.radLblLongDateFormat.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.radLblLongDateFormat.Location = New System.Drawing.Point(-3, 0)
            Me.radLblLongDateFormat.Name = "radLblLongDateFormat"
            Me.radLblLongDateFormat.Size = New System.Drawing.Size(96, 18)
            Me.radLblLongDateFormat.TabIndex = 1
            Me.radLblLongDateFormat.Text = "Long date format:"
            ' 
            ' radLblShortDateFormat
            ' 
            Me.radLblShortDateFormat.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.radLblShortDateFormat.Location = New System.Drawing.Point(-3, 34)
            Me.radLblShortDateFormat.Name = "radLblShortDateFormat"
            Me.radLblShortDateFormat.Size = New System.Drawing.Size(97, 18)
            Me.radLblShortDateFormat.TabIndex = 3
            Me.radLblShortDateFormat.Text = "Short date format:"
            ' 
            ' radDateTimePicker2
            ' 
            Me.radDateTimePicker2.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.radDateTimePicker2.Checked = True
            Me.radDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
            Me.radDateTimePicker2.Location = New System.Drawing.Point(114, 34)
            Me.radDateTimePicker2.MinDate = New System.DateTime(1900, 1, 1, 0, 0, 0, _
             0)
            Me.radDateTimePicker2.Name = "radDateTimePicker2"
            Me.radDateTimePicker2.NullDate = New System.DateTime(1900, 1, 1, 0, 0, 0, _
             0)
            Me.radDateTimePicker2.Size = New System.Drawing.Size(191, 20)
            Me.radDateTimePicker2.TabIndex = 2
            Me.radDateTimePicker2.TabStop = False
            Me.radDateTimePicker2.Text = "1/1/1980"
            Me.radDateTimePicker2.Value = New System.DateTime(1980, 1, 1, 0, 0, 0, _
             0)
            ' 
            ' radLblTimeFormat
            ' 
            Me.radLblTimeFormat.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.radLblTimeFormat.Location = New System.Drawing.Point(-3, 67)
            Me.radLblTimeFormat.Name = "radLblTimeFormat"
            Me.radLblTimeFormat.Size = New System.Drawing.Size(70, 18)
            Me.radLblTimeFormat.TabIndex = 5
            Me.radLblTimeFormat.Text = "Time format:"
            ' 
            ' radDateTimePicker3
            ' 
            Me.radDateTimePicker3.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.radDateTimePicker3.Checked = True
            Me.radDateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Time
            Me.radDateTimePicker3.Location = New System.Drawing.Point(114, 68)
            Me.radDateTimePicker3.MinDate = New System.DateTime(1900, 1, 1, 0, 0, 0, _
             0)
            Me.radDateTimePicker3.Name = "radDateTimePicker3"
            Me.radDateTimePicker3.NullDate = New System.DateTime(1900, 1, 1, 0, 0, 0, _
             0)
            Me.radDateTimePicker3.ShowUpDown = True
            Me.radDateTimePicker3.Size = New System.Drawing.Size(191, 21)
            Me.radDateTimePicker3.TabIndex = 4
            Me.radDateTimePicker3.TabStop = False
            Me.radDateTimePicker3.Text = "12:00:00 AM"
            Me.radDateTimePicker3.Value = New System.DateTime(1980, 1, 1, 0, 0, 0, _
             0)
            ' 
            ' radBtnClearAll
            ' 
            Me.radBtnClearAll.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radBtnClearAll.BackColor = System.Drawing.Color.FromArgb(CInt(CByte(248)), CInt(CByte(248)), CInt(CByte(248)))
            Me.radBtnClearAll.Location = New System.Drawing.Point(5, 84)
            Me.radBtnClearAll.Name = "radBtnClearAll"
            Me.radBtnClearAll.Size = New System.Drawing.Size(170, 23)
            Me.radBtnClearAll.TabIndex = 6
            Me.radBtnClearAll.Text = "Clear All"
            ' 
            ' radGroupExampleSettings
            ' 
            Me.radGroupExampleSettings.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
            Me.radGroupExampleSettings.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radGroupExampleSettings.Controls.Add(Me.radRadio24Hours)
            Me.radGroupExampleSettings.Controls.Add(Me.radRadio12Hours)
            Me.radGroupExampleSettings.Controls.Add(Me.radBtnClearAll)
            Me.radGroupExampleSettings.FooterText = ""
            Me.radGroupExampleSettings.ForeColor = System.Drawing.Color.Black
            Me.radGroupExampleSettings.HeaderMargin = New System.Windows.Forms.Padding(10, 0, 0, 0)
            Me.radGroupExampleSettings.HeaderText = " Settings "
            Me.radGroupExampleSettings.Location = New System.Drawing.Point(10, 6)
            Me.radGroupExampleSettings.Name = "radGroupExampleSettings"
            Me.radGroupExampleSettings.Padding = New System.Windows.Forms.Padding(10, 20, 10, 10)
            Me.radGroupExampleSettings.Size = New System.Drawing.Size(180, 122)
            Me.radGroupExampleSettings.TabIndex = 7
            Me.radGroupExampleSettings.Text = " Settings "
            ' 
            ' radRadio24Hours
            ' 
            Me.radRadio24Hours.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radRadio24Hours.Location = New System.Drawing.Point(5, 53)
            Me.radRadio24Hours.Name = "radRadio24Hours"
            Me.radRadio24Hours.Size = New System.Drawing.Size(65, 18)
            Me.radRadio24Hours.TabIndex = 7
            Me.radRadio24Hours.Text = "24 Hours"
            ' 
            ' radRadio12Hours
            ' 
            Me.radRadio12Hours.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.radRadio12Hours.Location = New System.Drawing.Point(5, 31)
            Me.radRadio12Hours.Name = "radRadio12Hours"
            Me.radRadio12Hours.Size = New System.Drawing.Size(65, 18)
            Me.radRadio12Hours.TabIndex = 7
            Me.radRadio12Hours.Text = "12 Hours"
            ' 
            ' radDateTimePicker4
            ' 
            Me.radDateTimePicker4.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.radDateTimePicker4.Checked = True
            Me.radDateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
            Me.radDateTimePicker4.Location = New System.Drawing.Point(115, 103)
            Me.radDateTimePicker4.MinDate = New System.DateTime(1900, 1, 1, 0, 0, 0, _
             0)
            Me.radDateTimePicker4.Name = "radDateTimePicker4"
            Me.radDateTimePicker4.NullDate = New System.DateTime(1900, 1, 1, 0, 0, 0, _
             0)
            Me.radDateTimePicker4.Size = New System.Drawing.Size(191, 20)
            Me.radDateTimePicker4.TabIndex = 6
            Me.radDateTimePicker4.TabStop = False
            Me.radDateTimePicker4.Text = "1/1/1980"
            Me.radDateTimePicker4.Value = New System.DateTime(1980, 1, 1, 0, 0, 0, _
             0)
            ' 
            ' radLabel1
            ' 
            Me.radLabel1.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.radLabel1.Location = New System.Drawing.Point(-2, 103)
            Me.radLabel1.Name = "radLabel1"
            Me.radLabel1.Size = New System.Drawing.Size(82, 18)
            Me.radLabel1.TabIndex = 7
            Me.radLabel1.Text = "Date and Time:"
            ' 
            ' Form1
            ' 
            Me.Name = "Form1"
            Me.Size = New System.Drawing.Size(1176, 630)
            DirectCast(Me.radPanelDemoHolder, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radPanelDemoHolder.ResumeLayout(False)
            Me.radPanelDemoHolder.PerformLayout()
            DirectCast(Me.settingsPanel, System.ComponentModel.ISupportInitialize).EndInit()
            Me.settingsPanel.ResumeLayout(False)
            Me.settingsPanel.PerformLayout()
            DirectCast(Me.themePanel, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radDateTimePicker1, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radLblLongDateFormat, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radLblShortDateFormat, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radDateTimePicker2, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radLblTimeFormat, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radDateTimePicker3, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radBtnClearAll, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radGroupExampleSettings, System.ComponentModel.ISupportInitialize).EndInit()
            Me.radGroupExampleSettings.ResumeLayout(False)
            Me.radGroupExampleSettings.PerformLayout()
            DirectCast(Me.radRadio24Hours, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radRadio12Hours, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radDateTimePicker4, System.ComponentModel.ISupportInitialize).EndInit()
            DirectCast(Me.radLabel1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private radDateTimePicker1 As Telerik.WinControls.UI.RadDateTimePicker
        Private radLblLongDateFormat As Telerik.WinControls.UI.RadLabel
        Private radLblShortDateFormat As Telerik.WinControls.UI.RadLabel
        Private radDateTimePicker2 As Telerik.WinControls.UI.RadDateTimePicker
        Private radLblTimeFormat As Telerik.WinControls.UI.RadLabel
        Private radDateTimePicker3 As Telerik.WinControls.UI.RadDateTimePicker
        Private radBtnClearAll As Telerik.WinControls.UI.RadButton
        Private radGroupExampleSettings As Telerik.WinControls.UI.RadGroupBox
        Private radRadio24Hours As Telerik.WinControls.UI.RadRadioButton
        Private radRadio12Hours As Telerik.WinControls.UI.RadRadioButton
        Private radDateTimePicker4 As Telerik.WinControls.UI.RadDateTimePicker
        Private radLabel1 As Telerik.WinControls.UI.RadLabel

    End Class
End Namespace