Imports Telerik.Examples.WinControls.Editors.ComboBox
Imports Telerik.WinControls.UI

Namespace Telerik.Examples.WinControls.Editors.DateTimePicker

    Partial Public Class Form1
        Inherits EditorExampleBaseForm
        ''' <summary>
        ''' 
        ''' </summary>
        Public Sub New()
            InitializeComponent()
            Me.SelectedControl = Me.radDateTimePicker1
            Me.radDateTimePicker3.DateTimePickerElement.ShowCurrentTime = False
            Me.radDateTimePicker3.Format = DateTimePickerFormat.Custom
            Me.radDateTimePicker3.Culture = New System.Globalization.CultureInfo("en-US")
            Me.radDateTimePicker3.DateTimePickerElement.CustomFormat = "hh:mm:ss tt"
            Me.radRadio12Hours.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On

            Me.radDateTimePicker1.DateTimePickerElement.TextBoxElement.TextBoxItem.HostedControl.Enabled = True
            Me.radDateTimePicker2.DateTimePickerElement.TextBoxElement.TextBoxItem.HostedControl.Enabled = True
            Me.radDateTimePicker3.DateTimePickerElement.TextBoxElement.TextBoxItem.HostedControl.Enabled = True

            Me.radDateTimePicker4.Format = DateTimePickerFormat.[Custom]
            Me.radDateTimePicker4.CustomFormat = "MMM - dd - yyyy hh:mm tt"
            TryCast(Me.radDateTimePicker4.DateTimePickerElement.CurrentBehavior, RadDateTimePickerCalendar).ShowTimePicker = True
            TryCast(Me.radDateTimePicker4.DateTimePickerElement.CurrentBehavior, RadDateTimePickerCalendar).DropDownMinSize = New System.Drawing.Size(330, 250)
        End Sub

        Protected Overrides Sub WireEvents()
            AddHandler radBtnClearAll.Click, AddressOf radButton1_Click
            AddHandler radRadio24Hours.ToggleStateChanged, AddressOf OnRadRadioHourFormat_ToggleStateChanged
            AddHandler radRadio12Hours.ToggleStateChanged, AddressOf OnRadRadioHourFormat_ToggleStateChanged
        End Sub

        Private Sub radButton1_Click(ByVal sender As Object, ByVal e As EventArgs)
            Me.radDateTimePicker1.DateTimePickerElement.SetToNullValue()
            Me.radDateTimePicker2.DateTimePickerElement.SetToNullValue()
            Me.radDateTimePicker3.DateTimePickerElement.SetToNullValue()
            Me.radDateTimePicker4.DateTimePickerElement.SetToNullValue()
        End Sub

        Protected Overrides Sub OnLoad(ByVal e As EventArgs)
            MyBase.OnLoad(e)

            Me.radDateTimePicker1.Value = Date.Now
            Me.radDateTimePicker2.Value = Date.Now
            Me.radDateTimePicker3.Value = Date.Now
            Me.radDateTimePicker4.Value = Date.Now
        End Sub

        Private Sub OnRadRadioHourFormat_ToggleStateChanged(ByVal sender As Object, ByVal args As Telerik.WinControls.UI.StateChangedEventArgs)
            If Me.radRadio12Hours.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
                Me.radDateTimePicker3.DateTimePickerElement.CustomFormat = "hh:mm:ss  tt"
            Else
                Me.radDateTimePicker3.DateTimePickerElement.CustomFormat = "HH:mm:ss"
            End If
        End Sub
    End Class
End Namespace