using System;
using Telerik.Examples.WinControls.Editors.ComboBox;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace Telerik.Examples.WinControls.Editors.DateTimePicker
{

    public partial class Form1 : EditorExampleBaseForm
    {
        /// <summary>
        /// 
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            this.SelectedControl = this.radDateTimePicker1;
            this.radDateTimePicker3.DateTimePickerElement.ShowCurrentTime = false;
			this.radDateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.radDateTimePicker3.Culture = new System.Globalization.CultureInfo("en-US");
            this.radDateTimePicker3.DateTimePickerElement.CustomFormat = "hh:mm:ss tt";
            this.radRadio12Hours.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;

            this.radDateTimePicker1.DateTimePickerElement.TextBoxElement.TextBoxItem.HostedControl.Enabled = true;
            this.radDateTimePicker2.DateTimePickerElement.TextBoxElement.TextBoxItem.HostedControl.Enabled = true;
            this.radDateTimePicker3.DateTimePickerElement.TextBoxElement.TextBoxItem.HostedControl.Enabled = true;

            this.radDateTimePicker4.Format = DateTimePickerFormat.Custom;
            this.radDateTimePicker4.CustomFormat = "MMM - dd - yyyy hh:mm tt";
            (this.radDateTimePicker4.DateTimePickerElement.CurrentBehavior as RadDateTimePickerCalendar).ShowTimePicker = true;
            (this.radDateTimePicker4.DateTimePickerElement.CurrentBehavior as RadDateTimePickerCalendar).DropDownMinSize = new System.Drawing.Size(330, 250);
        }

        protected override void WireEvents()
        {
            this.radBtnClearAll.Click += new System.EventHandler(this.radButton1_Click);
            this.radRadio24Hours.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.OnRadRadioHourFormat_ToggleStateChanged);
            this.radRadio12Hours.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.OnRadRadioHourFormat_ToggleStateChanged);
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            this.radDateTimePicker1.DateTimePickerElement.SetToNullValue();
            this.radDateTimePicker2.DateTimePickerElement.SetToNullValue();
            this.radDateTimePicker3.DateTimePickerElement.SetToNullValue();
            this.radDateTimePicker4.DateTimePickerElement.SetToNullValue();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.radDateTimePicker1.Value = DateTime.Now;
            this.radDateTimePicker2.Value = DateTime.Now;
            this.radDateTimePicker3.Value = DateTime.Now;
            this.radDateTimePicker4.Value = DateTime.Now;
        }

        private void OnRadRadioHourFormat_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (this.radRadio12Hours.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                this.radDateTimePicker3.DateTimePickerElement.CustomFormat = "hh:mm:ss  tt";
            }
            else
            {
                this.radDateTimePicker3.DateTimePickerElement.CustomFormat = "HH:mm:ss";
            }
        }
    }
}