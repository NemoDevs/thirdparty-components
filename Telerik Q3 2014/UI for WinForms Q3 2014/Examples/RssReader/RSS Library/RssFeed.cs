﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.IO;
using Telerik.WinControls;
using System.Threading;

namespace RssReader.RSS_Library
{
    [XmlRoot("rss")]
    public class RssFeed
    {
        [XmlElement("channel")]
        public RssChannel Channel { get; set; }

        public RssFeed()
        {

        }

        public static RssFeed Load(string url)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(RssFeed));
                using (XmlTextReader textReader = new XmlTextReader(url))
                {
                    RssFeed feed = serializer.Deserialize(textReader) as RssFeed;
                    return feed;
                }
            }
            catch (Exception)
            {
                RadMessageBox.Show("Unsupported RSS feed version");
                return null;
            }
        }
    }
}
