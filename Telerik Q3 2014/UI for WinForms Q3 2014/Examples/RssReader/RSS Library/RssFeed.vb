﻿Imports System.Text
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Net
Imports System.IO
Imports Telerik.WinControls
Imports System.Threading

Namespace CS.RSS_Library
	<XmlRoot("rss")>
	Public Class RssFeed
		<XmlElement("channel")>
		Public Property Channel() As RssChannel

		Public Sub New()

		End Sub

		Public Shared Function Load(ByVal url As String) As RssFeed
			Try
				Dim serializer As New XmlSerializer(GetType(RssFeed))
				Using textReader As New XmlTextReader(url)
					Dim feed As RssFeed = TryCast(serializer.Deserialize(textReader), RssFeed)
					Return feed
				End Using
			Catch ex As Exception
				RadMessageBox.Show("Unsupported RSS feed version")
				Return Nothing
			End Try
		End Function
	End Class
End Namespace
