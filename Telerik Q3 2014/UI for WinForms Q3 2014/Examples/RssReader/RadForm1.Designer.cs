﻿namespace RssReader
{
    partial class RadForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radCommandBar1 = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.refreshButton = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.addSubscriptionButton = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.newCategoryButton = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.windowButton = new Telerik.WinControls.UI.CommandBarDropDownButton();
            this.commandBarStripElement3 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.commandBarLabel1 = new Telerik.WinControls.UI.CommandBarLabel();
            this.office2010SilverButton = new Telerik.WinControls.UI.CommandBarButton();
            this.office2010BlackButton = new Telerik.WinControls.UI.CommandBarButton();
            this.office2010BlueButton = new Telerik.WinControls.UI.CommandBarButton();
            this.controlDefaultButton = new Telerik.WinControls.UI.CommandBarButton();
            this.telerikButton = new Telerik.WinControls.UI.CommandBarButton();
            this.radDock1 = new Telerik.WinControls.UI.Docking.RadDock();
            this.toolWindow1 = new Telerik.WinControls.UI.Docking.ToolWindow();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.feedContentCommandBar = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.scrollToPanelButton = new Telerik.WinControls.UI.CommandBarButton();
            this.openInBrowserButton = new Telerik.WinControls.UI.CommandBarButton();
            this.documentContainer1 = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.toolTabStrip1 = new Telerik.WinControls.UI.Docking.ToolTabStrip();
            this.office2010BlackTheme1 = new Telerik.WinControls.Themes.Office2010BlackTheme();
            this.office2010BlueTheme1 = new Telerik.WinControls.Themes.Office2010BlueTheme();
            this.office2010SilverTheme1 = new Telerik.WinControls.Themes.Office2010SilverTheme();
            this.telerikMetroTheme1 = new Telerik.WinControls.Themes.TelerikMetroTheme();
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDock1)).BeginInit();
            this.radDock1.SuspendLayout();
            this.toolWindow1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.feedContentCommandBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip1)).BeginInit();
            this.toolTabStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radCommandBar1
            // 
            this.radCommandBar1.AutoSize = true;
            this.radCommandBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.radCommandBar1.Name = "radCommandBar1";
            this.radCommandBar1.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.radCommandBar1.Size = new System.Drawing.Size(1069, 55);
            this.radCommandBar1.TabIndex = 0;
            this.radCommandBar1.Text = "radCommandBar1";
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.DisplayName = null;
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement2,
            this.commandBarStripElement3});
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement2.DrawText = true;
            this.commandBarStripElement2.FloatingForm = null;
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.refreshButton,
            this.commandBarSeparator1,
            this.addSubscriptionButton,
            this.commandBarSeparator2,
            this.newCategoryButton,
            this.commandBarSeparator3,
            this.windowButton});
            this.commandBarStripElement2.Name = "commandBarStripElement1";
            this.commandBarStripElement2.Text = "";
            this.commandBarStripElement2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // refreshButton
            // 
            this.refreshButton.AccessibleDescription = "commandBarButton1";
            this.refreshButton.AccessibleName = "commandBarButton1";
            this.refreshButton.DisplayName = "Refresh";
            this.refreshButton.Image = global::RssReader.Properties.Resources.arrow_refresh;
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Text = "commandBarButton1";
            this.refreshButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.refreshButton.VisibleInOverflowMenu = true;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.AccessibleDescription = "commandBarSeparator1";
            this.commandBarSeparator1.AccessibleName = "commandBarSeparator1";
            this.commandBarSeparator1.DisplayName = "commandBarSeparator1";
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // addSubscriptionButton
            // 
            this.addSubscriptionButton.AccessibleDescription = "commandBarButton2";
            this.addSubscriptionButton.AccessibleName = "commandBarButton2";
            this.addSubscriptionButton.DisplayName = "Add subscription";
            this.addSubscriptionButton.DrawText = true;
            this.addSubscriptionButton.Image = global::RssReader.Properties.Resources.feed_add;
            this.addSubscriptionButton.Name = "addSubscriptionButton";
            this.addSubscriptionButton.Text = "Add subscription";
            this.addSubscriptionButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.addSubscriptionButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.addSubscriptionButton.VisibleInOverflowMenu = true;
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.AccessibleDescription = "commandBarSeparator2";
            this.commandBarSeparator2.AccessibleName = "commandBarSeparator2";
            this.commandBarSeparator2.DisplayName = "commandBarSeparator2";
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.commandBarSeparator2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // newCategoryButton
            // 
            this.newCategoryButton.AccessibleDescription = "commandBarButton1";
            this.newCategoryButton.AccessibleName = "commandBarButton1";
            this.newCategoryButton.DisplayName = "New Category";
            this.newCategoryButton.DrawText = true;
            this.newCategoryButton.Image = global::RssReader.Properties.Resources.folder_feed;
            this.newCategoryButton.Name = "newCategoryButton";
            this.newCategoryButton.Text = "New category";
            this.newCategoryButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.newCategoryButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.newCategoryButton.VisibleInOverflowMenu = true;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.AccessibleDescription = "commandBarSeparator3";
            this.commandBarSeparator3.AccessibleName = "commandBarSeparator3";
            this.commandBarSeparator3.DisplayName = "commandBarSeparator3";
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.commandBarSeparator3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // windowButton
            // 
            this.windowButton.AccessibleDescription = "Windows";
            this.windowButton.AccessibleName = "Windows";
            this.windowButton.DisplayName = "Window";
            this.windowButton.DrawText = true;
            this.windowButton.Image = global::RssReader.Properties.Resources.application_double;
            this.windowButton.Name = "windowButton";
            this.windowButton.Text = "Window";
            this.windowButton.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.windowButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.windowButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.windowButton.VisibleInOverflowMenu = true;
            // 
            // commandBarStripElement3
            // 
            this.commandBarStripElement3.DisplayName = "commandBarStripElement3";
            this.commandBarStripElement3.FloatingForm = null;
            this.commandBarStripElement3.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.commandBarLabel1,
            this.office2010SilverButton,
            this.office2010BlackButton,
            this.office2010BlueButton,
            this.controlDefaultButton,
            this.telerikButton});
            this.commandBarStripElement3.Name = "commandBarStripElement3";
            this.commandBarStripElement3.Text = "";
            // 
            // commandBarLabel1
            // 
            this.commandBarLabel1.AccessibleDescription = "Themes:";
            this.commandBarLabel1.AccessibleName = "Themes:";
            this.commandBarLabel1.DisplayName = "commandBarLabel1";
            this.commandBarLabel1.Name = "commandBarLabel1";
            this.commandBarLabel1.Text = "Themes:";
            this.commandBarLabel1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.commandBarLabel1.VisibleInOverflowMenu = false;
            // 
            // office2010SilverButton
            // 
            this.office2010SilverButton.AccessibleDescription = "Office2010Silver";
            this.office2010SilverButton.AccessibleName = "Office2010Silver";
            this.office2010SilverButton.DisplayName = "Office2010Silver";
            this.office2010SilverButton.Image = global::RssReader.Properties.Resources.theme_silver;
            this.office2010SilverButton.Name = "office2010SilverButton";
            this.office2010SilverButton.Text = "Office2010Silver";
            this.office2010SilverButton.ToolTipText = "Office2010Silver";
            this.office2010SilverButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.office2010SilverButton.VisibleInOverflowMenu = true;
            // 
            // office2010BlackButton
            // 
            this.office2010BlackButton.AccessibleDescription = "Office2010Black";
            this.office2010BlackButton.AccessibleName = "Office2010Black";
            this.office2010BlackButton.DisplayName = "Office2010Black";
            this.office2010BlackButton.Image = global::RssReader.Properties.Resources.theme_black;
            this.office2010BlackButton.Name = "office2010BlackButton";
            this.office2010BlackButton.Text = "Office2010Black";
            this.office2010BlackButton.ToolTipText = "Office2010Black";
            this.office2010BlackButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.office2010BlackButton.VisibleInOverflowMenu = true;
            // 
            // office2010BlueButton
            // 
            this.office2010BlueButton.AccessibleDescription = "Office2010Blue";
            this.office2010BlueButton.AccessibleName = "Office2010Blue";
            this.office2010BlueButton.DisplayName = "Office2010Blue";
            this.office2010BlueButton.Image = global::RssReader.Properties.Resources.theme_blue;
            this.office2010BlueButton.Name = "office2010BlueButton";
            this.office2010BlueButton.Text = "Office2010Blue";
            this.office2010BlueButton.ToolTipText = "Office2010Blue";
            this.office2010BlueButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.office2010BlueButton.VisibleInOverflowMenu = true;
            // 
            // controlDefaultButton
            // 
            this.controlDefaultButton.AccessibleDescription = "ControlDefault";
            this.controlDefaultButton.AccessibleName = "ControlDefault";
            this.controlDefaultButton.DisplayName = "ControlDefault";
            this.controlDefaultButton.Image = global::RssReader.Properties.Resources.theme_default;
            this.controlDefaultButton.Name = "controlDefaultButton";
            this.controlDefaultButton.Text = "ControlDefault";
            this.controlDefaultButton.ToolTipText = "ControlDefault";
            this.controlDefaultButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.controlDefaultButton.VisibleInOverflowMenu = true;
            // 
            // telerikButton
            // 
            this.telerikButton.AccessibleDescription = "TelerikMetro";
            this.telerikButton.AccessibleName = "TelerikMetro";
            this.telerikButton.DisplayName = "TelerikMetro";
            this.telerikButton.Image = global::RssReader.Properties.Resources.theme_telerik;
            this.telerikButton.Name = "telerikButton";
            this.telerikButton.Text = "TelerikMetro";
            this.telerikButton.ToolTipText = "TelerikMetro";
            this.telerikButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.telerikButton.VisibleInOverflowMenu = true;
            // 
            // radDock1
            // 
            this.radDock1.ActiveWindow = this.toolWindow1;
            this.radDock1.Controls.Add(this.documentContainer1);
            this.radDock1.Controls.Add(this.toolTabStrip1);
            this.radDock1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radDock1.DocumentManager.DocumentInsertOrder = Telerik.WinControls.UI.Docking.DockWindowInsertOrder.InFront;
            this.radDock1.IsCleanUpTarget = true;
            this.radDock1.Location = new System.Drawing.Point(0, 55);
            this.radDock1.MainDocumentContainer = this.documentContainer1;
            this.radDock1.Name = "radDock1";
            this.radDock1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.radDock1.Padding = new System.Windows.Forms.Padding(5);
            // 
            // 
            // 
            this.radDock1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.radDock1.RootElement.Padding = new System.Windows.Forms.Padding(5);
            this.radDock1.Size = new System.Drawing.Size(1069, 574);
            this.radDock1.SplitterWidth = 4;
            this.radDock1.TabIndex = 1;
            this.radDock1.TabStop = false;
            this.radDock1.Text = "radDock1";
            // 
            // toolWindow1
            // 
            this.toolWindow1.Caption = null;
            this.toolWindow1.Controls.Add(this.webBrowser1);
            this.toolWindow1.Controls.Add(this.radLabel1);
            this.toolWindow1.Controls.Add(this.feedContentCommandBar);
            this.toolWindow1.Location = new System.Drawing.Point(1, 24);
            this.toolWindow1.Name = "toolWindow1";
            this.toolWindow1.PreviousDockState = Telerik.WinControls.UI.Docking.DockState.Docked;
            this.toolWindow1.Size = new System.Drawing.Size(1057, 174);
            this.toolWindow1.Text = "Feed content";
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(54, 25);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(1003, 149);
            this.webBrowser1.TabIndex = 0;
            // 
            // radLabel1
            // 
            this.radLabel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(54, 0);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(14, 25);
            this.radLabel1.TabIndex = 5;
            this.radLabel1.Text = " ";
            // 
            // feedContentCommandBar
            // 
            this.feedContentCommandBar.AutoSize = true;
            this.feedContentCommandBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.feedContentCommandBar.Location = new System.Drawing.Point(0, 0);
            this.feedContentCommandBar.Name = "feedContentCommandBar";
            this.feedContentCommandBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.feedContentCommandBar.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement2});
            this.feedContentCommandBar.Size = new System.Drawing.Size(54, 174);
            this.feedContentCommandBar.TabIndex = 4;
            this.feedContentCommandBar.Text = "radCommandBar2";
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.DisplayName = null;
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1});
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.FloatingForm = null;
            this.commandBarStripElement1.GradientAngle = 1440F;
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.scrollToPanelButton,
            this.openInBrowserButton});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            this.commandBarStripElement1.StretchHorizontally = false;
            this.commandBarStripElement1.StretchVertically = false;
            this.commandBarStripElement1.Text = "";
            // 
            // scrollToPanelButton
            // 
            this.scrollToPanelButton.AccessibleDescription = "commandBarButton1";
            this.scrollToPanelButton.AccessibleName = "commandBarButton1";
            this.scrollToPanelButton.DisplayName = "Scroll to panel";
            this.scrollToPanelButton.Image = global::RssReader.Properties.Resources.application_go;
            this.scrollToPanelButton.Name = "scrollToPanelButton";
            this.scrollToPanelButton.StretchHorizontally = false;
            this.scrollToPanelButton.StretchVertically = false;
            this.scrollToPanelButton.Text = "commandBarButton1";
            this.scrollToPanelButton.ToolTipText = "Scroll to panel";
            this.scrollToPanelButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.scrollToPanelButton.VisibleInOverflowMenu = true;
            this.scrollToPanelButton.Click += new System.EventHandler(this.scrollToPanelButon_Click);
            // 
            // openInBrowserButton
            // 
            this.openInBrowserButton.AccessibleDescription = "commandBarButton2";
            this.openInBrowserButton.AccessibleName = "commandBarButton2";
            this.openInBrowserButton.DisplayName = "Open in browser";
            this.openInBrowserButton.Image = global::RssReader.Properties.Resources.page_world;
            this.openInBrowserButton.Name = "openInBrowserButton";
            this.openInBrowserButton.StretchHorizontally = false;
            this.openInBrowserButton.StretchVertically = false;
            this.openInBrowserButton.Text = "commandBarButton2";
            this.openInBrowserButton.ToolTipText = "Open in browser";
            this.openInBrowserButton.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.openInBrowserButton.VisibleInOverflowMenu = true;
            this.openInBrowserButton.Click += new System.EventHandler(this.openInBrowserButton_Click);
            // 
            // documentContainer1
            // 
            this.documentContainer1.Location = new System.Drawing.Point(5, 5);
            this.documentContainer1.Name = "documentContainer1";
            // 
            // 
            // 
            this.documentContainer1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.documentContainer1.Size = new System.Drawing.Size(1059, 360);
            this.documentContainer1.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            this.documentContainer1.SplitterWidth = 4;
            this.documentContainer1.TabIndex = 0;
            this.documentContainer1.TabStop = false;
            // 
            // toolTabStrip1
            // 
            this.toolTabStrip1.Controls.Add(this.toolWindow1);
            this.toolTabStrip1.Location = new System.Drawing.Point(5, 369);
            this.toolTabStrip1.Name = "toolTabStrip1";
            // 
            // 
            // 
            this.toolTabStrip1.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.toolTabStrip1.SelectedIndex = 0;
            this.toolTabStrip1.Size = new System.Drawing.Size(1059, 200);
            this.toolTabStrip1.TabIndex = 1;
            this.toolTabStrip1.TabStop = false;
            // 
            // RadForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1069, 629);
            this.Controls.Add(this.radDock1);
            this.Controls.Add(this.radCommandBar1);
            this.Name = "RadForm1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Rss reader by Telerik";
            this.ThemeName = "ControlDefault";
            ((System.ComponentModel.ISupportInitialize)(this.radCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDock1)).EndInit();
            this.radDock1.ResumeLayout(false);
            this.toolWindow1.ResumeLayout(false);
            this.toolWindow1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.feedContentCommandBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.documentContainer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolTabStrip1)).EndInit();
            this.toolTabStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCommandBar radCommandBar1;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton newCategoryButton;
        private Telerik.WinControls.UI.CommandBarButton addSubscriptionButton;
        private Telerik.WinControls.UI.Docking.RadDock radDock1;
        private Telerik.WinControls.UI.Docking.ToolWindow toolWindow1;
        private Telerik.WinControls.UI.Docking.DocumentContainer documentContainer1;
        private Telerik.WinControls.UI.Docking.ToolTabStrip toolTabStrip1;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private Telerik.WinControls.UI.CommandBarDropDownButton windowButton;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement3;
        private Telerik.WinControls.UI.CommandBarButton refreshButton;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.CommandBarLabel commandBarLabel1;
        private Telerik.WinControls.UI.CommandBarButton office2010SilverButton;
        private Telerik.WinControls.UI.CommandBarButton office2010BlackButton;
        private Telerik.WinControls.UI.CommandBarButton office2010BlueButton;
        private Telerik.WinControls.UI.CommandBarButton controlDefaultButton;
        private Telerik.WinControls.UI.CommandBarButton telerikButton;
        private Telerik.WinControls.Themes.Office2010BlackTheme office2010BlackTheme1;
        private Telerik.WinControls.Themes.Office2010BlueTheme office2010BlueTheme1;
        private Telerik.WinControls.Themes.Office2010SilverTheme office2010SilverTheme1;
        private Telerik.WinControls.Themes.TelerikMetroTheme telerikMetroTheme1;
        private Telerik.WinControls.UI.RadCommandBar feedContentCommandBar;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton scrollToPanelButton;
        private Telerik.WinControls.UI.CommandBarButton openInBrowserButton;
        private Telerik.WinControls.UI.RadLabel radLabel1;
    }
}
