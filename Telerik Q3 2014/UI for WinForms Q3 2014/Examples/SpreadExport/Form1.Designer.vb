Imports Microsoft.VisualBasic
Imports System
Namespace SpreadExport
	Public Partial Class Form1
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (Not components Is Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.components = New System.ComponentModel.Container()
			Me.settingsPanel = New Telerik.WinControls.UI.RadPanel()
			Me.customizeVisualSettingsCheckBox = New Telerik.WinControls.UI.RadCheckBox()
			Me.exportHierarchyCheckBox = New Telerik.WinControls.UI.RadCheckBox()
			Me.radGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
			Me.txtRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.pdfRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.csvRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.xlsxRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.exportVisualSettingsCheckBox = New Telerik.WinControls.UI.RadCheckBox()
			Me.radGroupBox5 = New Telerik.WinControls.UI.RadGroupBox()
			Me.enablePagingCheckBox = New Telerik.WinControls.UI.RadCheckBox()
			Me.pagingAllPagesRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.pagingCurrentPageOnlyRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.radGroupBox4 = New Telerik.WinControls.UI.RadGroupBox()
			Me.showSummaryCheckBox = New Telerik.WinControls.UI.RadCheckBox()
			Me.summariesDoNotExportRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.summariesExportOnlyBottomRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.summariesExportOnlyTopRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.summariesExportAllRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.radGroupBox3 = New Telerik.WinControls.UI.RadGroupBox()
			Me.hideFirstRow = New Telerik.WinControls.UI.RadCheckBox()
			Me.hiddenRowExportAsHiddenRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.hiddenRowDoNotExportRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.hiddenRowExportAlwaysRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.radGroupBox2 = New Telerik.WinControls.UI.RadGroupBox()
			Me.hideColumnsCheckBox = New Telerik.WinControls.UI.RadCheckBox()
			Me.hiddenColumnExporAsHiddenRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.hiddenColumnDoNotExportRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.hiddenColumnExportAlwaysRadioButton = New Telerik.WinControls.UI.RadRadioButton()
			Me.exportButton = New Telerik.WinControls.UI.RadButton()
			Me.radGridView1 = New Telerik.WinControls.UI.RadGridView()
			Me.orderDetailsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
			Me.nwindDataSet = New SpreadExport.NwindDataSet()
			Me.order_DetailsTableAdapter = New SpreadExport.NwindDataSetTableAdapters.Order_DetailsTableAdapter()
			Me.ordersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
			Me.ordersTableAdapter = New SpreadExport.NwindDataSetTableAdapters.OrdersTableAdapter()
			Me.customersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
			Me.customersTableAdapter = New SpreadExport.NwindDataSetTableAdapters.CustomersTableAdapter()
			CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.settingsPanel.SuspendLayout()
			CType(Me.customizeVisualSettingsCheckBox, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.exportHierarchyCheckBox, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.radGroupBox1.SuspendLayout()
			CType(Me.txtRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.pdfRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.csvRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.xlsxRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.exportVisualSettingsCheckBox, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radGroupBox5, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.radGroupBox5.SuspendLayout()
			CType(Me.enablePagingCheckBox, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.pagingAllPagesRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.pagingCurrentPageOnlyRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radGroupBox4, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.radGroupBox4.SuspendLayout()
			CType(Me.showSummaryCheckBox, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.summariesDoNotExportRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.summariesExportOnlyBottomRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.summariesExportOnlyTopRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.summariesExportAllRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radGroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.radGroupBox3.SuspendLayout()
			CType(Me.hideFirstRow, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.hiddenRowExportAsHiddenRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.hiddenRowDoNotExportRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.hiddenRowExportAlwaysRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.radGroupBox2.SuspendLayout()
			CType(Me.hideColumnsCheckBox, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.hiddenColumnExporAsHiddenRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.hiddenColumnDoNotExportRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.hiddenColumnExportAlwaysRadioButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.exportButton, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.radGridView1.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.orderDetailsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.nwindDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.ordersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.customersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.SuspendLayout()
			' 
			' settingsPanel
			' 
			Me.settingsPanel.Controls.Add(Me.customizeVisualSettingsCheckBox)
			Me.settingsPanel.Controls.Add(Me.exportHierarchyCheckBox)
			Me.settingsPanel.Controls.Add(Me.radGroupBox1)
			Me.settingsPanel.Controls.Add(Me.exportVisualSettingsCheckBox)
			Me.settingsPanel.Controls.Add(Me.radGroupBox5)
			Me.settingsPanel.Controls.Add(Me.radGroupBox4)
			Me.settingsPanel.Controls.Add(Me.radGroupBox3)
			Me.settingsPanel.Controls.Add(Me.radGroupBox2)
			Me.settingsPanel.Controls.Add(Me.exportButton)
			Me.settingsPanel.Dock = System.Windows.Forms.DockStyle.Right
			Me.settingsPanel.Location = New System.Drawing.Point(826, 0)
			Me.settingsPanel.Name = "settingsPanel"
			Me.settingsPanel.Size = New System.Drawing.Size(200, 748)
			Me.settingsPanel.TabIndex = 3
			Me.settingsPanel.ThemeName = "ControlDefault"
			' 
			' customizeVisualSettingsCheckBox
			' 
			Me.customizeVisualSettingsCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.customizeVisualSettingsCheckBox.AutoSize = False
			Me.customizeVisualSettingsCheckBox.Location = New System.Drawing.Point(8, 41)
			Me.customizeVisualSettingsCheckBox.Name = "customizeVisualSettingsCheckBox"
			Me.customizeVisualSettingsCheckBox.Size = New System.Drawing.Size(180, 34)
			Me.customizeVisualSettingsCheckBox.TabIndex = 7
			Me.customizeVisualSettingsCheckBox.Text = "Customize visual settings when exporting"
			Me.customizeVisualSettingsCheckBox.TextWrap = True
			CType(Me.customizeVisualSettingsCheckBox.GetChildAt(0), Telerik.WinControls.UI.RadCheckBoxElement).Text = "Customize visual settings when exporting"
			' 
			' exportHierarchyCheckBox
			' 
			Me.exportHierarchyCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.exportHierarchyCheckBox.Location = New System.Drawing.Point(8, 105)
			Me.exportHierarchyCheckBox.Name = "exportHierarchyCheckBox"
			Me.exportHierarchyCheckBox.Size = New System.Drawing.Size(100, 18)
			Me.exportHierarchyCheckBox.TabIndex = 7
			Me.exportHierarchyCheckBox.Text = "ExportHierarchy"
'			Me.exportHierarchyCheckBox.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.exportHierarchyCheckBox_ToggleStateChanged);
			' 
			' radGroupBox1
			' 
			Me.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
			Me.radGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.radGroupBox1.Controls.Add(Me.txtRadioButton)
			Me.radGroupBox1.Controls.Add(Me.pdfRadioButton)
			Me.radGroupBox1.Controls.Add(Me.csvRadioButton)
			Me.radGroupBox1.Controls.Add(Me.xlsxRadioButton)
			Me.radGroupBox1.HeaderText = "Export format"
			Me.radGroupBox1.Location = New System.Drawing.Point(8, 130)
			Me.radGroupBox1.Name = "radGroupBox1"
			Me.radGroupBox1.Size = New System.Drawing.Size(180, 117)
			Me.radGroupBox1.TabIndex = 1
			Me.radGroupBox1.Text = "Export format"
			' 
			' txtRadioButton
			' 
			Me.txtRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.txtRadioButton.Location = New System.Drawing.Point(5, 94)
			Me.txtRadioButton.Name = "txtRadioButton"
			Me.txtRadioButton.Size = New System.Drawing.Size(39, 18)
			Me.txtRadioButton.TabIndex = 2
			Me.txtRadioButton.TabStop = False
			Me.txtRadioButton.Text = "TXT"
'			Me.txtRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.txtRadioButton_ToggleStateChanged);
			' 
			' pdfRadioButton
			' 
			Me.pdfRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.pdfRadioButton.Location = New System.Drawing.Point(5, 70)
			Me.pdfRadioButton.Name = "pdfRadioButton"
			Me.pdfRadioButton.Size = New System.Drawing.Size(40, 18)
			Me.pdfRadioButton.TabIndex = 1
			Me.pdfRadioButton.TabStop = False
			Me.pdfRadioButton.Text = "PDF"
'			Me.pdfRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.pdfRadioButton_ToggleStateChanged);
			' 
			' csvRadioButton
			' 
			Me.csvRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.csvRadioButton.Location = New System.Drawing.Point(5, 46)
			Me.csvRadioButton.Name = "csvRadioButton"
			Me.csvRadioButton.Size = New System.Drawing.Size(40, 18)
			Me.csvRadioButton.TabIndex = 1
			Me.csvRadioButton.TabStop = False
			Me.csvRadioButton.Text = "CSV"
'			Me.csvRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.csvRadioButton_ToggleStateChanged);
			' 
			' xlsxRadioButton
			' 
			Me.xlsxRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.xlsxRadioButton.Location = New System.Drawing.Point(5, 22)
			Me.xlsxRadioButton.Name = "xlsxRadioButton"
			Me.xlsxRadioButton.Size = New System.Drawing.Size(45, 18)
			Me.xlsxRadioButton.TabIndex = 0
			Me.xlsxRadioButton.TabStop = False
			Me.xlsxRadioButton.Text = "XLSX"
'			Me.xlsxRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.xlsxRadioButton_ToggleStateChanged);
			' 
			' exportVisualSettingsCheckBox
			' 
			Me.exportVisualSettingsCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.exportVisualSettingsCheckBox.Location = New System.Drawing.Point(8, 81)
			Me.exportVisualSettingsCheckBox.Name = "exportVisualSettingsCheckBox"
			Me.exportVisualSettingsCheckBox.Size = New System.Drawing.Size(122, 18)
			Me.exportVisualSettingsCheckBox.TabIndex = 6
			Me.exportVisualSettingsCheckBox.Text = "ExportVisualSettings"
'			Me.exportVisualSettingsCheckBox.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.exportVisualSettingsCheckBox_ToggleStateChanged);
			' 
			' radGroupBox5
			' 
			Me.radGroupBox5.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
			Me.radGroupBox5.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.radGroupBox5.Controls.Add(Me.enablePagingCheckBox)
			Me.radGroupBox5.Controls.Add(Me.pagingAllPagesRadioButton)
			Me.radGroupBox5.Controls.Add(Me.pagingCurrentPageOnlyRadioButton)
			Me.radGroupBox5.HeaderText = "Paging export option"
			Me.radGroupBox5.Location = New System.Drawing.Point(8, 649)
			Me.radGroupBox5.Name = "radGroupBox5"
			Me.radGroupBox5.Size = New System.Drawing.Size(180, 91)
			Me.radGroupBox5.TabIndex = 5
			Me.radGroupBox5.Text = "Paging export option"
			' 
			' enablePagingCheckBox
			' 
			Me.enablePagingCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.enablePagingCheckBox.Location = New System.Drawing.Point(5, 21)
			Me.enablePagingCheckBox.Name = "enablePagingCheckBox"
			Me.enablePagingCheckBox.Size = New System.Drawing.Size(91, 18)
			Me.enablePagingCheckBox.TabIndex = 8
			Me.enablePagingCheckBox.Text = "Enable paging"
'			Me.enablePagingCheckBox.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.radCheckBox3_ToggleStateChanged);
			' 
			' pagingAllPagesRadioButton
			' 
			Me.pagingAllPagesRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.pagingAllPagesRadioButton.Location = New System.Drawing.Point(5, 69)
			Me.pagingAllPagesRadioButton.Name = "pagingAllPagesRadioButton"
			Me.pagingAllPagesRadioButton.Size = New System.Drawing.Size(62, 18)
			Me.pagingAllPagesRadioButton.TabIndex = 1
			Me.pagingAllPagesRadioButton.Text = "AllPages"
'			Me.pagingAllPagesRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.pagingAllPagesRadioButton_ToggleStateChanged);
			' 
			' pagingCurrentPageOnlyRadioButton
			' 
			Me.pagingCurrentPageOnlyRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.pagingCurrentPageOnlyRadioButton.Location = New System.Drawing.Point(5, 45)
			Me.pagingCurrentPageOnlyRadioButton.Name = "pagingCurrentPageOnlyRadioButton"
			Me.pagingCurrentPageOnlyRadioButton.Size = New System.Drawing.Size(106, 18)
			Me.pagingCurrentPageOnlyRadioButton.TabIndex = 0
			Me.pagingCurrentPageOnlyRadioButton.Text = "CurrentPageOnly"
'			Me.pagingCurrentPageOnlyRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.pagingCurrentPageOnlyRadioButton_ToggleStateChanged);
			' 
			' radGroupBox4
			' 
			Me.radGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
			Me.radGroupBox4.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.radGroupBox4.Controls.Add(Me.showSummaryCheckBox)
			Me.radGroupBox4.Controls.Add(Me.summariesDoNotExportRadioButton)
			Me.radGroupBox4.Controls.Add(Me.summariesExportOnlyBottomRadioButton)
			Me.radGroupBox4.Controls.Add(Me.summariesExportOnlyTopRadioButton)
			Me.radGroupBox4.Controls.Add(Me.summariesExportAllRadioButton)
			Me.radGroupBox4.HeaderText = "Summaries option"
			Me.radGroupBox4.Location = New System.Drawing.Point(8, 503)
			Me.radGroupBox4.Name = "radGroupBox4"
			Me.radGroupBox4.Size = New System.Drawing.Size(180, 140)
			Me.radGroupBox4.TabIndex = 4
			Me.radGroupBox4.Text = "Summaries option"
			' 
			' showSummaryCheckBox
			' 
			Me.showSummaryCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.showSummaryCheckBox.Location = New System.Drawing.Point(5, 21)
			Me.showSummaryCheckBox.Name = "showSummaryCheckBox"
			Me.showSummaryCheckBox.Size = New System.Drawing.Size(96, 18)
			Me.showSummaryCheckBox.TabIndex = 11
			Me.showSummaryCheckBox.Text = "Show summary"
'			Me.showSummaryCheckBox.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.showSummaryCheckBox_ToggleStateChanged);
			' 
			' summariesDoNotExportRadioButton
			' 
			Me.summariesDoNotExportRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.summariesDoNotExportRadioButton.Location = New System.Drawing.Point(5, 117)
			Me.summariesDoNotExportRadioButton.Name = "summariesDoNotExportRadioButton"
			Me.summariesDoNotExportRadioButton.Size = New System.Drawing.Size(86, 18)
			Me.summariesDoNotExportRadioButton.TabIndex = 2
			Me.summariesDoNotExportRadioButton.Text = "DoNotExport"
'			Me.summariesDoNotExportRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.summariesDoNotExportRadioButton_ToggleStateChanged);
			' 
			' summariesExportOnlyBottomRadioButton
			' 
			Me.summariesExportOnlyBottomRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.summariesExportOnlyBottomRadioButton.Location = New System.Drawing.Point(5, 93)
			Me.summariesExportOnlyBottomRadioButton.Name = "summariesExportOnlyBottomRadioButton"
			Me.summariesExportOnlyBottomRadioButton.Size = New System.Drawing.Size(113, 18)
			Me.summariesExportOnlyBottomRadioButton.TabIndex = 1
			Me.summariesExportOnlyBottomRadioButton.Text = "ExportOnlyBottom"
'			Me.summariesExportOnlyBottomRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.summariesExportOnlyBottomRadioButton_ToggleStateChanged);
			' 
			' summariesExportOnlyTopRadioButton
			' 
			Me.summariesExportOnlyTopRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.summariesExportOnlyTopRadioButton.Location = New System.Drawing.Point(5, 69)
			Me.summariesExportOnlyTopRadioButton.Name = "summariesExportOnlyTopRadioButton"
			Me.summariesExportOnlyTopRadioButton.Size = New System.Drawing.Size(95, 18)
			Me.summariesExportOnlyTopRadioButton.TabIndex = 1
			Me.summariesExportOnlyTopRadioButton.Text = "ExportOnlyTop"
'			Me.summariesExportOnlyTopRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.summariesExportOnlyTopRadioButton_ToggleStateChanged);
			' 
			' summariesExportAllRadioButton
			' 
			Me.summariesExportAllRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.summariesExportAllRadioButton.Location = New System.Drawing.Point(5, 45)
			Me.summariesExportAllRadioButton.Name = "summariesExportAllRadioButton"
			Me.summariesExportAllRadioButton.Size = New System.Drawing.Size(65, 18)
			Me.summariesExportAllRadioButton.TabIndex = 0
			Me.summariesExportAllRadioButton.Text = "ExportAll"
'			Me.summariesExportAllRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.summariesExportAllRadioButton_ToggleStateChanged);
			' 
			' radGroupBox3
			' 
			Me.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
			Me.radGroupBox3.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.radGroupBox3.Controls.Add(Me.hideFirstRow)
			Me.radGroupBox3.Controls.Add(Me.hiddenRowExportAsHiddenRadioButton)
			Me.radGroupBox3.Controls.Add(Me.hiddenRowDoNotExportRadioButton)
			Me.radGroupBox3.Controls.Add(Me.hiddenRowExportAlwaysRadioButton)
			Me.radGroupBox3.HeaderText = "Hidden row option"
			Me.radGroupBox3.Location = New System.Drawing.Point(8, 381)
			Me.radGroupBox3.Name = "radGroupBox3"
			Me.radGroupBox3.Size = New System.Drawing.Size(180, 116)
			Me.radGroupBox3.TabIndex = 4
			Me.radGroupBox3.Text = "Hidden row option"
			' 
			' hideFirstRow
			' 
			Me.hideFirstRow.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.hideFirstRow.Location = New System.Drawing.Point(5, 21)
			Me.hideFirstRow.Name = "hideFirstRow"
			Me.hideFirstRow.Size = New System.Drawing.Size(87, 18)
			Me.hideFirstRow.TabIndex = 10
			Me.hideFirstRow.Text = "Hide first row"
'			Me.hideFirstRow.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.hideFirstRow_ToggleStateChanged);
			' 
			' hiddenRowExportAsHiddenRadioButton
			' 
			Me.hiddenRowExportAsHiddenRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.hiddenRowExportAsHiddenRadioButton.Location = New System.Drawing.Point(5, 93)
			Me.hiddenRowExportAsHiddenRadioButton.Name = "hiddenRowExportAsHiddenRadioButton"
			Me.hiddenRowExportAsHiddenRadioButton.Size = New System.Drawing.Size(101, 18)
			Me.hiddenRowExportAsHiddenRadioButton.TabIndex = 1
			Me.hiddenRowExportAsHiddenRadioButton.Text = "ExportAsHidden"
'			Me.hiddenRowExportAsHiddenRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.hiddenRowExportAsHiddenRadioButton_ToggleStateChanged);
			' 
			' hiddenRowDoNotExportRadioButton
			' 
			Me.hiddenRowDoNotExportRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.hiddenRowDoNotExportRadioButton.Location = New System.Drawing.Point(5, 69)
			Me.hiddenRowDoNotExportRadioButton.Name = "hiddenRowDoNotExportRadioButton"
			Me.hiddenRowDoNotExportRadioButton.Size = New System.Drawing.Size(86, 18)
			Me.hiddenRowDoNotExportRadioButton.TabIndex = 1
			Me.hiddenRowDoNotExportRadioButton.Text = "DoNotExport"
'			Me.hiddenRowDoNotExportRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.hiddenRowDoNotExportRadioButton_ToggleStateChanged);
			' 
			' hiddenRowExportAlwaysRadioButton
			' 
			Me.hiddenRowExportAlwaysRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.hiddenRowExportAlwaysRadioButton.Location = New System.Drawing.Point(5, 45)
			Me.hiddenRowExportAlwaysRadioButton.Name = "hiddenRowExportAlwaysRadioButton"
			Me.hiddenRowExportAlwaysRadioButton.Size = New System.Drawing.Size(86, 18)
			Me.hiddenRowExportAlwaysRadioButton.TabIndex = 0
			Me.hiddenRowExportAlwaysRadioButton.Text = "ExportAlways"
'			Me.hiddenRowExportAlwaysRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.hiddenRowExportAlwaysRadioButton_ToggleStateChanged);
			' 
			' radGroupBox2
			' 
			Me.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
			Me.radGroupBox2.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.radGroupBox2.Controls.Add(Me.hideColumnsCheckBox)
			Me.radGroupBox2.Controls.Add(Me.hiddenColumnExporAsHiddenRadioButton)
			Me.radGroupBox2.Controls.Add(Me.hiddenColumnDoNotExportRadioButton)
			Me.radGroupBox2.Controls.Add(Me.hiddenColumnExportAlwaysRadioButton)
			Me.radGroupBox2.HeaderText = "Hidden column option"
			Me.radGroupBox2.Location = New System.Drawing.Point(8, 253)
			Me.radGroupBox2.Name = "radGroupBox2"
			Me.radGroupBox2.Size = New System.Drawing.Size(180, 122)
			Me.radGroupBox2.TabIndex = 3
			Me.radGroupBox2.Text = "Hidden column option"
			' 
			' hideColumnsCheckBox
			' 
			Me.hideColumnsCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.hideColumnsCheckBox.Location = New System.Drawing.Point(5, 21)
			Me.hideColumnsCheckBox.Name = "hideColumnsCheckBox"
			Me.hideColumnsCheckBox.Size = New System.Drawing.Size(119, 18)
			Me.hideColumnsCheckBox.TabIndex = 9
			Me.hideColumnsCheckBox.Text = "Hide some columns"
'			Me.hideColumnsCheckBox.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.hideColumnsCheckBox_ToggleStateChanged);
			' 
			' hiddenColumnExporAsHiddenRadioButton
			' 
			Me.hiddenColumnExporAsHiddenRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.hiddenColumnExporAsHiddenRadioButton.Location = New System.Drawing.Point(5, 93)
			Me.hiddenColumnExporAsHiddenRadioButton.Name = "hiddenColumnExporAsHiddenRadioButton"
			Me.hiddenColumnExporAsHiddenRadioButton.Size = New System.Drawing.Size(101, 18)
			Me.hiddenColumnExporAsHiddenRadioButton.TabIndex = 1
			Me.hiddenColumnExporAsHiddenRadioButton.Text = "ExportAsHidden"
'			Me.hiddenColumnExporAsHiddenRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.hiddenColumnExporAsHiddenRadioButton_ToggleStateChanged);
			' 
			' hiddenColumnDoNotExportRadioButton
			' 
			Me.hiddenColumnDoNotExportRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.hiddenColumnDoNotExportRadioButton.Location = New System.Drawing.Point(5, 69)
			Me.hiddenColumnDoNotExportRadioButton.Name = "hiddenColumnDoNotExportRadioButton"
			Me.hiddenColumnDoNotExportRadioButton.Size = New System.Drawing.Size(86, 18)
			Me.hiddenColumnDoNotExportRadioButton.TabIndex = 1
			Me.hiddenColumnDoNotExportRadioButton.Text = "DoNotExport"
'			Me.hiddenColumnDoNotExportRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.hiddenColumnDoNotExportRadioButton_ToggleStateChanged);
			' 
			' hiddenColumnExportAlwaysRadioButton
			' 
			Me.hiddenColumnExportAlwaysRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.hiddenColumnExportAlwaysRadioButton.Location = New System.Drawing.Point(5, 45)
			Me.hiddenColumnExportAlwaysRadioButton.Name = "hiddenColumnExportAlwaysRadioButton"
			Me.hiddenColumnExportAlwaysRadioButton.Size = New System.Drawing.Size(86, 18)
			Me.hiddenColumnExportAlwaysRadioButton.TabIndex = 0
			Me.hiddenColumnExportAlwaysRadioButton.Text = "ExportAlways"
'			Me.hiddenColumnExportAlwaysRadioButton.ToggleStateChanged += New Telerik.WinControls.UI.StateChangedEventHandler(Me.hiddenColumnExportAlwaysRadioButton_ToggleStateChanged);
			' 
			' exportButton
			' 
			Me.exportButton.Anchor = System.Windows.Forms.AnchorStyles.Top
			Me.exportButton.Location = New System.Drawing.Point(8, 12)
			Me.exportButton.Name = "exportButton"
			Me.exportButton.Size = New System.Drawing.Size(180, 23)
			Me.exportButton.TabIndex = 0
			Me.exportButton.Text = "Export"
'			Me.exportButton.Click += New System.EventHandler(Me.exportButton_Click);
			' 
			' radGridView1
			' 
			Me.radGridView1.Dock = System.Windows.Forms.DockStyle.Fill
			Me.radGridView1.Location = New System.Drawing.Point(0, 0)
			' 
			' radGridView1
			' 
			Me.radGridView1.MasterTemplate.PageSize = 10
			Me.radGridView1.Name = "radGridView1"
			Me.radGridView1.Size = New System.Drawing.Size(826, 748)
			Me.radGridView1.TabIndex = 2
			Me.radGridView1.Text = "radGridView1"
			' 
			' orderDetailsBindingSource
			' 
			Me.orderDetailsBindingSource.DataMember = "Order Details"
			Me.orderDetailsBindingSource.DataSource = Me.nwindDataSet
			' 
			' nwindDataSet
			' 
			Me.nwindDataSet.DataSetName = "NwindDataSet"
			Me.nwindDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
			' 
			' order_DetailsTableAdapter
			' 
			Me.order_DetailsTableAdapter.ClearBeforeFill = True
			' 
			' ordersBindingSource
			' 
			Me.ordersBindingSource.DataMember = "Orders"
			Me.ordersBindingSource.DataSource = Me.nwindDataSet
			' 
			' ordersTableAdapter
			' 
			Me.ordersTableAdapter.ClearBeforeFill = True
			' 
			' customersBindingSource
			' 
			Me.customersBindingSource.DataMember = "Customers"
			Me.customersBindingSource.DataSource = Me.nwindDataSet
			' 
			' customersTableAdapter
			' 
			Me.customersTableAdapter.ClearBeforeFill = True
			' 
			' Form1
			' 
			Me.AutoScaleDimensions = New System.Drawing.SizeF(6F, 13F)
			Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
			Me.ClientSize = New System.Drawing.Size(1026, 748)
			Me.Controls.Add(Me.radGridView1)
			Me.Controls.Add(Me.settingsPanel)
			Me.Name = "Form1"
			' 
			' 
			' 
			Me.RootElement.ApplyShapeToControl = True
'			Me.Load += New System.EventHandler(Me.Form1_Load);
			CType(Me.settingsPanel, System.ComponentModel.ISupportInitialize).EndInit()
			Me.settingsPanel.ResumeLayout(False)
			Me.settingsPanel.PerformLayout()
			CType(Me.customizeVisualSettingsCheckBox, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.exportHierarchyCheckBox, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
			Me.radGroupBox1.ResumeLayout(False)
			Me.radGroupBox1.PerformLayout()
			CType(Me.txtRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.pdfRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.csvRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.xlsxRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.exportVisualSettingsCheckBox, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radGroupBox5, System.ComponentModel.ISupportInitialize).EndInit()
			Me.radGroupBox5.ResumeLayout(False)
			Me.radGroupBox5.PerformLayout()
			CType(Me.enablePagingCheckBox, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.pagingAllPagesRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.pagingCurrentPageOnlyRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radGroupBox4, System.ComponentModel.ISupportInitialize).EndInit()
			Me.radGroupBox4.ResumeLayout(False)
			Me.radGroupBox4.PerformLayout()
			CType(Me.showSummaryCheckBox, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.summariesDoNotExportRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.summariesExportOnlyBottomRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.summariesExportOnlyTopRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.summariesExportAllRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radGroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
			Me.radGroupBox3.ResumeLayout(False)
			Me.radGroupBox3.PerformLayout()
			CType(Me.hideFirstRow, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.hiddenRowExportAsHiddenRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.hiddenRowDoNotExportRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.hiddenRowExportAlwaysRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
			Me.radGroupBox2.ResumeLayout(False)
			Me.radGroupBox2.PerformLayout()
			CType(Me.hideColumnsCheckBox, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.hiddenColumnExporAsHiddenRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.hiddenColumnDoNotExportRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.hiddenColumnExportAlwaysRadioButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.exportButton, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radGridView1.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.radGridView1, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.orderDetailsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.nwindDataSet, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.ordersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.customersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
			Me.ResumeLayout(False)

		End Sub

		#End Region

		Private WithEvents exportButton As Telerik.WinControls.UI.RadButton
		Private radGroupBox2 As Telerik.WinControls.UI.RadGroupBox
		Private WithEvents hiddenColumnExporAsHiddenRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private WithEvents hiddenColumnDoNotExportRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private WithEvents hiddenColumnExportAlwaysRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private radGroupBox1 As Telerik.WinControls.UI.RadGroupBox
		Private WithEvents txtRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private WithEvents pdfRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private WithEvents csvRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private WithEvents xlsxRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private radGroupBox5 As Telerik.WinControls.UI.RadGroupBox
		Private WithEvents pagingAllPagesRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private WithEvents pagingCurrentPageOnlyRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private radGroupBox4 As Telerik.WinControls.UI.RadGroupBox
		Private WithEvents summariesDoNotExportRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private WithEvents summariesExportOnlyBottomRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private WithEvents summariesExportOnlyTopRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private WithEvents summariesExportAllRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private radGroupBox3 As Telerik.WinControls.UI.RadGroupBox
		Private WithEvents hiddenRowExportAsHiddenRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private WithEvents hiddenRowDoNotExportRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private WithEvents hiddenRowExportAlwaysRadioButton As Telerik.WinControls.UI.RadRadioButton
		Private WithEvents exportHierarchyCheckBox As Telerik.WinControls.UI.RadCheckBox
		Private WithEvents exportVisualSettingsCheckBox As Telerik.WinControls.UI.RadCheckBox
		Private WithEvents enablePagingCheckBox As Telerik.WinControls.UI.RadCheckBox
		Private radGridView1 As Telerik.WinControls.UI.RadGridView
		Private WithEvents showSummaryCheckBox As Telerik.WinControls.UI.RadCheckBox
		Private WithEvents hideFirstRow As Telerik.WinControls.UI.RadCheckBox
		Private WithEvents hideColumnsCheckBox As Telerik.WinControls.UI.RadCheckBox
		Private customizeVisualSettingsCheckBox As Telerik.WinControls.UI.RadCheckBox
		Private settingsPanel As Telerik.WinControls.UI.RadPanel
		Private nwindDataSet As SpreadExport.NwindDataSet
		Private orderDetailsBindingSource As System.Windows.Forms.BindingSource
		Private order_DetailsTableAdapter As SpreadExport.NwindDataSetTableAdapters.Order_DetailsTableAdapter
		Private ordersBindingSource As System.Windows.Forms.BindingSource
		Private ordersTableAdapter As SpreadExport.NwindDataSetTableAdapters.OrdersTableAdapter
		Private customersBindingSource As System.Windows.Forms.BindingSource
		Private customersTableAdapter As NwindDataSetTableAdapters.CustomersTableAdapter
	End Class
End Namespace
