using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.Data;
using Telerik.WinControls.Themes;
using Telerik.WinControls.UI;

namespace SpreadExport
{
    public partial class Form1 : RadForm
    {
        Telerik.WinControls.UI.Export.SpreadExport.SpreadExport spreadExporter;

        public Form1()
        {
            InitializeComponent();

            Telerik.WinControls.ThemeResolutionService.ApplicationThemeName = "TelerikMetro"; //set default theme

            if (Program.themeName != "") //set the example theme to the same theme QSF uses
            {
                Telerik.WinControls.ThemeResolutionService.ApplicationThemeName = Program.themeName;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.customersTableAdapter.Fill(this.nwindDataSet.Customers);
            this.ordersTableAdapter.Fill(this.nwindDataSet.Orders);
            this.order_DetailsTableAdapter.Fill(this.nwindDataSet.Order_Details);

            SetupExample();

            spreadExporter = new Telerik.WinControls.UI.Export.SpreadExport.SpreadExport(radGridView1);
            spreadExporter.CellFormatting += spreadExporter_CellFormatting;

            SetupInitialSettings();
        }

        void spreadExporter_CellFormatting(object sender, Telerik.WinControls.UI.Export.SpreadExport.CellFormattingEventArgs e)
        {
            if (customizeVisualSettingsCheckBox.ToggleState != Telerik.WinControls.Enumerations.ToggleState.On)
            {
                return;
            }

            if (e.GridRowInfoType == typeof(GridViewTableHeaderRowInfo))
            {
                e.CellStyleInfo.Underline = true;

                if (e.GridCellInfo.RowInfo.HierarchyLevel == 0)
                {
                    e.CellStyleInfo.BackColor = Color.DeepSkyBlue;
                }
                else if (e.GridCellInfo.RowInfo.HierarchyLevel == 1)
                {
                    e.CellStyleInfo.BackColor = Color.LightSkyBlue;
                }
            }

            if (e.GridRowInfoType == typeof(GridViewHierarchyRowInfo))
            {
                if (e.GridCellInfo.RowInfo.HierarchyLevel == 0)
                {
                    e.CellStyleInfo.IsItalic = true;
                    e.CellStyleInfo.FontSize = 12;
                    e.CellStyleInfo.BackColor = Color.GreenYellow;
                }
                else if (e.GridCellInfo.RowInfo.HierarchyLevel == 1)
                {
                    e.CellStyleInfo.ForeColor = Color.DarkGreen;
                    e.CellStyleInfo.BackColor = Color.LightGreen;
                }
            }
        }

        private void SetupInitialSettings()
        {
            exportVisualSettingsCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            exportHierarchyCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            xlsxRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            hiddenColumnDoNotExportRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            hiddenRowDoNotExportRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            summariesExportAllRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            pagingCurrentPageOnlyRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;

            hideColumnsCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
        }

        private void SetupExample()
        {
            this.radGridView1.DataMember = "Customers";
            this.radGridView1.AutoGenerateHierarchy = true;
            this.radGridView1.DataSource = nwindDataSet;
            this.SetupMasterForAutoGenerateHierarchy();

            this.radGridView1.GroupDescriptors.Add(new GroupDescriptor(new SortDescriptor("City", ListSortDirection.Ascending)));
            this.radGridView1.Groups[0].Expand();

            this.radGridView1.Groups[0].GetItems()[0].IsExpanded = true;
            this.radGridView1.Groups[0].GetItems()[0].ChildRows[0].IsExpanded = true;
        }

        private void SetupMasterForAutoGenerateHierarchy()
        {
            this.radGridView1.MasterTemplate.Columns["CompanyName"].HeaderText = "Company Name";
            this.radGridView1.MasterTemplate.Columns["ContactName"].HeaderText = "Contact Name";

            this.radGridView1.MasterTemplate.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            GridViewTemplate template = this.radGridView1.MasterTemplate.Templates[0];
            template.Columns["OrderID"].HeaderText = "Order ID";
            template.Columns["ShipAddress"].HeaderText = "Ship Address";
            template.Columns["OrderDate"].HeaderText = "Order Date";
            template.Columns["OrderDate"].TextAlignment = ContentAlignment.MiddleRight;
            template.Columns["OrderDate"].FormatString = "{0: ddd, MM/dd}";
            template.Columns["CustomerID"].IsVisible = false;
            template.Columns["EmployeeID"].IsVisible = false;
            template.Columns["RequiredDate"].IsVisible = false;
            template.Columns["ShippedDate"].IsVisible = false;
            template.Columns["ShipVia"].IsVisible = false;
            template.Columns["ShipName"].IsVisible = false;
            template.Columns["ShipCity"].IsVisible = false;
            template.Columns["ShipRegion"].IsVisible = false;
            template.Columns["ShipPostalCode"].IsVisible = false;
            template.Columns["ShipCity"].IsVisible = false;
            template.Columns["ShipName"].IsVisible = false;
            template.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;

            this.radGridView1.MasterTemplate.Templates[0].Templates.RemoveAt(0);
        }

        #region Event handlers

        private void exportButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.FileName = "exportedFile";
            if (xlsxRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                dialog.Filter = "xlsx files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            }
            else if (csvRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                dialog.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
            }
            else if (pdfRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                dialog.Filter = "pdf files (*.pdf)|*.pdf|All files (*.*)|*.*";
            }
            else if (txtRadioButton.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                dialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            }

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                spreadExporter.RunExport(dialog.FileName);
            }
        }

        private void hideColumnsCheckBox_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            this.radGridView1.MasterTemplate.Columns["CustomerID"].IsVisible = args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off;
            this.radGridView1.MasterTemplate.Columns["ContactTitle"].IsVisible = args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off;
            this.radGridView1.MasterTemplate.Columns["Region"].IsVisible = args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off;
            this.radGridView1.MasterTemplate.Columns["PostalCode"].IsVisible = args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off;
            this.radGridView1.MasterTemplate.Columns["Country"].IsVisible = args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off;
            this.radGridView1.MasterTemplate.Columns["Fax"].IsVisible = args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off;
        }

        private void hideFirstRow_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.ChildRows[0].IsVisible = args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.Off;
        }

        private void showSummaryCheckBox_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                GridViewSummaryItem summaryItem = new GridViewSummaryItem();
                summaryItem.Name = "CompanyName";
                summaryItem.Aggregate = GridAggregateFunction.Count;

                GridViewSummaryRowItem summaryRowTopItem = new GridViewSummaryRowItem();
                summaryRowTopItem.Add(summaryItem);
                this.radGridView1.SummaryRowsTop.Add(summaryRowTopItem);

                GridViewSummaryRowItem summaryRowBottomItem = new GridViewSummaryRowItem();
                summaryRowBottomItem.Add(summaryItem);
                this.radGridView1.SummaryRowsBottom.Add(summaryRowBottomItem);
            }
            else
            {
                radGridView1.SummaryRowsBottom.Clear();
                radGridView1.SummaryRowsTop.Clear();
            }
        }

        private void exportVisualSettingsCheckBox_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            spreadExporter.ExportVisualSettings = args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On;
        }

        private void exportHierarchyCheckBox_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            spreadExporter.ExportHierarchy = args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On;
        }

        void xlsxRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.ExportFormat = Telerik.WinControls.UI.Export.SpreadExport.SpreadExportFormat.Xlsx;
            }
        }

        private void csvRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.ExportFormat = Telerik.WinControls.UI.Export.SpreadExport.SpreadExportFormat.Csv;
            }
        }

        private void pdfRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.ExportFormat = Telerik.WinControls.UI.Export.SpreadExport.SpreadExportFormat.Pdf;
            }
        }

        private void txtRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.ExportFormat = Telerik.WinControls.UI.Export.SpreadExport.SpreadExportFormat.Txt;
            }
        }

        private void hiddenColumnExportAlwaysRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.HiddenColumnOption = Telerik.WinControls.UI.Export.HiddenOption.ExportAlways;
            }
        }

        private void hiddenColumnDoNotExportRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.HiddenColumnOption = Telerik.WinControls.UI.Export.HiddenOption.DoNotExport;
            }
        }

        private void hiddenColumnExporAsHiddenRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.HiddenColumnOption = Telerik.WinControls.UI.Export.HiddenOption.ExportAsHidden;
            }
        }

        private void hiddenRowExportAlwaysRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.HiddenRowOption = Telerik.WinControls.UI.Export.HiddenOption.ExportAlways;
            }
        }

        private void hiddenRowDoNotExportRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.HiddenRowOption = Telerik.WinControls.UI.Export.HiddenOption.DoNotExport;
            }
        }

        private void hiddenRowExportAsHiddenRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.HiddenRowOption = Telerik.WinControls.UI.Export.HiddenOption.ExportAsHidden;
            }
        }

        private void summariesExportAllRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.SummariesExportOption = Telerik.WinControls.UI.Export.SummariesOption.ExportAll;
            }
        }

        private void summariesExportOnlyTopRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.SummariesExportOption = Telerik.WinControls.UI.Export.SummariesOption.ExportOnlyTop;
            }
        }

        private void summariesExportOnlyBottomRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.SummariesExportOption = Telerik.WinControls.UI.Export.SummariesOption.ExportOnlyBottom;
            }
        }

        private void summariesDoNotExportRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.SummariesExportOption = Telerik.WinControls.UI.Export.SummariesOption.DoNotExport;
            }
        }

        private void radCheckBox3_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            radGridView1.EnablePaging = args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On;
        }

        private void pagingCurrentPageOnlyRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.PagingExportOption = Telerik.WinControls.UI.Export.PagingExportOption.CurrentPageOnly;
            }
        }

        private void pagingAllPagesRadioButton_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (args.ToggleState == Telerik.WinControls.Enumerations.ToggleState.On)
            {
                spreadExporter.PagingExportOption = Telerik.WinControls.UI.Export.PagingExportOption.AllPages;
            }
        }

        #endregion
    }
}