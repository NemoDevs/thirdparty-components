namespace SpreadExport
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.settingsPanel = new Telerik.WinControls.UI.RadPanel();
            this.customizeVisualSettingsCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.exportHierarchyCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.txtRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.pdfRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.csvRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.xlsxRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.exportVisualSettingsCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox5 = new Telerik.WinControls.UI.RadGroupBox();
            this.enablePagingCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.pagingAllPagesRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.pagingCurrentPageOnlyRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupBox4 = new Telerik.WinControls.UI.RadGroupBox();
            this.showSummaryCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.summariesDoNotExportRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.summariesExportOnlyBottomRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.summariesExportOnlyTopRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.summariesExportAllRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupBox3 = new Telerik.WinControls.UI.RadGroupBox();
            this.hideFirstRow = new Telerik.WinControls.UI.RadCheckBox();
            this.hiddenRowExportAsHiddenRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.hiddenRowDoNotExportRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.hiddenRowExportAlwaysRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.hideColumnsCheckBox = new Telerik.WinControls.UI.RadCheckBox();
            this.hiddenColumnExporAsHiddenRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.hiddenColumnDoNotExportRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.hiddenColumnExportAlwaysRadioButton = new Telerik.WinControls.UI.RadRadioButton();
            this.exportButton = new Telerik.WinControls.UI.RadButton();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.orderDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nwindDataSet = new SpreadExport.NwindDataSet();
            this.order_DetailsTableAdapter = new SpreadExport.NwindDataSetTableAdapters.Order_DetailsTableAdapter();
            this.ordersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ordersTableAdapter = new SpreadExport.NwindDataSetTableAdapters.OrdersTableAdapter();
            this.customersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.customersTableAdapter = new SpreadExport.NwindDataSetTableAdapters.CustomersTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).BeginInit();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customizeVisualSettingsCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exportHierarchyCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdfRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.csvRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xlsxRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exportVisualSettingsCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox5)).BeginInit();
            this.radGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.enablePagingCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pagingAllPagesRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pagingCurrentPageOnlyRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).BeginInit();
            this.radGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.showSummaryCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.summariesDoNotExportRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.summariesExportOnlyBottomRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.summariesExportOnlyTopRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.summariesExportAllRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).BeginInit();
            this.radGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hideFirstRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hiddenRowExportAsHiddenRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hiddenRowDoNotExportRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hiddenRowExportAlwaysRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hideColumnsCheckBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hiddenColumnExporAsHiddenRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hiddenColumnDoNotExportRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hiddenColumnExportAlwaysRadioButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exportButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nwindDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ordersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.customizeVisualSettingsCheckBox);
            this.settingsPanel.Controls.Add(this.exportHierarchyCheckBox);
            this.settingsPanel.Controls.Add(this.radGroupBox1);
            this.settingsPanel.Controls.Add(this.exportVisualSettingsCheckBox);
            this.settingsPanel.Controls.Add(this.radGroupBox5);
            this.settingsPanel.Controls.Add(this.radGroupBox4);
            this.settingsPanel.Controls.Add(this.radGroupBox3);
            this.settingsPanel.Controls.Add(this.radGroupBox2);
            this.settingsPanel.Controls.Add(this.exportButton);
            this.settingsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.settingsPanel.Location = new System.Drawing.Point(826, 0);
            this.settingsPanel.Name = "settingsPanel";
            this.settingsPanel.Size = new System.Drawing.Size(200, 748);
            this.settingsPanel.TabIndex = 3;
            this.settingsPanel.ThemeName = "ControlDefault";
            // 
            // customizeVisualSettingsCheckBox
            // 
            this.customizeVisualSettingsCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.customizeVisualSettingsCheckBox.AutoSize = false;
            this.customizeVisualSettingsCheckBox.Location = new System.Drawing.Point(8, 41);
            this.customizeVisualSettingsCheckBox.Name = "customizeVisualSettingsCheckBox";
            this.customizeVisualSettingsCheckBox.Size = new System.Drawing.Size(180, 34);
            this.customizeVisualSettingsCheckBox.TabIndex = 7;
            this.customizeVisualSettingsCheckBox.Text = "Customize visual settings when exporting";
            this.customizeVisualSettingsCheckBox.TextWrap = true;
            ((Telerik.WinControls.UI.RadCheckBoxElement)(this.customizeVisualSettingsCheckBox.GetChildAt(0))).Text = "Customize visual settings when exporting";
            // 
            // exportHierarchyCheckBox
            // 
            this.exportHierarchyCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.exportHierarchyCheckBox.Location = new System.Drawing.Point(8, 105);
            this.exportHierarchyCheckBox.Name = "exportHierarchyCheckBox";
            this.exportHierarchyCheckBox.Size = new System.Drawing.Size(100, 18);
            this.exportHierarchyCheckBox.TabIndex = 7;
            this.exportHierarchyCheckBox.Text = "ExportHierarchy";
            this.exportHierarchyCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.exportHierarchyCheckBox_ToggleStateChanged);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox1.Controls.Add(this.txtRadioButton);
            this.radGroupBox1.Controls.Add(this.pdfRadioButton);
            this.radGroupBox1.Controls.Add(this.csvRadioButton);
            this.radGroupBox1.Controls.Add(this.xlsxRadioButton);
            this.radGroupBox1.HeaderText = "Export format";
            this.radGroupBox1.Location = new System.Drawing.Point(8, 130);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(180, 117);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = "Export format";
            // 
            // txtRadioButton
            // 
            this.txtRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtRadioButton.Location = new System.Drawing.Point(5, 94);
            this.txtRadioButton.Name = "txtRadioButton";
            this.txtRadioButton.Size = new System.Drawing.Size(39, 18);
            this.txtRadioButton.TabIndex = 2;
            this.txtRadioButton.TabStop = false;
            this.txtRadioButton.Text = "TXT";
            this.txtRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.txtRadioButton_ToggleStateChanged);
            // 
            // pdfRadioButton
            // 
            this.pdfRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pdfRadioButton.Location = new System.Drawing.Point(5, 70);
            this.pdfRadioButton.Name = "pdfRadioButton";
            this.pdfRadioButton.Size = new System.Drawing.Size(40, 18);
            this.pdfRadioButton.TabIndex = 1;
            this.pdfRadioButton.TabStop = false;
            this.pdfRadioButton.Text = "PDF";
            this.pdfRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.pdfRadioButton_ToggleStateChanged);
            // 
            // csvRadioButton
            // 
            this.csvRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.csvRadioButton.Location = new System.Drawing.Point(5, 46);
            this.csvRadioButton.Name = "csvRadioButton";
            this.csvRadioButton.Size = new System.Drawing.Size(40, 18);
            this.csvRadioButton.TabIndex = 1;
            this.csvRadioButton.TabStop = false;
            this.csvRadioButton.Text = "CSV";
            this.csvRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.csvRadioButton_ToggleStateChanged);
            // 
            // xlsxRadioButton
            // 
            this.xlsxRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.xlsxRadioButton.Location = new System.Drawing.Point(5, 22);
            this.xlsxRadioButton.Name = "xlsxRadioButton";
            this.xlsxRadioButton.Size = new System.Drawing.Size(45, 18);
            this.xlsxRadioButton.TabIndex = 0;
            this.xlsxRadioButton.TabStop = false;
            this.xlsxRadioButton.Text = "XLSX";
            this.xlsxRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.xlsxRadioButton_ToggleStateChanged);
            // 
            // exportVisualSettingsCheckBox
            // 
            this.exportVisualSettingsCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.exportVisualSettingsCheckBox.Location = new System.Drawing.Point(8, 81);
            this.exportVisualSettingsCheckBox.Name = "exportVisualSettingsCheckBox";
            this.exportVisualSettingsCheckBox.Size = new System.Drawing.Size(122, 18);
            this.exportVisualSettingsCheckBox.TabIndex = 6;
            this.exportVisualSettingsCheckBox.Text = "ExportVisualSettings";
            this.exportVisualSettingsCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.exportVisualSettingsCheckBox_ToggleStateChanged);
            // 
            // radGroupBox5
            // 
            this.radGroupBox5.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox5.Controls.Add(this.enablePagingCheckBox);
            this.radGroupBox5.Controls.Add(this.pagingAllPagesRadioButton);
            this.radGroupBox5.Controls.Add(this.pagingCurrentPageOnlyRadioButton);
            this.radGroupBox5.HeaderText = "Paging export option";
            this.radGroupBox5.Location = new System.Drawing.Point(8, 649);
            this.radGroupBox5.Name = "radGroupBox5";
            this.radGroupBox5.Size = new System.Drawing.Size(180, 91);
            this.radGroupBox5.TabIndex = 5;
            this.radGroupBox5.Text = "Paging export option";
            // 
            // enablePagingCheckBox
            // 
            this.enablePagingCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.enablePagingCheckBox.Location = new System.Drawing.Point(5, 21);
            this.enablePagingCheckBox.Name = "enablePagingCheckBox";
            this.enablePagingCheckBox.Size = new System.Drawing.Size(91, 18);
            this.enablePagingCheckBox.TabIndex = 8;
            this.enablePagingCheckBox.Text = "Enable paging";
            this.enablePagingCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radCheckBox3_ToggleStateChanged);
            // 
            // pagingAllPagesRadioButton
            // 
            this.pagingAllPagesRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pagingAllPagesRadioButton.Location = new System.Drawing.Point(5, 69);
            this.pagingAllPagesRadioButton.Name = "pagingAllPagesRadioButton";
            this.pagingAllPagesRadioButton.Size = new System.Drawing.Size(62, 18);
            this.pagingAllPagesRadioButton.TabIndex = 1;
            this.pagingAllPagesRadioButton.Text = "AllPages";
            this.pagingAllPagesRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.pagingAllPagesRadioButton_ToggleStateChanged);
            // 
            // pagingCurrentPageOnlyRadioButton
            // 
            this.pagingCurrentPageOnlyRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pagingCurrentPageOnlyRadioButton.Location = new System.Drawing.Point(5, 45);
            this.pagingCurrentPageOnlyRadioButton.Name = "pagingCurrentPageOnlyRadioButton";
            this.pagingCurrentPageOnlyRadioButton.Size = new System.Drawing.Size(106, 18);
            this.pagingCurrentPageOnlyRadioButton.TabIndex = 0;
            this.pagingCurrentPageOnlyRadioButton.Text = "CurrentPageOnly";
            this.pagingCurrentPageOnlyRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.pagingCurrentPageOnlyRadioButton_ToggleStateChanged);
            // 
            // radGroupBox4
            // 
            this.radGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox4.Controls.Add(this.showSummaryCheckBox);
            this.radGroupBox4.Controls.Add(this.summariesDoNotExportRadioButton);
            this.radGroupBox4.Controls.Add(this.summariesExportOnlyBottomRadioButton);
            this.radGroupBox4.Controls.Add(this.summariesExportOnlyTopRadioButton);
            this.radGroupBox4.Controls.Add(this.summariesExportAllRadioButton);
            this.radGroupBox4.HeaderText = "Summaries option";
            this.radGroupBox4.Location = new System.Drawing.Point(8, 503);
            this.radGroupBox4.Name = "radGroupBox4";
            this.radGroupBox4.Size = new System.Drawing.Size(180, 140);
            this.radGroupBox4.TabIndex = 4;
            this.radGroupBox4.Text = "Summaries option";
            // 
            // showSummaryCheckBox
            // 
            this.showSummaryCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.showSummaryCheckBox.Location = new System.Drawing.Point(5, 21);
            this.showSummaryCheckBox.Name = "showSummaryCheckBox";
            this.showSummaryCheckBox.Size = new System.Drawing.Size(96, 18);
            this.showSummaryCheckBox.TabIndex = 11;
            this.showSummaryCheckBox.Text = "Show summary";
            this.showSummaryCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.showSummaryCheckBox_ToggleStateChanged);
            // 
            // summariesDoNotExportRadioButton
            // 
            this.summariesDoNotExportRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.summariesDoNotExportRadioButton.Location = new System.Drawing.Point(5, 117);
            this.summariesDoNotExportRadioButton.Name = "summariesDoNotExportRadioButton";
            this.summariesDoNotExportRadioButton.Size = new System.Drawing.Size(86, 18);
            this.summariesDoNotExportRadioButton.TabIndex = 2;
            this.summariesDoNotExportRadioButton.Text = "DoNotExport";
            this.summariesDoNotExportRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.summariesDoNotExportRadioButton_ToggleStateChanged);
            // 
            // summariesExportOnlyBottomRadioButton
            // 
            this.summariesExportOnlyBottomRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.summariesExportOnlyBottomRadioButton.Location = new System.Drawing.Point(5, 93);
            this.summariesExportOnlyBottomRadioButton.Name = "summariesExportOnlyBottomRadioButton";
            this.summariesExportOnlyBottomRadioButton.Size = new System.Drawing.Size(113, 18);
            this.summariesExportOnlyBottomRadioButton.TabIndex = 1;
            this.summariesExportOnlyBottomRadioButton.Text = "ExportOnlyBottom";
            this.summariesExportOnlyBottomRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.summariesExportOnlyBottomRadioButton_ToggleStateChanged);
            // 
            // summariesExportOnlyTopRadioButton
            // 
            this.summariesExportOnlyTopRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.summariesExportOnlyTopRadioButton.Location = new System.Drawing.Point(5, 69);
            this.summariesExportOnlyTopRadioButton.Name = "summariesExportOnlyTopRadioButton";
            this.summariesExportOnlyTopRadioButton.Size = new System.Drawing.Size(95, 18);
            this.summariesExportOnlyTopRadioButton.TabIndex = 1;
            this.summariesExportOnlyTopRadioButton.Text = "ExportOnlyTop";
            this.summariesExportOnlyTopRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.summariesExportOnlyTopRadioButton_ToggleStateChanged);
            // 
            // summariesExportAllRadioButton
            // 
            this.summariesExportAllRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.summariesExportAllRadioButton.Location = new System.Drawing.Point(5, 45);
            this.summariesExportAllRadioButton.Name = "summariesExportAllRadioButton";
            this.summariesExportAllRadioButton.Size = new System.Drawing.Size(65, 18);
            this.summariesExportAllRadioButton.TabIndex = 0;
            this.summariesExportAllRadioButton.Text = "ExportAll";
            this.summariesExportAllRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.summariesExportAllRadioButton_ToggleStateChanged);
            // 
            // radGroupBox3
            // 
            this.radGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox3.Controls.Add(this.hideFirstRow);
            this.radGroupBox3.Controls.Add(this.hiddenRowExportAsHiddenRadioButton);
            this.radGroupBox3.Controls.Add(this.hiddenRowDoNotExportRadioButton);
            this.radGroupBox3.Controls.Add(this.hiddenRowExportAlwaysRadioButton);
            this.radGroupBox3.HeaderText = "Hidden row option";
            this.radGroupBox3.Location = new System.Drawing.Point(8, 381);
            this.radGroupBox3.Name = "radGroupBox3";
            this.radGroupBox3.Size = new System.Drawing.Size(180, 116);
            this.radGroupBox3.TabIndex = 4;
            this.radGroupBox3.Text = "Hidden row option";
            // 
            // hideFirstRow
            // 
            this.hideFirstRow.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.hideFirstRow.Location = new System.Drawing.Point(5, 21);
            this.hideFirstRow.Name = "hideFirstRow";
            this.hideFirstRow.Size = new System.Drawing.Size(87, 18);
            this.hideFirstRow.TabIndex = 10;
            this.hideFirstRow.Text = "Hide first row";
            this.hideFirstRow.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.hideFirstRow_ToggleStateChanged);
            // 
            // hiddenRowExportAsHiddenRadioButton
            // 
            this.hiddenRowExportAsHiddenRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.hiddenRowExportAsHiddenRadioButton.Location = new System.Drawing.Point(5, 93);
            this.hiddenRowExportAsHiddenRadioButton.Name = "hiddenRowExportAsHiddenRadioButton";
            this.hiddenRowExportAsHiddenRadioButton.Size = new System.Drawing.Size(101, 18);
            this.hiddenRowExportAsHiddenRadioButton.TabIndex = 1;
            this.hiddenRowExportAsHiddenRadioButton.Text = "ExportAsHidden";
            this.hiddenRowExportAsHiddenRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.hiddenRowExportAsHiddenRadioButton_ToggleStateChanged);
            // 
            // hiddenRowDoNotExportRadioButton
            // 
            this.hiddenRowDoNotExportRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.hiddenRowDoNotExportRadioButton.Location = new System.Drawing.Point(5, 69);
            this.hiddenRowDoNotExportRadioButton.Name = "hiddenRowDoNotExportRadioButton";
            this.hiddenRowDoNotExportRadioButton.Size = new System.Drawing.Size(86, 18);
            this.hiddenRowDoNotExportRadioButton.TabIndex = 1;
            this.hiddenRowDoNotExportRadioButton.Text = "DoNotExport";
            this.hiddenRowDoNotExportRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.hiddenRowDoNotExportRadioButton_ToggleStateChanged);
            // 
            // hiddenRowExportAlwaysRadioButton
            // 
            this.hiddenRowExportAlwaysRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.hiddenRowExportAlwaysRadioButton.Location = new System.Drawing.Point(5, 45);
            this.hiddenRowExportAlwaysRadioButton.Name = "hiddenRowExportAlwaysRadioButton";
            this.hiddenRowExportAlwaysRadioButton.Size = new System.Drawing.Size(86, 18);
            this.hiddenRowExportAlwaysRadioButton.TabIndex = 0;
            this.hiddenRowExportAlwaysRadioButton.Text = "ExportAlways";
            this.hiddenRowExportAlwaysRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.hiddenRowExportAlwaysRadioButton_ToggleStateChanged);
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.radGroupBox2.Controls.Add(this.hideColumnsCheckBox);
            this.radGroupBox2.Controls.Add(this.hiddenColumnExporAsHiddenRadioButton);
            this.radGroupBox2.Controls.Add(this.hiddenColumnDoNotExportRadioButton);
            this.radGroupBox2.Controls.Add(this.hiddenColumnExportAlwaysRadioButton);
            this.radGroupBox2.HeaderText = "Hidden column option";
            this.radGroupBox2.Location = new System.Drawing.Point(8, 253);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(180, 122);
            this.radGroupBox2.TabIndex = 3;
            this.radGroupBox2.Text = "Hidden column option";
            // 
            // hideColumnsCheckBox
            // 
            this.hideColumnsCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.hideColumnsCheckBox.Location = new System.Drawing.Point(5, 21);
            this.hideColumnsCheckBox.Name = "hideColumnsCheckBox";
            this.hideColumnsCheckBox.Size = new System.Drawing.Size(119, 18);
            this.hideColumnsCheckBox.TabIndex = 9;
            this.hideColumnsCheckBox.Text = "Hide some columns";
            this.hideColumnsCheckBox.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.hideColumnsCheckBox_ToggleStateChanged);
            // 
            // hiddenColumnExporAsHiddenRadioButton
            // 
            this.hiddenColumnExporAsHiddenRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.hiddenColumnExporAsHiddenRadioButton.Location = new System.Drawing.Point(5, 93);
            this.hiddenColumnExporAsHiddenRadioButton.Name = "hiddenColumnExporAsHiddenRadioButton";
            this.hiddenColumnExporAsHiddenRadioButton.Size = new System.Drawing.Size(101, 18);
            this.hiddenColumnExporAsHiddenRadioButton.TabIndex = 1;
            this.hiddenColumnExporAsHiddenRadioButton.Text = "ExportAsHidden";
            this.hiddenColumnExporAsHiddenRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.hiddenColumnExporAsHiddenRadioButton_ToggleStateChanged);
            // 
            // hiddenColumnDoNotExportRadioButton
            // 
            this.hiddenColumnDoNotExportRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.hiddenColumnDoNotExportRadioButton.Location = new System.Drawing.Point(5, 69);
            this.hiddenColumnDoNotExportRadioButton.Name = "hiddenColumnDoNotExportRadioButton";
            this.hiddenColumnDoNotExportRadioButton.Size = new System.Drawing.Size(86, 18);
            this.hiddenColumnDoNotExportRadioButton.TabIndex = 1;
            this.hiddenColumnDoNotExportRadioButton.Text = "DoNotExport";
            this.hiddenColumnDoNotExportRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.hiddenColumnDoNotExportRadioButton_ToggleStateChanged);
            // 
            // hiddenColumnExportAlwaysRadioButton
            // 
            this.hiddenColumnExportAlwaysRadioButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.hiddenColumnExportAlwaysRadioButton.Location = new System.Drawing.Point(5, 45);
            this.hiddenColumnExportAlwaysRadioButton.Name = "hiddenColumnExportAlwaysRadioButton";
            this.hiddenColumnExportAlwaysRadioButton.Size = new System.Drawing.Size(86, 18);
            this.hiddenColumnExportAlwaysRadioButton.TabIndex = 0;
            this.hiddenColumnExportAlwaysRadioButton.Text = "ExportAlways";
            this.hiddenColumnExportAlwaysRadioButton.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.hiddenColumnExportAlwaysRadioButton_ToggleStateChanged);
            // 
            // exportButton
            // 
            this.exportButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.exportButton.Location = new System.Drawing.Point(8, 12);
            this.exportButton.Name = "exportButton";
            this.exportButton.Size = new System.Drawing.Size(180, 23);
            this.exportButton.TabIndex = 0;
            this.exportButton.Text = "Export";
            this.exportButton.Click += new System.EventHandler(this.exportButton_Click);
            // 
            // radGridView1
            // 
            this.radGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView1.Location = new System.Drawing.Point(0, 0);
            // 
            // radGridView1
            // 
            this.radGridView1.MasterTemplate.PageSize = 10;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.Size = new System.Drawing.Size(826, 748);
            this.radGridView1.TabIndex = 2;
            this.radGridView1.Text = "radGridView1";
            // 
            // orderDetailsBindingSource
            // 
            this.orderDetailsBindingSource.DataMember = "Order Details";
            this.orderDetailsBindingSource.DataSource = this.nwindDataSet;
            // 
            // nwindDataSet
            // 
            this.nwindDataSet.DataSetName = "NwindDataSet";
            this.nwindDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // order_DetailsTableAdapter
            // 
            this.order_DetailsTableAdapter.ClearBeforeFill = true;
            // 
            // ordersBindingSource
            // 
            this.ordersBindingSource.DataMember = "Orders";
            this.ordersBindingSource.DataSource = this.nwindDataSet;
            // 
            // ordersTableAdapter
            // 
            this.ordersTableAdapter.ClearBeforeFill = true;
            // 
            // customersBindingSource
            // 
            this.customersBindingSource.DataMember = "Customers";
            this.customersBindingSource.DataSource = this.nwindDataSet;
            // 
            // customersTableAdapter
            // 
            this.customersTableAdapter.ClearBeforeFill = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1026, 748);
            this.Controls.Add(this.radGridView1);
            this.Controls.Add(this.settingsPanel);
            this.Name = "Form1";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.settingsPanel)).EndInit();
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.customizeVisualSettingsCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exportHierarchyCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pdfRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.csvRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xlsxRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exportVisualSettingsCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox5)).EndInit();
            this.radGroupBox5.ResumeLayout(false);
            this.radGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.enablePagingCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pagingAllPagesRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pagingCurrentPageOnlyRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox4)).EndInit();
            this.radGroupBox4.ResumeLayout(false);
            this.radGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.showSummaryCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.summariesDoNotExportRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.summariesExportOnlyBottomRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.summariesExportOnlyTopRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.summariesExportAllRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox3)).EndInit();
            this.radGroupBox3.ResumeLayout(false);
            this.radGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hideFirstRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hiddenRowExportAsHiddenRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hiddenRowDoNotExportRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hiddenRowExportAlwaysRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hideColumnsCheckBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hiddenColumnExporAsHiddenRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hiddenColumnDoNotExportRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hiddenColumnExportAlwaysRadioButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exportButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nwindDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ordersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadButton exportButton;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadRadioButton hiddenColumnExporAsHiddenRadioButton;
        private Telerik.WinControls.UI.RadRadioButton hiddenColumnDoNotExportRadioButton;
        private Telerik.WinControls.UI.RadRadioButton hiddenColumnExportAlwaysRadioButton;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadRadioButton txtRadioButton;
        private Telerik.WinControls.UI.RadRadioButton pdfRadioButton;
        private Telerik.WinControls.UI.RadRadioButton csvRadioButton;
        private Telerik.WinControls.UI.RadRadioButton xlsxRadioButton;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox5;
        private Telerik.WinControls.UI.RadRadioButton pagingAllPagesRadioButton;
        private Telerik.WinControls.UI.RadRadioButton pagingCurrentPageOnlyRadioButton;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox4;
        private Telerik.WinControls.UI.RadRadioButton summariesDoNotExportRadioButton;
        private Telerik.WinControls.UI.RadRadioButton summariesExportOnlyBottomRadioButton;
        private Telerik.WinControls.UI.RadRadioButton summariesExportOnlyTopRadioButton;
        private Telerik.WinControls.UI.RadRadioButton summariesExportAllRadioButton;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox3;
        private Telerik.WinControls.UI.RadRadioButton hiddenRowExportAsHiddenRadioButton;
        private Telerik.WinControls.UI.RadRadioButton hiddenRowDoNotExportRadioButton;
        private Telerik.WinControls.UI.RadRadioButton hiddenRowExportAlwaysRadioButton;
        private Telerik.WinControls.UI.RadCheckBox exportHierarchyCheckBox;
        private Telerik.WinControls.UI.RadCheckBox exportVisualSettingsCheckBox;
        private Telerik.WinControls.UI.RadCheckBox enablePagingCheckBox;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.WinControls.UI.RadCheckBox showSummaryCheckBox;
        private Telerik.WinControls.UI.RadCheckBox hideFirstRow;
        private Telerik.WinControls.UI.RadCheckBox hideColumnsCheckBox;
        private Telerik.WinControls.UI.RadCheckBox customizeVisualSettingsCheckBox;
        private Telerik.WinControls.UI.RadPanel settingsPanel;
        private SpreadExport.NwindDataSet nwindDataSet;
        private System.Windows.Forms.BindingSource orderDetailsBindingSource;
        private SpreadExport.NwindDataSetTableAdapters.Order_DetailsTableAdapter order_DetailsTableAdapter;
        private System.Windows.Forms.BindingSource ordersBindingSource;
        private SpreadExport.NwindDataSetTableAdapters.OrdersTableAdapter ordersTableAdapter;
        private System.Windows.Forms.BindingSource customersBindingSource;
        private NwindDataSetTableAdapters.CustomersTableAdapter customersTableAdapter;
    }
}
