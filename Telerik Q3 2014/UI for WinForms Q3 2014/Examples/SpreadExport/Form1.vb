Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.IO
Imports System.Text
Imports System.Windows.Forms
Imports Telerik.WinControls.Data
Imports Telerik.WinControls.Themes
Imports Telerik.WinControls.UI

Namespace SpreadExport
	Public Partial Class Form1
		Inherits RadForm
		Private spreadExporter As Telerik.WinControls.UI.Export.SpreadExport.SpreadExport

		Public Sub New()
			InitializeComponent()

			Telerik.WinControls.ThemeResolutionService.ApplicationThemeName = "TelerikMetro" 'set default theme

			If Program.themeName <> "" Then 'set the example theme to the same theme QSF uses
				Telerik.WinControls.ThemeResolutionService.ApplicationThemeName = Program.themeName
			End If
		End Sub

		Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
			Me.customersTableAdapter.Fill(Me.nwindDataSet.Customers)
			Me.ordersTableAdapter.Fill(Me.nwindDataSet.Orders)
			Me.order_DetailsTableAdapter.Fill(Me.nwindDataSet.Order_Details)

			SetupExample()

			spreadExporter = New Telerik.WinControls.UI.Export.SpreadExport.SpreadExport(radGridView1)
			AddHandler spreadExporter.CellFormatting, AddressOf spreadExporter_CellFormatting

			SetupInitialSettings()
		End Sub

		Private Sub spreadExporter_CellFormatting(ByVal sender As Object, ByVal e As Telerik.WinControls.UI.Export.SpreadExport.CellFormattingEventArgs)
			If customizeVisualSettingsCheckBox.ToggleState <> Telerik.WinControls.Enumerations.ToggleState.On Then
				Return
			End If

			If e.GridRowInfoType Is GetType(GridViewTableHeaderRowInfo) Then
				e.CellStyleInfo.Underline = True

				If e.GridCellInfo.RowInfo.HierarchyLevel = 0 Then
					e.CellStyleInfo.BackColor = Color.DeepSkyBlue
				ElseIf e.GridCellInfo.RowInfo.HierarchyLevel = 1 Then
					e.CellStyleInfo.BackColor = Color.LightSkyBlue
				End If
			End If

			If e.GridRowInfoType Is GetType(GridViewHierarchyRowInfo) Then
				If e.GridCellInfo.RowInfo.HierarchyLevel = 0 Then
					e.CellStyleInfo.IsItalic = True
					e.CellStyleInfo.FontSize = 12
					e.CellStyleInfo.BackColor = Color.GreenYellow
				ElseIf e.GridCellInfo.RowInfo.HierarchyLevel = 1 Then
					e.CellStyleInfo.ForeColor = Color.DarkGreen
					e.CellStyleInfo.BackColor = Color.LightGreen
				End If
			End If
		End Sub

		Private Sub SetupInitialSettings()
			exportVisualSettingsCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
			exportHierarchyCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
			xlsxRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
			hiddenColumnDoNotExportRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
			hiddenRowDoNotExportRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
			summariesExportAllRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
			pagingCurrentPageOnlyRadioButton.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On

			hideColumnsCheckBox.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
		End Sub

		Private Sub SetupExample()
			Me.radGridView1.DataMember = "Customers"
			Me.radGridView1.AutoGenerateHierarchy = True
			Me.radGridView1.DataSource = nwindDataSet
			Me.SetupMasterForAutoGenerateHierarchy()

			Me.radGridView1.GroupDescriptors.Add(New GroupDescriptor(New SortDescriptor("City", ListSortDirection.Ascending)))
			Me.radGridView1.Groups(0).Expand()

			Me.radGridView1.Groups(0).GetItems()(0).IsExpanded = True
			Me.radGridView1.Groups(0).GetItems()(0).ChildRows(0).IsExpanded = True
		End Sub

		Private Sub SetupMasterForAutoGenerateHierarchy()
			Me.radGridView1.MasterTemplate.Columns("CompanyName").HeaderText = "Company Name"
			Me.radGridView1.MasterTemplate.Columns("ContactName").HeaderText = "Contact Name"

			Me.radGridView1.MasterTemplate.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill

			Dim template As GridViewTemplate = Me.radGridView1.MasterTemplate.Templates(0)
			template.Columns("OrderID").HeaderText = "Order ID"
			template.Columns("ShipAddress").HeaderText = "Ship Address"
			template.Columns("OrderDate").HeaderText = "Order Date"
			template.Columns("OrderDate").TextAlignment = ContentAlignment.MiddleRight
			template.Columns("OrderDate").FormatString = "{0: ddd, MM/dd}"
			template.Columns("CustomerID").IsVisible = False
			template.Columns("EmployeeID").IsVisible = False
			template.Columns("RequiredDate").IsVisible = False
			template.Columns("ShippedDate").IsVisible = False
			template.Columns("ShipVia").IsVisible = False
			template.Columns("ShipName").IsVisible = False
			template.Columns("ShipCity").IsVisible = False
			template.Columns("ShipRegion").IsVisible = False
			template.Columns("ShipPostalCode").IsVisible = False
			template.Columns("ShipCity").IsVisible = False
			template.Columns("ShipName").IsVisible = False
			template.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill

			Me.radGridView1.MasterTemplate.Templates(0).Templates.RemoveAt(0)
		End Sub

		#Region "Event handlers"

		Private Sub exportButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles exportButton.Click
			Dim dialog As SaveFileDialog = New SaveFileDialog()
			If dialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
				spreadExporter.RunExport(dialog.FileName)
			End If
		End Sub

		Private Sub hideColumnsCheckBox_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles hideColumnsCheckBox.ToggleStateChanged
			Me.radGridView1.MasterTemplate.Columns("CustomerID").IsVisible = args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off
			Me.radGridView1.MasterTemplate.Columns("ContactTitle").IsVisible = args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off
			Me.radGridView1.MasterTemplate.Columns("Region").IsVisible = args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off
			Me.radGridView1.MasterTemplate.Columns("PostalCode").IsVisible = args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off
			Me.radGridView1.MasterTemplate.Columns("Country").IsVisible = args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off
			Me.radGridView1.MasterTemplate.Columns("Fax").IsVisible = args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off
		End Sub

		Private Sub hideFirstRow_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles hideFirstRow.ToggleStateChanged
			radGridView1.ChildRows(0).IsVisible = args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.Off
		End Sub

		Private Sub showSummaryCheckBox_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles showSummaryCheckBox.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				Dim summaryItem As GridViewSummaryItem = New GridViewSummaryItem()
				summaryItem.Name = "CompanyName"
				summaryItem.Aggregate = GridAggregateFunction.Count

				Dim summaryRowTopItem As GridViewSummaryRowItem = New GridViewSummaryRowItem()
				summaryRowTopItem.Add(summaryItem)
				Me.radGridView1.SummaryRowsTop.Add(summaryRowTopItem)

				Dim summaryRowBottomItem As GridViewSummaryRowItem = New GridViewSummaryRowItem()
				summaryRowBottomItem.Add(summaryItem)
				Me.radGridView1.SummaryRowsBottom.Add(summaryRowBottomItem)
			Else
				radGridView1.SummaryRowsBottom.Clear()
				radGridView1.SummaryRowsTop.Clear()
			End If
		End Sub

		Private Sub exportVisualSettingsCheckBox_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles exportVisualSettingsCheckBox.ToggleStateChanged
			spreadExporter.ExportVisualSettings = args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
		End Sub

		Private Sub exportHierarchyCheckBox_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles exportHierarchyCheckBox.ToggleStateChanged
			spreadExporter.ExportHierarchy = args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
		End Sub

		Private Sub xlsxRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles xlsxRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.ExportFormat = Telerik.WinControls.UI.Export.SpreadExport.SpreadExportFormat.Xlsx
			End If
		End Sub

		Private Sub csvRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles csvRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.ExportFormat = Telerik.WinControls.UI.Export.SpreadExport.SpreadExportFormat.Csv
			End If
		End Sub

		Private Sub pdfRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles pdfRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.ExportFormat = Telerik.WinControls.UI.Export.SpreadExport.SpreadExportFormat.Pdf
			End If
		End Sub

		Private Sub txtRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles txtRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.ExportFormat = Telerik.WinControls.UI.Export.SpreadExport.SpreadExportFormat.Txt
			End If
		End Sub

		Private Sub hiddenColumnExportAlwaysRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles hiddenColumnExportAlwaysRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.HiddenColumnOption = Telerik.WinControls.UI.Export.HiddenOption.ExportAlways
			End If
		End Sub

		Private Sub hiddenColumnDoNotExportRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles hiddenColumnDoNotExportRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.HiddenColumnOption = Telerik.WinControls.UI.Export.HiddenOption.DoNotExport
			End If
		End Sub

		Private Sub hiddenColumnExporAsHiddenRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles hiddenColumnExporAsHiddenRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.HiddenColumnOption = Telerik.WinControls.UI.Export.HiddenOption.ExportAsHidden
			End If
		End Sub

		Private Sub hiddenRowExportAlwaysRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles hiddenRowExportAlwaysRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.HiddenRowOption = Telerik.WinControls.UI.Export.HiddenOption.ExportAlways
			End If
		End Sub

		Private Sub hiddenRowDoNotExportRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles hiddenRowDoNotExportRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.HiddenRowOption = Telerik.WinControls.UI.Export.HiddenOption.DoNotExport
			End If
		End Sub

		Private Sub hiddenRowExportAsHiddenRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles hiddenRowExportAsHiddenRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.HiddenRowOption = Telerik.WinControls.UI.Export.HiddenOption.ExportAsHidden
			End If
		End Sub

		Private Sub summariesExportAllRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles summariesExportAllRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.SummariesExportOption = Telerik.WinControls.UI.Export.SummariesOption.ExportAll
			End If
		End Sub

		Private Sub summariesExportOnlyTopRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles summariesExportOnlyTopRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.SummariesExportOption = Telerik.WinControls.UI.Export.SummariesOption.ExportOnlyTop
			End If
		End Sub

		Private Sub summariesExportOnlyBottomRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles summariesExportOnlyBottomRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.SummariesExportOption = Telerik.WinControls.UI.Export.SummariesOption.ExportOnlyBottom
			End If
		End Sub

		Private Sub summariesDoNotExportRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles summariesDoNotExportRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.SummariesExportOption = Telerik.WinControls.UI.Export.SummariesOption.DoNotExport
			End If
		End Sub

		Private Sub radCheckBox3_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles enablePagingCheckBox.ToggleStateChanged
			radGridView1.EnablePaging = args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On
		End Sub

		Private Sub pagingCurrentPageOnlyRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles pagingCurrentPageOnlyRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.PagingExportOption = Telerik.WinControls.UI.Export.PagingExportOption.CurrentPageOnly
			End If
		End Sub

		Private Sub pagingAllPagesRadioButton_ToggleStateChanged(ByVal sender As Object, ByVal args As StateChangedEventArgs) Handles pagingAllPagesRadioButton.ToggleStateChanged
			If args.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On Then
				spreadExporter.PagingExportOption = Telerik.WinControls.UI.Export.PagingExportOption.AllPages
			End If
		End Sub

		#End Region
	End Class
End Namespace